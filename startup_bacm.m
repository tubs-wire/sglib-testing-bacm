function startup_bacm(model_name)
% STARTUP Called automatically by Matlab at startup.
%   STARTUP gets automatically called by Matlab if it was started from THIS
%   directory (i.e. the sglib home directory). STARTUP just delegates the
%   work SGLIB_STARTUP, so that SGLIB_STARTUP can be called from anywhere
%   else without interfering with any other startup script that might be on
%   the path.
%
% Example (<a href="matlab:run_example startup">run</a>)
%   startup
%
% See also

%   original by Elmar Zander and some paths added by Noemi
%   Copyright 2006, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


% We do the real startup in a file with a special name (SGLIB_STARTUP) so
% the user can run it individually without any startup on the path
% interfering with this one
basepath=fileparts( mfilename('fullpath') );
run( fullfile( basepath, 'sglib-testing', 'startup' ) );

% need to get the basepath again, since sglib_startup resets it
basepath=fileparts( mfilename('fullpath') );

% Set additional paths into sglib-testing
sglibtestingpath=fullfile(basepath, 'sglib-testing');
addpath( fullfile(sglibtestingpath, 'data') );
addpath( genpath(fullfile(sglibtestingpath, 'proposed')) );

addpath(fullfile(sglibtestingpath, 'demo', 'models'));
addpath(fullfile(sglibtestingpath, 'demo', 'methods'));
addpath(fullfile(sglibtestingpath, 'demo', 'tutorial', 'tools'));
addpath(fullfile(sglibtestingpath, 'demo', 'lecture'));
%addpath(fullfile(sglibtestingpath, 'demo', 'lecture', 'param_estim'));


% Set additional paths into bacm
bacmpath=fullfile(basepath, 'bacm');
addpath(bacmpath);
addpath(fullfile(bacmpath, 'sglib', 'proposed'));
addpath(fullfile(bacmpath, 'sglib', 'proposed', 'statistics'));
addpath(fullfile(bacmpath, 'demo', 'models'));
addpath(fullfile(bacmpath, 'mymethods'));
addpath(fullfile(bacmpath, 'mymethods', 'update'));
addpath(fullfile(bacmpath, 'mymethods', 'update', 'MMSE_noemi'));
addpath(fullfile(bacmpath, 'myutils'));
addpath(fullfile(bacmpath, 'myutils', 'Colormaps'));
addpath(fullfile(bacmpath, 'myutils', 'eliduenisch-latexTable-cb53f04'));
addpath(fullfile(bacmpath, 'myobjects'));
%addpath(fullfile(bacmpath, 'models'));

if nargin>0
    switch model_name
        case 'AM_horizontal'
            addpath(genpath(fullfile(bacmpath, 'models', 'aircraft_models_new', 'Analyse_horisontal_with_controller')));
    end
end
