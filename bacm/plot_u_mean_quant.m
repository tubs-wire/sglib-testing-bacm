function h=plot_u_mean_quant(time, u_mean, u_quant_l, u_quant_u,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%to plot mean and variance of stochastic solution of ODE
%-in function of the TIME
%-mean value:   u_mean
%-variance:     u_var


numplots=size(u_quant_l,2);
%% create automatic y labels
ystringarray(1:numplots,1)={''};
for i=1:numplots
    ystringarray{i,:}={strcat('Y',num2str(i))};
end
%% Check and cut off NaN values
% ind_max=length(time);
% for i=1:numplots
%  ind_m=[ find(isnan(u_mean(:,i))); find(u_mean(:,i)==inf)];
%  %ind_v=[find(isnan(u_var(:,i)))  ;find(u_var(:,i)==inf); find(abs(sqrt(u_var(:,i))./u_mean(:,i))>10^6)];
%  ind_v=[find(isnan(u_var(:,i)))  ;find(u_var(:,i)==inf)];
%  if ~isempty(ind_m)
%      ind_m=ind_m-1;
%  end
%   if ~isempty(ind_v)
%      ind_v=ind_v-1;
%  end
%  ind_max=min([ind_m; ind_v; ind_max]);
% end
%
% if ~(ind_max==length(time))
%     time=time(1:ind_max);
%     u_mean=u_mean(1:ind_max, :);
%     u_var=u_var(1:ind_max, :);
%     warning('SGLIB:plot_u_mean_var', 'NaN values have been removed from u_mean and u_var in the plot')
% end
%%
options=varargin2options(varargin);
[fill_color,options]=get_option(options, 'fill_color', 'blue');
[line_color,options]=get_option(options, 'line_color', 'black');
[transparency,options]=get_option(options, 'transparency', 1);
[line_width,options]=get_option(options, 'line_width', 2);
[y_lim,options]=get_option(options, 'y_lim', []);
[ylabels,options]=get_option(options, 'ylabels', ystringarray);
[x_label,options]=get_option(options, 'xlabel', 'time');
[flag_subplot,options]=get_option(options, 'flag_subplot', true);
[title,options]=get_option(options, 'title', 'Mean and 90% confidence interval of the response');
[subplot_dim, options]=get_option(options, 'subplot_dim',[]);
[change_axis, options]=get_option(options, 'change_axis',false);
check_unsupported_options(options, mfilename);

if ~ (size(time,1)==1)
    time=time';
end

T=[time,fliplr(time)];
if isempty(subplot_dim) && flag_subplot
    if numplots>4
        hornumb=2;
        vertnumb=ceil(numplots/hornumb);
    else
        hornumb=1;
        vertnumb=numplots;
    end
elseif flag_subplot
    hornumb=subplot_dim(2);
    vertnumb=subplot_dim(1);
end
for i=1:numplots
    if flag_subplot
        subplot(vertnumb,hornumb,i)
    elseif i>1
        figure
    end
    U_min_max=[u_quant_l(:,i)',fliplr(u_quant_u(:,i)')];
    
    if change_axis
        h(1)=fill(U_min_max, T',fill_color,'FaceAlpha', transparency);
        hold on
        if ~isempty(u_mean)
            plot(u_mean(:,i), time, line_color,'LineWidth',line_width)
        end
        if i>(vertnumb-1)*hornumb
            xlabel(ylabels{i},'FontSize',14)
        end
        if rem(i,hornumb)==1
            ylabel(x_label,'FontSize',14)
        end
    else
        h(1)=fill(T',U_min_max,fill_color,'FaceAlpha', transparency);
        hold on
        if ~isempty(u_mean)
            h(2)=plot(time,u_mean(:,i),'Color', line_color,'LineWidth',2);
        end
        if i>(vertnumb-1)*hornumb
            xlabel(x_label,'FontSize',14)
        end
        if rem(i,hornumb)==1
            ylabel(ylabels{i},'FontSize',14)
        else
            ylabel(ylabels{i},'FontSize',14)
        end
        if ~isempty(y_lim)
            ylim(y_lim)
        end
    end
end
%if i==1
%   title(title, 'FontSize', 20);
%end


end



