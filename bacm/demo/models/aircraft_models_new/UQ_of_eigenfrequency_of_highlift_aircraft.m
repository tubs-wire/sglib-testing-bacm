%===================================================================
% SCRIPT:
%       UQ of eigenfrequency of SFB 880 highlift aircraft
%===================================================================
%
%
%   Noemi Friedman
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%% Init stuff
close_system 'SFB880_Library.mdl'
close_system 'DLR_SFB880_Test_Quat'
close_system 'DLR_SFB880_Trim_Quat'
clear all
close all 
clc
pathname=pwd;
name_sglib_dir=(fileparts(which('startup.m')));
name_root_dir=fileparts(name_sglib_dir);
cd(name_sglib_dir);
startup

%% Add path for the model (deterministic solver)
parent_dir=fileparts(fileparts( mfilename('fullpath') ));
addpath( fullfile(parent_dir, 'aircraft_models_new') );
addpath(genpath( fullfile(parent_dir,'models', 'aircraft_models_new', 'fullflap_sim_and_frequencies2') ));
addpath(genpath( fullfile(parent_dir,'models', 'aircraft_models_new', 'param_estim') ));

%% set path for results (in a folder in the root, where sglib is saved)
cd(name_root_dir)
name_dir='Results_SFB880_eigenfreq_with_Simparams';
mkdir(name_dir);
cd(name_dir);
saving_path=pwd;
cd(pathname);

 %% Run trimming sim in background
slCharacterEncoding('windows-1252')
eval('DLR_SFB880_Trim_Quat');

%% Definition of uncertain parameters

% List of uncertain parameters and their description:
% alpha_wfc_max:      maximal wfc (wing/fuselage) angle of attack (not included yet)
%
% C_mu:                     jet momentum coefficient (only for full flap config)
%
% V_TAS:                    initial true air velocity
%
% C_L0:                      lift coefficient at alpha=0
% The slope of the lift coeff C_L_ALPHA is the sum of:
% C_Lalpha:               slope of the lift coeff curve
% C_Lalpha_FL:          slope of the lift coeff curve due to full flap pos
% C_Lalpha_DN:        slope of the lift coeff curve due to full flap pos
%
% C_D0:                     drag coeff at alpha=0
% The linear coeff k1 is the sum of:
% k1:                         coeff to calculate the drag coeff (C_D)
% k1_FL:                    -||- due to full flap positions
% k1_DN:                  -||- due to Droop Nose
% The quadratic coeff k2 is the sum of:
% k2:                         coeff to calculate the drag coeff (C_D)
% k2_FL:                    -||- due to full flap positions
% k2_DN:                  -||- due to Droop Nose
%
% C_m0:                    pitching moment coefficient for zero angle of attack
%
% The slope of the pitching moment curve is the sum of:
% C_malpha:             pitching moment slope of the curve
% C_malpha_FL:        -||- due to full flap positions
% C_malpha_DN:      -||- due to Droop Nose

% Definition of the distribution of parameters
P{1}=SimParameter('Cmu', UniformDistribution(0.033*0.95, 0.033*1.05), 'C_{mu}');

P{2}=SimParameter('C_L0', UniformDistribution( 0.344540120967742-0.15, 0.344540120967742+0.15), 'C_{L0}');

P{3}=SimParameter( 'C_Lalpha', UniformDistribution( 4.734449604654664*0.95, 4.734449604654664*1.05),  'C_{L\alpha-CL}');
P{4}=SimParameter( 'C_Lalpha_FL', UniformDistribution( -1.236767277669634*1.05, -1.236767277669634*0.95),  'C_{L\alpha-FL}');
P{5}=SimParameter( 'C_Lalpha_DL', UniformDistribution( 0.310327867813788*0.95, 0.310327867813788*1.05),  'C_{L\alpha-DN}');

P{6}=SimParameter('C_D0', UniformDistribution(0.020882442004759-0.05, 0.020882442004759+0.05), 'C_{D0}');

P{7}=SimParameter('k1', UniformDistribution(0.010465923328436*0.9, 0.010465923328436*1.1), 'k_{1-CL}');
P{8}=SimParameter('k1_FL', UniformDistribution(-0.010465923328436*1.1, -0.010465923328436*0.9), 'k_{1-FL}');
P{9}=SimParameter('k1_DN', UniformDistribution(-0.131599412034937*1.1, -0.131599412034937*0.9), 'k_{1-DN}');

P{10}=SimParameter('k2', UniformDistribution(0.035063886442427*0.9, 0.035063886442427*1.1), 'k_{2-CL}');
P{11}=SimParameter('k2_FL', UniformDistribution(0.006179345655616*0.9, 0.006179345655616*1.1), 'k_{2-FL}');
P{12}=SimParameter('k2_DN', UniformDistribution(0.020036923921946*0.9, 0.020036923921946*1.1), 'k_{2-DN}');

P{13}=SimParameter('C_m0', UniformDistribution(-0.119375806451613-0.15, -0.119375806451613+0.15), 'C_{m0}');

P{14}=SimParameter('C_malpha', UniformDistribution(0.986319120553432*0.95, 0.986319120553432*1.05), 'C_{m\alpha-CL}');
P{15}=SimParameter('C_malpha_FL', UniformDistribution(-0.339182978441908*1.05, -0.339182978441908*0.95), 'C_{m\alpha-FL}');
P{16}=SimParameter('C_malpha_DN', UniformDistribution(0.270784598950292*0.95, 0.270784598950292*1.05), 'C_{m\alpha-DN}');

% Add parameters to the SimParameterSet PSET:
PSet=SimParamSet1(P{:});
PSet.set_fixed(); %set all parameters to their mean values
PSet.set_not_fixed(P(1:2)); %Set P{1} (C_L0) and {2} (C_Lalpha) random 

%% Initialize simulation
t_max=100;
t_min=0;
t_step=0.01;

num_params=PSet.num_params;
param_names=PSet.param_names;
model = highlift_eigen_init2(num_params, 't_min_max', [t_min, t_max], 't_step', t_step);
num_vars=model.model_info.num_vars;
%% Run reference(deterministic) solution
means=PSet.mean_vals;
params=PSet.param_vals_to_struct(means);

display('Running_reference (deterministic) solution');
start = tic;
[ref_sol, ref_solve_info]=funcall(model.model_info.solve_func, model, params);
t = toc(start);

 %% Caclulate solution at integration points
p_gPCE = 2;        %gPCE order
p_int = 3;      %integration order to calculate all other gpce coeff-s
grid='full_tensor';
%grid='smolyak';

%gpc expansion of parameters
[p_alpha, V_p, varserr]=  gpc_expand_RVs(PSet);
V_u=gpcbasis_create(V_p{1}, 'p', p_gPCE, 'full_tensor', false);

[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', grid);
M = gpcbasis_size(V_u, 1);
Q = length(w);

a = PSet.gpc_evaluate(p_alpha, V_p, x_ref);
A=gpcbasis_evaluate(V_u, x_ref);
saving_fname=strvarexpand('$saving_path$$filesep$p_gpce_$p_gPCE$_p_int_$p_gPCE$$grid$Q_$Q$');

%solve_options={'steptol', steptol, 'abstol', abstol}
u_ij=zeros(num_vars, Q);
u_i_alpha = zeros(model.model_info.num_vars, M);
for j = 1:Q
    display(strvarexpand('$j$/$Q$'));
    a_j = a(:, j);
    params_j=PSet.param_vals_to_struct(a_j);
    [u_i_j, model] = model_solve_s(model, params, 'saving_fname', saving_fname);
    u_ij(:,j)=u_i_j;
    %[u_i_j, model] = model_solve_s(model, params, 'solve_options', solve_options);
    x_j = x_ref(:, j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
    u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
end

%% Projection
u_i_alpha_proj=u_i_alpha;
[u_mean_proj, u_var_proj]=gpc_moments(u_i_alpha_proj, V_u);

%% collocation (interpolation)
u_i_alpha_col=A\u_ij;
[u_mean_col, u_var_col]=gpc_moments(u_i_alpha_proj, V_u);

%% Evaluation of results
p_u = 2;
[u_mean,u_var, time, other_info] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options, 'gpc_full_tensor', false, 'grid', 'smolyak', 'saving_path', saving_path);

%ind=(multiindex_order(V_u{2})>=3);
%u_i_alpha(:,ind)=0;
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
plot_u_mean_var(time, u_mean(:,1:4), u_var(:,1:4), 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)


%show_mean_var('Interpolation, tensor (response surface)', u_mean, u_var);
plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_tensorcoll_i_alpha = u_i_alpha;
