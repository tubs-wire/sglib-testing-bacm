function test_BACM

%%  Signal input

S=load('Signal_for_eigenmotions_long.mat');
V_TAS=S.V_TAS';
Time=S.Time';
Alpha=S.alpha';
clear 'S'
%Alpha=S.Alpha+3.925843707825837;
% alpha1 = 0.2;
% omega1 = 1.4;
% phi1 = 0.123;
% 
% alpha2 = 1.6;
% omega2 = 3.4;
% phi2 = 0.45;
% 
% x = linspace(0, 10, 100);
% 
% y1 = exp(-alpha1*x).*sin(omega1*x + phi1);
% y2 = -0.6 * exp(-alpha2*x).*sin(omega2*x + phi2);
% y = y1 + y2;

%% Get parameters for true air speed (V_TAS)

%[ampl1, alpha1, omega1, phi1] = find_params(Time, V_TAS, 'do_opt', false);

[ampl_est, alpha_est, omega_est, phi_est] = find_params(Time, V_TAS, 'do_opt', true);
y_est_speed = ampl_est * exp(-alpha_est*Time).*sin(omega_est*Time+phi_est);
norm(V_TAS-y_est_speed)

n=3000;
[ampl_est, alpha_est, omega_est, phi_est] = find_params(Time, V_TAS, 'do_opt', true, 'n', n);
y_est_speed = ampl_est * exp(-alpha_est*Time).*sin(omega_est*Time+phi_est);

norm(V_TAS-y_est_speed)
norm(V_TAS(n:end)-y_est_speed(n:end))
%% Get parameters for angle of attack (alpha)
n=3000; %Cut off beginning of signal, till this element
[ampl1_est, alpha1_est, omega1_est, phi1_est] = find_params(Time, Alpha, 'n', n, 'do_opt', true);
y1_est = ampl1_est * exp(-alpha1_est*Time).*sin(omega1_est*Time+phi1_est);

i=1:1500
[ampl2_est, alpha2_est, omega2_est, phi2_est] = find_params(Time(i), Alpha(i)-y1_est(i), 'do_opt', false);
y2_est = ampl2_est * exp(-alpha2_est*Time).*sin(omega2_est*Time+phi2_est);

y_est_Alpha = y1_est + y2_est;
%%
func = @(p)(optimfunc_dupl(Time, Alpha, p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8)));
    p0=[ampl1_est; alpha1_est; omega1_est; phi1_est; ampl2_est; alpha2_est; omega2_est; phi2_est];
    H0=eye(8);
    newton_opts.abstol = 1e-12;
    newton_opts.verbosity = 1;
    [p,flag,iter] = minfind_quasi_newton(func, p0, H0, newton_opts)
    ampl1 = p(1);
    alpha1 = p(2);
    omega1 = p(3);
    phi1 = p(4);
    
    ampl2= p(5);
    alpha2 = p(6);
    omega2 = p(7);
    phi2 = p(8);

Alpha_est=ampl1* exp(-alpha1 * Time).*sin(omega1 * Time + phi1)+ampl2* exp(-alpha2 * Time).*sin(omega2 * Time+ phi2);
%%
 %func = @(p)(optimfunc_dupl(Time, Alpha, p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8)));
    %p0=[ampl1_est; alpha1_est; omega1_est; phi1_est; ampl2_est; alpha2_est; omega2_est; phi2_est];
    func = @(p)(optimfunc_dupl1(Time, Alpha, p(1), alpha_est, omega_est, p(2), p(3), p(4), p(5), p(6)));
    p0=[ampl1_est; phi1_est; ampl2_est; alpha2_est; omega2_est; phi2_est];
    H0=eye(6);
    newton_opts.abstol = 1e-12;
    newton_opts.verbosity = 1;
    [p,flag,iter] = minfind_quasi_newton(func, p0, H0, newton_opts)
    
    ampl1 = p(1);
    phi1 = p(2);
    
    ampl2= p(3);
    alpha2 = p(4);
    omega2 = p(5);
    phi2 = p(6);
    
    Alpha_est=ampl1* exp(-alpha_est * Time).*sin(omega_est * Time + phi1)+ampl2* exp(-alpha2 * Time).*sin(omega2 * Time+ phi2);
%%
n = 1000;

plot(x, y)
hold all
plot(x, y_est+0.001);
plot(x, y-y_est-0.001);
legend('y', 'y_est', 'dy');
hold off;


norm(y-y_est)

