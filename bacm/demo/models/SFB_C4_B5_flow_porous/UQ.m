load('Sim_result_L_shaped_center')
load('/home/noefried/sglib-testing-bacm/demo/models/SFB_C4_B5_flow_porous/Exp_data.mat')
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',  'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',  'rms reynolds stress (r_{12})'};

[p_alpha, V_p, varserr]=  gpc_expand_RVs(RVs);
V_u=gpcbasis_create(V_p{1}, 'p',4, 'full_tensor', false);

[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', 'smolyak');
Q=length(w);
M=gpcbasis_size(V_u, 1);
A=gpcbasis_evaluate(V_u, x_ref);

u_ij=reshape(Sim_results_L_shaped_center, 360*5, []);
u_i_alpha = zeros(360*5, M);
for j = 1:Q
    display(strvarexpand('$j$/$Q$'));
    a_j = x_1(:, j);
    u_i_j=u_ij(:,j);
    x_j = x_ref(:, j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
    u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
end

[u_mean_proj, u_var_proj]=gpc_moments(u_i_alpha, V_u);
u_mean_proj=reshape(u_mean_proj,360,5);
u_var_proj=reshape(u_var_proj, 360,5);

plot_u_mean_var(y_coord, u_mean_proj, u_var_proj, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels',list_of_response, 'subplot_dim', [5,1])

for i=1:5
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
end