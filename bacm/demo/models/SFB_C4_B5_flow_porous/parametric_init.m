function [state,polysys] = parametric_init( varargin )
%
%
%
%===================================================================
% Stochastic prey-predator equation - description
%===================================================================

% Prey predator ODE:
% P1` = alpha*P1-beta*P1*P2;
% P2` = gamma*P1*P2-sigma*P2;
%
% 
% Internal state: with
% - P1:number of prey
% - P2:number of predator
% - alpha:constant birth rate of birth rate of prey
% - beta*P2:mortality of prey depending on the number of predators
% - gamma*P1:birthrate of predators depending on the number of prey eaten
% - sigma:mortality rate of predators
%
% Example:
%    state = prey_predator_init('list_of_RVs',{'P1','P2','alpha'}, 'P1',{0.5, 0.1, 'G'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)


options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'beta', 'beta_T', 'c_t', 'c_epsilon_h', 'c_wd', 'c_d_p'});
[params.beta, options] = get_option(options, 'beta', {-10, 0, 'P'});
[params.beta_T, options] = get_option(options, 'beta_T', {-1, 1, 'P'});
[params.c_t, options] = get_option(options, 'c_t', {0, 0.2, 'P'});
[params.c_epsilon_h, options] = get_option(options, 'c_epsilon_h', {0, 0.3, 'P'});
[params.c_wd, options] = get_option(options, 'c_wd', {0, 30, 'P'});
[params.c_d_p, options] = get_option(options, 'c_d_p', {0, 0.4, 'P'});
check_unsupported_options(options, mfilename);



%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();

list_of_params={'beta', 'beta_T', 'c_t', 'c_epsilon_h', 'c_wd', 'c_d_p'};
num_tot_params=length(list_of_params);


%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs); 


state.list_of_params=list_of_params;

end

