 clear all

%% I.a Define Random Variables
%params for forward calculation
beta=SimParameter('beta',UniformDistribution(-10,-6.5),'plot_name', '\beta');
beta_t=SimParameter('beta_T',UniformDistribution(-1,0.1),'plot_name', '\beta_T');
c_t=SimParameter('c_t',UniformDistribution(0,0.2)); 
c_eh=SimParameter('c_eh',UniformDistribution(0,0.3),'plot_name', 'c_{\epsilon^h}');
c_wd=SimParameter('c_wd',UniformDistribution(2,30),'plot_name',  'c_{wd}');
c_dp=SimParameter('c_dp',UniformDistribution(0,0.395),'plot_name',  'c_{d,p}');

RVs=SimParamSet1(beta, beta_t, c_t, c_eh, c_wd, c_dp);
clear('beta', 'beta_t', 'c_t', 'c_eh', 'c_wd', 'c_dp')
n=RVs.num_params;

%params for prior
prior_dist=cell(n,1);
prior_dist{1}=fix_bounds(BetaDistribution(3,3), -10, 0);
prior_dist{2}=fix_bounds(BetaDistribution(3,1.6), -1, 1);
prior_dist{3}=fix_bounds(BetaDistribution(3,2.45), 0, 0.2);
prior_dist{4}=fix_bounds(BetaDistribution(1,3), 0, 0.3);
prior_dist{5}=fix_bounds(BetaDistribution(0.35,3), 2, 30);
prior_dist{6}=fix_bounds(BetaDistribution(3,2.8), 0,0.395);

prior_params=dists2simparamset(prior_dist, 'param_names',RVs.param_names);



%% I.b Define output
 %Load experimental solution data
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',  'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',  'rms reynolds stress (r_{12})'};
n_out=length(list_of_response);
load('/home/noefried/sglib-testing-bacm/demo/models/SFB_C4_B5_flow_porous/Exp_data.mat')

%% II.1.a Generate integration points for the  middle region of
% the L-shaped domain

%set gpc degree and integration order
p_gpc=4;  %degre of gpc
gpc_full_tensor=false; % complete of full tensor gpc basis
p_int=p_gpc+1; %number of  points for the univariate integration rule
grid='smolyak'; %'full_tensor' or 'smolyak' integration points

%gPCE of the parameters
[p_alpha, V_p, varserr]=  gpc_expand_RVs(RVs);
% generate integration points for the gPC germs (x_ref) 
[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', grid);
% map integration points to the parameter set
x_p1=RVs.gpc_evaluate( p_alpha, V_p, x_ref);
%% II.1.b. Project solution - gPCE in the  middle region of
% the L-shaped domain

%Load solution at integration points
file_name='Sim_result_L_shaped_center.mat'; %file_name where the simulation resutls at the integration points are stored
load(file_name)
u_ij=reshape(Sim_results_L_shaped_center, 360*5, []);

% Define approximating subspace
V_u=gpcbasis_create(V_p{1}, 'p',p_gpc, 'full_tensor', gpc_full_tensor);

%Project to approximating subspace
[u_i_alpha, V_u]=project_solution(u_ij, V_u, x_ref, w, 'method', 'projection');

%Calculate and plot solution moments
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);
u_mean=reshape(u_mean,[],n_out);
u_var=reshape(u_var, [],n_out);

%% Sensitivity analysis, response surface at the snapshots
[part_vars, I_un, ratios, ratio_per_order]=gpc_sobol_partial_vars(V_u, u_i_alpha,  'max_index', 1);
ratios=reshape(ratios, n,[],n_out);
part_vars=reshape(part_vars, n,[],n_out);

[maxval, maxind] = max(part_vars(:));
[maxxidx, maxyidx, maxzidx] = ind2sub(size(part_vars),maxind); 



%% choose snapshots, plot mean and var, sensitivites, response surface
%y_ind=[100:50:360]; %Ez nem mukodik
%y_ind=[100,150,200]; %Ez mukodik, de eleg fura
y_ind=118;
%y_ind=[220]; %ez is mukodik, de csak kulon
A=repmat(y_ind',1,n_out)+ repmat(((0:(n_out-1))*360),length(y_ind),1);
ind=reshape(A, [], 1);

%Plot mean and variance
plot_u_mean_var(y_coord, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels',list_of_response, 'subplot_dim', [5,1])
%Plot experimental data on the same figure
for i=1:n_out
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    plot([y_coord(y_ind), y_coord(y_ind)],ylim)
    %plot(y_coord(y_ind), u(i),'X')
    xlabel('y', 'FontSize', 12)
end
% Plot partial variances in new figure
figure
for i=1:n_out
    subplot(5,1,i)
    hold on
    plot(y_coord, part_vars(:,:,i), 'LineWidth', 2)
    if i==1
        legend(RVs.param_plot_names)
    end
    plot([y_coord(y_ind), y_coord(y_ind)],ylim)
    xlabel('y', 'FontSize', 12)
    ylabel(list_of_response{i})
end

% Plot response surface
RV_map=RVs.germ2RVs_func;
plot_multi_response_surface(u_i_alpha(ind(maxzidx),:), V_u, 'name_of_RVs', RVs.param_plot_names, 'germ2RV', RV_map, 'plot_fix_response', Exp_data_yi_j(y_ind,maxzidx))

%% UPDATE
% Measurement
Z=(Exp_data_yi_j(y_ind,:))';
% variance of the masurement errors
err_vars=(u_var(y_ind, :)/10)';
% prior distribution (with prior dist of the UQ)
prior_dist_xi=RVs.germ_dist; %or

%prior with beta distributions centered around gien mean values



% with Gaussian prior
observ_operator=@(xi) gpc_evaluate(u_i_alpha(ind, :), V_u,xi);
% 
%
%update with MCMC
N=5000;
samples_post=gpc_MCMC(N, observ_operator, prior_dist_xi, Z(:), err_vars(:));
gplotmatrix(samples_post', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);
%Update with Ensemble Kalman filter


%Update with MMSE

%% Generate integration points for the second (rigtht-bottom region of the L-shaped domain)
RVs.set_dist('beta' , {UniformDistribution(-6.5, 0)})
%[x_2,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');


%gPCE of the parameters
[p_alpha_b, V_p, varserr]=  gpc_expand_RVs(RVs);
% generate integration points for the gPC germs (x_ref) 
[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', grid);
% map integration points to the parameter set
x_p2=RVs.gpc_evaluate( p_alpha_b, V_p, x_ref);




%% Project solution - gPCE in the  bottom right region of
% the L-shaped domain

%Load solution at integration points
file_name='Sim_result_L_shaped_bottom.mat'; %file_name where the simulation resutls at the integration points are stored
load(file_name)
u_ij_b=reshape(Data_i, 360*n_out, []);

%Project to approximating subspace
[u_i_alpha_b, V_u]=project_solution(u_ij_b, V_u, x_ref, w, 'method', 'projection');

%Calculate and plot solution moments
[u_mean_b, u_var_b]=gpc_moments(u_i_alpha_b, V_u);
u_mean_b=reshape(u_mean_b,[],n_out);
u_var_b=reshape(u_var_b, [],n_out);

plot_u_mean_var(y_coord, u_mean_b, u_var_b, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels',list_of_response, 'subplot_dim', [5,1])

%% Sensitivity analysis, response surface at the snapshots
[part_vars_b, I_un, ratios_b, ratio_per_order_b]=gpc_sobol_partial_vars(V_u, u_i_alpha_b,  'max_index', 1);
ratios_b=reshape(ratios_b, n,[],n_out);
part_vars_b=reshape(part_vars_b, n,[],n_out);
%figure
for i=1:n_out
    subplot(5,1,i)
    hold on
    plot(y_coord, part_vars_b(:,:,i), 'LineWidth', 2)
    if i==1
        legend(RVs.param_plot_names)
    end
    xlabel('y', 'FontSize', 12)
    ylabel(list_of_response{i})
end
%% Update

%update with MCMC
samples_post_b=gpc_MCMC(u_i_alpha_b(ind, :), V_u, Z(:), err_vars(:));
gplotmatrix(samples_post')
%% Third (left-upper region of the L-shaped domain)
RVs.set_dist({'beta', 'beta_T' }, {UniformDistribution(-10,-6.5), UniformDistribution(0.1, 1)})
[x_3,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');


%% Plot the points
gplotmatrix([x_1,x_2, x_3]', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);
set(gcf,'NextPlot','add');
axes;
h = title('Smolyak integration points used for pseudo-spectral and for colocation methods', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%% Save points
save('int_points_1.mat', 'x_p1');
save('int_points_2.mat', 'x_p2');
save('int_points_3.mat', 'x_p3');
