function samples_post=gpc_MCMC(N, observ_func, prior_dist, Z, err_vars, varargin)

options=varargin2options(varargin);
[prop_dist, options]=get_option(options, 'prop_dist', {});
check_unsupported_options(options, mfilename);

%% Prior distribution
prior_params=dists2simparamset(prior_dist);
prior_vars=prior_params.var_vals;
%% get proposal distributions
if isempty(prop_dist)
    prop_params=generate_stdrn_simparamset(sqrt(prior_vars/10));
else
    prop_params=dists2simparamset(prop_dist);
end

%% measurement error
error_params=generate_stdrn_simparamset(sqrt(err_vars));

%% MCMC
samples_post=mh_sample(N, prior_params, prop_params, observ_func, error_params, Z);
%X=mh_sample_parallel(N, dist, prop_dist);

end
%% Metropolis-Hasting
function X=mh_sample(N, prior_params, prop_params, observ_func, error_params, Z)

% MH_SAMPLE Basic version of the Metropolis-Hastings sampler
N_burn = 1000;
% initial parameter point
x=prior_params.mean_vals;
p=get_likelyhood(observ_func, x, error_params, Z)*prior_params.pdf(x);


X=[];
for i=1:N+N_burn
    xn=x+prop_params.sample(1)';
    pn=get_likelyhood(observ_func, xn, error_params, Z)*prior_params.pdf(xn);
    a=pn/p;
    a=a*prop_params.pdf(x-xn)/prop_params.pdf(xn-x);
    if a>=1 || rand<a
        x=xn;
    end
    if i>N_burn
        display(strvarexpand('$i-N_burn$/$N$'));
        X=[X x];
    else
        display(strvarexpand('Burning in $i$/$N_burn$'));
    end
end
    function prob_l=get_likelyhood(observ_func, x, error_params, Z)
        u=feval(observ_func, x);
        prob_l=error_params.pdf(u-Z);
    end
end