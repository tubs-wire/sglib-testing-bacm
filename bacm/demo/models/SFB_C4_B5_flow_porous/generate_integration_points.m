  %% Define Random Variables
clear all
beta=SimParameter('beta',UniformDistribution(-10,-6.5), '\beta');
beta_t=SimParameter('beta_T',UniformDistribution(-1,0.1), '\beta_T');
c_t=SimParameter('c_t',UniformDistribution(0,0.2)); 
c_eh=SimParameter('c_eh',UniformDistribution(0,0.3), 'c_{\epsilon^h}');
c_wd=SimParameter('c_wd',UniformDistribution(2,30),  'c_{wd}');
c_dp=SimParameter('c_dp',UniformDistribution(0,0.395),  'c_{d,p}');

RVs=SimParamSet1(beta, beta_t, c_t, c_eh, c_wd, c_dp);

%% Generate integration points for the three local subregions
% First (middle region of the L-shaped domain)
[x_1,w, x_ref]=RVs.generate_integration_points( 5, 'grid', 'smolyak');

% Second (rigtht-bottom region of the L-shaped domain)
RVs.set_dist('beta' , {UniformDistribution(-6.5, 0)})
[x_2,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');

% Third (left-upper region of the L-shaped domain)
RVs.set_dist({'beta', 'beta_T' }, {UniformDistribution(-10,-6.5), UniformDistribution(0.1, 1)})
[x_3,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');

%% Plot the points
gplotmatrix([x_1,x_2, x_3]', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);
set(gcf,'NextPlot','add');
axes;
h = title('Smolyak integration points used for pseudo-spectral and for colocation methods', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%% Save points
save('int_points_1.mat', 'x_1');
save('int_points_2.mat', 'x_2');
save('int_points_3.mat', 'x_3');
