load('/home/noefried/sglib-testing-bacm/demo/models/SFB_C4_B5_flow_porous/sample_points.mat')
list_of_params={'\beta', '\beta_T', 'c_t', 'c_{\epsilon h}', 'c_{wd}', 'c_{dp}'};

conv_points=sample_points(:,(11:11:4092));
gplotmatrix(conv_points', [] ,[] ,'red')
sample_points(5,:)=3.5*ones(1,4096);
conv_points(5,:)=3.5*ones(1,372);

figure

for i=1:3
subplot (3,1,i)
scatter3(conv_points(4,:), conv_points(6,:), conv_points(i,:))
xlabel(list_of_params{4})
ylabel(list_of_params{6})
zlabel(list_of_params{i})
end



c=repmat('r',4096,1);
c(11:11:4092)='b';
for i=1:3
subplot (3,1,i)
scatter3(sample_points(4,:), sample_points(6,:), sample_points(i,:), 5,c)
end