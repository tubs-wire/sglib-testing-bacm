function varargout = prey_predator_standardsolve(state,sample,varargin)
% PREY_PREDATOR_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ (state.list_of_params{i}) '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 

t_span=state.t_span;


%Solve ODE with fixed timestaps:
[Time,Popul]=ode45(@(t,Popul) prey_predator_ODE(state.t_span,Popul,state.actual_params),t_span,[P1,P2]);
if nargout>1
    varargout{1}=Time;
    varargout{2}={[Popul(:,1);Popul(:,2)]};
else
     varargout= {[Popul(:,1);Popul(:,2)]};
end
