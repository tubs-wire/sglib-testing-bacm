function Pprime = prey_predator_ODE(t, P, var)


%P1` = alpha*P1-beta*P1*P2;
%P2` = gamma*P1*P2-sigma*P2;
%
% Gives thd ODE of the prey-predator model
%
% with
% - P1:number of prey
% - P2:number of predator
% - alpha:constant birth rate of birth rate of prey
% - beta*P2:mortality of prey depending on the number of predators
% - gamma*P1:birthrate of predators depending on the number of prey eaten
% - sigma:mortality rate of predators

% Input:alpha, beta, gamma, sigma
% Output: the ODE (Pprime)



Pprime =[ var.alpha*P(1)-var.beta*P(1)*P(2); var.gamma*P(1)*P(2)-var.sigma*P(2)];

end
