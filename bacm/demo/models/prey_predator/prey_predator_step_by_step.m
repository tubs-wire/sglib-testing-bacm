function deltaPopul = prey_predator_step_by_step(state,sample,varargin)
% PREY_PREDATOR_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%if 'step_info' is not given time integration for each element of state.t_span will be
%calculated and deltaPopul is the value of Population for each timevalue in
%t_span
%if 'step_info' is given, e.g. step_info={P0, n_th_timestep} the nth step is calculated and deltaPopul gives the
%difference betweeen Popul in the beginning and the end of the timestep

options = varargin2options(varargin);
[step_info, options] = get_option(options, 'step_info', []);
check_unsupported_options(options, mfilename);


%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ (state.list_of_params{i}) '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 


if isempty(step_info)
    %Solve ODE with fixed timesteps for whole time interval:
      deltaPopul=ode4(@(t,Popul) prey_predator_ODE(t_span,Popul,state.actual_params),state.t_span,[P1,P2]);
      deltaPopul= {[deltaPopul(:,1);deltaPopul(:,2)]};
else
    %Solve only one step for the ODE with fixed timesteps:
    if ~(size(step_info,2) == 2)
        if size(step_info,2) == 1 && step_info{1} == 0
            n=step_info;
            Popul_0=[P1,P2];
        else
            error('step function needs twoo elements in cell form {initial_state, nth_timestep}');
        end
    else
        Popul_0=step_info{1};
        n=step_info{2};
    end
    % first step of calculation n=0 gives the initial condition
    if n==0
        %calculate the first, initial value
        deltaPopul=Popul_0;
    else
        t_span=[state.t_span(n), state.t_span(n+1)];
        Popul=ode4(@(t,Popul)  prey_predator_ODE(t_span,Popul,state.actual_params),t_span,Popul_0);
        deltaPopul=(Popul(2,:)-Popul(1,:))';
    end
    
end


     %deltaPopul= {[Popul(:,1);Popul(:,2)]};
end



