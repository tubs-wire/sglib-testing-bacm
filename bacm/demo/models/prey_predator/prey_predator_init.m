function [state,polysys,time] = prey_predator_init( varargin )
% PREY_PREDATOR_INIT Initialises the structure that keeps the internal
% state of the prey-predator equation example
%
%
%===================================================================
% Stochastic prey-predator equation - description
%===================================================================

% Prey predator ODE:
% P1` = alpha*P1-beta*P1*P2;
% P2` = gamma*P1*P2-sigma*P2;
%
% 
% Internal state: with
% - P1:number of prey
% - P2:number of predator
% - alpha:constant birth rate of birth rate of prey
% - beta*P2:mortality of prey depending on the number of predators
% - gamma*P1:birthrate of predators depending on the number of prey eaten
% - sigma:mortality rate of predators
%
% Example:
%    state = prey_predator_init('list_of_RVs',{'P1','P2','alpha'}, 'P1',{0.5, 0.1, 'G'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)


options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'P1', 'P2'});
[params.P1, options] = get_option(options, 'P1', {19, 20, 'P'});
[params.P2, options] = get_option(options, 'P2', {20, 21, 'P'});
[params.alpha, options] = get_option(options, 'alpha', {1, 0.1, 'H'});
[params.beta, options] = get_option(options, 'beta', {0.01, 0.1, 'H'});
[params.gamma, options] = get_option(options, 'gamma', {0.02, 0.1, 'H'});
[params.sigma, options] = get_option(options, 'sigma', {1, 0.1, 'H'});
[t_min_max, options] = get_option(options, 't_min_max', [0,20]);
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);

% If beneath commented t_span (the timesteps for evaluation/time
% integration are calculated from ODE45 automatic timestep, otherwise
%numric reference solution/or only the t_span there should be taken out)
%problem_size=50;
%t_span=linspace(tmin,tmax,problem_size);


%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();

list_of_params={'P1','P2','alpha', 'beta', 'gamma', 'sigma'};
list_of_init_vals={'P1','P2'};
num_tot_params=length(list_of_params);


%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs); 

for i=1:length(list_of_params)
   eval([ list_of_params{i} '=' 'state.ref_params.(list_of_params{i});' ]);
end 


%Run reference(deterministic) solution and get tspan vector for stepbystep
%integration

options=odeset('Refine', 1, 'MaxStep', (tmax-tmin)/500);
%[t_span,Popul_ref]=ode45(@(t,Popul) prey_predator_ODE(t,Popul,alpha, beta, gamma, sigma),[tmin tmax],[P1,P2], options);
[t_span,Popul_ref]=ode45(@(t,Popul) prey_predator_ODE(t,Popul,state.ref_params),[tmin tmax],[P1,P2], options);
problem_size=length(t_span);
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars=list_of_init_vals;
state.ref_sol=Popul_ref;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=size(Popul_ref,2);
time=t_span;

end

