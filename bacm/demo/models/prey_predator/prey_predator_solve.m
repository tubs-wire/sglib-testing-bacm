function varargout = prey_predator_solve(state,sample,varargin)
% PREY_PREDATOR_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%update random variables from samples to state.actual_params
for i=1:length(state.list_of_RVs)
    var_name=char(state.list_of_RVs(i));
    state.actual_params.(var_name)=sample(i);
end

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ state.list_of_params{i} '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 
    
    
t_min=state.actual_params.alpha;
t_max=state.t_span;
Popul_0=[20, 20];

%Solve ODE with fixed timestaps:
[Time,Popul]=ode45(@(t,Popul) prey_predator_ODE(t,Popul, alpha, beta, gamma, sigma),[t_min,t_max],Popul_0);

if nargout>1
    varargout(1)=Time;
    varargout(2)=Popul;
else
     varargout= Popul;
end
