function varargout = spring_simple_solve(state,sample,varargin)
% SPRING_SIMPLE_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ (state.list_of_params{i}) '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 

t_span=state.t_span;


%Solve ODE with fixed timestaps:

[Time,stoch_sol]=ode45(@(t,stoch_sol) spring_simple_ODE(t,stoch_sol,state.actual_params),t_span,[x0,v0]);
accel=[0; diff(stoch_sol(:,2))./diff(t_span)];
stoch_sol(:,3)=accel;
if nargout>1
    varargout{1}=Time;
    varargout{2}={reshape(stoch_sol, state.num_eqs*state.num_vars,1)};
else
     varargout= {reshape(stoch_sol, state.num_eqs*state.num_vars,1)};
end
