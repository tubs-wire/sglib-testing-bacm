E = SimParamSet();
E.add(SimParameter('e', NormalDistribution(0, 0.1)));

u_m = 0.3;
q2likelihood =@(q)E.pdf(u_m-gpc_evaluate(u_beta(1, :), V_u, Q.params2germ(q)));
N = 5000;
q_p = bayes_mcmc(q2likelihood, Q, N, [], [], 'N_burn', 0, 'plot', true);

N = 1000000;
T= 100;
N_b = ceil(N/T);

q_p2=bayes_mcmc(q2likelihood, Q, N, [], Q.sample(N_b), 'T', T, 'T_burn', 100, 'plot', true, 'parallel', true);


