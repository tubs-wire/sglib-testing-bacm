function [state,polysys,time] = spring_simple_init( varargin )
% PREY_PREDATOR_INIT Initialises the structure that keeps the internal
% state of the prey-predator equation example
%
%
%===================================================================
% Stochastic spring with mass equation - description
%===================================================================

% ODE:
% mx'' + kx =0

% x(t)` = v(t);
% v(t)` = -k/mx(t);
%
% 
% Internal state: with
% - x(t):desplacement of mass
% - v(t):speed of the mass
% - k:spring moduli
% - m:mass
%
% Example:
%    state = spring_simple_init('list_of_RVs',{'x0','v0','k', 'm'}, 'x0',{0.5, 0.1, 'G'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)


options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'X0', 'v0'});
[params.x0, options] = get_option(options, 'x0', {1, 1, 'P'});
[params.v0, options] = get_option(options, 'v0', {0, 0, 'P'});
[params.a0, options] = get_option(options, 'a0', {0, 0, 'P'});
[params.m, options] = get_option(options, 'm', {1, 0.01, 'U'});
[params.k, options] = get_option(options, 'k', {1, 0.01, 'U'});
[t_min_max, options] = get_option(options, 't_min_max', [0,20]);
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);

% If beneath commented t_span (the timesteps for evaluation/time
% integration are calculated from ODE45 automatic timestep, otherwise
%numric reference solution/or only the t_span there should be taken out)
%problem_size=50;
%t_span=linspace(tmin,tmax,problem_size);


%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();

list_of_params={'x0','v0','a0','k', 'm'};
list_of_init_vals={'x0','v0'};
list_of_sol_vars={'x0','v0', 'a0'};
num_tot_params=length(list_of_params);

%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs); 

for i=1:length(list_of_params)
   eval([ list_of_params{i} '=' 'state.ref_params.(list_of_params{i});' ]);
end 


%Run reference(deterministic) solution and get tspan vector for stepbystep
%integration

options=odeset('Refine', 1, 'MaxStep', (tmax-tmin)/500);
%[t_span,Popul_ref]=ode45(@(t,Popul) prey_predator_ODE(t,Popul,alpha, beta, gamma, sigma),[tmin tmax],[x0,v0], options);
[t_span,Sol_ref]=ode45(@(t,Init_state) spring_simple_ODE(t,Init_state,state.ref_params),[tmin tmax],[x0,v0], options);
problem_size=length(t_span);
accel=[0; diff(Sol_ref(:,2))./diff(t_span)];
Sol_ref(:,3)=accel;
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars=list_of_sol_vars;
state.ref_sol=Sol_ref;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=size(Sol_ref,2);
time=t_span;

end

