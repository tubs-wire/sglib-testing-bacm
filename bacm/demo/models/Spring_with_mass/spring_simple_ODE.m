function Pprime = spring_simple_ODE(t, P, var)

% solving the ODE:
% mx'' + kx =0

% x(t)` = v(t);
% v(t)` = -k/m x(t);
%
% 
%Inputs:
% - x(t):desplacement of mass
% - v(t):speed of the mass
% - k:spring moduli
% - m:mass
%
%
% Initial conditions: P(1)=x0, P(2)=v0
% Input in the variance:k, m
% Output: the ODE (Pprime: x', v')



Pprime =[ P(2); -var.k/var.m*P(1)];

end
