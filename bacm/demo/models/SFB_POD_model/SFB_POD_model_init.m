function [state,polysys,time] = SFB_POD_model_init( varargin )
% SFB_POD_model
%
%
%===================================================================
% Stochastic POD (variations of coeffs)
%===================================================================
%
% POD:
% dadt(i)=Ci(i)+ sum_j Lij(i,j)*a(j) + sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j
% Bij(i,j)*c(j)
% with
% 
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -c(j): control modes
%       .- c(1): shift between controlled and not controlled phase B0
%        - c(2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(3): + oscillatory part with phase shift (B1*sin(\omega_a t)
% POD coefficients:
%
% coeffs of the linear term:    Lij : dissipative term
% coeffs of the quadratic term: Qijk :convective term
% coeffs of the actuation:      Bij  :control term
% initial condition:            a_0  :first coeffs of the POD basis
%                                       functions (\psi=\sum_k b_kl v_k(x) - does v_0 exists?)
%
%

% Example:
%    state = SFB_POD_model_init('list_of_RVs',{'dL','dQ','da_0'}, 'L',{0.5, 0.1, 'H'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)

%
%dentity matrix for measurement - not needed here
%and identity matrix times sigma for B^-1 (for initial cond) and C^-1 (for
%coeffs)
% sigma=0.1

options = varargin2options(varargin);
%[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'dL', 'dQ', 'da_0'});
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'dL', 'dQ', 'da_0'});
[params.dL, options] = get_option(options, 'dL', {0, 0.1, 'H'});
[params.dQ, options] = get_option(options, 'dQ', {0, 0.1, 'H'});
[params.da_0, options] = get_option(options, 'da_0', {0, 0.1, 'H'});
%[t_min_max, options] = get_option(options, 't_min_max', [0,90]);
[t_min_max, options] = get_option(options, 't_min_max', [0,90]);
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);

% If beneath commented t_span (the timesteps for evaluation/time
% integration are calculated from ODE45 automatic timestep, otherwise
%numric reference solution/or only the t_span there should be taken out)

%problem_size=50;
%t_span=linspace(tmin,tmax,problem_size);


%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();

list_of_params={'dL','dQ','da_0'};
multi_dim.dim_params={[4,4], [4,4,4], [4,1]};

list_of_init_vals={'da_0'};
num_tot_params=length(list_of_params);
%num_tot_params=4*4+51+4;

%% this part is only if the parameters in the list are not scalars
% Calculate the number of independent random variables (take only
% nonsymmetrical terms)
indQ(:,:,1)=[1,2,3,4;5,6,7,8;9,10,11,12;13,14,15,16];
indQ(:,:,2)=[2,17,18,19;6,20,21,22;10,23,24,25;14,26,27,28];
indQ(:,:,3)=[29,30,31,32;7,33,34,35;11,24,36,37;38,39,40,41];
indQ(:,:,4)=[42,43,44,45;8,46,47,48;12,25,49,50;14,28,41,51];

indL=(reshape(1:prod(multi_dim.dim_params{1}), multi_dim.dim_params{1}) )';
inda=reshape(1:prod(multi_dim.dim_params{3}), multi_dim.dim_params{3});
multi_dim.index_of_ind_params={indL,indQ,inda}; %index of independent parameters (this gives the order how the samples should fill in)
multi_dim.size_independent_params={max(indL(:)),max(indQ(:)),max(inda(:))};

%%

%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs, 'multi_dim', multi_dim); 


%% Run reference(deterministic) solution and get tspan vector for stepbystep
%integration
tic
[ref_POD_modes,t_span] = SFB_POD_ODE(tmax,tmin, state.ref_params);
toc
problem_size=length(t_span);
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars=list_of_init_vals;
state.ref_sol=ref_POD_modes;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=size(ref_POD_modes,2);
time=t_span;

end

