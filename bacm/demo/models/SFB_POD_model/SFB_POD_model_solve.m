function varargout = SFB_POD_model_solve(state,sample,varargin)
% PREY_PREDATOR_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
% for i=1:length(state.list_of_params)
%    eval([ (state.list_of_params{i}) '=' 'state.actual_params.(state.list_of_params{i});' ]);
% end 

t_span=state.t_span;


%Solve ODE with fixed timesteps:
[POD_modes,Time] = SFB_POD_ODE([],[], state.actual_params, 't_span', t_span);
modes_out=reshape(POD_modes,[],1);
if nargout>1
    varargout{1}=Time;
    varargout{2}={modes_out};
else
     
     varargout= {modes_out};
end
