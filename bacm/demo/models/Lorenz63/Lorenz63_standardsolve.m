function varargout = Lorenz63_standardsolve(state,sample,varargin)
% PREY_PREDATOR_SOLVE Short description 
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ (state.list_of_params{i}) '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 

t_span=state.t_span;


%Solve ODE with fixed timestaps:
[Time,State_atm]=ode45(@(t,State_atm) Lorenz63_ODE(t,State_atm,state.actual_params),t_span,[x0,y0,z0]);

if nargout>1
    varargout{1}=Time;
    varargout{2}={[State_atm(:,1);State_atm(:,2);State_atm(:,3)]};
else
     varargout= {[State_atm(:,1);State_atm(:,2);State_atm(:,3)]};
end
