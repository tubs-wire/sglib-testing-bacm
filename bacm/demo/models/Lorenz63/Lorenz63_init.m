function [state,polysys,time] = Lorenz63_init( varargin )
% LORENZ63_INIT Initialises the structure that keeps the internal
% state of the Lorenz63 equation example
%
%Physycal meaning of the system:
%Two dimensional fluid cell is warmed from below and cooled from above and
%the resulting convective motion is modelled by this PDE
%
%
%===================================================================
% StochasticLorenz63 equation - description
%===================================================================

%  Simplified nonlinear model for atmospheric convection
%  dx/dt= s(y − x)
%  dy/dt= rx − y − xz
%  dz/dt= xy − bz

%In hisworkLorenz showed
%such a system to exhibit, for a large set of parameter values, a peculiar chaotic behavior,
%that is exponentially sensitive to perturbations of initial conditions and the existence of a
%global attracting set for the flow nowadays called the generalized nontrivial hyperbolic
%attractor.


% initial values and parameters
% .................................

%  Lorenz used the values: s=10, b=8/3 and r=28 - chaotic behavior

%  if r<1: only one equilibrium point, which is at the origin (no
%  convections), all orbits converge to the origin
%  saddle-node bifurcation occurs at r=1
%  and for r>1 two additional critical points at:
% (+- sqrt(b(r-1)), +- sqrt(b(r-1)), (r-1) -> steady convection, or Hopf
% bifurcation
% this point is only stable if:
% r<s(s+b+3)/(s-b-1)

% where
% x: the rate of convective overturning
% y: horizontal temperature variation
% z: vertical temperature variation
% With parameters:
% s:nu/kappa - > Prandtl number
% r: Ra/Ra_c -> Ra: Raynolds number, Ra_c: critical Raynolds number
% b: 4/(1+a^2):
% 

%
% Example:
%    state = Lorenz63_init('list_of_RVs',{'x0','y0','z0'}, 'x0',{3, 1, 'H'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)


options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'x0', 'y0', 'z0'});
[params.x0, options] = get_option(options, 'x0', {3, 0.1, 'H'});
[params.y0, options] = get_option(options, 'y0', {-3, 0.1, 'H'});
[params.z0, options] = get_option(options, 'z0', {20, 0.1, 'H'});
[params.s, options] = get_option(options, 's', {10, 2, 'H'});
[params.r, options] = get_option(options, 'r', {28, 4, 'H'});
[params.b, options] = get_option(options, 'b', {8/3, 0.5, 'H'});
[t_min_max, options] = get_option(options, 't_min_max', [0,50]);
[n_timesteps, options] =get_option(options, 'n_timesteps', 3000); 
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);

% If beneath commented t_span (the timesteps for evaluation/time
% integration are calculated from ODE45 automatic timestep, otherwise
%numric reference solution/or only the t_span there should be taken out)
%problem_size=50;
%t_span=linspace(tmin,tmax,problem_size);


%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();

list_of_params={'x0','y0','z0', 's', 'r', 'b'};
list_of_init_vals={'x0','y0', 'z0'};
num_tot_params=length(list_of_params);


%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs); 

for i=1:length(list_of_params)
   eval([ list_of_params{i} '=' 'state.ref_params.(list_of_params{i});' ]);
end 


%Run reference(deterministic) solution and get tspan vector for stepbystep
%integration

options=odeset('Refine', 1, 'MaxStep', (tmax-tmin)/n_timesteps);

[t_span,State_ref]=ode45(@(t,State_ref) Lorenz63_ODE(t,State_ref,state.ref_params),[tmin tmax],[x0,y0,z0], options);
%State_ref=ode4(@(t,State_ref) Lorenz63_ODE(t,State_ref,state.ref_params),t_span,[x0,y0,z0]);
problem_size=length(t_span);
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars=list_of_init_vals;
state.ref_sol=State_ref;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=size(State_ref,2);
time=t_span;

end

