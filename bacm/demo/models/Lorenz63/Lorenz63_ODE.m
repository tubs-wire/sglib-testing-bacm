function xprime = Lorenz63_ODE(t, x, var)

%  dx/dt= s(y − x)
%  dy/dt= rx − y − xz
%  dz/dt= xy − bz




xprime =[var.s*(x(2)-x(1)); var.r*x(1)-x(2)-x(1)*x(3); x(1)*x(2)-var.b*x(3)];


end
