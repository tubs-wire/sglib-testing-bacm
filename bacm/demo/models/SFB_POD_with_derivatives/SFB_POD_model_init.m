function [state,polysys,time] = SFB_POD_model_init( varargin )
% SFB_POD_model
%
%
%===================================================================
% Stochastic POD (variations of coeffs)
%===================================================================
%
% POD:
% dadt(i)=sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j Mij(i,j)*dc(j)dt
% with
% 
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -a(j): control modes and POD modes
%       .- c(1): shift between controlled and not controlled phase B0
%        - c(2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(3): + oscillatory part with phase shift (B1*sin(\omega_a t)
% POD coefficients:
%

% Example:
%    state = SFB_POD_model_init('list_of_RVs',{'Q','a_0'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)

%
%dentity matrix for measurement - not needed here
%and identity matrix times sigma for B^-1 (for initial cond) and C^-1 (for
%coeffs)
% sigma=0.1

options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'Q', 'BQ', 'L', 'a0'});
check_unsupported_options(options, mfilename);

%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();
long_list_of_RVs={};
load('coeff_4D_Quad_3_act_with_der.mat');

%% Read  deterministic coefficients
a0=[-0.42779711595446163; -4.9748974032828318*10^(-2); -1.0883596966771526*10^(-2); -9.1617981596987027*10^(-3)];
inda0=(1:4)';
% reshape matrices in one column of independent values

C1=coeff.Q_ijk(:,1:3,1:3);
C2=coeff.Q_ijk(:,4,1:3);
C3=coeff.Q_ijk(:,5:8,1:3);

A1=coeff.Q_ijk(:,1:3, 4);
A2=coeff.Q_ijk(:,4,4);
A3=coeff.Q_ijk(:,5:8,4);

B1=coeff.Q_ijk(:,1:3,5:8);
B2=coeff.Q_ijk(:,4,5:8);
B3=coeff.Q_ijk(:,5:8,5:8);

%% Redefine coefficients
% in the form:
% dadt=\C_i + \sum_j B_ij cj + \sum_j sum_k B_ijk cj ck + \sum_j L_ij aj +
% \sum_jk Q_ijk aj ak + \sum_jk QB_ijk ck aj + \sum_j M_ij dcdtj

C_i=A2;     %\C_i (constant term)
B_ij=A1+squeeze(C2);  %\sum_j B_ij cj
B_ijk=C1;   %\sum_j sum_k B_ijk cj ck
L_ij=squeeze(B2)+A3;  %\sum_j L_ij aj
Q_ijk=B3;  %\sum_jk Q_ijk aj ak
BQ_ijk=C3+permute(B1,[1,3,2]);%\sum_jk QB_ijk ck aj
M_ij=coeff.M_ij; %\sum_j M_ij dcdtj
indQ=(1:numel(Q_ijk))';
indBQ=(1:numel(BQ_ijk))';
indL=(1:numel(L_ij))';

%% Input 
%Create parameter names, initiate random variables with given plus minus %
%perturbations and distributions (now only works for uniform distribution)

vec_name={'Q', 'BQ', 'L', 'a0'};
param_ind={indQ, indBQ, indL, inda0};
vecs={Q_ijk, BQ_ijk, L_ij, a0};
boundary_multipl={[0.995, 1.005], [0.995, 1.005], [0.998, 1.002],  [0.95, 1.05] };

[params, param_names]=initiate_rand_RVs(vec_name, param_ind, vecs, boundary_multipl);
long_list_of_RVs=[];
for i_vec=1:length(vec_name)
    if ~isempty(  strmatch(vec_name{i_vec}, list_of_RVs) )
        long_list_of_RVs= [long_list_of_RVs; param_names{i_vec}];
    end
end

%%
list_of_params=fieldnames(params);
list_of_init_vals=param_names{4};
num_tot_params=length(list_of_params);

%%
%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, long_list_of_RVs); 


%% Run reference(deterministic) solution and get tspan vector for stepbystep
%integration


state.param_C_i=C_i;
state.param_B_ij=B_ij;
state.param_B_ijk=B_ijk;
state.param_M_ij=M_ij;
state.vec_name=vec_name;
state.param_ind=param_ind;

tic
[t_span, ref_POD_modes] = SFB_POD_model_solve(state, [], 'stoch_flag', 0);
toc

problem_size=length(t_span);
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars={'a_1'; 'a_2'; 'a_3'; 'a_4' };
state.ref_sol=ref_POD_modes;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=4;
state.num_totRV_out= state.num_eqs(1)*problem_size;

time=t_span;
end

%Create names for parameters
function params_name=create_param_names(name, index)
%Check whether index is  (so that the parameter nemes will also be) unique.
if ~length(unique(index))==length(index)
    error('SGLIB: init func : indexing doesn t lead to unique parameter names')
end
ind_cell=num2cell(index);
ind_text=cellfun(@num2str, ind_cell, 'UniformOutput', false);
param_text=mat2cell(  repmat(  name, length(ind_text), 1), ones(length(ind_text),1), length(name));
params_name=cellfun(@strcat, param_text, ind_text,  'UniformOutput', false);
end

function [params, param_names]=initiate_rand_RVs(vec_name, param_ind, vecs, boundary_multipl)
n_vec=length(vec_name);
params=struct;
param_names=cell(1,3);
for i_vec=1:n_vec
    i_vec_name=strcat(vec_name{i_vec}, '_');
    i_param_names=create_param_names(i_vec_name, param_ind{i_vec});
    m_a=boundary_multipl{i_vec}(1);
    m_b=boundary_multipl{i_vec}(2);
    i_vector=reshape(vecs{i_vec}, [],1);
    % Initiate uniform independent variables with \pm 5% deviation
    for i=1: length(i_param_names)
        params.(i_param_names{i}) ={ i_vector(i)*m_a, i_vector(i)*m_b, 'P' };
    end
    param_names{i_vec}= i_param_names;
end
end
