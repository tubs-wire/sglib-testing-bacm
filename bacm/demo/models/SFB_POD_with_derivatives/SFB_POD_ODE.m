function [POD_modes,t_span] = SFB_POD_ODE(var, varargin)
% POD:
% dadt(i)=Ci(i)+ sum_j Lij(i,j)*a(j) + sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j
% Bij(i,j)*c(j)
% with
%
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -c(j): control modes
%       .- c(-1): shift between controlled and not controlled phase B0
%        - c(-2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(-3): + oscillatory part with phase shift (B1*sin(\omega_a t)
%           (for the control modes the reference modes are used, which can
%           be changed by setting the 'USE_REF_MODES' option 'FALSE' 
% POD coefficients
%
% coeffs of the linear term:    Lij : dissipative term
% coeffs of the quadratic term: Qijk :convective term
% coeffs of the actuation:      Bij  :control term
% initial condition:            a_0  :first coeffs of the POD basis
%

options = varargin2options(varargin);
[implicit_flag, options]=get_option(options, 'implicit_flag',0);
[plot_flag, options]=get_option(options, 'plot_flag',1);
check_unsupported_options(options, mfilename);
%tmin=0
%tmax=87.260999999999996; %deterministic model should run till here
%tmax=50 %this is how far the ODE solver can solve it so far
%%

%% Input of coefficients
a0=var.a0;

%% Control parameters from Richard
    Ref_mod=load('nut_AM3');
    c=Ref_mod.a(1:3,:);
    t_span=Ref_mod.time;
    a_ref=Ref_mod.a(5:8,:);
    clear 'Ref_mod';
 
    %% Control parameters from Gilles
%     Ref_mod=load('nut_AM3');
%     a_ref=Ref_mod.a(5:8,:);
%     clear 'Ref_mod';
%     load('Ref_modes_Gilles.mat');
%     c=[Ref.cmode3, Ref.cmode2, Ref.cmode1]';
%     t_span=Ref.time;
%     clear 'Ref';
    
    
    %zeroing out the last nonzero entry of the control term (so it does not
    %blows up):
    %c(646,3)=0;
    
    
%% Calculate derivatives of control modes
dt=t_span(2)-t_span(1);

dcdt=zeros(size(c));
    dcdt(:,1)= (c(:,2)-c(:,1))/dt;
    dcdt(:,2)= (c(:,3)-c(:,1))/(2*dt);
    
    ii=3:length(t_span)-2;
    dcdt(:,ii)= (c(:,ii-2)-8*c(:,ii-1)+8*c(:,ii+1)-c(:,ii+2)) / (12*dt);
    
    dcdt(:,end-1)= (c(:,end)-c(:,end-2))/(2*dt);
    dcdt(:,end)= (c(:,end)-c(:,end-1))/(dt);
    
%% Put everything in the 'VAR' structure

    var.a_ref=a_ref;
    var.time=t_span;
    var.c=c;
    var.dcdt=dcdt;
   
    %% Solve ODE

    if implicit_flag
        % with implicit solver:
        POD_modes=ode15i(@(t,POD_modes, dadt) solve_SFB_POD_impl(t, POD_modes, dadt, var), t_span, a0, dadt);
    else
        % with explicit fixed timestep solver:
        POD_modes=ode4(@(t,POD_modes) solve_SFB_POD(t,POD_modes,var), t_span, a0);
        % with explicit adaptive  timestep solver:
        %[t_span,POD_modes]=ode45(@(t,POD_modes) solve_SFB_POD(t,POD_modes,var, use_ref_modes_flag),t_span,a0);
    end


if implicit_flag
    t_span=(POD_modes.x)';
    POD_modes=(POD_modes.y)';
end

%% Plot modes if needed
if plot_flag
    %Plot reference POD modes
    for i=1:4
        subplot(4,1,i)
        plot(t_span, a_ref(i,:), 'b')
        if i==1||i==2
            ylim([-0.6,0.6])
        elseif i==3
            ylim([-1,0.2])
        else
            ylim([-0.6,1])
        end
    end
    
    % Plot ROM modes
    for i=1:4
        subplot(4,1,i)
        hold on
        plot(t_span,POD_modes(:,i), 'r')
        if i==1||i==2
            ylim([-0.6,0.6])
        elseif i==3
            ylim([-1,0.2])
        else
            ylim([-0.6,1])
        end
    end
end
end % end of SFB_POD_ODE

%% Function for the explicit solver
function POD_modes = solve_SFB_POD(t, a, var)

    c=get_ref_control_modes(t, var.time, var.c);
    dcdt=get_ref_control_modes(t, var.time, var.dcdt);
    
    sumeq=zeros(4,7);
    sumeq(:,1)=var.C_i;
    sumeq(:,2)=var.L_ij*a;
    sumeq(:,3)=reshape(reshape(var.Q_ijk, 4*4,4)*a, 4,4)*a;
    sumeq(:,4)=var.B_ij*c;
    sumeq(:,5)=reshape(reshape(var.B_ijk, 4*3,3)*c, 4,3)*c;
    sumeq(:,6)=reshape(reshape(var.BQ_ijk, 4*4,3)*c, 4,4)*a;
    sumeq(:,7)=var.M_ij*dcdt;
    POD_modes= sum(sumeq,2);

end

%% Function for the implicit solver
function POD = solve_SFB_POD_impl(t, a, dadt, var)
%POD_modes= var.C + var.L*a + reshape(reshape(var.Q,n*n,n)*a,n,n)*a + var.B*[0.0295;0.0239*cos(0.489*t);0.0239*sin(0.489*t)];
POD=0;
POD=-dadt+ var.C + var.L*a +   [var.Q(:,:,1)*a,var.Q(:,:,2)*a,var.Q(:,:,3)*a,var.Q(:,:,4)*a]*a + var.B*get_ref_control_modes(t, var.Ref_time, var.Ref_contr_modes);
end

%% Get control modes with linear interpolation of reference control modes 
function control_modes = get_ref_control_modes(t, Ref_time, Ref_contr_modes)
    a_min1=interp1(Ref_time,Ref_contr_modes(3,:), t);
    a_min2=interp1(Ref_time,Ref_contr_modes(2,:), t);
    a_min3=interp1(Ref_time,Ref_contr_modes(1,:), t);
    control_modes=[a_min3;a_min2; a_min1];
    %control_modes
end


