function varargout = SFB_POD_model_solve(state,sample,varargin)
% Calls the SFB_POD integrator
%

 options = varargin2options(varargin);
 [stoch_flag, options]=get_option(options, 'stoch_flag',1); % flag whether coefficients should be deterministic or stochastic(perturbed)
 check_unsupported_options(options, mfilename);

%% Define variables
var=struct;
    var.C_i=state.param_C_i;
    var.B_ij=state.param_B_ij;
    var.B_ijk=state.param_B_ijk;
    var.M_ij=state.param_M_ij;
    vec_name=state.vec_name;
    param_ind=state.param_ind;

    if stoch_flag
        %scale and shift from standard deviation and move to state.actual params
        state=scale_and_update_vars(sample, state);
        vecs=compile_to_vector(state.actual_params, vec_name, param_ind);
    else
        vecs=compile_to_vector(state.ref_params, vec_name, param_ind);
    end

    var.a0=vecs.a0;
    var.L_ij=reshape(vecs.L,4,4);
    var.Q_ijk=reshape(vecs.Q, 4,4,4);
    var.BQ_ijk=reshape(vecs.BQ, 4,4,3);
%% Solve ODE    
%Solve ODE with fixed timesteps:
    [POD_modes,Time] =SFB_POD_ODE(var);

%% Output
    modes_out=reshape(POD_modes,[],1);
    if nargout>1
        varargout{1}=Time;
        varargout{2}=modes_out;
    else
        varargout= {modes_out};
    end
