function varargout = logistic_equation_standardsolve(state,sample,varargin)
% LOGISTIC_EQUATION_SOLVE Short description of logistic_equation_solve
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);


%scale and shift from standard deviation and move to actual params
state=scale_and_update_vars(sample, state);

% assign values for all parameters in the state.list_of_params from state.
% actual_params
for i=1:length(state.list_of_params)
   eval([ state.list_of_params{i} '=' 'state.actual_params.(state.list_of_params{i});' ]);
end 

%Solve ODE with fixed timesteps:
[Time,Popul]=ode45(@(t,Popul) logistic_ODE(t,Popul,r,k),state.t_span,P0);
if nargout>1
    varargout(1)=Time;
    varargout(2)=Popul;
else
    varargout= Popul;
end
