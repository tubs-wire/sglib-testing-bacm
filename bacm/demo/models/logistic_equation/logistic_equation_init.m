function [state,polysys,time] = logistic_equation_init( varargin )
% LOGISTIC_INIT Initialises the structure that keeps the internal
% state of the logistic equation example
%
% Possible inputs (can be passed through a structure, too):
% 'list_of_RVs' -  default: = {'P0', 'r', 'k'});
% 'P0' 		-  default: {0.4, 0.6, 'P'});
% 'r' 		-  default: {0.4, 0.6, 'P'});
% 'k'		-  default: {1,0.1,'H'});
% 't_min_max'	-  default: [0,15];
%
% Output:
% Polysys:              type of polynomials used for the random variables
% state.list_of_init_vals:	list of the variables that define the init. val. of the ODE	(here: only 'P0'
% state.ref_params:		mean value of the variables
% state.actual_params:	actual value of the_parameters (here nonrandom
% parameters get there mean value, the random ones are kept empty)
% state.ref_sol.exact:	determinitstic analytical solution - logistic_exact_solve(state.ref_params, t_span); 
% state.ref.sol.numeric determinitstic numerical solution with mean values  
% state.t_span:         vector of time calculated from automatic timestep of ODE45         
% state.list_of_params: list of all the parameters
% state.num_params:		number of RV
% state.num_vars:       number of elements of timesteps
% state.num_eqs=1:		number of OD Equations
% time:                 same as state.t_span;

%===================================================================
% Stochastic logistic equation - description
%===================================================================
% Internal state:
%
% - r:Malthusian parameter (rate of maximum population growth)
% - k:carrying capacity of the environment (maximum sustainable population)
% - P0:initial value of the population (BC)
% 
% - t_min_max: time interval [t_from, t_to]

% Logistic equation:
% Pprime = r*P*(1-P/k);
%
% Example:
%    state = logistic_init('list_of_RVs',{'P0','r','k'});



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the logistic equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)

% list of random parameters:


options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'P0', 'r', 'k'});
[params.P0, options] = get_option(options, 'P0', {0.4, 0.6, 'P'});
[params.r, options] = get_option(options, 'r', {0.4, 0.6, 'P'});
[params.k, options] = get_option(options, 'k', {1,0.1,'H'});
[t_min_max, options] = get_option(options, 't_min_max', [0,15]);
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);

% If beneath commented t_span (the timesteps for evaluation/time
% integration are calculated from ODE45 automatic timestep, otherwise
%numric reference solution/or only the t_span there should be taken out)
%problem_size=50;
%t_span=linspace(tmin,tmax,problem_size);


%=========================================================================
% defining internal state
%=========================================================================
% storing params and problem description in STATE 

state = struct();

list_of_params={'P0','r','k'};
list_of_init_vals={'P0'};
num_tot_params=length(list_of_params);


[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs);     

%Run reference(deterministic) solution and get tspan vector for stepbystep
%integration

options=odeset('Refine', 1, 'MaxStep', (tmax-tmin)/50);
[t_span,Popul_ref]=ode45(@(t,Popul) logistic_ODE(t,Popul,state.ref_params.r,state.ref_params.k),[tmin tmax],state.ref_params.P0, options);
problem_size=length(t_span);
%dt_span=diff(t_span);


state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars=list_of_init_vals;
state.ref_sol_exact= logistic_exact_solve(state.ref_params, t_span); 
state.ref_sol=Popul_ref;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=1;
time=t_span;


end

