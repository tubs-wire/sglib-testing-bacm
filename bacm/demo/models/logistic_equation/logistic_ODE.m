function Pprime = logistic_ODE(t,P,r,k)

%P` = r*P(1-P/k);
% Gives thd ODE of the logistic equation
% ODE of Logistic equation:
% dP/dt=rP(1-P/k) BoundaryCondition: P(t=0)=p0
% with
% - P:Population
% - r:Malthusian parameter (rate of maximum population growth)
% - k:carrying capacity of the environment (maximum sustainable population)
% - P0:initial value of the population (BC)

% Input:r,k
% Output: the ODE (pprime)

Pprime = r*P*(1-P/k);

end
