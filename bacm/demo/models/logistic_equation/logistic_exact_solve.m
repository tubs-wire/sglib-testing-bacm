function [ sol ] = logistic_exact_solve(ref_params,t, p0_step, p0_max)
% Exact analytical solution of logistic function
%   input parameters:
%   -k:Carrying capacity of the environment (maximum sustainable pupulation
%   -r:Malthusian parameter (rate of maximum population growth)
%   -p0:first initial value of the population (BC)
%   -t:time vector for plotting
%in varagin:
%   -p0_step: increment of initial population for several plots
%   -p0_max:maximum of the initial value!

%   Output:
%    -Solution matrix (sol) first index refers to the different initial
%    values of population and the second to the different timesteps

dim_t=length(t);
k=ref_params.k;
r=ref_params.r;
p0=ref_params.P0;

if nargin>2
    p0_1=p0:p0_step:p0_max;
    dim_p0=length(p0_1);
else
    dim_p0=1;
    p0_1=p0;
end
    




%preallocation for solution matrix
% sol=eml.nullcopy(zeros(dim_p0,dimt));

%jj: running index for different values of initial population (p0)
sol=zeros(dim_p0,dim_t);

for jj=1:dim_p0
%ii: running index for timesteps
for ii=1:dim_t
exact_sol_nom=k*p0_1(jj)*exp(r*t(ii));
exact_sol_den=k+(exp(r*t(ii))-1)*p0_1(jj);
sol(jj,ii)=exact_sol_nom/exact_sol_den;
end
end

end


