function [init_options, ylabels]=logistic_users_init_options()

%Initialize parameters for logistic equation
%e.g.:
% 'init.options.list_of_RVs' -  default: = {'P0', 'r', 'k'}); /The parameters in the
% list will be treated as random variables
% 't_min_max'	-  default: [0,15];
%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the logistic equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)

%e.g.:
% 'P0' 		-  default: {0.4, 0.6, 'P'});
% 'r' 		-  default: {0.4, 0.6, 'P'});
% 'k'		-  default: {1,0.1,'H'});



init_options.list_of_RVs={'P0'};
init_options.t_min_max=[0,15];
init_options.P0={0.3, 0.4, 'P'};
init_options.r={0.4, 0.6, 'P'};
ylabels={'x: Population'};
end