function plot_me(Size, line, MSize,font)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funtion Call:
% plot_me(Text Size, Line Size, Marker Size, Font Style)
% 
% Sets Defaults for the given Input Values for the whole figure
% 
% Available Font Styles:
% 1 -   Times New Roman
% 2 -   Helvetica, Arial
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch font
    case (1)
      fontname = 'times';  
    case (2)
      fontname = 'helvetica';  
    case (3)
      fontname = 'courier';  
end
set(  gcf,'DefaultAxesFontName',fontname,'DefaultAxesFontSize',Size)
set(  gcf,'DefaultLineLineWidth',line,'DefaultLineMarkerSize',MSize)
end