%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                         BACM_Trim_HF_Quat                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Script                                                                                  *
%*                                                                                                    *
%* Purpose  : Setup trim conditions for the BACM aircraft model. Call the trim routines and save the  *
%*            results in a MAT-File.                                                                  *
%*                                                                                                    *
%* Version  : 1.3                                                                                     *
%*                                                                                                    *
%* Sub-Functions : DLR_BACM_Trim.mdl, BACM_Trimming_Quat.m                                            *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax :  BACM_Trim_HF_Quat                                                                        *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date     *                     Description                       *
%******************************************************************************************************
%* Christian Raab               * 20-NOV-09 * Basic Design                                            *
%* Jobst H. Diekmann            * 16-APR-12 * SFB 880 Application (6 Flaps with BLC-System)           *
%* Jobst Henning Diekmann       * 15-APR-14 * Simulation MK I Trim Function                  *
%******************************************************************************************************
%function [BACM, BACM_Trim, TrimPointInfo] = BACM_Trim_HF_Quat_Func(UncertaintyFactor)
function [BACM, BACM_Trim, TrimPointInfo] = BACM_Trim_HF_Quat_Func(uncertain_params)

% global BACM;
% global BACM_Trim;
% global TrimSettings;

% timestamp = clock;

% Path = 'Trimpoints';

% if ~exist(Path, 'dir')
%     mkdir (  Path);
% end
TrimSettings.SimMode = 2;                           % trimming
TrimSettings.MdlName = 'DLR_SFB880_Trim_Quat';
                                                    
BACM_Simulation_Init;
% disp(['Trim flag: ', num2str(BACM.Sim.Trim_Flag)]);

% Introduce Aerodynamic Uncertainties 
%%%%%%%%%%%%%%%%%%%%first part commented, second part written by Noemi
% BACM.Aerodynamics.Lift.C_A0         = BACM.Aerodynamics.Lift.C_A0       * UncertaintyFactor.CL0;
% BACM.Aerodynamics.Lift.dC_AdFL      = BACM.Aerodynamics.Lift.dC_AdFL    * UncertaintyFactor.CL0;
% BACM.Aerodynamics.Lift.dC_A_DN      = BACM.Aerodynamics.Lift.dC_A_DN    * UncertaintyFactor.CL0;
% BACM.Aerodynamics.Lift.C_AFRa       = BACM.Aerodynamics.Lift.C_AFRa     * UncertaintyFactor.CLa;
% BACM.Aerodynamics.Lift.dC_AadFL     = BACM.Aerodynamics.Lift.dC_AadFL   * UncertaintyFactor.CLa;
% BACM.Aerodynamics.Lift.dC_Aa_DN     = BACM.Aerodynamics.Lift.dC_Aa_DN   * UncertaintyFactor.CLa;
% BACM.Aerodynamics.Drag.C_W0         = BACM.Aerodynamics.Drag.C_W0       * UncertaintyFactor.CD0;
% BACM.Aerodynamics.Drag.dC_WdFL      = BACM.Aerodynamics.Drag.dC_WdFL    * UncertaintyFactor.CD0;
% BACM.Aerodynamics.Drag.dC_W0_DN     = BACM.Aerodynamics.Drag.dC_W0_DN   * UncertaintyFactor.CD0;
% BACM.Aerodynamics.Drag.k1           = BACM.Aerodynamics.Drag.k1         * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Drag.k2           = BACM.Aerodynamics.Drag.k2         * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Drag.dC_Wk1dFL    = BACM.Aerodynamics.Drag.dC_Wk1dFL  * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Drag.dC_Wk2dFL    = BACM.Aerodynamics.Drag.dC_Wk2dFL  * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Drag.dC_Wk1_DN    = BACM.Aerodynamics.Drag.dC_Wk1_DN  * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Drag.dC_Wk2_DN    = BACM.Aerodynamics.Drag.dC_Wk2_DN  * UncertaintyFactor.CDi;
% BACM.Aerodynamics.Pitch.C_m0        = BACM.Aerodynamics.Pitch.C_m0      * UncertaintyFactor.Cm0;
% BACM.Aerodynamics.Pitch.dC_mdFL     = BACM.Aerodynamics.Pitch.dC_mdFL   * UncertaintyFactor.Cm0;
% BACM.Aerodynamics.Pitch.dC_m_DN     = BACM.Aerodynamics.Pitch.dC_m_DN   * UncertaintyFactor.Cm0;
% BACM.Aerodynamics.Pitch.C_maFR      = BACM.Aerodynamics.Pitch.C_maFR    * UncertaintyFactor.Cma;
% BACM.Aerodynamics.Pitch.dC_madFL    = BACM.Aerodynamics.Pitch.dC_madFL  * UncertaintyFactor.Cma;
% BACM.Aerodynamics.Pitch.dC_ma_DN    = BACM.Aerodynamics.Pitch.dC_ma_DN  * UncertaintyFactor.Cma;
% BACM.Aerodynamics.Cmu.Cmu_Const     = BACM.Aerodynamics.Cmu.Cmu_Const   * UncertaintyFactor.Cmu;


%% Change uncertain parameters %%%%%%%%%%%%%%%%%by Noemi
if ~isempty(uncertain_params)
    list=fieldnames(uncertain_params);
    %list_of_params={'Cmu',  'C_L0', 'C_Lalpha',   'C_Lalpha_FL',  'C_Lalpha_DN', 'C_D0',  'k1',    'k1_FL',  'k1_DN',  'k2',  'k2_FL', 'k2_DN', 'C_m0' , 'C_malpha', 'C_malpha_FL',  'C_malpha_DN'} ;
    
    
        
    %C_LO (lift coefficient at alpha=0)
    % three components: basic value+full_flap shift + droop nose shift
    flag=strmatch( 'C_L0',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Lift.C_A0         = uncertain_params.('C_L0');
        %BACM.Aerodynamics.Lift.dC_AdFL      = BACM.Aerodynamics.Lift.dC_AdFL    * UncertaintyFactor.CL0;
        %BACM.Aerodynamics.Lift.dC_A_DN      = BACM.Aerodynamics.Lift.dC_A_DN    * UncertaintyFactor.CL0;
    end
    clear flag
    
    
    %C_Lalpha (slope of the lift coeff)
    % three components: basic value+full_flap shift + droop nose shift
    flag=strmatch( 'C_Lalpha',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Lift.C_AFRa       = uncertain_params.('C_Lalpha');
    end
    clear flag
    
    flag=strmatch( 'C_Lalpha_FL',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Lift.dC_AadFL(3)       = uncertain_params.('C_Lalpha_FL');
    end
    clear flag
    
    flag=strmatch( 'C_Lalpha_DN',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Lift.dC_Aa_DN       = uncertain_params.('C_Lalpha_DN');
    end
    clear flag
    
    flag=strmatch( 'C_D0',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.C_W0       = uncertain_params.('C_D0');
    end
    clear flag
    
    %BACM.Aerodynamics.Drag.dC_WdFL      = BACM.Aerodynamics.Drag.dC_WdFL    * UncertaintyFactor.CD0;
    %BACM.Aerodynamics.Drag.dC_W0_DN     = BACM.Aerodynamics.Drag.dC_W0_DN   * UncertaintyFactor.CD0;
    flag=strmatch( 'k1',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.k1        = uncertain_params.('k1');
    end
    clear flag
    flag=strmatch( 'k2',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.k2        = uncertain_params.('k2');
    end
    clear flag
    
    flag=strmatch( 'k1_FL',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.dC_Wk1dFL(3)        = uncertain_params.('k1_FL');
    end
    clear flag
    
    flag=strmatch( 'k2_FL',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.dC_Wk2dFL(3)        = uncertain_params.('k2_FL');
    end
    clear flag
    
    flag=strmatch( 'k1_DN',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.dC_Wk1_DN       = uncertain_params.('k1_DN');
    end
    clear flag
    
    flag=strmatch( 'k2_DN',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Drag.dC_Wk2_DN       = uncertain_params.('k2_DN');
    end
    clear flag
    
    flag=strmatch( 'C_m0',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Pitch.C_m0       = uncertain_params.('C_m0');
    end
    clear flag
    %BACM.Aerodynamics.Pitch.dC_mdFL     = BACM.Aerodynamics.Pitch.dC_mdFL   * UncertaintyFactor.Cm0;
    %BACM.Aerodynamics.Pitch.dC_m_DN     = BACM.Aerodynamics.Pitch.dC_m_DN   * UncertaintyFactor.Cm0;
    
    flag=strmatch('C_malpha',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Pitch.C_maFR       = uncertain_params.('C_malpha');
    end
    clear flag
    
    flag=strmatch('C_malpha_FL',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Pitch.dC_madFL(3)       = uncertain_params.('C_malpha_FL');
    end
    clear flag
    
    flag=strmatch('C_malpha_DN',list,'exact');
    if ~isempty(flag)
        BACM.Aerodynamics.Pitch.dC_ma_DN      = uncertain_params.('C_malpha_DN');
    end
    clear flag
end
 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%******************************************************************************************************
% Setup of trim parameters for unaccelerated horizontal flight                                        
%******************************************************************************************************
Trim_Case = 1;                   % Specify trim case:                                                 
                                 % 1 = Steady horizontal flight, descent or climb at specified gamma  
                                 % 2 = Steady descent or climb at specified power setting             
                                 % 3 = Steady horizontal flight at specified angle of attack          
                                 % 4 = Steady horizontal flight at specified angle of sideslip        
                                 % 5 = Steady turn at specified bank angle                            
                                 % 6 = Steady turn at specified heading rate                          

%******************************************************************************************************
% Setup of trim parameters for unaccelerated horizontal flight                                        *
%******************************************************************************************************
Alt              = 893.5; %900 * 0.3048;          % Altitude of the aircraft.                        [m]
Lat              = 0.913345000000000; %52.31866 * pi/180;           % Initial latitude position on earth             [rad]
Long             = 0.187677000000000; %10.54649 * pi/180;           % Initial longitude position on earth            [rad]
V_TAS            = 55;                      % True Air Speed of the aircraft.          [m/s]
Ma               = 0.15;                         % Mach number                                      [-]
alpha            = 3 * pi/180;                   % Angle of attack at specified trim point        [rad]
beta             = 3 * pi/180;                   % Angle of attack at specified trim point        [rad]
phi              = 0;                            % Starting value for bank angle                  [rad]
theta            = 3 * pi/180;                   % Starting value for pitch angle                 [rad]
psi              = 4.590215932745087; %* pi/1    % Starting value for heading angel                [rad]
psi_dot          =  0 * pi/180;                  % Heading rate in case of steady turn              [rad/s]
gamma            =0;                  % Set preferred flight path angle                [rad]
Throttle         = 1;                            % Throttle setting at the spec. trim point.        [-]
Flaps            = 65 * pi/180;                  % Flaps deflection at the specified trim point.  [rad]
Disable_BLC      = 1;                            % Activation/Deactivation(0/1) of BLC System       [-]    
Stabilizer       = 0  * pi/180;                  % Stabilizer Setting.                               [rad]
Spoiler          = 0;                            % Spoiler Setting.                               [rad]
Speed_Brake      = 0;                            % Speed Brake Setting                                          [-]
Gear             = 1;                            % Status of the landing gear [0] = retracted  / [1] = extended [-]
NG_Steering      = 0;                            % Nosegear Steering Angle                                    [rad]
Parking_Brake    = 0;                            % Parking_Brake [0] = released / [1] = fully applied           [-]
Ramp             = 0;                            % Status of the ramp door [0] = closed / [1] = open.           [-]
Set_Droop        = 1;
SetCmu           = 0.033;
Set_Slipstream   = 0;
Set_HTP_active   = 0;
Set_Lateral      = 1;

OWE             = 24061;
MLW             = 38901;
MTOW            = 40641;
MTest           = 36061;
Test_AC_Load    = (MLW - OWE);
Test_AC_Mass    = (Test_AC_Load + OWE); 
Trim_Name       = 'Solo_Trim';
X_CG            = 13.6;
S_H             = 24;
%% change uncertain Cmu%%%%%%%%%%%%%%%%%by Noemi
if ~isempty(uncertain_params)
       flag=strmatch('Cmu',list,'exact');
    if ~isempty(flag)
        SetCmu     = uncertain_params.('Cmu');
    end
    clear flag
end
%%
%******************************************************************************************************
% Setup of Aerodynamics Initialization                                                               *
%******************************************************************************************************
BACM.Aerodynamics.Cmu.Cmu_Var_Flg   = 0;
BACM.Aerodynamics.Cmu.Cmu_Const     = SetCmu / 2;
BACM.Aerodynamics.Unsteady_Flag     = 1;
BACM.Aerodynamics.Droop.Active      = Set_Droop;
BACM.Aerodynamics.Cmu.Cmu_ref_set   = BACM.Aerodynamics.Cmu.Cmu_ref(find(BACM.Aerodynamics.Flaps.Setting==Flaps));
BACM.Aerodynamics.Prop.On           = Set_Slipstream;
BACM.Aerodynamics.Active_HTP.On     = Set_HTP_active;
BACM.Aerodynamics.Inputs.Lat_Motion = Set_Lateral;

%******************************************************************************************************
% Setup of Propulsion Initialization                                                               *
%******************************************************************************************************
BACM.Propulsion.Prop_Efficiency     = 1;
BACM.Propulsion.OEI                 = 0;
BACM.Propulsion.BLCOP               = 0;

%******************************************************************************************************
% Setup of Weight and Balance Condition                                                               *
%******************************************************************************************************
% Weight and Balance struct assembly
BACM_Trim.Weight_Balance                       = BACM_WeightBalance_Init;
BACM_Trim.Weight_Balance.DynLoad               = BACM_DynamicLoad_Init;
BACM_Trim.Actuator.Surfaces.Blowing_Active     = Disable_BLC;
BACM_Trim.Weight_Balance.Fuel_Available        = Test_AC_Load;
BACM_Trim.Weight_Balance.AC_Empty.CG.x_rp      = -X_CG;

%******************************************************************************************************
% Setup of Wind Conditons
%******************************************************************************************************
% Wind Properties
Environment.Wind.V_W_o     =  0/1.943845;         % Magnitude of the Wind Velocity Vector         [m/s]
Environment.Wind.Direction =  0;                  % Direction of the Wind Veloctiy Vector         [deg]

% Environment struct assembly
BACM_Trim.Environment = Environment;

%******************************************************************************************************
% Setup of initial positons for coord. transformations
%******************************************************************************************************
BACM_Trim.Rigid_Body_Init.Position.lat      = Lat;                 % WGS-84 Latitude              [rad]
BACM_Trim.Rigid_Body_Init.Position.long     = Long;                % WGS-84 Longitude             [rad]
        
BACM.Configuration.Geometry.S_H             = S_H;

%******************************************************************************************************
% Assignment of Input, Output and State Vector Names                                                  *
%******************************************************************************************************
[X_Name,U_Name,Y_Name]=assignNames;

InputArgs.Alt           = Alt;
InputArgs.Lat           = Lat;
InputArgs.Long          = Long;
InputArgs.V_TAS         = V_TAS;
InputArgs.alpha         = alpha;
InputArgs.beta_A        = beta;
InputArgs.phi           = phi;
InputArgs.theta         = theta;
InputArgs.psi           = psi;
InputArgs.gamma         = gamma;
InputArgs.Throttle      = Throttle;
InputArgs.Flaps         = Flaps;
InputArgs.Spoiler       = Spoiler;
InputArgs.Speed_Brake   = Speed_Brake;
InputArgs.Gear          = Gear;
InputArgs.NG_Steering   = NG_Steering;
InputArgs.Parking_Brake = Parking_Brake;
InputArgs.Ramp          = Ramp;
InputArgs.Stabilizer    = Stabilizer;
InputArgs.psi_dot       = psi_dot;
InputArgs.Ma            = Ma;
InputArgs.Cmu           = SetCmu;

%******************************************************************************************************
% Call Trimming function for the BACM                                                                *
%******************************************************************************************************
[X0, U0, tY,error_flg] = ...
           BACM_Trimming_Quat_Func(InputArgs,...
           U_Name, X_Name, Y_Name, Trim_Case, BACM_Trim, BACM, TrimSettings); 
       
%******************************************************************************************************
%* BACM_Trim Struct Assembly                                                                          *
%******************************************************************************************************

        % Save names of the input, states and output vector
        BACM_Trim.Names.U_Name    = U_Name;                        % Input vector names
        BACM_Trim.Names.X_Name    = X_Name;                        % State vector names
        BACM_Trim.Names.Y_Name    = Y_Name;                        % Output vector names

        % Save Success
        BACM_Trim.Error = error_flg;

        % Save Initial Vectors
        BACM_Trim.Init.X0 = X0;                                    % Initial State Vector
        BACM_Trim.Init.U0 = U0;                                    % Initial Input Vector
        BACM_Trim.Init.Y0 = tY;                                    % Initial Ouput Vector

        % Save initial control vector values in struct
        BACM_Trim.U0.Throttle         = U0(1);                     % [-]
        BACM_Trim.U0.Aileron          = U0(2);                     % [rad]
        BACM_Trim.U0.Rudder           = U0(3);                     % [rad]
        BACM_Trim.U0.Elevator         = U0(4);                     % [rad]
        BACM_Trim.U0.Flaps            = U0(5);                     % [rad]
        BACM_Trim.U0.Stabilizer       = U0(6);                     % [rad]
        BACM_Trim.U0.Speed_Brake      = U0(7);                     % [-]
        BACM_Trim.U0.Ramp             = U0(8);                     % [-]
        BACM_Trim.U0.NG_Steering      = U0(11);                    % [rad]
        BACM_Trim.U0.Parking_Brake    = U0(12);                    % [-]
        BACM_Trim.U0.Gear             = U0(13);                    % [-]

        % Set Initial State Vector Values
        % Translation
        BACM_Trim.Rigid_Body_Init.Translation.u_K_b = X0(1);       % [m/s]
        BACM_Trim.Rigid_Body_Init.Translation.v_K_b = X0(2);       % [m/s]
        BACM_Trim.Rigid_Body_Init.Translation.w_K_b = X0(3);       % [m/s]

        % Rotation
        BACM_Trim.Rigid_Body_Init.Rotation.p_K_b    = X0(14);      % [m/s]
        BACM_Trim.Rigid_Body_Init.Rotation.q_K_b    = X0(13);      % [m/s]
        BACM_Trim.Rigid_Body_Init.Rotation.r_K_b    = X0(15);      % [m/s]

        % Attitude
        BACM_Trim.Rigid_Body_Init.Attitude.q_0      = X0(4);       % [rad]
        BACM_Trim.Rigid_Body_Init.Attitude.q_1      = X0(5);       % [rad]
        BACM_Trim.Rigid_Body_Init.Attitude.q_2      = X0(6);       % [rad]
        BACM_Trim.Rigid_Body_Init.Attitude.q_3      = X0(7);       % [rad]

        % Position
        BACM_Trim.Rigid_Body_Init.Position.h        = X0(8);       % [m]
        BACM_Trim.Rigid_Body_Init.Position.lat      = X0(9);       % [rad]
        BACM_Trim.Rigid_Body_Init.Position.long     = X0(33);       % [rad]

        % Propulsion
        BACM_Trim.Propulsion.LH_Inner_Throttle_Init     = X0(16);        % [-]
        BACM_Trim.Propulsion.RH_Inner_Throttle_Init     = X0(17);        % [-]
        BACM_Trim.Propulsion.LH_Inner_Throttle_Init_dot = X0(34);        % [-/s]
        BACM_Trim.Propulsion.RH_Inner_Throttle_Init_dot = X0(35);        % [-/s]

        % Booster Actuators
        BACM_Trim.Actuator.Booster.Stabilizer       = X0(19);      % [rad]
        BACM_Trim.Actuator.Booster.Elevator         = X0(21);      % [rad]
        BACM_Trim.Actuator.Booster.Aileron          = X0(23);      % [rad]
        BACM_Trim.Actuator.Booster.Rudder           = X0(22);      % [rad]
        BACM_Trim.Actuator.Booster.Aileron_Dot      = X0(26);      % [rad/s]
        BACM_Trim.Actuator.Booster.Elevator_Dot     = X0(28);      % [rad/s]
        BACM_Trim.Actuator.Booster.Rudder_Dot       = X0(31);      % [rad/s]
        BACM_Trim.Actuator.Booster.Stabilizer_Dot   = X0(32);      % [rad/s]
        BACM_Trim.Actuator.Spoiler_Init             = X0(27);      % [rad]
        BACM_Trim.Actuator.Speed_Brake_Init         = X0(24);      % [-]
        BACM_Trim.Actuator.Flaps_Init               = X0(18);      % [rad]
        BACM_Trim.Actuator.Ramp_Init                = X0(25);      % [-]
        BACM_Trim.Actuator.NG_Eta_Boost             = X0(29);      % [rad]
        BACM_Trim.Actuator.NG_Eta_Boost_dot         = X0(30);      % [rad/s]

        % Sensors
        BACM_Trim.Sensors.Temperature               = X0(36);      % [K]
        BACM_Trim.Sensors.alpha                     = X0(37);      % [rad]
        BACM_Trim.Sensors.alpha_dot                 = X0(38);      % [rad/s]
        BACM_Trim.Sensors.beta                      = X0(39);      % [rad]
        BACM_Trim.Sensors.beta_dot                  = X0(40);      % [rad/s]
        BACM_Trim.Sensors.nLoc                      = X0(41);      % [DDM]
        BACM_Trim.Sensors.nGs                       = X0(42);      % [DDM]
        BACM_Trim.Sensors.oF_frequency              = X0(43);      % [-/s]
        BACM_Trim.Sensors.oF_time                   = X0(44);      % [s]
        BACM_Trim.Sensors.dLoc_receive              = X0(45);      % [DDM]
        BACM_Trim.Sensors.dGs_receive               = X0(46);      % [DDM]
        
        % Save Success
        BACM_Trim.Error = error_flg;
        
%         % Display Results
%         disp(['V_TAS     [kt]: ', num2str(BACM_Trim.Init.Y0(1)*1.9438)]);
%         disp(['V_TAS    [m/s]: ', num2str(BACM_Trim.Init.Y0(1))]);
%         disp(['V_IAS     [kt]: ', num2str(BACM_Trim.Init.Y0(12)*1.9438)]);
%         disp(['q_inf  [N/m^2]: ', num2str(BACM_Trim.Init.Y0(25))]);
%         disp(['Ma         [-]: ', num2str(BACM_Trim.Init.Y0(15))]);
%         disp(['Alt        [m]: ', num2str(BACM_Trim.Init.Y0(5))]);
%         disp(['gamma    [deg]: ', num2str(BACM_Trim.Init.Y0(11)*180/pi)]);
%         disp(['alpha    [deg]: ', num2str(BACM_Trim.Init.Y0(2)*180/pi)]);
%         disp(['beta     [deg]: ', num2str(BACM_Trim.Init.Y0(6)*180/pi)]);
%         disp(['HTP-Trim [deg]: ', num2str(BACM_Trim.Init.U0(6)*180/pi)]);
%         disp(['Throttle   [%]: ', num2str(BACM_Trim.Init.U0(1)*100)]);
%         disp(['Elevator [deg]: ', num2str(BACM_Trim.Init.U0(4)*180/pi)]);
%         disp(['Aileron  [deg]: ', num2str(BACM_Trim.Init.U0(2)*180/pi)]);
%         disp(['Rudder   [deg]: ', num2str(BACM_Trim.Init.U0(3)*180/pi)]);
%         disp(['phi      [deg]: ', num2str(BACM_Trim.Init.Y0(7)*180/pi)]);
%         disp(['theta    [deg]: ', num2str(BACM_Trim.Init.Y0(4)*180/pi)]);
%         disp(['psi      [deg]: ', num2str(BACM_Trim.Init.Y0(8)*180/pi)]);
%         disp(['Flaps    [deg]: ', num2str(BACM_Trim.Init.X0(18)*180/pi)]);
%         disp(['C_mu       [-]: ', num2str(BACM_Trim.Init.Y0(14))]);
%         disp(['Look_Up    [-]: ', num2str(BACM_Trim.Init.Y0(16))]);
%         disp(['Thrust     [N]: ', num2str(BACM_Trim.Init.Y0(18))]);
%         disp(['Thrust Av  [N]: ', num2str(BACM_Trim.Init.Y0(19)*2)]);
%         disp(['alpha_H    [�]: ', num2str(BACM_Trim.Init.Y0(17)*180/pi)]);
%         disp(['CA_H       [-]: ', num2str(BACM_Trim.Init.Y0(21))]);
%         disp(['x_cg       [m]: ', num2str(BACM_Trim.Init.Y0(27))]);
%         disp(['CG         [%]: ', num2str(BACM_Trim.Init.Y0(28))]);
%         disp(['S_H      [m^2]: ', num2str(BACM_Trim.Init.Y0(26))]);
%         disp(['AC-Weight [kg]: ', num2str(Test_AC_Mass)]);
%         disp(['Set Droop  [-]: ', num2str(Set_Droop)]);
%         disp(['Set Slip   [-]: ', num2str(Set_Slipstream)]);
%         disp(['Set HTPact [-]: ', num2str(Set_HTP_active)]);
%         disp(['q_0        [-]: ', num2str(BACM_Trim.Init.X0(4))]);
%         disp(['q_1        [-]: ', num2str(BACM_Trim.Init.X0(5))]);
%         disp(['q_2        [-]: ', num2str(BACM_Trim.Init.X0(6))]);
%         disp(['q_3        [-]: ', num2str(BACM_Trim.Init.X0(7))]);
%         disp(['Error      [-]: ', num2str(BACM_Trim.Error)]);
%         disp(['C_A        [-]: ', num2str(BACM_Trim.Init.Y0(20))]);
%         disp(['C_D        [-]: ', num2str(BACM_Trim.Init.Y0(24))]);
%         disp(['E          [-]: ', num2str(BACM_Trim.Init.Y0(20)/BACM_Trim.Init.Y0(24))]);
%         disp(['C_ma       [-]: ', num2str(BACM_Trim.Init.Y0(29))]);
%         disp(['C_mA       [-]: ', num2str(BACM_Trim.Init.Y0(30))]);
%         disp(['X_N        [m]: ', num2str(BACM_Trim.Init.Y0(31))]);
%         disp(['X_N_Calc   [m]: ', num2str(BACM_Trim.Init.Y0(35))]);
%         disp(['X_N_cg     [m]: ', num2str(BACM_Trim.Init.Y0(32))]);
%         disp(['C_ma_Calc  [-]: ', num2str(BACM_Trim.Init.Y0(33))]);
%         disp(['C_Aa       [-]: ', num2str(BACM_Trim.Init.Y0(34))]);
%         disp(['nLoc     [DDM]: ', num2str(BACM_Trim.Init.X0(41))]);
%         disp(['nGs      [DDM]: ', num2str(BACM_Trim.Init.X0(42))]);
%         disp(['nLoc Rec [DDM]: ', num2str(BACM_Trim.Init.X0(45))]);
%         disp(['nGs Rec  [DDM]: ', num2str(BACM_Trim.Init.X0(46))]);
%         disp(['oF Freq   [Hz]: ', num2str(BACM_Trim.Init.X0(43))]);
%         disp(['oF Time    [s]: ', num2str(BACM_Trim.Init.X0(44))]);
% 
        TrimPointInfo.V_TAS_kt      = (BACM_Trim.Init.Y0(1)*1.9438);
        TrimPointInfo.V_TAS_mps     = (BACM_Trim.Init.Y0(1));
        TrimPointInfo.V_IAS_kt      = (BACM_Trim.Init.Y0(12)*1.9438);
        TrimPointInfo.Ma            = (BACM_Trim.Init.Y0(15));
        TrimPointInfo.Alt_m         = (BACM_Trim.Init.Y0(5));
        TrimPointInfo.gamma_deg     = (BACM_Trim.Init.Y0(11)*180/pi);
        TrimPointInfo.alpha_deg     = (BACM_Trim.Init.Y0(2)*180/pi);
        TrimPointInfo.beta_deg      = (BACM_Trim.Init.Y0(6)*180/pi);
        TrimPointInfo.HTP_Trim_deg  = (BACM_Trim.Init.U0(6)*180/pi);
        TrimPointInfo.Throttle      = (BACM_Trim.Init.U0(1)*100);
        TrimPointInfo.Elevator_deg  = (BACM_Trim.Init.U0(4)*180/pi);
        TrimPointInfo.Aileron_deg   = (BACM_Trim.Init.U0(2)*180/pi);
        TrimPointInfo.Rudder_deg    = (BACM_Trim.Init.U0(3)*180/pi);
        TrimPointInfo.phi_deg       = (BACM_Trim.Init.Y0(7)*180/pi);
        TrimPointInfo.theta_deg     = (BACM_Trim.Init.Y0(4)*180/pi);
        TrimPointInfo.psi_deg       = (BACM_Trim.Init.Y0(8)*180/pi);
        TrimPointInfo.Flaps_deg     = (BACM_Trim.Init.X0(18)*180/pi);
        TrimPointInfo.C_mu          = (BACM_Trim.Init.Y0(14));
        TrimPointInfo.alpha_H       = (BACM_Trim.Init.Y0(17)*180/pi);
        TrimPointInfo.q_0           = (BACM_Trim.Init.X0(4));
        TrimPointInfo.q_1           = (BACM_Trim.Init.X0(5));
        TrimPointInfo.q_2           = (BACM_Trim.Init.X0(6));
        TrimPointInfo.q_3           = (BACM_Trim.Init.X0(7));
        TrimPointInfo.Error         = (BACM_Trim.Error);
        TrimPointInfo.AC_Mass       = (Test_AC_Mass);
        TrimPointInfo.SetDroop      = (Set_Droop);
        TrimPointInfo.SetSlipstream = (Set_Slipstream);
        TrimPointInfo.SetHTPactive  = (Set_HTP_active);
        TrimPointInfo.SetLateral    = (Set_Lateral);
        TrimPointInfo.x_CG          = (BACM_Trim.Init.Y0(27));
        TrimPointInfo.CG            = (BACM_Trim.Init.Y0(28));
        TrimPointInfo.S_H           = (BACM_Trim.Init.Y0(26));
        TrimPointInfo.C_A           = (BACM_Trim.Init.Y0(20));
        TrimPointInfo.C_ma          = (BACM_Trim.Init.Y0(29));
        TrimPointInfo.C_mA          = (BACM_Trim.Init.Y0(30));
        TrimPointInfo.X_N           = (BACM_Trim.Init.Y0(31));
        TrimPointInfo.X_N_cg        = (BACM_Trim.Init.Y0(32));
        TrimPointInfo.C_ma_Calc     = (BACM_Trim.Init.Y0(33));
        TrimPointInfo.CAa           = (BACM_Trim.Init.Y0(34));
        TrimPointInfo.X_N_Calc      = (BACM_Trim.Init.Y0(35));
        TrimPointInfo.C_AH          = (BACM_Trim.Init.Y0(21));
        TrimPointInfo.F_A           = (BACM_Trim.Init.Y0(22));
        TrimPointInfo.F_AH          = (BACM_Trim.Init.Y0(23));
        TrimPointInfo.C_D           = (BACM_Trim.Init.Y0(24));
        TrimPointInfo.q_inf         = (BACM_Trim.Init.Y0(25));
        TrimPointInfo.Thrust        = (BACM_Trim.Init.Y0(18));
        TrimPointInfo.Thrust_avail  = (BACM_Trim.Init.Y0(19)*2);
        TrimPointInfo.Engine_Mode   = (BACM_Trim.Init.Y0(16));

% %******************************************************************************************************
% % Save the Trim Results to a MAT-File                                                                 *
% %******************************************************************************************************
% 
% TrimSettings.strTrimfile = [pwd '\'...
%                             'Alt' num2str(floor(Alt))...
%                             'Ma0' num2str(floor(BACM_Trim.Init.Y0(15)*100))...
%                             'FPA' num2str(floor(gamma))...
%                             date...
%                             num2str(timestamp(4))...
%                             num2str(timestamp(5))...
%                             '.mat'];       
% 
% % if ~exist([pwd '\' 'Test_Trim\'],'dir')
% %     mkdir([pwd '\' 'Test_Trim\']);
% % end
% 
% if(isempty(TrimSettings.strTrimfile) == 1)
%     % Ask for filename
%     [FileName,PathName] = uiputfile('*.mat','Save Trim-Parameter File');
%     TrimSettings.strTrimfile = strcat(PathName, FileName);
% end
% 
% % Save structure in MAT-File
% if(isempty(TrimSettings.strTrimfile) == 1)
%     disp('Warning: No file specified. Nothing saved!');
% else
%     save(TrimSettings.strTrimfile, 'BACM_Trim', 'TrimPointInfo', 'X0', 'tY', 'U0');
%     disp(['Save-File: Trim'...
%                         'H' num2str(floor(Alt))...
%                         'Ma0' num2str(floor(BACM_Trim.Init.Y0(15)*100))...
%                         'FPA' num2str(floor(gamma)) '_'...
%                         date '_'...
%                         num2str(timestamp(4))...
%                         num2str(timestamp(5))])
% end
end

% Clear Workspace
% clear all;
%---------------------------------------------------------------------------------------------------EOF