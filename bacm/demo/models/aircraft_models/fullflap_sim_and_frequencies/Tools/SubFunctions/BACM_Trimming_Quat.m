%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Trimming_Quat                                            *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Trim the BACM aircraft model for horizontal and unaccelerated flight.                   *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%* Sub-Functions : jj_trim.m, DLR_BACM_Trim.mdl, clean_string.m, find_index.m                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : [X0 U0,Y0,varargout] =                                                                              *
%* BACM_Trimming_Quat(Alt, V_TAS,alpha, phi, theta, psi, Throttle, Flaps, Speed_Brake, Gear, Ramp, ...*
%*     U_Name,X_Name,Y_Name)                                                                          *
%*                                                                                                    *
%* Input Parameters:                                                                                  *
%* Alt           = Altitude of the aircraft.                                                      [m] *
%* V_TAS         = True Air Speed of the aircraft.                                              [m/s] *
%* Lat           = Latitude at the initial aircraft position.                                   [rad] *
%* Long          = Longitude at the initial aircraft position.                                  [rad] *
%* alpha         = Starting value for the angle of attack at specified trim point               [rad] *
%* phi           = Starting value for the phi angle at specified trim point.                    [rad] *
%* theta         = Starting value for the theta angle at specified trim point.                  [rad] *
%* psi           = Starting value for the aircraft heading                                      [rad] *
%* Throttle      = Starting value for the overall throttle setting at the specified trim point.   [-] *
%* Flaps         = Flaps setting at the specified trim point.                                   [rad] *
%* Spoiler       = Spoiler Setting.                                                               [-] *
%* Speed_Brake   = Speed Brake Setting.                                                         [rad] *
%* Gear          = Status of the landing gear [0] = retracted  / [1] = extended.                  [-] *
%* NG_Steering   = Nose gear steering angle                                                     [rad] *
%* Parking_Brake = Praking brake [1] = on or [0] = off.                                           [-] *
%* Ramp          = Status of the ramp door [0] = closed / [1] = open.                             [-] *
%* X_Name        = Vector containting the state names.                                            [-] *
%* U_Name        = Vector containing the input names.                                             [-] *
%* Y_Name        = Vector containing the output names.                                            [-] *
%*                                                                                                    *
%* Output Parameters:                                                                                 *
%* X0     = Initial state vector resulting from the trim calculation.                             [-] *
%* U0     = Initial input vector resulting from the trim calculation.                             [-] *
%* Y0     = Initial output vector resulting from the trim calculation.                            [-] *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 20-NOV-09 * Basic Design                                            *
%******************************************************************************************************
function [x_tr, u_tr, y_tr, varargout] = ...
           BACM_Trimming_Quat(InputArgs,...
           U_Name, X_Name, Y_Name, Trim_Case)

global BACM;
global BACM_Trim;
global TrimSettings

Alt             = InputArgs.Alt;
Lat             = InputArgs.Lat;
Long            = InputArgs.Long;
V_TAS           = InputArgs.V_TAS;
alpha           = InputArgs.alpha;
beta_A          = InputArgs.beta_A;
phi             = InputArgs.phi;
theta           = InputArgs.theta;
psi             = InputArgs.psi;
gamma           = InputArgs.gamma;
Throttle        = InputArgs.Throttle;
Flaps           = InputArgs.Flaps;
Spoiler         = InputArgs.Spoiler;
Speed_Brake     = InputArgs.Speed_Brake;
Gear            = InputArgs.Gear;
NG_Steering     = InputArgs.NG_Steering;
Parking_Brake   = InputArgs.Parking_Brake;
Ramp            = InputArgs.Ramp;
Stabilizer      = InputArgs.Stabilizer;
psi_dot         = InputArgs.psi_dot;
Ma              = InputArgs.Ma;
Cmu             = InputArgs.Cmu;           

%******************************************************************************************************
% Check model for state vector and state namens consistency
%******************************************************************************************************

% Get sizes of the model vectors and the state names
[vec_sizes, initial_conditions, state_names_with_nl] = feval(TrimSettings.MdlName);

% Clean state names from subsystem path names
cleaned_state_names = clean_string(state_names_with_nl);

% Check consistency of the vector sizes
if  vec_sizes(1) ~= length(X_Name);
    error('Size of the state vector does not meet specification!');
elseif vec_sizes(3) ~= length(Y_Name);
    error('Size of the output vector does not meet specification!');
elseif vec_sizes(4) ~= length(U_Name);
    error('Size of the input vector does not meet specification!');
end

%******************************************************************************************************
% Determination of initial quaternions from Euler angles
%******************************************************************************************************
q_0_init = cos(phi/2)*cos(theta/2)*cos(psi/2)+sin(phi/2)*sin(theta/2)*sin(psi/2);
q_1_init = sin(phi/2)*cos(theta/2)*cos(psi/2)-cos(phi/2)*sin(theta/2)*sin(psi/2);
q_2_init = cos(phi/2)*sin(theta/2)*cos(psi/2)+sin(phi/2)*cos(theta/2)*sin(psi/2);
q_3_init = cos(phi/2)*cos(theta/2)*sin(psi/2)-sin(phi/2)*sin(theta/2)*cos(psi/2);

%******************************************************************************************************
% Setup of trim variables and conditons
%******************************************************************************************************

% Set trim start conditions for the aircraft velocity in body system x and z direction
u_K_b = V_TAS*cos(alpha);
w_K_b = V_TAS*sin(alpha);

% State Space Initial Vector tX0
x_init = [  u_K_b      ;...     %  1 - u_K_b                        [m/s]
            0          ;...     %  2 - v_K_b                        [m/s]
            w_K_b      ;...     %  3 - w_K_b                        [m/s]
            q_0_init   ;...     %  4 - q_0                            [-]
            q_1_init   ;...     %  5 - q_1                            [-]
            q_2_init   ;...     %  6 - q_2                            [-]
            q_3_init   ;...     %  7 - q_3                            [-]
            Alt        ;...     %  8 - AC_h                           [m]
            Lat        ;...     %  9 - AC_lat                       [rad]
            0          ;...     % 10 - u_Turb_x1                      [-]
            0          ;...     % 11 - v_Turb_x1                      [-]
            0          ;...     % 12 - w_Turb_x1                      [-]
            0          ;...     % 13 - q_K_b                      [rad/s]
            0          ;...     % 14 - p_K_b                      [rad/s]
            0          ;...     % 15 - r_K_b                      [rad/s]
            Throttle   ;...     % 16 - LH_Inner_Throttle_State        [-]
            Throttle   ;...     % 17 - RH_Inner_Throttle_State        [-]
            Flaps      ;...     % 18 - Flaps                        [rad]
            0          ;...     % 19 - Stabilizer_Boost             [rad]
            0          ;...     % 20 - Fuel_Used                     [kg]
            0          ;...     % 21 - Elevator_Boost               [rad]
            0          ;...     % 22 - Rudder_Boost                 [rad]
            0          ;...     % 23 - Aileron_Boost                [rad]
            Speed_Brake;...     % 24 - Speed_Brake                    [-]
            Ramp       ;...     % 25 - Ramp_Door                      [-]
            0          ;...     % 26 - Aileron_Boost_dot          [rad/s]
            Spoiler    ;...     % 27 - Roll_Spoiler                 [rad]
            0          ;...     % 28 - Elevator_Boost_dot         [rad/s]
            0          ;...     % 29 - NG_Eta_Boost                 [rad]
            0          ;...     % 30 - NG_Eta_Boost_dot           [rad/s]
            0          ;...     % 31 - Rudder_Boost_dot           [rad/s]
            0          ;...     % 32 - Stabilizer_Boost_dot       [rad/s]
            Long       ;...     % 33 - AC_long                      [rad]
            0          ;...     % 34 - LH_Inner_Throttle_State_dot  [-/s]
            0          ;...     % 35 - RH_Inner_Throttle_State_dot  [-/s]
            288.15     ;...     % 36 - Temperature_Sensor             [K]
            0          ;...     % 37 - alpha_Sensor                 [rad]
            0          ;...     % 38 - alpha_Sensor_dot           [rad/s]
            0          ;...     % 39 - beta_Sensor                  [rad]
            0          ;...     % 40 - beta_Sensor_dot            [rad/s]
            0          ;...     % 41 - nLoc                         [DDM]
            0          ;...     % 42 - nGs                          [DDM]
            0          ;...     % 43 - oF frequency                  [Hz]
            0          ;...     % 44 - oF time                      [sec]
            0          ;...     % 45 - dLoc receiver                [DDM]
            0          ;...     % 46 - dGs receiver                 [DDM]
            0          ;...     % 47 - v_Turb_x3                      [-]
            0          ;...     % 48 - v_Turb_x2                      [-]
            0          ;...     % 49 - u_Turb_x2                      [-]
            0          ;...     % 50 - w_Turb_x3                      [-]
            0]         ;        % 51 - w_Turb_x2                      [-]

% Set Index of trim variable states
switch Trim_Case
    case {1 2 3 4}
%       i_x = [1 2 3 4 5 6 7          17 18 20 22 23 24]'; % Stationary horinzontal flight 
        i_x = [1 2 3 4 5 6 7          16 17 19 21 22 23]'; % Stationary horinzontal flight 
    case {5 6}
%       i_x = [1 2 3 4 5 6 7 13 15 16 17 18 20 22 23 24]'; % Stationary horinzontal flight
        i_x = [1 2 3 4 5 6 7 13 14 15 16 17 19 21 22 23]'; % Stationary horinzontal flight
    otherwise
        errordlg('Trim Case not Specified or Wrong!','Input Error');
end;

% Initial Vector U0
u_init = [  Throttle        ; ... %  1  - Throttle                    [-]
            0               ; ... %  2  - Aileron                   [rad]
            0               ; ... %  3  - Rudder                    [rad]
            0               ; ... %  4  - Elevator                  [rad]
            Flaps           ; ... %  5  - Flaps                     [rad]
            Stabilizer      ; ... %  6  - Stabilizer                [rad]
            Speed_Brake     ; ... %  7  - Speed Brake                 [-]
            Ramp            ; ....%  8  - Ramp                        [-]
            0               ; ....%  9  - Wheel Brake LH              [-]
            0               ; ....% 10  - Wheel Brake RH              [-]
            NG_Steering     ; ... % 11  - Gear Steering Angle       [rad]
            Parking_Brake   ; ... % 12  - Parking_Brake               [-]
            Gear ]          ;     % 13  - Gear                        [-]

% Set Index of trim variable inputs
switch Trim_Case
    case {1 3 4 5 6} ;
        i_u = [1 2 3 6]'; % Stationary horinzontal flight
    case 2 ;
        i_u = [  2 3 6]'; % Stationary horinzontal flight
end;

% Derivates of the State Space Vector tDX0
xdot_init = [   0         ;...     %  1 - u_K_b_dot                        [m/s^2]
                0         ;...     %  2 - v_K_b_dot                        [m/s^2]
                0         ;...     %  3 - w_K_b_dot                        [m/s^2]
                0         ;...     %  4 - q_0_dot                            [-/s]
                0         ;...     %  5 - q_1_dot                            [-/s]
                0         ;...     %  6 - q_2_dot                            [-/s]
                0         ;...     %  7 - q_3_dot                            [-/s]
                0         ;...     %  8 - AC_h_dot                           [m/s]
                0         ;...     %  9 - AC_lat_dot                       [rad/s]
                0         ;...     % 10 - u_Turb_x1_dot                        [-]
                0         ;...     % 11 - v_Turb_x1_dot                        [-]
                0         ;...     % 12 - w_Turb_x1_dot                        [-]
                0         ;...     % 13 - q_K_b_dot                      [rad/s^2]
                0         ;...     % 14 - p_K_b_dot                      [rad/s^2]
                0         ;...     % 15 - r_K_b_dot                      [rad/s^2]
                0         ;...     % 16 - LH_Inner_Throttle_State_dot        [-/s]
                0         ;...     % 17 - RH_Inner_Throttle_State_dot        [-/s]
                0         ;...     % 18 - Flaps_dot                        [rad/s]
                0         ;...     % 19 - Stabilizer_Boost_dot             [rad/s]
                0         ;...     % 20 - Fuel_Used_dot                     [kg/s]
                0         ;...     % 21 - Elevator_Boost_dot               [rad/s]
                0         ;...     % 22 - Rudder_Boost_dot                 [rad/s]
                0         ;...     % 23 - Aileron_Boost_dot                [rad/s]
                0         ;...     % 24 - Speed_Brake_dot                    [-/s]
                0         ;...     % 25 - Ramp_Door_dot                      [-/s]
                0         ;...     % 26 - Aileron_Boost_dot_dot          [rad/s^2]
                0         ;...     % 27 - Roll_Spoiler_dot                 [rad/s]
                0         ;...     % 28 - Elevator_Boost_dot_dot         [rad/s^2]
                0         ;...     % 29 - NG_Eta_Boost_dot                 [rad/s]
                0         ;...     % 30 - NG_Eta_Boost_dot_dot           [rad/s^2]
                0         ;...     % 31 - Rudder_Boost_dot_dot           [rad/s^2]
                0         ;...     % 32 - Stabilizer_Boost_dot_dot       [rad/s^2]
                0         ;...     % 33 - AC_long_dot                      [rad/s]
                0         ;...     % 34 - LH_Inner_Throttle_State_dot_dot  [-/s^2]
                0         ;...     % 35 - RH_Inner_Throttle_State_dot_dot  [-/s^2]
                0         ;...     % 36 - Temperature_Sensor_dot             [K/s]
                0         ;...     % 37 - alpha_Sensor_dot                 [rad/s]
                0         ;...     % 38 - alpha_Sensor_dot_dot           [rad/s^2]
                0         ;...     % 39 - beta_Sensor_dot                  [rad/s]
                0         ;...     % 40 - beta_Sensor_dot_dot            [rad/s^2]
                0         ;...     % 41 - nLoc                             [DDM/s]
                0         ;...     % 42 - nGs                              [DDM/s]
                0         ;...     % 43 - oF frequency                     [-/s^2]
                0         ;...     % 44 - oF time                              [-]
                0         ;...     % 45 - dLoc receiver                    [DDM/s]
                0         ;...     % 46 - dGs receiver                     [DDM/s]
                0         ;...     % 47 - v_Turb_x3_dot                        [-]
                0         ;...     % 48 - v_Turb_x2_dot                        [-]
                0         ;...     % 49 - u_Turb_x2_dot                        [-]
                0         ;...     % 50 - w_Turb_x3_dot                        [-]
                0]        ;        % 51 - w_Turb_x2_dot                        [-]

% Set Index for Trim Requirements for the Derivative of States for
% unaccelerated horizontal flight
% tIDX = [1 2 3 13 15 33 36 37]'; % Stationary horinzontal flight
% i_xdot = [1 2 3 13 15 16 27 29 32 33 36 37]'; % Stationary horinzontal flight
  i_xdot = [1 2 3 13 14 15 26 28 31 32 34 35]'; % Stationary horinzontal flight

% Output Vector
y_init = [  V_TAS   ; ...              %  1 - V_TAS      [m/s]
            alpha   ; ...              %  2 - alpha_A    [rad]
            0       ; ...              %  3 - q_K_b      [rad/s]
            theta   ; ...              %  4 - theta      [rad]
            Alt     ; ...              %  5 - Alt        [m]
            beta_A  ; ...              %  6 - beta_A     [rad]
            phi     ; ...              %  7 - phi        [rad]
            psi     ; ...              %  8 - psi        [rad]
            0       ; ...              %  9 - p_K_b      [rad/s]
            0       ; ...              % 10 - r_K_b      [rad/s]
            gamma   ; ...              % 11 - gamma      [rad]
            0       ; ...              % 12 - VIAS       [m/s]
            1       ; ...              % 13 - Quat_norm  [-]
            Cmu     ; ...              % 14 - C_mu       [-]
            Ma      ; ...              % 15 - Ma         [-]
            0       ; ...              % 16 - Thrust_Look_Up
            0       ; ...              % 17 - alpha_H    [rad]  
            0       ; ...              % 18 - Thrust     [N]  
            0       ; ...              % 19 - Thrust Available [N]  
            0       ; ...              % 20 - Lift Coefficient [-]  
            0       ; ...              % 21 - HTP Lift [-]  
            0       ; ...              % 22 - Lift Force [N]  
            0       ; ...              % 23 - HTP Lift Force [N]  
            0       ; ...              % 24 - Drag Coefficient [N]  
            0       ; ...              % 25 - Dynamic Pressure [N]  
            0       ; ...              % 26 - S_H [m^2]  
            0       ; ...              % 27 - x_CG [m]  
            0       ; ...              % 28 - CG [%]
            0       ; ...              % 29 - C_ma [-]  
            0       ; ...              % 30 - CmA [-]
            0       ; ...              % 31 - X_N [m]
            0       ; ...              % 32 - X_N_cg [m]
            0       ; ...              % 33 - C_ma_Calc [-]
            0       ; ...              % 34 - C_Aa [-]
            0       ; ...              % 35 - X_N_Calc [m]
            0       ; ...              % 36 - phi_dot [rad/s]
            0       ; ...              % 37 - theta_dot [rad/s]
            psi_dot   ...              % 38 - psi_dot [rad/s]
             ] ;                

% Set Index of Trim Requirements for the Output Vector
switch Trim_Case
    case 1
        i_y = [1     7 8 11 13]'; % Steady horizontal flight, descent or climb at specified gamma
    case 2
        i_y = [1     7 8    13]'; % Steady descent or climb at specified power setting 
    case 3
        i_y = [  2     8 11 13 15]'; % Steady horizontal flight at specified angle of attack
    case 4
        i_y = [1   6   8 11 13]'; % Steady horizontal flight at specified angle of sideslip
    case 5
        i_y = [1   6 7 8 11 13 36 37]'; % Steady turn at specified bank angle
    case 6
        i_y = [1   6   8 11 13 36 37 38]'; % Steady turn at specified heading rate
end;
% tIY = [1 8 11 13]'; % Stationary horinzontal flight

%******************************************************************************************************
% Automatic check and matching of the model state vector
%******************************************************************************************************

% Compare the strings in X_Name with the ones obtained from SIMULINK model
if  all(strcmp(cleaned_state_names, X_Name)) == false;
    warning('State names are not correct or in wrong order! Trying to sort automatically!');

    % Re-arrange state vector order and index vector for trim variables and requirements

    % Find the indeces of the states defined in X_name within the state vector obtained from SIMULINK model
    [X_Index] = find_index(X_Name, cleaned_state_names);

    % Compare the strings in X_Name with the ones obtained from SIMULINK model
    if  sum(X_Index==0) == 1;
        error('State names are not correct!');
    end;

    % Create new state vector and state vector derivative order
    x_init_new(X_Index)   = x_init;
    xdot_init_new(X_Index)= xdot_init;
    X_Name_new(X_Index) = X_Name;    % <------------------- zur �berpr�fung

    x_init    = x_init_new(:);
    xdot_init = xdot_init_new(:);
    X_Name    = X_Name_new(:);

    % Create new trim variable index vector
    i_x_new = X_Index(i_x);
    i_x = sort(i_x_new)';

    % Create new trim requirement index vector for the state derivatives
    [row_ir col_ir] = size(i_xdot);
    i_xdot_new = X_Index(i_xdot);
    i_xdot = sort(i_xdot_new)';

    % Check if sorting was successful  <------------------- zur �berpr�fung
    if  all(strcmp(cleaned_state_names, X_Name)) == false;
        error('Not able to sort!');
    else
        disp('Success!')
    end

else
    X_Index = 1:22;
end;

%******************************************************************************************************
% Call Trim-Routine
%******************************************************************************************************

[x_tr,u_tr,xdot_tr,y_tr,err_flg] = ...
     jj_trim(TrimSettings.MdlName,x_init,u_init,xdot_init,y_init, ...
               i_x,i_u,i_xdot,i_y,X_Name,U_Name,X_Name,Y_Name);

if nargout > 3
    varargout(1)   = {err_flg};
end
%******************************************************************************************************
% Check if state vector was re-arranged and store results in the original state vector
%******************************************************************************************************
if  exist('tX0_new','var')

    % Re-arrange trimming results to original order
    xtr_new = x_tr(X_Index);
    xdottr_new = xdot_tr(X_Index);
    X_Name_new = X_Name(X_Index);

    x_tr   =  xtr_new(:);
    xdot_tr =  xdottr_new(:);
    X_Name = X_Name_new(:);

end;
%---------------------------------------------------------------------------------------------------EOF