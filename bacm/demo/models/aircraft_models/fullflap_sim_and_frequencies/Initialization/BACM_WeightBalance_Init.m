%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      SFB880_WeightBalance_template                                 *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the workspace variables that contain basic aircraft mass, CG, inertia.      *
%*            A template file is replaced by the values generated with DATCOM.                        *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : Aircraft Name: SFB880_Konfig_28_2_2012                                                      *
%*                                                                                                    *
%*            file created : 11-FEB-2011                                                              *
%*                           The data is taken from Jategaonkar (FB 90-40)and ATTAS Fortran model     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : SFB880_WeightBalance_template                                                             *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Andre Thurm                  * 27-JAN-11 * Basic Design                                            *
%******************************************************************************************************
function Weight_Balance = BACM_WeightBalance_Init

BACM.Configuration  =   BACM_Configuration_Init;

Weight_Balance.AC_Empty.Mass       = 24061;             % Aircraft Empty Mass                             [kg]
Weight_Balance.Fuel_Available      = 12000;             % Fuel Available                                  [kg]
Weight_Balance.Fuel_Used           = 0;                 % Fuel Used                                       [kg]
   
% AC CG Position without additional loads
Weight_Balance.AC_Empty.CG.MAC  = 25;                   % Define X-coord. of CG Postion in %-MAC           [%]
Weight_Balance.AC_Empty.CG.x_rp = BACM.Configuration.Geometry.x_lmac - Weight_Balance.AC_Empty.CG.MAC/100 ...
* BACM.Configuration.Geometry.l_mac;                    % AC Empty CG Pos. x-coord.                        [m]
Weight_Balance.AC_Empty.CG.y_rp =  0.00;                % AC Empty CG Pos. y-coord. in reference frame     [m]
Weight_Balance.AC_Empty.CG.z_rp =  -1.563;              % AC Empty CG Pos. z-coord. in reference frame     [m]

% Moments of inertia of aircraft with fuel but wihtout additonal loads
Weight_Balance.AC_Empty.Inertia.I_xx = 446027.33;       % Inertia xx-component at about defined CG.[kgm^2]
Weight_Balance.AC_Empty.Inertia.I_yy = 982930.45;       % Inertia yy-component at about defined CG.[kgm^2]
Weight_Balance.AC_Empty.Inertia.I_zz = 1327869.07;      % Inertia zz-component at about defined CG.[kgm^2]
Weight_Balance.AC_Empty.Inertia.I_xz = -59255.22;       % Inertia xz-component at about defined CG.[kgm^2]
Weight_Balance.AC_Empty.Inertia.I_yx = 0; %-554;        % Inertia yx-component at about defined CG.[kgm^2]
Weight_Balance.AC_Empty.Inertia.I_zy = 0; %53.27;       % Inertia zy-component at about defined CG.[kgm^2]

% Fuel Consumption Flag
Weight_Balance.EnableFuelConsumption = 0;               % Turn Fuel Consumption [1] ON / [0] OFF

end
%---------------------------------------------------------------------------------------------------EOF