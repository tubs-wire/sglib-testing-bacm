clear all
close all
clc

Aerodynamics = BACM_Aerodynamics_Init

%% Inputs
alpha       = (-10:20)      * pi/180;
beta        =   0           * pi/180;
Flaps       =   65          * pi/180;
i_htp       =   0           * pi/180;
eta         =   0           * pi/180;
xi          =   0           * pi/180;
Ceta        =   0           * pi/180;
p           =   0           * pi/180;
q           =   0           * pi/180;
r           =   0           * pi/180;

%% Data Assembly
CL0         = Aerodynamics.Lift.C_A0;
CLa         = Aerodynamics.Lift.C_AFRa;
CLa_H       = Aerodynamics.Lift.C_Aa_H;
CLeta_H     = Aerodynamics.Lift.C_Aeta;
dCL0_FL     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AdFL,Flaps);
dCLa_FL     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AadFL,Flaps);
dCL0_BL     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AdCmu_BLC,Flaps);
dCL0_CC     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AdCmu_CC,Flaps);
dCLa_BL     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AadCmu_BLC,Flaps);
dCLa_CC     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_AadCmu_CC,Flaps);
dCLa2BL     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_Aa2dCmu_BLC,Flaps);
dCLa2CC     = interp1(Aerodynamics.Flaps.Setting,Aerodynamics.Lift.dC_Aa2dCmu_CC,Flaps);
dCL0_DN     = Aerodynamics.Lift.dC_A_DN;
dCLa_DN     = Aerodynamics.Lift.dC_Aa_DN;

f_Ref       = Aerodynamics.Lift.f_Ref;
a1_Ref      = Aerodynamics.Lift.a1_Ref;
as_Ref      = Aerodynamics.Lift.astar_Ref;

f_BLC       = Aerodynamics.Lift.df_dcmu_BLC;
a1_BLC      = Aerodynamics.Lift.da1_dcmu_BLC;
as_BLC      = Aerodynamics.Lift.dastar_dcmu_BLC;

f_CC        = Aerodynamics.Lift.df_dcmu_CC;
a1_CC       = Aerodynamics.Lift.da1_dcmu_CC;
as_CC       = Aerodynamics.Lift.dastar_dcmu_CC;

f_DN        = Aerodynamics.Lift.df_DN;
a1_DN       = Aerodynamics.Lift.da1_DN;
as_DN       = Aerodynamics.Lift.dastar_DN;

Ff_DN       = Aerodynamics.Lift.Ff_DN;
Fa1_DN      = Aerodynamics.Lift.Fa1_DN;
Fas_DN      = Aerodynamics.Lift.Fastar_DN;



