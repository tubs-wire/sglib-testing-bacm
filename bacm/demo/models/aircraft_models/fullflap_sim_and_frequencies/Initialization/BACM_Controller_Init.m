%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2014      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Controller_Init                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the workspace variables for the Controller Modul.                            *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Controller_Init                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Jobst Henning Diekmann       * 30-Okt-14 * Integration of multiple Basic Controller, AP & AThr     *
%******************************************************************************************************

function Controller = BACM_Controller_Init(BACM_Trim)

%% Initial Inputs
% Detect Saturated Output Signal
Controller.Control                  = 0;    % Control Section on/off [1/0]
Controller.Time_Control             = 0;    % Time Switch On Outer Controller
Controller.Int_Sat_Prot             = 1;	% Switch protection on/off [1/0]
Controller.Control_Type             = 1;    % Types:    1 - No Outer Controller
                                            %           2 - Experimental Recovery Cotroller
                                            %           3 - AutoPilot/AutoThrottle
                                            %           4 - TECS/Energy Control
                                            
%% Thurst LookUp
load Inv_TP_Look_Up_REF2.mat
Controller.ThrustLookUp.Case1.Bleed     = Inv_Thrust_Look_up.Case1.Bleed;
Controller.ThrustLookUp.Case1.Ma        = Inv_Thrust_Look_up.Case1.Ma;
Controller.ThrustLookUp.Case1.PL        = Inv_Thrust_Look_up.Case1.PL;
Controller.ThrustLookUp.Case1.Alt       = Inv_Thrust_Look_up.Case1.Alt;
Controller.ThrustLookUp.Case1.Thrust    = Inv_Thrust_Look_up.Case1.Thr;

Controller.ThrustLookUp.Case2.Bleed     = Inv_Thrust_Look_up.Case2.Bleed;
Controller.ThrustLookUp.Case2.Ma        = Inv_Thrust_Look_up.Case2.Ma;
Controller.ThrustLookUp.Case2.PL        = Inv_Thrust_Look_up.Case2.PL;
Controller.ThrustLookUp.Case2.Alt       = Inv_Thrust_Look_up.Case2.Alt;
Controller.ThrustLookUp.Case2.Thrust    = Inv_Thrust_Look_up.Case2.Thr;

Controller.ThrustLookUp.Case3.Bleed     = Inv_Thrust_Look_up.Case3.Bleed;
Controller.ThrustLookUp.Case3.Ma        = Inv_Thrust_Look_up.Case3.Ma;
Controller.ThrustLookUp.Case3.PL        = Inv_Thrust_Look_up.Case3.PL;
Controller.ThrustLookUp.Case3.Alt       = Inv_Thrust_Look_up.Case3.Alt;
Controller.ThrustLookUp.Case3.Thrust    = Inv_Thrust_Look_up.Case3.Thr;

Controller.ThrustLookUp.Case4.Bleed     = Inv_Thrust_Look_up.Case4.Bleed;
Controller.ThrustLookUp.Case4.Ma        = Inv_Thrust_Look_up.Case4.Ma;
Controller.ThrustLookUp.Case4.PL        = Inv_Thrust_Look_up.Case4.PL;
Controller.ThrustLookUp.Case4.Alt       = Inv_Thrust_Look_up.Case4.Alt;
Controller.ThrustLookUp.Case4.Thrust    = Inv_Thrust_Look_up.Case4.Thr;

Controller.ThrustLookUp.Case5.Bleed     = Inv_Thrust_Look_up.Case5.Bleed;
Controller.ThrustLookUp.Case5.Ma        = Inv_Thrust_Look_up.Case5.Ma;
Controller.ThrustLookUp.Case5.PL        = Inv_Thrust_Look_up.Case5.PL;
Controller.ThrustLookUp.Case5.Alt       = Inv_Thrust_Look_up.Case5.Alt;
Controller.ThrustLookUp.Case5.Thrust    = Inv_Thrust_Look_up.Case5.Thr;

Controller.ThrustLookUp.Case6.Bleed     = Inv_Thrust_Look_up.Case6.Bleed;
Controller.ThrustLookUp.Case6.Ma        = Inv_Thrust_Look_up.Case6.Ma;
Controller.ThrustLookUp.Case6.PL        = Inv_Thrust_Look_up.Case6.PL;
Controller.ThrustLookUp.Case6.Alt       = Inv_Thrust_Look_up.Case6.Alt;
Controller.ThrustLookUp.Case6.Thrust    = Inv_Thrust_Look_up.Case6.Thr;

Controller.ThrustLookUp.Case7.Bleed     = Inv_Thrust_Look_up.Case7.Bleed;
Controller.ThrustLookUp.Case7.Ma        = Inv_Thrust_Look_up.Case7.Ma;
Controller.ThrustLookUp.Case7.PL        = Inv_Thrust_Look_up.Case7.PL;
Controller.ThrustLookUp.Case7.Alt       = Inv_Thrust_Look_up.Case7.Alt;
Controller.ThrustLookUp.Case7.Thrust    = Inv_Thrust_Look_up.Case7.Thr;

Controller.ThrustLookUp.Case8.Bleed     = Inv_Thrust_Look_up.Case8.Bleed;
Controller.ThrustLookUp.Case8.Ma        = Inv_Thrust_Look_up.Case8.Ma;
Controller.ThrustLookUp.Case8.PL        = Inv_Thrust_Look_up.Case8.PL;
Controller.ThrustLookUp.Case8.Alt       = Inv_Thrust_Look_up.Case8.Alt;
Controller.ThrustLookUp.Case8.Thrust    = Inv_Thrust_Look_up.Case8.Thr;

%% Basic Controller Unit

% Pitch Damper
Pitch_DamperPID_Gains               = [.5 0 0];
Pitch_DamperOverall                 = 1; 
Pitch_Damper                        = build_controller(Pitch_DamperPID_Gains, Pitch_DamperOverall);
Pitch_Damper.On_Off                 = 0;	% Switch Controller on/off [1/0]

% dNz Control
Nz_ControlPID_Gains                  = [1 0.0 0];
Nz_ControlOverall                    = 0; 
Nz_Control                           = build_controller(Nz_ControlPID_Gains, Nz_ControlOverall);
Nz_Control.On_Off                    = 0;	% Switch Controller on/off [1/0]

% Theta Control
Theta_Control.TD                     = 0; 
Theta_Control.TI                     = 0; 
Theta_Control.Overall_Gain           = -1.0; 
Theta_Control.KI                     = .2; 
Theta_Control.On_Off                 = 0;	% Switch Controller on/off [1/0]
Theta_Control.Ext_Elevator_Cmd       = 0;	% Switch Controller on/off [1/0]
Controller.Theta_Cmd_Type            = 1;   % 1 - Theta_Cmd = 0 | 2 - Step Input 3 - Sinus Input
Controller.Theta_Cmd_T_step          = 3;    % Control Section on/off [1/0]
Controller.Theta_Cmd                 = 10 *pi/180;    % Control Section on/off [1/0]
Controller.Theta_Cmd_Amp             = 10 *pi/180;
Controller.Theta_Cmd_Freq            = pi/5;

% Roll Attitude Hold
Roll_Att_HoldPID_Gains              = [3 0.5 0];
Roll_Att_HoldOverall                = 5; 
Roll_Att_Hold                       = build_controller(Roll_Att_HoldPID_Gains, Roll_Att_HoldOverall);
Roll_Att_Hold.On_Off                = 1;	% Switch Controller on/off [1/0]

% Airspeed Hold
Speed_HoldPID_Gains                 = [1 .3 2];
Speed_HoldOverall                   = 1; 
Speed_Hold                          = build_controller(Speed_HoldPID_Gains, Speed_HoldOverall);
Speed_Hold.On_Off                   = 0;	% Switch Controller on/off [1/0]

Basic_Controller.Pitch_Damper       = Pitch_Damper;
Basic_Controller.Nz_Control         = Nz_Control;
Basic_Controller.Theta_Control      = Theta_Control;
Basic_Controller.Roll_Att_Hold      = Roll_Att_Hold;
Basic_Controller.Speed_Hold         = Speed_Hold;

clear Pitch_Damper Nz_Control Roll_Att_Hold Speed_Hold
%% Auto-Pilot
% Airspeed Deviation
PID_VTAS.PID_Gains                  = [1 0.1 8];
PID_VTAS.Overall                    = 1*(pi/180);
PID_VTAS                            = build_controller(PID_VTAS.PID_Gains, PID_VTAS.Overall);
PID_VTAS.On_Off                     = 0;	% Switch Controller on/off [1/0]

% ILS Deviation from Glideslope
PID_dGs.PID_Gains                   = [1 2 2];
PID_dGs.Overall                     = 1; 
PID_dGs                             = build_controller(PID_dGs.PID_Gains, PID_dGs.Overall);
PID_dGs.On_Off                      = 0;	% Switch Controller on/off [1/0]

% Pitch Attitude Control
PID_Theta.PID_Gains                 = [1 0 0];
PID_Theta.Overall                   = 1; 
PID_Theta                           = build_controller(PID_Theta.PID_Gains, PID_Theta.Overall);
PID_Theta.Ctrl_Type                 = 1;
PID_Theta.Alt_Gain                  = 2;
PID_Theta.On_Off                    = 0;	% Switch Controller on/off [1/0]

% Vertical Speed
PID_dH_dot.PID_Gains                = [3 0 0];
PID_dH_dot.Overall                  = .001; 
PID_dH_dot                          = build_controller(PID_dH_dot.PID_Gains, PID_dH_dot.Overall);
PID_dH_dot.On_Off                   = 0;	% Switch Controller on/off [1/0]

% Pitch Damper
Pitch_DamperPID_Gains               = [3 0.2 0];
Pitch_DamperOverall                 = 1; 
Pitch_Damper                        = build_controller(Pitch_DamperPID_Gains, Pitch_DamperOverall);
Pitch_Damper.On_Off                 = 0;	% Switch Controller on/off [1/0]

AP.Pitch_Damper                     = Pitch_Damper;
AP.PID_VTAS                         = PID_VTAS;
AP.PID_dGs                          = PID_dGs;
AP.PID_Theta                        = PID_Theta;
AP.PID_dH_dot                       = PID_dH_dot;

clear Pitch_Damper PID_VTAS PID_dGs PID_Theta PID_dH_dot 
%% Auto Throttle
% ILS Deviation from Glideslope
PID_dGs_AThr.PID_Gains              = [4 0.1 100];
PID_dGs_AThr.Overall                = -1; %-1.1; 
PID_dGs_AThr                        = build_controller(PID_dGs_AThr.PID_Gains, PID_dGs_AThr.Overall);
PID_dGs_AThr.On_Off                 = 0;	% Switch Controller on/off [1/0]

% Theta Change Compensation
PID_Theta_AThr.PID_Gains            = [1 0 0];
PID_Theta_AThr.Overall              = .5; 
PID_Theta_AThr                      = build_controller(PID_Theta_AThr.PID_Gains, PID_Theta_AThr.Overall);
PID_Theta_AThr.On_Off               = 0;	% Switch Controller on/off [1/0]

% A300 Autothrottle Speed Hold
AThr.On_Off                         = 1;	% Switch Controller on/off [1/0]
AThr.dV_Overall                     = 1;
AThr.dax_Overall                    = 10;
AThr.Gain_Out                       = .0;

AThr.PID_dGs                        = PID_dGs_AThr;
AThr.PID_Theta                      = PID_Theta_AThr;

clear PID_dGs_AThr PID_Theta_AThr

%% Energy Based Autopilot/AThr
% Auto-Pilot
% Airspeed Deviation
PID_VTAS.PID_Gains                  = [1 0.1 8];
PID_VTAS.Overall                    = 1*(pi/180);
PID_VTAS                            = build_controller(PID_VTAS.PID_Gains, PID_VTAS.Overall);
PID_VTAS.On_Off                     = 1;	% Switch Controller on/off [1/0]

% ILS Deviation from Glideslope
PID_dGs.PID_Gains                   = [1 2 2];
PID_dGs.Overall                     = 1; 
PID_dGs                             = build_controller(PID_dGs.PID_Gains, PID_dGs.Overall);
PID_dGs.On_Off                      = 0;	% Switch Controller on/off [1/0]

% Pitch Attitude Control
PID_Theta.PID_Gains                 = [1 0 0];
PID_Theta.Overall                   = 1; 
PID_Theta                           = build_controller(PID_Theta.PID_Gains, PID_Theta.Overall);
PID_Theta.Ctrl_Type                 = 1;
PID_Theta.Alt_Gain                  = 2;
PID_Theta.On_Off                    = 0;	% Switch Controller on/off [1/0]

% Vertical Speed
PID_dH_dot.PID_Gains                = [3 0 0];
PID_dH_dot.Overall                  = .001; 
PID_dH_dot                          = build_controller(PID_dH_dot.PID_Gains, PID_dH_dot.Overall);
PID_dH_dot.On_Off                   = 0;	% Switch Controller on/off [1/0]

% Pitch Damper
Pitch_DamperPID_Gains               = [3 0.2 0];
Pitch_DamperOverall                 = 1; 
Pitch_Damper                        = build_controller(Pitch_DamperPID_Gains, Pitch_DamperOverall);
Pitch_Damper.On_Off                 = 0;	% Switch Controller on/off [1/0]

Energy.AP.Pitch_Damper                     = Pitch_Damper;
Energy.AP.PID_VTAS                         = PID_VTAS;
Energy.AP.PID_dGs                          = PID_dGs;
Energy.AP.PID_Theta                        = PID_Theta;
Energy.AP.PID_dH_dot                       = PID_dH_dot;

clear Pitch_Damper PID_VTAS PID_dGs PID_Theta PID_dH_dot 

% Auto Throttle
% ILS Deviation from Glideslope
PID_dGs_AThr.PID_Gains              = [4 0.1 100];
PID_dGs_AThr.Overall                = -1; %-1.1; 
PID_dGs_AThr                        = build_controller(PID_dGs_AThr.PID_Gains, PID_dGs_AThr.Overall);
PID_dGs_AThr.On_Off                 = 1;	% Switch Controller on/off [1/0]

% Theta Change Compensation
PID_Theta_AThr.PID_Gains            = [1 0 0];
PID_Theta_AThr.Overall              = .5; 
PID_Theta_AThr                      = build_controller(PID_Theta_AThr.PID_Gains, PID_Theta_AThr.Overall);
PID_Theta_AThr.On_Off               = 0;	% Switch Controller on/off [1/0]

% A300 Autothrottle Speed Hold
AThr.On_Off                         = 1;	% Switch Controller on/off [1/0]
AThr.dV_Overall                     = 1;
AThr.dax_Overall                    = 10;
AThr.Gain_Out                       = .0;

%% Total Energy Control System TECS
% Throttle Gains
Setting                             = 2;
TECS.Setting                        = Setting;
TECS.KF                             = 1;
TECS.TwoMinusKF                     = 2-TECS.KF;
KTP                                 = [1.0    1.0   .5];
TECS.KTP                            = KTP(Setting);
KTI                                 = [.5     .5    .1];
TECS.KTI                            = KTI(Setting);
KThrottle                           = [.3     .3    .5];
TECS.KThrottle                      = KThrottle(Setting);

% Elevator Gains
KEP                                 = [3    3    1.5];
TECS.KEP                            = KEP(Setting);
KEI                                 = [.4  .4   .1];
TECS.KEI                            = KEI(Setting);
KElevator                           = [.6  .3   .01]; %.05;
TECS.KElevator                      = KElevator(Setting); %.05;
TECS.KTheta                         = 0; %-1;
TECS.Kq                             = 0; %1;

% Pre-Filtering
Kh_gammaE                           = [.05 .05 .01];
Energy.Kh_gammaE                    = Kh_gammaE(Setting); % .06;            % Create artificial gamma_Error by (K_H*dH_G/S-h_dot)/V_dot
Th_gammaE                           = [2.5 2.5 10];
Energy.Th_gammaE                    = Th_gammaE(Setting);             % LoPass timeconstant for dH filtering 

KV_VE                               = [.1 .1 .02];
Energy.KV_VE                        = KV_VE(Setting);             % Create artificial V_dot/g_Error by (K_V*(V_Appr-V_IAS))-V_dot)
KV_Compl                            = [20 20 40];
Energy.KV_Compl                     = KV_Compl(Setting);
Energy.TECSOnOff                    = 1;
Energy.TECS_Elevator_Int            = 0;
Energy.TECS_Thrust_Int              = BACM_Trim.Init.Y0(11);    

Energy.AThr.PID_dGs                 = PID_dGs_AThr;
Energy.AThr.PID_Theta               = PID_Theta_AThr;
Energy.TECS                         = TECS;

clear PID_dGs_AThr PID_Theta_AThr

%% Output
Controller.Basic_Controller         = Basic_Controller;
Controller.AP                       = AP;
Controller.AThr                     = AThr;
Controller.Energy                   = Energy;
end