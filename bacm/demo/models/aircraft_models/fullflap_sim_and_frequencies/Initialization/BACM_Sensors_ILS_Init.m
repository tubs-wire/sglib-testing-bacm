%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2010      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Sensors_ILS_Init                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the workspace variables for the ILS sensors.                                 *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Sensors_ILS_Init                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* �zg�r Atesoglu               * 10-MAR-10 * ILS Glideline and Sensor Model                          *
%* �zg�r Atesoglu               * 12-NOV-10 * ILS.Gs.R  value is changed to 15 nm as default          *
%******************************************************************************************************

function ILS = BACM_Sensors_ILS_Init
% unit conversion values
ft2m    = 0.3048;                       % feet to meter 
nm2m    = 1852;                         % nm to meter
deg2rad = 1/180*pi;                     % deg to rad
ILS.Conversions.ft2m    = ft2m;
ILS.Conversions.nm2m    = nm2m;
ILS.Conversions.deg2rad = deg2rad;
ILS.Loc.uA_2_DDM        = 0.155/150;    % uA to DDM for Loc
ILS.Gs.uA_2_DDM         = 0.175/150;    % uA to DDM for Gs
ILS.eps                 = 1E-10;        % epsilon
% Notes:
% Landing reference frame (LRF) origin is the center of runway end.
% LRF x-coord. direction is along the runway azimuth.
% All position coordinates are defined in LRF.
% The numerical data is specific to Braunschweig Airport - Runway 26
% If Cat ~= 1 (or 2 or 3) then following parameters can be adjusted
%    ILS.Loc.dy    (localizer lateral displacement in m)
%    ILS.Loc.sig   (2sigma noise on localizer beam in DDM)                                                       
%    ILS.Gs.sig    (2sigma noise on glideslope beam DDM)
%    ILS.noise.tau (time const. of glideline noise shaping filters in sec)

ILS.Cat         =  1;                                   % ILS site category                         [-]

ILS.Rw.L        =  1198.45074146533;                    % runway length                             [m]
ILS.Rw.W        =  30;                                  % runway width                              [m]
ILS.Rw.psi      =  1.45820801750469;                    % runway azimuth (inv. of app. direction) [rad]
ILS.Rw.N        =  5797607.58584385;                    % runway end northing in UTM coordinates    [m]
ILS.Rw.E        =  605409.227290106;                    % runway end easting in UTM coordinates     [m]
ILS.Rw.h        =  83.22;                               % runway end altitude                       [m]
ILS.Rw.thrx     =  ILS.Rw.L;                            % runway threshold x-coord.                 [m]
ILS.Rw.thrz     =  88.830;                              % runway threshold z-coord.                 [m]
ILS.Rw.xA       =  8522.15776096967;                    % point A (usually OM)                      [m]
ILS.Rw.xB       =  2333.04355412844;                    % point B (usually MM)                      [m]
ILS.Rw.xC       =  1198.45074146533;                    % point C (usually THR)                     [m]
ILS.Rw.xD       =  871.600539247516;                    % point D (shall be between C and E)        [m]
ILS.Rw.xE       =  217.900134811879;                    % point E (shall be between D and END)      [m]
ILS.Rw.Dir      =  86.0*deg2rad;                        % runway direction true north             [rad]
% the information above is not so accurate 
%(lacks more decimal numbers) and need further
% modification for Braunschweig�s Airport
ILS.Rw.Lat      =  52.31866;                            % runway threshold latitude               [deg]
ILS.Loc.x       = -374.799996676983;                    % localizer emitter x-coord.                [m]
ILS.Loc.y       =  0;                                   % localizer emitter y-coord.                [m]
ILS.Loc.z       =  78.220;                              % localizer emitter altitude                [m]
ILS.Loc.A1      =  180*deg2rad;                         % azimuth of localizer zone 1             [rad]
ILS.Loc.A2      =  35*deg2rad;                          % azimuth of localizer zone 2             [rad]
ILS.Loc.A3      =  10*deg2rad;                          % azimuth of localizer zone 3             [rad]
ILS.Loc.R1      =  10*nm2m;                             % radius of localizer zone 1                [m]
ILS.Loc.R2      =  17*nm2m;                             % radius of localizer zone 2                [m] 
ILS.Loc.R3      =  25*nm2m;                             % radius of localizer zone 3                [m] 
ILS.Loc.dpsi    =  0.0*deg2rad;                         % additional azimuth bend on localizer    [rad]
ILS.Loc.tau     =  0.5;                                 % time constant of localizer receiver     [sec]
ILS.Loc.SF      =  00*1/100;                            % scale factor error on localizer receiver  [%]
                                                        % Note: 20 % for every Cat
ILS.Loc.dsig    =  0;                                   % add. 1sigma noise on localizer receiver  [uA] 
                                                        % Note: 4.47uA diff. for Cat3-Cat1
ILS.Loc.dy      =  0;                                   % localizer lateral displacement            [m]

% Flags used to allow errors on ILS modeling
% Set value 1 if it�s allowed or 0 if not
ILS.LocalizerLateralErrorFlag           = 1;            % sets localizer lateral error on or off    [-] 
ILS.LocalizerAzimuthErrorFlag           = 1;            % sets localizer azimuth error on or off    [-]
ILS.LocalizerSensitivityErrorFlag       = 1;            % sets localizer sensitivity error on or off[-]
ILS.LocalizerSTDFlag                    = 1;            % sets localizer std on or off              [-]
ILS.GlideslopeSensitivityFlag           = 1;            % sets glideslope sensitivity on or off     [-]
ILS.GlideslopeElevationErrorFlag        = 1;            % sets glideslope elevation error on or off [-]
ILS.GlideslopeSTDFlag                   = 1;            % sets glideslope STD on or off             [-]
ILS.nLocFlag                            = 1;            % sets nLoc on or off                       [-]
ILS.nGsFlag                             = 1;            % sets nGs on or off                        [-]
ILS.DistEnv_FreqCalcFlag                = 1;            % sets DistEnv_FreqCalc on or off           [-]
ILS.DisturbanceValueFlag                = 1;            % sets the disturbance value on or off      [-]
ILS.iLocFlag                            = 1;            % sets the iLoc on or off                   [-]
ILS.iGsFlag                             = 1;            % sets the iGs on or off                    [-]
ILS.LocErrFlag                          = 1;            % sets the Loc_err on or off                [-]
ILS.GsErrFlag                           = 1;            % sets the Gs_err on or off                 [-]

% Flag used to use radar or true altitude 
ILS.RadarFlag = 1;                                      % sets the radar altitude or true altitude  [-]
                                                        % 1 for radar altitude and 0 for true altitude                                                        

ILS.RefVel    = 0;                                      % velocity of reference for landing       [m/s]

ILS.Loc.sig     =  0*2.5*2*ILS.Loc.uA_2_DDM;            % 2sigma noise on localizer beam          [DDM]
ILS.Loc.seed    =  fix(10*sum(clock));                  % seed value for all loc. noise signals     [-]
% information above copied from ILS\BACM_Sensors_ILS_Init_FR.m
ILS.Loc.n       =  0;                                   % init. for localizer beam noise dynamics [DDM]
ILS.Loc.d       =  0;                                   % init. for localizer receiver dynamics   [DDM]
ILS.Loc.dSat    =  0.155;                               % localizer receiver saturation value     [DDM]
ILS.Loc.Filter.b(1) =  0;                               % localizer receiver filter constants for a 
ILS.Loc.Filter.b(2) =  0.0046788;                       % second order Butterworth pattern filter with
ILS.Loc.Filter.b(3) =  0.0043771;                       % wnFilt = 10 rad/s and exactly damped
ILS.Loc.Filter.a(1) =  1;
ILS.Loc.Filter.a(2) = -1.80970;
ILS.Loc.Filter.a(3) =  0.81873;

ILS.Gs.x        =  887.525681777729;                    % glideslope emitter x-coord.               [m]
ILS.Gs.y        = -118.362842670370;                    % slope emitter y-coord.   

Faf             =  1;                                   % check the validity of Faf information     [-]
if Faf == 1
% the following values match (~50 ft) height at Thr
% and the altitiude at FAF  - 2500 ft 
ILS.Gs.z        =  103.80-18.7253839819068;             % glideline hyperboloid base altitude       [m] 
ILS.Gs.theta    =  (3.48883572047327)*deg2rad;          % glideslope angle    
ILS.Gs.R        =  15*nm2m; %11102.4937283383;          % radius of glideslope zone                 [m]
                                                        % Note: the distance from GS antenna to
                                                        % LERDI(FAF) point is 11102.4937283383 m
else
% the following values match (~50 ft) height at Thr
% and the altitiude at FAF is not considered
ILS.Gs.z        =  103.80-18.7253589532623;             % glideline hyperboloid base altitude       [m] 
ILS.Gs.theta    =  (3.5)*deg2rad;                       % slope angle    
ILS.Gs.R        =  15*nm2m;                             % the distance from GS antenna for Gs zone  [m]
end

ILS.Gs.A        =  3*deg2rad;                           % azimuth of slope zone                   [rad]
ILS.Gs.dtheta   =  0.0*deg2rad;                         % additional elevation bend on glideslope [rad] 
ILS.Gs.dphi     =  0.0*deg2rad;                         % additional roll bend on glideslope      [rad]
ILS.Gs.tau      =  0.5;                                 % time constant of glideslope receiver    [sec]
ILS.Gs.SF       =  00*1/100;                            % scale factor error on receiver            [%]
                                                        % Note: 20 % for every Cat
ILS.Gs.dsig     =  0;                                   % add. 1sigma noise on glideslope receiver [uA] 
                                                        % Note: 3.00uA diff. for Cat3-Cat1
ILS.Gs.sig      =  0*10*2*ILS.Gs.uA_2_DDM;              % 2sigma noise on glideslope beam         [DDM]
ILS.Gs.seed     =  fix(11*sum(clock));                  % seed value for all gs. noise signals      [-]
% information above copied from ILS\BACM_Sensors_ILS_Init_FR.m
ILS.Gs.n        =  0;                                   % init. for glideslope beam noise dynamics[DDM]
ILS.Gs.d        =  0;                                   % init. for glideslope receiver dynamics  [DDM]
ILS.Gs.dSat     =  0.175;                               % glideslope receiver saturation value    [DDM]
ILS.Gs.Filter.b(1) =  0;                                % glideslope receiver filter constants for a 
ILS.Gs.Filter.b(2) =  0.0046788;                        % second order Butterworth pattern filter with
ILS.Gs.Filter.b(3) =  0.0043771;                        % wnFilt = 10 rad/s and exactly damped
ILS.Gs.Filter.a(1) =  1;
ILS.Gs.Filter.a(2) = -1.80970;
ILS.Gs.Filter.a(3) =  0.81873;

ILS.noise.tau   =  0.5;                                 % time const. of glideline noise filters  [sec]

ILS.Loc.oF.On   =   0;                                  % overflying A/C dist. 1/0 -> On/Off        [-] 
% information above copied from ILS\BACM_Sensors_ILS_Init_FR.m
ILS.Loc.oF.tOn  = 180;                                  % starting time of overflying A/C dist.   [sec] 
% information above copied from ILS\BACM_Sensors_ILS_Init_FR.m
ILS.Loc.oF.t    =  60;                                  % overflying A/C dist. time               [sec]
ILS.Loc.oF.h    =  1500;                                % overflying A/C altitude                   [m]
ILS.Loc.oF.f    =  5;                                   % overflying A/C dist. initial frequency   [Hz]
ILS.Loc.oF.f0   =  0;                                   % init. for overflying A/C frequency       [Hz]
ILS.Loc.oF.t0   =  0;                                   % init. for overflying A/C time           [sec]
ILS.Loc.oF.h1V  =  [10 20 30]*ft2m;                     % altitude vector for look-up table         [m]
ILS.Loc.oF.h2V  =  [4500 9500]*ft2m;                    % oF. A/C altitude vector for look-up table [m]
ILS.Loc.oF.h1h2 =  [100 10;80 08;35 05];                % oF. A/C disturbance look-up table        [uA]
ILS.Loc.oF.fV   =  [2:1:20];                            % cubic spline coeff.for 2-20 Hz freqs.    [Hz]
ILS.Loc.oF.fC   =  [2            -9           12          -4
                    0.25         -1.5         2.25         0
                    0.074074     -0.55556     0.88889      0.59259
                    0.03125      -0.28125     0.46875      0.78125
                    0.016        -0.168       0.288        0.864
                    0.0092593    -0.11111     0.19444      0.90741
                    0.0058309    -0.078717    0.13994      0.93294
                    0.0039062    -0.058594    0.10547      0.94922
                    0.0027435    -0.045267    0.082305     0.96022
                    0.002        -0.036       0.066        0.968
                    0.0015026    -0.029301    0.054095     0.9737
                    0.0011574    -0.024306    0.045139     0.97801
                    0.00091033   -0.020482    0.038234     0.98134
                    0.00072886   -0.017493    0.032799     0.98397
                    0.00059259   -0.015111    0.028444     0.98607
                    0.00048828   -0.013184    0.024902     0.98779
                    0.00040708   -0.011602    0.021982     0.98921
                    0.00034294   -0.010288    0.019547     0.9904
                    0.00029159   -0.009185    0.017495     0.9914];

% Data Assembly
Sensors.ILS = ILS;
%---------------------------------------------------------------------------------------------------EOF