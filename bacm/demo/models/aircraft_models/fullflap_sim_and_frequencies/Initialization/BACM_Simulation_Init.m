%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Simulation_Init                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the general simulation parameters.                                          *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Simulation_Init                                                                      *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************

if(TrimSettings.SimMode == 2)
	% Call Initialization Routines
	BACM.Configuration  =   BACM_Configuration_Init;       % Aircraft configuration
	BACM.Aerodynamics   =   BACM_Aerodynamics_Init;        % Aerodynamic tables
	BACM.Propulsion     =   BACM_Propulsion_Init;          % Propulsion data
	BACM.Gear           =   BACM_Gear_Init;                % Landing gear data
	BACM.Sensors        =   BACM_Sensors_Init;             % Sensor/Instrument/ILS data
	BACM.Environment    =   BACM_Environment_Init(BACM);         % Environment constants
	BACM.Actuator       =   BACM_Actuator_Init;            % Actuator data
    BACM.Tail_Bumper    =   BACM_Tail_Bumper_Init;         % Tail Bumper chracteristics
	BACM.Sim            =   BACM_Sim_Init;                 % Simulation init
	BACM.NavData        =   BACM_NavData_Init;             % Navigation Data VOR/DME Init

	BACM.Sim.Trim_Flag  =  1;                              % Set Trim Flag to ON

	BACM_Trim.Weight_Balance.DynLoad = BACM_DynamicLoad_Init;

	% Release Control Vector Limits
	BACM.Actuator.Surfaces.Aileron.max    =  90 * pi/180;  % Max. Aileron deflection angle        [rad]
	BACM.Actuator.Surfaces.Aileron.min    = -90 * pi/180;  % Min. Aileron deflection angle        [rad]
   
	BACM.Actuator.Surfaces.Elevator.max   =  90 * pi/180;  % Max. Elevator deflection angle       [rad]
	BACM.Actuator.Surfaces.Elevator.min   = -90 * pi/180;  % Min. Elevator deflection angle       [rad]

	BACM.Actuator.Surfaces.Stabilizer.max =  90 * pi/180;  % Max. Stabilizer deflection angle     [rad]
	BACM.Actuator.Surfaces.Stabilizer.min = -90 * pi/180;  % Min. Stabilizer deflection angle     [rad]

	BACM.Actuator.Surfaces.Rudder.max     =  90 * pi/180;  % Max. Rudder deflection angle         [rad]
	BACM.Actuator.Surfaces.Rudder.min     = -90 * pi/180;  % Min. Rudder deflection angle         [rad]

	BACM.Propulsion.Throttle.Max          =  inf;          % Set throttle limits to inf.            [-]
	BACM.Propulsion.Throttle.Min          = -inf;          % Set throttle limits to inf.            [-]

	% Set Dummy Control Vector Values
	BACM_Trim.U0.Throttle             = 0;                           % [-]
	BACM_Trim.U0.Aileron              = 0;                           % [rad]
	BACM_Trim.U0.Elevator             = 0;                           % [rad]
	BACM_Trim.U0.Rudder               = 0;                           % [rad]
	BACM_Trim.U0.Flaps                = 0;                           % [rad]
	BACM_Trim.U0.Stabilizer           = 0;                           % [rad]
	BACM_Trim.U0.Speed_Brake          = 0;                           % [-]
	BACM_Trim.U0.Ramp                 = 0;                           % [-]
	BACM_Trim.U0.NG_Steering          = 0;                           % [rad]
	BACM_Trim.U0.Parking_Brake        = 0;                           % [-]
	BACM_Trim.U0.Gear                 = 0;                           % [-]

	% Set Dummy State Vector Values
	% Translation
	BACM_Trim.Rigid_Body_Init.Translation.u_K_b = 77;                % [m/s]
	BACM_Trim.Rigid_Body_Init.Translation.v_K_b = 0;                 % [m/s]
	BACM_Trim.Rigid_Body_Init.Translation.w_K_b = 0;                 % [m/s]

	% Rotation
	BACM_Trim.Rigid_Body_Init.Rotation.p_K_b = 0;                    % [m/s]
	BACM_Trim.Rigid_Body_Init.Rotation.q_K_b = 0;                    % [m/s]
	BACM_Trim.Rigid_Body_Init.Rotation.r_K_b = 0;                    % [m/s]

	% Attitude
	BACM_Trim.Rigid_Body_Init.Attitude.phi   = 0;                    % [rad]
	BACM_Trim.Rigid_Body_Init.Attitude.theta = 0;                    % [rad]
	BACM_Trim.Rigid_Body_Init.Attitude.psi   = 0;                    % [rad]
	% Attitude for quaternions
	BACM_Trim.Rigid_Body_Init.Attitude.q_0   = 1;                    % [-]
	BACM_Trim.Rigid_Body_Init.Attitude.q_1   = 0;                    % [-]
	BACM_Trim.Rigid_Body_Init.Attitude.q_2   = 0;                    % [-]
	BACM_Trim.Rigid_Body_Init.Attitude.q_3   = 0;                    % [-]

	% Position
	BACM_Trim.Rigid_Body_Init.Position.x_o = 0;                      % [m]
	BACM_Trim.Rigid_Body_Init.Position.y_o = 0;                      % [m]
	BACM_Trim.Rigid_Body_Init.Position.z_o = -1000;                  % [m]
    % Position for new navigation equations
    BACM_Trim.Rigid_Body_Init.Position.h    = 1000;                  % [m]
    BACM_Trim.Rigid_Body_Init.Position.lat  = 50 * pi/180;           % [rad]
    BACM_Trim.Rigid_Body_Init.Position.long = 0;                     % [rad]

	% Propulsion
	BACM_Trim.Propulsion.LH_Outer_Throttle_Init     = 0;             % [-] 
	BACM_Trim.Propulsion.LH_Inner_Throttle_Init     = 0;             % [-] 
	BACM_Trim.Propulsion.RH_Outer_Throttle_Init     = 0;             % [-] 
	BACM_Trim.Propulsion.RH_Inner_Throttle_Init     = 0;             % [-]
	BACM_Trim.Propulsion.LH_Outer_Throttle_Init_dot = 0;             % [-/s] 
	BACM_Trim.Propulsion.LH_Inner_Throttle_Init_dot = 0;             % [-/s] 
	BACM_Trim.Propulsion.RH_Outer_Throttle_Init_dot = 0;             % [-/s] 
	BACM_Trim.Propulsion.RH_Inner_Throttle_Init_dot = 0;             % [-/s]
	BACM_Trim.Propulsion.Fuel_Mass_Consumed         = 0;             % [kg]

	% Booster Actuator Dummys
	BACM_Trim.Actuator.Booster.Aileron        = 0;                   % [rad]
	BACM_Trim.Actuator.Booster.Aileron_Dot    = 0;                   % [rad/s]
	BACM_Trim.Actuator.Booster.Rudder         = 0;                   % [rad]
	BACM_Trim.Actuator.Booster.Rudder_Dot     = 0;                   % [rad/s]
	BACM_Trim.Actuator.Booster.Elevator       = 0;                   % [rad]
	BACM_Trim.Actuator.Booster.Elevator_Dot   = 0;                   % [rad/s]
	BACM_Trim.Actuator.Booster.Stabilizer     = 0;                   % [rad]
	BACM_Trim.Actuator.Booster.Stabilizer_Dot = 0;                   % [rad/s]
	BACM_Trim.Actuator.Spoiler_Init           = 0;                   % [rad]
	BACM_Trim.Actuator.Speed_Brake_Init       = 0;                   % [-]
	BACM_Trim.Actuator.Flaps_Init             = 0;                   % [rad]
	BACM_Trim.Actuator.Gear_Init              = 0;                   % [-]
	BACM_Trim.Actuator.Ramp_Init              = 0;                   % [-]
	BACM_Trim.Actuator.NG_Eta_Boost           = 0;                   % [rad]
	BACM_Trim.Actuator.NG_Eta_Boost_dot       = 0;                   % [rad/s]
	BACM_Trim.Actuator.BLC                    = 0;                   % [rad]

	% Sensors
	BACM_Trim.Sensors.Temperature             = 288.15;              % [K]
	BACM_Trim.Sensors.alpha                   = 0;                   % [rad]
	BACM_Trim.Sensors.alpha_dot               = 0;                   % [rad/s]
	BACM_Trim.Sensors.beta                    = 0;                   % [rad]
	BACM_Trim.Sensors.beta_dot                = 0;                   % [rad/s]
    BACM_Trim.Sensors.nLoc                    = 0;                   % [DDM]
    BACM_Trim.Sensors.nGs                     = 0;                   % [DDM]
    BACM_Trim.Sensors.oF_frequency            = 0;                   % [-/s]
    BACM_Trim.Sensors.oF_time                 = 0;                   % [s]
    BACM_Trim.Sensors.dLoc_receive            = 0;                   % [DDM]
    BACM_Trim.Sensors.dGs_receive             = 0;                   % [DDM]

	% Assign variable structure to workspace
	assignin('caller', 'BACM', BACM);
	assignin('caller', 'BACM_Trim', BACM_Trim);
	
elseif(TrimSettings.SimMode < 5)

	% Call Initialization Routines
	BACM.Configuration  =   BACM_Configuration_Init;       % Aircraft configuration
	BACM.Aerodynamics   =   BACM_Aerodynamics_Init;        % Aerodynamic tables
	BACM.Propulsion     =   BACM_Propulsion_Init;          % Propulsion data
	BACM.Gear           =   BACM_Gear_Init;                % Landing gear data
	BACM.Sensors        =   BACM_Sensors_Init;             % Sensor/ Instrument data
	BACM.Environment    =   BACM_Environment_Init(BACM);         % Environment constants
	BACM.Actuator       =   BACM_Actuator_Init;            % Actuator data
    BACM.Tail_Bumper    =   BACM_Tail_Bumper_Init;         % Tail Bumper chracteristics
	BACM.Sim            =   BACM_Sim_Init;                 % Simulation init
	BACM.NavData        =   BACM_NavData_Init;             % Navigation Data VOR/DME Init

	BACM.Sim.Trim_Flag  = 0;                               % Set [1] for Trimming                   [-]
														   % Set [0] for Simulation                 [-] 
	if(isempty(TrimSettings.strTrimfile) == 1)
		% Ask for filename to load Trimm Values
		[FileName,PathName] = uigetfile('*.mat', 'Load Trim-Parameter File');
		TrimSettings.strTrimfile = strcat(PathName, FileName);
	end
	
	if(isempty(TrimSettings.strTrimfile) == 1)
		disp('Warnig: No file specified. Nothing loaded!');
	else
		load(TrimSettings.strTrimfile, '-mat');
	end

	assignin('caller','BACM', BACM);
	assignin('caller','BACM_Trim', BACM_Trim);                                   
end
%---------------------------------------------------------------------------------------------------EOF