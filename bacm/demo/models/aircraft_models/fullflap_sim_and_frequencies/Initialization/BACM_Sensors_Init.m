%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Sensors_Init                                             *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the workspace variables for the sensors/ instruments.                        *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Sensors_Init                                                                         *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design to BACM Modell                             *
%* Christian Raab               * 02-DEC-09 * Added position of radar sensor.                         *
%* Christian Raab               * 02-DEC-09 * Added position of wind vanes                            *
%******************************************************************************************************

function Sensors = BACM_Sensors_Init

% Radar Sensor Properties
Radar.x_rp = -20.525;               % Radar Sensor x-coord. in reference frame                      [m]
Radar.y_rp =  18.0;                 % Radar Sensor y-coord. in reference frame                      [m]
Radar.z_rp =  -2.0;                 % Radar Sensor z-coord. in reference frame                      [m]
Radar.Gain =   1;                   % Radar Sensor Gain                                             [-]
Radar.Bias =   0;                   % Radar Sensor Bias                                             [-]

% Air Data System Properties
% Pressure Sensor
Air_Data.Pressure.Static.Delay = 0.5;                      % Static Pressure Sensor Delay           [s]
Air_Data.Pressure.Static.Gain  = 1;                        % Static Pressure Sensor Gain            [-]
Air_Data.Pressure.Static.Bias  = 0;                        % Static Pressure Sensor Bias        [N/m^2]
Air_Data.Pressure.Total.Delay  = 0.5;                      % Total Pressure Sensor Delay            [s]
Air_Data.Pressure.Total.Gain   = 1;                        % Total Pressure Sensor Gain             [-]
Air_Data.Pressure.Total.Bias   = 0;                        % Total Pressure Sensor Bias         [N/m^2]    

% Temperature Sensor
Air_Data.Temperature.Gain               = 1;               % Total Temperature Sensor Gain          [-]
Air_Data.Temperature.Bias               = 0;               % Total Temperature Sensor Bias          [-]
% Look Up Table Data for the Calculation of PT-1 Time Constant
Air_Data.Temperature.Look_Up.Ma         = [0.04 0.06 0.08 0.1 0.12 0.14 0.16 0.2 0.3 0.4 0.5 0.6];% [-]                                                 
Air_Data.Temperature.Look_Up.Time_Const = [5.7 4.3 3.5 3.0 2.7 2.4 2.2 1.9 1.5 1.2 1.1 1.0];      % [s]

% Wind Vanes Properties
Air_Data.Wind_Vanes.alpha.x_rp    = -1;                    % Alpha Sensor x-coord. in ref. frame    [m]
Air_Data.Wind_Vanes.alpha.y_rp    =  2;                    % Alpha Sensor y-coord. in ref. frame    [m]
Air_Data.Wind_Vanes.alpha.z_rp    =  1;                    % Alpha Sensor z-coord. in ref. frame    [m]
Air_Data.Wind_Vanes.alpha.Gain     = 1;                    % Alpha Sensor Gain                      [-]
Air_Data.Wind_Vanes.alpha.Bias     = 0;                    % Alpha Sensor Bias                    [rad]
Air_Data.Wind_Vanes.alpha.omega_0  = 120;                  % Alpha Sensor Eigenfreq.            [rad/s]
Air_Data.Wind_Vanes.alpha.Damping  = 0.9;                  % Alpha Sensor Damping                   [-]
Air_Data.Wind_Vanes.alpha.Delay    = 0.1;                  % Alpha Sensor Time Delay                [s]
Air_Data.Wind_Vanes.alpha.max      =  80 * pi/180;         % Alpha Sensor max. deflection         [rad]
Air_Data.Wind_Vanes.alpha.rate_max =  5  * pi/180;         % Alpha Sensor max. deflection rate  [rad/s]
Air_Data.Wind_Vanes.alpha.min      = -80 * pi/180;         % Alpha Sensor max. deflection         [rad]
Air_Data.Wind_Vanes.alpha.rate_min = -5  * pi/180;         % Alpha Sensor max. deflection rate  [rad/s]

Air_Data.Wind_Vanes.beta.x_rp      = -2;                   % Beta Sensor x-coord. in ref. frame     [m]
Air_Data.Wind_Vanes.beta.y_rp      =  0;                   % Beta Sensor y-coord. in ref. frame     [m]
Air_Data.Wind_Vanes.beta.z_rp      =  2;                   % Beta Sensor z-coord. in ref. frame     [m]
Air_Data.Wind_Vanes.beta.Gain      = 1;                    % Beta Sensor Gain                       [-]
Air_Data.Wind_Vanes.beta.Bias      = 0;                    % Beta Sensor Bias                     [rad]
Air_Data.Wind_Vanes.beta.omega_0   = 120;                  % Beta Sensor Eigenfreq.             [rad/s]
Air_Data.Wind_Vanes.beta.Damping   = 0.9;                  % Beta Sensor Damping                    [-]
Air_Data.Wind_Vanes.beta.Delay     = 0.1;                  % Beta Sensor Time Delay                 [s]
Air_Data.Wind_Vanes.beta.max      =  80 * pi/180;          % Beta Sensor max. deflection          [rad]
Air_Data.Wind_Vanes.beta.rate_max =  5  * pi/180;          % Beta Sensor max. deflection rate   [rad/s]
Air_Data.Wind_Vanes.beta.min      = -80 * pi/180;          % Beta Sensor max. deflection          [rad]
Air_Data.Wind_Vanes.beta.rate_min = -5  * pi/180;          % Beta Sensor max. deflection rate   [rad/s]

% Atmospheric Properties for Reference Altitude  
Air_Data.R         = 287.05307;          % Gas Constant                                        [J/kg*K]
Air_Data.kappa     = 1.4;                % Isentropic Exponent                                      [-]
Air_Data.g_n       = 9.80665;            % Earth Acceleration at 45�32'33'' Latitude            [m/s^2]
Air_Data.T_0       = 288.15;             % Static Temperature at 0 m Geopotential Heigth            [K]
Air_Data.p_0       = 1.013250e+05;       % Static Pressure at 0 m Geopotential Heigth           [N/m^2]
Air_Data.rho_0     = 1.225000e+00;       % Static Density at 0 m Geopotential Height           [kg/m^3]
Air_Data.a_0       = sqrt(Air_Data.T_0 * Air_Data.kappa * Air_Data.R);% Speed of Sound at SL      [m/s]
Air_Data.gamma_H   = -6.500000e-03;      % Temperature Gradient for H<11km                        [K/m]

% GADIRU
% Translation
% Accelerations
GADIRU.a_x.Gain          = 1;                              % X-Direction Accelerometer Gain         [-]
GADIRU.a_x.Bias          = 0;                              % X-Direction Accelerometer Bias     [m/s^2]
GADIRU.a_y.Gain          = 1;                              % Y-Direction Accelerometer Gain         [-]
GADIRU.a_y.Bias          = 0;                              % Y-Direction Accelerometer Bias     [m/s^2]
GADIRU.a_z.Gain          = 1;                              % Z-Direction Accelerometer Gain         [-]
GADIRU.a_z.Bias          = 0;                              % Z-Direction Accelerometer Bias     [m/s^2]

% Velocity in Body Frame
GADIRU.u_K.Gain          = 1;                              % X-Direction Vel. in Body Frame Gain    [-]
GADIRU.u_K.Bias          = 0;                              % X-Direction Vel. in Body Frame Bias  [m/s]
GADIRU.v_K.Gain          = 1;                              % Y-Direction Vel. in Body Frame Gain    [-]
GADIRU.v_K.Bias          = 0;                              % Y-Direction Vel. in Body Frame Bias  [m/s]
GADIRU.w_K.Gain          = 1;                              % Z-Direction Vel. in Body Frame Gain    [-]
GADIRU.w_K.Bias          = 0;                              % Z-Direction Vel. in Body Frame Bias  [m/s]

% Velocity in North-East-Down Frame
GADIRU.u_o.Gain          = 1;                              % X-Direction Vel. in NED Frame Gain     [-]
GADIRU.u_o.Bias          = 0;                              % X-Direction Vel. in NED Frame Bias   [m/s]
GADIRU.v_o.Gain          = 1;                              % Y-Direction Vel. in NED Frame Gain     [-]
GADIRU.v_o.Bias          = 0;                              % Y-Direction Vel. in NED Frame Bias   [m/s]
GADIRU.w_o.Gain          = 1;                              % Z-Direction Vel. in NED Frame Gain     [-]
GADIRU.w_o.Bias          = 0;                              % Z-Direction Vel. in NED Frame Bias   [m/s]

% Position in North East Down Frame
GADIRU.x_o.Gain          = 1;                              % X-Position in NED Frame Gain           [-]
GADIRU.x_o.Bias          = 0;                              % X-Position in NED Frame Bias         [m/s]
GADIRU.y_o.Gain          = 1;                              % Y-Postion in NED Frame Gain            [-]
GADIRU.y_o.Bias          = 0;                              % Y-Postion in NED Frame Bias          [m/s]
GADIRU.z_o.Gain          = 1;                              % Z-Postion in NED Frame Gain            [-]
GADIRU.z_o.Bias          = 0;                              % Z-Postion in NED Frame Bias          [m/s]

% Rotation
% Body Angular Rates
GADIRU.p_K.Gain    = 1;                                    % X-Axis Angular Rate Sensor Gain        [-]
GADIRU.p_K.Bias    = 0;                                    % X-Axis Angular Rate Sensor Bias    [rad/s]
GADIRU.q_K.Gain    = 1;                                    % Y-Axis Angular Rate Sensor Gain        [-]
GADIRU.q_K.Bias    = 0;                                    % Y-Axis Angular Rate Sensor Bias    [rad/s]
GADIRU.r_K.Gain    = 1;                                    % Z-Axis Angular Rate Sensor Gain        [-]
GADIRU.r_K.Bias    = 0;                                    % Z-Axis Angular Rate Sensor Bias    [rad/s]

% Attitude Angular Rates
GADIRU.phi_dot.Gain    = 1;                                % Phi Angular Rate Gain                  [-]
GADIRU.phi_dot.Bias    = 0;                                % Phi Angular Rate Bias                [rad]
GADIRU.theta_dot.Gain  = 1;                                % Theta Angular Rate Gain                [-]
GADIRU.theta_dot.Bias  = 0;                                % Theta Angular Rate Bias              [rad]
GADIRU.psi_dot.Gain    = 1;                                % Psi Anglular Rate Gain                 [-]
GADIRU.psi_dot.Bias    = 0;                                % Psi Anglular Rate Bias               [rad]

% Attitude Angles
GADIRU.phi.Gain    = 1;                                    % Phi Angle Gain                         [-]
GADIRU.phi.Bias    = 0;                                    % Phi Angle Bias                       [rad]
GADIRU.theta.Gain  = 1;                                    % Theta Angle Gain                       [-]
GADIRU.theta.Bias  = 0;                                    % Theta Angle Bias                     [rad]
GADIRU.psi.Gain    = 1;                                    % Psi Angle Gain                         [-]
GADIRU.psi.Bias    = 0;                                    % Psi Angle Bias                       [rad]

% Magnetic Heading Sensor
GADIRU.Heading.Gain     = 1;                               % Heading Sensor Gain                    [-]
GADIRU.Heading.Variance = 0;                               % Magnetic Variation                   [rad]

% Data Assembly
Sensors.Radar    = Radar;
Sensors.Air_Data = Air_Data;
Sensors.GADIRU   = GADIRU;
Sensors.ILS      = BACM_Sensors_ILS_Init;
end
%---------------------------------------------------------------------------------------------------EOF