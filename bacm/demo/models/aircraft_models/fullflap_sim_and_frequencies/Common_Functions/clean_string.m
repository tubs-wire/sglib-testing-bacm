%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2005      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                     clean_string.m                                                 *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Helper function to extract state names from a Simulink model. Subsystem path and un-    *
%*            neccessary slashes are deleted.                                                         *
%*                                                                                                    *                           
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : cleaned_string = clean_string(string)                                                     *
%*                                                                                                    *
%* Input Parameters:                                                                                  *    
%* string = String array or cell array of strings.                                                    *
%*                                                                                                    *
%* Output Parameters:                                                                                 *
%* cleaned_string = String array or cell array being cleaned from subsystem path names and slashes.   *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 16-AUG-2005 *                   Basic Design                        *
%******************************************************************************************************

function cleaned_string = clean_string(string)

% Normal string array
if isstr (string);
    % Find slashes and unit brakets
    ind_unit_start = findstr (string, '[');
    ind_unit_end   = findstr (string, ']');
    ind_slash      = findstr (string, '/');

    if isempty (ind_slash), ind_slash = 0; end  
    
    % Check for slashes in units
    if (ind_slash(end) > ind_unit_start(end)) & (ind_slash(end) < ind_unit_end(end));
        cleaned_string = string(ind_slash(end-2)+1:end);
        % Find double slashes
        ind_double_slash = findstr(cleaned_string, '//');
        % Delete one slash in double slashes
        if isempty(ind_double_slash), ind_double_slash = 0; end
            cleaned_string = strcat(cleaned_string(1 : ind_double_slash-1), ...
                                                            cleaned_string(ind_double_slash+1 : end));
    else;
        cleaned_string = string(ind_slash(end)+1:end);
    end;
    

% Cell array of strings    
elseif iscellstr (string)
  
  % find the number of rows and columns
  [rows, columns] = size (string);
  
  % Loop over all rows
  for i_row = 1 : rows
    
    % Loop over all columns
    for i_column = 1 : columns
      
      % Extract one cell array element
        one_string = string{i_row, i_column};      
				%if(i_row >= 64)
				%	disp(one_string);
				%end
        % Find slashes and unit brakets
        ind_unit_start = findstr (one_string, '[');
        ind_unit_end   = findstr (one_string, ']');
        ind_slash      = findstr (one_string, '/');
				if isempty (ind_unit_start) 
					ind_unit_start = [0];
				end
				if isempty (ind_unit_end)
					ind_unit_end = [0];
				end

				if isempty (ind_slash), ind_slash = 0; end  
            % Check for slashes in units
            if (ind_slash(end) > ind_unit_start(end)) & (ind_slash(end) < ind_unit_end(end));
                cleaned_onestring = one_string(ind_slash(end-2)+1:end); 
                % Find double slashes
                ind_double_slash = findstr(cleaned_onestring, '//');
                % Delete one slash in double slashes
                if isempty(ind_double_slash), ind_double_slash = 0; end
                cleaned_onestring = strcat(cleaned_onestring(1 : ind_double_slash-1),...
                                                          cleaned_onestring(ind_double_slash+1 : end));
        else;
            cleaned_onestring = one_string(ind_slash(end)+1:end);
        end;
        
        cleaned_string{i_row, i_column} = cleaned_onestring;
				%disp(['cleaned: ', cleaned_onestring]);

		end;
    
  end;
  
% If the input argument is neither a string array, 
% nor a (two-dimensional) cell array (of strings),
else
  
  % display an error message
  error ('Input argument has to be a string array or a cell array of strings')
  
end
%---------------------------------------------------------------------------------------------------EOF