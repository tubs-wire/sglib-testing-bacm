function [state,polysys,time] = highlift_eigen_init( varargin )
%HIGHLIFT_EIGEN_INIT Initialises the structure that keeps the internal
% state of the aircraft simulation
%
% Physycal meaning of the system:
% Simulation of Longitudinal motion of CESTOL aircraft
%
%
%===================================================================
% Full BACM simulation model - description
%===================================================================




% initial values and parameters
% .................................


% list of parameters
%
%
% alpha_wfc_max:    maximal wfc (wing/fuselage) angle of attack (not included yet)


% C_mu:             jet momentum coefficient (only for full flap config)
% V_TAS:            initial true air velocity
% C_L0:             lift coefficient at alpha=0
% C_Lalpha:         slope of the lift coeff curve
% C_D0:             drag coeff at alpha=0
% k1:               coeff to calculate the drag coeff (C_D)
% k2:               coeff to calculate the drag coeff (C_D)
% C_m0:             pitching moment coefficient for zero angle of attack
% C_malpha:         Pitching moment slope of the curve
% additional coeffs should be added
%
% Example:
%    state =Full_BACM_init('list_of_RVs',{'C_L0','V_TAS'}, 'C_L0',{3, 1, 'H'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)



%Mean_values= [0.045, 0.344540120967742,  2.807674154122847, -0.444446500925589, 4.734449604654664, -1.236767277669634,  0.310327867813788,   0.020882442004759,  0.050609658549986, 0.226286258235210, 0.010465923328436,  -0.010465923328436,   -0.131599412034937, 0.035063886442427, 0.006179345655616,   0.020036923921946,     -0.119375806451613, -0.584539227935720, 0.026811557229833, 0.986319120553432, -0.339182978441908, 0.270784598950292, ];
%list_of_params={'Cmu', 'C_L0',  'C_L0_FL',  'C_L0_DL', 'C_Lalpha',
%'C_Lalpha_FL',  'C_Lalpha_DN', 'C_D0',  'C_D0_FL',  'C_D0_DN', 'k1', 'k1_FL', 'k1_DN', 'k2', 'k2_FL', 'k2_DN', 'C_m0' ,   'C_m0_FL' ,  'C_m0_DN' , 'C_malpha', 'C_malpha_FL', 'C_malpha_DN'} ;


options = varargin2options(varargin);
%[list_of_RVs, options] =    get_option(options, 'list_of_RVs', {'Cmu',  'C_L0', 'C_Lalpha',   'C_Lalpha_FL',  'C_Lalpha_DN', 'C_D0',  'k1',    'k1_FL',  'k1_DN',  'k2',  'k2_FL', 'k2_DN', 'C_m0' , 'C_malpha', 'C_malpha_FL',  'C_malpha_DN'});
[list_of_RVs, options] =    get_option(options, 'list_of_RVs',{'C_Lalpha'});
[t_min_max, options] = get_option(options, 't_min_max', [0,20]);
check_unsupported_options(options, mfilename);


%Input of examined time interval
tmin=t_min_max(1);
tmax=t_min_max(2);
% Define uncertain parameters
%Mean_values= [0.045, 0.344540120967742, 4.734449604654664, -1.236767277669634,  0.310327867813788,   0.020882442004759,  0.010465923328436,  -0.010465923328436,   -0.131599412034937, 0.035063886442427, 0.006179345655616,   0.020036923921946,     -0.119375806451613,   0.986319120553432, -0.339182978441908, 0.270784598950292, ];

Mean_values= [0.033, 0.344540120967742, 4.734449604654664, -1.236767277669634,  0.310327867813788,   0.020882442004759,  0.010465923328436,  -0.010465923328436,   -0.131599412034937, 0.035063886442427, 0.006179345655616,   0.020036923921946,     -0.119375806451613,   0.986319120553432, -0.339182978441908, 0.270784598950292, ];
Deviations = [Mean_values(1)*0.05, 0.15, Mean_values(3:5)*0.05, 0.05, Mean_values(7:12)*0.1, 0.15, Mean_values(14:16)*0.05];
list_of_params={'Cmu',  'C_L0', 'C_Lalpha',   'C_Lalpha_FL',  'C_Lalpha_DN', 'C_D0',  'k1',    'k1_FL',  'k1_DN',  'k2',  'k2_FL', 'k2_DN', 'C_m0' , 'C_malpha', 'C_malpha_FL',  'C_malpha_DN'} ;
Left_values=Mean_values-Deviations;
Right_values=Mean_values+Deviations;
for i=1:length(list_of_params)
    params.(list_of_params{i})={Left_values(i), Right_values(i), 'P'};
end
%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();


%list_of_init_vals={'C_L0'};
num_tot_params=length(list_of_params);
num_RVs=length(list_of_RVs);


%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs); 
% for i=1:length(list_of_params)
%    eval([ list_of_params{i} '=' 'state.ref_params.(list_of_params{i});' ]);
% end 


%% Run reference(deterministic) solution
%integration

[t_span, State_ref]=run_highlift_sim(state.ref_params, tmin, tmax);
% problem_size=(tmax-tmin)/0.01+1;
problem_size=length(t_span);
state.num_eqs=[5, 10];
%dt_span=diff(t_span);
Ref_sol_t= {State_ref{1,1}(1:state.num_eqs(1)*problem_size)};
Ref_sol_freq={State_ref{1,1}(state.num_eqs(1)*problem_size+1:end)};
%state.list_of_init_vals=list_of_init_vals;
state.ref_sol=reshape(Ref_sol_t{1,1},size(t_span,1),[]);
state.ref_freq=Ref_sol_freq;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_params = num_RVs;   %number of RV
state.num_vars=problem_size;
state.num_totRV_out= state.num_eqs(1)*problem_size+state.num_eqs(2);
%state.num_eqs=size(State_ref,2);
state.t_min_max=t_min_max;
time=t_span;

end

