function [sol, model] = model_solve_s(model, params, varargin)
% MODEL_SOLVE Short description of model_solve.
%   MODEL_SOLVE Long description of model_solve.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example model_solve">run</a>)
%
% See also

%   Elmar Zander (Noemi Friedman: saving f_name (optional saving) is added)
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[saving_fname, options] = get_option(options, 'saving_fname', '');
[solve_options, options] = get_option(options, 'solve_options',{});
check_unsupported_options(options, mfilename);

solve_func = model.model_info.solve_func;
start = tic;
[sol, solve_info, model]=funcall(solve_func, model, params, solve_options);
t = toc(start);
model.model_stats.num_solve_calls = model.model_stats.num_solve_calls + 1;
model.model_stats.time_solve_calls = model.model_stats.time_solve_calls + t;
model.solve_info = solve_info;

if nargout<2
    warning('sglib:model_solve', 'Not updating model info, stats will possibly be wrong');
end
%% Save SOL and SOL_INFO when SAVING_FNAME is defined
if ~isempty(saving_fname)
    file_name=strvarexpand('$saving_fname$_$model.model_stats.num_solve_calls$.mat');
    save(file_name, 'sol', 'model')
end