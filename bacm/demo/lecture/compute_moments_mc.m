function [u_mean, u_var, time, ref_sol, varargout] = compute_moments_mc(init_func, solve_func, N, saving_path, init_options, varargin)
% COMPUTE_MOMENTS_MC Compute mean and variance by Monte-Carlo.
%   [U_MEAN, U_VAR] = COMPUTE_MOMENTS_MC(INIT_FUNC, SOLVE_FUNC, N) computes
%   the mean and variance of a system described by INIT_FUNC and SOLVE_FUNC
%   by a Monte-Carlo method with N samples. The distribution is specified
%   by POLYSYS (see <a href="matlab:help gpc">GPC</a>).
%
% Example (<a href="matlab:run_example compute_moments_mc">run</a>)
%
% See also GPC

%   Elmar Zander
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%%
options=varargin2options(varargin);
[mode,options]=get_option(options, 'mode', '');
[N_per,options]=get_option(options, 'N_per', 250);    %saving result after each N_perth sample
[saving_all_samples, options]=get_option(options, 'saving_all_samples', 0);
check_unsupported_options(options, mfilename);

%%
sample_options=struct;
if ~isempty(mode)
    sample_options.mode = mode;
end


[state,polysys,time] = funcall(init_func, init_options);
n_t=(length(state.t_span));
n_eq=state.num_eqs(1);
n_prod=n_t*n_eq;
if ~isscalar(state.num_eqs)
    n_eq2=state.num_eqs(2);
    n_prod=n_t*n_eq+n_eq2;
end

V = gpcbasis_create(polysys, 'm', state.num_params);
x = gpcgerm_sample(V, N, sample_options);

%definition of file- and foldername for saving results
if ~isempty(saving_path)
equation_name=strrep(char(init_func),'init','_');
file_name=get_saving_filenames(saving_path,equation_name,'MC',{N, mode, N_per});
end

%% Initialize memory for convergence vector (with means/vars after certain
%time)

if mod(N, N_per)==0
    n_conv=N/N_per;  %convergence is checked at N_CONV number of points
else
    if N_per<N
        n_conv=floor(N/N_per+1);
    else
        n_conv=1;
    end
end


u_conv_test_mean=zeros(n_prod, n_conv);
u_conv_test_var=zeros(n_prod, n_conv);
if saving_all_samples
   u_samples= repmat({zeros(n_t, N_per)},[n_eq,1]); %time dependent samples
  if ~isscalar(state.num_eqs)
    u_samples1= zeros(n_eq2, N_per); %not time dependent samples
  end
end    
    
u_mean = [];
u_var = [];
ind_conv=1;
%% 
for j = 1:N
    j
    x_j = x(:,j);
    u_j = funcall(solve_func, state, x_j);
            
    [u_mean, u_var] = mean_var_update(j, u_j, u_mean, u_var);
    
    % saving all the solution samples
    if saving_all_samples %write sample to u_samples if all samples are to be saved
        if mod(j,N_per)==0
                jth=N_per;
            else
                jth=mod(j,N_per);
        end
            
        for i_var=1:n_eq
            fromi=(i_var-1) *n_t+1;
            toi=i_var*n_t;
            u_samples{i_var,1} (: , jth ) = u_j(fromi:toi);
        end
        
        if ~isscalar(state.num_eqs)
           u_samples1(:,jth)= u_j((n_prod-n_eq2):n_prod); %not time dependent samples
        end
    end
      
    %% saving mat file for convergence checking /save all samples if needed
    if mod(j,N_per)==0 || j==N
        u_conv_test_mean(:,ind_conv)=u_mean;
        u_conv_test_var(:,ind_conv)=u_var;
        if ~isempty(saving_path)
            if j==N %final saving
                file_name_i=strrep(file_name{end},'_all_samples','');
                save(file_name_i,'u_mean','u_var','u_conv_test_mean', 'u_conv_test_var','polysys','state', 'init_options');
                if saving_all_samples
                  file_name_i = file_name(end);
                  if isscalar(state.num_eqs)
                       save(file_name_i,'u_samples','N','polysys','state');
                  else
                       save(file_name_i,'u_samples','u_samples1','N','polysys','state');
                  end
                end      
            else  %in between saving
                file_index = floor(j/N_per);
                file_name_i=file_name(file_index);
                save(file_name_i,'u_mean','u_var','u_conv_test_mean', 'u_conv_test_var');
                if saving_all_samples
                  file_name_i=strrep(file_name(file_index),'all_samples','');
                  if isscalar(state.num_eqs)
                      save(file_name_i,'u_samples','N'); 
                  else
                      save(file_name_i,'u_samples','u_samples1', 'N'); 
                  end
                  save(file_name_i,'u_samples','N'); 
                  u_samples= repmat({zeros(n_t, N_per)},[n_eq,1]);
                  u_samples1= zeros(n_eq2, N_per);
                end 
            end            
        end
        ind_conv=ind_conv+1;
    end
 
end

%Check convergence of the norm
norm_u_varconv=zeros(size(u_conv_test_mean,2));
norm_u_meanconv=zeros(size(u_conv_test_mean,2));
for i=1:size(u_conv_test_mean,2)
   norm_u_varconv(i)=norm(u_conv_test_var(:,i)-u_var)/(norm(u_var));
   norm_u_meanconv(i)=norm(u_conv_test_mean(:,i)-u_mean)/norm(u_mean);
end

%   plot(norm_u_varconv)
 %  figure
 %  plot(norm_u_meanconv)
    


%put different variables in u_mean in different columns
if isscalar(state.num_eqs)
    u_mean=reshape(u_mean,state.num_vars,n_eq);
    u_var=reshape(u_var,state.num_vars,n_eq);
    ref_sol=reshape(state.ref_sol,state.num_vars,n_eq);
else
    u_mean_intemp=u_mean(n_t*n_eq(1)+1:end);
    u_var_intemp=u_var(n_t*n_eq(1)+1:end);
    u_mean=reshape(u_mean(1:n_t*n_eq(1)), n_t,n_eq(1));
    u_var=reshape(u_var(1:n_t*n_eq(1)),n_t,n_eq(1));
    
end

 if nargout>0
     varargout{1}=u_mean;
     varargout{2}=u_var;
     if nargout==3
         varargout{3}=ref_sol;
     end
 end

% if nargout>0
%     varargout{1}=u_mean_intemp;
%     varargout{2}=u_var_intemp;
%     if nargout==3
%         varargout{3}={norm_u_varconv; norm_u_meanconv};
%     elseif nargout==4
%         varargout{4}={u_conv_test_mean; u_conv_test_var};
%     end
% end
end
   