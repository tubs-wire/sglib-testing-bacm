function varargout=get_quantiles_and_pdf(init_func, init_options, method, N, varargin)

%Description: get quantiles, probabilites, histogram
%   [u_quant, t_span]=
%   get_quantiles_and_pdf 'MC', N, 'file_path', FILE_PATH, , 'N_per', N_PER, 'saving_results', 1, 'quantiles', [0.05, 0.95])
%   [u_quant, u_prob, t_span]=
%   get_quantiles_and_pdf((INIT_FUNC, INIT_OPTIONS, 'NIGAL', N, 'file_path', FILE_PATH, 'p_u',P_U,'p_int',P_INT,'saving results', 1, 'quantiles', [0.05, 0.95])
%Output:
%   -u_quant: value of solution for the given qunatiles u_quant{j_var,1}(:,itt)
%   -u_prob: u_prob.probabilities
%            u_prob.xi
%            u_prob.histogram
%
%Input:
%Input from file:   
% OBLIGATORY INPUTS:
%   -INIT_FUNC:     (example: @logistic_equation_init)
%   -INIT_OPTIONS:  (if initial values vary from the default one
%   -METHOD:        e.g.: 'MC' (Monte Carlo), 'NIGAL' (nonintrusive Galerkin)
%   -N:             for 'MC': number of samples
%                   for 'NIGAL': number of samples for
%                   sampling to draw statistics
% OPTIONAL INPUTS:
%
%
%  -MODE:          for 'MC' methods (example: ''(for nomal MC) 'QMC' (for quasi MC), 'LHS' (for latin hypercubic)
%                  default: ''
%  -N_per:         number of samples if saved in separated files 
%                  default: N
%  -P_U:           PC order for Galerkin method (needed for file_name definition
%                  default: empty []
%  -P_INT:         Integration order for Galerkin method (needed for file_name definition
%                  default: empty []
%  -SAVING RESULTS:=0: don't save results (default)
%                  =1: save results
%  -FILE_PATH:     where files with samples/PCE coefss are located
%                  where results to be saved when SAVING RESULT=1
%                  default: empty []
%  -QUANTILES:     vector of quantiles that is needed to be calculated from samples
%                  default: [0.05, 0.95] (it is possible to declare more than two values)
%  -U_SAMPLES:     samples can be directly given
%                  default: empty {}
%  -Nt_per:        draw statistics only from solution
%                  at each Nt_per-th timestep (to get smaller data,
%                  and faster calculation
%  -GLOBAL_DISTRIB: =0: PDF/HIST calculated seperately for each timestep (default), with local resolution                        
%                   =1: PDF/HIST calculated for a global ui vector (time
%                   and memory consuming, good for global plot) with global
%                   distribution
%  -FILE_NAMES:     name of the file, where samples/coeffs are located (automaticaly given from saving name from the 'COMPUTE.. .M' files 
%  
%  -DU:             resolution of the PDF/HIST (distance in between ui points)
%                   default: 0.1
%  -GET_HISTOGRAM   =0 (default): no output with histogram, only PDF is given
%                   =1: get in output U_PROB the histogram data as well


%%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig


options=varargin2options(varargin);
[mode,options]=get_option(options, 'mode', '');
[N_per,options]=get_option(options, 'N_per', N);    %saving result after each N_perth sample
[p_u,options]=get_option(options, 'p_u', []);
[p_int,options]=get_option(options, 'p_int', []);
[saving_results, options]=get_option(options, 'saving_results', 0);
[file_path,options]=get_option(options, 'file_path', []);
[quantiles,options]=get_option(options, 'quantiles', [0.05, 0.95]);
[u_samples, options]=get_option(options, 'u_samples', {});
[Nt_per,options]=get_option(options, 'Nt_per', 1);
[tt,options]=get_option(options, 'tt', []);
[global_distrib,options]=get_option(options, 'global_distrib', 0);
[file_names,options]=get_option(options, 'file_names', {});
[du,options]=get_option(options, 'du', 0.1);
[get_histogram, options]=get_option(options, 'get_histogram', 0);
[u_mean, options]=get_option(options, 'u_mean', []);
check_unsupported_options(options, mfilename);

%% Get general parameters from init_func
[state,polysys, time] = funcall(init_func, init_options);

n_var=state.num_eqs;
nt_step=state.num_vars;


%% Define data filenames- getting filenames of mat files where data
% stored and get samples/PCE coeffs when not multiple files are given

if isempty(u_samples)
    if isempty(file_names)
        file_names=get_filenames(init_func,method, file_path, N, N_per,mode, p_u, p_int);
    end
    if size(file_names,1)==1
        switch method
            case 'MC'
                [u_mean, u_samples]=load_solution_from_file_MC(file_names{1,1});
            case 'NIGAL'
                [u_mean, ui_alpha, V_u]=load_solution_from_file_G(file_names);
            case 'PROJ'
                [u_mean, ui_alpha, V_u]=load_solution_from_file_G(file_names);    
        end
    end
end
%% Get min and max values of solution for global histogram discretisation
%could be precised better

if nargout>2 && global_distrib
 if isempty(u_mean)
    u_mean=state.ref_sol;
 else
    u_mean=reshape(u_mean,nt_step,n_var);
 end
    xi_global=cell(n_var,1);
    for j_var=1:n_var
        xi_global{j_var,1} =get_ui_vector(u_mean(:,j_var), du, 'global');
    end
end

%% Initialize memory for solution vectors
if isempty(tt)
t_span=zeros(   floor(nt_step/Nt_per) ,1    );
else
    t_span=zeros(size(tt))';
end
u_quant=cell(n_var,1);
for j_var=1:n_var
    u_quant{j_var,1}= zeros(max(size(quantiles)),length(t_span));
end

if nargout>2
    u_prob=struct();
    if global_distrib
        if get_histogram
        hist_global=cell(j_var,1);
        end
        prob_global=cell(j_var,1);
        for j_var=1:n_var
             if get_histogram
            hist_global{j_var,1}=sparse( zeros(size(xi_global{j_var,1},2),length(t_span)));
             end
            prob_global{j_var,1}=sparse ( zeros(size(xi_global{j_var,1},2),length(t_span)));
        end
    else
        xi_local  =cell(j_var,length(t_span));
         if get_histogram
        hist_local=cell(j_var,length(t_span));
         end
        prob_local=cell(j_var,length(t_span));
    end
end

if isempty(tt)
    max_itt=floor(nt_step/Nt_per);
    tt=linspace(1,((max_itt-1)*Nt_per+1),max_itt);
else
    max_itt=length(tt);
end

for itt=1:max_itt
    t_span(itt)=state.t_span(tt(itt));
    % Get MC samples from multiple files or from sampling from PCE coeffs
    % or just simply get actual timeshot from existing samples 
    if isempty(u_samples)
        if  strcmp(method,'MC')
            u_samples_tt=draw_sample_from_multiple_file(file_names, N_per,tt(itt));
            
        elseif strcmp(method,'NIGAL')||strcmp(method,'PROJ')
            if iscell(V_u{1})
               V_u{1}= strcat(V_u{1}{:});
            end
            samples=gpcgerm_sample(V_u, N);
            %samples=(-1+2*rand(N,n_var))';
            u_samples_tt=gpc_evaluate(ui_alpha(:,:,tt(itt)), V_u, samples);
            u_samples_tt=num2cell(u_samples_tt,2);
        end
    else
        for j_var=1:n_var
            u_samples_tt{j_var,1}=u_samples{j_var,1}(tt(itt),:);
        end
    end
    % Get histrogram, PDF, quantiles for each timestep
    if nargout>2
        if global_distrib
            for j_var=1:n_var
                prob_global{j_var,1}(:,itt)    = sparse( (ksdensity(  u_samples_tt{j_var,1},  xi_global{j_var}) ) );
                if get_histogram
                    hist_global{j_var,1}(:,itt)    = sparse(  ( hist     (   u_samples_tt{j_var,1} , xi_global{j_var} )) /N );
                end
            end
        else
            for  j_var=1:n_var
                xi_local{j_var,tt(itt)}=get_ui_vector(u_samples_tt{j_var,1}, du, 'local');
                prob_local{j_var,tt(itt)}         =(  ksdensity(   u_samples_tt{j_var,1},   xi_local{j_var,tt(itt)}));
                if get_histogram
                    hist_local{j_var,tt(itt)}       =  (hist    (    u_samples_tt{j_var,1} ,  xi_local{j_var,tt(itt)} ))/N;
                end
            end
        end
    end
    for  j_var=1:n_var
        u_quant{j_var,1}(:,itt)  = quantile( u_samples_tt{j_var,1},quantiles);
    end
end
%% send output variables
if nargout<3
    if nargout==1
        varargout{1}=u_quant;
    else
        varargout{1}=u_quant;
        varargout{2}=t_span;
    end
else
    varargout{1}=u_quant;
    varargout{3}=t_span;
    if global_distrib
         if get_histogram
        u_prob.hist_global=hist_global;
         end
        u_prob.prob_global=prob_global;
        u_prob.xi_global=xi_global;
    else
         if get_histogram
        u_prob.hist_local=hist_local;
         end
        u_prob.prob_local=prob_local;
        u_prob.xi_local=xi_local;
    end
    varargout{2}=u_prob;
end
%% Save results
if saving_results
    sfile_name=get_saving_filenames(init_func,method,file_path, N, mode, p_u, p_int);
if nargout>2
 save(sfile_name, 'u_prob', 'u_quant','t_span', 'polysys', 'state')
 
else
 save(sfile_name, 'u_quant', 'polysys', 'state') 
end
disp(strcat('file have been saved to', sfile_name))
end
end
%%
function  u_samples_tt=draw_sample_from_multiple_file(file_name, N_per,tt)
    % Load fist part of the samples
    load (file_name{1})
    n_files=size(file_name,1);
    n_var=size(u_samples,1);
    u_samples_tt=cell(n_var,1);
    for j_var=1:n_var    %initialize memory
        u_samples_tt{j_var,1}=zeros(N,1);
    end
    % put solution together in one vector from all files at tt
    % timestep
    for k_files=1:n_files
        if ~(k_files==1)
            clear 'u_samples'
            load (file_name{k_files})
        end
        for j_var=1:n_var
            if k_files==n_files
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:end,1  )=u_samples{j_var,1}(tt,:);
            else
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:k_files*N_per,1  )=u_samples{j_var,1}(tt,:);
            end
        end
    end
end

%%
function x=get_ui_vector(u_samples, du, type)
 delta= max(u_samples)- min(u_samples);
   
if strcmp(type,'local')
    du=min([du, delta/50]);
    x_min= (   min(u_samples) -0.25*delta);
    x_max= (   max(u_samples)+0.25*delta );
elseif strcmp(type,'global')
    x_min=( min(u_samples)*(1-0.2) );
    x_max=( max(u_samples)*(1+0.2) );
end
x =(x_min:du:x_max);
end

%%
function file_name=get_filenames(init_func,method, file_path, N, N_per,mode, p_u, p_int)
equation_name=strrep(char(init_func),'init','_');
switch method
    case 'MC'
        n_files=ceil(N/N_per);
        file_name=cell(n_files,1);
        if n_files>1
        for i=1:n_files-1
            file_name{i,1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_part_',num2str(i),'all_samples', '.mat');
        end
        end
        file_name{n_files,1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','_all_samples', '.mat');
        %file_name{n_files+1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','.mat');
        
    case 'NIGAL'    %Data storing nonint Galerkin results
        file_name = strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint');
    case 'PROJ'    %Data storing nonint Galerkin results
      
       file_name=strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'full_tensor','_projection','.mat');
       
end
end

%%
function file_name=get_saving_filenames(init_func,method,file_path, N, mode, p_u, p_int)
equation_name=strrep(char(init_func),'init','_');
switch method
    case 'MC'
        file_name=strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_statistics', '.mat');
       
    case 'NIGAL'
        file_name=strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint', '_statistics','.mat');
    case 'PROJ'

       file_name=strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'full_tensor','_projection','_statistics','.mat');
end
end

%%
function [u_mean, ui_alpha, V_u]=load_solution_from_file_G(file_name)
    load(file_name)
    if ~exist('u_mean')
        u_mean=[];
    end
            %ui_alpha = u_i_alpha_tosave;
        ui_alpha = u_i_alpha;
   end

%%
 function [u_mean, u_samples]=load_solution_from_file_MC(file_name)
    load(file_name)
    if ~exist('u_mean')
        u_mean=[];
    end
 end
 %%
%    function xn = normalize_(x)
% % NORMALIZE Normalise a vector or matrix along the second dim to [0,1] (by
% % Elmar ZANDER)
% x1=min(x,[],2);
% x2=max(x,[],2);
% 
% xn = repmat(0.5, size(x));
% ind = (x2-x1) > 1e-6 * (x1+x2);
% xn(ind,:)=binfun(@rdivide, binfun(@minus, x(ind,:), x1(ind)), x2(ind)-x1(ind));
%    end
