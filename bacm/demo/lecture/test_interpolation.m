init_func = @electrical_network_init
solve_func = @electrical_network_solve
c=4; % index of the component of u to interpolate


% 1. Setup basis
V=gpcbasis_create('pu', 'p', 3);
M=gpcbasis_size(V,1);

% 2. Setup the points
N=M;
p = gpcgerm_sample(V, M);

% 3. Compute matrix (transpose!)
W = gpcbasis_evaluate(V, p)';

% 4. Compute RHS
state=funcall(init_func);

b = zeros(N,1);
for i=1:N
    ui = funcall(solve_func, state, p(:,i));
    b(i) = ui(c);
end

% 5. Solve for u
u_i_alpha = (W\b)';


% Plot response surface and interpolation points
plot_response_surface(u_i_alpha, V)
u=gpc_evaluate(u_i_alpha, V, p);
hold on; plot3(p(1,:), p(2,:), u(1,:)+0.002, 'kx'); hold off;
hold on; plot3(p(1,:), p(2,:), b+0.002, 'ko'); hold off;







%V=gpcbasis_create('pu', 'p', 4, 'full_tensor', true)
