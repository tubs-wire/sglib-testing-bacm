init_func = @electrical_network_init;
solve_func = @electrical_network_solve;
c=4; % index of the component of u to interpolate


% 1. Setup basis
deg=5;
V=gpcbasis_create('uu', 'p', deg, 'full_tensor', true);
%V=gpcbasis_create('uu', 'p', deg, 'full_tensor', false);
M=gpcbasis_size(V,1);

% 2. Setup the points
[p,w]=gpc_integrate([], V, deg+1, 'grid', 'full_tensor'); % forget about w
N=size(p,2);

% 3. Compute matrix (transpose!)
W = gpcbasis_evaluate(V, p)';

% 4. Compute RHS
state=funcall(init_func);

b = zeros(N,1);
for i=1:N
    ui = funcall(solve_func, state, p(:,i));
    b(i) = ui(c);
end

% 5. Solve for u
u_i_alpha = (W\b)';


% Plot response surface and interpolation points
plot_response_surface(u_i_alpha, V)
u=gpc_evaluate(u_i_alpha, V, p);
hold on; plot3(p(1,:), p(2,:), u(1,:)+0.002, 'kx'); hold off;
hold on; plot3(p(1,:), p(2,:), b+0.002, 'ko'); hold off;
