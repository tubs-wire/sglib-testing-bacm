function preplot_probabilities

%% Create histogram plot for every tt_th timestep

tt_th=50;
n_plots=ceil(nt_step/tt_th)+1;
for i_plot=1:n_plots
    
    if i_plot==n_plots
        tt=nt_step;
    else
        tt=(i_plot-1)*tt_th+1;
    end
    subplot(ceil(n_plots/2),2,i_plot)
    bar(xi_global{1},(prob_global{1,1}(:,tt)) );
    
    
    %xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
    %ylabel('number of samples')
    %'FaceAlpha', 0.5
end

figure
plot3( X ,  Y,    prob_global{1,1}   )

hist_global{1,1}(find(~hist_global{1,1}))=NaN;
prob_global{1,1}(find(~prob_global{1,1}))=NaN;
[X,Y]=ndgrid(xi_global{1,1},(t_span)' );

surf(   X, Y, prob_global{1,1}  )
shading interp
colormap(gray)
set(gca, 'ZScale', 'log')
%title('HISTOGRAM OF NUMBER OF PREYS for different time snapshots')

figure
for i_plot=1:n_plots
    subplot(ceil(n_plots/2),2,i_plot)
    if i_plot==n_plots
        tt=nt_step;
    else
        tt=(i_plot-1)*tt_th+1;
    end
    plot(x_prob_tt{1,1}(:,:,tt),prob_tt{1,1}(:,:,tt) )
    xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
    ylabel('probability')
end





end