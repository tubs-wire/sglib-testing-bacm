function Evaluate(init_func,mode,N, N_per, saving_path)

init_func = @prey_predator_init;
N=1000;
N_per=500;
N_gpc_sample=10000;
mode='';
saving_path='/home/noefried/Results_Noemi_sglib';
t_max=30;

p_u=2;
p_int=2;

%label for the plots


%% Define data filenames- getting filenames of mat files where data stored


equation_name=strrep(char(init_func),'init','_');
%Data storing MC sampling
n_files=ceil(N/N_per);
for i=1:n_files-1
file_name{i} = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_part_',num2str(i),'all_samples', '.mat');
end
file_name_end = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','_all_samples', '.mat');
%Data storing Galerkin results
file_name_nonint_gal = strcat(saving_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint');
file_MC = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','.mat');

%% Get maximum of mean values for histogram discretisation
load(file_MC)
for j_var=1:n_var
        x_min=min(u_mean(:,j_var)-3*sqrt( u_var(:,jvar) ) );
        x_max=max(u_mean(:,j_var)+3*sqrt( u_var(:,jvar) ) );
        step=min(3*sqrt( u_var(:,jvar) ) )/5;
        x_i_global{j_var} =(x_min:step:x_max);
end
%% Get histrogram, PDF and quantiles for each timestep
% Load fist part of the samples
load (file_name{1})
load (file_name_nonint_gal)
nt_step=size(u_samples{1},1);
n_var=size(u_samples,1);


%Initialize memory for values

xi=cell(n_var,nt_step);
xi_prob=cell(n_var,nt_step);

for tt=1:nt_step
    tt
    for j_var=1:n_var
        u_samples_tt{j_var,1}=zeros(N,1);
    end
    % put output variables together in one vector from all files at tt
    % timestep
    for k_files=1:n_files
        if ~(k_files==1)
            clear 'u_samples'
            if k_files==n_files
                load (file_name_end)
            else
                load (file_name{k_files})
            end
        end
        
        for j_var=1:n_var
            if k_files==n_files
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:end,1  )=u_samples{j_var,1}(tt,:);
            else
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:k_files*N_per,1  )=u_samples{j_var,1}(tt,:);
            end
        end
    end
    %draw histogram, probabilities, quantiles for all output variables at
    %tt timestep for MC
    sol_tt= gpc_evaluate(u_i_alpha_tosave(:,:,tt), V_u, (-1+2*rand(N_gpc_sample,n_var))'); 
    
    for j_var=1:n_var
        x_min=min(u_mean(:,j_var)-3*sqrt( u_var(:,jvar) ) );
        x_max=max(u_mean(:,j_var)+3*sqrt( u_var(:,jvar) ) );
        
        x_i{j_var} =(x_min:0.1:x_max);
        %x_i_prob{j_var,tt} =linspace(x_min,x_max,100);
        
        var_hist
        n_tt{j_var,tt}      =hist(   u_samples_tt{j_var,1} , x_i{j_var} )/N;
        prob_tt{j_var,tt}   =ksdensity(   u_samples_tt{j_var,1},  x_i{j_var});
        quant_min_max{j_var,1}(:,tt)=quantile( u_samples_tt{j_var,1},[0.05, 0.95]);
        
        %same for Galerkin nonintrusive
        n_tt_G{j_var,tt}      =hist(    sol_tt(j_var,:) , x_i{j_var,tt} )/N_gpc_sample;
        prob_tt{j_var,tt}   =ksdensity(    sol_tt(j_var,:),  x_i_prob{j_var,tt});
        quant_min_max{j_var,1}(:,tt)=quantile(  sol_tt(j_var,:),[0.05, 0.95]);
        
              
        %[n_tt{j_var,1}(:,:,tt),x_out_tt{j_var,1}(:,1,tt)]=hist(   u_samples_tt{j_var,1} ,30 );
        %[prob_tt{j_var,1}(:,1,tt), x_prob_tt{j_var,1}(:,1,tt)]=ksdensity(   u_samples_tt{j_var,1});
        %quant_min_max{j_var,1}(:,tt)=quantile( u_samples_tt{j_var,1}, [0.05, 0.95]);
    
     
    end
    
    end
    
%% Create histogram plot for every tt_th timestep

tt_th=50;
n_plots=ceil(nt_step/tt_th)+1;
 for i_plot=1:n_plots
     
      if i_plot==n_plots
 tt=nt_step;  
      else
 tt=(i_plot-1)*tt_th+1;
      end
       subplot(ceil(n_plots/2),4,(2*i_plot-1))
     bar(x_i{1,tt},(n_tt{1,tt}) );
        subplot(ceil(n_plots/2),4,i_plot*2)
     bar(x_i{1,tt},n_tt_G{1,tt},'r');
    
     %xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
     %ylabel('number of samples')
     %'FaceAlpha', 0.5
 end
 title('HISTOGRAM OF NUMBER OF PREYS for different time snapshots')
 
 figure
 for i_plot=1:n_plots
      subplot(ceil(n_plots/2),2,i_plot)
      if i_plot==n_plots
 tt=nt_step;  
      else
 tt=(i_plot-1)*tt_th+1;
      end
     plot(x_prob_tt{1,1}(:,:,tt),prob_tt{1,1}(:,:,tt) )
     xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
     ylabel('probability')
 end
 
 %% Draw histogram from gPC coeffs
 %load mat file with coeffs
 
 
 tt=1
 sol_tt  = gpc_evaluate(u_i_alpha_tosave(1,:,tt), V_u, x_prob_tt{1,1}(:,:,tt));
 plot(x_prob_tt, sol_tt)
 %%
% u_samples_j({}
% 
% end
% u_meanpu2p2=u_mean;
% u_varpu2p2=u_var;
% 
% error_mean=abs(u_meanpu2p2-u_mean_MC);
% rel_error_mean=error_mean./u_mean_MC;
% [max_mean_error,i]=max(rel_error_mean)
% norm_mean=norm(rel_error_mean)
% 
% error_var=abs(u_varpu2p2-u_var_MC);
% rel_error_var=error_var./u_var_MC;
% [c,i]=max(rel_error_var);
% 
% norm_var=norm(rel_error_var)
% [max_var_error,i]=max(rel_error_var)
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% u_meanpu3p4=u_mean;
% u_varpu3p4=u_var;
% 
% error_mean=abs(u_meanpu3p4-u_mean_MC);
% rel_error_mean=error_mean./u_mean_MC;
% rel_error_mean=reshape(rel_error_mean,[],1);
% [max_mean_error,i]=max(rel_error_mean)
% norm_mean=norm(rel_error_mean)
% 
% error_var=abs(u_varpu3p4-u_var_MC);
% rel_error_var=error_var./u_var_MC;
% rel_error_var=reshape(rel_error_var,[],1);
% 
% norm_var=norm(rel_error_var)
% [max_var_error,i]=max(rel_error_var)
% 
