function get_minipdf_plots(u_prob, t_span, varargin)

options=varargin2options(varargin);
[tt,options]=get_option(options, 'tt', []);
[n_plots,options]=get_option(options, 'n_plots', 10);
[global_distrib,options]=get_option(options, 'global_distrib', 0);
[line_color,options]=get_option(options, 'line_color', 'black');
[LineWidth, options]=get_option(options, 'LineWidth', 2);
[subplot_dim,options]=get_option(options, 'subplot_dim', []);
[method_name,options]=get_option(options, 'method_name', {});
check_unsupported_options(options,mfilename);
%[multiple_plots, options]=get_option(options, 'multiple_plots', 0);

%% Initialize parameters
if isempty(tt);
    dtt=length(t_span)/(n_plots-1);
    tt=round(1:dtt:length(t_span));
    tt(n_plots)=length(t_span);
end

n_plots=length(tt);
if global_distrib
    my_xi=u_prob.xi_global;
    my_prob=u_prob.prob_global;
    
else
    if isfield(u_prob, 'xi_local')
        my_xi=u_prob.xi_local;
        my_prob=u_prob.prob_local;
    elseif isfield(u_prob, 'xi_global')
        my_xi=u_prob.xi_global;
        my_prob=u_prob.prob_global;
        warning('Gloabal probabilities are plotted, though local was initialy defined. In U_PROB only global parameters are defined')
        global_distrib=1;
    else
        error('not enough parameters are defined in U_PROB')
    end
end

%% Get histograms/pdfs for each tt-th timeshots
n_var=size(my_prob,1);

for i_var=1:n_var
    %subplot_def
    m=subplot_dim(1);
    n=subplot_dim(2);
    subplot(m,n,i_var)
    hold on
 for iplot=1:n_plots
         %Plot PDF
          shift_x=t_span(tt(iplot));
        if global_distrib
            ind=find(   my_prob{i_var,1}(:,tt(iplot))    ); %find nonzero part
            yyp=my_xi{i_var,1}(1,ind);
            xxp=normalize_((my_prob{i_var,1}(ind,tt(iplot)))')+shift_x;
        else
            yyp=my_xi{i_var,tt(iplot)};
            xxp=normalize_(my_prob{i_var,tt(iplot)})+shift_x;
        end
        const_x=shift_x*ones(size(yyp)); 
        plot(xxp,yyp,line_color, 'LineWidth', LineWidth, 'DisplayName', method_name); %draw PDF
        plot(const_x,yyp,line_color, 'LineWidth', LineWidth);                               %draw vertical line
   end

end

end
function xn = normalize_(x)
% NORMALIZE Normalise a vector or matrix along the second dim to [0,1] by
% Elmar ZANDER
x1=min(x,[],2);
x2=max(x,[],2);

xn = repmat(0.5, size(x));
ind = (x2-x1) > 1e-6 * (x1+x2);
xn(ind,:)=binfun(@rdivide, binfun(@minus, x(ind,:), x1(ind)), x2(ind)-x1(ind));
end