function Evaluate(init_func,mode,N, N_per, saving_path)

init_func = @prey_predator_init;
p_u=2;
p_int=2;
saving_path='/home/noefried/Results_Noemi_sglib';
t_max=30;
%label for the plots

%% Define data filenames


equation_name=strrep(char(init_func),'init','_');

%Data storing Galerkin results
file_name_nonint_gal = strcat(saving_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint');

%% Get maximum of mean values for histogram discretisation

%% Get histrogram for each timestep
% Load fist part of the samples
load (file_name_nonint_gal)
nt_step=size(u_i_alpha_tosave,3);
n_var=size(u_i_alpha_tosave,1);

for tt=1:nt_step
    tt
    sol_tt= gpc_evaluate(u_i_alpha_tosave(:,:,tt), V_u, rand(10000,n_var)');  
    %draw histogram, probabilities, quantiles for all output variables at tt timestep
    
    for j_var=1:n_var
        [n_tt{j_var,1}(:,:,tt),x_out_tt{j_var,1}(:,1,tt)]=hist(  sol_tt(j_var,:) ,30 );
        [prob_tt{j_var,1}(:,1,tt), x_prob_tt{j_var,1}(:,1,tt)]=ksdensity(   sol_tt(j_var,:));
        quant_min_max{j_var,1}(:,tt)=quantile(sol_tt(j_var,:), [0.05, 0.95]);
    end
    
end
    
%% Create histogram plot for every tt_th timestep

tt_th=50;
n_plots=ceil(nt_step/tt_th)+1;
 for i_plot=1:n_plots
      subplot(ceil(n_plots/2),2,i_plot)
      if i_plot==n_plots
 tt=nt_step;  
      else
 tt=(i_plot-1)*tt_th+1;
      end
     bar(x_out_tt{1,1}(:,:,tt),n_tt{1,1}(:,:,tt) )
     xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
     ylabel('number of samples')
 end
 
 figure
 for i_plot=1:n_plots
      subplot(ceil(n_plots/2),2,i_plot)
      if i_plot==n_plots
 tt=nt_step;  
      else
 tt=(i_plot-1)*tt_th+1;
      end
     plot(x_prob_tt{1,1}(:,:,tt),prob_tt{1,1}(:,:,tt) )
     xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
     ylabel('probability')
 end
 
 
 %%
% u_samples_j({}
% 
% end
% u_meanpu2p2=u_mean;
% u_varpu2p2=u_var;
% 
% error_mean=abs(u_meanpu2p2-u_mean_MC);
% rel_error_mean=error_mean./u_mean_MC;
% [max_mean_error,i]=max(rel_error_mean)
% norm_mean=norm(rel_error_mean)
% 
% error_var=abs(u_varpu2p2-u_var_MC);
% rel_error_var=error_var./u_var_MC;
% [c,i]=max(rel_error_var);
% 
% norm_var=norm(rel_error_var)
% [max_var_error,i]=max(rel_error_var)
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% u_meanpu3p4=u_mean;
% u_varpu3p4=u_var;
% 
% error_mean=abs(u_meanpu3p4-u_mean_MC);
% rel_error_mean=error_mean./u_mean_MC;
% rel_error_mean=reshape(rel_error_mean,[],1);
% [max_mean_error,i]=max(rel_error_mean)
% norm_mean=norm(rel_error_mean)
% 
% error_var=abs(u_varpu3p4-u_var_MC);
% rel_error_var=error_var./u_var_MC;
% rel_error_var=reshape(rel_error_var,[],1);
% 
% norm_var=norm(rel_error_var)
% [max_var_error,i]=max(rel_error_var)
% 
