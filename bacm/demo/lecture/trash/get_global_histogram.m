function varargout=get_global_histogram(init_func,mode,N, N_per, saving_path)

%Description: get quantiles, probabilites, histogram
%Example:
%   u_quant=
%   [u_quant, u_prob]=
%Output:
%   -u_quant: value of solution for the given qunatiles u_quant{j_var,1}(:,itt)
%   -u_prob: 
init_func = @prey_predator_init;
saving_path='/home/noefried/Results_Noemi_sglib';
t_min=0;
t_max=30;
quantiles=[0.05, 0.95];
Nt_per=1; %statistics for every Nt_perth timestep
global_distrib=0;  %if same x_values needed for all the probability plots(not recommended)
du=0.1; %for local xi vector spacing for pdf and histogram resolution
method='MC';
%method='NIGAL'
switch method
    case 'MC'
N=1000; %when filename needed
N_per=500; %when filename needed
mode=''; %when filename needed
    case 'NIGAL'
N=10000; %number of samples to draw statistics
p_u=2; %when filename needed
p_int=2; %when filename needed
end
%label for the plots? (ha nem rakok bele plottot akkor nem kell)


%% Define data filenames- getting filenames of mat files where data stored
%% and load file where final u_mean and u_var is located

equation_name=strrep(char(init_func),'init','_');

switch method
    case 'MC'  %Data storing MC sampling
        n_files=ceil(N/N_per);
        file_name=cell(n_files,1);
        for i=1:n_files-1
            file_name{i} = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_part_',num2str(i),'all_samples', '.mat');
        end
        file_name{n_files} = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','_all_samples', '.mat');
        file_MC = strcat(saving_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','.mat');
        load(file_MC)
        
    case 'NIGAL'    %Data storing nonint Galerkin results
        file_name_nonint_gal = strcat(saving_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint');
        load (file_name_nonint_gal)
end
%% Get min and max values of solution for global histogram discretisation
%could be precised better
n_var=state.num_eqs;
nt_step=state.num_vars;
if global_distrib
    
    u_mean=reshape(u_mean,nt_step,n_var);
    u_var=reshape(u_var,nt_step,n_var);
    xi_global=cell(n_var,1);
    
    for j_var=1:n_var
        xi_global{j_var,1} =get_ui_vector([u_mean(:,j_var),u_var(:,j_var) ], du, type)
    end
end

%% Initialize memory for solution vectors

quant_min_max=cell(j_var,1);
for j_var=1:n_var
    quant_min_max{j_var,1}= zeros(max(size(quantiles)),length(t_span));
end
t_span=zeros(   floor(nt_step/Nt_per) ,1    );

if global_distrib
    hist_global=cell(j_var,1);
    prob_global=cell(j_var,1);
    for j_var=1:n_var
        hist_global{j_var,1}=sparse( zeros(size(xi_global{j_var,1},2),length(t_span)));
        prob_global{j_var,1}=sparse ( zeros(size(xi_global{j_var,1},2),length(t_span)));
    end
else
    xi_local  =cell(j_var,length(t_span));
    hist_local=cell(j_var,length(t_span));
    prob_local=cell(j_var,length(t_span));
end


  for itt=1:floor(nt_step/Nt_per)
    tt=(itt-1)*Nt_per+1
    t_span(itt)=state.t_span(tt);
%% Get samples from files
    switch method
        case 'MC'
            u_samples_tt=draw_sample_from_multiple_file(file_name, N_per,tt);
        case 'NIGAL'
            u_samples_tt= gpc_evaluate(u_i_alpha_tosave(:,:,tt), V_u, (-1+2*rand(N_gpc_sample,n_var))');
    end
%% Get histrogram, PDF and quantiles for each timestep
if global_distrib
    for j_var=1:n_var
        hist_global{j_var,1}(:,itt)    = sparse(  hist     (   u_samples_tt{j_var,1} , xi_global{j_var} )/N  );
        prob_global{j_var,1}(:,itt)    = sparse(  ksdensity(  u_samples_tt{j_var,1},  xi_global{j_var})  );
    end
else
    for  j_var=1:n_var
     xi_local{j_var,tt}=get_local_ui_vector(u_samples_tt{j_var,1}, du);
     hist_local{j_var,tt}         =hist    (    u_samples_tt{j_var,1} ,  x_i{j_var,tt} );
     prob_local{j_var,tt}         =ksdensity(   u_samples_tt{j_var,1},   x_i_prob{j_var,tt});
    end
for  j_var=1:n_var  
 quant_min_max{j_var,1}(:,itt)  = quantile( u_samples_tt{j_var,1},quantiles);
end
end

%% Create histogram plot for every tt_th timestep

tt_th=50;
n_plots=ceil(nt_step/tt_th)+1;
for i_plot=1:n_plots
    
    if i_plot==n_plots
        tt=nt_step;
    else
        tt=(i_plot-1)*tt_th+1;
    end
    subplot(ceil(n_plots/2),2,i_plot)
    bar(xi_global{1},(prob_global{1,1}(:,tt)) );
    
    
    %xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
    %ylabel('number of samples')
    %'FaceAlpha', 0.5
end

figure
plot3( X ,  Y,    prob_global{1,1}   )

hist_global{1,1}(find(~hist_global{1,1}))=NaN;
prob_global{1,1}(find(~prob_global{1,1}))=NaN;
[X,Y]=ndgrid(xi_global{1,1},(t_span)' );

surf(   X, Y, prob_global{1,1}  )
shading interp
colormap(gray)
set(gca, 'ZScale', 'log')
%title('HISTOGRAM OF NUMBER OF PREYS for different time snapshots')

figure
for i_plot=1:n_plots
    subplot(ceil(n_plots/2),2,i_plot)
    if i_plot==n_plots
        tt=nt_step;
    else
        tt=(i_plot-1)*tt_th+1;
    end
    plot(x_prob_tt{1,1}(:,:,tt),prob_tt{1,1}(:,:,tt) )
    xlabel(strcat('number of preys at time ',num2str(tt/nt_step*t_max)))
    ylabel('probability')
end





end
function  u_samples_tt=draw_sample_from_multiple_file(file_name, N_per,tt)
% Load fist part of the samples
load (file_name{1})
n_files=size(file_name,2)+1;
n_var=size(u_samples,1);
for j_var=1:n_var    %initialize memory
    u_samples_tt{j_var,1}=zeros(N,1);
end
% put solution together in one vector from all files at tt
% timestep
for k_files=1:n_files
    if ~(k_files==1)
        clear 'u_samples'
        load (file_name{k_files})
    end
    
    for j_var=1:n_var
        if k_files==n_files
            u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:end,1  )=u_samples{j_var,1}(tt,:);
        else
            u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:k_files*N_per,1  )=u_samples{j_var,1}(tt,:);
        end
    end
end
end
function x=get_ui_vector(u_samples, du, type)
if type=='local'
    x_min= floor(   min(u_samples) );
    x_max= ceil (   max(u_samples) );
    
elseif tpye=='global'
    
    u_mean=u_samples(:,1);
    u_var =u_var    (:,1);
    x_min=floor( min(u_mean-3*sqrt( u_var ) ) );
    x_max=ceil ( max(u_mean+3*sqrt( u_var ) ) );
    if empty(du)
        du=max (  min( 3*sqrt( u_var ) )/3, 0.5 );
    end
    
end
x =(x_min:du:x_max);
end

