function [sample_matrix] = get_sample_points_mc(init_func,  N, init_options, varargin)
%%
options=varargin2options(varargin);
[mode,options]=get_option(options, 'mode', '');
check_unsupported_options(options, mfilename);

%%
sample_options=struct;
if ~isempty(mode)
    sample_options.mode = mode;
end


[state,polysys] = funcall(init_func, init_options);

V = gpcbasis_create(polysys, 'm', state.num_params);
x = gpcgerm_sample(V, N, sample_options);


%% 
sample_matrix=zeros(state.num_params, N);
for j = 1:N
    x_j = x(:,j);
    state=scale_and_update_vars(x_j, state);
  sample_matrix(:,j)= cell2mat(struct2cell(state.actual_params));
end