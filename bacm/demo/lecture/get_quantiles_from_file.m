function u_quant=get_quantiles_from_file(file_name, method, quantile, varargin)

%Description: get quantiles from MC samples
%   
%  e.g.:
%   
%   u_quant=get_quantiles_MC(file_name,[0.05, 0.95])

options=varargin2options(varargin);
[saving_results, options]=get_option(options, 'saving_results', 0);
[file_path,options]=get_option(options, 'file_path', []);
[Nt_per,options]=get_option(options, 'Nt_per', 1);
[tt,options]=get_option(options, 'tt', []);
[i_var,options]=get_option(options, 'i_var', []);
check_unsupported_options(options, mfilename);



% Outputs:
%   -u_quant: value of solution for the given qunatiles u_quant{j_var,1}(:,itt)


% INPUTS:
%   -FILENAME:     where statistics are stored
%   -METHOD:       quantile from sample/from gGPCE
%   -QUANTILE:     vector of quantiles that is needed to be calculated
%                  e.g.: [0.05, 0.95] (it is also possible to declare one, or more than two values)

% OPTIONAL INPUTS:
%
%  -SAVING RESULTS:=0: don't save results (default)
%                  =1: save results
%  -FILE_PATH:     where results to be saved when SAVING RESULT=1
%                  default: empty []
%  -Nt_per:        draw statistics only from solution
%                  at each Nt_per-th timestep (to get smaller data,
%                  and faster calculation
%  -TT:            draw statistics only from solution at tt-th timestep (a vector with indices)
%  -I_VAR          draw statistics only for the I_VARth VARIABLE
%
%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig

%% Get samples/gPCE and vector sizes from file
if strcmp(method, 'MC')
    samples=get_samples_from_file(file_name, method,tt,'tt', tt, 'i_var', i_var);
   
elseif strcmp(method, 'DIRINTEG')
    error('quantiles can not be calculated if direct integration method was used')
    
else
    samples=get_samples_from_file(file_name, 'gPCE',tt,'tt', tt, 'i_var', i_var);
end
%%

%% Initialize memory for solution vectors
if ~(isempty(i_var)) || n_var==1
    u_quant= zeros(max(size(quantiles)),length(t_span));
else
    u_quant=cell(n_var,1);
    for j_var=1:n_var
        u_quant{j_var,1}= zeros(max(size(quantiles)),length(t_span));
    end
end

if isempty(tt)
    max_itt=floor(nt_step/Nt_per);
    tt=linspace(1,((max_itt-1)*Nt_per+1),max_itt);
else
    max_itt=length(tt);
end

for itt=1:max_itt
    t_span(itt)=state.t_span(tt(itt));
    % Get MC samples from multiple files or from sampling from PCE coeffs
    % or just simply get actual timeshot from existing samples 
    if isempty(u_samples)
        if  strcmp(method,'MC')
            u_samples_tt=draw_sample_from_multiple_file(file_names, N_per,tt(itt));
            
        elseif strcmp(method,'NIGAL')
            if iscell(V_u{1})
               V_u{1}= strcat(V_u{1}{:});
            end
            samples=gpcgerm_sample(V_u, N);
            %samples=(-1+2*rand(N,n_var))';
            u_samples_tt=gpc_evaluate(ui_alpha(:,:,tt(itt)), V_u, samples);
            u_samples_tt=num2cell(u_samples_tt,2);
        end
    else
        for j_var=1:n_var
            u_samples_tt{j_var,1}=u_samples{j_var,1}(tt(itt),:);
        end
    end
    % Get histrogram, PDF, quantiles for each timestep
    if nargout>2
        if global_distrib
            for j_var=1:n_var
                prob_global{j_var,1}(:,itt)    = sparse( (ksdensity(  u_samples_tt{j_var,1},  xi_global{j_var}) ) );
                if get_histogram
                    hist_global{j_var,1}(:,itt)    = sparse(  ( hist     (   u_samples_tt{j_var,1} , xi_global{j_var} )) /N );
                end
            end
        else
            for  j_var=1:n_var
                xi_local{j_var,tt(itt)}=get_ui_vector(u_samples_tt{j_var,1}, du, 'local');
                prob_local{j_var,tt(itt)}         =(  ksdensity(   u_samples_tt{j_var,1},   xi_local{j_var,tt(itt)}));
                if get_histogram
                    hist_local{j_var,tt(itt)}       =  (hist    (    u_samples_tt{j_var,1} ,  xi_local{j_var,tt(itt)} ))/N;
                end
            end
        end
    end
    for  j_var=1:n_var
        u_quant{j_var,1}(:,itt)  = quantile( u_samples_tt{j_var,1},quantiles);
    end
end
%% send output variables
if nargout<3
    if nargout==1
        varargout{1}=u_quant;
    else
        varargout{1}=u_quant;
        varargout{2}=t_span;
    end
else
    varargout{1}=u_quant;
    varargout{3}=t_span;
    if global_distrib
         if get_histogram
        u_prob.hist_global=hist_global;
         end
        u_prob.prob_global=prob_global;
        u_prob.xi_global=xi_global;
    else
         if get_histogram
        u_prob.hist_local=hist_local;
         end
        u_prob.prob_local=prob_local;
        u_prob.xi_local=xi_local;
    end
    varargout{2}=u_prob;
end
%% Save results
if saving_results
    sfile_name=get_saving_filenames(init_func,method,file_path, N, mode, p_u, p_int);
if nargout>2
 save(sfile_name, 'u_prob', 'u_quant','t_span', 'polysys', 'state')
else
 save(sfile_name, 'u_quant', 'polysys', 'state') 
end
end
end
%%
function  u_samples_tt=draw_sample_from_multiple_file(file_name, N_per,tt)
    % Load fist part of the samples
    load (file_name{1})
    n_files=size(file_name,1);
    n_var=size(u_samples,1);
    u_samples_tt=cell(n_var,1);
    for j_var=1:n_var    %initialize memory
        u_samples_tt{j_var,1}=zeros(N,1);
    end
    % put solution together in one vector from all files at tt
    % timestep
    for k_files=1:n_files
        if ~(k_files==1)
            clear 'u_samples'
            load (file_name{k_files})
        end
        for j_var=1:n_var
            if k_files==n_files
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:end,1  )=u_samples{j_var,1}(tt,:);
            else
                u_samples_tt{j_var,1}(  (k_files-1)*N_per+1:k_files*N_per,1  )=u_samples{j_var,1}(tt,:);
            end
        end
    end
end

%%
function x=get_ui_vector(u_samples, du, type)
if strcmp(type,'local')
   delta= max(u_samples)- min(u_samples);
    x_min= floor(   min(u_samples) -0.1*delta);
    x_max= ceil (   max(u_samples)+0.1*delta );
elseif strcmp(type,'global')
    u_mean=u_samples;
    x_min=floor( min(u_mean)*(1-0.2) );
    x_max=ceil ( max(u_mean)*(1+0.2) );
end
x =(x_min:du:x_max);
end

%%
function file_name=get_filenames(init_func,method, file_path, N, N_per,mode, p_u, p_int)
equation_name=strrep(char(init_func),'init','_');
switch method
    case 'MC'
        n_files=ceil(N/N_per);
        file_name=cell(n_files,1);
        if n_files>1
        for i=1:n_files-1
            file_name{i,1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_part_',num2str(i),'all_samples', '.mat');
        end
        end
        file_name{n_files,1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','_all_samples', '.mat');
        %file_name{n_files+1} = strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC',mode,'_final','.mat');
        
    case 'NIGAL'    %Data storing nonint Galerkin results
        file_name = strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint');
end
end

%%
function file_name=get_saving_filenames(init_func,method,file_path, N, mode, p_u, p_int)
equation_name=strrep(char(init_func),'init','_');
switch method
    case 'MC'
        file_name=strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_statistics', '.mat');
       
    case 'NIGAL'
        file_name=strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint', '_statistics','.mat');
    end
end

%%
function [u_mean, ui_alpha, V_u]=load_solution_from_file_G(file_name)
    load(file_name)
    if ~exist('u_mean')
        u_mean=[];
    end
    ui_alpha = u_i_alpha_tosave;
end

%%
 function [u_mean, u_samples]=load_solution_from_file_MC(file_name)
    load(file_name)
    if ~exist('u_mean')
        u_mean=[];
    end
 end
 
 %% 
 function get_quantiles_from_MC_file(file_name, varargin)
 end
