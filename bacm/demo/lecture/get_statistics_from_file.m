
function get_statistics_from_file
%Description: get quantiles, probabilites, plots
%   [u_quant, t_span]=
%   get_quantiles_and_pdf(init_func, init_options, 'MC', N_sample, file_path, 'N_per', N_per, 'saving results', 1, 'quantiles', [0.05, 0.95])
%   [u_quant, u_prob, t_span]=
%   get_quantiles_and_pdf(init_func, init_options, 'NIGAL', N_sample, file_path, 'p_u',3,'p_int',4,'saving results', 1, 'quantiles', [0.05, 0.95])
%Output:
%   -u_quant: value of solution for the given qunatiles u_quant{j_var,1}(:,itt)
%   -u_prob: u_prob.probabilities
%            u_prob.xi
%            u_prob.histogram
%
%Input:
%1.)Input from file:    file_path: where file with data to be read from
%                       init_func: (example: @logistic_equation_init)                                         
%                       Nt_per=1; %statistics only for every Nt_perth timestep
%                       global_distrib=0 (def);  %=1:if same x_values needed for all the probability plots(not recommended)
%                       du=0.1 (def); %for local xi vector spacing for pdf and histogram resolution
%%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig
%% Define statistics needed
quantiles=[0.05, 0.95]; %if quantile is empty, no quantiles are calculated
method='NIGAL';
equation_name='logistic'; %options: 'logistic', 'prey_predator', Lorenz63'
%% Define the filename
switch equation_name
    case 'logistic'
        init_func = @logistic_init;
    case 'prey_predator'
        init_func = @prey_predator_init; 
    case 'Lorenz63'
        init_func = @Lorenz63_init;
    case 'spring_simple'
        init_func = @spring_simple_init;
end
equation_name=strrep(char(init_func),'init','_');
file_path='/home/noefried/SFB880_C4/Results_Noemi_SFB880_trial_ex';
switch method
    case 'MC'
        N=1000; %number of total samples
        mode='';  % (''/'qmc'/'lhs')
        N_per=N/10; %number of samples/file (for multiple files)
        p={N,mode,N_per};
     case 'NIGAL'
        p_u=4;  %gPCE degree
        p_int=8; %degree of quadrature rule
        p={p_u,p_int};
    case 'DIRINTEG'
        p_int=8; %degree of quadrature rule
        points_type='smolyak'; %'smolyak'/'full_tensor'
        p={p_int,points_type};
    case 'PROJ'
        p_u=4;  %gPCE degree
        p_int=8; %degree of quadrature rule
        points_type='smolyak'; %'smolyak'/'full_tensor'
        p={p_u,p_int, points_type};
    case 'COLLOC'
        p_u=4;  %gPCE degree
        mode='interp'; %('interp'/'regression')
        p={p_u,mode};
end
file_name=get_saving_filenames(file_path, equation_name,  method, p);
%% Get reference solution and select timespots at its minmax values or other user defined places

switch equation_name
    case 'logistic'
        init_func = @logistic_init;
        [init_options, ylabels]=logistic_users_init_options();
    case 'prey_predator'
        init_func = @prey_predator_init;
        [init_options, ylabels]=prey_predator_users_init_options();
    case 'Lorenz63'
        [init_options, ylabels]=Lorenz63_users_init_options();
    case 'spring_simple'
        init_options=struct;
        ylabels={{'x0'};{'v0'}; {'a0'}};
end
[state,~, time] = funcall(init_func, init_options);
%get critical spots
if strcmp(equation_name,'logistic')
   tt=get_index_from_interval_devision(time,3);
else
   tt=get_index_of_min_max(state.ref_sol);
end
%% Get quantiles
[u_quant,t_span]=get_quantiles_from_file(init_func,init_options, method, N, 'file_path', file_path, 'p_u', p_u, 'p_int', p_int, 'saving_results', 1, 'quantiles', quantiles, 'get_histogram', 1, 'global_distrib',0);
   

%% Plot 
t_max=max(time);
subplot_dim=[ceil(length(tt)/3),3];
%tt=round([3,5,10, 15, 20, 25, 30]/t_max*(length(time)-1));
%tt=[1,tt]
%%
switch method
    case 'MC'
        file_name=get_saving_filenames(init_func,method,file_path, N, mode);
    case 'NIGAL'
        file_name=get_saving_filenames(init_func,method,file_path, [p_u, p_int], '');
end
%load(strcat(file_path, filesep, 'prey_predator__1000000sampledMC_statistics.mat'))
statistics_result=load(file_name);
u_prob=statistics_result.u_prob;
u_quant=statistics_result.u_quant;
%%
figure_handles=plot_pdf_or_hist(u_prob,time, 'tt', tt, 'get_histogram', 0, 'ylabels', ylabels, 'global_distrib',0, 'subplot_dim', subplot_dim, 'saving_results', 0, 'method_name', 'MC 10_6');
load(strcat(file_path, filesep, 'prey_predator__pu3_pint6_Gal_nonint_statistics.mat'))
plot_pdf_or_hist(u_prob,time, 'tt', tt, 'subplot_dim', subplot_dim, 'method_name', 'NIGAL pu4pi8', 'fig_handles', figure_handles, 'line_color', 'cyan', 'LineWidth', 1, 'multiple_plots', 1, 'ylabels', ylabels)
%plot_pdf_or_hist(u_prob,time)
plot3D_pdf(u_prob, u_mean, time, 'tt', tt, 'ylabels', ylabels,'global_distrib',1,'u_quant', u_quant, 'quantiles', quantiles);


%plot quantiles

plot_u_mean_quant_pdf(time, u_mean, u_quant, 'u_prob', u_prob, 'tt', tt,  'ylabels', ylabels, 'method_name', 'MC1000')
hold on
plot_u_mean_quant_pdf(time, u_mean, u_quant, 'u_prob', u_prob, 'tt', tt,  'ylabels', ylabels, 'method_name', 'NIGALpu2pint2','fill_color','red', 'line_color','black','transparency', 0.4 )
end

function tt=get_index_of_min_max(u)
tt=cell(1,size(u,2));
for i=1:size(u,2)
[~,ind_max]=findpeaks(u(:,i));
[~,ind_min]=findpeaks(-u(:,i));
tt{1,i}=[ind_max, ind_min];
end
tt=[1,sort(cell2mat(tt)),size(u,1)];
end

function tt=get_index_from_interval_devision(time,n_per)
maxt_index=size(time,1);
tt=round(linspace(1,maxt_index,n_per+1));
end
