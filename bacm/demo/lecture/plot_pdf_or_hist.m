function varargout=plot_pdf_or_hist(u_prob, t_span, varargin)

options=varargin2options(varargin);
[tt,options]=get_option(options, 'tt', []);
[saving_results, options]=get_option(options, 'saving_results', 0);
[saving_path, options]=get_option(options, 'saving_path', []);
[figfile_name, options]=get_option(options, 'figfile_name', []);
[n_plots,options]=get_option(options, 'n_plots', 10);
[u_var,options]=get_option(options, 'u_var', []);
[get_histogram, options]=get_option(options, 'get_histogram', 0);
[ylabels,options]=get_option(options, 'ylabels', {});
[global_distrib,options]=get_option(options, 'global_distrib', 0);
[line_color,options]=get_option(options, 'line_color', 'red');
[LineWidth, options]=get_option(options, 'LineWidth', 2);
[fig_handles,options]=get_option(options, 'fig_handles', {});
[subplot_dim,options]=get_option(options, 'subplot_dim', []);
[method_name,options]=get_option(options, 'method_name', {});
[multiple_plots, options]=get_option(options, 'multiple_plots', 0);
check_unsupported_options(options,mfilename);

%% Initialize parameters
if isempty(tt);
    dtt=length(t_span)/(n_plots-1);
    tt=round(1:dtt:length(t_span));
    tt(n_plots)=length(t_span);
end

n_plots=length(tt);
if global_distrib
    my_xi=u_prob.xi_global;
    if get_histogram
        my_hist=u_prob.hist_global;
    end
    my_prob=u_prob.prob_global;
    
else
    if isfield(u_prob, 'xi_local')
        my_xi=u_prob.xi_local;
        if get_histogram
            my_hist=u_prob.hist_local;
        end
        my_prob=u_prob.prob_local;
    elseif isfield(u_prob, 'xi_global')
        my_xi=u_prob.xi_global;
        if get_histogram
            my_hist=u_prob.hist_global;
        end
        my_prob=u_prob.prob_global;
        warning('Gloabal probabilities are plotted, though local was initialy defined. In U_PROB only global parameters are defined')
        global_distrib=1;
    else
        error('not enough parameters are defined in U_PROB')
    end
end

%% Get histograms/pdfs for each tt-th timeshots
n_var=size(my_prob,1);
if isempty(fig_handles)
for i_var=1:n_var
    fig_handles{i_var,1}=figure;
end
end


for i_var=1:n_var
    figure(fig_handles{i_var,1})
    for iplot=1:n_plots
        %Define subplot parameters
        if isempty(subplot_dim)
            m=ceil(length(tt)/2);
            n=2;
        else
            m=subplot_dim(1);
            n=subplot_dim(2);
        end
        subplot(m,n,iplot)
        hold on
        %Plot histogram if GET_HISTOGRAM set to 1
        if get_histogram
            if global_distrib
                ind=find(   my_hist{i_var,1}(:,tt(iplot))    );
                xx= my_xi{i_var,1}(1,ind) ;
                yy=normalize_((my_hist{i_var,1}(ind,tt(iplot)))');
                
            else
                xx=my_xi{i_var,tt(iplot)};
                yy=normalize_(my_hist{i_var,tt(iplot)});
            end
            bar(xx, yy );
        end
        
        %Plot PDF
        if global_distrib
            ind=find(   my_prob{i_var,1}(:,tt(iplot))    );
            xxp=my_xi{i_var,1}(1,ind);
            yyp=normalize_((my_prob{i_var,1}(ind,tt(iplot)))');
        else
            xxp=my_xi{i_var,tt(iplot)};
            yyp=normalize_(my_prob{i_var,tt(iplot)});
        end
        
        plot(xxp,yyp,line_color, 'LineWidth', LineWidth, 'DisplayName', method_name);
        
%         if ~(iplot==n)
%             legend(gca,'hide')
%         else legend(gca,'show')
%         end
        %lebel subplots and add legend on the right upper corner
        if ~multiple_plots
            if isempty(u_var)
                xlabel({  (strcat('time = ', num2str(t_span(tt(iplot)) )))},'FontSize',12) ;
            else
                xlabel({  (strcat('time = ', num2str(t_span(tt(iplot)) ))); (strcat ('var = ',  num2str(u_var(tt(iplot) ))  ) )}) ;
            end
        else
            if iplot==3
            legend(gca,'show')
            end
        end
      
    end
%set title of all subplots in the top center of the figure

 if ~isempty(ylabels)    
    set(gcf,'NextPlot','add');
    axes;
    h = title(upper(['probability distributions of ',  ylabels{i_var,1}, ' at different timespots']),'FontSize',12);
    set(gca,'Visible','off');
    set(h,'Visible','on');
 end
end
%% saving figure when SAVING_RESULTS=1
if saving_results
    if isempty(saving_path)
        saving_path=pwd;
        warning(['no saving_path was given, figure will be saved in ', saving_path])
    end
    if isempty(figfile_name)
        figfile_name='figure_PDF_plots';
    end
    file_name=cell(n_var,1);
    if n_var==1;
        file_name{1,1}=figfile_name;
    else
        for i_var=1:n_var
            file_name{i_var,1}=strcat(figfile_name, '_', num2str(i_var));
        end
    end
    for i_var=1:n_var
        file_pname=strcat(saving_path, filesep, file_name{i_var,1}, '.fig' );
        saveas(fig_handles{i_var,1}, file_pname)
    end
    if n_var==1
        display(['figure with PDF(s) is saved in ', saving_path, 'with file names ',figfile_name ])
    else
        display(['figures with PDF(s) is saved in ', saving_path, 'with file names ',figfile_name, '_N' ])
    end
end
  



%% output with figerhandles if needed
if nargout>0
    varargout{1}=fig_handles;
end
end
function xn = normalize_(x)
% NORMALIZE Normalise a vector or matrix along the second dim to [0,1] by
% Elmar ZANDER
x1=min(x,[],2);
x2=max(x,[],2);

xn = repmat(0.5, size(x));
ind = (x2-x1) > 1e-6 * (x1+x2);
xn(ind,:)=binfun(@rdivide, binfun(@minus, x(ind,:), x1(ind)), x2(ind)-x1(ind));
end
