function [u_mean, u_var, time, varargout] = compute_response_surface_tensor_interpolate(init_func, solve_func, V_u, p_u, init_options, varargin)
% COMPUTE_RESPONSE_SURFACE_TENSOR_INTERPOLATE Short description of compute_response_surface_tensor_interpolate.
%   COMPUTE_RESPONSE_SURFACE_TENSOR_INTERPOLATE Long description of compute_response_surface_tensor_interpolate.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example compute_response_surface_tensor_interpolate">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[saving_path,options]=get_option(options, 'saving_path', []);
[gpc_full_tensor, options]=get_option(options, 'gpc_full_tensor', true);
[grid, options]=get_option(options, 'grid',  'full_tensor');
check_unsupported_options(options, mfilename);

[state, polysys, time] = funcall(init_func, init_options);
if isempty(V_u)
V_u = gpcbasis_create(polysys, 'm', state.num_params, 'p', p_u, 'full_tensor', gpc_full_tensor);
end


% compute interpolation points (need one level more than order)
[x,w] = gpc_integrate([], V_u, p_u+1, 'grid', grid);

% compute the (generalised) Vandermonde matrix
A=gpcbasis_evaluate(V_u, x);

M=gpcbasis_size(V_u,1);
Q = length(w);
u = zeros(state.num_totRV_out, Q);
for j = 1:Q
    disp(strcat(num2str(j), '/', num2str(Q)));
    x_j = x(:,j);
    u(:,j) = funcall(solve_func, state, x_j);
end

u_i_alpha = u/A;
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);

% the next line should go into a unit test
%   norm(gpc_evaluate(u_i_alpha, V_u, x)-u)

%put different variables in u_mean in different columns
 if ~isscalar(state.num_eqs);
     u_i_alpha_nontemp=u_i_alpha(end-state.num_eqs(2)+1: end,:);
     u_i_alpha=u_i_alpha(1:end-state.num_eqs(2),:);
     
     u_mean_nontemp=u_mean( end-state.num_eqs(2)+1:end);
    u_var_nontemp=u_var( end-state.num_eqs(2)+1:end);
    u_mean=u_mean(1:end-state.num_eqs(2));
    u_var=u_var(1:end-state.num_eqs(2));
 end

 u_mean=reshape(u_mean,state.num_vars,state.num_eqs(1));
 u_var=reshape(u_var,state.num_vars,state.num_eqs(1));
 
 u_i_alpha=reshape(u_i_alpha,state.num_vars,M,state.num_eqs(1));
 if nargout>0
     varargout{1}.u_i_alpha=u_i_alpha;
     varargout{1}.V_u=V_u;
     if  ~isscalar(state.num_eqs);
         varargout{1}.u_i_alpha_nontemp=u_i_alpha_nontemp;
         varargout{1}.u_mean_nontemp=u_mean_nontemp;
         varargout{1}.u_var_nontemp=u_var_nontemp;
     end
 end

 %% save results if saving_path is given
 if ~(isempty(saving_path))
     %definition of file- and foldername for saving results
        equation_name=strrep(char(init_func),'init','_');
        file_name=get_saving_filenames(saving_path,equation_name,'COLLOC',{p_u, 'interp'});
        if isscalar(state.num_eqs);
            save(file_name,'u_mean','u_var','p_u', 'polysys','u_i_alpha', 'init_options', 'state', 'V_u');
        else
            save(file_name,'u_mean','u_var','u_mean_nontemp', 'u_var_nontemp', 'p_u', 'polysys','u_i_alpha', 'u_i_alpha_nontemp', 'init_options', 'state', 'V_u');
        end
end
