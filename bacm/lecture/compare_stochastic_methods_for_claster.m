%% Init stuff
clear variables
clc;
% set path for results
pathname=pwd;
name_dir='Results_Noemi_sglib';
mkdir(name_dir);
cd(name_dir);
saving_path=pwd;
cd(pathname);

init_func{1} = @logistic_equation_init;
step_func{1} = @logistic_step_by_step;
solve_func{1} = @logistic_step_by_step;

init_func{2} = @prey_predator_init;
step_func{2} = @prey_predator_step_by_step;
solve_func{2} = @prey_predator_standardsolve;

init_func{3} = @Lorenz63_init;
step_func{3} = @Lorenz63_step_by_step;
solve_func{3} = @Lorenz63_standardsolve;

%% Start compare MC with non-intrusive Galerkin
for i=1:3
    %Declare initial parameters
    %Define random and not random parameters + time interval (see more in prey_predator_init)
    clear init_options
    clear ylablels
    if i==1
        [init_options, ylabels]=logistic_users_init_options();
    elseif i==2
        [init_options,ylabels]=prey_predator_users_init_options();
    else
        [init_options,ylabels]=Lorenz63_users_init_options();
    end

%% Monte Carlo
tic
N = 1000;
[u_mean_MC, u_var_MC, time, ref_sol] = compute_moments_mc(init_func{i}, solve_func{i}, N, saving_path, init_options, 'N_per', N/2, 'saving_all_samples', 0);
%show_mean_var('Monte-Carlo', u_mean, u_var)
figure
toc
plot_u_mean_var(time, u_mean_MC, u_var_MC,'fill_color','blue', 'ylabels', ylabels)

%% Non-intrusive Galerkin

p_u = 2;
p_int = 3;
tic
[u_mean, u_var, time]=compute_response_surface_nonintrusive_galerkin(init_func{i}, step_func{i}, [], p_int, p_u, init_options, 'grid', 'full_tensor', 'saving_path', saving_path);
toc
plot_u_mean_var(time, u_mean, u_var, 'fill_color','cyan', 'line_color','black','transparency', 0.7, 'ylabels', ylabels)

%% Plot solution with mean values
for i=1:size(u_mean,2)
    subplot(size(u_mean,2),1,i)
plot(time, ref_sol(:,i), 'red', 'LineWidth', 2)
end

end