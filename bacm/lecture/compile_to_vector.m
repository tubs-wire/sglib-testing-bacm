function comp_params=compile_to_vector(params, vector_names, ind_params, varargin)
%Compiles parameters to vector with name VECTOR_NAMES{i}
% if IND_params is not ordered, the vector will be reordered accordingly
% optionally a PARAM_ IDENTIFYER can be given, if the parameter names does
% not begin with the vector name

options = varargin2options(varargin);
[param_identifyer, options] = get_option(options, 'param_identifyer', {});
check_unsupported_options(options, mfilename);
%check should be added whether VECTOR_NAMES_IND_PARAM are really dentified
%in PARAMS
%param_names=fieldnames(params);
for i_vec=1:length(vector_names)
    ivec_size=length(ind_params{i_vec});
    ivec_name=vector_names{i_vec};
    if isempty(param_identifyer)
        i_ident=strcat(ivec_name, '_'); %name identifyer in params struct
    else
        i_ident=param_identifyer{i_vec};
    end
    comp_params.(ivec_name)=zeros(ivec_size,1);
    for i=1:ivec_size
        i_index_text=num2str(ind_params{i_vec}(i));
        iparam_name=strcat(i_ident, i_index_text);
        comp_params.(ivec_name)(i)=params.(iparam_name);
    end
end