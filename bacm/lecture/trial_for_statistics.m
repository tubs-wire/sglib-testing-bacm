
function trial_for_statistics
%Description: get quantiles, probabilites, histogram
%   [u_quant, t_span]=
%   get_quantiles_and_pdf(init_func, init_options, 'MC', N_sample, file_path, 'N_per', N_per, 'saving results', 1, 'quantiles', [0.05, 0.95])
%   [u_quant, u_prob, t_span]=
%   get_quantiles_and_pdf(init_func, init_options, 'NIGAL', N_sample, file_path, 'p_u',3,'p_int',4,'saving results', 1, 'quantiles', [0.05, 0.95])
%Output:
%   -u_quant: value of solution for the given qunatiles u_quant{j_var,1}(:,itt)
%   -u_prob: u_prob.probabilities
%            u_prob.xi
%            u_prob.histogram
%
%Input:
%1.)Input from file:    file_path: where file with data to be read from
%                       init_func: (example: @logistic_equation_init)                                         
%                       Nt_per=1; %statistics only for every Nt_perth timestep
%                       global_distrib=0 (def);  %=1:if same x_values needed for all the probability plots(not recommended)
%                       du=0.1 (def); %for local xi vector spacing for pdf and histogram resolution
%%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig
quantiles=[0.05, 0.95];
%init_func=@logistic_equation_init;
%init_func=@Lorenz63_init;
init_func = @prey_predator_init;
file_path='/home/noefried/SFB880_C4/Results_Noemi_SFB880_trial_ex';
method='MC';
%% input options
switch method
    case 'MC'
N=1000000; %when filename needed
N_per=N/10; %when filename needed
mode=''; %when filename needed
    case 'NIGAL'
N=1000000; %number of samples to draw statistics
p_u=4; %when filename needed
p_int=5; %when filename needed
end
%INIT OPTIONS FOR LGOISTIC_EQ
% init_options.list_of_RVs={'P0'};
% init_options.t_min_max=[0,15];
% init_options.P0={0.3, 0.4, 'P'};
% init_options.r={0.4, 0.6, 'P'};
% ylabels={'Population'};
% du=0.005;

%INIT OPTIONS FOR PREDATOR_PREY
init_options.list_of_RVs={'P1', 'P2'};
init_options.t_min_max=[0,30];
init_options.P1={15, 16, 'P'};
init_options.P2={20, 21, 'P'};
du=0.1;
%INIT OPTIONS FOR LORENZ 63
%init_options.t_min_max=[0,5];
%init_options.n_timesteps=5000;


%get critical spots
%get minmax points of number of prays
%% Get general parameters from init_func
[state,~, time] = funcall(init_func, init_options);
    u_mean=state.ref_sol;
tt=[];
for i=1:size(u_mean,2)
[~,ind_max]=findpeaks(u_mean(:,i));
[~,ind_min]=findpeaks(-u_mean(:,i));
tt=[tt,ind_max, ind_min];
end

tt=[1,sort(tt),length(time)];
 
%% Get quantiles for t_span(tt) or for all time values
switch method
    case 'NIGAL'
       % [u_quant, u_prob, t_span]=get_quantiles_and_pdf(init_func,init_options, method, N, 'du', du, 'file_path', file_path, 'p_u', p_u, 'p_int', p_int, 'saving_results', 1, 'quantiles', quantiles, 'Nt_per',17, 'get_histogram', 0, 'global_distrib',0);
         [u_quant, u_prob, t_span]=get_quantiles_and_pdf(init_func,init_options, method, N, 'du', du, 'file_path', file_path, 'p_u', p_u, 'p_int', p_int, 'saving_results', 1, 'quantiles', quantiles, 'tt',tt, 'get_histogram', 0, 'global_distrib',0);

    case 'MC'
        %[u_quant, u_prob, t_span]=get_quantiles_and_pdf(init_func,init_options, method, N, 'du', du,'file_path', file_path, 'N_per', N_per, 'saving_results', 1, 'Nt_per',17, 'quantiles', quantiles, 'get_histogram', 1, 'global_distrib',0);
        [u_quant, u_prob, t_span]=get_quantiles_and_pdf(init_func,init_options, method, N, 'du', du,'file_path', file_path, 'N_per', N_per, 'saving_results', 1, 'tt',tt, 'quantiles', quantiles, 'get_histogram', 1, 'global_distrib',0);

end

%% Plot 
%ylabels={{'x: number of preys'}; {'y: number of predators'}};
ylabels={{'population'}};
t_max=max(time);
subplot_dim=[ceil(length(tt)/3),3];
%tt=round([3,5,10, 15, 20, 25, 30]/t_max*(length(time)-1));
%tt=[1,tt]
%%
switch method
    case 'MC'
        file_name=get_saving_filenames(init_func,method,file_path, N, mode);
    case 'NIGAL'
        file_name=get_saving_filenames(init_func,method,file_path, [p_u, p_int], '');
end
%load(strcat(file_path, filesep, 'prey_predator__1000000sampledMC_statistics.mat'))
statistics_result=load(file_name);
u_prob=statistics_result.u_prob;
u_quant=statistics_result.u_quant;
%%
MC_stat=load(strcat(file_path, filesep, 'prey_predator__1000000sampledMC_statistics.mat'));
figure_handles=plot_pdf_or_hist(MC_stat.u_prob,time, 'tt', tt, 'get_histogram', 0, 'ylabels', ylabels, 'global_distrib',0, 'subplot_dim', subplot_dim, 'saving_results', 0, 'method_name', 'MC_10^6');
%load(strcat(file_path, filesep, 'prey_predator__pu3_pint6_Gal_nonint_statistics.mat'))
load(strcat(file_path, filesep, 'prey_predator__pu12_pint13full_tensor_projection_statistics.mat'))
plot_pdf_or_hist(u_prob,time, 'tt', tt, 'subplot_dim', subplot_dim, 'method_name', 'PseudoGal gpc12', 'fig_handles', figure_handles, 'line_color', 'blue', 'LineWidth', 1, 'multiple_plots', 1, 'ylabels', ylabels)
%plot_pdf_or_hist(u_prob,time)
plot3D_pdf(u_prob, u_mean, time, 'tt', tt, 'ylabels', ylabels,'global_distrib',1,'u_quant', u_quant, 'quantiles', quantiles);


%plot quantiles

plot_u_mean_quant_pdf(time, u_mean, u_quant, 'u_prob', u_prob, 'tt', tt,  'ylabels', ylabels, 'method_name', 'MC')
hold on
plot_u_mean_quant_pdf(time, u_mean, u_quant, 'u_prob', u_prob, 'tt', tt,  'ylabels', ylabels, 'method_name', 'NIGALpu3pint6','fill_color','red', 'line_color','black','transparency', 0.4 )
end
%%
function file_name=get_saving_filenames(init_func,method,file_path, N, mode)
equation_name=strrep(char(init_func),'init','_');
switch method
    case 'MC'
        file_name=strcat(file_path,filesep,equation_name,num2str(N),'sampled','MC', mode,'_statistics', '.mat');
       
    case 'NIGAL'
        p_u=N(1);p_int=N(2);
        file_name=strcat(file_path,filesep,equation_name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint', '_statistics','.mat');
    end
end
