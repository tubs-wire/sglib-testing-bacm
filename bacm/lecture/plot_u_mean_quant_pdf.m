function plot_u_mean_quant_pdf(time, u_mean, u_quant, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%to plot mean and quantiles (with colored regions) of stochastic solution of ODE
%-in function of the TIME
%-mean value:   u_mean
%-variance:     u_var

options=varargin2options(varargin);
[u_prob,options]=get_option(options, 'u_prob', {});
[fill_color,options]=get_option(options, 'fill_color', 'blue');
[line_color,options]=get_option(options, 'line_color', 'yellow');
[transparency,options]=get_option(options, 'transparency', 1);
[ylabels,options]=get_option(options, 'ylabels', {});
[tt,options]=get_option(options, 'tt', []);
[n_plots,options]=get_option(options, 'n_plots', 10);
[global_distrib,options]=get_option(options, 'global_distrib', 0);
[line_color,options]=get_option(options, 'line_color', 'red');
[LineWidth, options]=get_option(options, 'LineWidth', 2);
[subplot_dim,options]=get_option(options, 'subplot_dim', []);
[method_name,options]=get_option(options, 'method_name', {});
check_unsupported_options(options, mfilename);

%% create automatic y labels

numplots=size(u_mean,2);
if isempty(ylabels)
    ylabels(1:numplots,1)={''};
    for i=1:numplots
        ylabels{i,:}={strcat('Y',num2str(i))};
    end
end

%% plot mean and quantiles
if ~ (size(time,1)==1)
    time=time';
end
%time and flipped time    
T=[time,fliplr(time)];
%subplot definitions
if isempty(subplot_dim)
if numplots>3
        n=2;
        m=ceil(numplots/hornumb);
    else
       n=1;
       m=numplots;
end

end
    
for i=1:numplots
    subplot(m,n,i)
%fill plot of quantiles        
U_min_max=[u_quant{i,1}(1,:),fliplr(  u_quant{i,1}(2,:)  )];
fill(T',U_min_max,fill_color,'FaceAlpha', transparency);
hold on
% plot mean
plot(time,u_mean(:,i),'LineWidth',LineWidth,'color',line_color)
%labels
ylabel(ylabels{i},'FontSize',16)
xlabel('time','FontSize',16)
end

%% get probabilities at certain timespots
if ~isempty(u_prob)
    miniplot_options.line_color=fill_color;
    if ~isempty(tt)
    miniplot_options.tt=tt;
    end
    if isempty(subplot_dim)
    miniplot_options.subplot_dim=[m,n];
    else
    miniplot_options.subplot_dim=subplot_dim; 
    end
    if ~isempty (n_plots)
        miniplot_options.n_plots=n_plots;
    end
    if ~isempty (method_name)
        miniplot_options.method_name=method_name;
    end
    miniplot_options.LineWidth=1;
    miniplot_options.global_distrib=global_distrib;
    get_minipdf_plots(u_prob, time, miniplot_options);
end    
end

