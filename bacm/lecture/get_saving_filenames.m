function file_name=get_saving_filenames(file_path, name,  method, p, varargin)
%
%Description: gives filenamames for saving stochastic solution/statistics for
%different methods
%   file_name=get_saving_filenames(file_path, init_func, p, 'endtext',
%   'statistics')

%Output:
%   filenames (for multiple file in a cell vector format)
%
%Input:

%-file_path
%-name - name of the equation (e.g. prey_predator_predator)
%-method
%   -'MC'-Monte Carlo
%   -'NIGAL' - Nonintrusive Galerkin
%   -'DIRINTEG' - Direct Integration
%   -'PROJ' - Projection
%   -'COLLOC' - Collocation (interpolation/regression)
% Optional input:
%   -'endtext': specific end of the filename

%-p (needed parameters)
%   -for method='MC' p{1}=N
%                    p{2}=mode (''/'qmc'/'lhs')
%                    p{3}=N_per(for multiple files - optional)
%   -for method='NIGAL'
%                    p{1}=p_u (gPCE degree)
%                    p{2}=p_int (degree of quadrature rule)
%   -for method= 'DIRINTEG'
%                    p{1}=p_int;
%                    p{2}=points_type; %'smolyak'/'full_tensor'
%   -for method= 'PROJ'
%                    p{1}=p_u (gPCE degree)
%                    p{2}=p_int (degree of quadrature rule)
%                    p{3}=points_type; %'smolyak'/'full_tensor'
%   -for method= 'COLLOC'
%                    p{1}=p_u (gPCE degree)
%                    p{2}=mode ('interp'/'regression')
%
%   By Noemi FRIEDMAN
%   Institute of Scientific Computation
%   TU Braunschweig

options=varargin2options(varargin);
[endtext,options]=get_option(options, 'endtext', '');
check_unsupported_options(options, mfilename);

switch method
    case 'MC'
        N=p{1};
        mode=p{2};
        if length(p)>2
            N_per=p{3};
        else
            N_per=[];
        end
        
        if isempty(N_per)
            file_name=strcat(file_path,filesep,name,num2str(N),'sampled','MC', mode,endtext, '.mat');
        else %if multiple filenames
            n_files=ceil(N/N_per);
            file_name=cell(n_files,1);
            for i=1:n_files-1
                file_name{i,1} = strcat(file_path,filesep,name,num2str(N),'sampled','MC', mode,'_part_',num2str(i),'all_samples', '.mat');
            end
            file_name{n_files,1} = strcat(file_path,filesep,name,num2str(N),'sampled','MC',mode,'_final','_all_samples', '.mat');
        end
    case 'NIGAL'
        p_u=p{1};
        p_int=p{2};
        
        file_name=strcat(file_path,filesep,name,'pu',num2str(p_u),'_pint',num2str(p_int),'_Gal_nonint', endtext,'.mat');
    case 'DIRINTEG'
        p_int=p{1};
        points_type=p{2}; %'smolyak'/'full_tensor'
        
        file_name=strcat(file_path,filesep,name,'_pint',num2str(p_int),points_type,'_direct_int', endtext,'.mat');
    case 'PROJ'
        p_u=p{1};
        p_int=p{2};
        points_type=p{3}; %'smolyak'/'full_tensor'
        
        file_name=strcat(file_path,filesep,name,'pu',num2str(p_u),'_pint',num2str(p_int),points_type,'_projection', endtext,'.mat');
    case 'COLLOC'
        p_u=p{1};
        mode=p{2}; %'interp'/'regression'
        
        file_name=strcat(file_path,filesep,name,'pu',num2str(p_u),mode,'_colloc', endtext,'.mat');
        
end

end