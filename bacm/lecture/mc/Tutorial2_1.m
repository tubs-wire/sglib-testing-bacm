function Tutorial2_1()
%% Approximation of $\pi$ - convergence of the MC
%
% Here we observe the convergence of the MC or the QMC method by
% checking the convergence rate of the the estimation of the value of $\pi$
% with MC or QMC method (see more in subfunction ESTIMATE_PI_BY_MC).

%% Initial setting

mode='MC';

%% Try with different sample number

% Run estimation with different number of MC samples
N = 2.^(1:22);
%
% Initiate memory for the pi estimation
pi_est=zeros(1,length(N));

for i=1:length(N);
    % estimate pi (for some N values plot the darts and the circle)
    % (see sub-function ESTIMATE_PI_BY_MC bellow)
    if i==5||i==10||i==15
        figure
        pi_est(i) = estimate_pi_by_MC(N(i),mode, 'flag_plot', true);
    else
        pi_est(i) = estimate_pi_by_MC(N(i),mode);
    end
    % Display the number of darts and the estimate for $\pi$:
    display(strvarexpand('N=$N(i)$, $pi_est(i)$'));
end

%% Check convergence rate

% Error of the pi estimate
err = abs(pi_est-pi);

% Plot the error
loglog(N, err, 'x-')

% Convergence rate
c = [ones(size(N')),log(N')]\log(err');

% Display convergence rate
display(strvarexpand('The convergence rate is $c(2)$'))
hold on
loglog(N, exp(c(1))*N.^c(2))
xlabel('N (logscale)')
ylabel('|\pi-\pi_{est}| (logscale)')
end

%% Function ESTIMATE_PI_BY_MC for estimating the value of $\pi$ 
% This is a subfunction to approximate $\pi$
%
% Here we approximate the the value of $\pi$ with Monte Carlo (mode ='MC') or
% Quasi Monte Carlo (mode='QMC' methods.
% The estimation is calculated by throwing N darts and approximate the ratio
% of the area of the circle and the area of the  square by the ratio of the
% darts falling within the unit circle and the total number of darts.  
%
% Ratio of the area of the circle with radius $r$ and the area of the
% square with sidelengths $r$
% $q=\frac{r^2\pi}{4r^2}$
% Rearranging for $\pi$
% $\pi=4q$

function pi_est = estimate_pi_by_MC(N, mode, varargin)


% Define optional inputs
options=varargin2options(varargin);
[flag_plot,options]=get_option(options, 'flag_plot', false);
check_unsupported_options(options, mfilename);

% Throw darts to the squared domain $\Omega\in [0,1]\times[0,1]$.
    % The random x1,x2 coordinates of the darts:
    switch mode
        case 'MC'
            x = rand(2,N);
        case 'QMC'
            x = (halton_sequence(N,2))';
    end
    
    % Get index of darts within the circle:
    ind = find(x(1,:).^2+x(2,:).^2<1);
    
    % Estimate of the ratio $q$:
    q_est = length(ind)/N;
    
    % Estimate of $\pi$:
    pi_est =q_est*4;
    
    % Plot darts and circle
    if flag_plot
        % plot circle
        plot(linspace(0,1,100), sqrt(1-(linspace(0,1,100).^2)), 'LineWidth',2)
        hold on
        % plot darts within the circle
        scatter(x(1,ind), x(2,ind), 'b');
        % plot darts that are not within the circle
        nind=setdiff(1:N, ind);
        scatter(x(1,nind), x(2,nind), 'r');
        title(strvarexpand('N=$N$, estimate for pi: $pi_est$'))
        hold off
    end
end