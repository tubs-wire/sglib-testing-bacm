%% Init stuff
startup
clear variables;
clc;
close_system 'SFB880_Library.mdl'
close_system 'DLR_SFB880_Trim_Quat'
close_system 'DLR_SFB880_Test_Quat'
init_func = @highlift_eigen_init;
solve_func = @highlift_eigen_solve;

clear global BACM;
clear global BACM_Trim;
clear global TrimSettings;
global BACM;
global BACM_Trim;
global TrimSettings;
%% set path for results (in a folder in the root, where sglib is saved)
 pathname=pwd;
 name_root_dir=fileparts(fileparts(which('startup.m')));
 cd(name_root_dir);
 name_dir='Results_Noemi_sglib_SFB880_eigenfreq';
 mkdir(name_dir);
 cd(name_dir);
 saving_path=pwd;
 cd(pathname);
%% Add path for the model (deterministic solver)
parent_dir=fileparts(fileparts( mfilename('fullpath') ));
 addpath( fullfile(parent_dir,'models', 'aircraft_models') );
 addpath(genpath( fullfile(parent_dir,'models', 'aircraft_models', 'full_BACM_stoch_trigger_eigenmotions_2015') ));
 
 

%%
%Define random and not random parameters + time interval (see more in prey_predator_init)
% init_options.list_of_RVs={'C_L0', 'C_Lalpha'};
% %init_options.list_of_RVs={'C_L0', 'C_mu', 'C_m0', 'C_Lalpha', 'C_D0'};
% init_options.C_mu={0.0313, 0.0346, 'P'};
% init_options.V_TAS={54.5, 55.5, 'P'};
% init_options.C_L0={0.2445, 0.4445, 'P'};
% init_options.C_Lalpha={4.49768, 4.97112, 'P'};
% init_options.C_D0= {0.0188, 0.023, 'P'};
% init_options.k1= {0.0105, 0.0105, 'P'};
% init_options.k2={0.0351, 0.0351, 'P'};
% init_options.C_m0= {-0.1373, -0.1015, 'P'};
% init_options.C_malpha={0.9370, 1.0356, 'P'};
% init_options.alpha_wfc_max={18*pi/180, 22*pi/180, 'P'};
 init_options.list_of_RVs={'C_L0', 'C_Lalpha'};
init_options.t_min_max= [0,100];
%init_options.list_of_RVs={'C_L0', 'C_mu', 'C_m0', 'C_Lalpha', 'C_D0'};
ylabels={{'Altitude [m]'};{'V_{TAS}: True air speed [m/s]'};{'\alpha: angle of attack [degree]'};{'q: pitch rate [degree/s]'};{'\theta : pitch altitude [degree]'} };


%% Monte Carlo

N_MCL = 1;
[u_mean, u_var, time, ref_sol] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options);
%show_mean_var('Monte-Carlo', u_mean, u_var)
figure
numbplots=size(u_mean,2);
for i=1:numbplots
    subplot(numbplots/2,2,i)
plot(time, ref_sol(:,i), 'red', 'LineWidth', 3)
end
plot_u_mean_var(time, u_mean, u_var,'fill_color','blue', 'ylabels', ylabels)


%% Quasi Monte Carlo

[u_mean, u_var, time] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options, 'mode', 'qmc');
%show_mean_var('Quasi Monte-Carlo', u_mean, u_var)

plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4 , 'ylabels', ylabels)


%% Latin hypercube

[u_mean, u_var,time] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options, 'mode', 'lhs');
%show_mean_var('Latin hypercube', u_mean, u_var)

plot_u_mean_var(time, u_mean, u_var, 'fill_color','yellow', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

%% Direct integration full tensor grid

p_integration_ord = 5;
[u_mean, u_var, time] = compute_moments_quad(init_func, solve_func, p_integration_ord, init_options, 'grid', 'full_tensor');
%show_mean_var('Full tensor grid integration', u_mean, u_var);
figure
plot_u_mean_var(time,  u_mean, u_var, 'fill_color','black', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)


%% Direct integration sparse grid

p_integration_ord = 3;
[u_mean, u_var, time] = compute_moments_quad(init_func, solve_func, p_integration_ord, init_options, 'grid', 'smolyak', 'saving_path', saving_path);
%show_mean_var('Sparse grid (Smolyak) integration', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','green', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

%% Projection with full tensor grid
p_gPCE_order = 8;
%p_int = [7, 16];
p_integration_ord=9;
[u_mean, u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_integration_ord, p_gPCE_order, init_options, 'grid', 'full_tensor', 'saving_path', saving_path);
%show_mean_var('Projection (L_2, response surface, tensor)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','cyan', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%% Projection with sparse grid
p_gPCE_order = 3;        %gPCE order
p_integration_ord = 4;      %integration order to calculate all other gpce coeff-s
p_int_proj=4;   %integration order to calculate initial gpce coeff-s from galerkin projection

[u_mean,u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_integration_ord, p_gPCE_order, init_options, 'grid', 'smolyak', 'saving_path', saving_path);
%show_mean_var('Projection (L_2, response surface, sparse)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
plot_u_mean_var(time, u_mean(:,1:4), u_var(:,1:4), 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)


%% Full tensor grid collocation (interpolation)

p_u = 3;
[u_mean,u_var, time] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options);

%ind=(multiindex_order(V_u{2})>=3);
%u_i_alpha(:,ind)=0;


%show_mean_var('Interpolation, tensor (response surface)', u_mean, u_var);
plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_tensorcoll_i_alpha = u_i_alpha;

%% Sparse grid collocation (regression)

%% Non-intrusive Galerkin
%init_func = @electrical_network_init;
%solve_func = @electrical_network_solve;
%step_func = @electrical_network_picard_iter_step;


p_u = 3;
p_int = 4;

[u_mean, u_var, time]=compute_response_surface_nonintrusive_galerkin(init_func, step_func, [], p_int, p_u, init_options, 'grid', 'full_tensor');
plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_galerkin_i_alpha = u_i_alpha;

%%
%i=1;
%u_plot=[u_galerkin_i_alpha(i, :); u_proj_i_alpha(i, :)];
%u_plot(1,1)=u_plot(1,1)+0.001;
%plot_response_surface(u_plot, V_u)
