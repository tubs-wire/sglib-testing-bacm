function gpc_sobol_vars()
%ylabels={{'Altitude [m]'};{'V_{TAS}: True air speed [m/s]'};{'\alpha: angle of attack [degree]'};{'q: pitch rate [degree/s]'};{'\theta : pitch altitude [degree]'} };
ylabels={{'Altitude [m]'};{'V_{TAS}: True air speed [m/s]'};{'\alpha: angle of attack [degree]'};{'q: pitch rate [degree/s]'};{'\theta : pitch altitude [degree]'} };

figure_names={['Altitude'];['V_TAS']; ['alpha'];['q_pitch_rate'];['theta_pitch_alti'] };
extra_fig_name='all_coeff_rand_devmax';
%extra_fig_name='4_lift_coeff_rand_';
%RV_names={'C_{L0}', 'C_{L\alpha}',   '\Delta C_{L\alpha-FL}','\Delta C_{L\alpha-DN}'} ;
RV_names={'C_{\mu}', 'C_{L0}', 'C_{L\alpha}',   '\Delta C_{L\alpha-FL}','\Delta C_{L\alpha-DN}', 'C_{D0}',  'k_1',    'k_{1-FL}',  'k_{1-DN}',  'k_2',  'k{2-FL}', 'k{2-DN}', 'C_{m0}' , 'C_{m\alpha}', 'C_{m\alpha-FL}',  'C_{m\alpha-DN}'}; 
lineStyles ={'-', '--', ':', '-.', '-', '--', ':', '-.', '-', '--', ':', '-.', '-', '--', ':', '-.'};
%saving_path=['/home/noefried/Results_Noemi_sglib_SFB880_eigenfreq_lift/'];
saving_path='/home/noefried/Results_Noemi_sglib_SFB880_eigenfreq/all_random/';
%load('/home/noefried/Results_Noemi_sglib_SFB880_eigenfreq_lift/highlift_eigen__pu2_pint3smolyak_projection.mat')
load('/home/noefried/Results_Noemi_sglib_SFB880_eigenfreq/all_random/highlift_eigen__pu2_pint3smolyak_projection.mat')
multi_ind=V_u{2};
polysys=V_u{1};


gpc_size=gpcbasis_size(V_u, 1);
num_RVs=gpcbasis_size(V_u, 2);
num_vars=size(u_i_alpha,1);
num_t=size(u_i_alpha,3);
problem_size=num_t*num_vars;
time=state.t_span;
%% resize coefficients and variance if necessary
%resize u_i_alpha
u_r_alpha=zeros(problem_size, gpc_size);
for i=1:size(u_i_alpha,1)
    u_r_alpha( (i-1)*num_t+1: i*num_t,:)=squeeze(u_i_alpha(i,:,:))';
end

%resize u_var
u_r_var=reshape(u_var, problem_size, 1);



%% Cut the mixed polynomial terms from the gPCE

ind=find(multi_ind);
V_V=multi_ind;
V_V(ind)=1;  %set all nonzero terms one
Vrow_ind=find(V_V*ones(num_RVs,1)==1); %get indices or rows of univariate polynomials
cuted_multi_index=multi_ind(Vrow_ind, :); %keep only multiidices of univariate polynomials

%% difference between variance from cuted expansion and origianl variance:
u_r_alpha_cut=u_r_alpha(:,Vrow_ind);
[~, u_r_var_cut]=gpc_moments(u_r_alpha_cut,  {polysys, cuted_multi_index});
norm(u_r_var_cut-u_r_var)/norm(u_r_var);

u_f_alpha_cut=u_i_alpha_nontemp(:,Vrow_ind);
[~, u_f_var_cut]=gpc_moments(u_f_alpha_cut,  {polysys, cuted_multi_index});
norm(u_f_var_cut-u_var_nontemp)/norm(u_var_nontemp)

%% Calculate partial variances
row_ind=cell(1,num_RVs);
% partial variance for Sobol index
u_i_var=zeros(problem_size, num_RVs);
u_if_var=zeros(length(u_mean_nontemp), num_RVs);
for i=1:num_RVs
    row_ind{i}=find(cuted_multi_index(:,i));
    [~, u_i_var(:,i)]=gpc_moments( u_r_alpha_cut(:,row_ind{i}) ,  {polysys, cuted_multi_index(row_ind{i} , :)});
    [~, u_if_var(:,i)]=gpc_moments( u_f_alpha_cut(:,row_ind{i}) ,  {polysys, cuted_multi_index(row_ind{i} , :)});
    
end
% Sobol index from deviding the partial vars with var
    %sobol_index=u_i_var./repmat(u_r_var_cut, 1, num_RVs);
% Sobol index from deviding the partial vars with the max var
max_var_devid=1./max(u_var);
max_var_devid=reshape(ones(num_t,1)*max_var_devid,1,problem_size);

sobol_index=repmat(max_var_devid',1,num_RVs).*u_i_var;
sobol_index_f=u_if_var./repmat(u_f_var_cut, 1, num_RVs);

%% plot sensitivites and save plots
if ~ (size(time,1)==1)
    time=time';
end
    
T=[time,fliplr(time)];
for i=1:num_vars
    figure_h(i)=figure('Position', [100, 100, 600, 895]);
    
subplot(3,1,1) %plot mean values and 3 sigma region
    ind=(i-1)*num_t+1:i*num_t;
    %plot(state.t_span, u_mean(1:10001,1))
    U_min_max=[( u_mean(:,i)-3* sqrt(u_r_var_cut(ind))  )',fliplr((u_mean(:,i)+3* sqrt(u_r_var_cut(ind)))')];
    fill(T',U_min_max,'cyan','FaceAlpha', 0.8);
    hold on
    plot(time,u_mean(:,i),'LineWidth',2,'color','black')
    %xlabel('t: time [s]', 'FontSize', 16)
    title('Mean value and its 3 \sigma region', 'FontSize',18)
    ylabel(ylabels{i}, 'FontSize', 16)
    h_legend= legend('3 \sigma region', 'mean value');
      set(h_legend,'FontSize',14);
subplot(3,1,2) %plot variance and partial variances
    %plot variances
        %plot(state.t_span, u_r_var(ind), 'black', 'LineWidth', 2.5),
     %plot variance of coefficients
        plot(state.t_span, 100*u_r_var(ind)/max(u_mean(:,i)), 'black', 'LineWidth', 2.5),
    
     hold on
     h_plot=plot(state.t_span, 100*u_i_var(ind,:)/max(u_mean(:,i)), 'LineWidth', 1.5);
     h_legend= legend('total variance',RV_names{1:num_RVs});
     set(h_legend,'FontSize',14);
     %xlabel('t: time [s]', 'FontSize', 16)
    title('Total and partial coefficients of variance', 'FontSize', 18)
    ylabel('total and partial variances [%]', 'FontSize', 16)
    for iplot=1:length(h_plot)
         set(h_plot(iplot),'LineStyle',lineStyles{iplot})
    end
    
subplot(3,1,3) %plot sobolev indices
    h_plot=plot(state.t_span, sobol_index(ind,:)*100, 'LineWidth', 1.5);
    %legend('C_{L0}', 'C_{L\alpha}',   '\Delta C_{L\alpha-FL}',  '\Delta C_{L\alpha-DN}')
     %h_legend= legend(RV_names , 'FontSize', 16);
     %h_legend=columnlegend(2,RV_names , 'FontSize', 16,  'linestyle',  lineStyles{1:num_RVs});
     %set(h_legend,'FontSize',14);
    xlabel('t: time [s]', 'FontSize', 16)
    title('Sobol indices', 'FontSize', 18)
    ylabel('Partial var/ max total var [%]', 'FontSize', 16)
  
    for iplot=1:length(h_plot)
         set(h_plot(iplot),'LineStyle',lineStyles{iplot})
    end
   %save figure
   saveas(figure_h(i), [saving_path, extra_fig_name, figure_names{i}, '.fig'])
   saveas(figure_h(i), [saving_path, extra_fig_name, figure_names{i}, '.eps'])
   saveas(figure_h(i), [saving_path, extra_fig_name, figure_names{i}, '.pdf'])
    end

%% plot frequencies and damping ratio samples and save figure
% sample the avaraged frequencies and damping ratios
h_sample=figure;
samples=gpc_sample(u_i_alpha_nontemp(9:10,:) , V_u, 10000); %phugoid freq
scatter(-samples(2,:), samples(1,:));
hold on
scatter(-state.ref_freq{1}(10), state.ref_freq{1}(9), 'red',  'LineWidth', 2);
set(gca,'fontsize',16)
xlabel('\sigma: damping ratio', 'FontSize', 16)
ylabel('\omega: eigenfrequency [rad/s]', 'FontSize', 16)
title('Angular frequency and damping ratio of the phugoid motion', 'FontSize', 18)
%save figure
   saveas(h_sample, [saving_path, extra_fig_name,'freq_samples', '.fig'])
   saveas(h_sample, [saving_path, extra_fig_name, 'freq_samples', '.eps'])
   saveas(h_sample, [saving_path, extra_fig_name, 'freq_samples', '.pdf'])
   
    %% plot frequency sensitivites (trial)
% h_freq_sensit=figure;
% y = [sobol_index_f(9,:); sobol_index_f(10,:)];
% h_bars=bar(y,'stacked');
% title('Sensitivity of phugoid motion due to different aerodynamical parameters', 'FontSize', 18)
%  set(gca,'XTickLabel',{'angular frequency', 'damping ratio'})
%  set(gca,'fontsize',16)
%  legend(fliplr(h_bars), fliplr(RV_names)) 
 
 %% plot phugoid frequency sensitivites
 h_freq_sensit=figure;
y = [sobol_index_f(9,:); sobol_index_f(10,:)];
h_bars=bar(y*100);
title('Sensitivity of phugoid motion due to different aerodynamical parameters', 'FontSize', 18)
 set(gca,'XTickLabel',{'angular frequency', 'damping ratio'})
 set(gca,'fontsize',18)
 h_legend= legend(RV_names , 'FontSize', 16);
 ylabel('Sobol index [%]', 'FontSize', 18)
 %save figure
   saveas(h_freq_sensit, [saving_path, extra_fig_name,'freq_sensit', '.fig'])
   saveas(h_freq_sensit, [saving_path, extra_fig_name, 'freq_sensit', '.eps'])
   saveas(h_freq_sensit, [saving_path, extra_fig_name, 'freq_sensit', '.pdf'])
   
   %% plot samples of short period frequencies and damping ratio
   h_sample=figure;
samples=gpc_sample(u_i_alpha_nontemp(6:7,:) , V_u, 10000);

scatter(-samples(2,:), samples(1,:));
hold on
scatter(-state.ref_freq{1}(7), state.ref_freq{1}(6), 'red',  'LineWidth', 2);
plot(xlim, [0,0], 'black')
set(gca,'fontsize',16)
xlabel('\sigma: damping ratio', 'FontSize', 16)
ylabel('\omega: eigenfrequency [rad/s]', 'FontSize', 16)
title('Angular frequency and damping ratio of the short period motion', 'FontSize', 18)
%save figure
   saveas(h_sample, [saving_path, extra_fig_name,'freq_samples_sp', '.fig'])
   saveas(h_sample, [saving_path, extra_fig_name, 'freq_samples_sp', '.eps'])
   saveas(h_sample, [saving_path, extra_fig_name, 'freq_samples_sp', '.pdf'])
   
    %% plot  short period frequency sensitivites
 h_freq_sensit=figure;
y = [sobol_index_f(7,:); sobol_index_f(6,:)];
h_bars=bar(y*100);
title('Sensitivity of short period motion due to different aerodynamical parameters', 'FontSize', 18)
 set(gca,'XTickLabel',{'angular frequency', 'damping ratio'})
 set(gca,'fontsize',18)
 h_legend= legend(RV_names , 'FontSize', 16);
 ylabel('Sobol index [%]', 'FontSize', 18)
 %save figure
   saveas(h_freq_sensit, [saving_path, extra_fig_name,'freq_sensit_sp', '.fig'])
   saveas(h_freq_sensit, [saving_path, extra_fig_name, 'freq_sensit_sp', '.eps'])
   saveas(h_freq_sensit, [saving_path, extra_fig_name, 'freq_sensit_sp', '.pdf'])
 