hold off


N=100;
multiplot_init(1,3)
clc


multiplot
strvarexpand('Equiprob');
x = normal_invcdf(linspace(0+0.5/N, 1-0.5/N, N));
plot_density(x, 'rug', true)
hold all
legend_add('normal');
strvarexpand('Orig Mean: $mean(x)$ var: $var(x)$');

y = bootstrap_sample(x, 10000, 1);
plot_density(y, 'rug', true)
legend_add('bootstrap');
strvarexpand('Boot Mean: $mean(y)$ var: $var(y)$');


multiplot
strvarexpand('MC');
x = randn(N, 1);
plot_density(x, 'rug', true)
hold all
legend_add('normal');
strvarexpand('Orig Mean: $mean(x)$ var: $var(x)$');

y = bootstrap_sample(x, 10000, 1);
plot_density(y, 'rug', true)
legend_add('bootstrap');
strvarexpand('Boot Mean: $mean(y)$ var: $var(y)$');


multiplot
strvarexpand('QMC');
x = normal_invcdf(halton_sequence(N, 1));
plot_density(x, 'rug', true)
hold all
legend_add('normal');
strvarexpand('Orig Mean: $mean(x)$ var: $var(x)$');

y = bootstrap_sample(x, 10000, 1);
plot_density(y, 'rug', true)
legend_add('bootstrap');
strvarexpand('Boot Mean: $mean(y)$ var: $var(y)$');


hold off
