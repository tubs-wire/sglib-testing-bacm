%% Init stuff
tic
init_func = @logistic_equation_init;
%solve_func = @logistic_equation_standardsolve;
step_func = @logistic_step_by_step;
solve_func=@logistic_equation_solve;

% set path for results
pathname=pwd;
name_dir='Results_Noemi_sglib';
mkdir(name_dir);
cd(name_dir);
saving_path=pwd;
cd(pathname);
toc

%Define random and not random parameters + time interval (see more in logistic_init)
% init_options.list_of_RVs={'P0'};
% init_options.t_min_max=[0,15];
% init_options.P0={0.3, 0.4, 'P'};
% init_options.r={0.4, 0.6, 'P'};
% [params.r, options] = get_option(options, 'r', {0.4, 0.6, 'P'});
% [params.k, options] = get_option(options, 'k', {1,0.1,'H'});

P0=SimParameter('P0', UniformDistribution(0.3,0.4));
r=SimParameter('r', fix_bounds(BetaDistribution(3,3), 0.4, 0.6));
k=SimParameter('k', LogNormalDistribution(1,0.4));
params=SimParamSet(P0, r,k);

%% Monte Carlo
tic
N = 10000;
[u_mean, u_var, info] = compute_moments_mc_mod(init_func, solve_func, params, N, []);
%show_mean_var('Monte-Carlo', u_mean, u_var)
figure
plot_u_mean_var(time, u_mean, u_var)
plot(info.time, info.ref_sol, 'black', 'LineWidth', 3)
toc
%% Quasi Monte Carlo

[u_mean, u_var, info] = compute_moments_mc_mod(init_func, solve_func, params, N, [], 'mode', 'qmc');
%show_mean_var('Quasi Monte-Carlo', u_mean, u_var)
plot_u_mean_var(info.time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4 )

%% Latin hypercube

[u_mean, u_var, info] =  compute_moments_mc_mod(init_func, solve_func, params, N, [], 'mode', 'lhs');
%show_mean_var('Latin hypercube', u_mean, u_var)
plot_u_mean_var(info.time, u_mean, u_var, 'fill_color','yellow', 'line_color','black','transparency', 0.4)

%% Direct integration full tensor grid

p_int = 5;
[u_mean, u_var, info] = compute_moments_quad_mod(init_func, solve_func, params, p_int, 'grid', 'full_tensor');
%show_mean_var('Full tensor grid integration', u_mean, u_var);
plot_u_mean_var(info.time, u_mean, u_var, 'fill_color','black', 'line_color','black','transparency', 0.4)

%% Direct integration sparse grid

p = 5;
[u_mean, u_var, info] = compute_moments_quad_mod(init_func, solve_func, params, p_int, 'grid', 'smolyak');
%show_mean_var('Sparse grid (Smolyak) integration', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','green', 'line_color','black','transparency', 0.4)

%% Projection with full tensor grid
p_u = 3;
p_int = 7;
[u_mean, u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_int, p_u, init_options, 'grid', 'full_tensor');
%show_mean_var('Projection (L_2, response surface, tensor)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','cyan', 'line_color','black','transparency', 0.4)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%% Projection with sparse grid
p_u = 3;        %gPCE order
p_int = 3;      %integration order to calculate all other gpce coeff-s
p_int_proj=3;   %integration order to calculate initial gpce coeff-s from galerkin projection

[u_mean,u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_int, p_u, init_options, 'grid', 'smolyak');
%show_mean_var('Projection (L_2, response surface, sparse)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4)


%% Full tensor grid collocation (interpolation)

p_u = 3;
[u_mean,u_var, time] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options);

%ind=(multiindex_order(V_u{2})>=3);
%u_i_alpha(:,ind)=0;


%show_mean_var('Interpolation, tensor (response surface)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_tensorcoll_i_alpha = u_i_alpha;

%% Sparse grid collocation (regression)

%% Non-intrusive Galerkin
%init_func = @electrical_network_init;
%solve_func = @electrical_network_solve;
%step_func = @electrical_network_picard_iter_step;
tic

p_u = 4;
p_int = 7;

[u_mean, u_var, time]=compute_response_surface_nonintrusive_galerkin(init_func, step_func, [], p_int, p_u, init_options, 'grid', 'full_tensor', 'saving_gpc_coefffs',1);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4)
toc
% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_galerkin_i_alpha = u_i_alpha;

%%
%i=1;
%u_plot=[u_galerkin_i_alpha(i, :); u_proj_i_alpha(i, :)];
%u_plot(1,1)=u_plot(1,1)+0.001;
%plot_response_surface(u_plot, V_u)
