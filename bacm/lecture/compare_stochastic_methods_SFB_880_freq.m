%% Init stuff
close_system 'SFB880_Library.mdl'
close_system 'DLR_SFB880_Test_Quat'
close_system 'DLR_SFB880_Trim_Quat'
clear all
close all 
clc
pathname=pwd;
name_sglib_dir=(fileparts(which('startup.m')));
name_root_dir=fileparts(name_sglib_dir);
cd(name_sglib_dir);
startup

%% Add path for the model (deterministic solver)
parent_dir=fileparts(fileparts( mfilename('fullpath') ));
 addpath( fullfile(parent_dir,'models', 'aircraft_models') );
 addpath(genpath( fullfile(parent_dir,'models', 'aircraft_models', 'fullflap_sim_and_frequencies') ));
 


%% Run trimming sim in background
slCharacterEncoding('windows-1252')
eval('DLR_SFB880_Trim_Quat');

%%
%init_func = @Full_BACM_init;
%solve_func = @Full_BACM_standardsolve;
init_func = @highlift_eigen_init;
solve_func = @highlift_eigen_solve;

%% set path for results (in a folder in the root, where sglib is saved)
cd(name_root_dir)
 name_dir='Results_Noemi_sglib_SFB880_eigenfreq';
 mkdir(name_dir);
 cd(name_dir);
 saving_path=pwd;
 cd(pathname);
%% Add path for the model (deterministic solver)
parent_dir=fileparts(fileparts( mfilename('fullpath') ));
 addpath( fullfile(parent_dir,'models', 'aircraft_models') );
 addpath(genpath( fullfile(parent_dir,'models', 'aircraft_models', 'fullflap_sim_and_frequencies') ));
 
%%
%Define random and not random parameters + time interval (see more in prey_predator_init)
%init_options.list_of_RVs={'C_L0'};
init_options.list_of_RVs={'Cmu',  'C_L0', 'C_Lalpha',   'C_Lalpha_FL',  'C_Lalpha_DN', 'C_D0',  'k1',    'k1_FL',  'k1_DN',  'k2',  'k2_FL', 'k2_DN', 'C_m0' , 'C_malpha', 'C_malpha_FL',  'C_malpha_DN'} ;

init_options.t_min_max= [0,100];
%init_options.list_of_RVs={'C_L0', 'C_mu', 'C_m0', 'C_Lalpha', 'C_D0'};
ylabels={{'Altitude [m]'};{'V_{TAS}: True air speed [m/s]'};{'\alpha: angle of attack [degree]'};{'q: pitch rate [degree/s]'};{'\theta : pitch altitude [degree]'} };

% %% Monte Carlo
% 
% N_MCL = 3;
% [u_mean, u_var, time, ref_sol, u_mean_freqs, u_var_freqs] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options);
% %show_mean_var('Monte-Carlo', u_mean, u_var)
% figure
% numbplots=size(u_mean,2);
% for i=1:numbplots
%     subplot(numbplots,1,i)
% plot(time, ref_sol(:,i), 'green', 'LineWidth', 3)
% end
% plot_u_mean_var(time, u_mean, u_var,'fill_color','blue', 'ylabels', ylabels)


% %% Quasi Monte Carlo
% 
% [u_mean, u_var, time,  ~, u_mean_freqs, u_var_freqs] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options, 'mode', 'qmc');
% %show_mean_var('Quasi Monte-Carlo', u_mean, u_var)
% 
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4 , 'ylabels', ylabels)
% 
% 
% %% Latin hypercube
% 
% [u_mean, u_var,time, ~, u_mean_freqs, u_var_freqs] = compute_moments_mc(init_func, solve_func, N_MCL, saving_path, init_options, 'mode', 'lhs');
% %show_mean_var('Latin hypercube', u_mean, u_var)
% 
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','yellow', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
% 
% %% Direct integration full tensor grid
% 
% p_integration_ord = 2;
% [u_mean, u_var, time, freq_info] = compute_moments_quad(init_func, solve_func, p_integration_ord, init_options, 'grid', 'full_tensor');
% %show_mean_var('Full tensor grid integration', u_mean, u_var);
% figure
% plot_u_mean_var(time,  u_mean, u_var, 'fill_color','black', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
% 
% 
% %% Direct integration sparse grid
% 
% p_integration_ord = 3;
% [u_mean, u_var, time, freq_info] = compute_moments_quad(init_func, solve_func, p_integration_ord, init_options, 'grid', 'smolyak', 'saving_path', saving_path, 'gpc_full_tensor', false, 'saving_path', saving_path);
% %show_mean_var('Sparse grid (Smolyak) integration', u_mean, u_var);
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','green', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

%% Projection with full tensor grid
% p_gPCE_order = 2;
% %p_int = [7, 16];
% p_integration_ord=3;
% [u_mean, u_var, time, gpc_info] = compute_response_surface_projection(init_func, solve_func, [], p_integration_ord, p_gPCE_order, init_options, 'grid', 'full_tensor', 'saving_path', saving_path);
% %show_mean_var('Projection (L_2, response surface, tensor)', u_mean, u_var);
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','cyan', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%% Projection with sparse grid
p_gPCE_order = 2;        %gPCE order
p_integration_ord = 3;      %integration order to calculate all other gpce coeff-s
p_int_proj=3;   %integration order to calculate initial gpce coeff-s from galerkin projection

[u_mean,u_var, time, gpc_info] = compute_response_surface_projection(init_func, solve_func, [], p_integration_ord, p_gPCE_order, init_options, 'grid', 'smolyak', 'saving_path', saving_path, 'gpc_full_tensor', false);
%show_mean_var('Projection (L_2, response surface, sparse)', u_mean, u_var);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
plot_u_mean_var(time, u_mean(:,1:4), u_var(:,1:4), 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)


%% Full tensor grid collocation (interpolation)

p_u = 2;
[u_mean,u_var, time, other_info] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options, 'gpc_full_tensor', false, 'grid', 'smolyak', 'saving_path', saving_path);

%ind=(multiindex_order(V_u{2})>=3);
%u_i_alpha(:,ind)=0;


%show_mean_var('Interpolation, tensor (response surface)', u_mean, u_var);
plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_tensorcoll_i_alpha = u_i_alpha;

%% Sparse grid collocation (regression)

