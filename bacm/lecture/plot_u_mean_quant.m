function plot_u_mean_quant(time, u_mean, u_quant_l, u_quant_u,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%to plot mean and variance of stochastic solution of ODE
%-in function of the TIME
%-mean value:   u_mean
%-variance:     u_var




%%
options=varargin2options(varargin);
[fill_color,options]=get_option(options, 'fill_color', 'blue');
[line_color,options]=get_option(options, 'line_color', 'black');
[transparency,options]=get_option(options, 'transparency', 1);
[y_lim,options]=get_option(options, 'y_lim', []);
[ylabels,options]=get_option(options, 'ylabels', {});
[subplot_dim, options]=get_option(options, 'subplot_dim',[]);
check_unsupported_options(options, mfilename);

%% create automatic y labels
numplots=size(u_mean,2);
if isempty(ylabels)
   ylabels(1:numplots,1)={''};
    for i=1:numplots
        ylabels{i,:}={strcat('Y',num2str(i))};
    end
end

if ~ (size(time,1)==1)
    time=time';
end
    
T=[time,fliplr(time)];
%% Get subplot dimension
if isempty(subplot_dim)
    if numplots>4
        hornumb=2;
        vertnumb=ceil(numplots/hornumb);
    else
        hornumb=1;
        vertnumb=numplots;
    end
else
    vertnumb=subplot_dim(1);
    hornumb=subplot_dim(2);
end

%% Plot
for i=1:numplots
    subplot(vertnumb,hornumb,i)
    U_min_max=[ (u_quant_l(:,i) )', fliplr( (u_quant_u(:,i) )')];
    fill(T',U_min_max,fill_color,'FaceAlpha', transparency);
    hold on
    plot(time,u_mean(:,i),'LineWidth',2,'color',line_color)
    ylabel(ylabels{i},'FontSize',14)
    xlabel('time','FontSize',14)
    if ~isempty(y_lim)
        ylim(y_lim)
    end
end

