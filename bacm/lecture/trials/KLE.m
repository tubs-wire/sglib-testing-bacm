function [pos, sigma_k r_i_k]=KLE()

 [pos, els, C]=mesh_test(10,14, 4, 'cov_type', 'gaussian');
% Compute Gramian matrix (aka mass matrix)
G = mass_matrix(pos, els);
G = full(G);

% Solve the KL eigenvalue problem
[V,D] = eig(G*C*G, G);

% Take only KL eigenvalues larger 10^-14
lambda = diag(D);
ind = lambda>1e-1;
lambda = lambda(ind);

% Select and normalise the KL eigenfunctions
R = V(:,ind);
R_norm = sqrt(diag(R'*G*R)');
R = binfun(@times, R, 1./R_norm);

% Compute the sigma's
sigma = sqrt(lambda);
%semilogy(sigma, 'x')

% Plot eigenfunctions
% for i=1:22
%     subplot(4,6,i)
%     surf(reshape(R(:,i), 10, 14))
% end

%output
r_i_k=R;
sigma_k=sigma';