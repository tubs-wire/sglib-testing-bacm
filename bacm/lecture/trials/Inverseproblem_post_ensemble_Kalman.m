% number of the samples of 'u' for the ensemble Kalman
n = 10;
% load initial gpc bases (V_u) and integration points (X_i) and
% weights (W_i)
%load('C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\Code\sglib-testing-master\pre_info.mat');
% load solution from the simulation
%save_pathname='C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\ONE-RV\GEPS3D-Sim';
%[X  Y  u_z]= load_results_to_matlab(save_pathname);

u_z =[1.2079 1.0333 0.9353 0.8621];

% calculate gPCE of the displacement
u_i_alpha = calculate_gpc_coeffs(V_u, x_i, u_z, w_i, 'method', 'project');

%% Statistics 
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
[phi_mean, phi_var] = gpc_moments( phi_alpha, V_phi);


%% Sample from the parameter and response
sample_phi=gpc_sample(phi_alpha, V_phi, n);
sample_u=gpc_sample(u_i_alpha, V_u, n);

%% Get Covariance matrix 
C_phi_u=1/(n-1)* (sample_phi-mean(sample_phi))*(sample_u-mean(sample_u))';
C_u=1/(n-1)* (sample_u-mean(sample_u))*(sample_u-mean(sample_u))';
estd = 0.0261;
K=C_phi_u*(C_u+estd^2);
z = 0.8702;
sample_phi_a=sample_phi+K*(z-sample_u);

%% mean and variance of assimilated parameter

u_mean_a=mean(sample_phi_a);
u_var_a=var(sample_phi_a);
