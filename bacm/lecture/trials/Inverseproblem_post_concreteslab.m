% number of the samples of 'u' for the ensemble Kalman
n = 100;
% load initial gpc bases (V_u) and integration points (X_i) and
% weights (W_i)
%load('C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\Code\sglib-testing-master\pre_info.mat');
% load solution from the simulation
%save_pathname='C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\ONE-RV\GEPS3D-Sim';
%[X  Y  u_z]= load_results_to_matlab(save_pathname);

u_z =[0.2661 0.2276 0.2060 0.1899];

% calculate gPCE of the displacement
u_i_alpha = calculate_gpc_coeffs(V_u, x_i, u_z, w_i, 'method', 'project');

%% Statistics 
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
[phi_mean, phi_var] = gpc_moments( phi_alpha, V_phi);

%% Get Covariance matrix 

v_alpha = [u_i_alpha;[phi_alpha 0 0]];

Pnp = gpc_covariance(v_alpha, V_u);


%% Update with KF
I = eye(2);

V_e = gpcbasis_create('H', 'm', 1, 'p', 1);
estd = 0.001;
v_j_gamma = [0, estd];
R = gpc_covariance(v_j_gamma, V_e);

H = [1 0];
xnp = [u_mean;phi_mean];
z = 0.1916;
%y = z - H * xnp;
y = z - u_mean;
S = H * Pnp * H' + R;
K = Pnp * H' / S;
xn = xnp + K * y;
Pn = (I - K*H)*Pnp;


%% Update with MMSE functions on GPC variables

% Define a priori model for xn: 
%   xn = F * x + B * u + w
% (The tricky thing is that the separate probalility spaces for x and w
% have to be combined into one, and during the function evaluation split up
% again, so that the x and w part only get "their" variables)
% [V_xnp, ~, ~, xi_x_ind, xi_w_ind] = gpcbasis_combine(V_x, V_w, 'outer_sum');
% xnp_func = @(xi)(...
%     F * gpc_evaluate(x_i_alpha, V_x, xi(xi_x_ind, :)) + ...  % F * x +
%     repmat(B*u, 1, size(xi,2)) + ...                         % B * u +
%     gpc_evaluate(w_i_beta, V_w, xi(xi_w_ind, :)));           % w

% Just another way to do it, by combining the GPC expansions and then
% making a function out of it
%[xw_ii_gamma, V_xnp] = gpc_combine_inputs(v_alpha, V_u, w_i_beta, V_w);
% xnp_i_gamma = [F, I] * xw_ii_gamma;
% xnp_i_gamma(:,1) = xnp_i_gamma(:,1) + B*u;
xnp_func = gpc_function(v_alpha, V_u);

% In-between check that the predicted state xnp and covariance Pnp match

assert_equals(gpc_covariance(v_alpha, V_u), Pnp, 'predicted covariance estimate Pnp')

% Define observation model (without the v, that comes extra)
%   z = H * xn
y_func = @(xi)(H * funcall(xnp_func, xi));

% In-between check of the covariance matrices of the observation
assert_equals(gpc_covariance(H*v_alpha, V_u)+R, S, 'innovation covariance S')

% Define error model (must be defined separately from observation model)
v_func = gpc_function(v_j_gamma, V_e);

% Now call the MMSE update procedure
p_phi=1;
p_int_mmse=4;
p_xn=3;
p_int_proj=4;
[xn_i_beta, V_xn]=mmse_update_gpc(v_alpha, y_func, V_u, z, v_func, V_e, p_phi, p_int_mmse, p_xn, p_int_proj);

% Final check of Kalman estimate and covariance update
assert_equals( gpc_moments(xn_i_beta, V_xn), xn, 'updated state estimate xn');
assert_equals( gpc_covariance(xn_i_beta, V_xn), Pn, 'updated covariance estimate Pn');

% ... and you see that the classical Kalman filter is exactly reproduced.
% The MMSE is completely independent of degree and in the limit case of all
% degrees equal to 1 reproduces the Kalman filter (or linear observation
% processes).


[mean_xn, var_xn] = gpc_moments (xn_i_beta, V_xn);

