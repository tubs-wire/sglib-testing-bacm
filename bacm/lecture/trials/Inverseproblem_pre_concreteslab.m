% Set random variable and make a gPCE of it
phi_dist=gendist_create('normal', {50,10});
polysys = 'h';  
[phi_alpha, V_phi]=gpc_param_expand(phi_dist, polysys);

% Set gPCE basis of the solution
gpc_order=3;
V_u = gpcbasis_create(polysys, 'm', 1, 'p', gpc_order);   

% Call integration rule
p_int=gpc_order+1;
[x_i, phi_i, w_i]=get_points(V_phi, phi_alpha, V_u, p_int);


