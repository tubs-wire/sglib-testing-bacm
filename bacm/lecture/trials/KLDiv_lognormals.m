function div=KLDiv_lognormals(mu1, sigma1,  mu2, sigma2 )
%Calculating the Kullback...L...Dive from a given lognormal

N_bins=1000;
N_samples=10^5;

%Setup distributiions
dist1=LogNormalDistribution(mu1,sigma1);
dist2=LogNormalDistribution(mu2,sigma2);

%Sample from distributions
x1=dist1.sample(N_samples);
x2=dist2.sample(N_samples);

%Discretized PDF (histogram)
x=linspace(min([x1;x2]), max([x1;x2]),N_bins);
h1=hist(x1, x);
h2=hist(x2, x);

%KLdiv
div=KLDiv(h1,h2);