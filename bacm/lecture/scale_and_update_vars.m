function state=scale_and_update_vars(sample, state)
%function SCALE_AND_UPDATE_VARS
%scale and shift samples from standard deviation and move random variables from sample to actual params in
%STATE.ACTUAL_PARAMS


sample_ind_min=1;
for i=1:length(state.list_of_RVs)
    var_name=char(state.list_of_RVs(i));
    if isfield(state,'multi_dim')
        
        sample_ind_max=sample_ind_min+state.multi_dim.size_independent_params{i}-1;
        sample_i=sample(sample_ind_min:sample_ind_max);
        Msample=sample_i(state.multi_dim.index_of_ind_params{i}); %put samples in the necessary dimensions
        sample_ind_min=sample_ind_max+1;
    else
        Msample=sample(i);
    end
    switch state.(var_name).distrib
        case 'H'    % Gaussian
            
            state.actual_params.(var_name)=Msample*state.(var_name).var+state.(var_name).mean;
        case 'G'    % Lognormal
            state.actual_params.(var_name)=exp(Msample*state.(var_name).sigma+state.(var_name).mu);
        case 'P'    % Uniform /transform from U(-1,1)
            state.actual_params.(var_name)=Msample*state.(var_name).multipl+state.(var_name).shift;
        case 'L'    % Exponential
            if isempty(strmatch('lambda',fieldnames(state.(var_name))))
                state.actual_params.(var_name)=state.(var_name).multipl*Msample;
            else
                state.actual_params.(var_name)=state.(var_name).multipl*state.(var_name).lambda*Msample^state.(var_name).lambda;
            end
        case 'T'    % Arcsin
            state.actual_params.(var_name)=Msample*state.(var_name).multipl+state.(var_name).shift;
        case 'U'    % Semicircle
            state.actual_params.(var_name)=Msample*state.(var_name).multipl+state.(var_name).shift;
    end
end
end
