%% Init stuff
clc, clear variables

pathname=pwd;
name_sglib_dir=(fileparts(which('startup.m')));
name_root_dir=fileparts(name_sglib_dir);
cd(name_sglib_dir);
startup

%% Add path for the model (deterministic solver)
parent_dir=fileparts(fileparts( mfilename('fullpath') ));
addpath( fullfile(parent_dir,'models', 'SFB_POD_multipl') );
 
%% set path for results (in a folder in the root, where sglib is saved)
cd(name_root_dir)
 name_dir='Results_sglib_POD_coeffs_a0_addUnc_short';
 mkdir(name_dir);
 cd(name_dir);
 saving_path=pwd;
 cd(pathname);

%%
init_func = @SFB_POD_model_init;
solve_func = @SFB_POD_model_solve;
init_options.list_of_RVs= {'a0'};
%init_options.list_of_RVs= {'L', 'Q', 'a0'};
%init_options.list_of_RVs= {'Q'};
ylabels={{'a_1'}; {'a_2'}; {'a_3'}; {'a_4'}};

%% Monte Carlo

% N = 50;
% [u_mean, u_var, time, ref_sol] = compute_moments_mc(init_func, solve_func, N, saving_path, init_options);
% figure
% for i=1:size(u_mean,2)
%     subplot(size(u_mean,2),1,i)
% plot(time, ref_sol(:,i), 'red', 'LineWidth', 2)
% end
% plot_u_mean_var(time, u_mean, u_var,'fill_color','blue', 'y_lim', [-1,1], 'ylabels', ylabels)


%% Quasi Monte Carlo

% [u_mean, u_var, time] = compute_moments_mc(init_func, solve_func, N, saving_path, init_options, 'mode', 'qmc');
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels);
% 
% 
% %% Latin hypercube
% 
% [u_mean, u_var,time] = compute_moments_mc(init_func, solve_func, N, saving_path, init_options, 'mode', 'lhs');
% plot_u_mean_var(time, u_mean, u_var, 'fill_color','yellow', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

%% Direct integration full tensor grid

p = 4;
[u_mean, u_var, time] = compute_moments_quad(init_func, solve_func, p, init_options, 'grid', 'full_tensor', 'saving_path', saving_path);
plot_u_mean_var(time,  u_mean, u_var, 'fill_color','black', 'line_color','black','transparency', 0.4)

%% Direct integration sparse grid

p = 4;
[u_mean, u_var, time] = compute_moments_quad(init_func, solve_func, p, init_options, 'grid', 'smolyak',  'saving_path', saving_path);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','green', 'line_color','black','transparency', 0.4,  'y_lim', [-1,1])

%% Projection with full tensor grid
p_u = 3;
%p_int = [7, 16, 7];
p_int=4;
[u_mean, u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_int, p_u, init_options, 'grid', 'full_tensor', 'saving_path', saving_path);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','cyan', 'line_color','black','transparency', 0.4, 'y_lim', [-1,1])

%% Projection with sparse grid
p_u = 3;        %gPCE order
p_int = 4;      %integration order to calculate all other gpce coeff-s
p_int_proj=4;   %integration order to calculate initial gpce coeff-s from galerkin projection

[u_mean,u_var, time] = compute_response_surface_projection(init_func, solve_func, [], p_int, p_u, init_options, 'grid', 'smolyak', 'saving_path', saving_path);
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4,  'y_lim', [-1,1])

%% Full tensor grid collocation (interpolation)

p_u = 3;
[u_mean,u_var, time] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options, 'saving_path', saving_path);

plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4)

%% Sparse grid collocation (regression)
p_u = 3;
[u_mean,u_var, time] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options, 'grid', 'smolyak', 'saving_path', saving_path);
plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'y_lim', [-1,1])

