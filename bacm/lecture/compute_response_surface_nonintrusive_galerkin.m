function [u_mean, u_var, time, varargout]=compute_response_surface_nonintrusive_galerkin(init_func, step_func, V_u, p_int, p_u, init_options, varargin)
% COMPUTE_RESPONSE_SURFACE_NONINTRUSIVE_GALERKIN Short description of compute_response_surface_nonintrusive_galerkin.
%   COMPUTE_RESPONSE_SURFACE_NONINTRUSIVE_GALERKIN Long description of compute_response_surface_nonintrusive_galerkin.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example compute_response_surface_nonintrusive_galerkin">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[solve_func, options]=get_option(options, 'solve_func', []);
[p_int_proj, options]=get_option(options, 'p_int_proj', []);
[u0_i_alpha, options]=get_option(options, 'u0_i_alpha', []);
[saving_gpc_coeffs, options]=get_option(options, 'saving_gpc_coeffs', 1);
[saving_path, options]=get_option(options, 'saving_path', []);

%[max_iter, options]=get_option(options, 'max_iter', 50);
%[threshold, options]=get_option(options, 'threshold', 1e-5);
[grid, options]=get_option(options, 'grid', 'smolyak');
check_unsupported_options(options, mfilename);

[state,polysys,time] = funcall(init_func, init_options);

if isempty(V_u)
    V_u = gpcbasis_create(polysys, 'm', state.num_params, 'p', p_u, 'full_tensor', false);
end

%*******************************************************************
% CALCULATE INITIAL GPCE COEFFS OF INITIAL STATE
%*******************************************************************
%If gpc coefficients are given in advance
if ~isempty(u0_i_alpha)
    u_i_alpha = u0_i_alpha;
    
    %If p_int_proj is given: calculate gpc coefficients with integration
    %order p_int_proj from first step iteration
elseif ~isempty(solve_func)
    if isempty(p_int_proj)
        p_int_proj=p_int;
    end
    [x,w] = gpc_integrate([], V_u, p_int_proj, 'grid', grid);
    M = gpcbasis_size(V_u, 1);
    Q = length(w);
    step_n = 0;
    u_i_alpha = zeros(state.num_eqs, M);
    
    for j = 1:Q
        p_j = x(:, j);
        u_i_j = funcall(solve_func,state, p_j, 'step_info', step_n);
        psi_j_alpha_dual = gpc_eval_basis(V_u, p_j, 'dual', true);
        u_i_alpha = u_i_alpha + w(j) * u_i_j(1) * psi_j_alpha_dual;
        %u_i_alpha = repmat(state.u0, 1, gpcbasis_size(V_u,1));
    end
    %Otherwise: coefficients are directly from initial condition
else
    M = gpcbasis_size(V_u, 1);
    u_i_alpha = zeros(state.num_eqs, M);
    
    %get the index of the line in the multiindex that corresponds to initial value (e.g. P0) with
    %different orders. e.g. ind_multiindex(n) corresponds to the (n-1)th
    %order expansion where the ind_P0th value is n-1 the rest is 0
    
    for jj = 1:state.num_eqs
        solname_jj=char(state.list_of_sol_vars(jj));
        ind_sol_jj=strmatch(solname_jj,state.list_of_RVs);
        if length(p_u) == 1
            p_sol_jj=p_u;  %order of gPCE in the 'solname_jj' direction
        else
            p_sol_jj=p_u(ind_solname_jj);
        end
        ind_multiline=zeros(p_sol_jj+1,1);
        for i=0:p_sol_jj
            ind=i+1;
            multiline=zeros(1,size(V_u{2},2));
            multiline(ind_sol_jj)=i;
            for j=1:size(V_u{2},1);
                if V_u{2}(j,:) == multiline
                    ind_multiline(ind)=j;
                    break
                end
            end
        end
        
        
        u_i_alpha(jj,ind_multiline(1)) = state.ref_params.(solname_jj);    %first coefficient from the mean (in case of lognormal and exp that will be modified)
        
        if ~isempty(strmatch(solname_jj,state.list_of_RVs))       %calculate further coefficients
            switch state.(solname_jj).distrib
                case 'H'    % Gaussian
                    u_i_alpha(jj,ind_multiline(2)) = state.(solname_jj).var;
                case 'G'    % Lognormal
                    for i=0:p_sol_jj
                        u_i_alpha(jj,ind_multiline(i+1)) = 1/factorial(i)*state.(solname_jj).sigma^i*state.(solname_jj).mean;
                    end
                case 'P'    % Uniform /transform from U(-1,1)
                    u_i_alpha(jj,ind_multiline(2))  = state.(solname_jj).multipl;
                case 'L'    % Exponential
                    u_i_alpha(jj,ind_multiline(1))  = state.(solname_jj).multipl;
                    u_i_alpha(jj,ind_multiline(2))  = -state.(solname_jj).multipl;
                case 'T'    % Arcsin
                    u_i_alpha(jj,ind_multiline(2))  = state.(solname_jj).multipl;
                case 'U'    % Semicircle
                    u_i_alpha(jj,ind_multiline(2))  = state.(solname_jj).multipl/2;
            end
        end
    end
end

 u_global_alpha=zeros(state.num_eqs*state.num_vars,M);
for kk=0:state.num_eqs-1
    u_global_alpha(kk*state.num_vars+1,:)=u_i_alpha(kk+1,:);
end
u_i_alpha(:,:,1)=u_i_alpha;
u_i_alpha(:,:,2:state.num_vars)=0;


%*******************************************************************
% CALCULATE gPCE COEFFS WITH NONINTRUSIVE GALERKIN FROM STEP BY STEP
% INTEGRATION
%*******************************************************************
[x,w] = gpc_integrate([], V_u, p_int, 'grid', grid);
M = gpcbasis_size(V_u, 1);
Q = length(w);
step_n=1;

% Calculate polynomials at integration points
psi_p_alpha_dual=zeros(Q,M);
   for j=1:Q
        p = x(:, j);    %integration points
       % evaluate polynomial at p
        psi_p_alpha_dual(j,:) = gpcbasis_evaluate(V_u, p, 'dual', true);
   end


for tt=1:state.num_vars-1
    tt
    deltau_i_alpha=zeros(state.num_eqs,M);
    % that's the z-sum here, i.e. z=x(:,j)
    for j=1:Q
        p = x(:, j);    %integration points
        % compute u_i_p = sum u_i_alpha Psi_alpha(p)
        u_i_p  = gpc_evaluate(u_i_alpha(:,:,tt), V_u, p); %solution at integration point at step_n
        % evaluate S at p, u_i_p
        S_p = funcall(step_func, state, p, 'step_info', {u_i_p, tt});
        % update unext
        deltau_i_alpha = deltau_i_alpha + w(j) * S_p * psi_p_alpha_dual(j,:);
    end
    u_i_alpha (:,:,tt+1) = u_i_alpha (:,:,tt)+deltau_i_alpha;
    step_n=step_n+1;
end


% Put different solutions one beneath the other in one column
u_i_alpha_reshaped=mat2cell(u_i_alpha,ones(1,state.num_eqs),M,[state.num_vars]);
u_i_alpha_reshaped=( shiftdim(cat(3,u_i_alpha_reshaped{:}),1) )';

[u_mean, u_var] = gpc_moments(u_i_alpha_reshaped, V_u);

%put different variables in u_mean in different columns
 u_mean=reshape(u_mean,state.num_vars,state.num_eqs);
 u_var=reshape(u_var,state.num_vars,state.num_eqs);
 u_i_alpha=reshape(u_i_alpha,state.num_vars,M,state.num_eqs);
 varargout{1}.u_i_alpha=u_i_alpha;
varargout{1}.V_u=V_u;

 %% save results if saving_path is given
 if ~(isempty(saving_path))
     %definition of file- and foldername for saving results
     equation_name=strrep(char(init_func),'init','_');
     file_name=get_saving_filenames(saving_path,equation_name,'NIGAL',{p_u, p_int});
     if saving_gpc_coeffs
         save(file_name,'p_u', 'p_int','init_options','u_i_alpha', 'u_mean', 'u_var', 'V_u' , 'state');
     else
         save(file_name,'p_u', 'p_int','init_options', 'u_mean', 'u_var', 'state');
     end
 end
end