function [u_mean, u_var, time, varargout] = compute_moments_quad(init_func, solve_func, p_int, init_options, varargin)
% COMPUTE_MOMENTS_QUAD Compute mean and variance by high-dimensional quadrature.
%   [U_MEAN, U_VAR] = COMPUTE_MOMENTS_QUAD(INIT_FUNC, SOLVE_FUNC, P) computes
%   the mean and variance of a system described by INIT_FUNC and SOLVE_FUNC
%   by a quadrature rule of order P. The distribution is specified
%   by POLYSYS (see <a href="matlab:help gpc">GPC</a>).
%
% Options
%   grid: {'smolyak'}, 'full_tensor'
%     Choose the grid to use for integration points.
%
% Example (<a href="matlab:run_example compute_moments_quad">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin);
[grid, options]=get_option(options, 'grid', 'smolyak');
[saving_path, options]=get_option(options, 'saving_path', []);
[gpc_full_tensor, options]=get_option(options, 'gpc_full_tensor', false);
check_unsupported_options(options, mfilename);

[state, polysys,time] = funcall(init_func, init_options);
V = gpcbasis_create(polysys, 'm', state.num_params, 'full_tensor', gpc_full_tensor);
[x, w] = gpc_integrate([], V, p_int, 'grid', grid);
Q = length(w);

u = zeros(state.num_totRV_out, Q);
for j = 1:Q
     disp(strcat(num2str(j), '/', num2str(Q)));
    x_j = x(:, j);
    u(:, j) = funcall(solve_func, state, x_j);
end

u_mean = u * w;
u_var = binfun(@minus, u, u_mean).^2 * w;
%put different variables in u_mean in different columns
 if ~isscalar(state.num_eqs);
    u_mean_nontemp=u_mean( end-state.num_eqs(1)+1:end);
    u_var_nontemp=u_var( end-state.num_eqs(1)+1:end);
    u_mean=u_mean(1:end-state.num_eqs(2));
    u_var=u_var(1:end-state.num_eqs(2));
 end
 u_mean=reshape(u_mean,state.num_vars,state.num_eqs(1));
 u_var=reshape(u_var,state.num_vars,state.num_eqs(1));
 
  if nargout>0
      if  ~isscalar(state.num_eqs);
         varargout{1}.u_mean_nontemp=u_mean_nontemp;
         varargout{1}.u_var_nontemp=u_var_nontemp;
     end
 end
 
 %% save results if saving_path is given
 if ~(isempty(saving_path))
     %definition of file- and foldername for saving results
        equation_name=strrep(char(init_func),'init','_');
        file_name=get_saving_filenames(saving_path,equation_name,'DIRINTEG',{p_int, grid});
        if isscalar(state.num_eqs);
            save(file_name,'u_mean','u_var','p_int', 'polysys', 'init_options', 'state');
        else
            save(file_name,'u_mean','u_var','u_mean_nontemp', 'u_var_nontemp','p_int', 'polysys', 'init_options', 'state');
        end
end