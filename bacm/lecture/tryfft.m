function tryfft()
dt = 0.01;                     % Sample time
Fs = 1/dt;                    % Sampling frequency
t=0:dt:100;                   % Time vector
x = 0.7*sin(2*pi*0.5*t) + sin(2*pi*0.25*t)+cos(2*pi*0.25*t); % Sum of a 50 Hz sinusoid and a 120 Hz sinusoid
%y = x + 2*randn(size(t));     % Sinusoids plus noise
L = length(x);                     % Length of signal


plot(t,x)
title('Signal Corrupted with Zero-Mean Random Noise')
xlabel('time')

%NFFT = 2^nextpow2(L); % Next power of 2 from length of y
NFFT=L;
Y = fft(x,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);

% Plot single-sided amplitude spectrum.
plot(f,2*abs(Y(1:NFFT/2+1))) 
[ampl,ind]=findpeaks(2*abs(Y(1:100)));
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')