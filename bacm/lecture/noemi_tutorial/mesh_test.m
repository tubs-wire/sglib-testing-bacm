function [pos, els, Cov]=mesh_test(n,m, l_c, varargin)
% n: number of element in the X direction
% m: number of element in the Y direction
% l_c: correlation length
% optional input (type of covariance matrix):
% 'COV_TYPE': 'GAUSSIAN'/'EXPONENTIAL'

options=varargin2options( varargin );
[cov_type,options]=get_option( options, 'cov_type', 'gaussian');
check_unsupported_options( options, mfilename );

%% Mesh definition

% X and Y coordinates
X=1:n;
Y=1:m;

% meshgrid
[x,y]=ndgrid(X,Y);
pos=[x(:)'; y(:)'];

% defining and ploting elements in the mesh
num_els=(n-1)*(m-1)*2;
els=zeros(3,num_els);

for j=1:m-1
    els(1, ((j-1)*(n-1)*2+1):2:(2*j*(n-1)))=(j-1)*n+1:j*n-1;
    els(1, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=(j-1)*n+1:j*n-1;
    els(2, ((j-1)*(n-1)*2+1):2:(2*j*(n-1)))=(j)*n+2:(j+1)*n;
    els(2, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=(j-1)*n+2:j*n;
    els(3, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=j*n+2:j*n+n;
    els(3, ((j-1)*(n-1)*2+3):2:(2*j*(n-1)))=j*n+2:j*n+n-1;
    els(3, (j-1)*2*(n-1)+1)=j*n+1;
end
%plot_mesh(pos, els);
%% Calculate covariance
switch cov_type
    case 'exponential'
        base_cov_func = @exponential_covariance;
    case 'gaussian'
        base_cov_func = @gaussian_covariance;
end
cov_func = funcreate(base_cov_func, funarg, funarg, l_c);
Cov=covariance_matrix( pos, cov_func);
%figure
%imagesc(Cov)

