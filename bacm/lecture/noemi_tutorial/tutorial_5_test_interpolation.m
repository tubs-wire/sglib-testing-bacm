%Tutorial on interpolation
%same as previous file, but not with random points but with Gauß points
%Electrical network example
%
%A(u(p);p)=f(p)

%Surrogate/proxi model:
%
%u_I(p)=\sum_{j=1}^M u_j\Phi_j(p)

%N interpolation points p_i, require:
%u(p_i)=u_I(p_i)  i=1..N
%M=N interpolation
%M<N regression

%Interpolation (N=M):
%u(p_1)=\sum_{j=1}^M u_j\Phi_j(p_1)=u_1\Phi_1(p_1)+u_2\Phi_2(p_1)+...u_M\Phi_M(p_1)
%u(p_2)=\sum_{j=1}^M u_j\Phi_j(p_2)=u_1\Phi_1(p_2)+u_2\Phi_2(p_2)+...u_M\Phi_M(p_2)
%
%
%u(p_M)=\sum_{j=1}^M u_j\Phi_j(p_M)=u_1\Phi_1(p_M)+u_2\Phi_2(p_M)+...u_M\Phi_M(p_M)
%In matrix form: b=Wu
%
%Algorithm
%0. Define deterministic solver
%1. Setup basis: \Phi_1, \Phi_2,.. \Phi_M
%2. Determine points: p_1, p_2, .. p_M (Chebyhev-points, Gauß-points.. etc.)
%3. Compute matrix: W_{ij}=\Phi_j(p_i)
%4. Compute r.h.s.: b_i=u(p_I)=A^{-1}(f(p_I);p_i)
%5. Solve for u: Wu=b
%6. Evaluate any uI at spacific p

%% Define deterministic solver

% the deterministic inputs
x0=1;v0=0; T=10; d=0;
% function handle [x, v]=solve_func(m,k)
solve_func = @(m,k)(spring_solve(x0, v0, m, d, k, T));
% define the output component we are interested in
c=1;

%% Define parameters
% Initiate SimParamSet
Q=SimParamSet();

% Add parameters k and m to the set
%Q.add(SimParameter('m', UniformDistribution(0.5,2.5)));
Q.add(SimParameter('m', fix_bounds(SemiCircleDistribution(), 0.5, 2.5)));
Q.add(SimParameter('k', fix_bounds(SemiCircleDistribution(), 0.5,2.5)));
%% 1. Setup basis

% Define approximating subspace
V=Q.get_germ;
p_gpc=10;
V_u=gpcbasis_modify(V,'p', p_gpc);
% Number of basis funcions:
M=gpcbasis_size(V_u,1);

%% 2. Setup the points with Gauss-points

%p_int=[4;6]; %different degrees for the dimensions are allowed only for
% with sparse grid:
%[p,w]=gpc_integrate([], V, p_gpc+1, 'grid', 'full_tensor');
% with full tensor grid:
p_int= p_gpc+1;
[xi,w]=gpc_integrate([], V, p_gpc+1, 'grid', 'full_tensor');
N=length(w);
% map germ to parameter
q=Q.germ2params(xi);

%% 3. Compute Vandermonde type matrix (transpose!)

W = gpcbasis_evaluate(V_u, xi)';

%% 4. Compute RHS

b = zeros(N,1);
for i=1:N
    ui = solve_func(q(1,i), q(2, i));
    b(i) = ui(c);
end

%% 5. Solve for u

u_i_alpha = (W\b)';

% Plot response surface and interpolation points
plot_response_surface(u_i_alpha, V_u)
u=gpc_evaluate(u_i_alpha, V_u, xi);
hold on; plot3(xi(1,:), xi(2,:), u(1,:)+0.001, 'kx'); hold off;
%hold on; plot3(xi(1,:), xi(2,:), b+0.001, 'ko'); hold off;


