clear variables
state=electrical_network_init();
%electrical_network_solve(state,[1,0.53])
N=20;
x=linspace(-1,1, N);
y=linspace(-1,1,N);
[x,y]=meshgrid(x,y);
p=[x(:)';y(:)'];
for i=1:size(p,2)
    p_i=p(:,i);
Pi(:,i)=electrical_network_solve(state, p_i);
end
for j=1:size(Pi,1);
u(:,:,j)=reshape(Pi(1,:), size(x));
end
surf(x,y,u(:,:,5));