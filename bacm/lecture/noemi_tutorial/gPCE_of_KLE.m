% KLE expansion of the covariance
[pos, sigma_k r_i_k]=KLE();

%field description
N=size(pos,2);
L=22;

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);


% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

% The conversion made explicit
% a) Multiply in the sigmas 
u_i_k = binfun(@times, r_i_k, sigma_k);

% b) Put the mean inside
u_i_k = [sin(pi*x'), u_i_k];
theta_k_alpha = [1, zeros(1,m); theta_k_alpha];

xi = randn(m, 5);

field_i=kl_pce_field_realization(u_i_k, theta_k_alpha, I, xi);

