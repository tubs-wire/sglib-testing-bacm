%Tutorial on projection
%
%Undamped spring example
%A(u(p);p)=f(p)
%u(p)=A^{-1}(f;p)=S(p)

%Surrogate/proxi model:
%ubar(p)=\sum_{\alpha \in I}u_\alpha \psi_\alpha(p)
%Orthogonal projection
%u()-ubar() should be orthogonal to span{\psi_alpha}
%E\left(u()\psi()\right)=\sum u_\alpha E\left(\psi_\alpha
%\psi_beta()\right) = h\beta u_\beta
%
%Algorithm:
%
%1)Specify the basis \psi_\alpha (therefore also the subspace V=span{\psi_alpha}
%2)Determine norms h_\alpha of basis functions
%3)Compute/Get  integration rule \xi_i, w_i for i=1..Q
%4)Solve A(u(\xi_i);\xsi_i)=f(\xi_i) for all i=1.. Q
%call them u_i=S(\xi_i)
%5)Evaluate basis functions \psi_\alpha at all the \xi_i
%6)Plug in u_\beta=\frac{1}{h_\beta}\sum_{i=1}^Q w_i u_i \psi_\beta(\xi_i)


%
%Algorithm
%%
%Initialize
init_func = @undamped_spring_init;
solve_func = @undamped_spring_solve;
T=10; % index of the time of u to analyse
s=1; %component of solution to be analysed
state=init_func('T', T);
%%
%Try deterministic solver

u=solve_func(state,[1,2]);

%%
%Create mesh for true response - test mesh
[pos, els]=create_mesh_2d_rect(6); %coordinates and connections
pos=pos*2-1; %scale it to [-1,1]
plot_mesh(pos,els);
z=sin(pos(1,:)*5)';
plot_field(pos,els, z, 'view',3);

%%
%True response surface
[pos, els]=create_mesh_2d_rect(6); %coordinates and connections
pos_u=pos*2-1; %scale from [0,1] it to [-1,1]
plot_mesh(pos_u,els);
u=zeros(size(pos_u,2),1);
for j=1:size(pos,2)
   u_comp= solve_func(state, pos_u(:,j)');
   u(j)=u_comp(s);
end

plot_field(pos_u,els, u, 'view',3, 'show_mesh', false)

%% Orthogonal Projection
%1. Specify the basis
p=12; %gpc order
V=gpcbasis_create('pu', 'p', p);


% 2. Determine norms h_\alpha of basis functions
h_alpha=gpcbasis_norm(V);

%3. Compute/Get  integration rule \xi_i, w_i for i=1..Q
degree=p+1;
[xi,w]=gpc_integrate([], V, degree, 'grid', 'full_tensor');
%[xi,w]=gpc_integrate([], V, degree, 'grid', 'smolyak');
Q=size(xi,2);
M=gpcbasis_size(V,1);
%m=gpcbasis_size(V,2);
u_beta=zeros(state.num_vars,M);
for i=1:Q  %for each integration point
    %4)Solve A(u(\xi_i);\xsi_i)=f(\xi_i) for all i=1.. Q
    xi_i=xi(:,i);
    S_i=solve_func(state, xi_i');
    %5)Evaluate basis functions \psi_\alpha at all the \xi_i
    Psi_i=gpcbasis_evaluate(V,xi_i, 'dual', true);
    w_i=w(i);
    %6)Plug in u_\beta=\frac{1}{h_\beta}\sum_{i=1}^Q w_i u_i \psi_\beta(\xi_i)
    %u_i=u_i+1/h_alpha w_i*S_i*Psi_i
    u_beta=u_beta+S_i*w_i*Psi_i;
end


% Plot response surface and interpolation points
plot_response_surface(u_beta(1,:), V)

%%
u=gpc_evaluate(u_beta, V, xi);
hold on; plot3(xi(1,:), xi(2,:), u(1,:)+0.002, 'kx'); hold off;
hold on; plot3(p(1,:), xi(2,:), b+0.002, 'ko'); hold off;







%V=gpcbasis_create('pu', 'p', 4, 'full_tensor', true)
