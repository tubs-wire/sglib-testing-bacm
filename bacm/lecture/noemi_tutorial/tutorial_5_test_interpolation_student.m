%% gPCE by interpolation
% 
%% Introduction
% Abstract formulation
%
% $$ A(u(q);q)=f(q) $$
%
% Surrogate/proxi model:
%
% $$u_I(q)=\sum_{j=1}^M u_j\Phi_j(q)$$
%
% N regression points $q_i$, require:
%
% $$u(q_i)=u_r(q_i) \quad i=1..N$$
%
% $M=N$ -> interpolation
%
% $M<N$  -> regression
%
% Interpolation ($N=M$):
%
% $$u(q_1)=\sum_{j=1}^M u_j\Phi_j(q_1)=u_1\Phi_1(q_1)+u_2\Phi_2(q_1)+...u_M\Phi_M(q_1)$$
%
% $$u(q_2)=\sum_{j=1}^M u_j\Phi_j(q_2)=u_1\Phi_1(q_2)+u_2\Phi_2(q_2)+...u_M\Phi_M(q_2)$$
%
% .
%
% .
% $$u(q_M)=\sum_{j=1}^M u_j\Phi_j(q_M)=u_1\Phi_1(q_M)+u_2\Phi_2(q_M)+...u_M\Phi_M(q_M)$$
%
% In matrix form:
%
% $$b=Wu$$
%
% Algorithm
%
% #  Define deterministic solver
% #  Define distribution of uncertain input parameters
% #  Setup basis: $\Phi_1, \Phi_2,.. \Phi_M$
% #  Determine points: $q_1, q_2, .. q_M$ (Chebyhev-points, Gauss-points.. etc.)
% #  Compute matrix: $W_{ij}=\Phi_j(q_i)$
% #  Compute r.h.s.: $b_i=u(q_i)=A^{-1}(f(q_i);q_i)$
% #  Solve for $u$: $Wu=b$
% # Get statistics of $u_I$ and evaluate proxi model at any parameter values $q$


%% 1. Define deterministic solver

% the deterministic inputs
x0=1;v0=0; T=10; d=0;
% function handle [x, v]=solve_func(m,k)
solve_func = @(m,k)(spring_solve(x0, v0, m, d, k, T));
% define the output component we are interested in
c=1;

%% 2. Define distribution of uncertain input parameters
% Initiate SimParamSet
Q=SimParamSet();

% Add parameters k and m to the set

Q.add(SimParameter('m', UniformDistribution(0.5,2.5)));
Q.add(SimParameter('k', UniformDistribution(0.5,2.5)));

%% 3. Setup approximating basis

% Define approximating subspace
V=Q.get_germ;
p_gpc=10;
V_u=gpcbasis_modify(V,'p', p_gpc, 'full_tensor', true);
% Number of basis funcions:
M=gpcbasis_size(V_u,1);

%% 4. Setup the points with Gauss-quadrature

% Define integration order and setup point and weights

% p_int=[4;6]; %different degrees for the dimensions are allowed only for
% [p,w]=gpc_integrate([], V, p_gpc+1, 'grid', 'full_tensor');
% % or with sparse grid (here, as we have only two random variables, it would
% % not make sense
%  p_int= p_gpc+1;
% [xi,w]=gpc_integrate([], V, p_int, 'grid', 'full_tensor');

% with full tensor grid:
p_int= p_gpc+1;
[xi,w]=gpc_integrate([], V, p_int, 'grid', 'full_tensor');
N=length(w);
% map germ values (xi_1 and xi_2) to parameter values k and m
q=Q.germ2params(xi);

%% 5. Compute Vandermonde type matrix (transpose!)

W = gpcbasis_evaluate(V_u, xi)';

%% 6. Compute RHS

b = zeros(N,1);
for i=1:N
    ui = solve_func(q(1,i), q(2, i));
    b(i) = ui(c);
end

%% 7. Solve for u

u_i_alpha = (W\b)';

%% 8. Get statistics of $u_I$ and evaluate proxi model at any parameter values $q$

% Plot response surface and interpolation points
plot_response_surface(u_i_alpha, V_u)
% Evaluate proxi model at the integration points
u=gpc_evaluate(u_i_alpha, V_u, xi);
% plot proxi model and the true response at integration points
hold on; plot3(xi(1,:), xi(2,:), u(1,:)+0.001, 'kx'); hold off;
hold on; plot3(xi(1,:), xi(2,:), b+0.001, 'ko'); hold off;
% Compute mean and variance of $u_p$:
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u)
% Compute sensitivity of $u$ to the uncertainties of $k$ and $m$:
[u_part_vars, I, sobol_index]=gpc_sobol_partial_vars(u_i_alpha, V_u)
% Plot Sobol-sensitivities
figure
labels={'u_{var} due to var of m', 'u_{var} due to var of k', 'u_{var} due to vars of k and m'};
pie3(sobol_index(1,:),[0,0,1])
legend(labels)
title('Sobol sensitivities of u to uncertainties of k and m')

