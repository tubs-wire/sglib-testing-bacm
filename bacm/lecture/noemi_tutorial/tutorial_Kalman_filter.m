%% gPCE by regression (by minimum mean squared error)
%
%% Introduction


%% 1. Define deterministic solver

% the deterministic inputs
x0=1;v0=0; T=0:0.1:10; d=0;
% function handle [x, v]=solve_func(m,k)
solve_func = @(q)(spring_solve(x0, v0, q(1), d, q(2), T));
% define the output component we are interested in
c=1;

%% 2. Define distributions of uncertain parameters

% Initiate SimParamSet
Q=SimParamSet();

% Add parameters k and m to the set

Q.add(SimParameter('m', UniformDistribution(0.5,2.5)));
Q.add(SimParameter('k', UniformDistribution(0.5,2.5)));
%% 3. Setup approximating basis

% Define approximating subspace
V=Q.get_germ;
p_gpc=10;
V_u=gpcbasis_modify(V,'p', p_gpc, 'full_tensor', false);
% Number of basis funcions:
M=gpcbasis_size(V_u,1);

%% 4. Setup the points with Gauss-points

%p_int=[4;6]; %different degrees for the dimensions are allowed only for
%[p,w]=gpc_integrate([], V, p_gpc+1, 'grid', 'full_tensor');
% with full tensor grid:
p_int= p_gpc+1;
[xi,w]=gpc_integrate([], V, p_int, 'grid', 'full_tensor');
N=length(w);
% map germ to parameter
q=Q.germ2params(xi);

%% 5. Compute Vandermonde type matrix (transpose!)

W = gpcbasis_evaluate(V_u, xi)';

%% 6. Compute RHS

b = zeros(N,length(T));
for i=1:N
    ui = solve_func(q(:,i));
    b(i) = ui(c);
end

%% 7. Solve for u

u_beta = (W\b)';

%% 9. Gernerate synthetic measurement

n_meas =5;
% 'true' parameter values
q_t = Q.sample(1);
% 'true' response
[u_t, v_t] =  solve_func(q_t);
% artificial measurement
ind = [5,50, 60, 100];
% the error model
E = generate_stdrn_simparamset(0.01*ones(length(ind),1));
y_t = u_t(ind)';
y_m = y_t +E.sample(1);



%%
[q_i_alpha, V_q]=Q.gpc_expand();
Yproxi_func = @(q)gpc_evaluate(u_beta(ind, :), V_u, Q.params2germ(q));
C_q = gpc_covariance(q_i_alpha, V_q);
[e_j_gamma, V_e] = E.gpc_expand();

%% Check enKF
N_en=10000;
q_en = gpc_sample(q_i_alpha, V_q, N_en);
e_en = gpc_sample(e_j_gamma, V_e, N_en);
q_p_en=enKF(q_en, Yproxi_func, e_en, y_m, 'cz_from_z', false);
q_p2_en=enKF(q_en, Yproxi_func, e_en, y_m, 'cz_from_z', true);

%% Check with projecting to eigenvalues
C_y=covariance_sample(Yproxi_func(q_en));
n_mode=7;
[r_i_k,sigma_k]=kl_solve_evp(C_y, [],n_mode);
y_mean=mean(H*q_en, 2);
%A=inv(diag(sigma_k))*r_i_k';
A=r_i_k';
Ay_func = @(xi)(A * binfun(@minus, funcall(Yproxi_func, xi), y_mean));    
q_p3_en=enKF(q_en, Ay_func, A*e_en, A*(z_m - y_mean), 'cz_from_z', false);

%% Elmar's low rank check

 [U,S,~]=svd(H*q_en,0);
 s=diag(S);
 L=7;
 sum(s(1:L))/sum(s)
 U=U(:,1:L);
 yU_func = @(xi)(U' * funcall(Yproxi_func, xi));
 q_p4_en=enKF(q_en, yU_func, U'*e_en, U'*z_m, 'cz_from_z', false);

%% Check KF
[q_p_mean, C_xi_p] = KF(q_mean, C_q, C_e, H, z_m);

%% Check gpc_KF
Y_j_alpha = H * q_i_alpha;
[qp_j_beta, V_qp]=gpc_KF(q_i_alpha, Y_j_alpha, V_q, e_j_gamma, V_e, z_m);

%% Check mixed_KF

%% Compare all
[q_true, q_mean, mean(q_p_en,2), mean(q_p2_en,2), mean(q_p3_en,2), mean(q_p4_en,2), q_p_mean, gpc_moments(qp_j_beta,V_qp)]
%norm(C_xi_p - covariance_sample(xi_p_en),'fro')/norm(C_xi_p,'fro')


