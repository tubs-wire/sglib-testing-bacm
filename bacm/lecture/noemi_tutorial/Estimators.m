%% Set distribution of RVs

% Mean of Gaussians
mu = 2;

% Standard deviations of Gaussians
sigma1 = 3;
sigma2 = 0.5;

% Variances of the Gaussians
var1 = sigma1^2;
var2 = sigma2^2;

% Set Gaussian distributions
dist1 = gendist_create('normal', {mu, sigma1});
dist2 = gendist_create('normal', {mu, sigma2});

%% Initialise memory for estimators and their errors
n = 20;
m = 50;
mu_hat1 = zeros (m,n);
err_hat1 = zeros (m,n);
var_hat1 = zeros (m,n);
errv_hat1 = zeros (m,n);

mu_hat2 = zeros (m,n);
err_hat2 = zeros (m,n);
var_hat2 = zeros (m,n);
errv_hat2 = zeros (m,n);


%% Estimate mean and  variance
N = 2.^(1:n);

for j = 1:m
    for i = 1:n
        %% Mean estimation
        % Sample number
        ns = N(i);

        % Sample from the two distributions
        x1 = gendist_sample (ns, dist1);
        x2 = gendist_sample (ns, dist2);

        % Compute estimators and their errors
        mu_hat1(j,i) = mean(x1);
        err_hat1(j,i) = mean(x1)-mu;
        var_hat1(j,i) = var(x1);
        errv_hat1(j,i) = var(x1)-var1;

        mu_hat2(j,i) = mean(x2);
        err_hat2(j,i) = mean(x2)-mu;
        var_hat2(j,i) = var(x2);
        errv_hat2(j,i) = var(x2)-var2;

    end
end

%% Plot the error of the mean estimator
h1 = semilogx(N, (err_hat1), "r", "LineWidth", 2);
hold on
h2 = semilogx(N, (err_hat2), "b", "LineWidth", 1);
xlim([0,10^6])
xlabel('Number of sample points')
ylabel('Error of the mean esimator')
legend([h1(1), h2(1)], {'N(2,3^2)', 'N(2,0.5^2)'})
