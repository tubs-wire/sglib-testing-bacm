%Tutorial on interpolation

%Electrical network example
%
%A(u(p);p)=f(p)

%Surrogate/proxi model:
%
%u_I(p)=\sum_{j=1}^M u_j\Phi_j(p)

%N interpolation points p_i, require:
%u(p_i)=u_I(p_i)  i=1..N
%M=N interpolation
%M<N regression

%Interpolation (N=M):
%u(p_1)=\sum_{j=1}^M u_j\Phi_j(p_1)=u_1\Phi_1(p_1)+u_2\Phi_2(p_1)+...u_M\Phi_M(p_1)
%u(p_2)=\sum_{j=1}^M u_j\Phi_j(p_2)=u_1\Phi_1(p_2)+u_2\Phi_2(p_2)+...u_M\Phi_M(p_2)
%
%
%u(p_M)=\sum_{j=1}^M u_j\Phi_j(p_M)=u_1\Phi_1(p_M)+u_2\Phi_2(p_M)+...u_M\Phi_M(p_M)
%In matrix form: b=Wu
%
%Algorithm
%1.Setup basis: \Phi_1, \Phi_2,.. \Phi_M
%2. Determine points: p_1, p_2, .. p_M (Chebyhev-points, Gauß-points.. etc.)
%3. Compute matrix: W_{ij}=\Phi_j(p_i)
%4. Compute r.h.s.: b_i=u(p_I)=A^{-1}(f(p_I);p_i)
%5. Solve for u: Wu=b
%6. Evaluate any uI at spacific p
gpc_evaluate(u,V,p)