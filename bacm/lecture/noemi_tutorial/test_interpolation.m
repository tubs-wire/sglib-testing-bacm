%Tutorial on interpolation

%Electrical network example
%
%A(u(p);p)=f(p)

%Surrogate/proxi model:
%
%u_I(p)=\sum_{j=1}^M u_j\Phi_j(p)

%N interpolation points p_i, require:
%u(p_i)=u_I(p_i)  i=1..N
%M=N interpolation
%M<N regression

%Interpolation (N=M):
%u(p_1)=\sum_{j=1}^M u_j\Phi_j(p_1)=u_1\Phi_1(p_1)+u_2\Phi_2(p_1)+...u_M\Phi_M(p_1)
%u(p_2)=\sum_{j=1}^M u_j\Phi_j(p_2)=u_1\Phi_1(p_2)+u_2\Phi_2(p_2)+...u_M\Phi_M(p_2)
%
%
%u(p_M)=\sum_{j=1}^M u_j\Phi_j(p_M)=u_1\Phi_1(p_M)+u_2\Phi_2(p_M)+...u_M\Phi_M(p_M)
%In matrix form: b=Wu
%
%Algorithm
%1.Setup basis: \Phi_1, \Phi_2,.. \Phi_M
%2. Determine points: p_1, p_2, .. p_M (Chebyhev-points, Gauß-points.. etc.)
%3. Compute matrix: W_{ij}=\Phi_j(p_i)
%4. Compute r.h.s.: b_i=u(p_I)=A^{-1}(f(p_I);p_i)
%5. Solve for u: Wu=b
%6. Evaluate any uI at spacific p
init_func = @electrical_network_init;
solve_func = @electrical_network_solve;
c=4; % index of the component of u to interpolate


% 1. Setup basis
V=gpcbasis_create('pu', 'p', 3);
M=gpcbasis_size(V,1);

% 2. Setup the points
N=1000;
p = gpcgerm_sample(V, N);

% 3. Compute matrix (transpose!)
W = gpcbasis_evaluate(V, p)';

% 4. Compute RHS
state=funcall(init_func);

b = zeros(N,1);
for i=1:N
    ui = funcall(solve_func, state, p(:,i));
    b(i) = ui(c);
end

% 5. Solve for u
u_i_alpha = (W\b)';


% Plot response surface and interpolation points
plot_response_surface(u_i_alpha, V)
u=gpc_evaluate(u_i_alpha, V, p);
hold on; plot3(p(1,:), p(2,:), u(1,:)+0.002, 'kx'); hold off;
hold on; plot3(p(1,:), p(2,:), b+0.002, 'ko'); hold off;







%V=gpcbasis_create('pu', 'p', 4, 'full_tensor', true)
