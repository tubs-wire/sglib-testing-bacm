function u_samples=get_samples_from_file(file_name, method,tt,varargin)

%Description: samples from saved file
%   samples=
%   get_samples_from_file(file_names,'MC', tt, 'ivar', 2)
% (get MC samples at tt-th timestep for the second solution variable)

%Output:
%  -u_samples(1xN)
%  -if IVAR is not given and N_VAR>1 u_samples{n_var,1}(1xN)
%
%Input:
%-FILENAME filename where samples/gpc coeffs are saved (samples can be stored in multiple files)
%-METHOD: type of statistics saved:
%   -'MC'- samples are saved
%   - 'gPCE' -gPCE coefficients are saved
%   - 'tt'  - get samples from tt-th timestep
% 
%
%OPTIONAL INPUT:
% I_VAR: draw statistics only the I_VARths variable
% N:    : number of samples to be drawn, default: 1000


%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig

options=varargin2options(varargin);
[i_var,options]=get_option(options, 'i_var', []);
[N,options]=get_option(options, 'N', 1000);
check_unsupported_options(options, mfilename);

%% Load first file and get sample size

switch method
    case 'MC'
        u_samples=get_MC_samples_from_file(file_name,tt,  'i_var',i_var);
    case 'gPCE'
        %Get coefficients from file
        [u_i_alpha, V_u]=get_gpc_coeffs_from_file(file_name, tt, i_var);
        %Sample from gPCE and reshape
        Psi_samples=gpcgerm_sample(V_u, N);
        u_samples=gpc_evaluate(u_i_alpha, V_u, Psi_samples);
        n_sample=size(u_samples,1);
        if ~(n_sample==1)
        u_samples=num2cell(u_samples,2);
        end
        
end
end
%% 

function [u_i_alpha, V_u]=get_gpc_coeffs_from_file(file_name,tt, i_var)

Sol=load(file_name);

%% get u_i _alpha coefficients

if ismember('u_i_alpha',fieldnames(Sol))
    u_i_alpha = Sol.u_i_alpha;
elseif  ismember('u_i_alpha_tosave',fieldnames(Sol))
    u_i_alpha = Sol.u_i_alpha_tosave;
else
       error('given filename does not contain u_i_alpha or u_i_alpha_tosave variable')
end


if ~isempty(i_var)
    u_i_alpha=u_i_alpha(i_var,:,tt);
else
    u_i_alpha=u_i_alpha(:,:,tt);
end
%% get V_u with polysis and multiindex

if ismember('V_u',fieldnames(Sol))
    V_u = Sol.V_u;
elseif  ismember('p_u',fieldnames(Sol)) && ismember('polysys',fieldnames(Sol))
    V_u=gpcbasis_create(Sol.polysys,'p', Sol.p_u);
else
    error('given filename does not contain V_u variable or p_u and polysys variables')
end
if iscell(V_u{1})
    V_u{1}= strcat(V_u{1}{:});
end
end

