%% L1 optimization with linear programming
% Example: Given the Anscombes-quartet data points x and y,
% find a polynomial model that is optimal in an L1 sense.
%
% Data: $x_i$ and $y_i$, $i = 1..m$
%
% Polynomial model: $y_p = q_0 + q_1 x + q_2 x^2...= A(x)q$
%
% Find q such that $||(Aq)_iq_i-y_i||_{L1} = \sum_i |A(x_i)q-y_i|$ is
% minimized
%
% Let's transform this task to a linear programming problem:
%
% Instead of minimising $|(A(x_i)q-y_i||_{L1}$, we minimise
%
% $\sum u_i$ subjected to $u_i\geq |A(x_i)q-y_i|\quad \forall i = 1..m$
%
% which is the same as minimising
%
% $\sum 1^Tu$ subjected to $u_i\geq A(x_i)q-y_i$ and $u_i\geq -(A(x_i)q-y_i)\quad \forall i = 1..m$
%
% rearranging the constraints, the minimisation is subjected to
%
% $y_i\geq A(x_i)q-u_i$ and $-y_i\geq -A(x_i)q-u_i\quad \forall i = 1..m$
%
% Introducing the variable $z = [\begin{array}{cc}q & u \end{array}]^T$
%
% the task is to compute
%
% $z_{opt}^T = argmin_z \left(
% \begin{array}{c}
% 0\\
% 1
% \end{array}\right)^T
% \left(\begin{array}{c}
% q\\
% u
% \end{array}\right)$
% subjected to
% $\left(\begin{array}{cc}
% A & -I\\
% -A& -I
% \end{array}\right)
% \left(\begin{array}{c}
% q\\
% u
% \end{array}\right)^T
% \leq
% \left(\begin{array}{c}
% y\\
% -y
% \end{array}
% \right).$
%
% This problem then can be written in the form
%
% $z_{opt} = argmin_z f^Tz$ subjected to $A_lz\leq y_l$
%
% with
%
% $f =\left(
% \begin{array}{c}
% 0\\
% 1
% \end{array}\right)$
% , 
% $A_l =\left(\begin{array}{cc}
% A & -I\\
% -A& -I
% \end{array}\right)$
%
% and
%
% $y_l =
% \left(\begin{array}{c}
% y\\
% -y
% \end{array}
% \right)$


for model_i = 1:4
    % load Anscombes-quartet data
    [x,y] = anscombes_quartet(model_i); 
    m = length(x);
    
    %% compute the linear regression model
    
    % max degree of polynomial basis
    n=1; % linear regression
    % evaluate monomials at the 'x' data points
    A = poly_matrix(x, n);
    % number of approximating functions
    N = size(A, 2);
    % reformulate to linear programing task
    f = [zeros(N, 1); ones(m,1)];
    A_l = [A, -eye(m);-A, -eye(m)];
    y_l = [y; -y];
    
    z_opt = linprog(f, A_l, y_l);
    q = z_opt(1:N);
    % plot the regression model
    xs= sort(x);
    % plot data
    figure
    plot(x,y, 'x', 'LineWidth', 2)
    hold on
    plot(xs, polyval(q,xs), 'lineWidth', 2)
    % squared error of the regression model
    ss_res = sum((y - polyval(q,x)).^2)
    % L1 norm of the regression model
    L1_res = sum(abs(y - polyval(q,x)))
    
    %% compute quadratic regression model
   % max degree of polynomial basis
    n=2; % linear regression
    % evaluate monomials at the 'x' data points
    A = poly_matrix(x, n);
    % number of approximating functions
    N = size(A, 2);
    % reformulate to linear programing task
    f = [zeros(N, 1); ones(m,1)];
    A_l = [A, -eye(m);-A, -eye(m)];
    y_l = [y; -y];
    
    z_opt = linprog(f, A_l, y_l);
    q = z_opt(1:N);
    % plot the regression model
    xs= sort(x);
    % plot data
    figure
    plot(x,y, 'x', 'LineWidth', 2)
    hold on
    plot(xs, polyval(q,xs), 'lineWidth', 2)
    % squared error of the regression model
    ss_res = sum((y - polyval(q,x)).^2)
    % L1 norm of the regression model
    L1_res = sum(abs(y - polyval(q,x)))
end