multiplot_init(2,2);

for model_i = 1:4
    % load Anscombes-quartet data
    [x,y] = anscombes_quartet(model_i);
    multiplot
    % plot data
    plot(x,y, 'x', 'LineWidth', 2)
    hold on
    
    %% compute the linear regression model
    
    % max degree of polynomial basis
    n=1; % linear regression
    % evaluate monomials at the 'x' data points
    A = poly_matrix(x, n);
    % solve for the coefficients of the regression model
    q = A\y;
    % plot the regression model
    [xs, ind]= sort(x);
    plot(xs, polyval(q,xs), 'lineWidth', 2)
    % squared error of the regression model
    ss_res = sum((y - polyval(q,x)).^2);
    
    %% compute quadratic regression model
    
    % max degree of polynomial basis
    n=2; % linear regression
    % evaluate monomials at the 'x' data points
    A = poly_matrix(x, n);
    % solve for the coefficients of the regression model
    q = A\y;
    % plot the regression model
    plot(xs, polyval(q,xs), 'lineWidth', 2)
    % squared error of the regression model
    ss_res = sum((y - polyval(q,x)).^2);
end