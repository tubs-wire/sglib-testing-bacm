function u_samples=get_MC_samples_from_file(file_name,tt,varargin)

%Description: samples from saved file, get samples for TT-th value of  
%   samples=
%   get_MC_samples_from_file(file_names,tt, 'i_var',1)

%Output:
%  -u_samples(1xN)
%  -if I_VAR is not given and N_VAR>1 u_samples{n_var,1}(1xN)
%
%Input:
%-TT: index of variable which statistics should be 
%-FILENAME (samples can be stored in multiple files)
%          in file u_samples should be {N_VARrx1} sized cell with N_TSTEPxN elements
%with
%   -N_VAR (number of variables)
%   -N_TSTEP (number of timesteps)
%   -N: sample size
%-TT: index of timestep for which the samples should be drawn

%By Noemi FRIEDMAN
%Institute of Scientific Computation
%TU Braunschweig

options=varargin2options(varargin);
[i_var,options]=get_option(options, 'i_var', []);
check_unsupported_options(options, mfilename);

%% Load first file and get sample size

n_files=length(file_name);
if n_files==1
    MC_sol=load(file_name);
else
    MC_sol=load (file_name{1});
end
n_var=size(MC_sol.u_samples,1);
N=MC_sol.N;

if n_files==1
    N_per=N;
else
    if iscell(MC_sol.u_samples)
        N_per=size(MC_sol.u_samples{1,1},2);
    else
        N_per=size(MC_sol.u_samples,2);
    end
end

%% Initialize memory
if n_var==1 || ~(isempty(i_var))
    u_samples=zeros(1,N);
else
    u_samples=cell(n_var,1);
    for j_var=1:n_var
        u_samples{j_var,1}=zeros(1,N);
    end
end
%% put solution together in one vector per variable from all files at tt
% timestep
for k_files=1:n_files
    if ~(k_files==1)
        clear 'MC_sol'
        MC_sol=load (file_name{k_files});
    end
    if n_var==1 || ~(isempty(i_var))
        if k_files==n_files
            u_samples( 1, (k_files-1)*N_per+1:end  )=MC_sol.u_samples{i_var,1}(tt,:);
        else
            u_samples( 1, (k_files-1)*N_per+1:k_files*N_per  )=MC_sol.u_samples{i_var,1}(tt,:);
        end
    else
        
        for j_var=1:n_var
            if k_files==n_files
                u_samples{j_var,1}( 1, (k_files-1)*N_per+1:end )=MC_sol.u_samples{j_var,1}(tt,:);
            else
                u_samples{j_var,1}( 1, (k_files-1)*N_per+1:k_files*N_per  )=MC_sol.u_samples{j_var,1}(tt,:);
            end
        end
    end
end

