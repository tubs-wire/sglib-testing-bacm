function pi_est = esimate_pi_by_MC(N, mode)
%% Approximation of $\pi$ 
%
% Here we approximate the the value of $\pi$ with Monte Carlo (mode ='MC') or
% Quasi Monte Carlo (mode='QMC' methods.
% The estimation is calculated by throwing N darts and approximate the ratio
% of the area of the circle and the area of the  square by the ratio of the
% darts falling within the unit circle and the total number of darts.  
%
% Ratio of the area of the circle with radius $r$ and the area of the
% square with sidelengths $r$
% $q=\frac{r^2\pi}{4r^2}$
% Rearranging for $\pi$
% $\pi=4q$

%% Throw N darts to a squared board randomly
% Throw darts to the squared domain $\Omega\in [0,1]\times[0,1]$.

    % The random x1,x2 coordinates of the darts:
    switch mode
        case 'MC'
            x = rand(2,N);
        case 'QMC'
            x = (halton_sequence(N,2))';
    end
    
    %% Estimation of $q$, the ratio of the areas, and from that the value of $\pi$
    % (number of the points within the circle per N)
    
    % Get index of darts within the circle:
    ind = find(x(1,:).^2+x(2,:).^2<1);
    
    % Estimate of the ratio $q$:
    q_est = length(ind)/N;
    
    % Estimate of $\pi$:
    pi_est =q_est*4;
end