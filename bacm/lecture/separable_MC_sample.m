function y=separable_MC_sample(X, N)
% SEPARABLE_MC_SAMPLE Performs Separable MC sampling from 
% permutations of the samples without replacement.
%   Y=SEPARABLE_MC_SAMPLE(X, N) returns a matrix of size (M x (N over K)
%
% UNDER CONSTRUCTION!!!!!!!!!!!!!!!!!!!!!!!
% Example (<a href="matlab:run_example eparable_MC_sample">run</a>)
%   % Sample from a set of prime numbers
%   x = [2, 3, 5; 1, 2, 3];
%   separable_MC_sample(x)
%
% See also...

%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

[d, n] = size(X);
max_n=nchoosek(n*d,d);
if nargin<2 || isempty(N)
    N = max_n;
end
%for i=1:
%ind = randi(imax,m,n);

% The reshape is necessary if N or M is 1 and X is vector of different
% shape
y=reshape(x(ind), m, n);