function [state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, list_of_RVs, varargin)

%function for the specification of random and not random paramters
%stored in a class STATE and the type of the related polygons in POLYSYS

%state.(NAME_OF_VARIABLE):  distribution properties (if parameter is rand)
%                           the mean value (if parameter is not rand)
%state.ref_params:          mean value of paramters
%state.actual_params:       mean value (if parameter is rand)
%                           [] (if parameter is not rand - to be passed to
%                           stochastic calculation
%state.distrib_params:      only used if samples are not to be standardized
%POLYSYS:                   vector with the type the associated orthogonal polygons

options = varargin2options(varargin);
[multi_dim, options] = get_option(options, 'multi_dim', {});
check_unsupported_options(options, mfilename);
polysys_tot='';
ind=0;          %RV index
numb_tot_RV=0; %counter for number of total random variables
for i=1:num_tot_params
    is_param_rand=strmatch(list_of_params{i},list_of_RVs);
    if isempty(is_param_rand)  %if parameter is not random (put mean value to state.actual.params and to state.ref_params)
        state.(list_of_params{i})=params.(list_of_params{i}){1};
        % get reference values
        switch params.(list_of_params{i}){3}
            case 'H'    % Gaussian
                state.actual_params.(list_of_params{i})=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
            case 'G'    % Lognormal
                state.actual_params.(list_of_params{i})=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
            case 'P'    % Uniform
                a=params.(list_of_params{i}){1};
                b=params.(list_of_params{i}){2};
                state.actual_params.(list_of_params{i})=(a+b)/2;
                state.ref_params.(list_of_params{i})=(a+b)/2;
            case 'L'    % Exponential - exponential is not yet implemented properly
                state.actual_params.(list_of_params{i})=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                %state.actual_params.(list_of_params{i})=1/params.(list_of_params{i}){1};
                %state.ref_params.(list_of_params{i})=1/params.(list_of_params{i}){1};
            case 'T'    % Arcsin
                state.actual_params.(list_of_params{i})=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
            case 'U'    % Semicircle
                state.actual_params.(list_of_params{i})=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
        end
        if ~isempty(multi_dim)
            state.actual_params.(list_of_params{i})=state.actual_params.(list_of_params{i})*ones(multi_dim.dim_params{i}); 
        end
        
    else   %if parameter is random
        ind=ind+1;
        
                %define polysys
        if  strcmp(params.(list_of_params{i}){3}, 'G');
            polysys(ind)='H';
        else
            polysys(ind)=params.(list_of_params{i}){3};
        end
        
        
        state.list_of_RVs{ind}=list_of_params{i};
        state.actual_params.(list_of_params{i})=[];
        state.(list_of_params{i}).distrib=params.(list_of_params{i}){3};
        
        
        switch params.(list_of_params{i}){3}
            case 'H'    % Gaussian
                state.(list_of_params{i}).mean=params.(list_of_params{i}){1};
                state.(list_of_params{i}).var=params.(list_of_params{i}){2};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                %state.distrib_params.first(ind)=state.(list_of_params{i}).mean;
                %state.distrib_params.second(ind)=state.(list_of_params{i}).var;
            case 'G'    % Lognormal
                state.(list_of_params{i}).mean=params.(list_of_params{i}){1};
                state.(list_of_params{i}).var=params.(list_of_params{i}){2};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                mean=params.(list_of_params{i}){1};
                var=params.(list_of_params{i}){2};
                state.(list_of_params{i}).mu=log((mean^2)/sqrt(var+mean^2));
                state.(list_of_params{i}).sigma=sqrt(log(1+var/(mean^2)));
                %state.distrib_params.first(ind)=state.(list_of_params{i}).mean;
                %state.distrib_params.second(ind)=state.(list_of_params{i}).var;
            case 'P'    % Uniform
                state.(list_of_params{i}).left_value=params.(list_of_params{i}){1};
                state.(list_of_params{i}).right_value=params.(list_of_params{i}){2};
                a=params.(list_of_params{i}){1};
                b=params.(list_of_params{i}){2};
                state.ref_params.(list_of_params{i})=(a+b)/2;
                state.(list_of_params{i}).shift=(a+b)/2;
                state.(list_of_params{i}).multipl=(b-a)/2;
                %state.distrib_params.first(ind)=state.(list_of_params{i}).left_value;
                %state.distrib_params.second(ind)=state.(list_of_params{i}).right_value;
            case 'L'    % Exponential
                state.(list_of_params{i}).multipl=params.(list_of_params{i}){1};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                %properly lambda should be given and not multipl:
                %state.(list_of_params{i}).lambda=params.(list_of_params{i}){2};
                %state.ref_params.(list_of_params{i})=1/params.(list_of_params{i}){2};
                warning ('exponential distribution is not yet properly tested')
                %state.distrib_params.first(ind)=state.(list_of_params{i}).lambda;
                %state.distrib_params.second(ind)=[];
            case 'T'    % Arcsin
                state.(list_of_params{i}).shift=params.(list_of_params{i}){1};
                state.(list_of_params{i}).multipl=params.(list_of_params{i}){2};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                %state.distrib_params.first(ind)=state.(list_of_params{i}).shift;
                %state.distrib_params.second(ind)=state.(list_of_params{i}).multipl;
            case 'U'    % Semicircle
                state.(list_of_params{i}).shift=params.(list_of_params{i}){1};
                state.(list_of_params{i}).multipl=params.(list_of_params{i}){2};
                state.ref_params.(list_of_params{i})=params.(list_of_params{i}){1};
                %state.distrib_params.first(ind)=state.(list_of_params{i}).shift;
                %state.distrib_params.second(ind)=state.(list_of_params{i}).multipl;
        end
        if ~isempty(multi_dim)
            state.multi_dim.size_independent_params{ind}=multi_dim.size_independent_params{i};
            state.multi_dim.index_of_ind_params{ind}=multi_dim.index_of_ind_params{i};
            numb_tot_RV=numb_tot_RV+multi_dim.size_independent_params{i};
            polysys_tot=strcat(polysys_tot,repmat(polysys(ind),1,multi_dim.size_independent_params{i}));
        end
    end
    if ~isempty(multi_dim)
        state.ref_params.(list_of_params{i})=state.ref_params.(list_of_params{i})*ones(multi_dim.dim_params{i});
    end
end

%polysys=strcat(polysys{:});


%% Define number of independent random variables and polysys
if isempty(multi_dim)
    state.num_params=ind;
    %state.num_tot_RVs=numb_tot_RV;
else
    state.num_params=numb_tot_RV;
    polysys=polysys_tot;
end

