function [u_i_alpha, model, x] = compute_response_surface_tensor_interpolate_g(model, Qset, V_u, p_u, varargin)
% COMPUTE_RESPONSE_SURFACE_TENSOR_INTERPOLATE Short description of compute_response_surface_tensor_interpolate.
%   COMPUTE_RESPONSE_SURFACE_TENSOR_INTERPOLATE Long description of compute_response_surface_tensor_interpolate.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example compute_response_surface_tensor_interpolate">run</a>)
%
% See also

%   Noemi Friedman & Elmar Zander
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[is_xi_stdnor,options]=get_option(options, 'is_xi_stdnor', false);
check_unsupported_options(options,mfilename);

% compute interpolation points (need one level more than order)
[xi,w] = gpc_integrate([], V_u, p_u+1, 'grid', 'full_tensor');

% evaluate parameter value at integration points
if is_xi_stdnor
    q=Qset.stdnor2params(xi);
else
    q=Qset.germ2params(xi);
end

% compute the (generalised) Vandermonde matrix
A=gpcbasis_evaluate(V_u, xi);

Q = length(w);
u = zeros(u_size, Q);
for j = 1:Q
    q_j = q(:,j);
    u(:,j) = funcall(Q2u_map, q_j);
end

u_i_alpha = u/A;

% the next line should go into a unit test
%   norm(gpc_evaluate(u_i_alpha, V_u, x)-u)
