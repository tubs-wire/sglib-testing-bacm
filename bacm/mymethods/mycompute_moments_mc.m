function [u_mean, u_var, model] = mycompute_moments_mc(model, q_ij, varargin)

% COMPUTE_MOMENTS_MC Compute mean and variance by Monte-Carlo.
%   [U_MEAN, U_VAR] = COMPUTE_MOMENTS_MC(INIT_FUNC, SOLVE_FUNC, N) computes
%   the mean and variance of a system described by INIT_FUNC and SOLVE_FUNC
%   by a Monte-Carlo method with N samples. The distribution is specified
%   by POLYSYS (see <a href="matlab:help gpc">GPC</a>).
%
% Example (<a href="matlab:run_example compute_moments_mc">run</a>)
%
% See also GPC

%   Elmar Zander and messed up by Noemi Friedman :)
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

N=size(q_ij,2);

u_mean = [];
u_var = [];
for j = 1:N
    display(strvarexpand('$j$/$N$'));
    q_j = q_ij(:,j);
    [u_j, model] = model_solve(model, q_j);
    [u_mean, u_var] = mean_var_update(j, u_j, u_mean, u_var);
end
