function u_i_alpha = calculate_gpc_coeffs(V_u,  x, u, w, varargin)
%METHOD: optional input 'PROJECT'(default) or 'COLLOC'

options=varargin2options(varargin);
[wp, options]=get_option(options, 'weight_pow',1);
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting',[]);
[method, options]=get_option(options, 'method','project'); %or 'colloc'
[weighted_regr, options]=get_option(options, 'weighted_regr',false);
check_unsupported_options(options, mfilename);

num_vars = size(u,1); % nnode

M = gpcbasis_size(V_u, 1);
Q = length(w);

%% Projection with full tensor grid
switch method
    case 'project'
        u_i_alpha = zeros(num_vars, M);
        for j = 1:Q
            x_j = x(:, j);
            u_i_j=u(:,j);
            psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
            u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
        end
    case 'colloc'
        % compute the (generalised) Vandermonde matrix
        A=gpcbasis_evaluate(V_u, x);
        if ~weighted_regr
            u_i_alpha = u/A;
        else
            %for weighted regression
            %Get the weights
            rel_err=get_rel_deviation_from_meas(u, u_ref, x, 'ignore_indices', ind_ign);
            W = diag(1./rel_err.^wp)/sum(1./rel_err.^wp);
            u_i_alpha =((A*W*A')\(A*W*u_ij'))';
        end
end
