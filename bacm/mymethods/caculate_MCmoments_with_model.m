function [u_mean, u_var, calc_model] = caculate_MCmoments_with_model(model, q_ij, varargin)

options=varargin2options(varargin);
[for_measurement,options]=get_option(options, 'for_measurement', false);
check_unsupported_options(options,mfilename);

num_params = size(q_ij,1);


if for_measurement
    num_vars = model.measurement_dim();
    calc_model = spmodel_init(num_params, num_vars, 'solve_func', @solve_with_meas_model);
else
    num_vars = model.response_dim();
    calc_model = spmodel_init(num_params, num_vars, 'solve_func', @solve_with_model);
end
calc_model.model = model;

func = funcreate('mycompute_moments_mc');

[u_mean, u_var, calc_model]=funcall(func, calc_model, q_ij);

%[u_i_alpha, ~]=compute_response_surface_projection(spectral_model, @(xi)(Q.germ2params(xi)), V_u, 3, 'grid', 'full_tensor');
% [u_mean, u_var] = multivector_map(@(coeffs)(gpc_moments(coeffs, V_u)), u_i_alpha);
% model.plot_all(u_mean)
% model.plot_all(u_var)




function [u, solve_info, spec_model]=solve_with_model(spec_model, q, model, varargin)
model = spec_model.model;
u = model.compute_response(q);
solve_info=struct();
function [y, solve_info, spec_model]=solve_with_meas_model(spec_model, q, model, varargin)
model = spec_model.model;
u = model.compute_response(q);
y = model.compute_measurements(u);
solve_info=struct();
