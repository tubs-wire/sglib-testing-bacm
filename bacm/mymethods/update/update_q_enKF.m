function qa_i=update_q_enKF(Q, E,  y_i_alpha, V_y, y_m, N)
%The Kalman gain
[Ke, xi_i_alpha,V_xi]=get_Kalman_gain(Q, E,  y_i_alpha, V_y);
%sample from the germ and calculate the response at the samples points
xi_i=gpc_sample(xi_i_alpha,V_xi, N);
% Compute observable from the germ samples
u_i=gpc_evaluate(y_i_alpha, V_y, xi_i);
e_i=E.sample(N);
% Update samples
%(originally: (y_m+e_i)-u_i;
xia_i=xi_i+Ke*(binfun(@minus,y_m,u_i-e_i));
% map xi samples to q samples
qa_i=Q.stdnor2params(xia_i);
end
