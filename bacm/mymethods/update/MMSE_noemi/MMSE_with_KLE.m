function [xi2y2xi_func, xi_mmse, y_mmse, xi_post]=...
    MMSE_with_KLE(y_i_alpha, V_y, y_m, x_m, E_m, p_phi, n_modes, varargin)

options=varargin2options(varargin);
[N,options]=get_option(options, 'N', 10000);
check_unsupported_options(options, mfilename);

%% Low rank representation

[r_i_k, sigma_k, y_i_alpha_fluct]=KLE_from_gPCE(y_i_alpha, V_y, 50, x_m);
plot(sigma_k, 'x')
xlim([0,15]);
A=inv(diag(sigma_k(1:n_modes)))*r_i_k(:,1:n_modes)';
mean_y=gpc_moments(y_i_alpha, V_y);

Ys_func = @(xi)gpc_evaluate(A*y_i_alpha_fluct, V_y, xi);

%% Error model
% Covariance matrix of transformed error
C_eps=A*diag(E_m.var)*A';

% standard deviations of each variable
S = diag(sqrt(diag(C_eps)));
S_inv = inv(S);
% Correlation matrix
Cor_eps = S_inv * C_eps * S_inv;
L = chol(Cor_eps, 'lower');

% Get transformation from standard normal distributed E to eps
E=generate_stdrn_simparamset(ones(n_modes));

% map from error germ to error
E_func = @(xi)(S*L*xi);

% PCE of the error
[~, V_e] = E.gpc_expand();

% allways update the germs distribution
XI=gpcgerm2simparamset(V_y);
[xi_i_alpha, V_xi]=XI.gpc_expand();

% MMSE order and num integration orders
%p_phi: order of the update defined on the top
p_xi = gpcbasis_info(V_xi, 'total_degree');
p_gpc= gpcbasis_info(V_y, 'total_degree');
p_int_mmse=max(p_gpc*p_phi+1,ceil((p_xi+p_phi*p_gpc+1)/2));
%p_int_proj=p_qn+1;
%% Update
% extra_options={'filter_func', filter_func};
% [q_post_i_beta, V_q_post] = mmse_update_gpc_noemi(q_i_alpha, ...
%     Ys_func, V_q, A*meas(:), E_func, V_e, p_phi, p_int_mmse,...
%     p_qn, p_int_proj, 'int_grid', 'qmc', 'extra_options', extra_options);

[xi2y2xi_func, xi_mmse, ~] = my_mmse_gpc(xi_i_alpha, ...
    Ys_func, V_xi, A*(y_m(:)-mean_y), E_func, V_e, p_phi, p_int_mmse,...
     'int_grid', 'qmc');
 
y_mmse=gpc_evaluate(y_i_alpha, V_y, xi_mmse);

% Posterior samples
if nargout>3;
    xi_prior=XI.sample(N);
    e_m=E_m.sample(N);
    xi_post=binfun(@plus,funcall(xi2y2xi_func, [xi_prior;e_m]), xi_mmse);
end
