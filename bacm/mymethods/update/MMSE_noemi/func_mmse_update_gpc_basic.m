function [QM_func, qm, phi_func]=func_mmse_update_gpc_basic...
    (X_func, YM_func, V_xe, ym, p_phi, p_int_mmse, p_qn, X_i_alpha, varargin)
% MMSE_UPDATE_GPC_BASIC Short description of mmse_update_gpc_basic.
%   MMSE_UPDATE_GPC_BASIC Long description of mmse_update_gpc_basic.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example mmse_update_gpc_basic">run</a>)
%
% See also

%   Elmar Zander (modified by No�mi Friedman)
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[int_grid,options]=get_option(options, 'int_grid', 'full_tensor');
[extra_options,options]=get_option(options, 'extra_options', {});
[project2gpc,options]=get_option(options, 'project2gpc', false);
check_unsupported_options(options, mfilename);

% Now compute the MMSE estimator for X given Y and make a function
% out of this estimator

[phi_j_delta,V_phi]=my_mmse_estimate(X_func, YM_func, V_xe, p_phi, p_int_mmse,...
    'int_grid', int_grid, extra_options{:});
phi_func = gpc_function(phi_j_delta, V_phi);

% Create the prediction stochastic model for X as function
% composition between Y and phi
XM_func = funcompose(YM_func, phi_func);


% Compute the best estimator value xm=phi(ym)
xm = funcall(phi_func, ym);
X2Xn_func=@(xi)(xi-funcall(XM_func, xi)+xm);

if project2gpc
    [V_xen, Pr_xyn] = gpcbasis_modify(V_xe, 'p', p_qn);
    Xpost_i_beta = my_gpc_projection(X2Xn_func, V_xen, p_int_proj);
    Xn_i_alpha=Xpost_i_beta-X_i_alpha * Pr_qyn;
    xn_i_alpha-
    
    %The updated model xn and the update should be orthogonal
    assert(norm(gpc_covariance(xn_i_alpha, V_x, xm_i_alpha))<1e-10)

    %The new model pn and the update should be orthogonal
    CQn = norm(gpc_covariance(Qn_i_beta, V_qn), 'fro');
    CQnQM = norm(gpc_covariance(Qn_i_beta, V_qn, QM_i_beta), 'fro');
    if CQnQM>1e-10*CQn
        warning('sglib:mmse_update_gpc_basic', 'gpc update not orthogonal (%g>1e-10*%g)', CQnQM, CQn);
    end
    [~, Q_var] = gpc_moments(Q_i_alpha, V_qy);
    [~, Qn_var]= gpc_moments(Qn_i_beta, V_qn);
    if any(Q_var - Qn_var<-1e-10)
        %keyboard
        warning('sglib:mmse_update_gpc_basic', 'gpc no variance reduction');
    end
end



