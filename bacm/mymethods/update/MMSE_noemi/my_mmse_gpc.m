function [QM_func, phi_func, qm, YM_func]=my_mmse_gpc...
    (Q_i_alpha, Y_func, V_q, ym, eps_func, V_eps, p_phi, p_int_mmse, varargin)
% MMSE_UPDATE_GPC Update a GPC given some measurements and a measurement model.
%
% Example (<a href="matlab:run_example mmse_update_gpc">run</a>)
%
% See also

%   Elmar Zander (modified by No�mi Friedman)
%   changes: projection to gPCE subspace removed, only the estimator is computed)
%   polynomial coeffs PHI_I_ALPHA can be computed by QMC integration
%   through the INT_GRID option)
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[int_grid,options]=get_option(options, 'int_grid', 'smolyak');
[extra_options,options]=get_option(options, 'extra_options', {});
check_unsupported_options(options, mfilename);

% Combine the bases of the a priori model (V_X) and of the error model
% (V_eps)
[V_qe, ind_V_X, ~, ind_xi_Q, ind_xi_eps] = gpcbasis_combine(V_q, V_eps, 'outer_sum');

% Create evaluation function for measurement + error model (YM_func) 
%YM_func = @(xi)(...
%    eval_on_subset(xi, Y_func, ind_xi_Q) + ...
%    eval_on_subset(xi, eps_func, ind_xi_eps));
YM_func = funcreate(@eval_separately, @funarg, Y_func, ind_xi_Q, eps_func, ind_xi_eps);

% Extend the GPC coefficients X_i_alpha to the larger basis V_Xn
Q_i_beta = zeros(size(Q_i_alpha,1), gpcbasis_size(V_qe,1));
Q_i_beta(:,ind_V_X) = Q_i_alpha;

[QM_func, qm, phi_func]=my_mmse_update_gpc_basic...
    (Q_i_beta, YM_func, V_qe, ym, p_phi, p_int_mmse, 'int_grid', int_grid,...
    'extra_options', extra_options);

function y=eval_separately(xi, Y_func, ind_xi_Q, eps_func, ind_xi_eps)
y = eval_on_subset(xi, Y_func, ind_xi_Q) + ...
    eval_on_subset(xi, eps_func, ind_xi_eps);


function y = eval_on_subset(xi, func, ind)
% EVAL_ON_SUBSET Evaluate function only on subset of input array
%   Y = EVAL_ON_SUBSET(XI, FUNC, IND) evaluates FUNC only on
%   XI_REDUCED=XI(:,IND). Since many of of the XI_REDUCED may be identical
%   they are made unique before and later put into the right place.

[xi_red, ~, ib] = unique(xi(ind, :)', 'rows');
xi_red = xi_red';
y_red = funcall(func, xi_red);
y = y_red(:,ib);
