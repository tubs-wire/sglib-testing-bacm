function [K, xi_i_alpha, V_xi, ind_xi, ind_y]=get_Kalman_gain(Q, E,  y_i_alpha, V_y)

% gPCE of the measurement
C_y=gpc_covariance(y_i_alpha, V_y);
%P=get_response_covariance(surr_model);
% Get gpc of the error
[e_i_alpha, V_e] = E.gpc_expand();
% Error covariance (C_epsilon)
R = gpc_covariance(e_i_alpha, V_e);
% gPCE expansion of the germ
XI=generate_stdrn_simparamset(ones(Q.num_params));
[xi_i_alpha,V_xi]=XI.gpc_expand();
% Calculation of C_xi_yf with combining the gpces
[C_xi_y, ind_xi, ind_y]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_y, V_xi);
% the extended Kalman gain
K=C_xi_y*inv(C_y+R);
end