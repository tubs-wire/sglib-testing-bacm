function [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(Q, E, y_i_alpha, V_y, y_m)
% Updates the gPCE coefficients of Q from the inputs:
% Y_I_ALPHA and V_Y: gPCE expansion of the observable (coefficients and
% basis)
% Y_M: observation
% Q: simparamset with parameters defined by prior distributions
% E: simparamset with observation error
ym_i_alpha=zeros(size(y_i_alpha));
ym_i_alpha(:,1)=y_m;
%The Kalman gain
[Ke, xi_i_alpha,V_xi, ind_xi]=get_Kalman_gain(Q, E, y_i_alpha, V_y);
U=Ke*(ym_i_alpha-y_i_alpha);
%expand gpc basis of xi
xi_a_i_alpha=zeros(size(U));
xi_a_i_alpha(:,ind_xi)=xi_i_alpha;
%update gPCE of xi
xi_a_i_alpha=xi_a_i_alpha+U;
V_xi_a=V_y;
end