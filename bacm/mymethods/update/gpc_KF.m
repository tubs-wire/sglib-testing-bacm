function [Qp_i_gamma, V_Qp, Z_j_gamma] = gpc_KF(Q_i_alpha, Y_j_alpha, V_Y, E_j_beta, V_E, z_m)
% [QP_I_GAMMA, V_QP] = GPC_KF(Q_I_ALPHA, Y_J_ALPHA, V_Y, E_J_BETA, V_E, Z_M)
% updates some
% parameter Q from measurements of a measurable Y poisened by 
% some measurement noise EPS using the Kalman Filter
%
% Outputs:
% - Qp_i_gamma: posterior gpc coeffs
% - V_Qp:       posterior gpc basis
%
% Inputs:
% - Q_i_alpha: gpc coeffs of Q prior (in the basis V_Y)
% - Y_j_alpha: gpc coeffs of Y  (in the basis V_Y)
% - E_j_beta, V_E: gpc coeffs and basis of the measurement noise EPS
% - z_m: actual measurement
%
% Other notation
%  - N_y=N_eps: size of the measureable
%  - Y: the prediction of what we can measure from a mathematical model (without measurement noise)
%  - Z: the prediction of what we can measure (Z  = Y + E)

%% Covariance of the prediction
C_y=gpc_covariance(Y_j_alpha, V_Y);

%% cov(xi,y)
C_q_y=gpc_covariance(Q_i_alpha, V_Y, Y_j_alpha);

%% Measurement (data)
% Covariance of the measurement noise
C_eps=gpc_covariance(E_j_beta, V_E);

%% update
C_z=C_y+C_eps;

%% Extend basis with measurement error germs 
% Combine the bases of the a priori model (V_Q) and of the error model
% (V_E)
% [V_Qp, Pr_V_Q, Pr_V_E, Res_Xi_Q, Res_Xi_E] = ...
%     gpcbasis_combine(V_Y, V_E, 'outer_sum', 'as_operators', true);
[V_Qp, Pr_V_Q, Pr_V_E] = ...
    gpcbasis_combine(V_Y, V_E, 'outer_sum', 'as_operators', true);

Q_i_gamma=Q_i_alpha*Pr_V_Q;

Z_j_gamma=Y_j_alpha*Pr_V_Q+ E_j_beta*Pr_V_E;

%% update
Qp_i_gamma      = Q_i_gamma       - C_q_y * (C_z\Z_j_gamma);
Qp_i_gamma(:,1) = Qp_i_gamma(:,1) + C_q_y * (C_z\z_m);
