function [C_xi_y, ind_xi, ind_y, V_xi_y, xi_ext_i_alpha]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_y, V_xi)
n_y=size(y_i_alpha, 1);
n_xi=size(xi_i_alpha,1);
% extend the basis to a common basis
[V_xi_y, ind_xi, ind_y]=gpcbasis_combine(V_xi,V_y, 'inner_sum');
% xi coefficients in the new basis
xi_ext_i_alpha=zeros(n_xi,gpcbasis_size(V_xi_y,1));
xi_ext_i_alpha(:, ind_xi)=xi_i_alpha;
y_ext_i_alpha=zeros(n_y, gpcbasis_size(V_xi_y,1));
y_ext_i_alpha(:, ind_y)=y_i_alpha;

% The covariance matrix C_xiy
C_xi_y=gpc_covariance(xi_ext_i_alpha, V_xi_y, y_ext_i_alpha);
end