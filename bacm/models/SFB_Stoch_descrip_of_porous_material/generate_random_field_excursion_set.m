clear
clf

%% optional settings

correlation_type={'gaussian', 'exponential', 'matern', 'spec_func'};
correlation_type=correlation_type{2};
% for matern coveriance
nu=1;
% for first three
L_c=0.1;
sigma=1;

% params for the spe func (damped oscillatory correlation function)
% Amplitude
    A = 1.0040;
% damping coeff
    alpha = 0.0147;
% frequency
    omega =0.0157;
% phase shift
    phi = -11.1833;
n_refine=3;
%% setup mesh
%generate_random_field_excursion_set.m
SGLIBpath= '/home/noefried/sglib';
addpath(fullfile(SGLIBpath, 'fast_fem_assembly'))

addpath(fullfile(SGLIBpath, 'fast_fem_assembly'))
addpath(fullfile(SGLIBpath, 'fast_fem_assembly', 'fast_fem_assembly'))
addpath(fullfile(SGLIBpath, 'fast_fem_assembly', 'fast_fem_assembly', 'library_vectorization'))

[coord,elem_nodes]=my_cubic_mesh(n_refine);
%coord=coord*1000;
%show_mesh(elem_nodes, coord)

%% Field properties

switch correlation_type
    case 'gaussian'
        cov_func=@(d)( reshape(gaussian_covariance( d(:)', [], L_c, sigma), size(d) ) );
    case 'exponential'
        cov_func=@(d)( reshape(exponential_covariance( d(:)', [], L_c, sigma), size(d) ) );
    case 'matern'
        cov_func=@(d)( reshape(matern_covariance(nu, d(:)', [], L_c, sigma), size(d) ) );
    case 'spec_func'
        cov_func=@(d)( reshape(spec_cov_func( d(:)', A, alpha, omega, phi), size(d) ) );
end
%% Represent a Gaussian random field
D=dist(coord');
%D=sqrt(coord*coord');
C = cov_func(D);
N = size(coord, 1);

% Compute the KL expansion
L = min(1000, N-1);
G = my_mass_matrix(coord, elem_nodes);
[r_i_k, lambda_k] = kl_solve_evp(C, G, L);
sigma_k=sqrt(lambda_k);
display(strvarexpand('The represented varince is $sum(sigma_k.^2)*100$%'));

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);

% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

% Convert to compact form and include mean
% [u2_i_k, theta2_k_alpha] = kl_pce_to_compact_form(sin(pi*x'), r_i_k, sigma_k, theta_k_alpha);

% The conversion made explicit
% a) Multiply in the sigmas 
u_i_k = binfun(@times, r_i_k, sigma_k);

% b) Put the mean inside
u_mean=zeros(N, 1);
u_i_k = [u_mean, u_i_k];
theta_k_alpha = [1, zeros(1,m); theta_k_alpha];


% Generate some normally distributed random numbers for plotting some
% realisations of the random field
xi = randn(m, 1);
clf

r=kl_pce_field_realization(u_i_k, theta_k_alpha, I, xi);
subplot(2, 1, 1)
plot_3d_surface(elem_nodes, coord, r)

%Level set
kappa=0.01;
rb=r;
excur_ind=rb>kappa;
rb(~excur_ind)=0;
rb(excur_ind)=1;
% plot cut part
subplot(2,1, 2)
plot_3d_surface(elem_nodes, coord, rb)
[X, Y, Z, R, reshape_func]=sort_structured_mesh_coord_to_meshgrid(coord, r);
figure
p = patch(isosurface(X,Y,Z,R,kappa));
isonormals(X,Y,Z,R,p)
p.FaceColor = [0.6,0.6,0.6];
p.EdgeColor = 'none';
daspect([1,1,1])
view(3); axis tight
camlight 
lighting gouraud

%% transform field
r=r.^3;
subplot(2, 1, 1)
plot_3d_surface(elem_nodes, coord, r)

%Level set
kappa=0.002;
rb=r;
excur_ind=rb>kappa;
rb(~excur_ind)=0;
rb(excur_ind)=1;
% plot cut part
subplot(2,1, 2)
plot_3d_surface(elem_nodes, coord, rb)
[X, Y, Z, R, reshape_func]=sort_structured_mesh_coord_to_meshgrid(coord, r);
figure
p = patch(isosurface(X,Y,Z,R,kappa));
isonormals(X,Y,Z,R,p)
p.FaceColor = [0.6,0.6,0.6];
p.EdgeColor = 'none';
daspect([1,1,1])
view(3); axis tight
camlight 
lighting gouraud


