function truss_2D_dyn
%% FEM of truss model
%Define forces
F_1=-1;
F_2=-2;
%F_3H=1;
%F_2H=1;
%Number of element
N=10;

%Geometrical and material properties of horizontal and vertical bars   
L_1  = 1;    %(1 m)
rho_1 = 8e3;  %(8000 kg/m^3)
E_1  = 40e6; %(40 GPa)
A_1  = 1e-4; %(1 cm^2)

%Geometrical and material properties of diagonal bars   
L_2  = sqrt(2);  
rho_2 = 2e3; %(8000 kg/m^3)
E_2  = 10e6;%(40 GPa)
A_2  = 4e-6; %(4 mm^2)

%coord (x,y)
coord=[1,1; 2,1;2,0; 1,0; 0,0; 0,1;];

% BARS matrix for compilation
A_elem=[A_1*ones(1,6), A_2*ones(1,4)];
E_elem=[E_1*ones(1,6), E_2*ones(1,4)];
nods_el=[1,6; 1,2;4,5;3,4;1,4;2,3; 4,6;1,5;1,3;2,4];

% this should be calculated from NODS_EL and COORD
angle_el=[pi,0,pi,pi, -pi/2, -pi/2, 3*pi/4, 5/4*pi, -pi/4, 5/4*pi];
L_elem=[L_1*ones(1,6), L_2*ones(1,4)];
rho_elem=[rho_1*ones(1,6), rho_2*ones(1,4)];

% Drow truss
plot_truss(coord, nods_el, 'b', 6)
numDofs = 12;

%%
K = zeros(numDofs,numDofs);
M = zeros(numDofs,numDofs);
for j=1:10
    % Construct stiffness and mass matrices for bar, j.
    kelem = element_stiffness(A_elem(j), E_elem(j), L_elem(j), angle_el(j));
    melem = element_mass_matrix(A_elem(j),  L_elem(j), rho_elem(j), angle_el(j));
    n1 = nods_el(j,1); n2 = nods_el(j,2);
    % Arrange indices.
    ix = [n1*2-1,2*n1,n2*2-1,n2*2];
    % Embed local elements into system matrices.
    K(ix,ix) = K(ix,ix) + kelem;
    M(ix,ix) = M(ix,ix) + melem;
end
% Define Load vector
F = zeros(size(K,1),1);
F(3*2)=F_2;
F(4*2)=F_1;
%F(3)=F_2H;
%F(5)=F_3H;

% Cancel rows an columns corresponding to the B.Cs

fixedDofs = [5*2-1, 5*2, 6*2-1, 6*2]; % DoFs to eliminate
K(fixedDofs,:) = [];
K(:,fixedDofs) = [];
M(fixedDofs,:) = [];
M(:,fixedDofs) = [];
F(fixedDofs)=[];
ind=setdiff(1:12, fixedDofs);

%Statik soluion
U=K\F;
[new_coord, delta_coord]=coord_from_du(ind, U, coord);

%Drow displaced bars
plot_truss(new_coord, nods_el, 'r', 6)
%check inner forces
f=zeros(4,size(nods_el,1));
f_el=zeros(1,size(nods_el,1));
for j=1:size(nods_el,1)
   kelem = element_stiffness(A_elem(j), E_elem(j), L_elem(j), angle_el(j));
   du_elem=[delta_coord(nods_el(j,1),:),delta_coord(nods_el(j,2),:)]';
   f(:,j)=kelem*du_elem;
   f_local=local_force(f(:,j), angle_el(j));
   f_el(j)=f_local(2);
end
%% Modal analysis
% The generalized eigenvalue problem
[V,D]=eig(K,M);
normalized_V = V/sqrt(repmat(diag(V'*M*V),1,size(V,2))');
%plot eigenmodes
plot_eigenmodes(coord, normalized_V, nods_el, ind, 'g', 6)
%%

end

function Ke=element_stiffness(A, E, L, a)
% Element stiffness matrix
K_e=A*E/L*[1 -1;-1 1];
T=transformation_matrix(a);
Ke=T'*K_e*T;
end

function Me=element_mass_matrix(A, L, rho, a)
% Mass matrix
M_e = A*rho*L/6*[2 1;1 2];
T=transformation_matrix(a);
Me = T'*M_e*T;
end

function F_el=local_force(F,a)
T=transformation_matrix(a);
F_el=T*F;
end

function T=transformation_matrix(a)
T = [cos(a) sin(a) 0 0; 0 0 cos(a) sin(a)];
end

function plot_truss(coord, nods_el, color, n_thick_el)
    for i=1:length(nods_el) % for every element
        x=[coord(nods_el(i,1),1),coord(nods_el(i,2),1)].'; % <- note: transpose is important!
        y=[coord(nods_el(i,1),2),coord(nods_el(i,2),2)].';
        if i>n_thick_el
             line(x,y,'Color',  color, 'linewidth',2);
        else
            line(x,y,'Color', color, 'linewidth',4);
        end
    end
end
function plot_eigenmodes(coord, V, nods_el, ind, color, n_thick_el)
    figure
    multiplot_init(size(V,1),1)
    for i=1:size(V,1)
        multiplot
        new_coord=coord_from_du(ind, V(:,i), coord);
        %plot_truss(coord, nods_el, 'b', n_thick_el)
        plot_truss(new_coord, nods_el, color, n_thick_el)
    end
end

function [new_coord, delta_coord]=coord_from_du(ind, U, coord)
    u=zeros(12,1);
    u(ind)=U;
    delta_coord=[u(1:2:length(u)), u(2:2:length(u))];
    new_coord=coord+delta_coord;
end
