function [f, omega, Ampl, phase_shift]=sample_from_PSD(A, B, N, t)
% The SMAPLE_FROM_PSD function generates excitation from the sum of N frequencies
% in for the time domain T
% from the Pierson-Moskowitz spectrum with parameters A and B:
%
% S(\omega)=A/\omega^5 *exp(-B/\omega^4)
% Example:
% A=50*10^9;
% B=2*10^6;
% N=10;
% t=0:0.1:10;
% Noemi Friedman
%
[omega, Ampl, phase_shift]=sample(A, B, N);
f_of_t=Ampl*cos(omega*t-repmat(phase_shift,1, length(t)));
f=sum(f_of_t,1)



    function [omega, Ampl, phase_shift]=sample(A, B, N)
%omega=1:120;
phi=SimParameter('phi_1', UniformDistribution(0, 2*pi));
phase_shift=phi.sample(N);
    
omega=invcdf(rand(N,1), A, B);
sigma=sqrt(A/(4*B));
Ampl=sqrt(2/N)*sigma;
    end

end
function s=psd(omega, A, B)
s=A./omega.^5.*exp(-B./omega.^4);
end

function x=cdf(omega, A, B)
%x=A/(4*B)*exp(-B*omega.^(-4));
x=exp(-B*omega.^(-4));
end

function omega=invcdf(x, A, B)
y=-1/B*log(x);
omega=1./nthroot(y,4);
end
