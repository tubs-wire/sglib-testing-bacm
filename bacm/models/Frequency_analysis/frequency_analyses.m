function [f_signal, abs_signal] = frequency_analyses(data_vector, discard_samples, sampling_period, frequency_resolution, plot_till_freq)
% data_vector contains the samples of the signal to be analyzed
% discard_samples is the number of samples which is discarded at the
% beginning of the signal
% sampling_period is the sampling period of the signal (data_vector)
% frequency_resolution is the desired frequency resolution

if discard_samples > 0
    data_vector(1:discard_samples) =[];
end

fft_length = (1/sampling_period)/frequency_resolution;

data_temp = zeros(fft_length,1);
data_temp(1:length(data_vector)) = data_vector;

frequency_axis = (0:frequency_resolution:(1/sampling_period)-1);

fft_temp = fft(data_temp(1:fft_length),fft_length)/fft_length;
FFT_abs = abs(fft_temp(1:fft_length/2+1));
freqs=    frequency_axis(1:fft_length/2+1);

ind_freq=frequency_axis<1;

%figure
f_signal=freqs(ind_freq);
abs_signal=FFT_abs(ind_freq);

end