% Frequency analysis of eigenmotions
clear variables
% Load vectors Alpha, Time, V_TAS
load('Signal_for_eigenmotions_long.mat'); 
Alpha = alpha;

sampling_period = 0.001;
discard_samples = 0;
frequency_resolution = 0.01;

frequency_analyses(V_TAS, 0, sampling_period, 0.01);
frequency_analyses(Alpha, 0, sampling_period, 0.01);