
Q=gpcbasis_size(V_u,2);
n_u=size(u_i_alpha,1);
time=state.t_span;

ylabels={{'a_1'}; {'a_2'}; {'a_3'}, ; {'a_4'}};

%% get rid of NaN values
ind_max=length(time);
for i=1:n_u
    ind_m=[ find(isnan(u_mean(:,i))); find(u_mean(:,i)==inf)];
    ind_v=[find(isnan(u_var(:,i)))  ;find(u_var(:,i)==inf); find(abs(sqrt(u_var(:,i))./u_mean(:,i))>10^6)];
    if ~isempty(ind_m)
        ind_m=ind_m-1;
    end
    if ~isempty(ind_v)
        ind_v=ind_v-1;
    end
    ind_max=min([ind_m; ind_v; ind_max]);
end

if ~(ind_max==length(time))
    time=time(1:ind_max);
    n_t=length(time);
    u_mean=u_mean(1:ind_max, :);
    u_var=u_var(1:ind_max, :);
    u_i_alpha=u_i_alpha(:,:,1:ind_max);
    warning('SGLIB:plot_u_mean_var', 'NaN values have been removed from u_mean and u_var in the plot')

end

%% get plot for quantiles
% Prelocate memory
u_quant_up=zeros(n_t,n_u);
u_quant_low=zeros(n_t,n_u);
for i_t=1:n_t
    u_quant=gpc_quantiles(V_u, (squeeze(u_i_alpha(:,:,i_t))), 'quantiles', [0.01, 0.99]);
    u_quant_up(i_t,:)=u_quant(1,:);
    u_quant_low(i_t,:)=u_quant(2,:);
end
plot_u_mean_quant(time, u_mean, u_quant_low, u_quant_up,  'fill_color','b', 'line_color','black','transparency', 0.4,  'ylabels', ylabels, 'y_lim', [-1,1])

%% Plot ref modes (from orthogonal proj)

Ref_mod=load('nut_AM3');
var.Ref_time=Ref_mod.time;
var.Ref_modes=Ref_mod.a(5:8,:);

hold on
for i=1:n_u
subplot(n_u,1,i)
hold on
plot(var.Ref_time, var.Ref_modes(i,:), 'b', 'LineWidth', 1)
%xlim([0,56])
end
%% get sobol sensitivities and variances in seperate cells
% Prelocate memory
part_var_c=cell(n_u,1);
sobol_index=cell(n_u,1);

for i=1:n_u
part_var_c{i,1}=zeros(length(time),Q);
sobol_index{i,1}=zeros(length(time),Q);
end
% 
for i=1:length(time)
    [part_var, ~, ratios]=gpc_sobol_partial_vars(V_u, u_i_alpha(:,:,i), 'max_index', 1);
    for i_u=1:4
        part_var_c{i_u}(i,:)=(part_var(:,i_u))';
        sobol_index{i_u}(i,:)=(ratios{1}(:,i_u))';
    end
end

%Plot all sobol sensitivities in subplots with the different responses
for i_u=1:n_u
    subplot(n_u,1,i_u)
    plot(time, sobol_index{i_u}, 'LineWidth', 2)
    ylim([0, 1.1])
    ylabel(strcat('Sensitivities of a', num2str(i_u)), 'FontSize', 14)
    xlabel('Time', 'FontSize', 14)
    if i_u==1
            h_legend=legend('a_{01}', 'a_{02}', 'a_{03}', 'a_{04}');
            set(h_legend,'FontSize',14);
            title('Sensitivities to uncertainties of initial conditions', 'FontSize', 20)
    end
end

%% Plot only the partial variances/sensitivities for germs with high sobol
%% sensitivity
% Get index of germs with more then 0.3 sensitivity
index = cellfun(@(x) max(x)>0.01, sobol_index, 'UniformOutput', 0);
%Plot sobol sensitivities in subplots with the different responses
for i_u=1:n_u
    subplot(n_u,1,i_u)
    plot(time, sobol_index{i_u}(:,index{i_u}), 'LineWidth', 2)
    ylim([0, 1.1])
    ylabel(strcat('a', num2str(i_u)), 'FontSize', 14)
    xlabel('Time', 'FontSize', 14)
    h_legend=legend(state.list_of_RVs(index{i_u}));
    set(h_legend,'FontSize',14);
end

%Plot partial variances/max mean and tot_var/max mean in subplots with the different responses
for i_u=1:4
    subplot(4,1,i_u)
    plot(time, part_var_c{i_u}(:,index{i_u})/max(u_mean(:,i_u)), 'LineWidth', 2)
    hold on
    plot(time, u_var(:,i_u)/max(u_mean(:,i_u)), 'LineWidth', 1)
    ylim([0, 1.1])
    ylabel(strcat('Coeff. of var. of a', num2str(i_u)), 'FontSize', 14)
    xlabel('Time', 'FontSize', 14)
    
    h_legend=legend(  [state.list_of_RVs(index{i_u}), 'Tot var'] );
    set(h_legend,'FontSize',14);
     if i_u==1
        title('Coefficient of variations due to uncertainties of initial conditions',  'FontSize', 20)
    end
end

%% Plot partial variances and tot_var  in subplots with the different responses
for i_u=1:4
    subplot(4,1,i_u)
    plot(time, part_var_c{i_u}(:,index{i_u}), 'LineWidth', 2)
    hold on
    plot(time, u_var(:,i_u), 'LineWidth', 1)
    %ylim([0, 1.1])
    xlim([0, 50])
    ylabel(strcat('Coeff. of var. of a', num2str(i_u)), 'FontSize', 14)
    xlabel('Time', 'FontSize', 14)
    
    h_legend=legend(  [state.list_of_RVs(index{i_u}), 'Tot var'] );
    set(h_legend,'FontSize',14);
     if i_u==1
        title('Coefficient of variations due to uncertainties of initial conditions',  'FontSize', 20)
    end
end

% figure
% for i_u=1:4
%     subplot(4,1,i_u)
%     plot(time, u_var(:,i_u)/max(u_mean(:, i_u))*100, 'LineWidth', 1)
%     %plot(time, u_var(:,i_u)./u_mean(:, i_u)*100, 'LineWidth', 1)
%     %ylim([0, 1.1])
%     ylabel(strcat('Coeff. for a', num2str(i_u), ' [%]'), 'FontSize', 14)
%     xlabel('Time', 'FontSize', 14)
%     %ylim([0,100]);
%     %h_legend=legend('a_{1}', 'a_{2}', 'a_{3}', 'a_{4}');
%     %set(h_legend,'FontSize',14);
%     if i_u==1
%         title('Coefficient of variations due to uncertainties of initial conditions',  'FontSize', 20)
%     end
% end