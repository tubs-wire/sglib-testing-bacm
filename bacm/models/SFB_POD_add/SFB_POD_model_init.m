function [state,polysys,time] = SFB_POD_model_init( varargin )
% SFB_POD_model
%
%
%===================================================================
% Stochastic POD (variations of coeffs)
%===================================================================
%
% POD:
% dadt(i)=Ci(i)+ sum_j Lij(i,j)*a(j) + sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j
% Bij(i,j)*c(j)
% with
% 
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -c(j): control modes
%       .- c(1): shift between controlled and not controlled phase B0
%        - c(2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(3): + oscillatory part with phase shift (B1*sin(\omega_a t)
% POD coefficients:
%
% coeffs of the linear term:    Lij : dissipative term
% coeffs of the quadratic term: Qijk :convective term
% coeffs of the actuation:      Bij  :control term
% initial condition:            a_0  :first coeffs of the POD basis
%                                       functions (\psi=\sum_k b_kl v_k(x) - does v_0 exists?)
%
%

% Example:
%    state = SFB_POD_model_init('list_of_RVs',{'L','Q','a_0'} );



%===================================================================
% Definition of parameter values (if not random, mean value is used)
%===================================================================
% Define parameter values (mean, variance, or left and right values and
% distribution for parameters of the prey-predator equation
% (if parameter is not random,variance and distribution are ignored, and
% mean value is used for deterministic parameter.

% Possible distrubutions:
% -'H': normal /input form: {mean,variance,'H'}  Hermite (normalised)
% -'G':lognormal /input form: {mean,variance,'G'} Hermite (normalised)
% -'P': uniform /input form: {left_value,right_value,'P'} Legendre (normalised)
% -'L': exponential /input form: {multipl,'lambda','L'}   Laguerre (both are
% automatically normalised) - only multipl*exp(-x) is working now but
% multipl*lambda*exp(-lambda*x)should be also implemented
% -'T': arcsin distrib /input form: {shift,multipl,'T'} (shift=0-> [-1,1]) Chebyshev 1st kind (both normalised)
% -'U': semicircle distribution/input form: {shift,multipl,'U'} Chebyshev 2nd kind (both normalised)

%
%dentity matrix for measurement - not needed here
%and identity matrix times sigma for B^-1 (for initial cond) and C^-1 (for
%coeffs)
% sigma=0.1

options = varargin2options(varargin);
%[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'dL', 'dQ', 'da_0'});
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'L', 'Q', 'a0'});
%[t_min_max, options] = get_option(options, 't_min_max', [0,90]);
check_unsupported_options(options, mfilename);

%=========================================================================
% defining internal state
%=========================================================================
% storing non params and problem description in STATE 

state = struct();
long_list_of_RVs={};
load('coeff_4D_Quad_3_act');

%% Read coefficients
a0=[-0.42779711595446163; -4.9748974032828318*10^(-2); -1.0883596966771526*10^(-2); -9.1617981596987027*10^(-3)];
% reshape matrices in one column of independent values
L=coeff.L_ij;
Q=coeff.Q_ijk;

%% Input Qijk
% sort unique values to force symmetries
[uniq_Q, indq1, indq2]=unique(Q); %uniq_Q=Q(indq1) and Q=reshape(uniq_Q(indq2),4,4,4)
%Create parameter names
Q_ind_text=create_param_names('Q_', indq1);
% Initiate uniform independent variables with \pm 5% deviation
for i=1: length(Q_ind_text)
           params.(Q_ind_text{i}) ={ 0, 0.02, 'H' };
end
if ~isempty(  strmatch('Q', list_of_RVs) )
    long_list_of_RVs= [long_list_of_RVs; Q_ind_text];
end

%% Input Lij
L=reshape(coeff.L_ij, [],1);
%Create parameter names
ind_L=1:length(L);
L_ind_text=create_param_names('L_', ind_L');
% Initiate uniform independent variables with \pm 5% deviation
for i=1: length(L_ind_text)
           params.(L_ind_text{i}) ={ 0, 0.02, 'H'  };
    end
if ~isempty(  strmatch('L', list_of_RVs) )
    long_list_of_RVs= [long_list_of_RVs; L_ind_text];
end
%% Input ai
%Create parameter names
ind_a=1:length(a0);
a_ind_text=create_param_names('a0_', ind_a');
% Initiate uniform independent variables with \pm 1% deviation
for i=1: length(a_ind_text)
           params.(a_ind_text{i}) ={ 0, 0.02, 'H'  };
 end
if ~isempty(  strmatch('a0', list_of_RVs) )
    long_list_of_RVs= [long_list_of_RVs; a_ind_text];
end
%%
list_of_params=fieldnames(params);
list_of_init_vals=a_ind_text;
num_tot_params=length(list_of_params);

%%
%specify properties of random and not random parameters in state class
[state, polysys] = spec_init_parameters(state, params, num_tot_params, list_of_params, long_list_of_RVs); 


%% Run reference(deterministic) solution and get tspan vector for stepbystep
%integration

%[ref_POD_modes,t_span] = SFB_POD_ODE(ref_params, state);

state.indq1=indq1;
state.indq2=indq2;
state.param_C=coeff.C_i;
state.param_B=coeff.B_ij;
state.param_a0=a0;
state.param_L=coeff.L_ij;
state.param_Q=coeff.Q_ijk;
tic
[t_span, ref_POD_modes] = SFB_POD_model_solve(state, [], 'stoch_flag', 0);
toc

problem_size=length(t_span);
%dt_span=diff(t_span);

state.list_of_init_vals=list_of_init_vals;
state.list_of_sol_vars={'a_1'; 'a_2'; 'a_3'; 'a_4' };
state.ref_sol=ref_POD_modes;
state.t_span=t_span;
state.list_of_params=list_of_params;
state.num_vars=problem_size;
state.num_eqs=4;
state.num_totRV_out= state.num_eqs(1)*problem_size;

time=t_span;
end

%Create names for parameters
function params_name=create_param_names(name, index)
%Check whether index is  (so that the parameter nemes will also be) unique.
if ~length(unique(index))==length(index)
    error('SGLIB: init func : indexing doesn t lead to unique parameter names')
end
ind_cell=num2cell(index);
ind_text=cellfun(@num2str, ind_cell, 'UniformOutput', false);
param_text=mat2cell(  repmat(  name, length(ind_text), 1), ones(length(ind_text),1), length(name));
params_name=cellfun(@strcat, param_text, ind_text,  'UniformOutput', false);
end
