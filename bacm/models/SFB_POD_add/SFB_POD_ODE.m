function [POD_modes,t_span] = SFB_POD_ODE(a0, L, Q, C, B, varargin)
% POD:
% dadt(i)=Ci(i)+ sum_j Lij(i,j)*a(j) + sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j
% Bij(i,j)*c(j)
% with
%
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -c(j): control modes
%       .- c(-1): shift between controlled and not controlled phase B0
%        - c(-2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(-3): + oscillatory part with phase shift (B1*sin(\omega_a t)
%           (for the control modes the reference modes are used, which can
%           be changed by setting the 'USE_REF_MODES' option 'FALSE' 
% POD coefficients
%
% coeffs of the linear term:    Lij : dissipative term
% coeffs of the quadratic term: Qijk :convective term
% coeffs of the actuation:      Bij  :control term
% initial condition:            a_0  :first coeffs of the POD basis
%

options = varargin2options(varargin);
[t_span, options] = get_option(options, 't_span',[]);
[ref_modes, options]=get_option(options, 'ref_modes','Semaan'); %flag whether the given referenc POD modes or the ones given in the paper should be used
[implicit_flag, options]=get_option(options, 'implicit_flag',0);
[plot_flag, options]=get_option(options, 'plot_flag',1);
check_unsupported_options(options, mfilename);
%tmin=0
%tmax=87.260999999999996; %deterministic model should run till here
%tmax=50 %this is how far the ODE solver can solve it so far

var.L=L;
var.Q=Q;
var.C=C;
var.B=B;


%% Control parameters
if strcmpi(ref_modes, 'Semaan')
    Ref_mod=load('nut_AM3');
    var.Ref_time=Ref_mod.time;
    var.Ref_contr_modes=Ref_mod.a(1:3,:);
    %zeroing out the last nonzero entry of the control term (so it does not
    %blows up):
    var.Ref_contr_modes(3,646)=0;
    
    var.Ref_modes=Ref_mod.a(5:8,:);
    clear 'Ref_mod';
        ind_t=1:700;                        % Index set to a value till when it is not blowing up
        t_span=var.Ref_time(ind_t); %Overwrite t_span, SHOULD BE CHANGED
    t_span=var.Ref_time;
    
    %The following sin and cosine modes should not be used, because here the
    %reference control modes have to be used (but kept it here)
elseif strcmpi(ref_modes, 'Gilles')
    Ref_mod=load('nut_AM3');
    var.Ref_modes=Ref_mod.a(5:8,:);
    clear 'Ref_mod';
    load('Ref_modes_Gilles.mat');
    var.Ref_contr_modes=[Ref.cmode3, Ref.cmode2, Ref.cmode1]';
    t_span=Ref.time;
    var.Ref_time=t_span;
    var.Ref_contr_modes(3,645)=0;
    clear 'Ref';
else
    normalization_param=0.489/(0.58/(2*pi))/0.3;  %c/U_inf from the paper
    var.contr_par.omega=0.58*normalization_param; %frequency of oscillatory control modes (a-2,a-3)
    var.contr_par.B1 = 4.515; %non oscillatory part of control(a-1)
    var.contr_par.B2 = 0.07;  %amplitude of the oscillation
    var.contr_par.tc1=7.395;  %time when actuation begins
    var.contr_par.tc2=43.843; %time when actuation ends
end

%% Calculate initial tangent for implicit integration rule
if implicit_flag
    dadt= (var.Ref_modes(:,2)-var.Ref_modes(:,1))/(t_span(2)-t_span(1));
end
%% Solve ODE

% Following line is never computed, for that the t_span definition needs to
% be removed
if isempty(t_span)
    options=odeset('Refine', 1, 'MaxStep', (tmax-tmin)/2000);
    [t_span,POD_modes]=ode45(@(t,POD_modes) solve_SFB_POD(t,POD_modes,var, ref_modes),[tmin tmax],a0, options);
else
    if implicit_flag
        % with implicit solver:
        POD_modes=ode15i(@(t,POD_modes, dadt) solve_SFB_POD_impl(t, POD_modes, dadt, var), t_span, a0, dadt);
    else
        % with explicit fixed timestep solver:
        POD_modes=ode4(@(t,POD_modes) solve_SFB_POD(t,POD_modes,var, ref_modes),t_span,a0);
        % with explicit adaptive  timestep solver:
        %[t_span,POD_modes]=ode45(@(t,POD_modes) solve_SFB_POD(t,POD_modes,var, use_ref_modes_flag),t_span,a0);
    end
end

if implicit_flag
    t_span=(POD_modes.x)';
    POD_modes=(POD_modes.y)';
end

%% Plot modes if needed
if plot_flag
    %Plot reference POD modes
    for i=1:4
        subplot(4,1,i)
        plot(var.Ref_time, var.Ref_modes(i,:), 'b')
        if i==1||i==2
            ylim([-0.6,0.6])
        elseif i==3
            ylim([-1,0.2])
        else
            ylim([-0.6,1])
        end
    end
    
    % Plot ROM modes
    for i=1:4
        subplot(4,1,i)
        hold on
        plot(t_span,POD_modes(:,i), 'r')
        if i==1||i==2
            ylim([-0.6,0.6])
        elseif i==3
            ylim([-1,0.2])
        else
            ylim([-0.6,1])
        end
    end
end
end % end of SFB_POD_ODE

%% Function for the explicit solver
function POD_modes = solve_SFB_POD(t, a, var, ref_modes)
    %POD_modes= var.C + var.L*a + reshape(reshape(var.Q,n*n,n)*a,n,n)*a + var.B*[0.0295;0.0239*cos(0.489*t);0.0239*sin(0.489*t)];
    if ~isempty(ref_modes)
        POD_modes= var.C + var.L*a +   [var.Q(:,:,1)*a,var.Q(:,:,2)*a,var.Q(:,:,3)*a,var.Q(:,:,4)*a]*a + var.B*get_ref_control_modes(t, var.Ref_time, var.Ref_contr_modes);
        %POD_modes= var.C + var.L*a + [var.Q(:,:,1)*a,var.Q(:,:,2)*a,var.Q(:,:,3)*a,var.Q(:,:,4)*a]*a + var.B*(var.Ref_contr_modes(:,it));
    else
        POD_modes= var.C + var.L*a + [var.Q(:,:,1)*a,var.Q(:,:,2)*a,var.Q(:,:,3)*a,var.Q(:,:,4)*a]*a + var.B*get_control_modes(t, var.contr_par);
    end
end

%% Function for the implicit solver
function POD = solve_SFB_POD_impl(t, a, dadt, var)
%POD_modes= var.C + var.L*a + reshape(reshape(var.Q,n*n,n)*a,n,n)*a + var.B*[0.0295;0.0239*cos(0.489*t);0.0239*sin(0.489*t)];
POD=0;
POD=-dadt+ var.C + var.L*a +   [var.Q(:,:,1)*a,var.Q(:,:,2)*a,var.Q(:,:,3)*a,var.Q(:,:,4)*a]*a + var.B*get_ref_control_modes(t, var.Ref_time, var.Ref_contr_modes);
end

%% Get control modes with linear interpolation of reference control modes 
function control_modes = get_ref_control_modes(t, Ref_time, Ref_contr_modes)
    a_min1=interp1(Ref_time,Ref_contr_modes(3,:), t);
    a_min2=interp1(Ref_time,Ref_contr_modes(2,:), t);
    a_min3=interp1(Ref_time,Ref_contr_modes(1,:), t);
    control_modes=[a_min3;a_min2; a_min1];
    %control_modes
end

%% These are the controle modes with sin and cosine, but should not be used
function control_modes = get_control_modes(t, contr_par)
    tc1=contr_par.tc1;
    tc2=contr_par.tc2;
    B1=contr_par.B1;
    B2=contr_par.B2;
    omega=contr_par.omega;
    control_modes=zeros(3,length(t));
    for i=1:length(t)
        if t(i)>tc1 && t(i)<tc2
            control_modes(:,i)=[B2*sin(omega*t(i)); B2*cos(omega*t(i)); B1];
        else
            control_modes(:,i)=[0; 0; 0];
        end
    end
end



