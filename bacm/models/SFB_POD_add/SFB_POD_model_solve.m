function varargout = SFB_POD_model_solve(state,sample,varargin)
% Calls the SFB_POD integrator
%

 options = varargin2options(varargin);
 [stoch_flag, options]=get_option(options, 'stoch_flag',1); % flag whether coefficients should be deterministic or stochastic(perturbed)
 check_unsupported_options(options, mfilename);

vector_names={'a0', 'Q', 'L'};
indq1=state.indq1;
indq2=state.indq2;

ind_a=1:4;
ind_L=1:16;
ind_params={ind_a, indq1, ind_L};

if stoch_flag
    %scale and shift from standard deviation and move to state.actual params
    state=scale_and_update_vars(sample, state);
    var=compile_to_vector(state.actual_params, vector_names, ind_params);
else
    var=compile_to_vector(state.ref_params, vector_names, ind_params);
end

C=state.param_C;
B=state.param_B;
a0=var.a0+state.param_a0;
L=reshape(var.L,4,4)+state.param_L;
Q=reshape(var.Q(indq2), 4,4,4)+state.param_Q;   
    
%Solve ODE with fixed timesteps:
[POD_modes,Time] =SFB_POD_ODE(a0, L, Q, C, B);

modes_out=reshape(POD_modes,[],1);
if nargout>1
    varargout{1}=Time;
    varargout{2}=modes_out;
else
    varargout= {modes_out};
end
