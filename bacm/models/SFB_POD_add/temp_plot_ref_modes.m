
Ref_mod=load('nut_AM3');
var.Ref_time=Ref_mod.time;
var.Ref_modes=Ref_mod.a(5:8,:);

figure(1)
hold on
for i=1:4
    
subplot(4,1,i)
hold on

plot(var.Ref_time, var.Ref_modes(i,:), 'b', 'LineWidth', 1)
xlim([0,47])
end