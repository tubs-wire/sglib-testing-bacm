function [u_mean, u_var, time, varargout] = compute_response_surface_projection_s(init_func, solve_func, V_u, p_int, p_u, init_options, varargin)
% COMPUTE_RESPONSE_SURFACE_PROJECTION Compute a gpc response surface representation.
%   COMPUTE_RESPONSE_SURFACE_PROJECTION Long description of compute_response_surf_projection.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example compute_response_surface_projection">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


options=varargin2options(varargin);
[grid,options]=get_option(options, 'grid', 'smolyak');
[gpc_full_tensor, options]=get_option(options, 'gpc_full_tensor', false);
[saving_path,options]=get_option(options, 'saving_path', []);
check_unsupported_options(options, mfilename);


[state, polysys, time] = funcall(init_func, init_options);


if isempty(V_u)
    V_u = gpcbasis_create(polysys, 'm', state.num_params, 'p', p_u, 'full_tensor', gpc_full_tensor);
    % V_u = gpcbasis_create(polysys, 'm', state.num_params, 'p', p_u);

end

[x,w] = gpc_integrate([], V_u, p_int, 'grid', grid);
M = gpcbasis_size(V_u, 1);
Q = length(w);
u_i_alpha = zeros(state.num_totRV_out, M);


for j = 1:Q
    disp(strcat(num2str(j), '/', num2str(Q)));
    p_j = x(:, j);
    u_i_j = funcall(solve_func, state, p_j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_u, p_j, 'dual', true);
    u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
end


[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
%put different variables in u_mean in different columns
if ~isscalar(state.num_eqs)
    u_mean_nontemp=u_mean( end-state.num_eqs(2)+1:end);
    u_var_nontemp=u_var( end-state.num_eqs(2)+1:end);
         
    u_mean=u_mean(1:end-state.num_eqs(2));
    u_var=u_var(1:end-state.num_eqs(2));
    
    u_i_alpha_nontemp=u_i_alpha(end-state.num_eqs(2)+1: end,:);
    u_i_alpha=u_i_alpha(1:end-state.num_eqs(2),:);
end
 u_mean=reshape(u_mean, state.num_vars,state.num_eqs(1));
 u_var=reshape(u_var, state.num_vars,state.num_eqs(1));
 
u_i_alpha_ieq=zeros(state.num_eqs(1), M, state.num_vars);
 for i=1:state.num_eqs(1)
     kk=(i-1)*state.num_vars+1;
     ll=i*state.num_vars;
     u_i_alpha_ieq(i,:,:)=(u_i_alpha(kk:ll,:))';
 end
  
 u_i_alpha=u_i_alpha_ieq;
%u_i_alpha=mat2cell(u_i_alpha,ones(1,state.num_eqs),[M],[state.num_vars]);
%u_i_alpha=( shiftdim(cat(3,u_i_alpha{:}),1) )';
if nargout>0
 varargout{1}.u_i_alpha=u_i_alpha;
varargout{1}.V_u=V_u;
if ~isscalar(state.num_eqs);
    varargout{1}.u_i_alpha_nontemp=u_i_alpha_nontemp;
    varargout{1}.u_mean_nontemp=u_mean_nontemp;
    varargout{1}.u_var_nontemp=u_var_nontemp;
end
end

 %% save results if saving_path is given
 if ~(isempty(saving_path))
     %definition of file- and foldername for saving results
        equation_name=strrep(char(init_func),'init','_');
        file_name=get_saving_filenames(saving_path,equation_name,'PROJ',{p_u, p_int, grid});
       if isscalar(state.num_eqs);
            save(file_name,'u_mean','u_var','p_int', 'p_u', 'polysys','u_i_alpha', 'init_options', 'state', 'V_u');
       else
           save(file_name,'u_mean','u_var','u_mean_nontemp', 'u_var_nontemp', 'p_int', 'p_u', 'polysys','u_i_alpha', 'u_i_alpha_nontemp', 'init_options', 'state', 'V_u');
       end
end