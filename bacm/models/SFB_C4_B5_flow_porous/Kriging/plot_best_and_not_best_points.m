function krig_model=plot_best_and_not_best_points()%(V_u, u_i_alpha, RVs)
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x0, u0]=main_UQ(5, 'point_type', 'scan_point', 'prior_dist','beta');
  [x_b,u_b]=load_points_and_solution('point_type', 'best_point');
[x,u]=load_points_and_solution('point_type', 'best_point_from_proxi');
u_p=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x));
u_pb=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x_b));
u_p=reshape(u_p,360,5);
u_pb=reshape(u_pb,360,5);

%% Kriging

S=x0';

krig_model=cell(5,1);
krig_up=zeros(360,5);
krig_upb=zeros(360,5);
reg_f={@regpoly2, @regpoly0, @regpoly2,@regpoly0,@regpoly0};
corr_f={@correxp, @corrgauss, @correxp, @correxp, @correxp};
for i=1:5
    display(i)
    Y_i=(squeeze(u0(:,i,:)))';
    %krig_model{i}=dacefit(S, Y_i, reg_f{i}, corr_f{i}, 0.5*ones(6,1), 0.1*ones(6,1),2*ones(6,1));
    krig_up(:,i)=predictor(x,krig_model{i});
    krig_upb(:,i)=predictor(x,krig_model{i});
end
%% Compare B5 model, gpc, kriging
for i=1:5
    %subplot(5,1,i)
    display(i)
    figure
    subplot(2,1,1)
    plot(coord, u(:,i), 'red',  'LineWidth', 2)
    hold on
    plot(coord, u_b(:,i), 'blue',  'LineWidth', 2)
    plot(coord, u_p(:,i), 'red--')
    plot(coord, u_pb(:,i), 'blue--')
    plot(coord, krig_up(:,i), 'red:')
    plot(coord, krig_upb(:,i), 'blue:')
    
    subplot(2,1,2)
    plot(coord, abs(u_p(:,i)-u(:,i)), 'red');
    hold on
    plot(coord, abs(u_pb(:,i)-u_b(:,i)), 'blue');
    plot(coord, abs(krig_up(:,i)-u(:,i)), 'red:');
    plot(coord, abs(krig_upb(:,i)-u_b(:,i)), 'blue:');
    %plot(coord, abs( reshape(u_p_b(:,i,:), 360,7)-repmat(u_b(:,i),1,7)), '--', 'LineWidth', 2);
end

