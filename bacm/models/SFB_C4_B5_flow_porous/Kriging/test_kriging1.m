clear variables
separate_ratio=2/3;
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, xs, us, ~, ~, x1, u1]=main_UQ(5, 'point_type', 'scan_point', 'prior_dist','beta', 'separate_points',separate_ratio);
    %Y_check=(reshape(u1, [], size(u1,3)))';
    S_check=x1';
%[xs,us]=load_points_and_solution('point_type', 'scan_point');

S=xs;
%Y=reshape(us,360*5, []);
%Y=Y';
S=S';
%corrf={@corrgauss, @correxp, @correxpg, @corrlin, @corrcubic, @corrspherical, @corrspline};
corrf={@corrgauss, @correxp @corrlin};
n_c=length(corrf);
regr={@regpoly0, @regpoly1, @regpoly2};
n_r=length(regr);
%theta=linspace(0.01,2, 20);
%n_t=length(theta);
err_L2=zeros(5, n_c, n_r);
krig_models=cell(5, n_c, n_r);
perfs=cell(5,n_c, n_r);
for l=1:5
    Y_l=(squeeze(us(:,l,:)))';
    Y_check_l=(squeeze(u1(:,l,:)))';
    for k=1:n_c
        display(k)
        for j=1:n_r
            regr_j=regr{j};
            corf_k=corrf{k};
            display(j)
            %for i=1:length(theta)
                %display(i)
                %[dmodel, perf]=dacefit(S, Y_l, regr_j, corf_k, theta(i));
                [krig_models{l,k,j}, perfs{l,k,j}]=dacefit(S, Y_l, regr_j, corf_k, 0.5*ones(6,1), 0.1*ones(6,1),2*ones(6,1));
                Y_pred=predictor(S_check, krig_models{l,k,j});
                err_L2(l,k,j)=norm(Y_pred -Y_check_l);
                
            %end
        end
    end
end
%% compare with gpc error
Y_pred_gpc=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x1));
Y_pred_gpc=reshape(Y_pred_gpc, 360, 5, 1139);
err_gpc=Y_pred_gpc-u1;

for i=1:5
    err_L2_gpc=norm(squeeze(err_gpc(:,i,:)))
end

%%
%prediction at best point
for k=1:n_c
    plot(theta, squeeze(err_L2(k,1,:)))
    hold on
    plot(theta, squeeze(err_L2(k,2,:)), '--')
    plot(theta, squeeze(err_L2(k,3,:)), '-o' )
end

[ind, opt_err]=min(squeeze(err_L2(k,3,:)))
opt_theta=theta(11)

plot(ypred)
hold on; plot(ub, 'red', 'LineWidth', 2)
p_gpc=7;
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','default');
u_gpc_b=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(xb));
plot(u_gpc_b, 'c')