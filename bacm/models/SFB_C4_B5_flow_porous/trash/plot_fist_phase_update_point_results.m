%% This scrip plots the updated points from the first phase and the solutions
% at get's the best point among them
clear all
clf

% Name of fitting responsees
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',...
    'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',...
    'rms reynolds stress (r_{12})'};
list_of_response_short={'v_x', 'r_{11}',...
    'r_{22}', 'r_{33}',...
    'r_{12}'};

% Load DNS result
[u_dns,coord]=load_experiment_sol_and_coord();
n_out=size(u_dns,2)
% Load best point of B5 from hand callibration
[x_best, u_best]=load_points_and_solution('point_type', 'best_point');
% Load first scan points from phase 1 and the B5 computations at the
% points and calculate statistics
[x_old, u_old, ind_old]=load_points_and_solution('point_type', 'scan_point',...
    'only_converging', true);
N=size(u_old, 3);
var_old=zeros(size(u_dns));
mean_old=zeros(size(u_dns));
%quant_old_l=zeros(size(u_dns));
%quant_old_u=zeros(size(u_dns));
min_old=zeros(size(u_dns));
max_old=zeros(size(u_dns));

mean_old_e=zeros(size(u_dns));
min_old_e=zeros(size(u_dns));
max_old_e=zeros(size(u_dns));

for i=1:n_out
    var_old(:,i)=(var(( squeeze( u_old(:,i,:) ) )'))';
    mean_old(:,i)=(mean(( squeeze( u_old(:,i,:) ) )'))';
    %quant_old_l(:,i)=(quantile(( squeeze( u_old(:,i,:) ) )', 0.05))';
    %quant_old_u(:,i)=(quantile(( squeeze( u_old(:,i,:) ) )', 0.95))';
    min_old(:,i)=(min(( squeeze( u_old(:,i,:) ) )'))';
    max_old(:,i)=(max(( squeeze( u_old(:,i,:) ) )'))';
    
    
    mean_old_e(:,i)=(mean(( squeeze( u_old(:,i,:) )-repmat(u_dns(:,i),1,N) )'))';
    min_old_e(:,i)=(min(( squeeze( u_old(:,i,:) )-repmat(u_dns(:,i),1,N))'))';
    max_old_e(:,i)=(max(( squeeze( u_old(:,i,:) )-repmat(u_dns(:,i),1,N))'))';
end
% Load updated MCMC points from phase 1 and the B5 computations at the
% points
[x, u, ind]=load_points_and_solution('point_type', 'points_from_MCMC',...
    'only_converging', false);

u_var=zeros(size(u_dns));
u_mean=zeros(size(u_dns));
%u_quant_l=zeros(size(u_dns));
%u_quant_u=zeros(size(u_dns));

u_mean_e=zeros(size(u_dns));
u_min_e=zeros(size(u_dns));
u_max_e=zeros(size(u_dns));

for i=1:n_out
    u_var(:,i)=(var(( squeeze( u(:,i,ind) ) )'))';
    u_mean(:,i)=(mean(( squeeze( u(:,i,ind) ) )'))';
    %u_quant_l(:,i)=(quantile(( squeeze( u(:,i,ind) ) )', 0.05))';
    %u_quant_u(:,i)=(quantile(( squeeze( u(:,i,ind) ) )', 0.95))';
    u_min(:,i)=(min(( squeeze( u(:,i,ind) ) )'))';
    u_max(:,i)=(max(( squeeze( u(:,i,ind) ) )'))';
    % the errors
    u_mean_e(:,i)=(mean(( squeeze( u(:,i,ind))-repmat(u_dns(:,i),1,sum(ind)) )'))';
    u_min_e(:,i)=(min(( squeeze( u(:,i,ind) ) -repmat(u_dns(:,i),1,sum(ind)))'))';
    u_max_e(:,i)=(max(( squeeze( u(:,i,ind) ) -repmat(u_dns(:,i),1,sum(ind)))'))';
    
end
% Plot quantiles with QMC samples and with MCMC samples
plot_u_mean_quant(coord', mean_old, min_old, max_old, ...
    'line_color', 'k', 'fill_color', 'b','change_axis', true,...
    'ylabels', list_of_response, 'subplot_dim',[1,5], 'transparency', 0.6)
hold on
plot_u_mean_quant(coord', u_mean, u_min, u_max, ...
    'line_color', 'k:', 'fill_color', 'c', 'change_axis', true,...
    'ylabels', list_of_response, 'subplot_dim',[1,5], 'transparency', 0.8)
for i=1:n_out
    subplot(1,5,i)
    plot(u_dns(:,i), coord,'k-.', 'LineWidth', 3)
end


% Load old points
[x_old, u_old, ind_old]=load_points_and_solution('point_type', 'scan_point', 'only_converging', false);

% RVs
RVs = set_prior_paramset();
%% Get the point with the best norm BEST_U
N=size(u,3);
normofdiff=zeros(1,N);
ERR=reshape((u-repmat(u_dns,1,1,N))./repmat(u_dns,1,1,N),360*5,N );
for i=1:N
    normofdiff(i) = norm(ERR(:,i));
end
[mindiff, minind]=min(normofdiff);

%% Plot new points
figure;
    group=[ones(sum(~ind_old),1);...
        2*ones(sum(ind_old),1); 3;...
        4*ones(sum(~ind),1);...
        5*ones(sum(ind),1); 6];
    gplotmatrix([x_old(:,~ind_old),x_old(:,ind_old),x_best...
        x(:,~ind), x(:,ind), x(:,minind)]',...
        [] ,group ,'rgcrgm','..xoox',[],[],[],RVs.param_plot_names,RVs.param_plot_names);

%%
for i=1:5
    figure
    subplot(1,2,1)
    plot(squeeze(u(:,i, :)),coord )
    hold on
    h_dns  = plot(u_dns(:,i), coord, 'k','LineWidth', 2);
    h_best = plot(u_best(:,i), coord, 'm', 'LineWidth', 2);
    h_ubest= plot(squeeze(u(:,i,minind)), coord, 'c', 'LineWidth',2);
    xlabel(list_of_response_short{i})
    ylabel('coord')
    x_lim=(xlim);
    xlim(x_lim);
    h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    legend([h_dns, h_best, h_ubest, h_interface],...
        {'DNS data', 'B5 hand callibration', 'C4 fist stage maschinery', 'Interface'});
    % plot the same but with the errors
    subplot(1,2,2)
    hold on
    hh_best = plot(abs(u_best(:,i)-u_dns(:,i)), coord, 'm', 'LineWidth', 2);
    hh_ubest= plot(abs(squeeze(u(:,i,minind))-u_dns(:,i)), coord, 'c', 'LineWidth',2);
    xlabel(strvarexpand('|$list_of_response_short{i}$-$list_of_response_short{i}$_{ DNS}|'))
    ylabel('coord')
    x_lim=xlim;
    hh_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    xlim(x_lim);
    legend([hh_best, hh_ubest, hh_interface],...
        { 'B5 hand callibration', 'C4 fist stage maschinery', 'Interface'});
end
actual_path=pwd;
cd f:\\noemi\Documents\SFB880\SFB_flow_over_porous_material\second_phase_results\evaluate_sols_at_fist_hase_updated_points

h = get(0,'children');
for i=1:length(h)
    saveas(h(i), ['figure' num2str(i)], 'png');
    saveas(h(i), ['figure' num2str(i)], 'fig');
end
cd(actual_path)