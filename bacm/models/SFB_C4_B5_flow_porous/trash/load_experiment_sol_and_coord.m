function [u, coord]=load_experiment_sol_and_coord(varargin)
% LOAD_POINTS_AND_SOLUTION
% loads the saved coordinate (X) of the computation points and the
% corresponding solution calculated by the B5 RANS simulation. IND gives
% the indices of converging points. If no OPTIONAL input is defined the
% function loads only the coordinates and solutions of the converging
% points.
% OPTIONAL INPUTS
%  - ONLY_CONVERGING=TRUE/FALSE gives whether only converging points should
%  be loaded (TRUE) or also the nonconverging ones (FALSE)
%
%  - METHOD IDENT or POINT TYPE defines the type of points that should be
%  loaded (some predefined options are available with different POINT_TYPE
%  names, otherwise with METHOD_IDENT any combination of points can be
%  loaded.
%
% - BASE PATH: gives the place where the points and solutions are saved
% (the pathname of the folder POINTS AND SOLUTIONS.
%  
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



options=varargin2options(varargin);
[file_name,options]=get_option(options, 'file_name', []);
check_unsupported_options(options, mfilename);

%% Define paths for points/solutions/index of converging points
if isempty(file_name)
    if isunix
        file_name='/home/noefried/SFB880/flow_trhough_porous_media/SFB_project/points_and_solutions/experiment/exp_sol_and_coord';
    elseif ispc
        file_name='F:\noemi\Documents\SFB880\SFB_flow_over_porous_material\points_and_solutions\experiment\exp_sol_and_coord';
    end
    
end

%%
%% Load data
    S=load(file_name);
    u=S.u;
    coord=S.y;
  