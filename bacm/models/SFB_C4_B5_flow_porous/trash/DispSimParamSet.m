classdef DispSimParamSet < SimParamSet

    methods
        function disp(paramset)
            disp(paramset.tostring)
        end
    end
end