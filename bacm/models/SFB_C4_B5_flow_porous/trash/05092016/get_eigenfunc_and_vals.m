function[r_i_k, sigma_k, u_i_alpha_fluct]=...
    get_eigenfunc_and_vals(u_i_alpha, V_u, L)
n_u=size(u_i_alpha, 1);
[pos, els] = create_mesh_1d(1, n_u, n_u);
u_i_alpha_fluct=u_i_alpha;
u_i_alpha_fluct(:,1)=0;
C=gpc_covariance(u_i_alpha_fluct, V_u);
G_N = mass_matrix(pos, els);
[r_i_k,sigma_k]=kl_solve_evp(C, G_N, L);
