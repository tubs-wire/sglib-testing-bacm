function kalman_filter_upd_SFB
% For this file Elmar's demo of KALMAN filter was used (Noemi Friedman)
%
%   This demos tries to directly reproduce the results from [1], first in
%   the classical "Kalman manner", and then via a GPC model and the MMSE
%   functions. The naming of the variables closely follows [1].
%
% References
%   [1] http://en.wikipedia.org/wiki/Kalman_filter
%
% Example (<a href="matlab:run_example demo_kalman_filter">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

% F is the state transition model which is applied to the previous state x
% B is the control-input model which is applied to the control vector u
% w is the process noise which is assumed to be drawn from a zero mean
%   multivariate normal distribution with covariance Q. 
% H is the observation model which maps the true state space into the
%   observed space 
% v is the observation noise which is assumed to be zero mean Gaussian
%   white noise with covariance R

% State transition model:
%   xn = F * x + B * u + w
% Observation model:
%   z = H * x + v

%% Problem setup
%rand_seed(1234553412345)
% Define the prediction model
p_gpc=7;      
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','beta');
%plot_density(gpc_sample(u_i_alpha, V_u, 100000)','type', 'kernel')
n_params=RVs.num_params;
n_out=size(u_mean,2);
%define coordinates of the measurements
z_ind=[90,100,140,150,170,180, 200,230,240,280];
n_out_ind=(1:5)';
ind=sub2ind(size(u_mean), repmat(z_ind,length(n_out_ind),1), repmat(n_out_ind,1,length(z_ind)));
%outputs to update for

% map to the observable:
V_y=V_u;
y_i_alpha=u_i_alpha(ind,:);
% Define the prior parameters combined with predicted response
[q_i_alpha, V_q]=RVs.gpc_expand();
[V_qy, alpha_qind, alpha_yind] = gpcbasis_combine(V_q, V_y, 'inner_sum');
qy_i_alpha=zeros(n_params+length(ind(:)), gpcbasis_size(V_qy,1));
qy_i_alpha(1:n_params,alpha_qind)=q_i_alpha;
qy_i_alpha(n_params+1:end,alpha_yind)=y_i_alpha;


%% Model error (I don't put here now anything)
%V_w = gpcbasis_create('P', 'm', mw, 'p', 1);
%w_i_beta = gpc_rand_coeffs(V_w, nx, 'zero_mean', true);
%% Measurement
Z=(meas(z_ind,n_out_ind))';

%% Define the measurement error
err_vars=u_var(z_ind, n_out_ind)'/100;
%err_vars(5,:)=err_vars(5,:)/5;
error_params=generate_stdrn_simparamset(sqrt(err_vars));
[v_j_gamma, V_v]=error_params.gpc_expand;

%% Recover the covariance matrices from here
P = gpc_covariance(qy_i_alpha, V_qy);
%Q = gpc_covariance(w_i_beta, V_w);
R = gpc_covariance(v_j_gamma, V_v);


%% Now the classical Kalman stuff
%  a) Prediction step (predicted new state xnp and covariance Pnp)

mean_xi = gpcgerm_moments(V_q);
y_pred  =gpc_evaluate(y_i_alpha, V_y, mean_xi);

xnp = F * mean_x + B * u;
Pnp = F * P * F' + Q;

%  b) Update step (innovation y, innovation covariance S, Kalman gain K,
%  updated state estimate xn, updated covariance Pn)
y = z - H * xnp;
S = H * Pnp * H' + R;
K = Pnp * H' / S;
xn = xnp + K * y;
Pn = (I - K*H)*Pnp;

%% And now with the MMSE functions on GPC variables

% Define a priori model for xn: 
%   xn = F * x + B * u + w
% (The tricky thing is that the separate probalility spaces for x and w
% have to be combined into one, and during the function evaluation split up
% again, so that the x and w part only get "their" variables)
[V_xnp, ~, ~, xi_x_ind, xi_w_ind] = gpcbasis_combine(V_x, V_w, 'outer_sum');
xnp_func = @(xi)(...
    F * gpc_evaluate(x_i_alpha, V_x, xi(xi_x_ind, :)) + ...  % F * x +
    repmat(B*u, 1, size(xi,2)) + ...                         % B * u +
    gpc_evaluate(w_i_beta, V_w, xi(xi_w_ind, :)));           % w

% Just another way to do it, by combining the GPC expansions and then
% making a function out of it
[xw_ii_gamma, V_xnp] = gpc_combine_inputs(x_i_alpha, V_x, w_i_beta, V_w);
xnp_i_gamma = [F, I] * xw_ii_gamma;
xnp_i_gamma(:,1) = xnp_i_gamma(:,1) + B*u;
xnp_func = gpc_function(xnp_i_gamma, V_xnp);

% In-between check that the predicted state xnp and covariance Pnp match
assert_equals(gpc_moments(xnp_i_gamma, V_xnp), xnp, 'predicted state estimate xnp')
assert_equals(gpc_covariance(xnp_i_gamma, V_xnp), Pnp, 'predicted covariance estimate Pnp')

% Define observation model (without the v, that comes extra)
%   z = H * xn
y_func = @(xi)(H * funcall(xnp_func, xi));

% In-between check of the covariance matrices of the observation
assert_equals(gpc_covariance(H*xnp_i_gamma, V_xnp)+R, S, 'innovation covariance S')

% Define error model (must be defined separately from observation model)
v_func = gpc_function(v_j_gamma, V_v);

% Now call the MMSE update procedure
p_phi=1;
p_int_mmse=2;
p_xn=1;
p_int_proj=2;
[xn_i_beta, V_xn]=mmse_update_gpc(xnp_i_gamma, y_func, V_xnp, z, v_func, V_v, p_phi, p_int_mmse, p_xn, p_int_proj);

% Final check of Kalman estimate and covariance update
assert_equals( gpc_moments(xn_i_beta, V_xn), xn, 'updated state estimate xn');
assert_equals( gpc_covariance(xn_i_beta, V_xn), Pn, 'updated covariance estimate Pn');

plot_density(gpc_sample(xn_i_beta, V_xn, 100000)','type', 'kernel', 'kde_sig', 0.001)


% ... and you see that the classical Kalman filter is exactly reproduced.
% The MMSE is completely independent of degree and in the limit case of all
% degrees equal to 1 reproduces the Kalman filter (or linear observation
% processes). 
