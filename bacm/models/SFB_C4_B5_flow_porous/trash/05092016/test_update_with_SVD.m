clear
gpc_registry('reset')

flag_reduce_basis=true;
flag_use_only_hermite=true;
flag_cancel_nonconvergence_gap_probability=false;
%%
% set the type of prior distribution of Q
    a_priori_dist='default';  % for big region update
    %a_priori_dist='beta';  % for big region update
    
% type of points and solution that shall be used for the update
    %point_type= 'best_point_island'; %for island update
    point_type= 'scan_point';         % for big region update
% measurement error variance scaling factor:
    %beta_sigma=1; %for island update
    beta_sigma=2; % for big region update
% error percentage of sigma
    sigma_fact=[0.5,2,2,2,1.5];
% number of posterior samples
N=10000;
%% gPCE proxi model
p_gpc=5;
plot_options={'plot_mean_var_flag',false, 'plot_part_vars',false,'plot_response_surf',false};
uq_options={'point_type', point_type, 'prior_dist',a_priori_dist};

if flag_use_only_hermite
    [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=...
        main_UQ(p_gpc, uq_options{:},'flag_PCE', true, plot_options{:});
    germ2param_func=@(xi)RVs.stdnor2params(xi);
    param2germ_func=@(q)RVs.params2stdnor(q);
else
    
    [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=...
        main_UQ(p_gpc, uq_options{:},plot_options{:});
    germ2param_func=@(xi)RVs.germ2params(xi);
    param2germ_func=@(q)RVs.params2germ(q);
end

%% Reduce basis to beta and beta_t
if flag_reduce_basis
    [u_i_alpha, V_u]=gpc_reduce_basis_dim(u_i_alpha, V_u, [1,2]);
    % Fix least influentiable parameters
    RVs.set_to_mean(3)
    RVs.set_to_mean(4)
    RVs.set_to_mean(5)
    RVs.set_to_mean(6)
end
%% Initialize update
% Measurement

% variance of the masurement errors (gassian curve with center at interface
% +3%)
perc_sigma=(3*ones(size(coord))+2*exp(-(coord-0.9).^2./(2*(0.1/3)^2)));
err_sigma=binfun(@times,  max(abs(meas)).*sigma_fact, perc_sigma/200)*beta_sigma;
err_var=(err_sigma).^2;

%% Call classifyer for nonconvergence gap definition
if flag_cancel_nonconvergence_gap_probability
    flag_converge_func=get_flag_converge_func('point_type', point_type);
    does_xi_a_good_region_func=@(xi)(funcall(flag_converge_func,germ2param_func(xi)));
    filter_func=@(xi)xi(:,does_xi_a_good_region_func(xi));
else
    flag_converge_func={};
end
%% Low rank representation
Q_sample=Q.sample(10000);
xi_sample=param2germ_func(Q_sample);
if flag_cancel_nonconvergence_gap_probability
    xi_sample=filter_func(xi_sample);
end
% Subtract mean
u_i_alpha_0mean=u_i_alpha;
u_i_alpha_0mean(:,1)=0;
mean_y=gpc_moments(u_i_alpha, V_u);
Y_samples=gpc_evaluate(u_i_alpha_0mean, V_u, xi_sample(~Q.find_fixed, :));

% [U,S,V] = svd(Y_samples, 0);
% plot(diag(S), 'x')
% xlim([0,15]);
% % Let's keep only 5 modes
% n_modes=5;
% A=U(:,1:n_modes)';

U=zeros(1800,1);
S=zeros(1,1);
V=zeros(0,1);
N_Y=size(Y_samples,2);
for i=1:10:N_Y-rem(N_Y,10)
    [Un,S,V]=svd_update(U, S, V, Y_samples(:,i:i+7), 'reltol', 0.01);
    U=flip_to_align(Un,  U);
    [i, size(U)]
    plot(U)
    ylim(0.15*[-1;1]);
    drawnow
end
%Linear transformation
n_mod=6;
%n_mod=size(U,2);
A=U(:,1:n_mod)';
Ys_func = @(xi)gpc_evaluate(A*u_i_alpha_0mean, V_u, xi);
%% Error model
% Covariance matrix of transformed error
C_eps=A*diag(err_sigma(:))*A';

% standard deviations of each variable
S = diag(sqrt(diag(C_eps)));
SInv = inv(S);
% Correlation matrix
Cor_eps = SInv * C_eps * SInv;
L = chol(Cor_eps, 'lower');

% Get transformation from standard normal distributed E to eps
E=generate_stdrn_simparamset(ones(n_mod));

% map from error germ to error
E_func = @(xi)(S*L*xi);

% PCE of the error
[e_i_alpha, V_e] = E.gpc_expand();
% gPCE of f_cm
    %[q_i_alpha, V_q] = RVs.gpc_expand();
    
    [q_i_alpha, V_q] = RVs.gpc_expand('expand_options', {'syschar', 'h'});
% MMSE order and num integration orders
p_phi=3;
p_qn=p_phi*p_gpc+2;
p_q = gpcbasis_info(V_q, 'total_degree');
p_int_mmse=max(p_gpc*p_phi+1,ceil((p_q+p_phi*p_gpc+1)/2));
p_int_proj=p_qn+1;
% Update
% extra_options={'filter_func', filter_func};
% [q_post_i_beta, V_q_post] = mmse_update_gpc_noemi(q_i_alpha, ...
%     Ys_func, V_q, A*meas(:), E_func, V_e, p_phi, p_int_mmse,...
%     p_qn, p_int_proj, 'int_grid', 'qmc', 'extra_options', extra_options);

[q_post_i_beta, V_q_post] = mmse_update_gpc_noemi(q_i_alpha, ...
    Ys_func, V_q, A*(meas(:)-mean_y), E_func, V_e, p_phi, p_int_mmse,...
    p_qn, p_int_proj, 'int_grid', 'smolyak');
% mean and var of the updated f_cm
[q_post_mean, q_post_var]=gpc_moments(...
    q_post_i_beta, V_q_post);
% samples of the posterior
samples_post_param=gpc_sample(q_post_i_beta, V_q_post, N);


