function island_points=island_scanning()

%% I.a Define Random Variables
%params for forward calculation
RVs = set_prior_paramset('type', 'island_uni');

qmc_options={'scramble', 'bw', 'n0', 10^7};
island_points=RVs.sample(1500,'mode', 'qmc', 'qmc_options', qmc_options);
%save('island_points.mat', 'island_points')