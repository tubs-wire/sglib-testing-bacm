function [u_i_alpha, V_u]=project_solution(u_ij, V_u, x_ref, w, varargin)

% options=varargin2options(varargin);
% [method, options]=get_option(options, 'method', 'projection');
% check_unsupported_options(options, mfilename);

Q=length(w);
M=gpcbasis_size(V_u, 1);

%A=gpcbasis_evaluate(V_u, x_ref);

u_i_alpha = zeros(size(u_ij,1), M);
for j = 1:Q
    display(strvarexpand('$j$/$Q$'));
    u_i_j=u_ij(:,j);
    x_j = x_ref(:, j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
    u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
end

% if strcmp(method, 'colocation')
%     u_i_alpha=A\u_ij;
% end



