function [q_i_alpha]=gpc_EnKF(u_i_alpha, V_u, Q,  Z, err_vars, varargin)

%   Elmar Zander & Noemi Friedman
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



%% Prior distribution xi
prior_xi=gpcgerm_create_paramset(V_u);
vars_germ=prior_xi.vars;
% if change_prior_to_normal
%     prior_q=generate_standard_normal_simparamset(sqrt(vars_germ));
% end
V_xi=prior_xi.get_germ;
[xi_i_alpha, V_xi]=gpc_expand(prior_xi);
%% Prior distribution xi
prior_xi=gpcgerm_create_paramset(V_u);
vars_germ=prior_xi.vars;
% if change_prior_to_normal
%     prior_q=generate_standard_normal_simparamset(sqrt(vars_germ));
% end
V_xi=prior_xi.get_germ;
[xi_i_alpha, V_xi]=gpc_expand(prior_xi);

%% measurement error
e=generate_standard_normal_simparamset(sqrt(err_vars));

[e_i_alpha, V_e]=  gpc_expand_RVs(e);

%% Problem setup

% Recover the covariance matrices from here
C_qy = gpc_covariance(q_i_alpha, y_i_alpha);
C_yy = gpc_covariance(u_i_alpha, u_i_alpha);
C_err = gpc_covariance(e_i_alpha, e_i_alpha);


%% Now the classical Kalman stuff
%  a) Prediction step (predicted new state xnp and covariance Pnp)

mean_x = gpc_moments(x_i_alpha, V_x);
xnp = F * mean_x + B * u;
Pnp = F * P * F' + Q;

%  b) Update step (innovation y, innovation covariance S, Kalman gain K,
%  updated state estimate xn, updated covariance Pn)
y = z - H * xnp;
S = H * Pnp * H' + R;
K = Pnp * H' / S;
xn = xnp + K * y;
Pn = (I - K*H)*Pnp;

%% And now with the MMSE functions on GPC variables

% Define a priori model for xn: 
%   xn = F * x + B * u + w
% (The tricky thing is that the separate probalility spaces for x and w
% have to be combined into one, and during the function evaluation split up
% again, so that the x and w part only get "their" variables)
[V_xnp, ~, ~, xi_x_ind, xi_w_ind] = gpcbasis_combine(V_x, V_w, 'outer_sum');
xnp_func = @(xi)(...
    F * gpc_evaluate(x_i_alpha, V_x, xi(xi_x_ind, :)) + ...  % F * x +
    repmat(B*u, 1, size(xi,2)) + ...                         % B * u +
    gpc_evaluate(w_i_beta, V_w, xi(xi_w_ind, :)));           % w

% Just another way to do it, by combining the GPC expansions and then
% making a function out of it
[xw_ii_gamma, V_xnp] = gpc_combine_inputs(x_i_alpha, V_x, w_i_beta, V_w);
xnp_i_gamma = [F, I] * xw_ii_gamma;
xnp_i_gamma(:,1) = xnp_i_gamma(:,1) + B*u;
xnp_func = gpc_function(xnp_i_gamma, V_xnp);

% In-between check that the predicted state xnp and covariance Pnp match
assert_equals(gpc_moments(xnp_i_gamma, V_xnp), xnp, 'predicted state estimate xnp')
assert_equals(gpc_covariance(xnp_i_gamma, V_xnp), Pnp, 'predicted covariance estimate Pnp')

% Define observation model (without the v, that comes extra)
%   z = H * xn
y_func = @(xi)(H * funcall(xnp_func, xi));

% In-between check of the covariance matrices of the observation
assert_equals(gpc_covariance(H*xnp_i_gamma, V_xnp)+R, S, 'innovation covariance S')

% Define error model (must be defined separately from observation model)
v_func = gpc_function(v_j_gamma, V_v);

% Now call the MMSE update procedure
p_phi=1;
p_int_mmse=2;
p_xn=1;
p_int_proj=2;
[xn_i_beta, V_xn]=mmse_update_gpc(xnp_func, y_func, V_xnp, z, v_func, V_v, p_phi, p_int_mmse, p_xn, p_int_proj);

% Final check of Kalman estimate and covariance update
assert_equals( gpc_moments(xn_i_beta, V_xn), xn, 'updated state estimate xn');
assert_equals( gpc_covariance(xn_i_beta, V_xn), Pn, 'updated covariance estimate Pn');

% ... and you see that the classical Kalman filter is exactly reproduced.
% The MMSE is completely independent of degree and in the limit case of all
% degrees equal to 1 reproduces the Kalman filter (or linear observation
% processes). 
end
%% Generate parameterset with standard normal parameters
function paramset=generate_standard_normal_simparamset(sigmas)
n=length(sigmas);
params=cell(n,1);
for i=1:n
    str= strvarexpand('params{$i$}=SimParameter(''p_$i$'',NormalDistribution(0,sqrt(sigmas($i$))));');
    eval(str);
end
paramset=SimParamSet1(params{:});
end

%% Generate parameterset from gPCE germs
function paramset=gpcgerm_create_paramset(V_u)
%Get polysys letters of the germs
M=gpcbasis_size(V_u, 2);
polys=V_u{1};
if ~(length(polys)==M)
    repmat(polys, 1,M)
end

dist=cell(M,1);
for i=1:M
    polys_i=polys(i);
    [~, dist{i}]=gpc_registry('get', polys_i);
end
paramset=SimParamSet();
params=cell(M,1);
for i=1:M
    if ~isa(dist{i}, 'Distribution')
        dist_i=gendist2object(dist{i});
    else
        dist_i=dist{i};
    end
    str= strvarexpand('params{$i$}=SimParameter(''p_$i$'',dist_i);');
    eval(str);
end

paramset.add_parameter(params{:})
end
     
