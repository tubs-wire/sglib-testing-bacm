%% reset
clear
gpc_registry('reset')

%% UPDATE settints
update_mode='MCMC';
% update_mode='MMSE';
% A priori distribution
%a_priori_dist='island_uni'; %for island update
a_priori_dist='default';  % for big region update
%a_priori_dist='beta';  % for big region update
% should the model look for the region where it's not converging and set 0
% prior probabilities there for the update?
flag_cancel_nonconvergence_gap_probability=true;
% number of 'sensors'
%    n_meas=50; % this is not really doable for MMSE
     n_meas=5;  % for MMSE
% type of points and solution that shall be used for the update
%point_type= 'best_point_island'; %for island update
point_type= 'scan_point';         % for big region update
%point_type= 'qMC_and_MCMC';         % for second phase update
%('default'=uniform in the big domain
% see more in set_prior_paramset
% gPCE order
p_gpc=5;

% number of MCMCM posterior samples
N=10000;
% measurement error variance scaling factor:
%beta_sigma=1; %for island update
beta_sigma=1; % for big region update
sigma_fact=[0.5,2,2,2,1.5];
% make plots?
flag_plot=true;
% save plots?
flag_save_plots=true;
% save results (posterior samples, proxi model...)?
save_results=false;
%% Caculation of the forward method
% Calculate the forward model
plot_options={'plot_mean_var_flag',true, 'plot_part_vars',true,'plot_response_surf',true};
uq_options={'point_type', point_type, 'prior_dist',a_priori_dist};
switch update_mode
    case 'MCMC'
        [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=...
            main_UQ(p_gpc, uq_options{:},plot_options{:});
        germ2param_func=@(xi)RVs.germ2params(xi);
        param2germ_func=@(q)RVs.params2germ(q);
    case'enKF'
        [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=...
            main_UQ(p_gpc, uq_options{:},'flag_PCE', true, plot_options{:});
        germ2param_func=@(xi)RVs.stdnor2params(xi);
        param2germ_func=@(q)RVs.params2stdnor(q);
    case 'MMSE'
        [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=...
            main_UQ(p_gpc, uq_options{:},plot_options{:});
        germ2param_func=@(xi)RVs.germ2params(xi);
        param2germ_func=@(q)RVs.params2germ(q);
end

%%  sign measurement locations
% choose randomly N_MEAS coordinates in (0,2)
%meas_coord=rand(1,n_meas)*2;
% n_1=ceil(n_meas/4);
% n_2=ceil(n_meas/20);
% n_3=n_meas-n_1-n_2;

% n_1=ceil(n_meas/3);
% n_2=ceil(n_meas/5);
% n_3=n_meas-n_1-n_2;
X=MySimParamSet();
X.add('x_1', UniformDistribution(0,2));
meas_coord=sort(X.sample(n_meas, 'mode', 'qmc'));

% meas_coord=zeros(1,n_meas);
% meas_coord(1:n_1)=rand(1,n_1)*0.8;
% meas_coord(n_1+1:n_1+n_2)=rand(1,n_2)*0.2+0.8;
% meas_coord(n_1+n_2+1:end)=rand(1,n_3)+1.1;
% meas_coord=sort(meas_coord);
[~, z_ind]=min(abs(repmat(meas_coord, length(coord),1)-repmat(coord,1,n_meas)));


%define coordinates of the measurements
%z_ind=[90,100,125,140,150,170,180,200,230,240,260,280];
%z_ind=randi([1,length(coord)],1,12);
%z_ind=1:360;

% pointer IND to the measureables (to prediction at the sensors)
syschars=V_u{1};
n_out=size(u_mean,2);
%outputs to update for
n_out_ind=(1:5)';
ind=sub2ind(size(u_mean), repmat(z_ind,length(n_out_ind),1), repmat(n_out_ind,1,length(z_ind)));
ind=ind(:);


%% Initialize update
% Measurement
Z=(meas(z_ind,n_out_ind))';
% variance of the masurement errors (gassian curve with center at interface
% +3%)

perc_sigma=(3*ones(size(coord))+2*exp(-(coord-0.9).^2./(2*(0.1/3)^2)));
err_sigma=binfun(@times,  max(abs(meas)).*sigma_fact, perc_sigma/200)*beta_sigma;
err_sigmas=err_sigma(z_ind, n_out_ind)';
err_var=(err_sigma).^2;
err_vars=(err_sigmas).^2;

%err_vars=u_var(z_ind, n_out_ind)'/100;
%err_vars(5,:)=err_vars(5,:);

% prior distribution (with prior dist of the UQ)
n_germ=gpcbasis_size(V_u,2);
prior_dist=cell(n_germ, 1);
for i=1:n_germ
    prior_dist{i}=polysys_dist(syschars(i));
end
prior_dist_xi=gendist2object(prior_dist);

% Observation operator
observ_operator=@(xi) gpc_evaluate(u_i_alpha(ind,:), V_u, xi);


%% Best point from model and best point from proxi model
[x_best, u_best]=load_points_and_solution('point_type', 'best_point');
germ_at_best=param2germ_func(x_best);
u_proxi_at_best=gpc_evaluate(u_i_alpha, V_u, germ_at_best);
u_proxi_at_best=reshape(u_proxi_at_best, [], n_out);

%% Call classifyer for nonconvergence gap definition
if flag_cancel_nonconvergence_gap_probability
    flag_converge_func=get_flag_converge_func('point_type', point_type);
    does_xi_a_good_region_func=@(xi)(funcall(flag_converge_func,germ2param_func(xi)));
else
    flag_converge_func={};
end

%% UPDATE
switch update_mode
    %% update with MCMC
    case 'MCMC'
        MCMC_options={'start_point',germ_at_best, 'is_prior_nonzero_func', does_xi_a_good_region_func};
        %MCMC_options={'is_prior_nonzero_func', does_xi_a_good_region_func};
        [samples_post, acc_rate]=...
            gpc_MCMC_v(N, observ_operator, prior_dist_xi, Z(:), err_vars(:), MCMC_options{:});
        samples_post_param=RVs.germ2params(samples_post);
        
    case 'enKF'
        E=generate_stdrn_simparamset(err_sigmas(:));
        [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(RVs, E, u_i_alpha(ind,:), V_u, Z(:));
        samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
        samples_post_param=RVs.stdnor2params(samples_post);
        if flag_cancel_nonconvergence_gap_probability
            ind_conv=flag_converge_func(samples_post_param);
        end
        samples_post_param=samples_post_param(:,ind_conv);
    case 'MMSE'
        Y_func = @(xi)gpc_evaluate(u_i_alpha(ind,:), V_u, xi);
        E=generate_stdrn_simparamset(err_sigmas(:));
        % map from error germ to error
        E_func = @(xi)(E.germ2params(xi));
        % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();
        % gPCE of f_cm
        [q_i_alpha, V_q] = RVs.gpc_expand();
        % MMSE order and num integration orders
        p_phi=3;
        p_pn=4;
        p_int_mmse=9;
        p_int_proj=9;
        % Update
        [q_post_i_beta, V_q_post] = mmse_update_gpc(q_i_alpha, ...
            Y_func, V_q, Z(:), E_func, V_e, p_phi, p_int_mmse,...
            p_pn, p_int_proj, 'int_grid', 'smolyak');
        % mean and var of the updated f_cm
        [q_post_mean, q_post_var]=gpc_moments(...
            q_post_i_beta, V_q_post);
        % samples of the posterior
        samples_post_param=gpc_sample(q_post_i_beta, V_q_post, N);
        
end

%% setup for the plots
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',...
    'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',...
    'rms reynolds stress (r_{12})'};

short_list_of_response={'v_x', 'r_11',...
    'r_22', 'r_33',...
    'r_12'};
%% Plot
if flag_plot
    % plot posterior samples
    h(1)=figure;
    x_new=[x,samples_post_param, x_best];
    gplotmatrix(samples_post_param', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);
    % plot priori and posterior samples and the one found to be best by B5
    h(2)=figure;
    group=[ones(size(x,2),1);2*ones(size(samples_post_param,2),1); 3];
    gplotmatrix(x_new', [] ,group ,'bmg','.ox',[],[],[],RVs.param_plot_names,RVs.param_plot_names);
    % statistics of posterior samples and output from proxi model
    u_upd=gpc_evaluate(u_i_alpha, V_u, samples_post);
    u_mean_u=reshape(mean(u_upd,2),[],n_out);
    u_var_u=reshape(var(u_upd,[],2), [],n_out);
    u_quant_ul=reshape(quantile(u_upd, 0.025,2), [], n_out);
    u_quant_uu=reshape(quantile(u_upd, 0.975,2), [], n_out);
    MCMC_xi_mean=mean(samples_post,2);
    % response and the posterior mean
    u_with_updated_param_mean=gpc_evaluate(u_i_alpha, V_u, MCMC_xi_mean);
    u_newbest=reshape(u_with_updated_param_mean,size(u_mean)); %reshape
    % plot mean and quantile of solution from posterior samples from proxi
    u_quant=gpc_quantiles(V_u, u_i_alpha, 'quantiles', [0.025, 0.975]);
    u_quant_l=reshape(squeeze(u_quant(1,:)),[], n_out); %2.5% quantile
    u_quant_u=reshape(squeeze(u_quant(2,:)),[], n_out); %97.5% quantile
    for i=1:n_out
        h(2+i)=figure;
        % plot mean and quantiles
        subplot(2,1,1)
        plot_u_mean_quant(coord, u_mean(:,i), u_quant_l(:,i), u_quant_u(:,i),...
            'fill_color','c', 'line_color','k:','transparency', 0.4,...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        hold on
        plot_u_mean_quant(coord, u_mean_u(:,i), u_quant_ul(:,i), u_quant_uu(:,i),...
            'fill_color','magenta', 'line_color','m','transparency', 0.4, ...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        plot_u_mean_var(coord, meas(:,i), err_var(:,i), 'fill_color','b', ...
            'line_color','b--','transparency', 0.4, 'ylabels',...
            list_of_response, 'times_sigma', 2, 'xlabel', 'coord', ...
            'flag_subplot', false, 'ylabels',{list_of_response{i}})
        legend('A priori 95% confidence region', 'A priori mean',...
            'Posterior 95% confidence value', 'Posterior mean',...
            'DNS simulation 95% confidence region', 'DNS simulation mean' )
        
        % plot only errors
        subplot(2,1,2)
        plot_u_mean_quant(coord, ...
            u_mean(:,i)- meas(:,i), u_quant_l(:,i)- meas(:,i), u_quant_u(:,i)- meas(:,i),...
            'fill_color','c', 'line_color','k:','transparency', 0.4,...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        hold on
        plot_u_mean_quant(coord,...
            u_mean_u(:,i)- meas(:,i), u_quant_ul(:,i)- meas(:,i), u_quant_uu(:,i)- meas(:,i),...
            'fill_color','magenta', 'line_color','m','transparency', 0.4,...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        plot_u_mean_var(coord, zeros(size(meas(:,i))), err_var(:,i),...
            'fill_color','b', 'line_color','b--','transparency', 0.4, ...
            'ylabels',list_of_response, 'times_sigma', 2, 'xlabel', 'coord',...
            'flag_subplot', false, 'ylabels',{list_of_response{i}})
        legend('A priori 95% confidence region', 'A priori mean deviation from DNS',...
            'Posterior 95% confidence value', 'Posterior mean deviation from DNS',...
            'DNS simulation 95% confidence region', 'DNS simulation reference')
    end
    %Choose closest among the points
    normofdiff=zeros(1,N);
    ERR=(u_upd-repmat(meas(:),1,N))./repmat(meas(:),1,N);
    for i=1:N
        normofdiff(i) = norm(ERR(:,i));
    end
    [mindiff, minind]=min(normofdiff);
    best_upd_germ=samples_post(:,minind);
    best_upd_u=gpc_evaluate(u_i_alpha, V_u, best_upd_germ);
    u_bestbest=reshape(best_upd_u,[],n_out); % reshape
    % plot DNS response (MEAS), plot best solution from B5 (U_BEST), and
    % the one which is closest to MEAS from posterior samples (U_BESTBEST)
    
    for i=1:n_out
        %subplot(5,1,i)
        h(2+n_out+i)=figure
        subplot(2,1,1)
        hold on
        
        plot(coord, u_best(:,i), 'b')
        plot(coord, u_newbest(:,i), 'c', 'LineWidth', 2)
        plot(coord, u_bestbest(:,i),'b',  'LineWidth', 2)
        plot(coord, meas(:,i),'red',  'LineWidth', 2)
        plot(coord(z_ind),zeros(size(z_ind)), 'x')
        xlabel('y', 'FontSize', 12)
        ylabel(list_of_response{i})
        legend('Calibrated response from B5','Response from posterior mean',...
            'Closest to DNS from posterior samples', 'DNS simulation', 'Sensors' )
        
        subplot(2,1,2)
        hold on
        %plot(coord, meas(:,i),'red',  'LineWidth', 2)
        %plot(coord, u_newbest(:,i), 'c')
        
        plot(coord, abs(u_best(:,i)-meas(:,i)), 'b')
        plot(coord, abs(u_newbest(:,i)-meas(:,i)),'c',  'LineWidth', 2)
        plot(coord, abs(u_bestbest(:,i)-meas(:,i)),'b',  'LineWidth', 2)
        plot(coord(z_ind),zeros(size(z_ind)), 'x')
        xlabel('y', 'FontSize', 12)
        ylabel(list_of_response{i})
        legend('Calibrated response from B5','Response from posterior mean', 'Closest to DNS from posterior samples', 'Sensors')
        
    end
    
    h(2+2*n_out+1)=figure
    plot(samples_post')
    legend(RVs.param_plot_names)
    
    if flag_save_plots
        savefig(h(1), 'posterior_samples_matrixplot.fig');
        savefig(h(2), 'prior_and_posterior_samples_matrixplot.fig');
        for i=1:n_out
            file_name_i1=strvarexpand(...
                '$i$$short_list_of_response{i}$_prior_and_post_mean_and_quantiles_and_dev_from_DNS.fig');
            savefig(h(2+i),file_name_i1)
            file_name_i2=strvarexpand(...
                '$i$$short_list_of_response{i}$_response_from_posterior_mean_and_best_fit_point.fig');
            savefig(h(2+n_out+i),file_name_i2)
        end
    end
    
end

if save_results
    save('results.mat', 'samples_post', 'samples_post_param','err_sigma', 'err_sigmas', 'beta_sigma', 'meas_coord', 'ind', 'RVs', 'V_u', 'u_i_alpha')
end