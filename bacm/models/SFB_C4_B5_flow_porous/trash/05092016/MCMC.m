function samples_post=gpc_MCMC(V_p, u_i_alpha, V_u, Z, err_vars)
t_ind=100;
ind=t_ind:360:1800;

%u_i_alpha
u_i_alpha=u_i_alpha(ind, :);
%Measurement
Z=(Exp_data_yi_j(t_ind,:))';
%variance of the errors
err_vars=u_var_proj(t_ind,:)/10;

%% get proposal distributions

%get mean and var of germs
M=gpcbasis_size(V_p, 2);
dist=cell(M,1);
vars_germ=zeros(M,1);
mean_germ=zeros(M,1);
for i=1:M
    polys_i=polys(i);
    [~, dist{i}]=gpc_register_polysys(polys_i);
    [mean_germ(i), vars_germ(i)]=gendist_moments(dist{i});
end

Prop_params=generate_standard_normal_simparamset(sqrt(vars/2));

%% measurement error
M_error_params=generate_standard_normal_simparamset(sqrt(err_vars));

%% MCMC
N=10000;
Posterior=mh_sample(N, dist, Prop_params, M_error);
%X=mh_sample_parallel(N, dist, prop_dist);
X2=gendist_sample(N, dist);


multiplot_init(2,2)

% Plot MH density and correlation
multiplot;
hold off;
plot_density(X); hold all; grid on;
legend_add('metropolis');
plot_density(dist);
legend_add('exact dist');

multiplot;
%plot(X(1:end-1), X(2:end), 'x');


% Plot density and correlation from direct sampling
multiplot;
plot_density(X2); hold all; grid on;
legend_add('direct samples');
plot_density(dist);
legend_add('exact dist');

multiplot;
plot(X2(1:end-1), X2(2:end), 'x');

multiplot_adjust_range('separate', 'rows');


end
function X=mh_sample(N, x_0, Prop_params, u_i_alpha, V_u, M_error_params, Z)
% MH_SAMPLE Basic version of the Metropolis-Hastings sampler
x_0


N_burn = 1000;
    
x=x_0;
u=gpc_evaluate(u_i_alpha(ind,:),V_u, x);


X=[];
for i=1:N+N_burn
    xn=x+Prop_params.sample(1);
    un=gpc_evaluate(u_i_alpha(ind,:),V_u, xn');
    a=M_error_params.pdf(un-Z)/M_error_params.pdf(u-Z);
    a=a*Prop_params.pdf(x-xn)/Prop_params.pdf(xn-x);
    if a>=1 || rand<a
        x=xn;
    end
    if i>M
        X=[X x];
    end
end
X=X(1:N);
end

function X=mh_sample_parallel(N, dist, prop_dist)
% MH_SAMPLE_PARALLEL Parallel version of the Metropolis-Hastings sampler

K = 100;
M = K*ceil(1000/K);

x=gendist_moments(dist);
x = repmat(x, 1, K);
X=[];
p = gendist_pdf(x, dist);

for i=1:ceil((N+M)/K)
    xn=x+gendist_sample(K, prop_dist)';
    pn=gendist_pdf(xn, dist);
    a=pn./p;
    a=a.*gendist_pdf(x-xn, prop_dist)./gendist_pdf(xn-x,prop_dist);
    ind = (a>=1 | rand(1,K)<a);
    x(ind)=xn(ind);
    p(ind)=pn(ind);
    if i>M/K
        X=[X, x];
    end
end
X=X(1:N);
end
    function test_metropolis_hastings
% TEST_METROPOLIS_HASTINGS Test code for the Metropolis-Hastings sampling method
% See: https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm

%dist=gendist_create('normal', {20, 3});
dist=gendist_create('beta', {2, 4});
%dist=gendist_create('lognormal', {-1, .37});

[~,sig2]=gendist_moments(dist);
prop_dist = gendist_create('normal', {-0.02, 0.3*sqrt(sig2)});

N=10000;

%X=mh_sample(N, dist, prop_dist);
X=mh_sample_parallel(N, dist, prop_dist);
X2=gendist_sample(N, dist);


multiplot_init(2,2)

% Plot MH density and correlation
multiplot;
hold off;
plot_density(X); hold all; grid on;
legend_add('metropolis');
plot_density(dist);
legend_add('exact dist');

multiplot;
plot(X(1:end-1), X(2:end), 'x');


% Plot density and correlation from direct sampling
multiplot;
plot_density(X2); hold all; grid on;
legend_add('direct samples');
plot_density(dist);
legend_add('exact dist');

multiplot;
plot(X2(1:end-1), X2(2:end), 'x');

multiplot_adjust_range('separate', 'rows');

    end



    function paramset=generate_standard_normal_simparamset(sigmas)
        n=length(sigmas);
        params=cell(n,1);
        for i=1:n
            str= strvarexpand('params{$i$}=SimParameter(''p_$i$'',NormalDistribution(0,sqrt(sigmas($i$))));');
            eval(str);
        end
        paramset=SimParamSet1(params{:});
    end
     function paramset=generate_simparamset(dist)
        n=length(dist);
        params=cell(n,1);
        for i=1:n
            str= strvarexpand('params{$i$}=SimParameter(''p_$i$'',dist{$i$});');
            eval(str);
        end
        paramset=SimParamSet1(params{:});
    end
    