%points_tot=[conv_sample_points_tot,nonconv_sample_points_tot];
[x, ~, ind]=load_points_and_solution('only_converging', false, 'point_type', 'best_point_island');
%% set colors and point size for plot
%set coloring map for convergin/not converging/and nonconverging points
%extreme values
v_color=repmat([2], size(ind));
v_color(ind)=1;
ind_t=~(x(5,:)>2 &x(6,:)<0.395);
ind_t(ind)=false;
v_color(ind_t)=3;

v_size=repmat(50, size(ind));

%% Plot points
scatter3(x(1,:), x(2,:), x(6,:), v_size, v_color, 'filled')
figure
scatter3(x(1,:), x(2,:), x(5,:), v_size, v_color, 'filled')

scatter3(x(1,~ind_t), x(2,~ind_t), x(6,~ind_t), v_size(~ind_t), v_color(~ind_t), 'filled')
%figure
%scatter3(x(1,~ind_t), x(2,~ind_t), x(5,~ind_t), v_size(~ind_t), v_color(~ind_t), 'filled')

%% Plot surfaces
% plot ellipsoid
hold on
x=linspace(-10,0,30);
y=linspace(-1, 1, 30);
[X,Y]=meshgrid(x,y);
Z=-sqrt( 1.1-(X/6.5).^2-( (Y-1)/1.1).^2)*0.6+0.4;
surf(X,Y,real(Z));

%plot cyllinder
[X_c,Y_c,Z_c] = cylinder(1,50);
X_c=X_c*6.5;
Y_c=Y_c*1.1+1;
Z_c(2,:)=0.4;
ind_plot=X_c(1,:)>-10 & X_c(1,:)<0 & Y_c(1,:)>-1 & Y_c(1,:)<1;
surf(X_c(:,ind_plot), Y_c(:,ind_plot), Z_c(:,ind_plot))

%set axis limits and label
xlim([-10,0])
ylim([-1,1])
zlim([0, 0.4])
xlabel(list_of_RVs{1})
ylabel(list_of_RVs{2})
zlabel(list_of_RVs{6})