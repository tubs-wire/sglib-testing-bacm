clear variables
seperate_ratio=3/4;
[V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(2, 'point_type', 'qMC_and_MCMC', 'separate_points',seperate_ratio);
n=size(x1,2);
[V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(2, 'point_type', 'all_qMC_and_MCMC', 'separate_points',seperate_ratio);
n1=size(x1,2);
% Analyze surrogates from order
from_p=2;
% to order
to_p=8;
% check this many cross validations
n_v=10;
% initiate memory for solutions at validation points
u_p=zeros(size(u_i_alpha,1),  n,  to_p-from_p+1, n_v);
u_p1=zeros(size(u_i_alpha,1), n1, to_p-from_p+1, n_v);
seed_numb=644;
p=from_p:to_p;
errsum=zeros(length(p),5);
errsum1=zeros(length(p),5);


for j=1:n_v;
    for i=1:length(p)
        display(p(i))
        display(j)
        p_gpc=p(i);
        [V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=...
            main_UQ(p_gpc, 'point_type', 'qMC_and_MCMC',...
            'separate_points',seperate_ratio, 'seed_numb', seed_numb);
        u1=u1(:);
        u_p=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x1));
        u_p=u_p(:);
        errsum(i)=errsum(i)+norm(u_p-u1);
        [V_u, u_i_alpha, ~, RVs, meas, coord, x01, u01, ~, ~, x11, u11]=...
            main_UQ(p_gpc, 'point_type', 'all_qMC_and_MCMC',...
            'separate_points',seperate_ratio, 'seed_numb', seed_numb);
        u11=u11(:);
        u_p1=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x11));
        u_p1=u_p1(:);
        errsum1(i)=errsum1(i)+norm(u_p1-u11);
    end
    seed_numb=seed_numb+10;
end



% 
% u_test=reshape(u_p, [], to_p-from_p+1, j);
% u_test1=reshape(u_p1, [], to_p-from_p+1, j);
% 
% u1_test=u1(:);
% u1_test1=u11(:);
% Err_p=u_test-repmat(u1_test,1,to_p-from_p+1);
% Err_p1=u_test1-repmat(u1_test1,1,to_p-from_p+1);
% %Err_p=[u_p;u_p_b]-[repmat(u,1,to_p-from_p);repmat(u_b,1,to_p-from_p+1)];
% Snorm = diag(sqrt(Err_p'*Err_p));
% Snorm1 = diag(sqrt(Err_p1'*Err_p1));
