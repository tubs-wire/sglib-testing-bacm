function[flag_convergence_func]=get_flag_converge_func(varargin)
options=varargin2options(varargin);
[point_type, options]=get_option(options, 'point_type', 'all_qmc_point_with_spec_point');
check_unsupported_options(options, mfilename);

[x, ~, ind]=load_points_and_solution('only_converging', false, 'point_type', point_type);


SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction','RBF',...
    'KernelScale','auto', 'ClassNames', [false, true]);


%SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction','RBF',...
%'KernelScale','auto', 'ClassNames', [false, true], 'BoxConstraint', Inf);

flag_convergence_func=@(x)predict(SVMModel, x');