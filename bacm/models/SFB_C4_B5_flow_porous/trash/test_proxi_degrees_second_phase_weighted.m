clear variables
%% choose ratio of training and validating set 
separate_ratio=0.7;
[V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(2, 'point_type', 'qMC_and_MCMC', 'separate_points',seperate_ratio);
n=size(x1,2);
[V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(2, 'point_type', 'all_qMC_and_MCMC', 'separate_points',seperate_ratio);

[q,u]=load_points_and_solution();
n_sep=ceil(size(q,2)*separate_ratio);
    [q_train, ind] = shuffle_select_sort(q, 2, n_sep, 'fixed_randseed', seed_numb);
    u_new=u(:,:,ind);
    x_1=x(:,setdiff(1:size(x,2),ind));
    u_1=u(:,:,setdiff(1:size(x,2),ind));
    x=x_new;
    u=u_new;


n1=size(x1,2);
% Analyze surrogates from order
from_p=2;
% to order
to_p=7;
% check this many cross validations
n_v=15;
% initiate memory for solutions at validation points
%u_p=zeros(size(u_i_alpha,1),  n,  to_p-from_p+1, n_v);
%u_p1=zeros(size(u_i_alpha,1), n1, to_p-from_p+1, n_v);
seed_numb=644;
p=from_p:to_p;
norm_eps_p=zeros(length(p),5);


for j=1:n_v;
    for i=1:length(p)
        display(p(i))
        display(j)
        p_gpc=p(i);
        [V_u, u_i_alpha, ~, RVs, meas, coord, x0, u0, ~, ~, x_val, u_val]=...
            main_UQ(p_gpc, 'point_type', 'qMC_and_MCMC',...
            'separate_points',seperate_ratio, 'seed_numb', seed_numb);
        u_p=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x_val));
        u_p=reshape(u_p,size(u_val));
        % how far solutions at the validation points are from the measurement
        error_val_ij=u_val-repmat(meas,[1,1,n]);
        error_proxi_val_ij=u_p-u_val;
        for k=1:size(u1,2)
            % DNS - solution at the s samples
            error_val_ijk=squeeze(error_val_ij(:,k,:));
            % norm of error at the s samples
            norm_errors=diag(sqrt(error_val_ijk'*error_val_ijk))/norm(meas(:,k));
            % weight for the s validating sample points
            w_val=(1./norm_errors)/sum(1./norm_errors);
            % error of the proxi model
            eps_p=squeeze(error_proxi_val_ij(:,k,:));
            u_val_k=squeeze(u_val(:,k,:));
            norm_eps_p(i,k)=norm_eps_p(i,k)+w_val'*(diag(sqrt(eps_p'*eps_p))./diag(sqrt(u_val_k'*u_val_k)));
        end
    end
    seed_numb=seed_numb+10;
end
norm_eps_p=norm_eps_p/n_v;
%% Plot
sum_eps=sum(norm_eps_p');
plot(p, norm_eps_p, 'x-');
hold on
plot(p, sum_eps, 'x-', 'LineWidth', 2)
legend({'v_x', 'r_{11}',...
    'r_{22}', 'r_{33}',...
    'r_{12}', '\Sigma'});
title('Weighted error of approximation with gPCE', 'FontSize', 16)
ylabel('\epsilon', 'FontSize', 12)
xlabel('p: total polynomial degree', 'FontSize', 12)

