clear variables
%[x_b,u_b]=load_points_and_solution('point_type', 'best_point');
%[x,u]=load_points_and_solution('point_type', 'best_point_from_proxi');
%u=reshape(u, [], 1);
%u_b=reshape(u_b, [], 1);
%u_p=zeros(1800,7);
%u_p_b=zeros(1800,7);
seperate_ratio=4/5;
n_points=ceil(3417*seperate_ratio);
n_points1=ceil(4245*seperate_ratio);
from_p=2;
to_p=8;
u_p=zeros(1800, 3417-n_points,to_p-from_p+1);
u_p1=zeros(1800, 4245-n_points1,to_p-from_p+1);

for i=from_p:to_p
    display(i)
    p_gpc=i;
    %[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','default', 'separate_points',seperate_ratio);
    [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x0, u0, ~, ~, x1, u1]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','beta', 'separate_points',seperate_ratio);
    
    %u_p(:,i-1)=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x));
    %u_p_b(:,i-1)=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x_b));
    u_p(:,:,i-1)=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x1));
    [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x01, u01, ~, ~, x11, u11]=main_UQ(p_gpc, 'point_type', 'all_qmc_point', 'prior_dist','beta', 'separate_points',seperate_ratio);
    u_p1(:,:,i-1)=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(x11));
end

u_test=reshape(u_p, [], to_p-from_p+1);
u_test1=reshape(u_p1, [], to_p-from_p+1);

u1_test=u1(:);
u1_test1=u11(:);
Err_p=u_test-repmat(u1_test,1,to_p-from_p+1);
Err_p1=u_test1-repmat(u1_test1,1,to_p-from_p+1);
%Err_p=[u_p;u_p_b]-[repmat(u,1,to_p-from_p);repmat(u_b,1,to_p-from_p+1)];
Snorm = diag(sqrt(Err_p'*Err_p));
Snorm1 = diag(sqrt(Err_p1'*Err_p1));
