%% Load points and solutions there
% Load first trial of QMC sample points:
load('/home/noefried/sglib/sglib-testing-bacm/bacm/models/SFB_C4_B5_flow_porous/sample_points.mat')
% Load index of non-converging sample points:
load('/home/noefried/SFB880/flow_trhough_porous_media/SFB_project/transfer/transfer_2/nonconv_points.mat')
% Load experimental data, to which the model parameters should be fitted:
load('/home/noefried/sglib/sglib-testing-bacm/bacm/models/SFB_C4_B5_flow_porous/Exp_data.mat')
%load('/home/noefried/sglib-testing-bacm/demo/models/SFB_C4_B5_flow_porous/u_yi_j_ksample.mat')
% Load solution at the first trial of QMC sample points:
load('/home/noefried/SFB880//flow_trhough_porous_media/SFB_project/transfer/transfer_2/Simulation_data_milk_run_2.mat')

% Load solution and QMC points around two points which were close to 
% nonconverging points which was not filtered out:
load('/home/noefried/SFB880/flow_trhough_porous_media/SFB_project/transfer/list_of_nonconverging_points/non_conv_points_2414.mat')
load('/home/noefried/SFB880/flow_trhough_porous_media/SFB_project/transfer/list_of_nonconverging_points/non_conv_points_3769.mat')
load('new_points_2414');
load('new_points_3769');

% Load QMC points and solutions on the island around 'best points', and the index of the not converging points:
load('/home/noefried/sglib/sglib-testing-bacm/bacm/models/SFB_C4_B5_flow_porous/island_points.mat')
load('/home/noefried/sglib/sglib-testing-bacm/bacm/models/SFB_C4_B5_flow_porous/island_QMC_points_around_best_point.mat')
load('/home/noefried/sglib/sglib-testing-bacm/bacm/models/SFB_C4_B5_flow_porous/nonconv_island_points.mat')


%%
non_conv_points=nonconv_points';

list_of_RVs={'\beta', '\beta_T', 'c_t', 'c_{\epsilon h}', 'c_{wd}', 'c_{d p}'};
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',  'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',  'rms reynolds stress (r_{12})'};
%% Get not converging sample points
non_conv_ind=non_conv_points';
nonconv_sample_points=sample_points(:,non_conv_ind);

%% Total number of sample points
N_points=size(sample_points, 2);

%% Get converging sample points
full_ind=(1:N_points)';
conv_ind=setdiff(full_ind, non_conv_ind);
conv_sample_points=sample_points(:,conv_ind);

%% Add the points_around 2414 and 3769
conv_ind_2414=setdiff(1:730, non_conv_ind_2414);
conv_ind_3769=setdiff(1:730, non_conv_ind_3769);

conv_points_2414=points_close_to_2414(:,conv_ind_2414);
conv_points_3769=points_close_to_3769(:,conv_ind_3769);

nonconv_points_2414=points_close_to_2414(:,non_conv_ind_2414);
nonconv_points_3769=points_close_to_3769(:,non_conv_ind_3769);

%conv_sample_points_tot=[conv_sample_points, conv_points_2414, conv_points_3769];
%nonconv_sample_points_tot=[nonconv_sample_points, nonconv_points_2414, nonconv_points_3769];


%% Add the points in the island around 'best points'
conv_ind_island=setdiff(1:1500, nonconv_island_points);
conv_points_island=island_points(:,conv_ind_island);

nonconv_points_island=island_points(:,nonconv_island_points);

conv_sample_points_tot=[conv_sample_points, conv_points_2414, conv_points_3769, conv_points_island];
nonconv_sample_points_tot=[nonconv_sample_points, nonconv_points_2414, nonconv_points_3769, nonconv_points_island];

%% filter out  clear patterned nonconverging points

%Filter out only the right upper ellipsis
ind_f=(nonconv_sample_points(1,:)/6).^2+( nonconv_sample_points(2,:)-1).^2>1;
nonconv_sample_points_f=nonconv_sample_points(:,ind_f);

%Filter out the right upper ellipsis and extreme values for the last two
%parameters
ind_ff=(nonconv_sample_points(1,:)/6.5).^2+( (nonconv_sample_points(2,:)-1)/1.1).^2>1 ...
    & nonconv_sample_points(5,:)>2 ...
     & nonconv_sample_points(6,:)<0.395;
nonconv_sample_points_ff=nonconv_sample_points(:,ind_ff);

c_ind_ff=(conv_sample_points(1,:)/6.5).^2+( (conv_sample_points(2,:)-1)/1.1).^2>1 ...
    & conv_sample_points(5,:)>2 ...
     & conv_sample_points(6,:)<0.395;
 conv_points_filtered=conv_sample_points(:,c_ind_ff);

% Get the converging points which are in the filtered regions:
ind=(conv_sample_points(1,:)/6.5).^2+( (conv_sample_points(2,:)-1)/1.1).^2<1;
conv_points_filtered_out=conv_sample_points(:,ind);


% Filter only cubic region instead of the elliptic one
ind_c=~(nonconv_sample_points(1,:)>-6.5 &  nonconv_sample_points(2,:)>0.1) ...
    & nonconv_sample_points(5,:)>2 ...
     & nonconv_sample_points(6,:)<0.395;
nonconv_sample_points_c=nonconv_sample_points(:,ind_c);

c_ind_c=~(conv_sample_points(1,:)>-6.5 &  conv_sample_points(2,:)>0.1) ...
    & conv_sample_points(5,:)>2 ...
     & conv_sample_points(6,:)<0.395;
conv_sample_points_c_filtered=conv_sample_points(:,c_ind_c);
%% Calculate statistics of converging points
%Statistics from all converging sample points
i_samples=permute(Data_i(:,:, conv_ind),[3,1,2]);
u_mean=squeeze(mean(i_samples));
u_max=squeeze(max(i_samples));
u_min=squeeze(min(i_samples));
u_var=squeeze(var(i_samples));
u_quant_u=squeeze(quantile(i_samples, 0.99)); %99% quantile
u_quant_l=squeeze(quantile(i_samples, 0.01)); % 1% quantile

i_samples=permute(Data_i(:,:, conv_ind(c_ind_ff)),[3,1,2]);
u_mean_ff=squeeze(mean(i_samples));
u_var_ff=squeeze(var(i_samples));
u_max_ff=squeeze(max(i_samples));
u_min_ff=squeeze(min(i_samples));
u_quant_u_ff=squeeze(quantile(i_samples, 0.99));
u_quant_l_ff=squeeze(quantile(i_samples, 0.01));

%% Plot mean and quantiles
%Plot statistics from all converging points
plot_u_mean_quant(y_coord, u_mean, u_quant_l, u_quant_u, 'ylabels', list_of_response, 'subplot_dim',[5,1], 'transparency', 0.6)

% plot experimental data with thik line
for i=1:5
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
end

set(gcf,'NextPlot','add');
axes;
h = title('Rough estimation of mean and quantiles from QMC', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 
    
%Plot statistics from filtered converging points
plot_u_mean_quant(y_coord, u_mean_ff, u_quant_l_ff, u_quant_u_ff, 'ylabels', list_of_response, 'subplot_dim',[5,1], 'transparency', 0.6)
% plot experimental data with thik line
for i=1:5
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
end

set(gcf,'NextPlot','add');
axes;
h = title('Rough estimation of mean and quantiles from QMC (filtered samples)', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 



%% Plot mean and minmax
%Plot statistics from all converging points
plot_u_mean_quant(y_coord, u_mean, u_min, u_max, 'ylabels', list_of_response, 'subplot_dim',[5,1], 'transparency', 0.6, 'fill_color', 'cyan')

% plot experimental data with thick line
for i=1:5
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
end

set(gcf,'NextPlot','add');
axes;
h = title('Rough estimation of mean and min/max values from QMC', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 
    
%Plot statistics from filtered converging points
plot_u_mean_quant(y_coord, u_mean_ff, u_min_ff, u_max_ff, 'ylabels', list_of_response, 'subplot_dim',[5,1], 'transparency', 0.6,  'fill_color', 'cyan')
% plot experimental data with thik line
for i=1:5
    subplot(5,1,i)
    hold on
    plot(y_coord, Exp_data_yi_j(:,i),'red',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
end

set(gcf,'NextPlot','add');
axes;
h = title('Rough estimation of mean and min/max values from QMC (filtered samples)', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%% Plot matrixplots with sample points (converging, not converging, filtered)
%plot converging sample points
figure
gplotmatrix(conv_sample_points', [] ,[] ,'blue',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%plot not converging sample points
figure
gplotmatrix(nonconv_sample_points', [] ,[] ,'red',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Not converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%plot not converging sample points with eliminating the points in the right
%upper corner ellipsis
figure
gplotmatrix(nonconv_sample_points_f', [] ,[] ,'red',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Filtered (first phase) not converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%plot not converging sample points with eliminating the points in the right
%upper corner ellipsis and extreme values of the last two parameters
figure
gplotmatrix(nonconv_sample_points_ff', [] ,[] ,'red',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Filtered (second phase) not converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

% plot filtered out sample points that were converging in the right upper
% corner ellipsis
figure
gplotmatrix(conv_points_filtered_out', [] ,[] ,'blue',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Filtered out (first phase) converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%plot not converging and converging sample points with the additional runs (add points
%around 2414 and 3769
figure
gplotmatrix(nonconv_sample_points_tot', [] ,[] ,'red',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Not converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

figure
gplotmatrix(conv_sample_points_tot', [] ,[] ,'green',[],[],[],[],list_of_RVs,list_of_RVs) 
set(gcf,'NextPlot','add');
axes;
h = title('Converging sample points and histograms', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 

%% 3D plots of converging and not converging sample points
v_color=repmat([2], size(full_ind));
v_color(conv_ind)=1;
v_size=repmat([50], size(full_ind));
v_size(conv_ind)=10;
scatter3(sample_points(1,:), sample_points(2,:), sample_points(6,:), v_size, v_color, 'filled')

figure

for i=1:4
subplot (4,1,i)
scatter3(sample_points(1,:), sample_points(2,:), sample_points(i+2,:), v_size, v_color, 'filled')
xlabel(list_of_RVs{1})
ylabel(list_of_RVs{2})
zlabel(list_of_RVs{i+2})
end

for i=1:4
subplot (4,1,i)
scatter3(sample_points(5,:), sample_points(6,:), sample_points(i,:), v_size, v_color, 'filled')
xlabel(list_of_RVs{5})
ylabel(list_of_RVs{6})
zlabel(list_of_RVs{i})
end

for i=1:4
    j=[1,2,3,6];
subplot (4,1,i)
scatter3(sample_points(4,:), sample_points(5,:), sample_points(j(i),:), v_size, v_color, 'filled')
xlabel(list_of_RVs{4})
ylabel(list_of_RVs{5})
zlabel(list_of_RVs{j(i)})
end