function [x, u, ind]=load_points_and_solution(varargin)
options=varargin2options(varargin);
[point_type,options]=get_option(options, 'point_type', 'all_qmc_point');  %ALL_POINT/SCAN_POINT/SPEC_POINT/BEST_POINT/INT_POINT
[only_converging,options]=get_option(options, 'only_converging', true);
[method_ident,options]=get_option(options, 'method_ident', []);
[base_path,options]=get_option(options, 'base_path', []);
check_unsupported_options(options, mfilename);


%% Define which points/solutions/index are needed
if isempty(method_ident)
    switch point_type
        case 'all_qmc_point'
            method_ident=[1,3,4];
        case 'all_qmc_point_with_spec_point'
            method_ident=[1,3,4,5];
        case 'scan_point' % first scanning QMC points
            method_ident=1;
        case 'int_point' %Integration points
            method_ident=[6,7,8];
        case 'best_point' %'best point'
            method_ident=3;
        case 'best_point_island' %QMC points around 'best point'
            method_ident=4;
        case  'spec_point' %points around nonconverging points
            method_ident=5;
        case  'int_point_center' %integration point, middle of the L_shaped domain
            method_ident=6;
        case  'int_point_left' %integration point, middle of the L_shaped domain
            method_ident=7;
        case  'int_point_right' %integration point, middle of the L_shaped domain
            method_ident=8;
        case  'best_point_proxi' %found best point with MCMC with gPCE proxi model
            method_ident=9;
        case  'points_from_MCMC' %points for the second phase that were determined by MCMC
            method_ident=10;
        case  'qMC_and_MCMC' %scan points and MCMC points
            method_ident=[1,10];
        case  'all_qMC_and_MCMC' %scan points and MCMC points
            method_ident=[1,4,10];
    end
end
%% Define paths for points/solutions/index of converging points
if isempty(base_path)
    if isunix
        base_path='/home/noefried/SFB880/flow_trhough_porous_media/SFB_project/points_and_solutions';
    elseif ispc
        base_path='F:\noemi\Documents\SFB880\SFB_flow_over_porous_material\points_and_solutions';
    end
    end
spec_path={};
for i=method_ident
    if i==1;
        spec_path={spec_path{:},'first_scan_points'};
    elseif i==6;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_center')};
    elseif i==7;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_left')};
    elseif i==8;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_bottom')};
    elseif i==3;
        spec_path={spec_path{:},'best_point'};
    elseif i==4;
        spec_path={spec_path{:}, fullfile('best_point', 'island_around')};
    elseif i==5;
        spec_path={spec_path{:},fullfile('spec_point', '2414')};
        spec_path={spec_path{:},fullfile('spec_point', '3769')};
    elseif i==9;
        spec_path={spec_path{:},'best_point_with_proxi'};
    elseif i==10;
        spec_path={spec_path{:},'second_phase_points'};
    end
end
%%
%% Load points/solutions/index
x=[];
u=[];
ind=true(0,0);
for j=1:length(spec_path)
    file_name_j=fullfile(base_path, spec_path{j}, 'points_and_sol.mat');
    S_j=load(file_name_j);
    x_j=S_j.x;
    N_j=size(x_j,2);
    if isfield(S_j, 'u')
        u_j=S_j.u;
    else
        u_j=NaN([360,5,N_j]);
    end
    if isfield(S_j, 'nonconv_ind')
        ind_j=true(1,N_j);
        ind_j(S_j.nonconv_ind)=false;
        
    else
        ind_j=true(1,N_j);
    end
    x=[x, x_j];
    u=cat(3, u, u_j);
    ind=[ind,ind_j];
end
if only_converging
    x=x(:,ind);
    u=u(:,:,ind);
end



