%% UPDATE
clear
gpc_registry('reset')
flag_cancel_nonconvergence_gap_probability=true;
point_type= 'scan_point';
%% Caculation of the forward method
%gPCE order
    p_gpc=5;
%Calculate the forward model
    [V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', point_type, 'prior_dist','default');
    %[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','beta');
    %[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', 'best_point_island', 'prior_dist','island_uni');
    n_meas=20;
    meas_coord=rand(1,n_meas)*2;
    [~, z_ind]=min(abs(repmat(meas_coord, length(coord),1)-repmat(coord,1,n_meas)));
    
    syschars=V_u{1};
    n_out=size(u_mean,2);
%define coordinates of the measurements
   

    %z_ind=[90,100,125,140,150,170,180,200,230,240,260,280];
    %z_ind=randi([1,length(coord)],1,12);

    %z_ind=1:360;
%outputs to update for
    n_out_ind=(1:5)';
    %n_out_ind=[1;5];
    %n_out_ind=[2;3;4;5];
    ind=sub2ind(size(u_mean), repmat(z_ind,length(n_out_ind),1), repmat(n_out_ind,1,length(z_ind)));
    ind=ind(:);
    list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',  'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',  'rms reynolds stress (r_{12})'};

%% Initialize update
% Measurement
    Z=(meas(z_ind,n_out_ind))';
% variance of the masurement errors (gassian curve with center at interface
% +3%)

 perc_sigma=(3*ones(size(coord))+2*exp(-(coord-0.9).^2./(2*(0.1/3)^2)));

 err_sigma=binfun(@times,  max(abs(meas)).*[1,2,2,2,2], perc_sigma/200)*2.5;

  err_var=(err_sigma).^2;
 err_vars=err_var(z_ind, n_out_ind)';

%err_vars=u_var(z_ind, n_out_ind)'/100;
%err_vars(5,:)=err_vars(5,:);

% prior distribution (with prior dist of the UQ)
    n_germ=gpcbasis_size(V_u,2);
    prior_dist=cell(n_germ, 1);
    for i=1:n_germ
        prior_dist{i}=polysys_dist(syschars(i));
    end
    prior_dist_xi=gendist2object(prior_dist);

% Observation operator
    observ_operator=@(xi) gpc_evaluate(u_i_alpha(ind,:), V_u, xi);
 
    observ_operator=@(xi1,xi2) (gpc_evaluate(u_i_alpha(ind,:), V_u, xi)+;
%% Best point from model and best point from proxi model
[x_best, u_best]=load_points_and_solution('point_type', 'best_point');
    germ_at_best=RVs.params2germ(x_best);
    u_proxi_at_best=gpc_evaluate(u_i_alpha, V_u, germ_at_best);
    u_proxi_at_best=reshape(u_proxi_at_best, [], n_out);
    
%% Call classifyer for nonconvergence gap definition
if flag_cancel_nonconvergence_gap_probability
    flag_converge_func=get_flag_converge_func();
else
    flag_converge_func={};
end
%% update with MCMC
    N=8000;
%samples_post=gpc_MCMC(N, observ_operator, prior_dist_xi, Z(:), err_vars(:), 'start_point', germ_at_best);
%[samples_post, acc_rate]=gpc_MCMC(N, observ_operator, prior_dist_xi, Z(:), err_vars(:), 'cancel_prior_prob_func', flag_converge_func);
[samples_post, acc_rate]=gpc_MCMC(N, observ_operator, prior_dist_xi, Z(:), err_vars(:),'start_point',germ_at_best, 'cancel_prior_prob_func', flag_converge_func);


samples_post_param=RVs.germ2params(samples_post);
x_new=[x,samples_post_param, x_best];


%% Plot MCMC results
gplotmatrix(samples_post_param', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);

figure
group=[ones(size(x,2),1);2*ones(size(samples_post_param,2),1); 3];
gplotmatrix(x_new', [] ,group ,'bmg','.ox',[],[],[],RVs.param_plot_names,RVs.param_plot_names);

u_upd=gpc_evaluate(u_i_alpha, V_u, samples_post);
u_mean_u=reshape(mean(u_upd,2),[],n_out);
u_var_u=reshape(var(u_upd,[],2), [],n_out);
MCMC_xi_mean=mean(samples_post,2);
u_with_updated_param_mean=gpc_evaluate(u_i_alpha, V_u, MCMC_xi_mean);
u_newbest=reshape(u_with_updated_param_mean,size(u_mean));

figure;
plot_u_mean_var(coord, u_mean_u, u_var_u, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels',list_of_response, 'subplot_dim', [5,1])

%Choose closest among the points

u_MCMC=gpc_evaluate(u_i_alpha, V_u, samples_post);
normofdiff=zeros(1,N);
ERR=u_MCMC-repmat(meas(:),1,N);
for i=1:N
normofdiff(i) = norm(ERR(:,i));
end
[mindiff, minind]=min(normofdiff);
best_upd_germ=samples_post(:,minind);
best_upd_u=gpc_evaluate(u_i_alpha, V_u, best_upd_germ);
u_bestbest=reshape(best_upd_u,[],n_out);
figure
for i=1:n_out
    %subplot(5,1,i)
    figure
    hold on
    plot(coord, meas(:,i),'red',  'LineWidth', 2)
    %plot(coord, u_newbest(:,i), 'c')
    plot(coord(z_ind),zeros(size(z_ind)), 'x') 
    plot(coord, u_best(:,i), 'b')
    plot(coord, u_bestbest(:,i),'b',  'LineWidth', 2)
    xlabel('y', 'FontSize', 12)
    legend('Measurement', 'u_best', 'u with best norm')
end
figure
plot(samples_post')
