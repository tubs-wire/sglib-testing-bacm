function missclass1=find_conv_boundary_by_vector_maschine()
%[x, ~, ind]=load_points_and_solution('only_converging', false, 'point_type', 'all_qmc_point');
[x, ~, ind]=load_points_and_solution('only_converging', false, 'method_ident', [1,3,4,5,10]);


% [~, x_s, ind_s]=shuffle(x, ind);
% sepind=ceil(size(x,2)*2/3);
% x=x_s(:,1:sepind);
% ind=ind_s(:,:,1:sepind);
% x_check=x_s(:,sepind+1:end);
% ind_check=ind_s(:,:,sepind+1:end);
%% Compare different kernel functions

kernell_funcs={'RBF', 'polynomial', 'polynomial', 'polynomial', 'mlp' };
n_k=length(kernell_funcs);
%dontfit=zeros(n_k, 2);
misclass1=zeros(n_k, 2);
svm_objects=cell(n_k,2);
n_support_vects=zeros(n_k, 2);
for i=1:n_k
    display(i)
    % Train the SVM classifier
    switch kernell_funcs{i}
        case 'polynomial'
            p=i;
            SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction',kernell_funcs{i},...
                'PolynomialOrder', p, 'BoxConstraint', Inf,...
                'KernelScale','auto', 'ClassNames', [false, true]);
        otherwise
            SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction',kernell_funcs{i},...
                'KernelScale','auto', 'ClassNames', [false, true], 'BoxConstraint', Inf);
    end
    svm_objects{i,1}=SVMModel;
    CVSVModel=crossval(SVMModel);
    misclass1(i,1)=kfoldLoss(CVSVModel);
    n_support_vects(i,1)=length(SVMModel.Alpha);
    
    switch kernell_funcs{i}
        case 'polynomial'
            p=i;
            SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction',kernell_funcs{i},...
                'PolynomialOrder', p, ...
                'KernelScale','auto', 'ClassNames', [false, true]);
        otherwise
            SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction',kernell_funcs{i},...
                'KernelScale','auto', 'ClassNames', [false, true]);
    end
    svm_objects{i,2}=SVMModel;
    CVSVModel=crossval(SVMModel);
    misclass1(i,2)=kfoldLoss(CVSVModel);
    n_support_vects(i,2)=length(SVMModel.Alpha);
end
SVMModel = fitcsvm(x',ind','Standardize',true,'KernelFunction','RBF',...
    'KernelScale','auto','BoxConstraint',Inf, 'ClassNames', [false, true]);

%% Plot best separation
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x0, u0]=...
    main_UQ(5, 'point_type', 'scan_point');

Q_sample=RVs.sample(10000);
[label, scores]=predict(SVMModel, Q_sample');

%gscatter(x(1,:),x(2,:),ind);
%gscatter(Q_sample(1,:),Q_sample(2,:),label');

gplotmatrix(Q_sample',[],  label)
end
function [ind, x_shuffled, u_shuffled]=shuffle(x,u)
rng(1,'twister');
ind=randperm(size(x,2));

x_shuffled=x(:,ind);
u_shuffled=u(:,:,ind);
end