function [h, f]=plot_gPCE_cross_validation_result(gPCE, varargin)

options=varargin2options(varargin);
[plot_sep_errs,options]=get_option(options, 'plot_sep_errs', true);
[file_identifyer, options]=get_option(options, 'file_identifyer', '');
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
[str_legend, options]=get_option(options, 'legend', {});
[f, options]=get_option(options, 'figer_handle', []);
check_unsupported_options(options, mfilename);

%% open figure, or activate F
if isempty(f)
    f=figure();
else
    figure(f);
end


%% Plot
p=gPCE.order;
h(1)=plot(p, gPCE.rel_err, 'x-b', 'LineWidth', 2);
hold on
if plot_sep_errs
h(2:6)=plot(p, squeeze(gPCE.rel_err_i),'-', 'LineWidth', 1);
end
title('Weighted error of approximation with gPCE', 'FontSize', 14)
ylabel('\epsilon (relative error)', 'FontSize', 12)
xlabel('p: total polynomial degree', 'FontSize', 12)
if ~isempty(str_legend)
    legend(str_legend, 'FontSize', 12);
end

if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    % add artificialy the filename PHASE 1/2 that directory later for the
    % figure
    this_f_name=fullfile(mfilename('fullpath'), file_identifyer);
    % filename for fig
    filename=get_save_path(base_path, this_f_name, 'compare_gPCE_degrees.fig', 3);
    % save figure
    savefig(f, filename)
    % filename for fig (for png extension)
    filename=get_save_path(base_path, this_f_name, 'compare_gPCE_degrees.png', 3);
    % save  fig 1
    saveas(f, filename)
end

end
