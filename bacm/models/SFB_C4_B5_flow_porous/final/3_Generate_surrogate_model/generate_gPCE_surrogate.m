function [V_u, u_i_alpha, params2germs_func, germs2param_func]=generate_gPCE_surrogate(q, u, p_gpc, Q, u_ref, x, varargin)

options=varargin2options(varargin);
[flag_PCE, options]=get_option(options, 'flag_PCE',false);
[wp, options]=get_option(options, 'weight_pow',1);
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting',[]);
[weighted_regr, options]=get_option(options, 'weighted_regr',false);
check_unsupported_options(options, mfilename);

%% Generate gPCE basis for the proxi model

% Define approximating subspace
gpc_full_tensor=false; % complete or full tensor gpc basis
if flag_PCE %if gaussian germs are needed
    V_u=gpcbasis_create(repmat('h',1,Q.num_params),'p',p_gpc, 'full_tensor', gpc_full_tensor);
else
    V_u=Q.get_germ;
    V_u=gpcbasis_modify(V_u, 'p',p_gpc, 'full_tensor', gpc_full_tensor);
end

%% Map parameters to reference coordinate
%Define mapping from RVs to germs (standardly distributed random variables)
if flag_PCE
    params2germs_func=@(q)Q.params2stdnor(q);
    germs2param_func=@(xi)Q.stdnor2params(xi);
else
    params2germs_func=@(q)Q.params2germ(q);
    germs2param_func=@(xi)Q.germ2params(xi);
end
x_ref=params2germs_func(q);

%% II. Get gPCE coeffs with regression
if length(size(u))==3
    u_ij=reshape(u, size(u,1)*size(u,2), []);
else
    u_ij=u;
end
A=gpcbasis_evaluate(V_u, x_ref);
if ~weighted_regr
    u_i_alpha = u_ij/A;
else
    %for weighted regression
    %Get the weights
    rel_err=get_rel_deviation_from_meas(u, u_ref, x, 'ignore_indices', ind_ign);
    W = diag(1./rel_err.^wp)/sum(1./rel_err.^wp);
    u_i_alpha =((A*W*A')\(A*W*u_ij'))';
end

