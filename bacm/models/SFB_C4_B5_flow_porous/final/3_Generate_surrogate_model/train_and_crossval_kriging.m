function krig_models=train_and_crossval_kriging(q, u, n_s, K, x, u_ref, varargin)
%   Noemi Friedman
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting', []);
[weighted_error, options]=get_option(options, 'weighted_error', false);
check_unsupported_options(options, mfilename);

% q: parameter values (design sites)
% u: responses there
% ns:ratio #train points/# validating points
% K: K-fold cross validation
% x: coordinate of u

% Fix seed number
seed_numb=644;
% Number of training points
n_t=ceil(size(q,2)*n_s);
% dimension in q corresponding to the samples (sites)
dim_q=2;

% Regression and correlation functions used for for kriging
regr=@regpoly0;
corr_func={@corrgauss, @correxp};
n_c=length(corr_func);

% Initialize memory
rel_errs=zeros(n_c,K);
rel_errs_i=zeros(n_c,5, K);
krig_model=cell(K,n_c);
krig_infos=cell(K,n_c);
% k-validation
for k=1:K
    
    % partition q and u to training and validation sets
    [q_t, ind_t, ind_v] = ...
        shuffle_select_sort(q, dim_q, n_t, 'fixed_randseed', seed_numb);
    % Training responses
    u_t=u(:,ind_t)';
    % Validation points and responses
    q_t=q_t';
    q_v = q(:,ind_v)';
    u_v = u(:,ind_v)';
    [q_t, ind_u]=unique(q_t, 'rows');
    u_t=u_t(ind_u, :);
    
    for i=1:n_c
        display(strvarexpand('$i$/$n_c$'))
        display(strvarexpand('$k$/$K$'))
        corr_func_i=corr_func{i};
        [krig_model{k,i}, krig_infos{k,i}]=dacefit(q_t, u_t, regr, corr_func_i, 0.5*ones(6,1), 0.1*ones(6,1),2*ones(6,1));
        
        [rel_err, rel_err_i]=error_of_krig(krig_model{k,i}, q_v, u_v, u_ref, x, ind_ign, weighted_error);
        rel_errs(i,k)=rel_err;
        rel_errs_i(i,:,k)=rel_err_i;
    end
    seed_numb=seed_numb+10;
end
%% Structure result
% Mean errors
rel_errs_m=mean(rel_errs,2);
rel_errs_mi=mean(rel_errs_i,3);
% Optimal proxi model
[min_relerr, ind]=min(rel_errs_m);

% Put the results in a structure
krig_models.krig_models=krig_model;
krig_models.krig_infos=krig_infos;
krig_models.corr_funcs=corr_func;
krig_models.rel_err=rel_errs_m;
krig_models.rel_err_i=rel_errs_mi();
krig_models.opt.rel_err=min_relerr;
krig_models.opt.corr_func=corr_func{ind};
end

function [mean_err, mean_err_i]=error_of_krig(krig_model, q_v, u_v, u_ref, x, ind_ign, weighted_error)
% response with surrogate
u_p=predictor(q_v, krig_model);
% reshape to original form (with n_y x n_out x N_val_points)
u_p=reshape(u_p', size(u_ref,1), size(u_ref,2), []);
u_v=reshape(u_v', size(u_ref,1), size(u_ref,2), []);
% diff between responses from surrogate and High Fidelity Model
[rel_err, rel_err_i]=get_rel_deviation_from_meas(u_p, u_v, x, 'ignore_indices',ind_ign);
rel_err_i=squeeze(rel_err_i);
if weighted_error
    % diff between response and the optimal reference solution
    dev_ref=get_rel_deviation_from_meas(u_v, u_ref, x,  'ignore_indices',ind_ign);
    % weight error in accordance with dinstance from optimal solution
    w=1./dev_ref;
    w=w/sum(w);
    mean_err=w'*rel_err;
    mean_err_i=rel_err_i*w;
else
    mean_err=mean(rel_err);
    mean_err_i=mean(rel_err_i, 2);
end
end
