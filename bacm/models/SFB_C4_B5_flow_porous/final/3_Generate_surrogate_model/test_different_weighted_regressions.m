function [gPCE, h]=test_different_weighted_regressions(Q, q, u, K, p, n_s, u_ref, x, ign_ind)

str_pce={'PCE', 'gPCE'};
flag_pce=[false, true];
gPCE_options.ignored_indices_for_weighting=ign_ind;
for i=1:2
    for wp=0:2
        gPCE_options.flag_PCE=flag_pce(i);
        gPCE_options.weight_pow=wp;
        % file identifyer for saving the results in a distinguished folder
        file_ident=strvarexpand('$str_pce{i}$_with_weightpow$wp$');
        gPCE_ij =train_and_cross_val_gPCEs(Q, q, u, K, p, n_s, u_ref, x,...
            'base_path', 'default',...
            'save_results', true,'file_identifyer', file_ident,...
            'ignored_indices_for_weighting',ign_ind,... 
            'gPCE_options', gPCE_options);
        gPCE.(file_ident) =gPCE_ij;
        
    end
end
%% Plot the result
idents=fieldnames(gPCE);
h=[];

for i=1:length(idents)
gPCE_i=gPCE.(idents{i});
if i==1
[h1,f]=plot_gPCE_cross_validation_result(gPCE_i, 'plot_sep_errs', false);
        h=[h(:);h1(:)];
else
    [h1]=plot_gPCE_cross_validation_result(gPCE_i, 'plot_sep_errs', false, 'figer_handle', f);
        h=[h(:);h1(:)];
end
end
color='rbcrbc';
lstyle={'-', '-', '-', '-.', '-.', '-.'};
mark_i={'x', 'x', 'x', 'o', 'o', 'o'};
legend_i={'gPCE without weight', 'gPCE w=1/\epsilon', 'gPCE w=1/\epsilon^2',...
    'PCE without weight', 'PCE w=1/\epsilon', 'PCE w=1/\epsilon^2'};
for i=1:length(h)
  h_i=h(i);
  h_i.Color=color(i);
  h_i.LineWidth=2;
  h_i.LineStyle=lstyle{i};
  h_i.Marker=mark_i{i};
  h_i.DisplayName=legend_i{i};
end

legend('show')
