function gPCE  =train_and_cross_val_gPCEs_low_rank(Q, q, u, K, p, n_s, u_ref, x, varargin)
% This function cross validates the differen order gPCEs
% Example:
% TRAIN_AND_CROSS_VAL_GPCES...
% (simparamset, q_points, u, K-fold, P_gpc, ratiotrain/val, U_REF, coord)
% inputs:
%
%
%
%
%   Noemi Friedman
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[file_identifyer, options]=get_option(options, 'file_identifyer', '');
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting', []);
check_unsupported_options(options, mfilename);

% Q: prior paramset
% q: simulation parameter values
% u: model solutions at q (n_d x n_var xN
% K: k-fold cross validation
% p: Order of gPCEs
% n_s: separate ration (# training samples/# validating samples)

%% choose training and validating set

% Fix seed number
%seed_numb=644;

% Number of training points
n_t=ceil(size(q,2)*n_s);

% If u is not 3 dimensional
if length(size(u))==2
   u= reshape(u, [size(u,1), 1, size(u,2)]);
elseif length(size(u))==1
   u= reshape(u, [1, 1, length(u)]);
end
    
%% Cross validate
% Initialize memory
rel_errs=zeros(length(p),K);
rel_errs_i=zeros(length(p),size(u,2), K);
% k-validation
for k=1:K
    % Get index of training and validating set and the training points (q_t)
    dim_q=2;
    % [q_t, ind_t, ind_v] = ...
    %    shuffle_select_sort(q, dim_q, n_t, 'fixed_randseed', seed_numb);
    [q_t, ind_t, ind_v] = ...
        shuffle_select_sort(q, dim_q, n_t);
    % Training responses
    u_t=u(:,:,ind_t);
    % Validation points and responses
    q_v = q(:,ind_v);
    u_v = u(:,:,ind_v);
    for i=1:length(p)
        display(p(i))
        display(k)
        [u_tn, W, lambda, u2u_n, un2u]=low_rank_transform(reshape(u_t, [], size(u_t, 3)));
        [V_u, u_i_alpha, q2germ_func]=...
            generate_gPCE_surrogate(q_t, u_tn, p(i), Q, [], x, ...
            gPCE_options{:});
        [rel_err, rel_err_i]=error_of_gpc(u_i_alpha, V_u, q_v, u_v, q2germ_func, u_ref, x, ind_ign, false, 'map_u', un2u);
        rel_errs(i,k)=rel_err;
        rel_errs_i(i,:,k)=rel_err_i;
    end
    %seed_numb=seed_numb+10;
end
%% Structure result
% Mean errors
rel_errs_m=mean(rel_errs,2);
rel_errs_mi=mean(rel_errs_i,3);
% Optimal proxi model
[min_relerr_gPCE, ind_gPCE]=min(rel_errs_m);

% Put the results in a structure
gPCE.order=p;
gPCE.germ=V_u{1};
gPCE.rel_err=rel_errs_m;
gPCE.rel_err_i=rel_errs_mi();
gPCE.opt.rel_err=min_relerr_gPCE;
gPCE.opt.order=p(ind_gPCE);

%% Save results

if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    % add artificialy the filename PHASE 1/2 that directory later for the
    % results
    this_f_name=fullfile(mfilename('fullpath'), file_identifyer);
    % filename for results
    filename=get_save_path(base_path, this_f_name, 'Results.mat', 3);
    % save results
    save(filename, 'gPCE');
    % filename for fig
    %     filename=get_save_path(base_path, this_f_name, 'crossvalidation_of_surrogates.fig', 3);
    %     % save two figures together
    %     savefig(f, filename)
    %     % filename for fig (for png extension)
    %     filename=get_save_path(base_path,this_f_name, 'crossvalidation_of_surrogates.png', 3);
    %     % save  fig 1
    %     saveas(f, filename)
end

end
function [mean_err, mean_err_i]=error_of_gpc(u_i_alpha, V_u, q_v, u_v, q2germ_func, u_ref, x, ind_ign, weighted_error, varargin)
options=varargin2options(varargin);
[map_u, options]=get_option(options, 'map_u', @(u)(u));
check_unsupported_options(options, mfilename);
% set dimensions
n_var=size(u_v,2);
N=size(u_v,3);
% response with surrogate
u_p=gpc_evaluate(u_i_alpha, V_u, q2germ_func(q_v));
u_p=map_u(u_p);
u_p=reshape(u_p,size(u_v));
% diff between responses from surrogate and High Fidelity Model
[rel_err, rel_err_i]=get_rel_deviation_from_meas(u_p, u_v, x, 'ignore_indices',ind_ign);

rel_err_i=reshape(rel_err_i, n_var, N);

if weighted_error
    % diff between response and the optimal reference solution
    dev_ref=get_rel_deviation_from_meas(u_v, u_ref, x,  'ignore_indices',ind_ign);
    % weight error in accordance with dinstance from optimal solution
    w=1./dev_ref;
    w=w/sum(w);
    mean_err=w'*rel_err;
    mean_err_i=rel_err_i*w;
else
    mean_err=mean(rel_err);
    mean_err_i=mean(rel_err_i, 2);
end
end





