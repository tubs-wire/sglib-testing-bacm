function [ltab, T]=latex_table_compare_SVM_Kernel_funcs(svm_objects, misclass, phase_number, varargin)

%% Optional input

options=varargin2options(varargin);
[base_path, options]=get_option(options, 'base_path', 'default');
[save_table, options]=get_option(options, 'save_table', true);
check_unsupported_options(options, mfilename);

%%
n=length(svm_objects(:));

%% initiate memory
kernel=cell(n,1);
scaling=cell(n,1);
order=cell(n,1);
convergence=cell(n,1);
nsupportvectors=cell(n,1);
softmargin=cell(n,1);
misclass=misclass(:);

%% Store data in matlab table
for i=1:n
    svm_i=svm_objects{i};
    kernel{i}=svm_i.KernelParameters.Function;
    if isfield(svm_i.KernelParameters, 'Order');
        order{i}=num2str(svm_i.KernelParameters.Order, '%i');
    else
        order{i}='-';
    end
    scaling{i}=num2str(svm_i.KernelParameters.Scale, '%.3f');
    if svm_i.ConvergenceInfo.Converged
    convergence{i}='yes';
    else
        convergence{i}='no';
    end
    nsupportvectors{i}=num2str(length(svm_i.Alpha), '%i');
end
    [~, jj]=ind2sub(size(svm_objects), 1:n);
    softmargin(jj<2)={'no'};
    softmargin(jj~=1)={'yes'};
    
    misclass=strcat(  num2str(misclass*100, '%.2f'), '\%');
    misclass=mat2cell(misclass, ones(n,1));
T = table(kernel,order, scaling,convergence,...
    nsupportvectors, softmargin, misclass);

%% Make latex table

input.data = T;

% Column alignment ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';

% Switch table borders on/off:
input.tableBorders = 0;

% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 0;

% LaTex table caption:
input.tableCaption = strvarexpand('Optimized parameters of different kernel functions for classification, convergence of SVM and misclassification rate from crossvalidation, phase $phase_number$');

% LaTex table label:
input.tableLabel = strvarexpand('Compare_kernels$phase_number$');

% Now call the function to generate LaTex code:
ltab = latexTable(input);

%% Save results

% set base path
if save_table
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    if nargin>2 && ~isempty(phase_number)
        % add artificialy the filename PHASE 1/2 that directory later for the
        % results
        this_f_name=fullfile(mfilename('fullpath'), strvarexpand('phase$phase_number$'));
        n_folder=3;
    else
        this_f_name=mfilename('fullpath');
        n_folder=2;
    end
    % filename for matlab table
    filename=get_save_path(base_path, this_f_name, 'matlab_table', n_folder);
    % save matlab table
    save(filename, 'T')
    display(strvarexpand('saving table to: $filename$'))
    % filename for latex table
    filename=get_save_path(base_path,this_f_name, 'latex_table', n_folder);
    % save  latex table
    save(filename, 'ltab')
    display(strvarexpand('saving LATEX table to: $filename$'))
end
