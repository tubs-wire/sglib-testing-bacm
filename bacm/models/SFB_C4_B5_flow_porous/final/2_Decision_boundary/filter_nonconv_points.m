function q_filtered=filter_nonconv_points(q, q_classifyer)

ind=q_classifyer(q);
q_filtered=q(:,ind);