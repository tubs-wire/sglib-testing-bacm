function h=plot_prediction_of_random_samples(Q, SVMModel, phase_number, varargin)

options=varargin2options(varargin);
[base_path, options]=get_option(options, 'base_path', 'default');
[save_fig, options]=get_option(options, 'save_fig', true);
check_unsupported_options(options, mfilename);

%% Plot prediction with best separation
% Sample
Q_sample=Q.sample(10000);
% Classify
[label, ~]=predict(SVMModel, Q_sample');
ind = label==1;
% Plot prediction
h(1)=figure;
plot_grouped_scatter({Q_sample(:,ind),Q_sample(:,~ind)}, ...
    'MarkerSize', [4,4],'Color', 'gr','MarkerType','..',...
    'Legends', {'Converging points', 'Not converging points'}, ...
    'Labels', Q.param_plot_names,...
    'LineType', {'-', '--'}, 'LineWidth', [2,2],...
    'title', 'Prediction of classification by the Kernel trick')

%gplotmatrix(Q_sample',[],  label)
h(2)=figure;
plot_3d_points({Q_sample(:,ind),Q_sample(:,~ind)}, ...
    [1,2,6],...
    'MarkerSize', [10, 10], 'Color', 'gr','MarkerType','..',...
    'Legends', {'Converging points', 'Not converging points'}, ...
    'Labels', {Q.param_plot_names{[1,2,6]}},...
    'Title', 'Prediction of classification by the Kernel trick');

%% Save figures

% set base path
if save_fig
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    if nargin>2 && ~isempty(phase_number)
        % add artificialy the filename PHASE 1/2 that directory later for the
        % results
        this_f_name=fullfile(mfilename('fullpath'), strvarexpand('phase$phase_number$'));
        n_folder=3;
    else
        this_f_name=mfilename('fullpath');
        n_folder=2;
    end
    % filename for figs (the two figures are save together)
    filename=get_save_path(base_path, this_f_name, 'SVM_best_prediction.fig', n_folder);
    % save two figures together
    savefig(h, filename)
    % filename for fig 1 (for png extension)
    filename=get_save_path(base_path,this_f_name, 'SVM_best_prediction.png', n_folder);
    % save  fig 1
    saveas(h(1), filename)
    % filename for fig 2 (for png extension)
    filename=get_save_path(base_path, this_f_name, 'SVM_best_prediction2.png', n_folder);
    % save  fig 2
    saveas(h(2), filename)
end
