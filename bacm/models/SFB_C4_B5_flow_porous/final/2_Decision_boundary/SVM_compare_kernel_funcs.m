function [SVMModel, svm_objects, misclass]=...
    SVM_compare_kernel_funcs(q, ind, varargin)
%
% SVM_COMPARE_KERNEL_FUNCS
% Example: SVM_COMPARE_KERNEL_FUNCS(POINTS, CLASSIFYER)
%
% compares decision boundaries by partitioning the samples to training and
% validation points, and checking percentage of missclassified points.
% The output stores the SVM OBJECT and the vector with the
% missclassification percentage.
%
% Optional inputs:
%
% -PLOT_PREDICTION is a flag to switch off (default) or on the plot of
% new sample points and the prediction of their classification
%
% -SAVE_RESULTS is a flag to save (SAVE_RESULTS=true) or
%   not (SAVE_RESULTS=false) (default is false) the output variables, the
%   figures, and the data for the figures
%
% -BASE_PATH
%
% Outputs:
% - SVM_model: optimal classifyer (SVM object)
%
% -SVM objects: in a cell first column is without soft margins and 
%  second with aoutomatic setup (BoxConstrain=1). The rows are
%  corresponding to different Kernel functions:
%      1: RBF (Gaussian Kernel)
%      2: Polynomial (d=2)
%      3: Polynomial (d=3)
%      4: Polynomial (d=4)
%      5: Polynomial (d=5)
%
% -Misclass: is the ratio of avarage misclassification with the kernels
% corresponding to the kernel objects above
% 
% -n_support_vects: number of support vectors
%
% -Convergence info: whether the optimization in the SVM was converging
%
%   Noemi Friedman
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[parallel_comp, options]=get_option(options, 'parallel_comp', false);
check_unsupported_options(options, mfilename);

%% Compare different kernel functions

% Define type of Kernel functions that should be checked
kernell_funcs={'RBF', 'polynomial', 'polynomial', 'polynomial', 'polynomial'};
n_k=length(kernell_funcs);

%% Initiate memory

% Initiate memory 
svm_objects=cell(n_k,2);
misclass=zeros(n_k,2);

%% SVM training with all the data and crossvalidating

if parallel_comp
    parfor i=1:n_k
        display(strvarexpand('$i$/$n_k$'))
        [svm_objects_i, misclass_i]=...
            train_and_valid(q,ind,kernell_funcs{i}, i);
        svm_objects(i,:)=svm_objects_i;
        misclass(i,:)=misclass_i;
    end
else
    for i=1:n_k
        display(strvarexpand('$i$/$n_k$'))
        [svm_objects_i, misclass_i]=...
            train_and_valid(q,ind,kernell_funcs{i}, i);
        svm_objects(i,:)=svm_objects_i;
        misclass(i,:)=misclass_i;
    end
end

%% Check beest classifyer

[~, ind]=min(misclass(:));
[indi, indj]=ind2sub(size(misclass),ind);
SVMModel=svm_objects{indi, indj};
display(SVMModel);

end

function [svm_objects_i, misclass_i]...
    =train_and_valid(x,ind,kernell_funcs_i, i)

% Train the SVM classifier without soft margines
switch kernell_funcs_i
    case 'polynomial'
        p=i;
        svm_objects1_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'PolynomialOrder', p, 'BoxConstraint', Inf,...
            'KernelScale','auto', 'ClassNames', [false, true]);
    otherwise
        svm_objects1_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'KernelScale','auto', 'ClassNames', [false, true],...
            'BoxConstraint', Inf);
end

% Crossvalidate
CVSVModel=crossval(svm_objects1_i);

% Save missclassification
misclass1_i=kfoldLoss(CVSVModel);
% Train the SVM classifier with soft margines (Boxconstrain=1)
switch kernell_funcs_i
    case 'polynomial'
        p=i;
        svm_objects2_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'PolynomialOrder', p, ...
            'KernelScale','auto', 'ClassNames', [false, true]);
    otherwise
        svm_objects2_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'KernelScale','auto', 'ClassNames', [false, true]);
end

% Cross validate
CVSVModel=crossval(svm_objects2_i);

% Save missclassification and number of support vectors
misclass2_i=kfoldLoss(CVSVModel);
misclass_i=[misclass1_i,misclass2_i];
svm_objects_i={svm_objects1_i, svm_objects2_i};
end