function plot_decision_boundary_by_hand()
% PLOT_DECISION_BOUNDARY_BY_HAND
% attempts to find a decision boundary by hand separating converging and
% not converging points. Unfortunately there are too many converging points
% in the wrong side of the decision boundary.
% The function
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



%% load points and index of converging points (ind=true -> converging)

[q, ~, ind]=load_points_and_solution('only_converging', false,...
    'method_ident', 1);
%number of converging points
n_conv=sum(ind);
% load prior paramset (only the name of the params are used here)
RVs=set_prior_paramset();


%% set colors and point size for plot
% indices with extreme values
ind_t=~(q(5,:)>2 &q(6,:)<0.395);
ind_t(ind)=false;
% indices of not converging points that does not have extreme values
ind_n= ~(ind_t | ind);

%% Plot points

plot3(q(1,ind), q(2,ind), q(6,ind), '.', 'Color', 'g', 'MarkerSize', 5)
hold on
plot3(q(1,ind_t), q(2,ind_t), q(6,ind_t), '.', 'Color', 'k', 'MarkerSize', 10)
plot3(q(1,ind_n), q(2,ind_n), q(6,ind_n), '.', 'Color', 'r', 'MarkerSize', 10)
xlabel(RVs.param_plot_names{1})
ylabel(RVs.param_plot_names{2})
zlabel(RVs.param_plot_names{3})

%% Plot decision boundary

% plot ellipsoid
ax=linspace(-10,0,30);
ay=linspace(-1, 1, 30);
[X,Y]=meshgrid(ax,ay);
Z=-sqrt( 1.1-(X/6.5).^2-( (Y-1)/1.1).^2)*0.6+0.4;
h=surf(X,Y,real(Z));
h.FaceAlpha=0.6;
%plot cyllinder
% [X_c,Y_c,Z_c] = cylinder(1,50);
% X_c=X_c*6.5;
% Y_c=Y_c*1.1+1;
% Z_c(2,:)=0.4;
% ind_plot=X_c(1,:)>-10 & X_c(1,:)<0 & Y_c(1,:)>-1 & Y_c(1,:)<1;
% surf(X_c(:,ind_plot), Y_c(:,ind_plot), Z_c(:,ind_plot))

%set axis limits and label
xlim([-10,0])
ylim([-1,1])
zlim([0, 0.4])
xlabel(RVs.param_plot_names{1})
ylabel(RVs.param_plot_names{2})
zlabel(RVs.param_plot_names{6})

%% Check number of problematic point
%(that are not converging and does not have extreme values for some parameters)

% check number of nonconverging points on the wrong side of the boundary
nonconv_points=q(:,~ind);
ind_problem=(q(1,~ind)/6.5).^2+( (q(2,~ind)-1)/1.1).^2>1 ...
    & q(5,~ind)>2 & q(6,~ind) < 0.395;
 sum(ind_problem)
 display(strvarexpand(...
     'The number of the not converging points on thw wrong side of the boundary: $sum(ind_problem)$'))
 % check number of converging points on the wrong side of the boundary
 
 ind_droped=~( (q(1,ind)/6.5).^2+( (q(2,ind)-1)/1.1).^2>1 ...
    & q(5,ind)>2 & q(6,ind)<0.395);
 display(strvarexpand(...
     'The number of the converging points on thw wrong side of the boundary: $sum(ind_droped)$'))
 