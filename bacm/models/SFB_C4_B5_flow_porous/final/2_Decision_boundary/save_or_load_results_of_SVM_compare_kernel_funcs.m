function [SVM_model, svm_objects, misclass]= ...
    save_or_load_results_of_SVM_compare_kernel_funcs...
    (action, phase_number,...
    SVM_model, svm_objects, misclass, varargin)


options=varargin2options(varargin);
[base_path, options]=get_option(options, 'base_path', 'default');
check_unsupported_options(options, mfilename);

%% Get base path
if strcmpi(base_path,'default')
    base_path=set_results_path();
end
% add artificialy the filename PHASE 1/2 that directory later for the
% results
this_f_name=fullfile(mfilename('fullpath'), strvarexpand('phase$phase_number$'));
% filename for comparism
filename1=get_save_path(base_path, this_f_name, 'compare_kernels.mat', 3);
filename2=get_save_path(base_path, this_f_name, 'SVM_classifyer', 3);

switch action
    %% save results
    case 'save'
        if nargin>3
            save(filename1, 'svm_objects', 'misclass');
        end
        save(filename2, 'SVM_model');
        %% load results
    case 'load'
        if nargout>1
            load(filename1);
        end
        load(filename2);
end

