function tab_q=latex_table_with_qs_and_rel_errs(qs, rel_errs, row_names, q_names, varargin)
options=varargin2options(varargin);
[caption, options]=get_option(options, 'caption', '');
[label, options]=get_option(options, 'label', '');
[qdigits, options]=get_option(options, 'qdigits', 4);
[errdigits, options]=get_option(options, 'errdigits', 2);
[label, options]=get_option(options, 'label', '');
check_unsupported_options(options,mfilename);


    n_q=size(qs,2);
    ltable_data=[qs, rel_errs];
    input.data =ltable_data;
    
    % Set column labels (use empty string for no label):
    input.tableColLabels = [q_names; 'rel. err.[\%]'];
    % Set row labels (use empty string for no label):
    input.tableRowLabels = row_names;
    input.dataFormat = {strvarexpand('%.$qdigits$f'),n_q,strvarexpand('%.$errdigits$f'),1};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 0;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = caption;
    input.tableLabel = label;
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 0;
    % call latexTable:
    tab_q = latexTable(input);
end