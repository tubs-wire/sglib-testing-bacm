function [x, u, ind]=load_points_and_solution(varargin)
%
% LOAD_POINTS_AND_SOLUTION
% example:
% [X, U, IND]=LOAD_POINTS_AND_SOLUTION
% loads the saved coordinate (X) of the computation points and the
% corresponding solution calculated by the B5 RANS simulation. IND gives
% the indices of converging points. If no OPTIONAL input is defined the
% function loads only the coordinates and solutions of the converging
% points.
% OPTIONAL INPUTS
%  - ONLY_CONVERGING=TRUE/FALSE gives whether only converging points should
%  be loaded (TRUE) or also the nonconverging ones (FALSE)
%
%  - METHOD IDENT or POINT TYPE defines the type of points that should be
%  loaded (some predefined options are available with different POINT_TYPE
%  names, otherwise with METHOD_IDENT any combination of points can be
%  loaded.
%
% - BASE PATH: gives the place where the points and solutions are saved
% (the pathname of the folder POINTS AND SOLUTIONS.
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


options=varargin2options(varargin);
[point_type,options]=get_option(options, 'point_type', 'all_qmc_point');  %ALL_POINT/SCAN_POINT/SPEC_POINT/BEST_POINT/INT_POINT
[only_converging,options]=get_option(options, 'only_converging', true);
[method_ident,options]=get_option(options, 'method_ident', []);
[base_path,options]=get_option(options, 'base_path', 'default');
check_unsupported_options(options, mfilename);


%% Define which points/solutions/index are needed
if isempty(method_ident)
    switch point_type
        case 'all_qmc_point'
            method_ident=[1,3];
        case 'all_qmc_point_with_spec_and_island'
            method_ident=[1,3,4,5];
        case 'scan_point' % first scanning QMC points
            method_ident=1;
        case 'int_point' %Integration points
            method_ident=[6,7,8];
        case 'best_point' %'best point'
            method_ident=3;
        case 'best_point_island' %QMC points around 'best point'
            method_ident=4;
        case  'spec_point' %points around nonconverging points
            method_ident=5;
        case  'int_point_center' %integration point, middle of the L_shaped domain
            method_ident=6;
        case  'int_point_left' %integration point, middle of the L_shaped domain
            method_ident=7;
        case  'int_point_right' %integration point, middle of the L_shaped domain
            method_ident=8;
        case  '1_phase_MCMC_best_point' %found best point with MCMC with gPCE proxi model
            method_ident=9;
        case  'points_from_MCMC' %points for the second phase that were determined by MCMC
            method_ident=10;
        case  'qMC_and_MCMC' %scan points and MCMC points
            method_ident=[1,3, 10];
        case  'all_qMC_and_MCMC' %scan points and MCMC points
            method_ident=[1, 3, 4, 10];
        case  '2_phase_MMSE_best_point' %scan points and MCMC points
            method_ident=11;
        case  '2_phase_MCMC_best_point' %scan points and MCMC points
            method_ident=12;
        case  '3rd_phase_points' %scan points and MCMC points
            method_ident=[1, 3, 9, 10, 11, 12];
        case  '3_phase_MCMC_best_point' %scan points and MCMC points
            method_ident=[13];    
    end
end
%% Define paths for points/solutions/index of converging points
if strcmpi(base_path, 'default')
    base_path=fullfile(set_base_path(), 'points_and_solutions');
end
spec_path={};
for i=method_ident
    if i==1;
        spec_path={spec_path{:},'first_scan_points'};
    elseif i==3;
        spec_path={spec_path{:},'best_point'};
    elseif i==4;
        spec_path={spec_path{:}, fullfile('best_point', 'island_around')};
    elseif i==5;
        spec_path={spec_path{:},fullfile('spec_point', '2414')};
        spec_path={spec_path{:},fullfile('spec_point', '3769')};
    elseif i==6;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_center')};
    elseif i==7;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_left')};
    elseif i==8;
        spec_path={spec_path{:},fullfile('int_points_L_shaped', 'L_shaped_bottom')};
    elseif i==9;
        spec_path={spec_path{:},'best_point_with_proxi'};
    elseif i==10;
        spec_path={spec_path{:},'second_phase_points'};
    elseif i==11;
        spec_path={spec_path{:},'second_phase_best_pointsMMSE'};
    elseif i==12;
        spec_path={spec_path{:},'second_phase_best_pointsMCMC'};
    elseif i==13;
        spec_path={spec_path{:},'third_phase_points'};
    end
end
%%
%% Load points/solutions/index
x=[];
u=[];
ind=true(0,0);
for j=1:length(spec_path)
    file_name_j=fullfile(base_path, spec_path{j}, 'points_and_sol.mat');
    S_j=load(file_name_j);
    x_j=S_j.x;
    N_j=size(x_j,2);
    if isfield(S_j, 'u')
        u_j=S_j.u;
        %         % get rid of middle doubled point and take the avarage
        %         u_j_av=(u_j(160,:,:)+u_j(161,:,:))/2;
        %         tind=true(1,360);
        %         tind(160)=false;
        %         u_j=u_j(tind,:,:);
        %         u_j(161,:,:)=u_j_av;
    else
        u_j=NaN([360,5,N_j]);
    end
    if isfield(S_j, 'nonconv_ind')
        ind_j=true(1,N_j);
        ind_j(S_j.nonconv_ind)=false;
        
    else
        ind_j=true(1,N_j);
    end
    x=[x, x_j];
    u=cat(3, u, u_j);
    ind=[ind,ind_j];
end
if only_converging
    x=x(:,ind);
    u=u(:,:,ind);
end



