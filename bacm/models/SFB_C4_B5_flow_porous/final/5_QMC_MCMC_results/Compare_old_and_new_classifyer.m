function [misc_polyg3, misc_rbf]=Compare_old_and_new_classifyer()
% This functions compares the seperation boundary by polygonial 3rd order
% with SMV soft margins and the old one used by Gaussian kernel function

options=varargin2options(varargin);
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
check_unsupported_options(options, mfilename);

%% Get old and new rate of misclassification
% load classifyer
load('SVM_classifyer_1.mat')

% load points and solution from first phase MCMC samples
[q, ~, ind]=load_points_and_solution('point_type', 'points_from_MCMC',...
    'only_converging', false);
n=size(q,2);

% predict classification of Q_M
[label, ~]=predict(SVM_Model_1, q');
ind_p = (label==1)';

% check number of misclassification
ind_mis=(ind_p~=ind);
misc_polyg3=sum(ind_mis)/n;

% Number of misqualified points by Gaussian kernel function
misc_rbf=sum(~ind)/n;

% Load first scan points
[q_s, ~, ind_s]=load_points_and_solution('point_type', 'scan_point',...
    'only_converging', false);

%% Plot misclassification
% Load prior paramset for the names
RVs=set_prior_paramset();

% matrix plot
h(1)=figure;
plot_grouped_scatter({q_s(:,ind_s),q_s(:,~ind_s), q(:,ind), q(:,~ind), q(:,ind_mis)}, ...
    'MarkerSize', [2,2, 4, 4, 2],'Color', 'grgrk','MarkerType','....o',...
    'Legends', {'QMC conv points', 'QMC not conv points',...
    'MCMC conv points', 'MCMC not conv points (misclassified by Gaussian Kernel)',...
    'Misclassified by new (polynomial) Kernel'}, ...
    'Labels', RVs.param_plot_names,...
    'LineType', {'-', '--', '-', '-', '-.'}, 'LineWidth', [1,1, 2, 2, 2],...
    'title', 'Checking Gaussian and 3rd order polynomial Kernel');

%3D plot
    h(2)=figure;
    plot_3d_points({q_s(:,ind_s),q_s(:,~ind_s), q(:,ind), q(:,~ind), q(:,ind_mis)}, ...
    [1,2,6],...
    'MarkerSize', [6,6, 10, 10, 8],'Color', 'grgrk','MarkerType','....o',...
    'Legends', {'QMC conv points', 'QMC not conv points',...
    'MCMC conv points', 'MCMC not conv points (misclassified by Gaussian Kernel)', 'Misclassified by new (polynomial) Kernel'}, ...
    'Labels', {RVs.param_plot_names{[1,2,6]}},...
    'Title', 'Checking Gaussian and 3rd order polynomial Kernel');

%% Save results
if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
 % this files name
 this_f_name=mfilename('fullpath');
 % filename for results
 filename=get_save_path(base_path, this_f_name, 'Results.mat', 2);
 % save results
 save(filename, 'misc_polyg3', 'misc_rbf');
 % filename for figs (the two figures are save together)
 filename=get_save_path(base_path, this_f_name, 'compare_SVM_rbf_and_polyg3.fig', 2);
 % save two figures together
 savefig(h, filename)
 % filename for fig (for png extension)
 filename=get_save_path(base_path,this_f_name, 'compare_SVM_rbf_and_polyg3_1.png', 2);
 % save  fig 1
 saveas(h(1), filename)
 % filename for fig 2 (for png extension)
 filename=get_save_path(base_path, this_f_name, 'compare_SVM_rbf_and_polyg3_2.png', 2);
 % save  fig 2
 saveas(h(2), filename)
end

    
