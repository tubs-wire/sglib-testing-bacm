% PLOT_STATISTICS_FROM_QMC_AND_FIRST_PHASE_MCMC
% loads results from parametric scan with the QMC points and the result
% from the firsth phase update with MCMC and plots the results
% - minmax, mean and variance regions from the scan and the updated points
% - scattered plot of the converging and not converging points
% - Responses from DNS, from B5 best calibration, and the best MCMC point
%   and their errors.
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


%% Name of fitting responsees

list_of_response=load_list_of_response('short');

%% Load DNS result, best point by B5, QMC scan points and MCMC points

% Load DNS result
[u_DNS, coord]=load_experiment_sol_and_coord();
n_out=size(u_DNS,2);

% Load best point of B5 from hand calibration
[x_B5, u_B5]=load_points_and_solution('point_type', 'best_point');

% Load first scan points from phase 1 and the B5 computations at the
% points
[x_QMC, u_QMC, ind_QMC]=load_points_and_solution('point_type', 'scan_point',...
    'only_converging', false);

% Load updated MCMC points from phase 1 and the B5 computations at the
% points
[x_MCMC, u_MCMC, ind_MCMC]=load_points_and_solution('point_type', 'points_from_MCMC',...
    'only_converging', false);

%% Calculate statistics from first QMC scan points

[u_mean_QMC, u_var_QMC, u_min_QMC, u_max_QMC, ~, ~, ...
    mean_e_QMC, min_e_QMC, max_e_QMC]=...
    get_statistics_from_solution(u_QMC, u_DNS);

%% Calculate statistics from first phase MCMC points

[u_mean_MCMC, u_var_MCMC, u_min_MCMC, u_max_MCMC, ~, ~, ...
    mean_e_CMC, min_e_MCMC, max_e_MCMC]=...
    get_statistics_from_solution(u_MCMC, u_DNS);

%% Plot statistics minmax region of solutions with QMC and first phase MCMC

% plot minmax region of QMC solutions
plot_u_mean_quant(coord', u_mean_QMC, u_min_QMC, u_max_QMC, ...
    'line_color', 'k', 'fill_color', 'b','change_axis', true,...
    'ylabels', list_of_response, 'subplot_dim',[1,5], 'transparency', 0.6,...
    'xlabel', 'coord')
hold on
% plot minmax region of MCMC solutions
plot_u_mean_quant(coord', u_mean_MCMC, u_min_MCMC, u_max_MCMC, ...
    'line_color', 'k:', 'fill_color', 'c', 'change_axis', true,...
    'ylabels', list_of_response, 'subplot_dim',[1,5], 'transparency', 0.8,...
    'xlabel', 'coord')

% plot DNS result
for i=1:n_out
    subplot(1,5,i)
    plot(u_DNS(:,i), coord,'k-.', 'LineWidth', 3)
    if i==1
        legend('QMC solution region', 'mean with QMC', 'MCMC solution region', ...
        'mean with MCMC', 'DNS solution')
    end
end


%% Get the point with the best norm and compare with hand calibration results

N=size(u_MCMC(:,:, ind_MCMC),3);
% avaraged relative errors of the different responses
rel_err=get_rel_deviation_from_meas(u_MCMC(:,:, ind_MCMC), u_DNS, coord);
% Choose best MCMC point
[min_rel_err, minind]=min(rel_err);

% compare best norm with hand calibration results:
rel_err_B5=get_rel_deviation_from_meas(u_B5, u_DNS);

display(strvarexpand...
    ('The relative error with best hand calibration by B5 is: $rel_err_B5*100$%'))
display(strvarexpand...
    ('The relative error of the best MCMC point is: $min_rel_err*100$%'))

%% Load prior simparameters

RVs = set_prior_paramset();

%% Plot new points converging and not converging QMC and MCMC points and best points
% (hand calibrated and first phase calibrated)
figure
plot_grouped_scatter({x_QMC(:,~ind_QMC),x_QMC(:,ind_QMC),x_B5...
x_MCMC(:,~ind_MCMC), x_MCMC(:,ind_MCMC), x_MCMC(:,minind)}, ...
'MarkerSize', [2,2,6,3,3,6],'Color', 'rgkrgb','MarkerType','..x..x',... 
'Legends', {'QMC not converging points', 'QMC converging points',...
'B5 hand calibration', 'MCMC not converging points', 'MCMC converging points', ...
'First phase calibration'}, 'Labels', RVs.param_plot_names,...
'LineType', {'-', '--', '-.', ':', '-', '--' },...
'LineWidth', [1,1,1,2,2,2])

%% Plot the responses from DNS, from the B5 hand calibration, and best MCMC

for i=1:5
    figure
    subplot(1,2,1)
    plot(squeeze(u_MCMC(:,i, :)),coord )
    hold on
    h_dns  = plot(u_DNS(:,i), coord, 'k','LineWidth', 2);
    h_best = plot(u_B5(:,i), coord, 'k-.', 'LineWidth', 1);
    h_ubest= plot(squeeze(u_MCMC(:,i,minind)), coord, 'b--', 'LineWidth',2);
    xlabel(list_of_response{i})
    ylabel('coord')
    x_lim=(xlim);
    xlim(x_lim);
    h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    legend([h_dns, h_best, h_ubest, h_interface],...
        {'DNS data', 'B5 hand callibration', 'C4 fist stage maschinery', 'Interface'});
    % plot the same but with the errors
    subplot(1,2,2)
    hold on
    hh_best = plot(abs(u_B5(:,i)-u_DNS(:,i)), coord, 'k-.', 'LineWidth', 1);
    hh_ubest= plot(abs(squeeze(u_MCMC(:,i,minind))-u_DNS(:,i)), coord, 'b--', 'LineWidth',2);
    xlabel(strvarexpand('|$list_of_response{i}$-$list_of_response{i}$_{ DNS}|'))
    ylabel('coord')
    x_lim=xlim;
    hh_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    xlim(x_lim);
    legend([hh_best, hh_ubest, hh_interface],...
        { 'B5 hand callibration', 'C4 fist stage maschinery', 'Interface'});
end

%% save figures

actual_path=pwd;
cd f:\\noemi\Documents\SFB880\SFB_flow_over_porous_material\second_phase_results\evaluate_sols_at_fist_hase_updated_points

h = get(0,'children');
for i=1:length(h)
    saveas(h(i), ['figure' num2str(i)], 'png');
    saveas(h(i), ['figure' num2str(i)], 'fig');
end
cd(actual_path)