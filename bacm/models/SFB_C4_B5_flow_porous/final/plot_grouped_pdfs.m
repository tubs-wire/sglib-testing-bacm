function [x,p]=plot_grouped_pdfs(q)

if ~iscell(q)
    q={q};
end

n_samples=length(q);
n_param=size(q{1},1);
x=cell(n_param, n_samples);
p=cell(n_param, n_samples);

for i=1:n_samples
    q_i=q{i};
    
    
    for j=1:n_param
        q_ij=q_i(j,:);
        N=length(q_ij);
        figure(j)
        hold on
        if N==1
            y_lim=ylim();
            plot([q_ij, q_ij], y_lim)
        else
            [x{j,i},p{j,i}]=my_plot_density(q_ij, 'type', 'kde');
        end
    end
end
        
