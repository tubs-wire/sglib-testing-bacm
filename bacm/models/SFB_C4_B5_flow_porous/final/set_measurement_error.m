function E_m=set_measurement_error(u_DNS, y, beta_sigma, sigma_fact)

if nargin<3||isempty(beta_sigma)
  beta_sigma=1;
end
if nargin<4||isempty(sigma_fact)
    sigma_fact=[0.5,2,2,2,1.5];
end
% variance of the masurement errors (gassian curve with center at interface
% +3%)
perc_sigma=(3*ones(size(y))+2*exp(-(y-0.9).^2./(2*(0.1/3)^2)));
err_sigma=binfun(@times,  max(abs(u_DNS)).*sigma_fact, perc_sigma/200)*beta_sigma;
%err_var=(err_sigma).^2;
E_m=generate_stdrn_simparamset(err_sigma(:));