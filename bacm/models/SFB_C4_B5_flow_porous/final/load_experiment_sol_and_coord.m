function [u, coord]=load_experiment_sol_and_coord(varargin)

% LOAD_EXPERIMENT_SOL_AND_COORD
% loads the saved coordinate (COORD) in the channel where experimental
% (DNS) data is available and the corresponding measurements (U)
% OPTIONAL INPUTS
%  - FILE_NAME gives the name of the *.MAT filename where the data is
%  stored
%
%  
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



options=varargin2options(varargin);
[file_name,options]=get_option(options, 'file_name', []);
check_unsupported_options(options, mfilename);

%% Define paths for points/solutions/index of converging points
if isempty(file_name)
    if isunix
        file_name='/home/noefried/Documents/SFB880/flow_trhough_porous_media/SFB_project/points_and_solutions/experiment/exp_sol_and_coord';
    elseif ispc
        file_name='F:\noemi\Documents\SFB880\SFB_flow_over_porous_material\points_and_solutions\experiment\exp_sol_and_coord';
    end
    
end

%%
%% Load data
    S=load(file_name);
    u=S.u;
%     tind=true(1,size(u,1));
%     tind(160)=false;
%     u=u(tind,:);
    coord=S.y;
%     coord=coord(tind);