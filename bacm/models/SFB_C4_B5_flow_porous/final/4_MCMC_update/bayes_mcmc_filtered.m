function [x, acc_rate] = bayes_mcmc_filtered(x2likelihood, X, N, P, x0, varargin)
% BAYES_MCMC Test code for the Bayes MCMC sampling method
% X: Prior SimParamset
% P: Proposal Simparamset (with parameters having the proposal
% distribution)
% x2likelihood function from q to likelihood (log of the likelihood when
% LOG_FlAG is on)
% N: sample number
% x0: starting point of random walk
%
% If LOG_FLAG is true, the X"LIKELIHOOD function needs to give the loglikelihood
%
%   Noemi Friedman & Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional input
options=varargin2options(varargin);
[parallel, options]=get_option(options, 'parallel', false);
[T, options]=get_option(options, 'T', 100);
[N_burn, options]=get_option(options, 'N_burn', []);
[T_burn, options]=get_option(options, 'T_burn', []);
[plot_walk, options]=get_option(options, 'plot', false);
[plot_dim, options]=get_option(options, 'plot_dim', [1,2,3]);
[filter_x, options]=get_option(options, 'filter_x', []);
[log_flag, options]=get_option(options, 'log_flag', false);
check_unsupported_options(options, mfilename);

%% Set automatic inputs if not defined

if nargin<3||isempty(N)
    N=10000;
end
if nargin<4||isempty(P)
    prior_vars=X.var;
    P=generate_stdrn_simparamset(sqrt(prior_vars/10));
end
if nargin <5||isempty(x0)
    x0=X.mean;
end



if parallel
    N_b = ceil(N/T);
    if isempty(N_burn) && isempty(T_burn)
        T_burn = 0;
    elseif ~isempty(T_burn) && ~isempty(N_burn)
        error('Either T_burn or N_burn has to be defined')
    elseif isempty(T_burn)
        T_burn = floor(N_burn/T);
    end
end
%% Define posterior pdf function
if isempty(filter_x)
    filter_x = @(x)true(1,size(x,2));
end
if log_flag
    x2pdf_func=@(x) (funcall(x2likelihood, x)+X.logpdf(x)) + neg_infty_when_zeropdf(filter_x, x);
else
    x2pdf_func=@(x) (funcall(x2likelihood, x).*X.pdf(x) .* filter_x);
end

V_x=X.get_germ;
x_dim = gpcbasis_size(V_x, 2);

%% Run metropolis hasting sampler
if plot_walk
    plot_bounds=(X.germ2params(gpcgerm_linspace(V_x, 2)))';
else
    plot_bounds=[];
end

if parallel
    [x, acc_rate]=mh_sample_parallel(N_b, x0, x2pdf_func, P, T, X, plot_dim, x_dim,...
        'plot', plot_walk, 'plot_bounds', plot_bounds, 'log_flag', log_flag, ...
        'T_burn', T_burn);
else
    [x, acc_rate]=mh_sample(N, x0, x2pdf_func, P, X, plot_dim, x_dim,...
        'plot', plot_walk, 'plot_bounds', plot_bounds, 'log_flag', log_flag,...
        'N_burn', N_burn);
end

%% plot samples


end
function [Xs, acc_rate] = mh_sample(N, x0, x2pdf_func, Prop_params, X,  plot_dim, x_dim, varargin)
options=varargin2options(varargin);
[N_burn, options]=get_option(options, 'N_burn', 0);
[plot_walk, options]=get_option(options, 'plot', false);
[plot_bounds, options]=get_option(options, 'plot_bounds', []);
[log_flag, options]=get_option(options, 'log_flag', false);
check_unsupported_options(options, mfilename);
% MH_SAMPLE Basic version of the Metropolis-Hastings sampler

x=x0;
Xs=[];
acc_steps = 0;
for i=1:N + N_burn
    xn=x+Prop_params.sample(1);
    if log_flag
        loga=funcall(x2pdf_func,xn)-funcall(x2pdf_func,x);
        loga=loga+Prop_params.logpdf(x-xn)-Prop_params.logpdf(xn-x);
        a = exp(loga);
    else
        a=funcall(x2pdf_func,xn)/funcall(x2pdf_func,x);
        a=a*Prop_params.pdf(x-xn)/Prop_params.pdf(xn-x);
    end
    if a>=1 || rand<a
        x=xn;
        acc_steps = acc_steps + 1;
    end
    if i <= N_burn
        displaytext = (strvarexpand('Burning in, $i$/$N_burn$, accepted steps: $acc_steps$'));
        if i== N_burn
            % restart counter
            acc_steps=0;
        end
    else
        displaytext = (strvarexpand('$i-N_burn$/$N$, accepted steps: $acc_steps$'));
    end
    display(displaytext);
    if i>N_burn
        Xs=[Xs x];
    end
    if plot_walk
        if T>T_burn
            mcolor ='r';
        else
            mcolor = 'b';
        end
        if x_dim == 1
            plot(X(1,:),zeros(size(X(1,:))),'or','MarkerSize',5,'MarkerFaceColor',mcolor)
            axis([plot_bounds(:),-1, 1]);
            xlabel(X.param_names{1})
           
        elseif x_dim == 2
            plot(X(1,:),X(2,:),'or','MarkerSize',5,'MarkerFaceColor',mcolor)
            axis(plot_bounds(:));
            xlabel(X.param_names{1})
            ylabel(X.param_names{2})
            
        else
            plot_bounds_p=plot_bounds(:,plot_dim);
            plot3(X(plot_dim(1),:),X(plot_dim(2),:), X(plot_dim(3),:), 'or', 'MarkerSize',5,'MarkerFaceColor',mcolor)
            axis(plot_bounds_p(:));
            xlabel(X.param_names{plot_dim(1)})
            ylabel(X.param_names{plot_dim(2)})
            zlabel(X.param_names{plot_dim(3)})
        end
        pause(.01)
    end
end
% compute acceptance rate
acc_rate = acc_steps/N;
end
%X=X(:);


function [Xs, acc_rate] = mh_sample_parallel(N_b, x0, x2pdf_func, Prop_params, T, X, plot_dim, x_dim, varargin)
% MH_SAMPLE_PARALLEL Parallel version of the Metropolis-Hastings sampler
options=varargin2options(varargin);
[T_burn, options]=get_option(options, 'T_burn', 0);
[plot_walk, options]=get_option(options, 'plot', false);
[plot_bounds, options]=get_option(options, 'plot_bounds', []);
[log_flag, options]=get_option(options, 'log_flag', false);
check_unsupported_options(options, mfilename);

Xs=[];
acc_steps = zeros(1, N_b);
if size(x0, 2) == 1
    x = repmat(x0, 1, N_b);
elseif size(x0,2) == N_b
    x = x0;
else
    error('MCMC random walk: The second dimension of the initial point of the random walk has to match with the N number of samples')
end    
p = funcall(x2pdf_func, x);

for i=1:T + T_burn
    xn=x+Prop_params.sample(N_b);
    pn=funcall(x2pdf_func,xn);
    if log_flag
        loga=pn-p;
        a=loga+Prop_params.logpdf(x-xn)-Prop_params.logpdf(xn-x);
        a=exp(loga);
    else
        a=pn./p;
        a=a.*Prop_params.pdf(x-xn)./Prop_params.pdf(xn-x);
    end
    ind = (a>=1 | rand(1,N_b)<a);
    x(:,ind)=xn(:,ind);
    acc_steps = acc_steps + ind;
    if i <= T_burn
        displaytext = (strvarexpand('Burning in, $i$/$T_burn$, acceptance rate: $sum(acc_steps)/(N_b*i)*100$'));
        if i== T_burn
            % restart counter
            acc_steps=zeros(1, N_b);
        end
    else
        displaytext = (strvarexpand('$i-T_burn$/$T$, acceptance rate: $sum(acc_steps)/(N_b*(i-T_burn))*100$%'));
    end
    display(displaytext);
    p(ind)=pn(ind);
    if i>T_burn
        Xs=[Xs x];
    end
    %% plot points
    if plot_walk
        if T>T_burn
            mcolor ='r';
        else
            mcolor = 'b';
        end
        if x_dim==1
            plot(x(1,:),zeros(size(x(1,:))),'or','MarkerSize',5,'MarkerFaceColor',mcolor)
            axis([plot_bounds(:),-1, 1]);
            xlabel(X.param_names{1})
        elseif x_dim==2
            plot(x(1,:),x(2,:),'or','MarkerSize',5,'MarkerFaceColor',mcolor)
            axis(plot_bounds(:));
            xlabel(X.param_names{1})
            ylabel(X.param_names{2})
        else
            plot_bounds_p=plot_bounds(:,plot_dim);
            plot3(x(plot_dim(1),:),x(plot_dim(2),:),x(plot_dim(3),:), 'or','MarkerSize',5,'MarkerFaceColor',mcolor)
            axis(plot_bounds_p(:));
            xlabel(X.param_names{plot_dim(1)})
            ylabel(X.param_names{plot_dim(2)})
            zlabel(X.param_names{plot_dim(3)})
        end
        title(displaytext)
        pause(.01)
    end
end
acc_rate =sum(acc_steps)/(N_b*(i-T_burn));
end
    function additive_term = neg_infty_when_zeropdf(x_filter, x)
        additive_term = -inf(1, size(x,2));
        additive_term(x_filter(x))=0;
    end









