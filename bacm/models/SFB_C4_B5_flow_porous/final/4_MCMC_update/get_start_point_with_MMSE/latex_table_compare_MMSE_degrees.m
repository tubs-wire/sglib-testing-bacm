function tab_MMSE_degs=latex_table_compare_MMSE_degrees(mmse, varargin)

options=varargin2options(varargin);
[caption, options]=get_option(options, 'caption', '');
[errdigits, options]=get_option(options, 'errdigits', 2);
[label, options]=get_option(options, 'label', '');
check_unsupported_options(options,mfilename);

T=zeros(size(mmse.phi_degree,1),4);
T(:,1)=mmse.phi_degree;
T(:,2)=mmse.post.mean.rel_err*100;
T(1,3)=NaN;
T(2:end,3)=mmse.post.density_peak.rel_err*100;
T(1,4)=NaN;
T(2:end,4)=mmse.post.best_sample.rel_err*100;

%% Create table for results
        
    input.data =T;
    % Set column labels (use empty string for no label):
    input.tableColLabels = {'$p_\phi$';  'rel. err. with post. mean'; 'rel. err. with post. density peak'; 'rel. err. of best post. sample'};
    %input.tableRolLabels = '';
    input.dataFormat = {'%i',1,strvarexpand('%.$errdigits$f'),3};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 1;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = caption;
    input.tableLabel = label;
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 0;
    % call latexTable:
    tab_MMSE_degs = latexTable(input);
end

