function [mmse_mean, post_samples, prior_mean]=...
    SFB_MMSE_with_low_rank(Q, q, u, p_phi, p_gpc, u_DNS, y, n_modes, varargin)

options=varargin2options(varargin);
[low_rank, options]=get_option(options, 'low_rank', 'KLE');
%[save_results, options]=get_option(options, 'save_results', false);
%[base_path, options]=get_option(options, 'base_path', 'default');
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[ign_ind, options]=get_option(options, 'ignored_indices_for_update',[]);
[filter_func, options]=get_option(options, 'filter_func', {});
[filter_prior_zero, options]=get_option(options, 'filter_prior_zero', false);
check_unsupported_options(options, mfilename);

%%

n_out=size(u,2);
n_y=size(u,1);

%% Ignore measurement at the interface

m_ind=true(1,length(y));
m_ind(ign_ind)=false;
y_m=y(m_ind);
u_m_DNS=u_DNS(m_ind, :);

%% Set measurement error

E_m=set_measurement_error(u_m_DNS, y_m, 1, [0.5,2,2,2,1.5]);

%% gPCE proxi model

[V_u, u_i_alpha, param2germ_func, germ2param_func]=...
    generate_gPCE_surrogate(q, u, p_gpc, Q, u_DNS, y, ...
        gPCE_options{:});

%% Get gpce of the measureable

V_y=V_u;
ind1=1:n_y;
ind2=1:n_out;
lind=sub2ind(  size(u_DNS), ...
    repmat(ind1(m_ind)', 1, n_out), repmat(ind2, sum(m_ind),1));

y_i_alpha=u_i_alpha(lind, :);

%% Update
xi=param2germ_func(q);
switch(low_rank)
    case 'KLE'
        % use the first phase update prior samples
        [~, xi_mmse, ~, xi_post]=...
            MMSE_with_low_rank(y_i_alpha, V_y, u_m_DNS, y_m, E_m, p_phi, n_modes, 'xi_prior_samples', xi);
        % prior samples can be sampled from the distribution corresponding
        % to the germ
        %[~, xi_mmse, ~, xi_post]=...
        %    MMSE_with_low_rank(y_i_alpha, V_y, u_m_DNS, y_m, E_m, p_phi, n_modes);
       case 'SVD'
        [~, xi_mmse, ~, xi_post]=...
            MMSE_with_low_rank(y_i_alpha, V_y, u_m_DNS, [], E_m, p_phi, n_modes, 'xi_prior_samples', xi);
        %[~, xi_mmse, ~, xi_post]=...
        %    MMSE_with_low_rank(y_i_alpha, V_y, u_m_DNS, [], E_m, p_phi, n_modes);

end

%%
% solution at best MMSE point
u_mmse=gpc_evaluate(u_i_alpha, V_u, xi_mmse);
u_mmse=reshape(u_mmse, size(u_DNS));
% relative error at best MMSE point from the proxi model
rel_err=get_rel_deviation_from_meas(u_mmse, u_DNS, y, 'ignore_indices', ign_ind);
% posterior samples of q
q_post=germ2param_func(xi_post);
% best parameter values from MMSE
q_mmse=germ2param_func(xi_mmse);

%% filter posterior samples

if nargin>2 && ~isempty(filter_func)
    q_post=filter_func(q_post);
end
if filter_prior_zero
    lind=Q.pdf(q_post)==0;
    q_post=q_post(:,~lind);
end

%% Get sample distribution peaks and the solution there
% peak of sample distributions
q_peak=sample_density_peak(q_post);
u_at_peak=gpc_evaluate( u_i_alpha, V_u, param2germ_func( q_peak));
u_at_peak=reshape(u_at_peak, size(u_DNS));
rel_err_at_peak=get_rel_deviation_from_meas(u_at_peak, u_DNS, y, 'ignore_indices', ign_ind, 'sep_response', true);

%% compare to the initial norm

u_at_mean=gpc_evaluate( u_i_alpha, V_u, param2germ_func( Q.mean));
u_at_mean=reshape(u_at_mean, size(u_DNS));
rel_err_at_mean=get_rel_deviation_from_meas(u_at_mean, u_DNS, y, 'ignore_indices', ign_ind, 'sep_response', true);


%% check best points among samples
% solutions at the posterior samples
    u_post=gpc_evaluate(u_i_alpha, V_u, param2germ_func( q_post));
    u_post=reshape(u_post, size(u_DNS,1), size(u_DNS,2), []);
    rel_err_post=get_rel_deviation_from_meas(u_post, u_DNS, y, 'ignore_indices', ign_ind, 'sep_response', true);
    [rel_err_best, ind_min]=min(rel_err_post);
    u_best=u_post(:,:,ind_min);
    q_best=q_post(:,ind_min);
    
%% Structure result
% MMSE mean point
mmse_mean.q=q_mmse;
mmse_mean.rel_err=rel_err;
mmse_mean.u=u_mmse;
post_samples.q=q_post;
post_samples.u=u_post;
post_samples.q_best=q_best;
post_samples.u_best=u_best;
post_samples.rel_err_best=rel_err_best;
post_samples.density_peak.q=q_peak;
post_samples.density_peak.u=u_at_peak;
post_samples.density_peak.rel_err=rel_err_at_peak;
prior_mean.q=Q.mean;
prior_mean.u=u_at_mean;
prior_mean.rel_err=rel_err_at_mean;
