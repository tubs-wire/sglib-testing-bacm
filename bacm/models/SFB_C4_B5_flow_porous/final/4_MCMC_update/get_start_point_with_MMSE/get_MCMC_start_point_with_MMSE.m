function mmse=get_MCMC_start_point_with_MMSE...
    (Q, q, u, p_gpc, n_eig, p_phi, u_DNS, y, varargin)

options=varargin2options(varargin);
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
[low_rank, options]=get_option(options, 'low_rank', 'KLE');
[filter_func, options]=get_option(options, 'filter_func', {});
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[file_identifyer, options]=get_option(options, 'file_identifyer', '');
[ign_ind, options]=get_option(options, 'ignored_indices_for_update', []);
check_unsupported_options(options, mfilename);

%%
n_phi=length(p_phi);

%% initiate memory

% vector of the mmse posterior mean
q_mmse=zeros(Q.num_params,n_phi+1);
% vector of the solutions at q_mmse
u_mmse=zeros(360,5,n_phi+1);
% vector of error at q_mmse
rel_err=zeros(1,n_phi+1);
% for the posterior samples
q_post=cell(n_phi,1);
% for the solutions at posterior samples
u_post=cell(n_phi,1);
% point with best weighted error
q_best=zeros(Q.num_params, n_phi);
% and the weighted error there:
u_post_best_rel_err=zeros(n_phi,1);
% and the solution there:
u_post_best=zeros(360,5,n_phi);

%% Cacluclate MMSE for different orders
for i=1:n_phi
    p_phi_i=p_phi(i);
    
    display(i)
    [mmse_mean, post_samples, prior_mean]=...
        SFB_MMSE_with_low_rank(Q, q, u, p_phi_i, p_gpc, u_DNS, y, n_eig, ...
        'filter_prior_zero',true,...
        'filter_func', filter_func,...
        'gPCE_options', gPCE_options, 'low_rank', low_rank,...
        'ignored_indices_for_update', ign_ind);
    if i==1
        rel_err(1)=prior_mean.rel_err;
        u_mmse(:,:,1)=prior_mean.u;
        q_mmse(:,1)=prior_mean.q;
    end
    
    rel_err(i+1)=mmse_mean.rel_err;
    q_mmse(:,i+1)=mmse_mean.q;
    u_mmse(:,:,i+1)=mmse_mean.u;
    q_post{i}=post_samples.q;
    u_post_best_rel_err(i)=post_samples.rel_err_best;
    u_post_best(:,:,i)=post_samples.u_best;
    q_best(:,i)=post_samples.q_best;
    u_post{i}=post_samples.u;
end

%% Structure results

mmse.post.mean.q=q_mmse;
mmse.post.mean.u=u_mmse;
mmse.post.mean.rel_err=rel_err;
mmse.post.samples.q=q_post;
mmse.post.samples.u=u_post;
mmse.post.best_sample.q=q_best;
mmse.post.best_sample.u=u_post_best;
mmse.post.best_sample.rel_err=u_post_best_rel_err;
mmse.phi_degree=[0;p_phi(:)];
mmse.u_p_gpc=p_gpc;
mmse.low_rank.n_eig=n_eig;
mmse.low_rank.type=low_rank;

%% plot B5 calibration result, solution at best posterior point and DNS solution

% Load best point of B5 from hand calibration
[q_B5, u_B5]=load_points_and_solution('point_type', 'best_point');
[u_DNS, coord]=load_experiment_sol_and_coord();
[minerr, minind]=min(mmse.post.best_sample.rel_err);
B5_err=get_rel_deviation_from_meas(u_B5, u_DNS);
str_B5=strvarexpand('B5 hand calib (err = $B5_err$)');
str_MMSE=strvarexpand('MMSE best (err = $minerr$)');
ltext={'DNS', str_B5, str_MMSE};
f=plot_sols_dns_and_b5(coord, [], u_DNS, u_B5, u_post_best(:,:,minind), 'legend_text', ltext);


%% Save results
if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    % add artificialy the filename FILE_IDENTIFYER that directory later for the
    % results
    this_f_name=fullfile(mfilename('fullpath'), file_identifyer);
    % filename for results
    filename=get_save_path(base_path, this_f_name, 'Results.mat', 3);
    % save results
    save(filename, 'mmse');
    % save table
    if make_table
        filename=get_save_path(base_path, this_f_name, 'tab_MMSE_degs', 3);
        save(filename, 'tab_MMSE_degs_compare');
    end
    % filename for fig
    filename=get_save_path(base_path, this_f_name, 'best_C4_point.fig', 3);
    % save two figures together
    savefig(f, filename)
    % filename for fig (for png extension)
    filename=get_save_path(base_path,this_f_name, 'best_C4_point.png', 3);
    % save  fig 1
    saveas(f, filename)
end
