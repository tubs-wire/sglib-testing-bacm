function [mmse_mean, post_samples, prior_mean]=...
    MMSE_with_SVD(p_phi, p_gpc, n_modes, varargin)

options=varargin2options(varargin);
[point_type, options]=get_option(options, 'point_type', 'all_qMC_and_MCMC');
[prior_params, options]=get_option(options, 'prior_params', 'default');
[low_rank, options]=get_option(options, 'low_rank', 'KLE');
%[save_results, options]=get_option(options, 'save_results', false);
%[base_path, options]=get_option(options, 'base_path', 'default');
[wp, options]=get_option(options, 'weight_pow', 1);
[flag_PCE, options]=get_option(options, 'flag_PCE', false);
[filter_points,options]=get_option(options, 'filter_points', false);
[filter_func, options]=get_option(options, 'filter_func', 2);
[filter_prior_zero, options]=get_option(options, 'filter_prior_zero', false);
check_unsupported_options(options, mfilename);


%% Get prior simparamset, measurements and points from which the gPCE is calculated

Q=set_prior_paramset('type', prior_params);
[u_DNS, coord]=load_experiment_sol_and_coord();
[q, u]=load_points_and_solution('point_type', point_type);
n_out=size(u,2);
n_x=size(u,1);
% ignored indices at interface:
ign_ind=158:163;
%% Ignore measurement at the interface

tind=true(1,360);
tind(ign_ind)=false;
coord_y=coord(tind);
y_DNS=u_DNS(tind, :);
%y=u(tind, :, :);

%% Set measurement error

% measurement error variance scaling factor:
%beta_sigma=1; %for island update
beta_sigma=1; % for big region update
% error percentage of sigma (for the five different responses)
sigma_fact=[0.5,2,2,2,1.5];
% variance of the masurement errors (gassian curve with center at interface
% +3%)
perc_sigma=(3*ones(size(coord_y))+2*exp(-(coord_y-0.9).^2./(2*(0.1/3)^2)));
err_sigma=binfun(@times,  max(abs(y_DNS)).*sigma_fact, perc_sigma/200)*beta_sigma;
%err_var=(err_sigma).^2;
E_m=generate_stdrn_simparamset(err_sigma(:));

%% gPCE proxi model

if flag_PCE
    [V_u, u_i_alpha]=generate_gPCE_surrogate(q, u, p_gpc, Q, ...
        'weight_pow',wp, 'flag_PCE',true);
    germ2param_func=@(xi)Q.stdnor2params(xi);
    param2germ_func=@(q)Q.params2stdnor(q);
else
    [V_u, u_i_alpha]=generate_gPCE_surrogate(q, u, p_gpc, Q, ...
        'weight_pow',wp, 'flag_PCE',false);
    germ2param_func=@(xi)Q.germ2params(xi);
    param2germ_func=@(q)Q.params2germ(q);
end

%% Get gpce of the measureable

V_y=V_u;
ind1=1:n_x;
ind2=1:n_out;
ind=sub2ind(  size(u_DNS), ...
    repmat(ind1(tind)', 1, n_out), repmat(ind2, sum(tind),1));

y_i_alpha=u_i_alpha(ind, :);

%% Update

switch(low_rank)
    case 'KLE'
        [xi2y2xi_func, xi_mmse, ~, xi_post]=...
            MMSE_with_low_rank(y_i_alpha, V_y, y_DNS, coord_y, E_m, p_phi, n_modes);
    case 'SVD'
        [xi2y2xi_func, xi_mmse, ~, xi_post]=...
            MMSE_with_low_rank(y_i_alpha, V_y, y_DNS, [], E_m, p_phi, n_modes);
end

%%
% solution at best MMSE point
u_mmse=gpc_evaluate(u_i_alpha, V_u, xi_mmse);
u_mmse=reshape(u_mmse, size(u_DNS));
% relative error at best MMSE point from the proxi model
rel_err=get_rel_deviation_from_meas(u_mmse, u_DNS, coord, 'ignore_indices', ign_ind);
% posterior samples of q
q_post=germ2param_func(xi_post);
% best parameter values from MMSE
q_mmse=germ2param_func(xi_mmse);

%% filter posterior samples

if nargin>2 && filter_points
    q_classifyer=get_flag_converge_func(filter_func);
    ind=q_classifyer(q_post);
    q_post=q_post(:,ind);
end
if filter_prior_zero
    ind=Q.pdf(q_post)==0;
    q_post=q_post(:,~ind);
end
%% compare to the initial norm

u_at_mean=gpc_evaluate( u_i_alpha, V_u, param2germ_func( Q.mean));
u_at_mean=reshape(u_at_mean, size(u_DNS));
rel_err_at_mean=get_rel_deviation_from_meas(u_at_mean, u_DNS, coord, 'ignore_indices', ign_ind, 'sep_response', true);
plot_grouped_scatter({Q.sample(10000), q_post, q_mmse});

%% check best points among samples
% solutions at the posterior samples
    u_post=gpc_evaluate(u_i_alpha, V_u, param2germ_func( q_post));
    u_post=reshape(u_post, size(u_DNS,1), size(u_DNS,2), []);
    rel_err_post=get_rel_deviation_from_meas(u_post, u_DNS, coord, 'ignore_indices', ign_ind, 'sep_response', true);
    [rel_err_best, ind_min]=min(rel_err_post);
    u_best=u_post(:,:,ind_min);
    q_best=q_post(:,ind_min);
    
%% Structure result
% MMSE mean point
mmse_mean.q=q_mmse;
mmse_mean.rel_err=rel_err;
mmse_mean.u=u_mmse;
post_samples.q=q_post;
post_samples.q_best=q_best;
post_samples.u_best=u_best;
post_samples.rel_err_best=rel_err_best;
prior_mean.q=Q.mean;
prior_mean.u=u_at_mean;
prior_mean.rel_err=rel_err_at_mean;
