function mmse=get_MCMC_start_point_with_MMSE1...
    (Q, q, u, p_gpc, n_eig, p_phi, u_DNS, y, varargin)

options=varargin2options(varargin);
[low_rank, options]=get_option(options, 'low_rank', 'KLE');
[filter_func, options]=get_option(options, 'filter_func', {});
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[ign_ind, options]=get_option(options, 'ignored_indices_for_update', []);
check_unsupported_options(options, mfilename);

%%
n_phi=length(p_phi);

%% initiate memory

% vector of the mmse posterior mean
q_mmse=zeros(Q.num_params,n_phi+1);
% vector of the solutions at q_mmse
u_mmse=zeros(360,5,n_phi+1);
% vector of error at q_mmse
rel_err=zeros(1,n_phi+1);

% point with best weighted error
q_best=zeros(Q.num_params, n_phi);
% and the weighted error there:
u_best_rel_err=zeros(n_phi,1);
% and the solution there:
u_best=zeros(360,5,n_phi);

% point at the posterior peak
q_peak=zeros(Q.num_params, n_phi);
% the error of the solution there
u_peak_rel_err=zeros(n_phi,1);
% and the solution there:
u_peak=zeros(360,5,n_phi);

% variance of the posterior samples
q_post_var=zeros(6, n_phi+1);

%% Cacluclate MMSE for different orders
for i=1:n_phi
    p_phi_i=p_phi(i);
    
    display(i)
    [mmse_mean, post_samples, prior_mean]=...
        SFB_MMSE_with_low_rank(Q, q, u, p_phi_i, p_gpc, u_DNS, y, n_eig, ...
        'filter_prior_zero',true,...
        'filter_func', filter_func,...
        'gPCE_options', gPCE_options, 'low_rank', low_rank,...
        'ignored_indices_for_update', ign_ind);
    if i==1
        rel_err(i)=prior_mean.rel_err;
        u_mmse(:,:,1)=prior_mean.u;
        q_mmse(:,1)=prior_mean.q;
        q_post_var(:,i)=Q.var;
    end
    
    rel_err(i+1)=mmse_mean.rel_err;
    q_mmse(:,i+1)=mmse_mean.q;
    u_mmse(:,:,i+1)=mmse_mean.u;
    
    u_best_rel_err(i)=post_samples.rel_err_best;
    u_best(:,:,i)=post_samples.u_best;
    q_best(:,i)=post_samples.q_best;
    
    q_peak(:,i)=post_samples.density_peak.q;
    u_peak(:,:,i)=post_samples.density_peak.u;
    u_peak_rel_err(i)=post_samples.density_peak.rel_err;
    q_post_var(:, i+1)=var(post_samples.q,[], 2);
end

%% Structure results

mmse.post.mean.q=q_mmse;
mmse.post.mean.u=u_mmse;
mmse.post.mean.rel_err=rel_err;

mmse.post.best_sample.q=q_best;
mmse.post.best_sample.u=u_best;
mmse.post.best_sample.rel_err=u_best_rel_err;

mmse.post.density_peak.q=q_peak;
mmse.post.density_peak.u=u_peak;
mmse.post.density_peak.rel_err=u_peak_rel_err;

mmse.post.vars=q_post_var;

mmse.phi_degree=[0;p_phi(:)];
mmse.u_p_gpc=p_gpc;
mmse.low_rank.n_eig=n_eig;
mmse.low_rank.type=low_rank;

