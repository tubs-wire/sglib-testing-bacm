function tab_q=latex_table_best_q_and_qB5_and_their_rel_errs(us, qs, u_DNS, u_B5, q_B5, y, Q)

% Compute only on unique subset
[~, ind_u]=unique(qs', 'rows');
u_u=us(:,:, ind_u);
[~, ~, min_rel_err, q_best]=get_point_and_sol_with_min_err(u_u, u_DNS, y, qs);
rel_err_B5=get_rel_deviation_from_meas(u_B5, u_DNS);

    % best parameters and the relative error
    ltable_data=[q_best', min_rel_err];
    % best hand calibration and its relative error
    ltable_data=[ltable_data; q_B5', rel_err_B5];
    
    input.data =ltable_data;
    
    % Set column labels (use empty string for no label):
    input.tableColLabels = [strcat('$', Q.param_plot_names, '$'); 'rel. err.'];
    % Set row labels (use empty string for no label):
    input.tableRowLabels = {'$q_{post,best}$', 'hand calib'};
    input.dataFormat = {'%.4f',6,'%.4f',1};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 0;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = strvarexpand(...
    'Best parameters from Bayesian posterior samples and best hand calibration');
    input.tableLabel = 'tab: calibration results';
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 0;
    % call latexTable:
    tab_q = latexTable(input);
end