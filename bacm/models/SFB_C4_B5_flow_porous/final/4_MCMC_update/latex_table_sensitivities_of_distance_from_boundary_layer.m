function tab_q=latex_table_sensitivities_of_distance_from_boundary_layer(si_i_alpha, V_s, Q)

% sensitivity analysis
[partial_var, I_s, ratio_by_index, ratio_by_order]=...
    gpc_sobol_partial_vars(s_i_alpha, V_s);
% seperate different order indices
p_names=gpc_multiindex2param_names(I_s, Q.param_plot_names);
ind{1}=sum(I_s,2)==1;
I_s{1}=I_s(ind{1}, :);
ind{2}=sum(I_s,2)==2;
I_s{2}=I_s(ind{2}, :);
ind{3}=sum(I_s,2)==3;
I_s{3}=I_s(ind{3}, :);

%% Pie plots for first second and third order

%  plot sensitivities 1: fist order indices and higher
pie3([ratio_by_index(ind{1}),sum(ratio_by_index(ind{3})),...
    sum(ratio_by_index(ind_3))], ...
    {Q.param_plot_names{:}, 'second order', 'third order'})
%  2: second order
pie3(ratio_by_index(ind{2}), p_names(ind{2}));
%  3: third order
pie3(ratio_by_index(ind{3}), p_names(ind{3}));

%% Make table of the results

end
%% 
    function

    % best parameters and the relative error
    ltable_data=[params, sobol_index*100];
    % best hand calibration and its relative error
    ltable_data=[ltable_data; q_B5', rel_err_B5];
    
    input.data =ltable_data;
    
    % Set column labels (use empty string for no label):
    input.tableColLabels = [strcat('$', Q.param_plot_names, '$'); 'rel. err.'];
    % Set row labels (use empty string for no label):
    input.tableRowLabels = {'$q_{post,best}$', 'hand calib'};
    input.dataFormat = {'%.4f',6,'%.4f',1};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 0;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = strvarexpand(...
    'Best parameters from Bayesian posterior samples and best hand calibration');
    input.tableLabel = 'tab: calibration results';
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 0;
    % call latexTable:
    tab_q = latexTable(input);
end