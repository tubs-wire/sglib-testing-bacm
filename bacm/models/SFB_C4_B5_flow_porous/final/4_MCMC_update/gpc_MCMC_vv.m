function [samples_post, acc_rate]=gpc_MCMC_vv(N, observ_func, x_params, Z, err_vars, varargin)

options=varargin2options(varargin);
[prop_dist, options]=get_option(options, 'prop_dist', {});
[start_point, options]=get_option(options, 'start_point', 'default'); %starting germ point
[filter_prior, options]=get_option(options, 'filter_prior', {}); % additional condition to cancel prior probability
[log_flag, options]=get_option(options, 'log_flag', true);
[parallel, options]=get_option(options, 'parallel', false);
check_unsupported_options(options, mfilename);

%% Prior distribution

prior_params=dists2simparamset(x_params);
prior_vars=prior_params.var;

%% get proposal distributions
if isempty(prop_dist)
    prop_params=generate_stdrn_simparamset(sqrt(prior_vars/200));
else
    prop_params=dists2simparamset(prop_dist);
end


%% measurement error
error_params=generate_stdrn_simparamset(sqrt(err_vars));
x2loglikelihood = @(x)get_loglikelihood(observ_func, x, error_params, Z);
x2likelihood = @(x)get_likelihood(observ_func, x, error_params, Z);

%% MCMC
if log_flag
    likelihood_func = x2loglikelihood;
else
    likelihood_func = x2loglikelihood;
end

if parallel
    [samples_post, acc_rate] = bayes_mcmc_filtered(likelihood_func, prior_params,...
        N, prop_params, start_point, 'parallel', true, ...
        'plot_dim', 3:5, 'plot', true, 'T', 1000, 'log_flag', log_flag, ...
        'T_burn', 1000, 'filter_x', filter_prior);
else
   [samples_post, acc_rate] = bayes_mcmc_filtered(likelihood_func, prior_params,...
        N, prop_params, start_point, 'parallel', false, ...
        'plot_dim', 3:5, 'plot', true, 'log_flag', log_flag, ...
        'N_burn', 0, 'filter_x', filter_prior);
end
end
function prob_l=get_likelihood(observ_func, x, error_params, Z)
u=feval(observ_func, x);
prob_l=error_params.pdf(u-Z);
end
function prob_l=get_loglikelihood(observ_func, x, error_params, Z)
u=feval(observ_func, x);
prob_l=error_params.logpdf(u-Z);
end
