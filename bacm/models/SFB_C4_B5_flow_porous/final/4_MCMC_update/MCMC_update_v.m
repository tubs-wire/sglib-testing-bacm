function [q_MCMC, u_MCMC, acc_rate]=MCMC_update_v...
    (N, Q, q, u, p_gpc, u_DNS, y, n_meas, q_start, varargin)

%% Optional inputs
options=varargin2options(varargin);
[q_classifyer, options]=get_option(options, 'q_classifyer', {});
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[ign_ind, options]=get_option(options, 'ignored_indices_for_update', []);
[error_scaling, options]=get_option(options, 'error_scaling', 2);
[log_flag, options]=get_option(options, 'log_flag', true);
[parallel, options]=get_option(options, 'parallel', false);
check_unsupported_options(options, mfilename);

%% Initialize

n_out=size(u,2);
n_y=size(u,1);

%% Ignore measurement at the interface, choose measurement locations
% ignore points at interface (corresponding to IGN_IND)

m_ind=1:length(y);
m_ind=setdiff(m_ind, ign_ind);
y_m=y(m_ind);

% generate QMC points for coordinates
Y=MySimParamSet();
Y.add('y_1', UniformDistribution(min(y),max(y)));
y_m1=sort(Y.sample(n_meas, 'mode', 'qmc'));
% choose closest points from y_m
[~, m_ind1]=min(abs(repmat(y_m1, length(y_m),1)-repmat(y_m,1,n_meas)));
% indices without the ignored and the not measured points
m_ind=m_ind(m_ind1);
% the coordinate and measurement there:
y_m=y(m_ind);
u_m_DNS=u_DNS(m_ind, :);

%% Set measurement error

E_m=set_measurement_error(u_m_DNS, y_m, 1, [0.5,2,2,2,1.5]*error_scaling);

%% gPCE proxi model

[V_u, u_i_alpha, param2germ_func, germ2param_func]=...
    generate_gPCE_surrogate(q, u, p_gpc, Q, u_DNS, y, ...
    gPCE_options{:});

%% Get gpce of the measureable

V_y=V_u;
ind1=1:n_y;
ind2=1:n_out;
lind=sub2ind(  size(u_DNS), ...
    repmat(ind1(m_ind)', 1, n_out), repmat(ind2, length(m_ind),1));

y_i_alpha=u_i_alpha(lind, :);

%% Setup start point of MCMC

if nargin<8||isempty(q_start)
    q_start=Q.mean();
end
xi_start=param2germ_func(q_start);

%% Get prior germ distribution

% prior distribution (with prior dist of the UQ)
n_germ=gpcbasis_size(V_u,2);
syschars=V_u{1};
prior_dist=cell(n_germ, 1);
for i=1:n_germ
    prior_dist{i}=polysys_dist(syschars(i));
end
prior_dist_xi=gendist2object(prior_dist);

% Observation operator
observ_operator=@(xi) gpc_evaluate(y_i_alpha, V_y, xi);

%% Composition of germ2param and filter func (define classifyer of germ)

if ~isempty(q_filter)
    xi_classifyer_func=@(xi)(q_classifyer(germ2param_func(xi)));
end

%% UPDATE
MCMC_options={'start_point',xi_start, 'filter_prior', xi_classifyer_func,...
    'log_flag', log_flag, 'parallel', parallel};
%MCMC_options={'is_prior_nonzero_func', does_xi_a_good_region_func};
[xi_MCMC, acc_rate]=...
    gpc_MCMC_vv(N, observ_operator, prior_dist_xi, u_m_DNS(:), E_m.var,...
    MCMC_options{:});
q_MCMC=Q.germ2params(xi_MCMC);
display(acc_rate);
% solution at the posterior samples
u_MCMC=gpc_evaluate(u_i_alpha, V_u, xi_MCMC);
u_MCMC=reshape(u_MCMC, [size(u_DNS),N]);
