function [samples_post, acc_rate]=gpc_MCMC_v(N, observ_func, prior_dist, Z, err_vars, varargin)

options=varargin2options(varargin);
[prop_dist, options]=get_option(options, 'prop_dist', {});
[start_point, options]=get_option(options, 'start_point', 'default'); %starting germ point
[is_prior_nonzero_func, options]=get_option(options, 'is_prior_nonzero_func', {}); % additional condition to cancel prior probability
check_unsupported_options(options, mfilename);

%% Prior distribution

prior_params=dists2simparamset(prior_dist);
prior_vars=prior_params.var;

%% get proposal distributions
if isempty(prop_dist)
    prop_params=generate_stdrn_simparamset(sqrt(prior_vars/200));
else
    prop_params=dists2simparamset(prop_dist);
end


%% measurement error
error_params=generate_stdrn_simparamset(sqrt(err_vars));

%% MCMC
[samples_post, acc_rate]=mh_sample(N, prior_params, prop_params,...
    observ_func, error_params, Z, 'start_point',...
    start_point, 'is_prior_nonzero_func', is_prior_nonzero_func);
%X=mh_sample_parallel(N, dist, prop_dist);

end
%% Metropolis-Hasting
function [X, acc_rate]=mh_sample(N, prior_params, prop_params, observ_func, error_params, Z, varargin)
options=varargin2options(varargin);
[start_point, options]=get_option(options, 'start_point', 'default');
[is_prior_nonzero_func, options]=get_option(options, 'is_prior_nonzero_func', {}); % additional condition to cancel prior probability
check_unsupported_options(options, mfilename);

% MH_SAMPLE Basic version of the Metropolis-Hastings sampler
N_burn = 0;
% initial parameter point
if strcmp(start_point, 'default')
    x=prior_params.mean;
else
    x=start_point;
end
p=get_likelihood(observ_func, x, error_params, Z)*prior_params.pdf(x);
%count number  accepted steps
acc_steps=0;
acc_steps_burn=0;
X=[];
num_refusals_due_to_gapflag=0;
for i=1:N+N_burn
    xn=x+prop_params.sample(1);
    prior_pdf=prior_params.pdf(xn);
    pn=get_likelihood(observ_func, xn, error_params, Z)*prior_pdf;
    a=pn/p;
    a=a*prop_params.pdf(x-xn)/prop_params.pdf(xn-x);
    alpha=rand;
    if ~isempty(is_prior_nonzero_func)
        flag_nocancel_prior=feval(is_prior_nonzero_func, xn);
        if ~flag_nocancel_prior
            if a>=1 || alpha<a; a=0; num_refusals_due_to_gapflag=num_refusals_due_to_gapflag+1;end
        end
    end
    if a>=1 || alpha<a
        x=xn;
        p=pn;
        if i>N_burn; acc_steps=acc_steps+1;else acc_steps_burn=acc_steps_burn+1;end
    end
    if i>N_burn
        display(strvarexpand('$i-N_burn$/$N$'));
        X=[X x];
    else
        display(strvarexpand('Burning in $i$/$N_burn$'));
    end
    acc_rate=acc_steps/N;
end
if ~isempty(is_prior_nonzero_func)
    display(strvarexpand('num_refusals_due_to_gapflag=$num_refusals_due_to_gapflag$'))
end
end
function prob_l=get_likelihood(observ_func, x, error_params, Z)
u=feval(observ_func, x);
prob_l=error_params.pdf(u-Z);
end
