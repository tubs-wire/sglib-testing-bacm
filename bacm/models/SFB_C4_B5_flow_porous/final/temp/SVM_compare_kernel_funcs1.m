function [svm_objects, misclass, n_support_vects, convergence_info, SVMModel]=...
    SVM_compare_kernel_funcs1(varargin)

% SVM_COMPARE_KERNEL_FUNCS
% compares decision boundaries by partitioning the samples to training and
% validation points, and checking percentage of missclassified points.
% The output stores the SVM OBJECT and the vector with the
% missclassification percentage.
%
% Optional inputs:
% -SVM_COMPARE_KERNEL_FUNCS('phase_number', 1) takes the QMC points for
%  classification
% -SVM_COMPARE_KERNEL_FUNCS('phase_number', 2) takes the QMC and the MCMC
%  points from first stage update for classification
%
% -PLOT_PREDICTION is a flag to switch off (default) or on the plot of
% new sample points and the prediction of their classification
%
% -SAVE_RESULTS is a flag to save (SAVE_RESULTS=true) or
%   not (SAVE_RESULTS=false) (default is false) the output variables, the
%   figures, and the data for the figures
%
% -BASE_PATH
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[phase_number, options]=get_option(options, 'phase_number', 1);
[plot_prediction, options]=get_option(options, 'plot_prediction', false);
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
[parallel_comp, options]=get_option(options, 'parallel_comp', false);
check_unsupported_options(options, mfilename);

%% Load points and classifyers

if phase_number==1
    [x, ~, ind]=load_points_and_solution('only_converging',...
        false, 'point_type', 'all_qmc_point');
elseif phase_number==2
    [x, ~, ind]=load_points_and_solution('only_converging', false,...
        'method_ident', [1,3,4,5,10]);
end

%% Compare different kernel functions

% Define type of Kernel functions that should be checked
kernell_funcs={'RBF', 'polynomial', 'polynomial', 'polynomial', 'polynomial', 'polynomial'};
n_k=length(kernell_funcs);

%% Initiate memory

% Initiate memory for and missclassification percentage, SVM objects,
% the number of support vectors and whether the model was converging
% (the  first column belongs to infty
% BoxConstraint, that is without using soft
% margines and the second with aoutomatic setup (BoxConstrain=1)


svm_objects=cell(n_k,2);
misclass=zeros(n_k,2);
n_support_vects=zeros(n_k, 2);
convergence_info=zeros(n_k,2);

%% SVM training with all the data and crossvalidating
if parallel_comp
    parfor i=1:n_k
        display(strvarexpand('$i$/$n_k$'))
        [svm_objects_i, misclass_i, n_support_vects_i, convergence_info_i]=...
            train_and_valid(x,ind,kernell_funcs{i}, i);
        svm_objects(i,:)=svm_objects_i;
        misclass(i,:)=misclass_i;
        n_support_vects(i,:)=n_support_vects_i;
        convergence_info(i,:)=convergence_info_i;
    end
else
    for i=1:n_k
        display(strvarexpand('$i$/$n_k$'))
        [svm_objects_i, misclass_i, n_support_vects_i, convergence_info_i]=...
            train_and_valid(x,ind,kernell_funcs{i}, i);
        svm_objects(i,:)=svm_objects_i;
        misclass(i,:)=misclass_i;
        n_support_vects(i,:)=n_support_vects_i;
        convergence_info(i,:)=convergence_info_i;
    end
end
%% Check beest classifyer

[~, ind]=min(misclass(:));
[indi, indj]=ind2sub(size(misclass),ind);
SVMModel=svm_objects{indi, indj};
display(SVMModel);

%% Plot prediction with best separation

if plot_prediction
    % Load prior distribution of parameters
    RVs=set_prior_paramset();
    % Sample
    Q_sample=RVs.sample(10000);
    % Classify
    [label, ~]=predict(SVMModel, Q_sample');
    ind = label==1;
    % Plot prediction
    h(1)=figure;
    plot_grouped_scatter({Q_sample(:,ind),Q_sample(:,~ind)}, ...
        'MarkerSize', [4,4],'Color', 'rg','MarkerType','..',...
        'Legends', {'Converging points', 'Not converging points'}, ...
        'Labels', RVs.param_plot_names,...
        'LineType', {'-', '--'}, 'LineWidth', [2,2],...
        'title', 'Prediction of classification by the Kernel trick')
    
    %gplotmatrix(Q_sample',[],  label)
    h(2)=figure;
    plot_3d_points({Q_sample(:,ind),Q_sample(:,~ind)}, ...
    [1,2,6],...
    'MarkerSize', [10, 10], 'Color', 'gr','MarkerType','..',...
    'Legends', {'Converging points', 'Not converging points'}, ...
    'Labels', {RVs.param_plot_names{[1,2,6]}},...
    'Title', 'Prediction of classification by the Kernel trick');
end
%% Save results
% set base path
if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    % add artificialy the filename PHASE 1/2 that directory later for the
    % results
    this_f_name=fullfile(mfilename('fullpath'), strvarexpand('phase$phase_number$'));
    % filename for results
    filename=get_save_path(base_path, this_f_name, 'Results.mat', 3);
    % save results
    save(filename, 'svm_objects', 'misclass', 'n_support_vects', 'convergence_info', 'SVMModel');
    
    % filename for figs (the two figures are save together)
    filename=get_save_path(base_path, this_f_name, 'SVM_best_prediction.fig', 3);
    % save two figures together
    savefig(h, filename)
    % filename for fig 1 (for png extension)
    filename=get_save_path(base_path,this_f_name, 'SVM_best_prediction.png', 3);
    % save  fig 1
    saveas(h(1), filename)
    % filename for fig 2 (for png extension)
    filename=get_save_path(base_path, this_f_name, 'SVM_best_prediction2.png', 3);
    % save  fig 2
    saveas(h(2), filename)
end
end
function [svm_objects_i, misclass_i, n_support_vects_i, convergence_info_i]...
    =train_and_valid(x,ind,kernell_funcs_i, i)

% Train the SVM classifier without soft margines
switch kernell_funcs_i
    case 'polynomial'
        p=i;
        svm_objects1_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'PolynomialOrder', p, 'BoxConstraint', Inf,...
            'KernelScale','auto', 'ClassNames', [false, true]);
    otherwise
        svm_objects1_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'KernelScale','auto', 'ClassNames', [false, true],...
            'BoxConstraint', Inf);
end

% Crossvalidate
CVSVModel=crossval(svm_objects1_i);

% Save missclassification and number of support vectors
misclass1_i=kfoldLoss(CVSVModel);
n_support_vects1_i=length(svm_objects1_i.Alpha);
convergence_info1_i=svm_objects1_i.ConvergenceInfo.Converged;
% Train the SVM classifier with soft margines (Boxconstrain=1)
switch kernell_funcs_i
    case 'polynomial'
        p=i;
        svm_objects2_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'PolynomialOrder', p, ...
            'KernelScale','auto', 'ClassNames', [false, true]);
    otherwise
        svm_objects2_i = fitcsvm(x',ind','Standardize',true,...
            'KernelFunction',kernell_funcs_i,...
            'KernelScale','auto', 'ClassNames', [false, true]);
end

% Cross validate
CVSVModel=crossval(svm_objects2_i);

% Save missclassification and number of support vectors
misclass2_i=kfoldLoss(CVSVModel);
n_support_vects2_i=length(svm_objects2_i.Alpha);
convergence_info2_i=svm_objects2_i.ConvergenceInfo.Converged;

misclass_i=[misclass1_i,misclass2_i];
n_support_vects_i=[n_support_vects1_i,n_support_vects2_i];
svm_objects_i={svm_objects1_i, svm_objects2_i};
convergence_info_i=[convergence_info1_i, convergence_info2_i];
end