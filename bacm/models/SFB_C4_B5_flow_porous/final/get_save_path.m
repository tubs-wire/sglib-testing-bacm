function filename=get_save_path(root_path, caller_path, file_name, n_level)

% This functions creates a filename with
% ROOT_PATH>CALLER_PATH last n_level parth>FILE_NAME. If the middle folders
% do not exist, it creates it
% create subpathnames
name=cell(n_level,1);
for i=0:n_level-1
    [caller_path, name{n_level-i}]=fileparts(caller_path);
end
% create folders if does not exist yet
for i=1:n_level
    if ~exist(fullfile(root_path, name{1:i}), 'dir');
        mkdir(fullfile(root_path, name{1:i-1}), name{i})
    end
end
% filename:
filename=fullfile(root_path, name{:}, file_name);
end