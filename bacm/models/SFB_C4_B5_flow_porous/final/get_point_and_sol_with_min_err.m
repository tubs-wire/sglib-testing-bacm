function [u_best, minind, min_rel_err, q_best]=get_point_and_sol_with_min_err(us, u_ref, y, qs)

% avaraged relative errors of the different responses
rel_err=get_rel_deviation_from_meas(us, u_ref, y);
% Choose best point
[min_rel_err, minind]=min(rel_err);
u_best=us(:,:,minind);
if nargin>3 && ~isempty(qs)&& nargout>2
    q_best=qs(:,minind);
end