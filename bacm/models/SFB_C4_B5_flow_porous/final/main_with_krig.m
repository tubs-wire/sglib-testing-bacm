% MAIN
% This script goes through the main steps of parameter calibration of model
% parameters of the simulation model of flow over porous material
%
%
%   Noemi Friedman
%   (The code is integrating the libary SGLIB of Elmar Zander)
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.
%
%% I.0 Initialize
clear
%clf
% Choose indices to be ignored for proxi model weighting and update;
%ign_ind=150:171;
ign_ind=155:166;

% Phase 1 or 2 or 3?
phase_number=3;

% load favorite colors
FavColors=set_my_favorite_colors();

% define prior distribution of simulation coefficients
Q = set_prior_paramset();

% DNS result and the coordinates y:
[u_DNS, y]=load_experiment_sol_and_coord();

% Load best point of B5 from manual calibration
[q_B5, u_B5]=load_points_and_solution('point_type', 'best_point');

% Relative error at the manual calibrated point:
rel_err_B5=get_rel_deviation_from_meas(u_B5, u_DNS, y);

%% I.1 Scan the parametric space (with QMC or at integration points)

% Generate samples of the parametervalues by Halton Sequence
N = 4096;
q = generate_QMC_point_on_the_big_domain(Q, N,'flag_plot', true,...
    'save_results', false, 'base_path', 'default');

% Or if there is no problem with convergenece:
% p_int=6; % Smolyak grid from P_INT 1D point rules
% q_int_points=Q.get_integration_points(p_int, 'grid', 'smolyak');

% Generate samples from the island around the best hand calibrated point by
% B5
q_isl_QMC =  generate_points_around_the_best_hand_calibrated_point...
    ('flag_plot', true, 'save_results', false, 'base_path', 'default');

% This should be deleted, because it is reloaded together with the solution
% under the next part
clear variables

%% Calculate solution and load the results

% This loads the QMC points, the corresponding solutions and the IND indice
% showing whether the simulation converged or not (see different configurations in
% LOAD_POITNS_AND_SOLUTION)

if phase_number==1
    % load first QMC scanning points, island points and best B5 hand calibrated point
    [q, u, ind]=load_points_and_solution...
        ('point_type', 'all_qmc_point', 'only_converging', false);
    
elseif phase_number==2
    % load points from first phase (see above) + MCMC posterior point from
    % first phase update
    [q, u, ind]=load_points_and_solution...
        ('point_type', 'qMC_and_MCMC', 'only_converging', false);
elseif phase_number==3
    % load points from first phase (see above) + MCMC posterior point from
    % first phase update + best updated points
    [q, u, ind]=load_points_and_solution...
        ('point_type', '3rd_phase_points', 'only_converging', false);
else
    error('Only phase number 1 and 2 is defined')
end

%% Identify decision boundary by hand

% to be able to classify points as 'converging' or 'not conerging' point.
% Separating boundary by hand
plot_decision_boundary_by_hand();

if phase_number==2||phase_number==3;
    warning('Decision boundary by hand is only tried for first phase data!')
end

%% Identify decision boundary by SVM (if SVM classifyer saved, please go to next block)

% Compare different kernel functions and choose optimal one by
% crossvalidation:
[SVMModel, svm_objects, misclass]=...
    SVM_compare_kernel_funcs(q, ind, 'parallel_comp', true);

% At this point it may be a good idea to save the SVMModel
save_or_load_results_of_SVM_compare_kernel_funcs...
    ('save', phase_number,...
    SVMModel, svm_objects, misclass)

% Plot prediction of random samples
plot_prediction_of_random_samples(Q, SVMModel, phase_number,...
    'base_path', 'default',  'save_fig', true);

% Generate table of missclassifications and optimized Kernel func
% parameters

[ltab, T]=latex_table_compare_SVM_Kernel_funcs...
    (svm_objects, misclass, phase_number,...
    'save_table', true, 'base_path', 'default');

%% Get classifyer and filtering functions

% Results of comparism or the optimal SVMModel can be loaded by:
[SVMModel, svm_objects, misclass]= ...
    save_or_load_results_of_SVM_compare_kernel_funcs...
    ('load', phase_number);

% Define classifyer function (it gives true if the input point q is classified as converging
% (the classifyers are defined in the saved .mat files
q_classifyer=@(q)predict(SVMModel, q');

% Define filter function (it filters out the points that are not
% converging)
filter_func=@(q)(q(:,q_classifyer(q)));

%% Maybe it would be usefull to make a gpc of the classifyer

% p: Order of gPCEs (As the polynomial for the optimal kernel was 3rd degree we
% choose the polynomial degree 3
p=3;
% samples from the parameter
q_s=Q.sample(10000);
% compute score from SVMmodel (=0: point on boundary layer |score|<1 within
% seperating 'street', score>1: converging region  score<-1: not
% converging)
[is_conv,score] = predict(SVMModel,q_s');
u_s=reshape(score(:,2),[1,1,10000]);
% Define gPCE options
flag_PCE=false;
gPCE_options=...
    {'weight_pow',0,...
    'flag_PCE',flag_PCE,...
    'ignored_indices_for_weighting',ign_ind,...
    'weighted_regr',false};
% gpc expansion
[V_s, s_i_alpha, params2germs_func, germs2param_func]=...
generate_gPCE_surrogate(q_s,u_s , 3, Q, [], [], gPCE_options{:});


% sensitivity analysis of classifyer
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(s_i_alpha, V_s);
h=gpc_plot_sensitivities(I_s, ratio_by_index, Q.param_plot_names);
q_tab=gpc_plot_sensitivities(I_s, ratio_by_index, Q.param_plot_names, 'type', 'tab');

    
%% Choose surrogate model 
%(this has to be run only once, to decide on how to set up surrogate 
% properties for next blocks)

% K: k-fold cross validation
K=15;
% p: Order of gPCEs
p=1:7;
% n_s: separate ration (# training samples/# validating samples)
n_s=0.8;

% Test different type of weights (without, 1/deviation, 1/deviation^2)
%[gPCE, h]=test_different_weighted_regressions(Q, q(:,ind), u(:,:,ind), K, p, n_s, u_DNS, y, ign_ind);

% Choose surrogate by cross validation
% Define gPCE options
flag_PCE=false;
gPCE_options=...
    {'weight_pow',0,...
    'flag_PCE',flag_PCE,...
    'ignored_indices_for_weighting',ign_ind,...
    'weighted_regr',false};
% cross validate
file_ident='gPCE_check_order';
gPCE_6 =train_and_cross_val_gPCEs...
    (Q, q(:,ind), u(:,:,ind), K, p, n_s, u_DNS, y,...
    'base_path', 'default',...
    'gPCE_options', gPCE_options,...
    'save_results', true,'file_identifyer', file_ident,...
    'ignored_indices_for_weighting',ign_ind);

% Response names for the plot
u_names=load_list_of_response('short');
legend_str=[ 'avarage', u_names];
% plot comparism of accuracy of different degree gPCEs and save figure
h_6=plot_gPCE_cross_validation_result(gPCE_6,...
    'plot_sep_errs', true, 'save_results', true, 'base_path', 'default',...
    'legend', legend_str);

%% Add a Kriging model on the top
% This section makes a kriging model on top of the gPCE model

% Chose gPCE in accordance to above analysis
flag_PCE=false;
% Order of (g)PCE
p_gpc=5;
% optional inputs for surrogate
gPCE_options=...
    {'weight_pow',0,...
    'flag_PCE',flag_PCE,...
    'weighted_regr',false,...
    'ignored_indices_for_weighting',ign_ind};
% gPCE expansion
[V_u, u_i_alpha, params2germs_func, germs2param_func]=...
generate_gPCE_surrogate(q(:,ind), u(:,:,ind), p_gpc, Q, u_DNS, y, gPCE_options{:});
% response from B5
u_ij=reshape(u, size(u,1)*size(u,2), []);
u_ij=u_ij(:,ind);
% response from gPCE
u_gpc=gpc_evaluate(u_i_alpha, V_u, params2germs_func(q(:,ind)));
% error at points
eps_gpc=u_ij-u_gpc;
% set ration of training and validating sets
n_s=0.8;
% K-fold cross validation
K=15;
% cross and train validate for the error of the gpc
krig_models=train_and_crossval_kriging(params2germs_func(q(:,ind)), eps_gpc, n_s, K, y, u_DNS);

%% Setup MMSE options

% number of eigenmodes
n_eig=5;
% PCE or gPCE of solution
flag_PCE=true;
% Order of (g)PCE
p_gpc=5;
% SVD or KLE (the later is weighted with the grammian)
low_rank='SVD';

% optional inputs for surrogate
gPCE_options=...
    {'weight_pow',1,...
    'flag_PCE',flag_PCE,...
    'weighted_regr',false,...
    'ignored_indices_for_weighting',ign_ind};

%% Compare different MMSE updates with low rank
% Update with different p_phi degree estimators and compare mean results
% and best posterior sample points in a table format

% Estimators total degrees to be checked
p_phi=1:4;
% File identifyer (to create subfolder)
file_ident_mmse=strvarexpand('compare_p_phi$p_phi(1)$-$p_phi(end)$p_gpc$p_gpc$_n_eig$n_eig$_$low_rank$_PCE$flag_PCE$');

% Update
mmse=get_MCMC_start_point_with_MMSE1...
    (Q, q(:,ind), u(:,:,ind), p_gpc, n_eig, p_phi, u_DNS, y,...
    'ignored_indices_for_update', ign_ind,...
    'gPCE_options', gPCE_options, ...
    'filter_func', filter_func, 'low_rank', low_rank);
% make table of relative error results
caption_text=strvarexpand(...
'Results from different degree estimators($min(p_phi)$-$max(p_phi)$): mean values of posterior samples and the one corresponding to the smallest relative error, $low_rank$, $n_eig$ modes');
label_text='tab: compare MMSE orders';
tab_MMSE_degs=latex_table_compare_MMSE_degrees(mmse,'label', label_text, 'caption', caption_text);

%
q_best=mmse.post.best_sample.q;
q_mean=mmse.post.mean.q(:,2);
plot_grouped_scatter({q(:,ind), mmse.post.samples.q{1}, q_mean, q_best});
min_rel_err=mmse.post.best_sample.rel_err;

% plot statistics of MMSE update (mean minmax and quantiles)
f=plot_sample_statistics({u(:, :, ind), mmse.post.samples.u{1}}, y, u_DNS, varargin);

display(strvarexpand...
    ('The relative error with best manual calibration by B5 is: $rel_err_B5*100$%'))
display(strvarexpand...
    ('The relative error of the best posterior point with MMSE is: $min_rel_err*100$%'))

%% Or just make one specific order MMSE
% Define order of MMSE
p_phi=3;
% File identifyer (to create subfolder)
file_ident_mmse=strvarexpand('p_phi$p_phi$p_gpc$p_gpc$_n_eig$n_eig$_$low_rank$_PCE$flag_PCE$');
% MMSE
[mmse_mean, post_samples, prior_mean]=...
    SFB_MMSE_with_low_rank(Q, q(:,ind), u(:,:,ind), p_phi, p_gpc, u_DNS, y, n_eig, ...
    'filter_prior_zero',true,...
    'filter_func', filter_func,...
    'gPCE_options', gPCE_options, 'low_rank', low_rank,...
    'ignored_indices_for_update', ign_ind);
min_rel_err=post_samples.rel_err_best;
u_best_MMSE=post_samples.u_best;

% plot statistics of MMSE update (mean, minmax and quantiles)
plot_sample_statistics({u(:, :, ind), post_samples.u}, y, u_DNS,... 
'plot_quant', true, 'plot_minmax', false, ...
    'sample_names', {'Prior', 'MMSE posterior'},...
    'color_quant', 'bc','color_minmax', 'bc')

% Display error of best posterior point
display(strvarexpand...
    ('The relative error with best manual calibration by B5 is: $rel_err_B5*100$%'))
display(strvarexpand...
    ('The relative error of the best posterior point with MMSE is: $min_rel_err*100$%'))
display(strvarexpand...
    ('The relative error of the mean of the posterior points with MMSE is: $mmse_mean.rel_err*100$%'))
display(strvarexpand...
    ('The relative error of the solution at the posterior peak point with MMSE is: $post_samples.density_peak.rel_err*100$%'))


% Plot the best solution with dns and the errors
legend_text={'DNS', strcat('B5 (rel err: ', num2str(rel_err_B5*100, '%.2f'), '%)'),...
    strcat('MMSE (rel err: ', num2str(min_rel_err*100, '%.2f'),'%)') };
plot_sols_dns_and_b5(y, [], u_DNS, u_B5, u_best_MMSE, 'legend_text', legend_text);

% make table with mean/peak/best points and corresponding errors
qs=[post_samples.q_best';post_samples.density_peak.q'; mmse_mean.q';q_B5'];
rel_errs=[post_samples.rel_err_best; post_samples.density_peak.rel_err; mmse_mean.rel_err; rel_err_B5]*100;
q_names=strcat('$', Q.param_plot_names, '$');
row_names={'post. sample with min rel err', 'post. density peak', 'post. expected value','hand calib'};
caption='Parameters from Bayesian posterior samples from MMSE and best hand calibration';
tab_q=latex_table_with_qs_and_rel_errs(qs, rel_errs, row_names, q_names);

% matrixplot of prior and posterior points
figure
color_vect={FavColors.pastel_blue, FavColors.navy_blue, FavColors.bordeau, FavColors.dark_grey, FavColors.orange, FavColors.metal_grey};
plot_grouped_scatter({Q.sample(10000),post_samples.q, post_samples.q_best, mmse_mean.q, post_samples.density_peak.q, q_B5}, ...
    'MarkerSize', [4,6,8,8,8,10],'Color', color_vect,'MarkerType','..xxxo',...
    'Legends', {'prior sample', 'MMSE posterior sample',...
    'Best MMSE','Mean MMSE','Psot. density peak',  'Hand calibration'},...
    'Labels', Q.param_plot_names,...
    'LineType', {'-', '--', '-.', ':','--', '-'},'shifted', true,...
    'LineWidth', [1,2,3,3,3,2]);

%% MCMC update
% surrogate setup
% Order of (g)PCE
p_gpc=6;

% optional inputs for surrogate
gPCE_options=...
    {'weight_pow',1,...
    'flag_PCE',false,...
    'ignored_indices_for_weighting',ign_ind};
% sample number for posterior sampling
N=50000;
% MCMC update
[q_MCMC, u_MCMC, acc_rate]=MCMC_update(N, Q, q(:,ind), u(:,:,ind), p_gpc,...
    u_DNS, y, 50, post_samples.density_peak.q, ...
    'ignored_indices_for_update', ign_ind,...
    'gPCE_options', gPCE_options,...
    'q_classifyer', q_classifyer);

% Check random walk
xi_MCMC=Q.params2germ(q_MCMC);
figure
plot(xi_MCMC')
legend(Q.param_plot_names);

% Decide from random walk the burn in period, and cut corresponding qs and
% us
N_burn=5000;

q_MCMC_burned=q_MCMC(:,N_burn:end);
u_MCMC_burned=u_MCMC(:,:,N_burn:end);

% plot statistics of MMSE update (mean, minmax and quantiles)
plot_sample_statistics({u(:, :, ind), u_MCMC_burned}, y, u_DNS,... 
'plot_quant', true, 'plot_minmax', false, ...
    'sample_names', {'Prior', 'MMSE posterior'},...
    'color_quant', 'bc','color_minmax', 'bc')
% Find best point
[u_best_MCMC, minind, min_rel_err, q_MCMC_best]=...
    get_point_and_sol_with_min_err(u_MCMC_burned, u_DNS, y, q_MCMC_burned);

% Find peak of posterior distribution
%q_peak=sample_density_peak(q_MCMC_burned);
q_peak=peak_of_sample_probability(q_MCMC_burned);

[V_u, u_i_alpha, param2germ_func, germ2param_func]=...
    generate_gPCE_surrogate(q(:,ind), u(:,:,ind), p_gpc, Q, u_DNS, y, ...
        gPCE_options{:});

u_MCMC_peak=reshape(gpc_evaluate(u_i_alpha, V_u, Q.params2germ(q_peak)), [], 5) ;
rel_err_MCMC_peak=get_rel_deviation_from_meas(u_MCMC_peak, u_DNS, y);

% Mean of posterior samples
q_mean=mean(q_MCMC_burned,2);
u_at_mean=reshape(gpc_evaluate(u_i_alpha, V_u, Q.params2germ(q_mean)), [], 5) ;
rel_err_at_mean=get_rel_deviation_from_meas(u_at_mean, u_DNS, y);

% Display error of best posterior point
display(strvarexpand...
    ('The relative error with best manual calibration by B5 is: $rel_err_B5*100$%'))
display(strvarexpand...
    ('The relative error of the best posterior point with MCMC is: $min_rel_err*100$%'))
display(strvarexpand...
    ('The relative error at the posterior peak with MCMC is: $rel_err_MCMC_peak*100$%'))
display(strvarexpand...
    ('The relative error at the posterior mean with MCMC is: $rel_err_at_mean*100$%'))

% Plot the best solution with dns and the errors
legend_text={'DNS',...
    strcat('MCMC mean (rel err: ', num2str(rel_err_at_mean*100, '%.2f'), '%)'),...
    strcat('MCMC min err (rel err: ', num2str(min_rel_err*100, '%.2f'), '%)'),...
    strcat('MCMC prob peak (rel err: ', num2str(rel_err_MCMC_peak*100, '%.2f'),'%)'),...
    strcat('B5 (rel err: ', num2str(rel_err_B5*100, '%.2f'), '%)')};
color_vect={FavColors.bordeau, FavColors.dark_grey, FavColors.orange, FavColors.metal_grey};
plot_sols_and_errors(y, {u_best_MCMC, u_at_mean, u_MCMC_peak, u_B5 }, u_DNS,...
    'legend_text', legend_text, 'LineWidth', [2, 3, 2, 1],...
    'LineType',{':', '-', '.-', '--'} , 'Color', color_vect);

% make table with best point and B5 point rel errors
tab_q=latex_table_best_q_and_qB5_and_their_rel_errs...
    (u_MCMC_burned, q_MCMC_burned, u_DNS, u_B5, q_B5, y, Q);

% matrixplot of prior and posterior points
figure
color_vect={FavColors.pastel_blue, FavColors.navy_blue, 'r', FavColors.bordeau, FavColors.dark_grey, FavColors.orange, FavColors.metal_grey};
plot_grouped_scatter({Q.sample(10000),post_samples.q, q_MCMC_burned,...
    q_MCMC_best, q_mean, q_peak, q_B5}, ...
    'MarkerSize', [4,4,6,8, 8, 8, 8],'Color', color_vect,'MarkerType','...x.xo',...
    'Legends', {'prior sample', 'MMSE posterior sample',...
    'MCMC posterior sample', 'Min err MCMC', 'Mean MCMC', 'Pdf peak MCMC','Manual calibration'},...
    'Labels', Q.param_plot_names,...
    'LineType', {'-', '--', '-.','-.', ':', '-', '--'},'shifted', true,...
    'LineWidth', [1,2,3,3,3,3,2]);

% make table with mean/peak/best points and corresponding errors

qs=[q_MCMC_best'; q_peak'; q_mean'; q_B5'];
rel_errs=[min_rel_err; rel_err_MCMC_peak; rel_err_at_mean; rel_err_B5]*100;
q_names=strcat('$', Q.param_plot_names, '$');
row_names={'post. sample with min rel err', 'post. density peak', 'post. expected value','hand calib'};
caption='Parameters from Bayesian posterior samples from MCMC and best hand calibration';
tab_q=latex_table_with_qs_and_rel_errs(qs, rel_errs, row_names, q_names);

%% check solution from MMSE/MCMC second stage update spec point results with real black box

% which step of the update: MMSE or MCMC
update_methods={'MMSE', 'MCMC'};
update_method=update_methods{2};

point_type=strvarexpand('$phase_number$_phase_$update_method$_best_point');

% load best points (min_rel_err, prob_peak and mean) from B5 simulation
[qs, us]=load_points_and_solution('point_type', point_type);

% get relative error from simulation results
rel_err=get_rel_deviation_from_meas(us, u_DNS, y);
rel_err_B5=get_rel_deviation_from_meas(u_B5, u_DNS, y);

%plot solution with DNS ref solution
legend_text={'DNS',...
    strcat('MCMC mean (rel err: ', num2str(rel_err(3)*100, '%.2f'), '%)'),...
    strcat('MCMC min err (rel err: ', num2str(rel_err(1)*100, '%.2f'), '%)'),...
    strcat('MCMC prob peak (rel err: ', num2str(rel_err(2)*100, '%.2f'),'%)'),...
    strcat('B5 (rel err: ', num2str(rel_err_B5*100, '%.2f'), '%)')};

color_vect={FavColors.bordeau, FavColors.dark_grey, FavColors.orange, FavColors.metal_grey};
plot_sols_and_errors(y, {us(:,:,3), us(:,:,1), us(:,:,2), u_B5 }, u_DNS,...
    'legend_text', legend_text, 'LineWidth', [2, 3, 2, 1],...
    'LineType',{':', '-', '.-', '--'} , 'Color', color_vect);

%% compare proxi model with simulation result
gPCE_options{2}=1;
[V_u, u_i_alpha, param2germ_func, germ2param_func]=...
    generate_gPCE_surrogate(q(:,ind), u(:,:,ind), p_gpc, Q, u_DNS, y, ...
    gPCE_options{:});

xi_s=Q.params2germ(qs);
u_proxi=gpc_evaluate(u_i_alpha, V_u, xi_s);
u_proxi=reshape(u_proxi,length(y),5,[]);
for i=1:3
rel_err_proxi=get_rel_deviation_from_meas(u_proxi(:,:,i), us(:,:,i), y)
rel_err=get_rel_deviation_from_meas(u_proxi(:,:,i), u_DNS, y)

% figure
% legend_text={'DNS',...
%     strcat('proxi (rel err: ', num2str(rel_err*100, '%.2f'), '%)'),...
%     strcat('B5 sim (rel err: ', num2str(rel_err(1)*100, '%.2f'), '%)'),...
%     strcat('eng. calib (rel err: ', num2str(rel_err(2)*100, '%.2f'),'%)'),...
%     strcat('B5 (rel err: ', num2str(rel_err_B5*100, '%.2f'), '%)')};
% plot_sols_and_errors(y, {u_proxi(:,:,i), us(:,:,i) u_B5 }, u_DNS,...
%     'legend_text', legend_text, 'LineWidth', [2, 3, 2, 1],...
%     'LineType',{':', '-', '.-', '--'} , 'Color', color_vect);
% 
% plot_sols_and_errors(y, {u_proxi(:,:,i), us(:,:,i) u_B5 }, u_DNS,...
%     'legend_text', legend_text, 'LineWidth', [2, 3, 2],...
%     'LineType',{':', '-', '.-'} , 'Color', color_vect(2:4));
end

    



