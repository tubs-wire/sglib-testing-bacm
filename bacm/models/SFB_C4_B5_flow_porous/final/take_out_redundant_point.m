function x=take_out_redundant_point(y, ind, varargin)

options=varargin2options(varargin);
[dim,options]=get_option(options, 'dim', 'row');
check_unsupported_options(options, mfilename);

yind=true(size(y));
if strcmpi(dim, 'row')
    yind(ind,:)=false;
else  
   yind(:,ind)=false;
end
x=y(yind);
end