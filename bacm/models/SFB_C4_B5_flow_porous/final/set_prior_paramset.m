function RVs = set_prior_paramset(varargin )
%SET_PRIOR_PARAMSET Sets the prior simparamset
%   Detailed explanation goes here
options=varargin2options(varargin);
[type, options]=get_option(options, 'type','default');
check_unsupported_options(options, mfilename);

switch type
    case 'default'
        RVs=MySimParamSet();
        RVs.add('beta',UniformDistribution(-10,0),'plot_name', '\beta');
        RVs.add('beta_T',UniformDistribution(-1,1),'plot_name', '\beta_T');
        RVs.add('c_t',UniformDistribution(0,0.2));
        RVs.add('c_eh',UniformDistribution(0,0.3),'plot_name', 'c_{\epsilon^h}');
        RVs.add('c_wd',UniformDistribution(0,30),'plot_name',  'c_{wd}');
        RVs.add('c_dp',UniformDistribution(0,0.4),'plot_name',  'c_{d,p}');
    case 'beta'
        RVs=MySimParamSet();
        RVs.add('beta', (fix_bounds(BetaDistribution(3,3), -10, 0)),'plot_name', '\beta');
        RVs.add('beta_T',fix_bounds(BetaDistribution(4.5,1.6), -1, 1),'plot_name', '\beta_T');
        RVs.add('c_t',fix_bounds(BetaDistribution(3,2.9), 0, 0.2));
        RVs.add('c_eh',fix_bounds(BetaDistribution(1.4,3), 0, 0.3),'plot_name', 'c_{\epsilon^h}');
        RVs.add('c_wd',fix_bounds(BetaDistribution(1.4,3), 0, 30),'plot_name',  'c_{wd}');
        RVs.add('c_dp',fix_bounds(BetaDistribution(3,3), 0,0.4),'plot_name',  'c_{d,p}');
    case 'island_uni'
        RVs=MySimParamSet();
        RVs.add('beta',UniformDistribution(-6,-4),'plot_name', '\beta');
        RVs.add('beta_T',UniformDistribution(0.6,0.8),'plot_name', '\beta_T');
        RVs.add('c_t',UniformDistribution(0,0.2));
        RVs.add('c_eh',UniformDistribution(0,0.1),'plot_name', 'c_{\epsilon^h}');
        RVs.add('c_wd',UniformDistribution(0,10),'plot_name',  'c_{wd}');
        RVs.add('c_dp',UniformDistribution(0.16,0.24),'plot_name',  'c_{d,p}');
end
end

