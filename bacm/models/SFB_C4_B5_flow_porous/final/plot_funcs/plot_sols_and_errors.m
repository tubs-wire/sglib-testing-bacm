function f=plot_sols_and_errors(coord, u, u_DNS,  varargin)


% u - destinct solutions, example u={u_DNS, u_B5, u_best}
% u_DNS - reference solution
% u_B5  - hand calibration by B5
% u_best - best solution from posterior
%%
nu=length(u);
% Plot the responses from DNS, from the B5 hand calibration, and best MCMC
options=varargin2options(varargin);
[list_of_response, options]=get_option(options, 'list_of_response', 'default');
[legend_text, options]=get_option(options, 'legend_text', 'default');
[Color, options]=get_option(options, 'Color', 'default');
[LineType, options]=get_option(options, 'LineType', repmat({'-'},1,nu));
[LineWidth, options]=get_option(options, 'LineWidth', repmat(2,1,nu));
check_unsupported_options(options, mfilename);

%% Define colors
if strcmp(Color, 'default')
    colors='brcgyk';
    if length(colors)<nu
        repmat(colors,1,ceil(nu/length(colors)))
    end
    Color=colors(1:nu);
end
if ~iscell(Color)
    Color=mat2cell(Color, 1, ones(length(Color),1));
end


%%



if strcmp(list_of_response, 'default')
    list_of_response=get_list_of_response('type', 'short');
end
if strcmp(legend_text, 'default')
    legend_text={'DNS', 'B5', 'C4'};
end
%%
f=figure();
for i=1:5
    subplot(2,5,i)
    hold on
    h_dns  = plot(u_DNS(:,i), coord, 'k','LineWidth', 2);
    h=zeros(nu,1);
    for j=1:nu
        u_j=u{j};
        h(j) = plot(u_j(:,i), coord, LineType{j}, 'Color', Color{j},...
             'LineWidth', LineWidth(j));
    end
    xlabel(list_of_response{i})
    if i==1
        ylabel('coord')
    end
    x_lim=(xlim);
    xlim(x_lim);
    h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    if i==5
        legend([h_dns, h(:)', h_interface],...
            [legend_text,{'Interface'}]);
    end
    % plot the same but with the errors
    subplot(2,5,5+i)
    hold on
    for j=1:nu
        u_j=u{j};
        hh(j) = plot(abs(u_j(:,i)-u_DNS(:,i)), coord, LineType{j},...
            'Color', Color{j},...
             'LineWidth', LineWidth(j));
    end
    xlabel(strvarexpand('|$list_of_response{i}$-$list_of_response{i}$_{ DNS}|'))
    if i==1
        ylabel('coord')
    end
    x_lim=xlim;
    hh_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    xlim(x_lim);
    if i==5
        legend([hh, hh_interface],...
            [legend_text(2:end),{'Interface'}]);
    end
end
end
