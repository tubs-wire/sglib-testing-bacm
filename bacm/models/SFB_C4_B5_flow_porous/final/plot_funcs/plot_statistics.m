function plot_statistics(u, y, u_ref, u_add, varargin)
%
% PLOT_PRIOR_AND_POST_STATISTICS
% loads results from parametric scan with the QMC points and the result
% from the update and plots the results
% - minmax, mean and variance regions from the scan and the updated points
% - scattered plot of the converging and not converging points
% - Responses from DNS, from B5 best calibration, and the best MCMC point
%   and their errors.
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional/default inputs

options=varargin2options(varargin);
%[save_results, options]=get_option(options, 'save_results', false);
%[base_path, options]=get_option(options, 'base_path', 'default');
[quant, options]=get_option(options, 'quant', [0.05, 0.95]);
[plot_quant, options]=get_option(options, 'plot_quant', true);
[plot_minmax, options]=get_option(options, 'plot_minmax', true);
[legend_text, options]=get_option(options, 'legend', {});
[point_names, options]=get_option(options, 'point_names', {});
check_unsupported_options(options, mfilename);

%%
if ~iscell(u)
    u={u};
end
n_u=length(u);
if isempty(point_names)
    ii=1:n_u;
 point_names= num2str(mat2cell(1:n_u, [1,1]);  
end
%% Initialize plot

list_of_response=load_list_of_response('short');
n_out=size(u,2);
conf_numb=strcat(num2str((quant(2)-quant(1))*100, '%.0f'), '%');

for i=1:n_u
    %% Calculate statistics
    if nargin<3||isempty(u_ref)
        [u_mean, ~, u_min, u_max, u_quant_u, u_quant_l]=...
            get_statistics_from_solution(u, u_ref, 'quant', quant);
    else
        [u_mean, ~, u_min, u_max, u_quant_u, u_quant_l, ...
            mean_e, min_e, max_e, quant_e_u, quant_e_l]=...
            get_statistics_from_solution(u, u_ref, 'quant', quant);
    end
    
    %% Plot statistics minmax region of solutions with prior and posterior points
    
    % plot minmax region of prior solutions
    if plot_minmax
        plot_u_mean_quant(y', u_mean, u_min, u_max, ...
            'line_color', 'k', 'fill_color', 'b','change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.6,...
            'xlabel', 'coord')
        u_mean=[]; %emptied, so that it is not plotted again, if quantiles have to be plotted
        legend_text={'prior solution region','prior mean'};
    end
    hold on
    % plot quantiles
    if plot_quant
        plot_u_mean_quant(y', u_mean, u_quant_u, u_quant_l, ...
            'line_color', 'k', 'fill_color', 'b','change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.4,...
            'xlabel', 'coord')
        legend_text=[legend_text, strcat(conf_numb, 'confidence region')];
        if ~isempty(mean_e)
            legend_text=[legend_text, 'prior mean'];
        end
    end
    % plot minmax region of posterior solutions
    if plot_minmax
        plot_u_mean_quant(y', u_mean_po, u_min_po, u_max_po, ...
            'line_color', 'k:', 'fill_color', 'c', 'change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.8,...
            'xlabel', 'coord')
    end
    % plot quantiles
    if plot_quant
        plot_u_mean_quant(y', [], u_quant_u_po, u_quant_l_po, ...
            'line_color', 'k', 'fill_color', 'c','change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.4,...
            'xlabel', 'coord')
    end
    % plot reference result and interface
    for i=1:n_out
        subplot(1,n_out,i)
        plot(u_ref(:,i), y,'k-.', 'LineWidth', 3)
        x_lim=(xlim);
        xlim(x_lim);
        plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
        if i==1
            if isempty(legend_text)
                legend('prior solution region','prior mean', 'prior 90% confidence region',...
                    'posterior solution region','posterior mean',  'posterior 90% confidence region', ...
                    'DNS solution', 'Interface')
            else
                legend(legend_text)
            end
        end
    end
    %% Plot the errors
    figure
    % plot minmax region of prior solutions
    if plot_minmax
        plot_u_mean_quant(y', mean_e, min_e, max_e, ...
            'line_color', 'k', 'fill_color', 'b','change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.6,...
            'xlabel', 'coord')
        mean_e=[];
    end
    hold on
    % plot quantiles
    if plot_quant
        plot_u_mean_quant(y', mean_e, quant_e_u, quant_e_l, ...
            'line_color', 'k', 'fill_color', 'b','change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.4,...
            'xlabel', 'coord')
    end
    
    % plot minmax region of posterior solutions
    if plot_minmax
        plot_u_mean_quant(y', mean_e_po, min_e_po, max_e_po, ...
            'line_color', 'k:', 'fill_color', 'c', 'change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.8,...
            'xlabel', 'coord')
    end
    % plot quantiles
    if plot_quant
        plot_u_mean_quant(y', [], quant_e_u_po, quant_e_l_po, ...
            'line_color', 'k', 'fill_color', 'c','change_axis', true,...
            'subplot_dim',[1,n_out], 'transparency', 0.4,...
            'xlabel', 'coord')
    end
end
% plot reference result and interface
for i=1:n_out
    subplot(1,n_out,i)
    plot(zeros(length(y),1), y,'k-.', 'LineWidth', 3)
    x_lim=(xlim);
    xlim(x_lim);
    plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    xlabel(strvarexpand('|$list_of_response{i}$-$list_of_response{i}$_{ DNS}|'))
    if i==1
        if isempty(legend_text)
            legend('prior solution region','prior mean', 'prior 90% confidence region',...
                'posterior solution region', 'posterior mean', 'posterior 90% confidence region', ...
                'DNS solution', 'Interface')
        else
            legend(legend_text)
        end
    end
end

