function list_of_response=get_list_of_response(varargin)
options=varargin2options(varargin);
[type, options]=get_option(options, 'type', 'long');
check_unsupported_options(options, mfilename);

switch type
    case 'long'
        list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',...
            'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',...
            'rms reynolds stress (r_{12})'};
    case 'short'
        list_of_response={'v_x', 'r_{11}',...
            'r_{22}', 'r_{33}',...
            'r_{12}'};
end