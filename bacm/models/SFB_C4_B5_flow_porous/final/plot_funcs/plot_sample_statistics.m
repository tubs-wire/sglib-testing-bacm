function f=plot_sample_statistics(u, y, u_ref, varargin)
%
% PLOT_PRIOR_AND_POST_STATISTICS
% loads results from parametric scan with the QMC points and the result
% from the update and plots the results
% - minmax, mean and variance regions from the scan and the updated points
% - scattered plot of the converging and not converging points
% - Responses from DNS, from B5 best calibration, and the best MCMC point
%   and their errors.
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional/default inputs

options=varargin2options(varargin);
%[save_results, options]=get_option(options, 'save_results', false);
%[base_path, options]=get_option(options, 'base_path', 'default');
[quant, options]=get_option(options, 'quant', [0.05, 0.95]);
[plot_quant, options]=get_option(options, 'plot_quant', true);
[plot_minmax, options]=get_option(options, 'plot_minmax', true);
[legend_text, options]=get_option(options, 'legend', {});
[sample_names, options]=get_option(options, 'sample_names', {});
[color_quant, options]=get_option(options, 'color_quant', {});
[color_minmax, options]=get_option(options, 'color_minmax', {});
check_unsupported_options(options, mfilename);

%% Initialize
if ~iscell(u)
    u={u};
end
n_u=length(u);
% define first part of legend texts corresponding to the different samples
if isempty(sample_names)&& n_u>1
    % make 1,2,..n_u string for naming the statistics in the legend
    sample_names=cell(n_u, 1);
    for i=1:n_u
        sample_names{i}= num2str(i);
    end
elseif isempty(sample_names)
    sample_names={''};
end
% set colors
if isempty(color_quant)
    color_quant='byg';
    if length(color_quant)<n_u
        repmat(color_quant,1,ceil(n_u/length(color_quant)))
    end
end
if isempty(color_minmax)
    color_minmax='cgm';
    if length(color_quant)<n_u
        repmat(color_minmax,1,ceil(n_u/length(color_minmax)))
    end
end

% response names
list_of_response=load_list_of_response('short');
%
n_out=size(u{1},2);
%...percentage of confidence region
conf_numb=strcat(' ', num2str((quant(2)-quant(1))*100, '%.0f'), '% ');
if nargin<3
    u_ref=[];
end
%initialize default legend text
legend_text_d={};

for i=1:n_u
    %% Calculate statistics
    
    if nargin<3||isempty(u_ref)
        [u_mean, ~, u_min, u_max, u_quant_u, u_quant_l]=...
            get_statistics_from_solution(u{i}, u_ref, 'quantiles', quant);
    else
        [u_mean, ~, u_min, u_max, u_quant_u, u_quant_l, ...
            mean_e, min_e, max_e, quant_e_u, quant_e_l]=...
            get_statistics_from_solution(u{i}, u_ref, 'quantiles', quant);
    end
    
    %% Plot minmax region
    
    % plot minmax region of prior solutions
    if i==1
    f(1)=figure;
    else
        figure(f(1))
    end
    if plot_minmax
        plot_u_mean_quant(y', u_mean, u_min, u_max, ...
            'line_color', 'k', 'fill_color', color_minmax(i),'change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out],...
            'transparency', 0.6, 'xlabel', 'y')
        u_mean=[]; %emptied, so that it is not plotted again, if quantiles have to be plotted
        legend_text_d=strcat(sample_names{i}, {' solution region',' mean'});
    end
    hold on
    %% plot quantiles
    if plot_quant
        plot_u_mean_quant(y', u_mean, u_quant_u, u_quant_l, ...
            'line_color', 'k', 'fill_color', color_quant(i),'change_axis', true,...
            'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.4,...
            'xlabel', 'y')
        legend_text_d=[legend_text_d, {strcat(sample_names{i}, conf_numb, 'confidence region')}];
        if ~isempty(mean_e)
            legend_text_d=[legend_text_d, {strcat(sample_names{i}, ' mean')}];
        end
    end
    %% Plot the errors
    if i==1
        f(2)=figure;
    else
        figure(f(2))
    end
    if  ~isempty(u_ref)
        % plot minmax region of prior solutions
        if plot_minmax
            plot_u_mean_quant(y', mean_e, min_e, max_e, ...
                'line_color', 'k', 'fill_color', color_minmax(i),'change_axis', true,...
                'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.6,...
                'xlabel', 'y')
            mean_e=[];
        end
        hold on
        % plot quantiles
        if plot_quant
            plot_u_mean_quant(y', mean_e, quant_e_u, quant_e_l, ...
                'line_color', 'k', 'fill_color', color_quant(i),'change_axis', true,...
                'ylabels', list_of_response, 'subplot_dim',[1,n_out], 'transparency', 0.4,...
                'xlabel', 'y')
        end
    end
end
% plot reference result and interface
for j=1:length(f);
    figure(f(j))
    for i=1:n_out
        subplot(1,n_out,i)
        % plot reference solution
        if ~isempty(u_ref)
            if j==1
                plot(u_ref(:,i), y,'k-.', 'LineWidth', 3)
            else
                plot(zeros(length(y),1), y,'k-.', 'LineWidth', 3)
            end
            if i==1&&j==1
                legend_text_d=[legend_text_d, {'Reference solution'}];
            end
        end
        % plot interface
        x_lim=(xlim);
        plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
        xlim(x_lim);
        if i==1
            if isempty(legend_text)
                if j==1;
                legend_text_d=[legend_text_d, {'Interface'}];
                end
                legend(legend_text_d)
            else
                legend(legend_text)
            end
        end
    end
end
