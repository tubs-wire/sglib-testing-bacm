function f=plot_sols_dns_and_b5(coord, u, u_DNS, u_B5, u_best, varargin)

% u - samples of solutions, can be left empty
% u - destinct solutions, example u={u_DNS, u_B5, u_best}
% u_DNS - reference solution
% u_B5  - hand calibration by B5
% u_best - best solution from posterior


% Plot the responses from DNS, from the B5 hand calibration, and best MCMC
options=varargin2options(varargin);
[list_of_response, options]=get_option(options, 'list_of_response', 'default');
[legend_text, options]=get_option(options, 'legend_text', 'default');
check_unsupported_options(options, mfilename);

f=figure();

if strcmp(list_of_response, 'default')
    list_of_response=get_list_of_response('type', 'short');
end
if strcmp(legend_text, 'default')
    legend_text={'DNS', 'B5', 'C4'};
end

for i=1:5
    subplot(2,5,i)
    if ~isempty(u)
        plot(squeeze(u(:,i, :)),coord )
    end
    hold on
    h_dns  = plot(u_DNS(:,i), coord, 'k','LineWidth', 2);
    h_best = plot(u_B5(:,i), coord, 'k-.', 'LineWidth', 1);
    h_ubest= plot(u_best(:,i), coord, 'b--', 'LineWidth',2);
    xlabel(list_of_response{i})
    if i==1
        ylabel('coord')
    end
    x_lim=(xlim);
    xlim(x_lim);
    h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    if i==5
        legend([h_dns, h_best, h_ubest, h_interface],...
            [legend_text,{'Interface'}]);
    end
    % plot the same but with the errors
    subplot(2,5,5+i)
    hold on
    hh_best = plot(abs(u_B5(:,i)-u_DNS(:,i)), coord, 'k-.', 'LineWidth', 1);
    hh_ubest= plot(abs(squeeze(u_best(:,i)-u_DNS(:,i))), coord, 'b--', 'LineWidth',2);
    xlabel(strvarexpand('|$list_of_response{i}$-$list_of_response{i}$_{ DNS}|'))
    if i==1
        ylabel('coord')
    end
    x_lim=xlim;
    hh_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
    xlim(x_lim);
    if i==5
        legend([hh_best, hh_ubest, hh_interface],...
            [legend_text(2:end),{'Interface'}]);
    end
end
end
