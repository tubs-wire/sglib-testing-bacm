function plot_3d_points(Qs,indd, varargin)
if ~iscell(Qs)
    Qs={Qs};
end
ng=length(Qs);
colors='brcgyk';
if length(colors)<ng
    repmat(colors,1,ceil(ng/length(colors)))
end

options=varargin2options(varargin);
[Title, options]=get_option(options, 'Title', '');
[MarkerType,options]=get_option(options, 'MarkerType', repmat('.',1,ng));
[MarkerSize,options]=get_option(options, 'MarkerSize', repmat(5,1,ng));
[Labels,options]=get_option(options, 'Labels', cell(3,1));
[Legends,options]=get_option(options, 'Legends', 'default');
[Color,options]=get_option(options, 'Color', colors(1:ng));
[FontSize,options]=get_option(options, 'FontSize', 12);
check_unsupported_options(options,mfilename);

if nargin<2
    indd=[1,2,5];
end
hold on
for i=1:ng
    q_i=Qs{i};
    plot3(q_i(indd(1),:), q_i(indd(2),:), q_i(indd(3),:), ...
        MarkerType(i), 'Color', Color(i), 'MarkerSize', MarkerSize(i))
end

xlabel(Labels{1})
ylabel(Labels{2})
zlabel(Labels{3})
if strcmp(Legends, 'default')
    legend()
else
    legend(Legends)
end
if ~isempty(Title)
    title(Title)
end

fig=gcf;
set(findall(fig,'-property','FontSize'),'FontSize',FontSize)
all_axis = findall(fig, 'Type', 'Axes');
set(all_axis, 'FontSize', FontSize-1)%'XGrid', 'on', 'YGrid', 'on');

view(50, -30)