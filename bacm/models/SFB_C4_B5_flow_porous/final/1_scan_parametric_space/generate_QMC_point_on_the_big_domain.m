function QMC_points=generate_QMC_point_on_the_big_domain(Q, N, varargin)

% GENERATE_QMC_POINT_ON_BIG_DOMAIN
% generates the so called QMC_POINTS QMC sampling using the Halton-sequence
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional flags

options=varargin2options(varargin);
[flag_plot, options]=get_option(options, 'flag_plot', false);
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
check_unsupported_options(options,mfilename);

%% Sample from distribution with Halton-sequence

qmc_options={'scramble', 'bw', 'n0', 10^7};
QMC_points=Q.sample(N,'mode', 'qmc', 'qmc_options', qmc_options);

%% save points
if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    this_f_name=mfilename('fullpath');
    % filename for results
    filename=get_save_path(base_path, this_f_name, 'QMC_points.mat', 2);
    % save results
    save(filename, 'QMC_points');
end

%% plot the generated points

if flag_plot
    plot_grouped_scatter({QMC_points}, 'Color', 'b', 'Labels', Q.param_plot_names,...
        'FontSize', 12, 'Type', 'pdf');
end

