function island_points=...
    generate_points_around_the_best_hand_calibrated_point(varargin)

% GENERATE_POINTS_AROUND_THE_BEST_HAND_CALIBRATED_POINT
% generates the so called ISLAND POINTS around the best hand
% calibrated point with QMC sampling using the Halton-sequence
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional flags

options=varargin2options(varargin);
[flag_plot, options]=get_option(options, 'flag_plot', false);
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
check_unsupported_options(options,mfilename);

%% This script generates the so called ISLAND POINT, QMC points with Halton
% sequence around the best hand calibrated point

%% I.a Define QMC point distribution

%params for forward calculation
Q = set_prior_paramset('type', 'island_uni');

%% Sample from distribution with Halton-sequence

qmc_options={'scramble', 'bw', 'n0', 10^7};
island_points=Q.sample(1500,'mode', 'qmc', 'qmc_options', qmc_options);

%% save points

if save_results
    if strcmpi(base_path,'default')
        base_path=set_results_path();
    end
    this_f_name=mfilename('fullpath');
    % filename for results
    filename=get_save_path(base_path, this_f_name, 'island_points.mat', 2);
    % save results
    save(filename, 'island_points');
end

%% plot the generated points

if flag_plot
    plot_grouped_scatter({island_points}, 'Color', 'b', 'Labels', Q.param_plot_names,...
        'FontSize', 12, 'Type', 'pdf');
end



%% plot the generated points

if flag_plot
    plot_grouped_scatter({island_points}, 'Color', 'b', 'Labels', Q.param_plot_names,...
  'FontSize', 12, 'Type', 'pdf');
end

%% Save results
if flag_save
    
   save('island_points.mat', 'island_points')
end