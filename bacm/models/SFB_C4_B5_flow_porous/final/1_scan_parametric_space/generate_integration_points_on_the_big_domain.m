function [x_1,x_2, x_3]=generate_integration_points_on_the_big_domain
% GENERATE_INTEGRATION_POINTS_ON_THE_BIG_DOMAIN
% generates sparse (SMOLYAK) integration points on the L shaped domain
% where most converging points are.
%
%
%   Noemi Friedman
%   Copyright 2013, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

beta=SimParameter('beta',UniformDistribution(-10,-6.5), '\beta');
beta_t=SimParameter('beta_T',UniformDistribution(-1,0.1), '\beta_T');
c_t=SimParameter('c_t',UniformDistribution(0,0.2)); 
c_eh=SimParameter('c_eh',UniformDistribution(0,0.3), 'c_{\epsilon^h}');
c_wd=SimParameter('c_wd',UniformDistribution(2,30),  'c_{wd}');
c_dp=SimParameter('c_dp',UniformDistribution(0,0.395),  'c_{d,p}');

RVs=SimParamSet1(beta, beta_t, c_t, c_eh, c_wd, c_dp);
%clear('beta', 'beta_t', 'c_t', 'c_eh', 'c_wd', 'c_dp')

%% Generate integration points and project solution of  middle region of
% the L-shaped domain

%set gpc degree and integration order
p_gpc=4;  %degre of gpc
p_int=p_gpc+1; %number of  points for the univariate integration rule
grid='smolyak'; %'full_tensor' or 'smolyak' integration points

%gPCE of the parameters
[p_alpha, V_p, varserr]=  gpc_expand_RVs(RVs);
% generate integration points for the gPC germs (x_ref) 
[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', grid);
% map integration points to the parameter set
x_1=RVs.gpc_evaluate( p_alpha, V_p, x_ref);

%% Second (rigtht-bottom region of the L-shaped domain)

RVs.set_dist('beta' , {UniformDistribution(-6.5, 0)})
[x_2,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');

% Third (left-upper region of the L-shaped domain)
RVs.set_dist({'beta', 'beta_T' }, {UniformDistribution(-10,-6.5), UniformDistribution(0.1, 1)})
[x_3,~, ~]=RVs.generate_integration_points( 5, 'grid', 'smolyak');

%% Plot the points

gplotmatrix([x_1,x_2, x_3]', [] ,[] ,'blue',[],[],[],[],RVs.param_plot_names,RVs.param_plot_names);
set(gcf,'NextPlot','add');
axes;
h = title('Smolyak integration points used for pseudo-spectral and for colocation methods', 'FontSize', 18);
set(gca,'Visible','off');
set(h,'Visible','on'); 
