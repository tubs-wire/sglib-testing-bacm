function [u_mean, u_var, u_min, u_max, u_quant_u, u_quant_l, ...
    mean_e, min_e, max_e, quant_e_u, quant_e_l]=...
    get_statistics_from_solution(u, u_ref, varargin)
options=varargin2options(varargin);
[quantiles,options]=get_option(options, 'quantiles', [0.05, 0.95]);
check_unsupported_options(options, mfilename);

N=size(u, 3);
n_x=size(u,1);
n_out=size(u,2);
% Initiate memory for QMC statistics

u_mean = zeros(n_x, n_out);
if nargout>1
    u_var  = zeros(n_x, n_out);
    if nargout>2
        u_min=zeros(n_x, n_out);
        u_max=zeros(n_x, n_out);
        if nargout >4
            u_quant_l=zeros(n_x, n_out);
            u_quant_u=zeros(n_x, n_out);
            
            % Mean of deviation from DNS and min max error
            if nargin>1 && ~isempty(u_ref) && nargout >6
                mean_e=zeros(n_x, n_out);
                min_e=zeros(n_x, n_out);
                max_e=zeros(n_x, n_out);
                if nargout>8 && ~isempty(u_ref)
                    quant_e_l=zeros(n_x, n_out);
                    quant_e_u=zeros(n_x, n_out);
                end
            end
            
        end
    end
end

for i=1:n_out
    % Mean and variance
    u_mean(:,i)=mean(squeeze( u(:,i,:) ),2 );
    if nargout>1
        u_var(:,i)=var( squeeze( u(:,i,:)), [],2);
        if nargout>2
            % Min and max
            u_min(:,i)=min( squeeze( u(:,i,:) ),[],2);
            u_max(:,i)=max( squeeze( u(:,i,:) ),[],2);
            if nargout>4
                % Quantiles
                u_quant_l(:,i)=(quantile(( squeeze( u(:,i,:) ) )', quantiles(1)))';
                u_quant_u(:,i)=(quantile(( squeeze( u(:,i,:) ) )', quantiles(2)))';
                              
                % Mean of deviation from DNS and min max error
                if nargin>1 && nargout >6 && ~isempty(u_ref)
                    mean_e(:,i)=mean( squeeze( u(:,i,:) )-repmat(u_ref(:,i),1,N), 2);
                    min_e(:,i)=min  ( squeeze( u(:,i,:) )-repmat(u_ref(:,i),1,N),[],2);
                    max_e(:,i)=max  ( squeeze( u(:,i,:) )-repmat(u_ref(:,i),1,N),[],2);
                    if nargout>8
                        quant_e_l(:,i)=quantile  ( squeeze( u(:,i,:) )-repmat(u_ref(:,i),1,N),quantiles(1),2);
                        quant_e_u(:,i)=quantile  ( squeeze( u(:,i,:) )-repmat(u_ref(:,i),1,N),quantiles(2),2);
                    end
                end
            end
        end
    end
    
end
end