function [V_u, u_i_alpha, sensitivities,  RVs,  meas, coord, x, u, u_mean, u_var, x_1, u_1]=main_UQ(p_gpc, varargin)

options=varargin2options(varargin);
[point_type, options]=get_option(options, 'point_type','scan_point');
[prior_dist, options]=get_option(options, 'prior_dist','default');
[flag_PCE, options]=get_option(options, 'flag_PCE',false);
[plot_mean_var_flag, options]=get_option(options, 'plot_mean_var_flag',false);
[plot_part_vars, options]=get_option(options, 'plot_part_vars',false);
[plot_response_surf, options]=get_option(options, 'plot_response_surf',false);
[separate_points, options]=get_option(options, 'separate_points',[]);
[seed_numb, options]=get_option(options, 'seed_numb',324);
check_unsupported_options(options, mfilename);

%% I.a Define Random Variables
%params for forward calculation
RVs = set_prior_paramset('type', prior_dist);
n=RVs.num_params;
%param_names=RVs.param_names;

%% I.b Define output
%Load experimental solution data
list_of_response={'x-velocity (v_x)', 'rms reynolds stress (r_{11})',...
    'rms reynolds stress (r_{22})', 'rms reynolds stress (r_{33})',...
    'rms reynolds stress (r_{12})'};
list_of_response_short={'v_x', 'r_{11}',...
    'r_{22}', 'r_{33}',...
    'r_{12}'};
n_out=length(list_of_response);
[meas,coord]=load_experiment_sol_and_coord();

%% Load points and solution
[x,u]=load_points_and_solution('point_type', point_type);
if ~isempty(separate_points)
    %shuffled index
    n_sep=ceil(size(x,2)*separate_points);
    [x_new, ind] = shuffle_select_sort(x, 2, n_sep, 'fixed_randseed', seed_numb);
    u_new=u(:,:,ind);
    x_1=x(:,setdiff(1:size(x,2),ind));
    u_1=u(:,:,setdiff(1:size(x,2),ind));
    x=x_new;
    u=u_new;
end
%plot experimental data
% figure
% for i=1:n_out
%     subplot(5,1,i)
%     hold on
%     plot(coord, meas(:,i),'red',  'LineWidth', 2)
%     xlabel('y', 'FontSize', 12)
% end
%% II.1.a Generate gPCE basis for the proxi model

% Define approximating subspace
%p_gpc=7; %set gpc degree
gpc_full_tensor=false; % complete or full tensor gpc basis
if flag_PCE
    V_u=gpcbasis_create(repmat('h',1,RVs.num_params),'p',p_gpc, 'full_tensor', gpc_full_tensor);
else
    V_u=RVs.get_germ;
    V_u=gpcbasis_modify(V_u, 'm', n, 'p',p_gpc, 'full_tensor', gpc_full_tensor);
end


%% Map points to reference coordinate
%Define mapping from RVs to germs (standardly distributed random variables)
if flag_PCE
    x_ref=RVs.params2stdnor(x);
else
    x_ref=RVs.params2germ(x);
end


%% II. Get gPCE coeffs with regression
u_ij=reshape(u, 360*5, []);
A=gpcbasis_evaluate(V_u, x_ref);
u_i_alpha = u_ij/A;

%% Calculate and plot solution moments
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);
u_mean=reshape(u_mean,[],n_out);
u_var=reshape(u_var, [],n_out);
[x_best, u_best]=load_points_and_solution('point_type', 'best_point');


germ_at_best=RVs.params2germ(x_best);
if plot_mean_var_flag
    %plot_u_mean_var(coord, u_mean, u_var, 'fill_color','c', 'line_color','black','transparency', 0.4, 'ylabels',list_of_response, 'subplot_dim', [5,1])
    % quantile of the QMC samples
    u_quant_sample_l=quantile(u, 0.025,3);
    u_quant_sample_u=quantile(u, 0.975,3);
    u_mean_sample=mean(u,3);
    % plot mean and quantile of solution from posterior samples from proxi
    u_quant=gpc_quantiles(V_u, u_i_alpha, 'quantiles', [0.025, 0.975]);
    u_quant_l=reshape(squeeze(u_quant(1,:)),[], n_out); %2.5% quantile
    u_quant_u=reshape(squeeze(u_quant(2,:)),[], n_out); %97.5% quantile
    for i=1:n_out
        figure;
        % plot mean and quantiles
        subplot(2,1,1)
        hold on
        plot_u_mean_quant(coord, u_mean_sample(:,i), u_quant_sample_l(:,i),...
            u_quant_sample_u(:,i), ...
            'fill_color','c', 'line_color','k:','transparency', 0.4, ...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        plot_u_mean_quant(coord, u_mean(:,i), u_quant_l(:,i), u_quant_u(:,i),...
            'fill_color','m', 'line_color','m','transparency', 0.4,...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        plot(coord, meas(:,i), 'k', 'LineWidth',2)
        legend('95% confidence region from QMC samples',...
            'Mean response from QMC samples','95% confidence value from gPCE',...
            'Mean from gPCE','DNS simulation' )
        
        % plot only errors
        subplot(2,1,2)
        plot_u_mean_quant(coord, ...
            u_mean_sample(:,i)- meas(:,i), u_quant_sample_l(:,i)- meas(:,i),...
            u_quant_sample_u(:,i)- meas(:,i), 'fill_color','c', ...
            'line_color','k:','transparency', 0.4, ...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        hold on
        plot_u_mean_quant(coord, ...
            u_mean(:,i)- meas(:,i), u_quant_l(:,i)- meas(:,i), u_quant_u(:,i)- meas(:,i), ...
            'fill_color','magenta', 'line_color','m','transparency', 0.4,...
            'ylabels',list_of_response, 'xlabel', 'coord', 'flag_subplot', false)
        plot(coord, zeros(size(coord)), 'k', 'LineWidth',2)
        legend('95% confidence region from QMC samples',...
            'Deviation from DNS of mean response from QMC samples',...
            '95% confidence value from gPCE', 'Deviation from DNS of mean from gPCE',...
            'DNS simulation reference response')
        
    end
    %Plot samples and DNS data
    figure
    for i=1:n_out
        subplot(1,5,i)
        hold on
        if i==1
            ylabel('coord')
        end
        xlabel(list_of_response_short{i})
        h1=plot(squeeze(u(:,i,1)), coord);
        plot(squeeze(u(:,i,2:end)), coord);
        h2=plot(meas(:,i), coord, 'k', 'LineWidth',2);
        x_lim=xlim;
        h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
        xlim(x_lim);
        if i==1
            legend([h1, h2, h_interface],{'QMC samples', 'DNS data', 'Interface'});
        end
    end
    figure
    % Plot error of the samples
    for i=1:n_out
        subplot(1,5,i)
        hold on
        if i==1
            ylabel('coord')
        end
        xlabel(list_of_response_short{i})
        plot(squeeze(u(:,i,:))-repmat(meas(:,i),1,size(u,3)), coord);
        plot(zeros(size(coord)), coord, 'k', 'LineWidth',2);
        x_lim=xlim;
        h_interface=plot(x_lim, [0.9, 0.9],'k-.', 'LineWidth',2);
        xlim(x_lim);
    end
    set(gcf,'NextPlot','add');
    axes;
    h = title('Deviation of QMC samples from the DNS data', 'FontSize', 18);
    set(gca,'Visible','off');
    set(h,'Visible','on');
    
    %% Plot proxi evaluated at best point
    figure
    u_proxi_at_best=gpc_evaluate(u_i_alpha, V_u, germ_at_best);
    u_proxi_at_best=reshape(u_proxi_at_best, [], n_out);
    for i=1:n_out
        subplot(5,1,i)
        hold on
        plot(u_proxi_at_best(:,i), coord, 'blue',  'LineWidth', 2)
        plot(u_best(:,i), coord, 'c--')
        ylabel('y', 'FontSize', 12)
        
    end
end
%% Sobol partial vars

[part_vars, I_un, ratios, ratio_per_order]=gpc_sobol_partial_vars(u_i_alpha, V_u,  'max_index', 1);
ratios=reshape(ratios,[] ,n_out,n);
part_vars=reshape(part_vars, [] ,n_out,n);

[maxval, maxind] = max(part_vars(:));
[maxxidx, maxyidx, maxzidx] = ind2sub(size(part_vars),maxind);
if plot_part_vars
    figure
    for i=1:n_out
        subplot(1,5,i)
        hold on
        plot(squeeze(part_vars(:,i,:)), coord, 'LineWidth', 2)
        %plot(coord, squeeze(ratios(:,i,:)), 'LineWidth', 2)
        if i==n_out
            legend(RVs.param_plot_names)
            ylabel(coord, 'FontSize', 12)
        end
        plot([coord(maxxidx), coord(maxxidx)],xlim)
        xlabel(list_of_response{i})
    end
end
sensitivities=struct;
sensitivities.part_vars=part_vars;
sensitivities.ratios=ratios;
sensitivities.I_un=I_un;

%% Plot response surfaces
if plot_response_surf
    
    %ind_tosee=sub2ind([size(u,1), size(u,2)], 110, 4);
    ind_tosee=sub2ind([size(u,1), size(u,2)], maxxidx, maxyidx);
    response_name=list_of_response(  ceil(  ind_tosee/length(coord)  )  );
    germ2param_func=@(xi_i)(RVs.germ2params(xi_i));
    optional_inputs.name_of_response=response_name;
    optional_inputs.name_of_RVs=RVs.param_plot_names;
    optional_inputs.germ2param=germ2param_func;
    optional_inputs.germ_index=[1,2,6];
    optional_inputs.plot_fix_response=meas(maxxidx, maxyidx);
    optional_inputs.germ_fix_vals=germ_at_best;
    plot_multi_response_surface(u_i_alpha(ind_tosee, :), V_u, optional_inputs)
    %multiplot_init(4,4)
    for i=[1,2,6]
        for j=[1,2,6]
            multiplot
            limax = axis;
            hold on
            if i==j
                plot(x(i,:), u_ij(ind_tosee,:),'x')
            else
                plot3(x(i,:), x(j,:),u_ij(ind_tosee,:),'x')
            end
            axis(limax);
        end
    end
end

%% Test the 'BEST' value
% names=RVs.param_names;
% means=[-5; 0.7; 0.11;0;5;0.2];
% germ_val=zeros(size(means));
% for i=1:length(RVs2germ_func)
%     germ_val(i)=RVs.simparams.(names{i}).param2germ(means(i));
% end
% u_best=gpc_evaluate(u_i_alpha, V_u, germ_val);
% u_best=reshape(u_best,[],n_out);
%%
end
% function [ind, x_shuffled, u_shuffled]=shuffle(x,u)
% rng(1,'twister');
% ind=randperm(size(x,2));
%
%     x_shuffled=x(:,ind);
%     u_shuffled=u(:,:,ind);
% end
