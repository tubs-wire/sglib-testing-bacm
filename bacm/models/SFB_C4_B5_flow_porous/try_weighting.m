Q1=SimParamSet();
Q1.add('p1',NormalDistribution(0,1));
Q2=SimParamSet();
Q2.add('p2',NormalDistribution(1,1));
x=linspace(-3,4,100);
p1=Q1.pdf(x);
p2=Q2.pdf(x);
plot(x,[p1;p2]);

N_1=10000;
N_2=20000;

x1=Q1.sample(N_1);
x2=Q2.sample(N_2);
plot_grouped_scatter({x1, x2});

% kernel densitites
[xk1,p1k1]=kernel_density(x1, 1000);
[xk2,p2k2]=kernel_density(x2, 1000);

% plot kernel densities
figure
plot(xk1,pk1)
hold on 
plot(xk2,pk2)


% compute w2 from probabilities of x2 by interpolating with kernel densitiy

p1x2=interp1q([-inf;xk1;inf],[0;p1k1;0],x2');
p2x2=interp1q([-inf;xk2;inf],[0;p2k2;0],x2');
plot(x2,p1x2, '.')
legend('kernel1', 'kernel2', 'weight2');

w2=p1x2./p2x2;

% compute w1
w1=ones(N_1,1);

%new samples
xu=[x1,x2];

alpha=1/sum([w1;w2]); % N_1/(N_1+N_2)*
beta=1/sum([w1;w2]); %N_2/(N_1+N_2)*


[xku, puxu]=kernel_density(xu, 1000);
plot(xku,puxu)

% Build surrogate of u=x with regression
V_q2=Q2.get_germ;
V_q=gpcbasis_modify(V_q2, 'p',2);

w=zeros(N_1+N_2,1);
w(1:N_1)=w1*alpha;
w(N_1+1:end)=w2*beta;

u=xu;
A=gpcbasis_evaluate(V_q, Q2.params2germ(xu));


W = diag(w);
u_i_alpha =((A*W*A')\(A*W*u'))';

p_u=gpc_pdf_1d(u_i_alpha, V_q, x);
plot(x, p_u, 'red');
