clear variables;
clf;
grey = [0.8  0.8  0.8];
rand_seed(864);
clear
clc
grey = [0.8  0.8  0.8];
Colors=set_my_favorite_colors();

%% Options

%Choose update method( EnKF1 = with sampling / EnKF2 = sampling free )
update_methods = {'MMSE', 'MCMC', 'enKF', 'gpcKF'};
update_method_flag = update_methods{4};
% MMSE esimator order if used
p_phi=3;
% scaling of the displacement field from meters (so that the max true displacement is 1)
scale_u= -1/0.002924353;

% Measurement std (2.0 mms)
sigma_m= 2.0*10^(-4);
% number of posterior samples
N=10000;
% Which plots are needed?
plot_response_surf_flag=false;
plot_qunatiles_and_meas_flag=true;
plot_histograms_flag=true;
save_histograms_flag=true;
%Do update with low rank representation of the measureable?
project_flag=false;

%% Set path where the blackbox results are saved

% Pathname for loading solutions and predifined indices
if isunix
    data_pathname='/home/noefried/Documents/Gas_platform_Claudia/RField';
elseif ispc
    data_pathname='F:\noemi\Documents\Claudia_gas_platform\RField';
end

%% Maps of response and measurement error for better update

% apply a map on the displacement field (for better update, or other
% reason)
response_map=@(u)(u*scale_u); %for no map;
% Define the inverse map
inv_response_map=@(u)(u/scale_u);

%% Get proxi model and prior

% gPCE of the f_cm logfield and solution, mesh information, and map to get the f_cm field
[lf_i_alpha, V_lf, logfield2f_cm, f_cm2logfield, pos, els, ind, u_j_alpha, V_u, r_i_k]...
    = test_mesh_generation_v1('data_pathname', data_pathname, 'map_u',response_map);
% Statistics of the displacement field
[u_mean, u_var] = gpc_moments(u_j_alpha, V_u);
u_quant=(gpc_quantiles(V_u, u_j_alpha, 'quantiles', [0.023, 0.977]))';
% Check sensitivities (how the variance of the solution is influenced by
% the different scaling of the eigenfunctions
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(u_j_alpha, V_u, 'max_index',1);
n_germ=gpcbasis_size(V_lf, 2);

%% Get the true reference displ (u)
% Specicify the fcm true
%F=load(fullfile(data_pathname,'true-field-ln-1.89-5.0.mat'));
%f_cm_true = F.u_true;
% Load the solution for fcm_true
    %U=load(fullfile(data_pathname,'true_displ_field.mat'));
    %u_true=U.Trueuzfieldln1(:,3);
    %logfcm_true = f_cm2logfield(f_cm_true);
% Instead I cheat,and create the measurement from the proxi (the sample
% from the germ is saved and here loaded from the data file)
    X=load(fullfile(data_pathname,'cheat_xi_true'));
    xi_true=X.xi_true;
    u_true = (gpc_evaluate(u_j_alpha, V_u, xi_true));
    lf_cm_true=gpc_evaluate(lf_i_alpha, V_lf, xi_true);
    f_cm_true=logfield2f_cm(lf_cm_true);
    
    %% Create synthetic data from the reference
% Use only points where displacement is not too close to 0
ind_meas=u_true>0.01;
% Number of measurements
n_meas=sum(ind_meas);
% Get the true disp at the assimilation points
y_true=u_true(ind_meas);

% gPCE of the measurable
y_j_alpha=u_j_alpha(ind_meas,:);
V_y=V_u;

% Error model
% make a separate simparamset for the measurement errors
sigma_E=ones(n_meas,1)*sigma_m*abs(scale_u);
E_m=generate_stdrn_simparamset(sigma_E);
E=generate_stdrn_simparamset(sigma_E*1.2);
E_func = @(xi)(E.germ2params(xi));
y_m=y_true+E_m.sample(1);

%%
if plot_qunatiles_and_meas_flag
    %% Plot Quantiles, error model, the truth, and the measurements
    figure;
    plot_u_mean_quant(1:60,u_mean,u_quant(:,1), u_quant(:,2), 'xlabel', 'i_x', 'ylabels', {'displacement'});
    hold on
    all_indices=1:60;
    plot(all_indices(ind_meas), y_m, 'cX')
    plot_u_mean_var(all_indices(ind_meas),u_true(ind_meas), sigma_E.^2,'times_sigma',2,'fill_color','c', 'transparency', 0.4, 'line_color', 'r')
    plot(all_indices, (u_true), 'r', 'LineWidth', 2);
    legend('95% confidence region', 'Mean displacement', 'Measurement', 'Measurement error','True displacement')
    %%    Plot realizations, u_true, and the same with the proxi (but with cheating, so it has to be the same)
    figure
    xi_i = gpcgerm_sample(V_u, 100);
    u_i = (gpc_evaluate(u_j_alpha,V_u,xi_i));
    h3=plot(1:60, u_i, 'color', grey);
    % Plot the true
    u_true_gpce = (gpc_evaluate(u_j_alpha, V_u, xi_true));
    hold on
    h1=plot(1:60,u_true_gpce,'c','LineWidth',2);
    h2=plot(1:60,u_true,'m:','LineWidth',2);
    title('Realizations of displacement')
    xlabel('i')
    ylabel('displacement')
    legend([h1,h2, h3(1)], 'gPCE at f_{cm true}', 'True displacement', 'Realizations')
    %% Plot sensitivities
    % For the legend with xi_i
    xi_vect=cell(n_germ,1);
   for i=1:n_germ
       xi_vect{i}=strvarexpand('\xi_{$i$}');
   end
    lso = {'- '; '--'; ': '; '-.'}; % define a line style order
    lso = repmat(lso, 3,1); % define a line style order
    figure
      % Plot partial variances
    subplot(2,1,1)
    gfig=gca;
    gfig.LineStyleOrder=lso;
    gfig.NextPlot='add';
    plot(1:60, u_var, 'LineWidth', 3)
    plot(1:60, partial_var, 'LineWidth', 2)
    title('Partial variances from uncertainties of eigenfunction scaling')
    xlabel('ix')
    legend('Total variance', xi_vect{:})
    
    % Plot sobol sensitivities
    subplot(2,1,2)
    gfig=gca;
    gfig.LineStyleOrder=lso;
    gfig.NextPlot='add';
    plot(1:60, ones(1,60), 'LineWidth', 3)
    plot(1:60, ratio_by_index, 'LineWidth', 2)
    title('Sobol sensitivities to variations of eigenfunction scaling')
    xlabel('ix')
    legend('Total sum', xi_vect{:})
          
    
end

%% If projection (low rank representation)
if project_flag
    % find eigenfunctions and eigenmodes and number of the modes
    y_j_mean=gpc_moments(y_j_alpha, V_y);
    %     [r_i_k, sigma_k]=svd(y_j_alpha_fluct);
    %
    %     sigma_k=diag(sigma_k);
    %     plot(sigma_k(2:end));
    n_mode=8;
    [r_i_k, sigma_k, y_j_alpha_fluct]=...
        low_rank_from_gPCE(y_j_alpha, V_y, n_mode);
    
    %plot(1:size(y_j_alpha,1), r_i_k(:,1:n_mode)', 'x-')
    %semilogy(sigma_k, 'x')
    plot(cumsum(sigma_k), 'x')
    
    % project with the A matrix:
    A=r_i_k(:,1:n_mode)';
    % change Y
    y_j_alpha=A*y_j_alpha_fluct;
    % change measurement
    y_mp=A*(y_m - y_j_mean);
    % change measurement model
    C_eps=A*diag(E.var)*A';
    L = chol(C_eps, 'lower');
    % Get transformation from standard normal distributed E to eps
    E=generate_stdrn_simparamset(ones(n_mode,1));
    %E_1=generate_stdrn_simparamset(ones(1,n_node));
    E_func = @(xi)(L*E.germ2params(xi));
end 

if project_flag
    y_m=y_mp;
end

%% Updating
% map from germ to measurable
Y_func = @(xi)gpc_evaluate(y_j_alpha, V_y, xi);
% Choose a method
switch(update_method_flag)
    %%
    case 'MMSE'
         % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();
        
        % set MMSE integration orders
        p_lf = gpcbasis_info(V_lf, 'total_degree');
        p_gpc= gpcbasis_info(V_y, 'total_degree');
        p_int_mmse=max(p_gpc*p_phi+1,ceil((p_lf+p_phi*p_gpc+1)/2));
        
        % Update
        [phi_func, ~, lf_cm_post_mean, Y_m_func] = my_mmse_gpc(lf_i_alpha, ...
            Y_func, V_lf, y_m, E_func, V_e, p_phi, p_int_mmse,...
            'int_grid', 'smolyak');
        
        % posterior mean
        f_cm_post_mean = logfield2f_cm (lf_cm_post_mean);
               
        % samples of the posterior
        e_m_i=E.sample(N);
        theta_i=gpcgerm_sample(V_lf,N);
        lf_i =gpc_evaluate(lf_i_alpha, V_lf, theta_i);
        lf_post_samples=lf_i+...
            binfun(@plus,-funcall(phi_func, [theta_i; e_m_i]), lf_i);
        size_f_samples=size(lf_post_samples);
        f_cm_post_samples=reshape(logfield2f_cm(lf_post_samples(:)), size_f_samples);
        f_cm_post_var  = var(f_cm_post_samples, [],2);
                
        %% MCMC from proxi model
    case 'MCMC'
        % get distribution of the germ
        prior_dist=gpcgerm_dists(V_lf);
        %prop_dist={NormalDistribution(0,0.5)};
        % prop_dist=prior_dist;
        % MCMC update
        %[germ_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m, E, 'prop_dist', prop_dist);
        if project_flag
            pdf_error_func=@(u,z)(multi_normal_pdf( u-z, zeros(n_mode, 1), C_eps));
            [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
                pdf_error_func);
        else
            [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
                E, 'prop_dist', prop_dist);
        end
        
        % plot chain (One should mannually cut the burn in perior here)
        plot(theta_samples_post')
        legend(xi_vect)
        lf_post_samples = gpc_evaluate(lf_i_alpha, V_lf, theta_samples_post);
               
        size_f_samples=size(lf_post_samples);
        f_cm_post_samples=reshape(logfield2f_cm(lf_post_samples(:)), size_f_samples);
        f_cm_post_mean  = mean(f_cm_post_samples,2);
        f_cm_post_var  = var(f_cm_post_samples, [],2);
    case 'gpcKF'
        % gPCE of the meas error model
        [E_j_beta, V_E]=E.gpc_expand();
        if project_flag
            E_j_beta=L*E_j_beta;
        end
        % Define identity map: germ to Theta
        Theta_i_alpha=zeros(gpcbasis_size(V_y))';
        Theta_i_alpha(:, 2:end)=eye(n_germ);
        %gPCE of the updated Theta
        [Thetap_i_gamma, V_Thetap] = gpc_KF(Theta_i_alpha, y_j_alpha, V_y, E_j_beta, V_E, y_m);
        %sample from the updated THETA
        theta_samples_post=gpc_sample(Thetap_i_gamma, V_Thetap, N);
        lf_post_samples = gpc_evaluate(lf_i_alpha, V_lf, theta_samples_post);
               
        size_f_samples=size(lf_post_samples);
        f_cm_post_samples=reshape(logfield2f_cm(lf_post_samples(:)), size_f_samples);
        f_cm_post_mean  = mean(f_cm_post_samples,2);
        f_cm_post_var  = var(f_cm_post_samples, [],2);                  
        %% updating the coefficients (without samples) - Bojana's update
    case 'enKF'
        %gPCE of the updated germ
        % gPCE expansion of the germ
        XI=gpcgerm2simparamset(V_lf);
        [xi_i_alpha,V_xi]=XI.gpc_expand();
        %update
        [xi_a_i_alpha, V_xi_a] = gpc_update_enKF_1(xi_i_alpha, V_xi, e_i_alpha, V_e , y_i_alpha, V_y, y_m);
        %sample from the updated germ
        germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
        lf_cm_post_samples = gpc_evaluate(lf_i_alpha, V_lf, germ_samples_post);
        lf_cm_post_mean = mean(lf_cm_post_samples,2);
        lf_cm_post_var  = var(lf_cm_post_samples, [], 2);
end
%% Write and plot update results

%%
xi_prior_samples=gpcgerm_sample(V_y,10000);
figure
x_lim=[0,14000];
y_lim=[0,10000];
    subplot(1,3,1)
    plot_field(pos, els, f_cm_true)
    title('true f_{cm}')
    z_lim=zlim;
    xlim(x_lim)
    ylim(y_lim)
    
    subplot(1,3,2)
    %f_cm_post_mean=logfield2f_cm(lf_cm_post_mean);
    
    %plot_field(pos, els, 5.5*ones(size(f_cm_post_mean)));
    %hold on
    plot_field(pos, els, f_cm_post_mean);
    %plot_field(pos, els, f_cm_true);
    %caxis(z_lim);
    title('mean of posterior f_{cm}')
    %legend ('prior', 'posterior')
    zlim(z_lim)
    xlim(x_lim)
    ylim(y_lim)
    
    subplot(1,3,3)
    %f_cm_post_var=logfield2f_cm(lf_cm_post_var);
    
    %plot_field(pos, els, 10*ones(size(f_cm_post_mean)));
    %hold on
    plot_field(pos, els,f_cm_post_var )
    title('variance of posterior log(f_{cm})')
    %legend ('prior', 'posterior')
    %zlim(z_lim)
    xlim(x_lim)
    ylim(y_lim)
    
%% Plot 3 most influentiable eigenfunctions
figure
if update_log_disp_flag
    j_r=[1,2,5];
else
    j_r=[1,5,6];
end
for i=1:3
    subplot (1,3,i)
    plot_field(pos, els,r_i_k(:,j_r(i)))
    title(strvarexpand('Eigenfunction scaled by \xi_{$j_r(i)$}'))
end

% %% Sample for Claudia's ES update
% %Sample from the germ X_i
% f_cm_i = Q.sample(1000); % samples from germ (csi)
% xi = Q.params2germ(f_cm_i);
% % Calculate the corresponding u_i from the sampled X_i
% X_F_u = gpc_evaluate(u_i_alpha, V_u, xi);
% % Matrix for the updating (i.e. Ensemble Smoother)
% X_F = [X_F_u; xi];







