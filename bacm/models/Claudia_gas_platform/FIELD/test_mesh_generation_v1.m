function [f_i_alpha, V_f, logfield2f_cm, f_cm2logfield, pos, els, ind, u_i_alpha, V_u, r_i_k]=...
    test_mesh_generation_v1(varargin)

options = varargin2options(varargin);
%[plot_flag, options] = get_option(options, 'plot_flag', false);
%[data_pathname, options] = get_option(options, 'data_pathname', 'F:\noemi\Documents\Claudia_gas_platform\RV');
[data_pathname, options] = get_option(options, 'data_pathname', 'F:\noemi\Documents\Claudia_gas_platform\RV');

[map_u, options] = get_option(options, 'map_u', @(u)(u));
check_unsupported_options(options, mfilename);

nx = 16;
ny = 12;

[xm, x] = linspace_midpoints(-500, 14500, nx-1);
[ym, y] = linspace_midpoints(-500, 10500, ny-1);

[pos, els] = create_rect_mesh(x, y);

% Choose nu between 1 and 2 (realisations with nu=1 would be continuous
% functions and realisations with nu=2 would be one times continously
% differentiable).
nu = 2; 
l_c = 4000;
sigma = 1;

cov_func = funcreate(@matern_covariance, nu, funarg, funarg, l_c, sigma);
%cov_func = funcreate(@gaussian_covariance, funarg, funarg, l_c, sigma);
% This is not used here, as the eigenvalues and eigenvectors are loaded
% from Claudia's code to keep identity
%C = covariance_matrix(pos, cov_func);
%G_N = mass_matrix(pos, els);
L = 11;

%[r_i_k,sigma_k,L]=kl_solve_evp(C, G_N, L);
R=load(fullfile(data_pathname, 'r_i_k.mat'));
S=load(fullfile(data_pathname,'sigma_k.mat'));

r_i_k=R.r_i_k;
sigma_k=S.sigma_k;

% s = cumsum(sigma_k.^2);
% plot(s(1:30)/sum(sigma_k.^2)*100, 'x', 'LineWidth', 3)
% hold on
% plot([0,30],[92,92])
% 
% multiplot_init(3,3)
% for i = 1:9
%     multiplot
%     plot_field(pos, els, r_i_k(:, i))
%     xlabel('x')
%     ylabel('y')
%     zlabel(strvarexpand('r_$i$'))
%     shading interp
%     
% end

D = (max(x)-min(x))*(max(y)-min(y));
var_ex = D * sigma^2; 
var_act = sum(sigma_k.^2);
err_var = (var_ex - var_act) / var_ex;
%strvarexpand('Error in variance: $err_var*100$%');

% keep the variance high (kind of cheating)
sigma_k = sigma_k * sqrt(var_ex / var_act);

%%
% Make a PCE from the KLE 

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);

% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

f_i_k = binfun(@times, r_i_k, sigma_k);

%%
mean_f = 5.5;
var_f = 10.0;

% Parameters of the lognormal field
mu = log((mean_f^2)/sqrt(var_f+mean_f^2));
sig = sqrt(log(var_f/(mean_f^2)+1));

% x = linspace(0, mean_u+2*var_u);
% clf
% plot(x, lognormal_pdf(x, mu, sig));

%[mean_f_, var_f_] = lognormal_moments(mu, sig);
Q=SimParamSet();
Q.add('f_cm', LogNormalDistribution(mu,sig));

%%
% create and show integration points
V_f = gpcbasis_create('H', 'I', I);
[xi,w] = gpc_integrate([], V_f, 2, 'grid', 'smolyak');  % sparse grid

f_i_alpha=f_i_k* theta_k_alpha;
% field f_cm from integration points

%logfield=gpc_evaluate(f_i_alpha, V_f, xi);
logfield2f_cm = @(logfield)(Q.germ2params(logfield')');
f_cm2logfield = @(f_cm)(Q.params2germ(f_cm')');

%compute f_cm_fields where the code have to be run
f_cm_field_i=zeros(size(f_i_alpha,1), length(w));
for i=1:length(w);
    f_cm_field_i(:,i)=logfield2f_cm(gpc_evaluate(f_i_alpha, V_f, xi(:,i)));
end
%plot the first 8 fields

for i=1:8
    subplot (2,4,i)
    plot_field(pos, els, f_cm_field_i(:,i), 'view', 3 )
    zlim([3,7])
end

%%
% select internal points
ind = (pos(1,:)>=0 & pos(1,:)<=14000 & pos(2,:)>=0 & pos(2,:)<=10000);
% This was used only for ploting:
%f_int_field(~ind,:)=0;

%%

V_u=V_f;
U=load(fullfile(data_pathname,'displfield'));
u=U.displfield;
u_maped=funcall(map_u,u);
u_i_alpha = calculate_gpc_coeffs(V_u,  xi, u_maped, [], 'method', 'colloc');
end



