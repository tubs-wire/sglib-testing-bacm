% number of the samples of 'u' for the ensemble Kalman
n = 100;
% load initial gpc bases (V_u) and integration points (X_i) and
% weights (W_i)
load('/home/noefried/Gas_platform_Claudia/pre_info.mat');
% load solution from the simulation
%[X  Y  u_z]= load_results_to_matlab(save_pathname);
load('/home/noefried/Gas_platform_Claudia/output_all.mat')

% calculate gPCE of the displacement
u_i_alpha = calculate_gpc_coeffs(V_u, x_i, u_z, w_i, 'method', 'project');

%% Statistics 
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);

%% Get Covariance matrix and sampling
P = gpc_covariance(u_i_alpha, V_u); 
H=[eye(length(u_mean)),zeros(length(u_mean),1)];

%displacements

save('covariance.dat','P','-ascii');
save('mean.dat','u_mean','-ascii');

% Plot the mean and the variance on the FE grid
%scatter(X,Y);


% Sample from the germ X_i
% xi = gpcgerm_sample(V_u, n); % samples from germ (csi)
% 
% % Calculate the corresponding u_i from the sampled X_i
% X_F_u = gpc_evaluate(u_i_alpha, V_u, xi);
% phi_i = gpc_evaluate(phi_alpha, V_phi, xi);
% 
% % Matrix for the updating (i.e. Ensemble Smoother)
% X_F = [X_F_u; phi_i]; 
% 
% save('forecast_matrix.dat','X_F','-ascii');

%% Get Covariance matrix (combine for model response and parameters)
M = gpcbasis_size(V_u,1);
ZZ = zeros(1,(M-2));
% Scaling of u_i_alpha coeff
u_i_alpha = -100 * u_i_alpha(:,1:2);
V_u{2}=V_u{2}(1:2,:);
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
%v_alpha = [u_i_alpha;phi_alpha,ZZ];
v_alpha = [u_i_alpha;phi_alpha];
Pnp = gpc_covariance(v_alpha, V_u);

%% Get the measurement error covariance matrix
%load('meas_all.mat');
load('C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\Code\sglib-testing-master\meas.mat')
z = -100 * meas;
%R = eye(length(z));
V_e = gpcbasis_create('H', 'm', 1, 'p', 1);
estd = 0.1;
%v_j_gamma = estd * [zeros(length(u_mean),1),eye(length(u_mean))];
v_j_gamma = estd * [zeros(length(u_mean),1),ones(length(u_mean),1)];
%R = R * gpc_covariance(v_j_gamma, V_e);
%% Update with MMSE functions on GPC variables

% Define a priori model for xn: 
%   xn = F * x + B * u + w
% (The tricky thing is that the separate probalility spaces for x and w
% have to be combined into one, and during the function evaluation split up
% again, so that the x and w part only get "their" variables)
% [V_xnp, ~, ~, xi_x_ind, xi_w_ind] = gpcbasis_combine(V_x, V_w, 'outer_sum');
% xnp_func = @(xi)(...
%     F * gpc_evaluate(x_i_alpha, V_x, xi(xi_x_ind, :)) + ...  % F * x +
%     repmat(B*u, 1, size(xi,2)) + ...                         % B * u +
%     gpc_evaluate(w_i_beta, V_w, xi(xi_w_ind, :)));           % w

% Just another way to do it, by combining the GPC expansions and then
% making a function out of it
%[xw_ii_gamma, V_xnp] = gpc_combine_inputs(v_alpha, V_u, w_i_beta, V_w);
% xnp_i_gamma = [F, I] * xw_ii_gamma;
% xnp_i_gamma(:,1) = xnp_i_gamma(:,1) + B*u;
xnp_func = gpc_function(v_alpha, V_u);

% In-between check that the predicted state xnp and covariance Pnp match
assert_equals(gpc_moments(v_alpha, V_u), [u_mean;5.5], 'predicted state estimate xnp')
assert_equals(gpc_covariance(v_alpha, V_u), Pnp, 'predicted covariance estimate Pnp')

% Define observation model (without the v, that comes extra)
%   z = H * xn
y_func = @(xi)(H * funcall(xnp_func, xi));

% In-between check of the covariance matrices of the observation
%assert_equals(gpc_covariance(H*v_alpha, V_u)+R, S, 'innovation covariance S')

% Define error model (must be defined separately from observation model)
v_func = gpc_function(v_j_gamma, V_e);

% Now call the MMSE update procedure
p_phi=2; %we choose the estimator to be linear
p_gpce_u=2;
p_int_mmse=5; % 
p_xn=2;
p_int_proj=5;
[xn_i_beta, V_xn]=mmse_update_gpc(v_alpha, y_func, V_u, z, v_func, V_e, p_phi, p_int_mmse, p_xn, p_int_proj);

[xn_mean xn_var] = gpc_moments(xn_i_beta, V_xn);

% Final check of Kalman estimate and covariance update
%assert_equals( gpc_moments(xn_i_beta, V_xn), xn, 'updated state estimate xn');
%assert_equals( gpc_covariance(xn_i_beta, V_xn), Pn, 'updated covariance estimate Pn');

% ... and you see that the classical Kalman filter is exactly reproduced.
% The MMSE is completely independent of degree and in the limit case of all
% degrees equal to 1 reproduces the Kalman filter (or linear observation
% processes).

%% Plots
% u_mean_=reshape(u_mean,10,6);
% surf(u_mean_);
% figure;
% contourf(u_mean_);

% file_name='C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\Code\sglib-testing-master\Grid-Rand-field.mat';
% [X  Y]= load_coords_to_matlab(file_name);
% %Correlation length
% l_c=4000;
% base_cov_func = @gaussian_covariance;
% cov_func = funcreate(base_cov_func, funarg, funarg, l_c);       
% C=covariance_matrix([X';Y'], cov_func); 


