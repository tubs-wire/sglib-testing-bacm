clear

N=1000;

%% Define stuff
nq = 4;
ny = 30;

% Define the number of random variables x, w, and v are modelled upon
mq = 3;
me = 30;

% Now define some random models for them
V_q = gpcbasis_create('P', 'm', mq, 'p', 1);
q_i_alpha = gpc_rand_coeffs(V_q, nq);
%q_i_alpha(:,2:6)=eye(5);
%q_i_alpha(:,7:end)=0;

V_e = gpcbasis_create('P', 'm', me, 'p', 1);
e_j_gamma = gpc_rand_coeffs(V_e, ny, 'zero_mean', true);

% Recover the covariance matrices from here
C_e = gpc_covariance(e_j_gamma, V_e);

H = rand(ny, nq);
q_true = gpc_sample(q_i_alpha, V_q, 1);
%gpcgerm

y_func = @(q)(H*q);
z_m = y_func(q_true);

q_mean = gpc_moments(q_i_alpha, V_q);
C_q = gpc_covariance(q_i_alpha, V_q);

%% Check enKF
q_en = gpc_sample(q_i_alpha, V_q, N);
e_en = gpc_sample(e_j_gamma, V_e, N);
q_p_en=enKF(q_en, y_func, e_en, z_m, 'cz_from_z', false);
q_p2_en=enKF(q_en, y_func, e_en, z_m, 'cz_from_z', true);

%% Check with projecting to eigenvalues
C_y=covariance_sample(H*q_en);
n_mode=7;
[r_i_k,sigma_k]=kl_solve_evp(C_y, [],n_mode);
y_mean=mean(H*q_en, 2);
%A=inv(diag(sigma_k))*r_i_k';
A=r_i_k';
Ay_func = @(xi)(A * binfun(@minus, funcall(y_func, xi), y_mean));    
q_p3_en=enKF(q_en, Ay_func, A*e_en, A*(z_m - y_mean), 'cz_from_z', false);

%% Elmar's low rank check

 [U,S,~]=svd(H*q_en,0);
 s=diag(S);
 L=7;
 sum(s(1:L))/sum(s)
 U=U(:,1:L);
 yU_func = @(xi)(U' * funcall(y_func, xi));
 q_p4_en=enKF(q_en, yU_func, U'*e_en, U'*z_m, 'cz_from_z', false);

%% Check KF
[q_p_mean, C_xi_p] = KF(q_mean, C_q, C_e, H, z_m);

%% Check gpc_KF
Y_j_alpha = H * q_i_alpha;
[qp_j_beta, V_qp]=gpc_KF(q_i_alpha, Y_j_alpha, V_q, e_j_gamma, V_e, z_m);

%% Check mixed_KF

%% Compare all
[q_true, q_mean, mean(q_p_en,2), mean(q_p2_en,2), mean(q_p3_en,2), mean(q_p4_en,2), q_p_mean, gpc_moments(qp_j_beta,V_qp)]
%norm(C_xi_p - covariance_sample(xi_p_en),'fro')/norm(C_xi_p,'fro')
