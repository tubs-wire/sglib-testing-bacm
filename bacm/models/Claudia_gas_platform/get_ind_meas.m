function ind = shuffle_select_sort(n_meas, data_pathname)

rand_seed(644);
ind=1:60;
ind=randperm(size(x,2));
    
    x_shuffled=x(:,ind);
    u_shuffled=u(:,:,ind);


shuffle(1:60);
% Load the indeces of the points where the measurements are collected
file_name=fullfile(data_pathname, 'index.mat');
S=load(file_name);
ind = S.index(1:n_meas);
end