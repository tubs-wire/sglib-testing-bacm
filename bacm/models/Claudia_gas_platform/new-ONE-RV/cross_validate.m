function err = cross_validate()
addpath(fileparts(fileparts(mfilename('fullpath'))));
% Define plotting colors for plot
Colors=set_my_favorite_colors();

%% Set solution pathnames
sols_pathname = set_solution_pathnames();

%% Map output to displacements (scaling)
scale_u= -1/0.002924353;
map_u=@(u)(u*scale_u);

%% Evaluate error of the PCE
cross_points = 'qmc_points';
switch cross_points
    case 'all_int_points'
        [Q, xi_j, q_j, u_j]=load_all_int_points();
        u_j = funcall(map_u, u_j);
    case 'qmc_points'
        Q = set_prior();
        % qmc points (cross-validating points)
        q_j = [4.0597    7.6266    3.9689    1.9590    5.4407    4.4104    5.1875   10.2028    1.3420    2.7466];
        %[q_j, ind] = sort([4.0597    7.6266    3.9689    1.9590    5.4407    4.4104    5.1875   10.2028    1.3420    2.7466]);
        
        xi_j = Q.params2germ(q_j);
        file_name=fullfile(sols_pathname,'cross-valid');
        [X,  Y,  u_z]= load_results_to_matlab(file_name, 10);
        u_j = funcall(map_u, u_z);
        ind = [1, 3:10, 2];
        u_j = u_j(:, ind);
end

%% initialize error vector
err=zeros(5,4);
figure
hold on 
%% Try cross validate
imethod='project';
figure
hold on
for i = 1:5
    [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(6, i, imethod, map_u, sols_pathname);
    uh_j =funcall(proxi, xi_j);
    %plot_errors(u_j, uh_j, xi_j)
    err( i, 1) = norm (  sqrt(diag ( (uh_j-u_j)*(uh_j-u_j)' ) ./ diag(u_j*u_j') ));
    %plot(sqrt(diag ( (uh_j-u_j)*(uh_j-u_j)' ) ./ diag(u_j*u_j') ))
end

for i = 1:5
    [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(i+1, i, imethod, map_u, sols_pathname);
    uh_j =funcall(proxi, xi_j);
    %plot_errors(u_j, uh_j, xi_j)
    err( i, 2) = norm (  sqrt(diag ( (uh_j-u_j)*(uh_j-u_j)' ) ./ diag(u_j*u_j') ));
end

imethod='colloc';
for i = 1:5
    [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(6, i, imethod, map_u, sols_pathname);
    uh_j =funcall(proxi, xi_j);
    %plot_errors(u_j, uh_j, xi_j)
    err( i, 3) = norm (  sqrt(diag ( (uh_j-u_j)*(uh_j-u_j)' ) ./ diag(u_j*u_j') ));
end

for i = 1:5
    [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(i+1, i, imethod, map_u, sols_pathname);
    uh_j =funcall(proxi, xi_j);
    %plot_errors(u_j, uh_j, xi_j)
    err( i, 4) = norm (  sqrt(diag ( (uh_j-u_j)*(uh_j-u_j)' ) ./ diag(u_j*u_j') ));
end

display(err*100)

semilogy(1:5, err*100, 'x', 'LineWidth', 2)
%% make table with errors


rel_errs=err*100;
row_names={''};
col_names={'d: PCE degree', 'projection $Q=6$', 'projection $Q=d+1$', 'regression $Q=6$', 'interpolation $Q=d+1$'};
caption='Validation of different surrogate models';
tab_q=cross_valid_latex_table(1:5, rel_errs, row_names, col_names, 'caption', caption);


end
function [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(n, p_gpc, method, map_u, sols_pathname)
% Parameter values and gpc basis
[Q, xi_i, f_cm_i, w_i, V_u] = pre_get_integration_points_and_paramset(p_gpc, 'p_int', n);
% load solution from the simulation
file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X,  Y,  u_z]= load_results_to_matlab(file_name, n);
% Scale
u_i=funcall(map_u, u_z);
% Compute coeffs of the response surface
u_i_alpha = calculate_gpc_coeffs(V_u, xi_i, u_i, w_i, 'method', method);
proxi =@(xi) gpc_evaluate(u_i_alpha, V_u, xi);
end

function plot_errors(u_j, uh_j, xi_j)
clf
for i = 1:10
    
    subplot(2,5,i)
    plot(u_j(:,i), 'LineWidth', 2)
    hold on
    plot(uh_j(:,i))
    ylim([0,8])
    title(strvarexpand('\theta = $xi_j(i)$'))
end
end