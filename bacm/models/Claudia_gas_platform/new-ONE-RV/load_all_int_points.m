function [Q, x_i, q_i, u_i]=load_all_int_points()
x_i = [];
q_i = [];
u_i = [];

sols_pathname = set_solution_pathnames()
for j = 2:6
% Parameter values and gpc basis
n = j;
[Q, xi_i, f_cm_i, w_i, V_u] = pre_get_integration_points_and_paramset(0, 'p_int', n);
% load solution from the simulation
file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X,  Y,  u_z]= load_results_to_matlab(file_name, n);

x_i =  [x_i, xi_i ];
q_i =  [q_i, f_cm_i ];
u_i =  [u_i, u_z ];
end