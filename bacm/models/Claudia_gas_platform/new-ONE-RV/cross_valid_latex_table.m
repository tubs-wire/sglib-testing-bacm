function tab_q=cross_valid_latex_table(ds, rel_errs, row_names, col_names, varargin)
options=varargin2options(varargin);
[caption, options]=get_option(options, 'caption', '');
[label, options]=get_option(options, 'label', '');
[errdigits, options]=get_option(options, 'errdigits', 2);
[label, options]=get_option(options, 'label', '');
check_unsupported_options(options,mfilename);


    n_e = size(rel_errs,2);
    ltable_data=[ds', rel_errs];
    input.data =ltable_data;
    
    % Set column labels (use empty string for no label):
    input.tableColLabels = col_names;
    % Set row labels (use empty string for no label):
    input.tableRowLabels = row_names;
    input.dataFormat = {'%d',1, strvarexpand('%.$errdigits$f'),n_e};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 0;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = caption;
    input.tableLabel = label;
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 0;
    % call latexTable:
    tab_q = latexTable(input);
end