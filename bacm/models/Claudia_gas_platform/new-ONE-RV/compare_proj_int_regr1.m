function h = plot_comparism_of_surrogate_models()

% kth node number displacement
k=25;
% Define plotting colors
Colors=set_my_favorite_colors();

%% Set solution pathnames
if isunix
    sols_pathname='/home/noefried/Documents/Gas_platform_Claudia/RV';
elseif ispc
    sols_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
end

%% Map output to displacements (scaling)
scale_u= -1/0.002924353;
map_u=@(u)(u*scale_u);

%% Node number where displacements are checked
k = 25;
% (Here the displacements are relatively large)
%% Ploting
for ip=0:1
    if ip==0
        map2param=false;
    else
        map2param=true;
    end
    % xi-u
    subplot(3,2,1+ip)
    hold on
    h(1)=plot_response(4, 1, 'project', map_u, k, sols_pathname, Colors.bordeau, '-', 2, 'map2param', map2param);
    h(2)=plot_response(4, 1, 'colloc', map_u, k,  sols_pathname, Colors.cadet_blue, '--', 2, 'map2param', map2param);
    
    h(3)=plot_response(4, 2, 'project', map_u, k, sols_pathname, Colors.navy_blue, '-', 2, 'map2param', map2param);
    h(4)=plot_response(4, 2, 'colloc', map_u, k, sols_pathname, Colors.lavender, '--', 2, 'map2param', map2param);
    
    h(5)=plot_response(4, 3, 'project', map_u, k, sols_pathname, Colors.cadet_blue, ':', 2, 'map2param', map2param);
    [h(6), h(7)]=plot_response(4, 3, 'colloc', map_u, k, sols_pathname, Colors.grey, '-.', 1, 'map2param', map2param);
    
    legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'Int points Q=4')
    if map2param
        xlabel('f_{cM}')
    else
        xlabel('\theta')
    end
    ylabel('PCEs of the nodal displacement a_{h,25}')
    title('a)')
    
    % absolut error at the kth node of xi-u
    subplot(3,2,3+ip)
    hold on
    h(1)=plot_proxi_error(4, 1, 'project', map_u, sols_pathname, Colors.bordeau, '-', 2, 'node_numb', k, 'map2param', map2param);
    h(2)=plot_proxi_error(4, 1, 'colloc', map_u, sols_pathname, Colors.cadet_blue, '--', 2, 'node_numb', k, 'map2param', map2param);
    
    h(3)=plot_proxi_error(4, 2, 'project', map_u, sols_pathname, Colors.navy_blue, '-', 2, 'node_numb', k, 'map2param', map2param);
    h(4)=plot_proxi_error(4, 2, 'colloc', map_u, sols_pathname, Colors.lavender, '--', 2, 'node_numb', k, 'map2param', map2param);
    
    h(5)=plot_proxi_error(4, 3, 'project', map_u, sols_pathname, Colors.cadet_blue, ':', 2, 'node_numb', k, 'map2param', map2param);
    [h(6), h(7)]= plot_proxi_error(4, 3, 'colloc', map_u, sols_pathname, Colors.grey, '-.', 1, 'node_numb', k, 'map2param', map2param);
    
    legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'Int points Q=4')
    if map2param
        xlabel('f_{cM}')
    else
        xlabel('\theta')
    end
    ylabel('Absolut error of the PCEs at node 25 |a_{h}-a_{h,25}|')
    title('b)')
    
    % reltive absolut error xi-u
    subplot(3,2,5+ip)
    hold on
    h(1)=plot_proxi_error(4, 1, 'project', map_u, sols_pathname, Colors.bordeau, '-', 2, 'map2param', map2param);
    h(2)=plot_proxi_error(4, 1, 'colloc', map_u, sols_pathname, Colors.cadet_blue, '--', 2, 'map2param', map2param);
    
    h(3)=plot_proxi_error(4, 2, 'project', map_u, sols_pathname, Colors.navy_blue, '-', 2, 'map2param', map2param);
    h(4)=plot_proxi_error(4, 2, 'colloc', map_u, sols_pathname, Colors.lavender, '--', 2, 'map2param', map2param);
    
    h(5)=plot_proxi_error(4, 3, 'project', map_u, sols_pathname, Colors.cadet_blue, ':', 2, 'map2param', map2param);
    [h(6), h(7)]= plot_proxi_error(4, 3, 'colloc', map_u, sols_pathname, Colors.grey, '-.', 1, 'map2param', map2param);
    
    legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'Int points Q=4')
    if map2param
        xlabel('f_{cM}')
    else
        xlabel('\theta')
    end
    ylabel('Relative error of the PCEs \epsilon_h')
    title('c)')
end
end
function [Q, xi_i, u_i, proxi, V_u]=get_points_and_param(n, p_gpc, method, map_u, sols_pathname)
% Parameter values and gpc basis
[Q, xi_i, f_cm_i, w_i, V_u] = pre_get_integration_points_and_paramset(p_gpc, 'p_int', n);
% load solution from the simulation
file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X,  Y,  u_z]= load_results_to_matlab(file_name, n);
% Scale
u_i=funcall(map_u, u_z);
% Compute coeffs of the response surface
u_i_alpha = calculate_gpc_coeffs(V_u, xi_i, u_i, w_i, 'method', method);
proxi =@(xi) gpc_evaluate(u_i_alpha, V_u, xi);
end

function [h1, h2]=plot_response(Q, M, method, map_u, node_numb, sols_pathname, pcolor, plinetype, pwidth, varargin)
options=varargin2options(varargin);
[map2param, options]=get_option(options, 'map2param', true);
check_unsupported_options(options, mfilename);
[Q, xi_i, u_i, proxi, V_u]=get_points_and_param(Q, M, method, map_u, sols_pathname);
% Linspace from the germ
N_x=100;
xi_j = gpcgerm_linspace(V_u, N_x);
uh_j = proxi(xi_j);

if map2param
    h1 = plot(Q.germ2params(xi_j), uh_j(node_numb, :), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    
    if nargout==2
        h2 = plot(Q.germ2params(xi_i), u_i(node_numb,:), 'kx', 'lineWidth', 2, 'MarkerSize', 10);
    end
else
    h1 = plot(xi_j, uh_j(node_numb, :), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    
    if nargout==2
        h2 = plot(xi_i, u_i(node_numb,:), 'kx', 'lineWidth', 2, 'MarkerSize', 10);
    end
end
end

function [h1, h2]=plot_proxi_error(Q, M, method, map_u, sols_pathname, pcolor, plinetype, pwidth, varargin)
options=varargin2options(varargin);
[node_numb, options]=get_option(options, 'node_numb',[]);
[map2param, options]=get_option(options, 'map2param', true);
check_unsupported_options(options, mfilename);
[Q, xi_i, ~, proxi, V_u]=get_points_and_param(Q, M, method, map_u, sols_pathname);
k=25;
% Linspace from the germ
N_x=100;
xi_j = gpcgerm_linspace(V_u, N_x);
uh_j = proxi(xi_j);
[~, ~, ~, proxi]=get_points_and_param(6, 5, 'project', map_u, sols_pathname);
u_j = proxi(xi_j);
if map2param
    if isempty(node_numb)
        h1 = plot(Q.germ2params(xi_j), sqrt(diag ( (uh_j-u_j)'*(uh_j-u_j) ) ./ diag(u_j'*u_j) ), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    else
        h1 = plot(Q.germ2params(xi_j), abs(uh_j(node_numb,:)-u_j(node_numb,:)), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    end
    if nargout==2
        h2 = plot(Q.germ2params(xi_i), zeros(size(xi_i)), 'kx', 'lineWidth', 2, 'MarkerSize', 10);
    end
else
    if isempty(node_numb)
        h1 = plot(xi_j, sqrt(diag ( (uh_j-u_j)'*(uh_j-u_j) ) ./ diag(u_j'*u_j) ), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    else
        h1 = plot(xi_j, abs(uh_j(node_numb,:)-u_j(node_numb,:)), plinetype, 'LineWidth', pwidth, 'Color', pcolor);
    end
    if nargout==2
        h2 = plot(xi_i, zeros(size(xi_i)), 'kx', 'lineWidth', 2, 'MarkerSize', 10);
    end
end

end