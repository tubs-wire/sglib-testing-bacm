clear
clc
clf

% kth node number displacement
k=25;
%% Set solution pathnames
if isunix
    sols_pathname='/home/noefried/Documents/Gas_platform_Claudia/RV';
elseif ispc
    sols_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
end
Colors=set_my_favorite_colors();
%% Map output to displacements (scaling)
scale_u= -1/0.002924353;
map_u=@(u)(u*scale_u);

%% Get integration/regression/interolation points
% Integration/regression/interolation points with points 
 n=2;
 [Q, xi_i, f_cm_i, w_i, ~] = pre_get_integration_points_and_paramset(n-1);
% %gpc basis
 V_q=Q.get_germ();
 V_u1=gpcbasis_modify(V_q,'p', 1);
 V_u2=gpcbasis_modify(V_q,'p', 2);
% % load solution from the simulation
% file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
% [X,  Y,  u_z]= load_results_to_matlab(file_name, n);
% u_i=funcall(map_u, u_z(k,:));

% Integration/regression/interolation points
n=4;
[~, xi_i4, f_cm_i4, w_i4, ~] = pre_get_integration_points_and_paramset(n-1);
%gpc basis
V_u3=gpcbasis_modify(V_q,'p', 3);
% load solution from the simulation
file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X4,  Y4,  u_z4]= load_results_to_matlab(file_name, n);
u_i4=funcall(map_u, u_z4(k,:));

% Integration/regression/interolation points
n=6;
[~, xi_i6, f_cm_i6, w_i6, ~] = pre_get_integration_points_and_paramset(n-1);
%gpc basis
V_u5=gpcbasis_modify(V_q,'p', 5);
% load solution from the simulation
file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X6,  Y6,  u_z6]= load_results_to_matlab(file_name, n);
u_i6=funcall(map_u, u_z6(k,:));

%% Compute gpc coefficients with different methods and differnt number of points
%u_i_alpha_p2_V1 = calculate_gpc_coeffs(V_u1, xi_i, u_i, w_i, 'method', 'project');
%u_i_alpha_int_p2_V1=calculate_gpc_coeffs(V_u1, xi_i, u_i, w_i, 'method', 'colloc');
u_i_alpha_p4_V1 = calculate_gpc_coeffs(V_u1, xi_i4, u_i4, w_i4, 'method', 'project');
u_i_alpha_regr_p4_V1=calculate_gpc_coeffs(V_u1, xi_i4, u_i4, w_i4,  'method', 'colloc');

u_i_alpha_p4_V2 = calculate_gpc_coeffs(V_u2, xi_i4, u_i4, w_i4, 'method', 'project');
u_i_alpha_regr_p4_V2=calculate_gpc_coeffs(V_u2, xi_i4, u_i4, w_i4,  'method', 'colloc');

u_i_alpha_p4_V3 = calculate_gpc_coeffs(V_u3, xi_i4, u_i4, w_i4, 'method', 'project');
u_i_alpha_int_p4_V3 = calculate_gpc_coeffs(V_u3, xi_i4, u_i4, w_i4, 'method', 'colloc');

u_i_alpha_p6_V5 = calculate_gpc_coeffs(V_u5, xi_i6, u_i6, w_i6, 'method', 'project');

%% Compute response surface from PCE
% Linspace from the germ
N_x=100;
xi_ii = gpcgerm_linspace(V_u3,N_x);
% Evaluate solutions at different germs
uh_ii_p4_V1 = gpc_evaluate(u_i_alpha_p4_V1, V_u1, xi_ii);
uh_ii_regr_p4_V1 = gpc_evaluate(u_i_alpha_regr_p4_V1, V_u1, xi_ii);

uh_ii_p4_V2 = gpc_evaluate(u_i_alpha_p4_V2, V_u2, xi_ii);
uh_ii_regr_p4_V2 = gpc_evaluate(u_i_alpha_regr_p4_V2, V_u2, xi_ii);

uh_ii_p4_V3 = gpc_evaluate(u_i_alpha_p4_V3, V_u3, xi_ii);
uh_ii_int_p4_V3 = gpc_evaluate(u_i_alpha_int_p4_V3, V_u3, xi_ii);

uh_ii_p6_V5 = gpc_evaluate(u_i_alpha_p6_V5, V_u5, xi_ii);

f_cm_ii=Q.germ2params(xi_ii);

%% Ploting

% xi-u
subplot(2,1,1)
hold on
plot(xi_ii, uh_ii_p4_V1, '-', 'LineWidth', 2, 'Color', Colors.bordeau);
plot(xi_ii, uh_ii_regr_p4_V1, '--', 'LineWidth', 2, 'Color', Colors.cadet_blue);

plot(xi_ii, uh_ii_p4_V2, '-', 'LineWidth', 2, 'Color', Colors.navy_blue);
plot(xi_ii, uh_ii_regr_p4_V2, '--', 'LineWidth', 2, 'Color', Colors.lavender);

plot(xi_ii, uh_ii_p4_V3, ':', 'LineWidth', 2, 'Color', Colors.cadet_blue);
plot(xi_ii, uh_ii_int_p4_V3, '-.', 'LineWidth', 1, 'Color', Colors.grey);

%plot(xi_ii, uh_ii_p6_V5, '-', 'LineWidth', 2, 'Color', Colors.greenish_grey);
plot(xi_i4, u_i4, 'kx', 'lineWidth', 2, 'MarkerSize', 10);

legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'Int points Q=4')
xlabel('\theta')
ylabel('a_{h,k}')
title('a)')

% error of xi-u
subplot(2,1,2)
hold on
plot(xi_ii, abs(uh_ii_p4_V1-uh_ii_p6_V5), '-', 'LineWidth', 2, 'Color', Colors.bordeau);
plot(xi_ii, abs(uh_ii_regr_p4_V1-uh_ii_p6_V5), '--', 'LineWidth', 2, 'Color', Colors.cadet_blue);

plot(xi_ii, abs(uh_ii_p4_V2-uh_ii_p6_V5), '-', 'LineWidth', 2, 'Color', Colors.navy_blue);
plot(xi_ii, abs(uh_ii_regr_p4_V2-uh_ii_p6_V5), '--', 'LineWidth', 2, 'Color', Colors.lavender);

plot(xi_ii, abs(uh_ii_p4_V3-uh_ii_p6_V5), ':', 'LineWidth', 2, 'Color', Colors.cadet_blue);
plot(xi_ii, abs(uh_ii_int_p4_V3-uh_ii_p6_V5), '-.', 'LineWidth', 1, 'Color', Colors.grey);

plot(xi_i4, zeros(size(xi_i4)), 'kx', 'lineWidth', 2, 'MarkerSize', 10);

legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'Int points Q=4')
xlabel('\theta')
ylabel('|a_{h}-a_{h,k}|')
title('b)')
% % Plot the response fcm-u
% subplot(3,1,3)
% hold on
% 
% plot(f_cm_ii, uh_ii_p4_V1, '-', 'LineWidth', 2, 'Color', Colors.bordeau);
% plot(f_cm_ii, uh_ii_regr_p4_V1, '--', 'LineWidth', 2, 'Color', Colors.cadet_blue);
% 
% plot(f_cm_ii, uh_ii_p4_V2, '-', 'LineWidth', 2, 'Color', Colors.navy_blue);
% plot(f_cm_ii, uh_ii_regr_p4_V2, '--', 'LineWidth', 2, 'Color', Colors.lavender);
% 
% plot(f_cm_ii, uh_ii_p4_V3, ':', 'LineWidth', 2, 'Color', Colors.cadet_blue);
% plot(f_cm_ii, uh_ii_int_p4_V3, '-.', 'LineWidth', 1, 'Color', Colors.grey);
% 
% plot(f_cm_ii, uh_ii_p6_V5, '-', 'LineWidth', 2, 'Color', Colors.greenish_grey);
% plot(f_cm_i4, u_i4, 'kx', 'lineWidth', 2, 'MarkerSize', 10);
% 
% legend('proj Q=4 M=2', 'regr Q=4 M=2', 'proj Q=4 M=3', 'regr Q=4 M=3','proj Q=4 M=4', 'int Q=4 M=4', 'proj Q=6 M=5', 'Int points Q=4')
% xlabel('f_{cM}')
% ylabel('u_z')
% title('Displacement dependence on the parameter and germ at different coordinates')




