clear all
clc
grey = [0.8  0.8  0.8];
rand_seed(864);
%% Options
%Choose update method( EnKF1 = with sampling / EnKF2 = sampling free )
update_methods = {'MMSE', 'MCMC', 'EnKF1', 'EnKF2'};
update_method_flag = update_methods{3};
% scaling of the displacement field from meters
scale_u= -1/0.002924353;
% order of the gPCE
gpc_order=5;
% Maximum value of measurement error (for the largest displacement)
%sigma_max= 0.0005;
sigma_max= 0.0001;
% number of posterior samples
N=10000;
% Update for the log displacment?
update_log_disp_flag=false;
% Which plots are needed?
plot_response_surf_flag=true;
plot_qunatiles_and_meas_flag=true;
plot_histograms_flag=true;
% Define the number of measurements
n_meas=5;
% Pathname for loading solutions and predifined indices
if isunix
    data_pathname='/home/noefried/Gas_platform_Claudia/RV';
elseif ispc
    data_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
end

%% Maps of response and measurement error for better update
if update_log_disp_flag
% apply a map on the displacement field for better update
    response_map=@(u)(log(u*scale_u));
% Define the inverse map 
    inv_response_map=@(u)(exp(u)/scale_u);
% Gradient of the map, so that the maped error can be calculated
    %J_func=@(u)(abs(scale_u*1./u));
    sigma_func=@(u,sigma)(abs(log(1 + sigma./u)));
else
% apply a map on the displacement field for better update
    response_map=@(u)(u*scale_u); %for no map;
% Define the inverse map 
    inv_response_map=@(u)(u/scale_u); %for no map;
% Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(abs(-scale_u*ones(length(u),1))); %for no map;
end
%% Get proxi model and prior
[u_i_alpha, V_u, Q]=get_proxi_model(gpc_order,...
    'plot_flag',plot_response_surf_flag,...
    'sols_pathname', data_pathname, 'map_u', response_map);
% Statistics of the displacement field
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
u_quant=(gpc_quantiles(V_u, u_i_alpha, 'quantiles', [0.023, 0.977]))';

%% Get the true reference displ (u)
% Specicify the fcm true
f_cm_true = 1.89;
xi_true = Q.params2germ(f_cm_true);
% Load the solution for fcm_true
T=load(fullfile(data_pathname, 'displacement_f_cm_scalar_1_89.mat'));
%%% ASK FROM Claudia:  This file doesn't have u_true, only true!
u_true=T.u_true(1:60,1);

%% Create synthetic data from the reference
% Get the true reference at the assimilation points
[y_true, ind_meas]=shuffle_select_sort(u_true, 1, n_meas, 'fixed_randseed', 644);
y_true_maped=response_map(y_true);
y_i_alpha=u_i_alpha(ind_meas,:);
V_y=V_u;
% Measurement std
sigma_m=sigma_max; %*abs(u_true)/max(abs(u_true));
sigma_meas = sigma_max;%*abs(y_true)/max(abs(y_true)); %!!!! error, here I should devide with umax
% make a separate simparamset for the measurement errors
E_meas=generate_stdrn_simparamset(sigma_meas);
y_m=y_true+E_meas.sample(1);
y_m=response_map(y_m);

%% Error model 
 %sigma_e=J_func(u_true).*sigma_m;    
 %sigma_eps=J_func(y_true).*sigma_meas;
 if exist('sigma_func', 'var')
     sigma_e  =sigma_func(u_true,sigma_m)*1.2;
     sigma_eps=sigma_func(y_true,sigma_meas)*1.2;
 else
     sigma_e  =J_func(u_true).*sigma_m;
     sigma_eps=J_func(y_true).*sigma_meas;
 end
 E=generate_stdrn_simparamset(sigma_eps);

%% Plot quantiles, evaluate proxi model from true response
if plot_qunatiles_and_meas_flag
    % Plot Mean and Variance from the proxi model
    figure;
    plot_u_mean_quant(1:60,u_mean,u_quant(:,1), u_quant(:,2), 'xlabel', 'i_x', 'ylabels', {'displacement'});
    hold on
    plot(1:60, response_map(u_true), 'r', 'LineWidth', 2);
    plot(ind_meas, y_m, 'cX')
    plot_u_mean_var(1:60,response_map(u_true), sigma_e.^2,'times_sigma',2,'fill_color','c', 'transparency', 0.4)
    legend('95% confidence region', 'Mean displacement','True displacement', 'Measurement', 'Measurement error')
    %%    Plot Realizations
    figure
    xi_i = Q.params2germ(Q.sample(100));
    u_i = inv_response_map(gpc_evaluate(u_i_alpha,V_u,xi_i));
    hold on
    h3=plot(1:60, u_i, 'color', grey);
    % Plot the true
    u_true_gpce = inv_response_map(gpc_evaluate(u_i_alpha, V_u, xi_true));
    hold on
    h1=plot(1:60,u_true_gpce,'c','LineWidth',2);
    h2=plot(1:60,u_true,'m:','LineWidth',2);
    title('Realizations of displacement')
    xlabel('i')
    ylabel('displacement')
    legend([h1,h2, h3(1)], 'gPCE at f_{cm true}', 'True displacement', 'Realizations')
    
end

%% Updating
% map from germ to measurable
Y_func = @(xi)gpc_evaluate(y_i_alpha, V_y, xi);
% Choose a method
switch(update_method_flag)
    %%
    case 'MMSE'
        % MMSE update (of Elmar)
        % map from error germ to error
        E_func = @(xi)(E.germ2params(xi));
        % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();
        % gPCE of f_cm
        [f_cm_i_alpha, V_f_cm] = Q.gpc_expand();
        % MMSE order and num integration orders
        p_phi=1;
        p_pn=2;
        p_int_mmse=3;
        p_int_proj=4;
        % Update
        [f_cm_post_i_beta, V_f_cm_post] = mmse_update_gpc(f_cm_i_alpha, ...
            Y_func, V_f_cm, y_m, E_func, V_e, p_phi, p_int_mmse,...
            p_pn, p_int_proj, 'int_grid', 'smolyak');
        % mean and var of the updated f_cm
        [f_cm_post_mean, f_cm_post_var]=gpc_moments(...
            f_cm_post_i_beta, V_f_cm_post);
        % samples of the posterior
        f_cm_post_samples=gpc_sample(f_cm_post_i_beta, V_f_cm_post, N);
        
        %% MCMC from proxi model
    case 'MCMC'
        % get distribution of the germ
        [~, dist]=gpc_registry('get', V_y{1});
        prior_dist = {dist};
        prop_dist={NormalDistribution(0,1)};
        % MCMC update
        [germ_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
            E, 'prop_dist', prop_dist);
        f_cm_post_samples = Q.germ2params(germ_samples_post);
        f_cm_post_mean = mean(f_cm_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        
        %% Updating with the Ensemble Kalman filter
    case 'EnKF1'
        f_cm_post_samples=update_q_enKF(Q, E, y_i_alpha, V_y, y_m, N);
        f_cm_post_mean = mean(f_cm_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        %% updating the coefficients (without samples) - Bojana's update
    case 'EnKF2'
        %gPCE of the updated germ
        [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(Q, E, y_i_alpha, V_y, y_m);
        %sample from the updated germ
        germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
        % map to f_cm
        f_cm_post_samples=Q.germ2params(germ_samples_post);
        %Posterior mean
        f_cm_post_mean=mean(f_cm_post_samples,2);
        %Posterior variance
        f_cm_post_var=var(f_cm_post_samples, [],2);
end
%% Write and plot update results
f_cm_prior_samples=Q.sample(N);
display(strvarexpand('UPDATE RESULTS WITH $update_method_flag$'));
display(strvarexpand('f_cm_true=$f_cm_true$'));
display(strvarexpand('f_cm_post_mean=$f_cm_post_mean$'));
display(strvarexpand('f_cm_prior_var=$Q.var$'));
display(strvarexpand('f_cm_post_var=$f_cm_post_var$'));
if plot_histograms_flag
    % plot prior and posterior histograms
    figure
    h1=histogram(f_cm_prior_samples);
    hold on
    %[f1,p1]=plot_density(f_cm_prior_samples);
    %hp1=plot(f1, p1, 'b' ,'LineWidth',2);
    h2=histogram(f_cm_post_samples);
    %hp2=plot_density(f_cm_post_samples,'r' ,'LineWidth',2);
    h1.Normalization = 'probability';
    h1.BinWidth = 0.05;
    h2.Normalization = 'probability';
    h2.BinWidth = 0.05;
    y_limits=ylim;
    plot([f_cm_true, f_cm_true], y_limits, 'r', 'LineWidth', 3)
    ylim(y_limits);
    xlim([0,10]);
    legend('prior', 'posterior', 'true value');
    xlabel('f_{cm}')
    ylabel('Probability');
    title(strvarexpand('UPDATE WITH $update_method_flag$'));
    % calculate pics
    [val, post_pick_ind]=max(h2.Values);
    pick_bounds=h2.BinEdges([post_pick_ind, post_pick_ind+1]);
    display(strvarexpand('Pick of posterior is between $pick_bounds(1)$ and $pick_bounds(2)$'));

    figure
    hpost=histogram(f_cm_post_samples);
    hpost.FaceColor=[1 .5 0];
    y_limits=ylim;
    hold on
    plot([f_cm_true, f_cm_true], y_limits, 'r', 'LineWidth', 3)
    title('Posterior distribution')
    ylim(y_limits);

end




