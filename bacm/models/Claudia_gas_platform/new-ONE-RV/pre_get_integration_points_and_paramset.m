function [Q, xi_i, f_cm_i, w_i, V_u]=pre_get_integration_points_and_paramset(gpc_order, varargin)

options=varargin2options(varargin, mfilename);
[p_int, options]=get_option(options, 'p_int', gpc_order+1);
check_unsupported_options(options);

Q = set_prior();

% Set approximating subspace for the proxi model
V_q=Q.get_germ();
V_u=gpcbasis_modify(V_q,'p', gpc_order);
%
% Call integration rule
[xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

f_cm_i=Q.germ2params(xi_i);