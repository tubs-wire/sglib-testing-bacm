function [Q, f_cm] = set_prior()
% mean of the lognormally distributed parameter
mean_fcm = 5.5;
% variance of the longnormally distributed parameter
var_fcm = 10.0;
% compute parameters of the distribution (mean and std of the underlying
% Gaussian distribution)
mu = log((mean_fcm^2)/sqrt(var_fcm+mean_fcm^2));
sig = sqrt(log(var_fcm/(mean_fcm^2)+1));
% Define the parameter with the desired distribution
f_cm = MySimParameter('f_cm', LogNormalDistribution(mu,sig));
% Define prior parameter set
Q=MySimParamSet();
Q.add(f_cm);
end