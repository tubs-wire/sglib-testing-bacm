function [X  Y  u_z]= load_results_to_matlab(save_pathname,n)
%% Loading and Reading the GEPS3D simulation files
pathname=pwd;
cd(save_pathname)
files=dir();
j_start = find(strcmp({files.name}, 'Regina.1') == 1)-1;
X = zeros(60,1);
Y = zeros(60,1);
u_z = zeros(60,n);

for j=1:n
file_name=files(j+j_start).name;
fileID=fopen(file_name);

save_filename='output_all.mat';

for i=1:61
    temp1 = fgetl(fileID);
    if any(i<2)
        inputtext = textscan(temp1, '%d');
    else
        inputtext = textscan(temp1, '%d %d %f');
        X(i-1) = inputtext{1};
        Y(i-1) = inputtext{2};
        u_z(i-1, j) = inputtext{3};
    end
%     temp1=fgetl(fileID);
%     if any(i<6)
%         inputtext = textscan(temp1, '%d');
% %        temp_out = inputtext{:};
% %        nnode = temp_out;
%      else
%        inputtext = textscan(temp1, '%f %f %f %f %f  %f %f');
%        X(i-5) = inputtext{2};
%        Y(i-5) = inputtext{3};
%        u_z(i-5, j) = inputtext{7};
%     end
    
end
fclose(fileID);

end
save(save_filename, 'X', 'Y', 'u_z');
cd(pathname)