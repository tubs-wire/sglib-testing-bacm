clear
clc
grey = [0.8  0.8  0.8];
rand_seed(1306);

%% Options
%Choose update method( EnKF1 = with sampling / EnKF2 = sampling free )
update_methods = {'MMSE', 'MCMC','EnKF','gpcKF'};
update_method_flag = update_methods{4};
% scaling of the displacement field from meters (so that the max true displacement is 1)
scale_u= -1/0.002924353;
% order of the gPCE
gpc_order=5;
% Measurement std (3 mms)
sigma_max= 3*10^(-4);
% number of posterior samples
N=10000;
% Update for the log displacment?
update_log_disp_flag=false;
% Which plots are needed?
plot_response_surf_flag=false;
plot_qunatiles_and_meas_flag=false;
plot_histograms_flag=true;
%Do update with low rank representation of the measureable?
project_flag=false;

%%

% Pathname for loading solutions and predifined indices
if isunix
    data_pathname='/home/noefried/Documents/Gas_platform_Claudia/RV';
elseif ispc
    data_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
end

%% Maps of response and measurement error for better update
if update_log_disp_flag
    % apply a map on the displacement field for better update
    response_map=@(u)(real(log((u*scale_u))));
    % Define the inverse map
    inv_response_map=@(u)(exp(u)/scale_u);
    % Gradient of the map, so that the maped error can be calculated
    %     J_func=@(u)(abs(scale_u*1./u));
    %J_func=@(u)((scale_u*1./u));
    %     sigma_func=@(u,sigma)(abs(log(1 + sigma./u)));
    
    %log(y+e)=log(y(1+e/y))=log(y)+log(1+e/y)
    % the Taylor expansion of log(1+x): x-x^2/2+x^3/3+...=
    %=sum_n -(1)^(n+1)/n*x^n
    % with linear approximation
    % log(y+e)\approx log(y)+ e/y
    % if e~N(0,sigma) then e/y~N(0, sigma/y) (one could do a better
    % approximation in a gpc form)
    sigma_func=@(u,sigma)(abs(sigma./u));
else
    % apply a map on the displacement field for better update
    response_map=@(u)(u*scale_u); %for no map;
    % Define the inverse map
    inv_response_map=@(u)(u/scale_u); %for no map;
    % Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(abs(-scale_u*ones(length(u),1))); %for no map;
end
%%
T=load(fullfile(data_pathname, 'displacement_f_cm_scalar_1_89.mat'));
%%% ASK FROM Claudia:  This file doesn't have u_true, only true!
u_true=T.u_true(1:60,1);
u_true_maped=response_map(u_true);
if update_log_disp_flag
    ind_meas=exp(u_true_maped)>0.01;
else
    ind_meas=u_true_maped>0.01;
end
% Define the number of measurements
n_meas=sum(ind_meas);

%% Get proxi model and prior
[u_j_alpha, V_u, Q]=get_proxi_model(gpc_order,...
    'plot_flag',plot_response_surf_flag,...
    'sols_pathname', data_pathname, 'map_u', response_map);
% Statistics of the displacement field
[u_mean, u_var] = gpc_moments(u_j_alpha, V_u);
u_quant=(gpc_quantiles(V_u, u_j_alpha, 'quantiles', [0.023, 0.977]))';

%% Get the true reference displ (u)
% Specicify the fcm true
f_cm_true = 1.89;
xi_true = Q.params2germ(f_cm_true);

%% Create synthetic data from the reference
% Get the true reference at the assimilation points
%[y_true, ind_meas]=shuffle_select_sort(u_true, 1, n_meas, 'fixed_randseed', 644);
y_true=u_true(ind_meas);
y_true_maped=response_map(y_true);
y_j_alpha=u_j_alpha(ind_meas,:);
V_y=V_u;
% Measurement std
% the sigma value for the only scaled value
sigma_m=sigma_max; %*abs(u_true)/max(abs(u_true));
sigma_meas = sigma_max;%*scaled with the J_func
% make a separate simparamset for the measurement errors

E_meas=generate_stdrn_simparamset(sigma_meas*ones(1,n_meas));
y_m=response_map(y_true+E_meas.sample(1));
%y_m=response_map(y_m);

%% Error model
%sigma_e=J_func(u_true).*sigma_m;
%sigma_eps=J_func(y_true).*sigma_meas;
if exist('sigma_func', 'var')
    sigma_e  =sigma_func(u_true,sigma_m)*1.2;
    sigma_eps=sigma_func(y_true,sigma_meas)*1.2;
else
    sigma_e  =J_func(u_true).*sigma_m*1.2;
    sigma_eps=J_func(y_true).*sigma_meas*1.2;
end
%sigma_e=sigma_meas;
E=generate_stdrn_simparamset(sigma_eps);
%% If projection (low rank representation)
if project_flag
    % find eigenfunctions and eigenmodes and number of the modes
    %     y_j_alpha_fluct=y_j_alpha;
    %     y_j_alpha_fluct(1,:)=0;
    y_j_mean=gpc_moments(y_j_alpha, V_y);
    %     [r_i_k, sigma_k]=svd(y_j_alpha_fluct);
    %
    %     sigma_k=diag(sigma_k);
    %     plot(sigma_k(2:end));
    n_mode=3;
    [r_i_k, sigma_k, y_j_alpha_fluct]=...
        low_rank_from_gPCE(y_j_alpha, V_y, n_mode);
    
    plot(1:size(y_j_alpha,1), r_i_k(:,1:n_mode)', 'x-')
    semilogy(sigma_k, 'x')
    % project with the A matrix:
    A=r_i_k(:,1:n_mode)';
    %A=inv(diag(sigma_k(1:n_mode)))*r_i_k(:,1:n_mode)';
    % change Y
    y_j_alpha=A*y_j_alpha_fluct;
    % change measurement
    y_m=A*(y_m - y_j_mean);
    % change measurement model
    C_eps=A*diag(E.var)*A';
    L = chol(C_eps, 'lower');
    % Get transformation from standard normal distributed E to eps
    E=generate_stdrn_simparamset(diag(L));
    %E_1=generate_stdrn_simparamset(ones(1,n_node));
end
%% Plot quantiles, evaluate proxi model from true response
if plot_qunatiles_and_meas_flag
    % Plot Mean and Variance from the proxi model
    figure;
    plot_u_mean_quant(1:60,u_mean,u_quant(:,1), u_quant(:,2), ...
        'xlabel', 'i_x', 'ylabels', {'displacement'});
    hold on
    %plot_u_mean_var(1:60,response_map(u_true), sigma_e.^2,...
    %'times_sigma',2,'fill_color','c', 'transparency', 0.4)
    plot_u_mean_var(find(ind_meas),response_map(y_true), sigma_eps.^2,...
        'times_sigma',2,'fill_color','c', 'transparency', 0.4)
    plot(1:60, response_map(u_true), 'r', 'LineWidth', 2);
    plot(find(ind_meas), A\y_m+y_j_mean, 'kX', 'LineWidth', 2)
    legend('95% confidence region', 'Mean displacement',...
        'True displacement', 'Measurement', 'Measurement error')
    %%    Plot Realizations
    figure
    xi_i = Q.params2germ(Q.sample(100));
    u_i = inv_response_map(gpc_evaluate(u_j_alpha,V_u,xi_i));
    hold on
    h3=plot(1:60, u_i, 'color', grey);
    % Plot the true
    u_true_gpce = inv_response_map(gpc_evaluate(u_j_alpha, V_u, xi_true));
    hold on
    h1=plot(1:60,u_true_gpce,'c','LineWidth',2);
    h2=plot(1:60,u_true,'m:','LineWidth',2);
    title('Realizations of displacement')
    xlabel('i')
    ylabel('displacement')
    legend([h1,h2, h3(1)], 'gPCE at f_{cm true}', 'True displacement', 'Realizations')
    
end

%% Updating
% map from germ to measurable
Y_func = @(xi)gpc_evaluate(y_j_alpha, V_y, xi);
% Choose a method
switch(update_method_flag)
    %%
    case 'MMSE'
        % MMSE update (of Elmar)
        % map from error germ to error
        E_func = @(xi)(E.germ2params(xi));
        % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();
        % gPCE of Theta
        p_theta=1;
        V_theta = gpcbasis_create(V_y{1}, 'p', 1);
        theta_i_alpha=zeros(gpcbasis_size(V_theta))';
        theta_i_alpha(:,2:end)=identity(gpcbasis_size(V_theta,2));
        
        % MMSE order and num integration orders
        if update_log_disp_flag
            p_phi=1;
        else
            p_phi=1;
        end
        p_theta = gpcbasis_info(V_theta, 'total_degree');
        p_gpc= gpcbasis_info(V_y, 'total_degree');
        p_int_mmse=max(p_gpc*p_phi+1,ceil((p_theta+p_phi*p_gpc+1)/2));
        % The new gpc basis of theta will have this order (to which the
        % composition of Z_func = Y_func + E_func and the PHI_func will be projected)
        p_pn=p_gpc*p_phi;
        p_int_proj=p_pn+1;
        % Update
        [thetam_func, V_thetan, theta_m, Y_m_func] = my_mmse_gpc(theta_i_alpha, ...
            Y_func, V_theta, y_m, E_func, V_e, p_phi, p_int_mmse,...
            'int_grid', 'full_tensor');
        % posterior mean
        f_cm_post_mean = Q.germ2params(theta_m);
        % samples of the posterior
        e_m_i=E.sample(N);
        theta_prior_i =gpc_sample(theta_i_alpha, V_theta, N);
        
        
        theta_post_samples=theta_prior_i+...
            binfun(@plus,-funcall(thetam_func, [theta_prior_i; e_m_i]), theta_m);
        figure
        f_cm_post_samples=Q.germ2params(theta_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        
        %% MCMC from proxi model
    case 'MCMC'
        % get distribution of the germ
        [~, dist]=gpc_registry('get', V_y{1});
        prior_dist = {dist};
        prop_dist={NormalDistribution(0,0.5)};
        % MCMC update
        [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
            E, 'prop_dist', prop_dist);
        f_cm_post_samples = Q.germ2params(theta_samples_post);
        f_cm_post_mean = mean(f_cm_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        
        %% Updating with the Ensemble Kalman filter
        %% Elmar's cool translation of ENKF
    case 'EnKF'
        xi_samples=gpcgerm_sample(V_y, N);
        e_samples=E.sample(N);
        theta_samples_post=enKF(xi_samples, Y_func, e_samples, y_m);
        
        % map to f_cm
        f_cm_post_samples=Q.germ2params(theta_samples_post);
        %Posterior mean
        f_cm_post_mean=mean(f_cm_post_samples,2);
        %Posterior variance
        f_cm_post_var=var(f_cm_post_samples, [],2);
        
        %     case 'EnKF1'
        %         f_cm_post_samples=update_q_enKF(Q, E, y_i_alpha, V_y, y_m, N);
        %         f_cm_post_mean = mean(f_cm_post_samples);
        %         f_cm_post_var  = var(f_cm_post_samples);
        
        %% updating the coefficients (without samples) - Bojana's update
    case 'gpcKF'
        % gPCE of the meas error model
        [E_j_beta, V_E]=E.gpc_expand();
        % Define identity map: germ to Theta
        Theta_i_alpha=zeros(gpcbasis_size(V_y))';
        Theta_i_alpha(2)=1;
        %gPCE of the updated Theta
        [Thetap_i_gamma, V_Thetap] = gpc_KF(Theta_i_alpha, y_j_alpha, V_y, E_j_beta, V_E, y_m);
        %sample from the updated THETA
        theta_samples_post=gpc_sample(Thetap_i_gamma, V_Thetap, N);
        % map to f_cm
        f_cm_post_samples=Q.germ2params(theta_samples_post);
        %Posterior mean
        f_cm_post_mean=mean(f_cm_post_samples,2);
        %Posterior variance
        f_cm_post_var=var(f_cm_post_samples, [],2);
end
%% Write and plot update results
f_cm_prior_samples=Q.sample(N);
switch update_method_flag
    case 'MMSE'
        display(strvarexpand('UPDATE RESULTS WITH $update_method_flag$, p_\phi = $p_phi$'));
    otherwise
        display(strvarexpand('UPDATE RESULTS WITH $update_method_flag$'));
end
display(strvarexpand('f_cm_true=$f_cm_true$'));
display(strvarexpand('f_cm_post_mean=$f_cm_post_mean$'));
display(strvarexpand('f_cm_prior_var=$Q.var$'));
display(strvarexpand('f_cm_post_var=$f_cm_post_var$'));
if plot_histograms_flag
    % plot prior and posterior histograms
    figure
    subplot(2,1,1)
    h1=histogram(f_cm_prior_samples);
    hold on
    %[f1,p1]=plot_density(f_cm_prior_samples);
    %hp1=plot(f1, p1, 'b' ,'LineWidth',2);
    h2=histogram(f_cm_post_samples);
    %hp2=plot_density(f_cm_post_samples,'r' ,'LineWidth',2);
    h1.Normalization = 'pdf';
    h1.BinWidth = 0.05;
    h1.EdgeColor =h1.FaceColor;
    h2.Normalization = 'pdf';
    h2.BinWidth = 0.05;
    h2.EdgeColor =h2.FaceColor;
    h2.FaceAlpha =0.4;
    
    y_limits=ylim;
    plot([f_cm_post_mean, f_cm_post_mean], y_limits, 'k', 'LineWidth', 2)
    plot([f_cm_true, f_cm_true], y_limits, 'k--', 'LineWidth', 3)
    ylim(y_limits);
    xlim([0,10]);
    legend('prior', 'posterior', 'posterior mean', 'true value');
    xlabel('f_{cm}')
    ylabel('Prior and posterior probabilities');
    switch update_method_flag
        case 'MMSE'
            title(strvarexpand('UPDATE RESULTS WITH $update_method_flag$, esimator polynomial order: $p_phi$'));
        otherwise
            title(strvarexpand('UPDATE RESULTS WITH $update_method_flag$'));
    end
 
    % calculate pics
    [val, post_pick_ind]=max(h2.Values);
    pick_bounds=h2.BinEdges([post_pick_ind, post_pick_ind+1]);
    display(strvarexpand('Pick of posterior is between $pick_bounds(1)$ and $pick_bounds(2)$'));
    
    subplot(2,1,2)
    hpost=histogram(f_cm_post_samples);
    hpost.FaceColor=[1 .5 0];
    hpost.EdgeColor=hpost.FaceColor;
    y_limits=ylim;
    hold on
    plot([f_cm_post_mean, f_cm_post_mean], y_limits, 'k', 'LineWidth', 2)
    plot([f_cm_true, f_cm_true], y_limits, 'k--', 'LineWidth', 3)
    %title('Posterior distribution')
    ylim(y_limits);
    xlim([0.5, 3.5]);
     xlabel('f_{cm}')
    ylabel('Posterior probability');
    
end




