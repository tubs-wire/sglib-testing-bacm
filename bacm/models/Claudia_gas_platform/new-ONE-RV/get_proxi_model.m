function [u_i_alpha, V_u, Q, X, Y]=get_proxi_model(gpc_order, varargin)

options = varargin2options(varargin);
[plot_flag, options] = get_option(options, 'plot_flag', false);
[sols_pathname, options] = get_option(options, 'sols_pathname', 'F:\noemi\Documents\Claudia_gas_platform\RV');
[map_u, options] = get_option(options, 'map_u', @(u)(u));
check_unsupported_options(options, mfilename);

[Q, xi_i, f_cm_i, w_i, V_u] = pre_get_integration_points_and_paramset(gpc_order);
% load solution from the simulation
n =gpc_order+1;

file_name=fullfile(sols_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
[X,  Y,  u_z]= load_results_to_matlab(file_name, n);
u_i=funcall(map_u, u_z);
u_i_alpha = calculate_gpc_coeffs(V_u, xi_i, u_i, w_i, 'method', 'project');

if plot_flag
    % Plot the response fcm-u
    subplot(2,1,1)
    % Linspace from the germ
    N_x=100;
    x_i = gpcgerm_linspace(V_u,N_x);
    % Evaluate solution at different germs
    u = gpc_evaluate(u_i_alpha, V_u, x_i);
    f_cm_i=Q.germ2params(x_i);
    plot(f_cm_i, u)
    xlabel('f_{cm}')
    ylabel('u_z')
    title('Displacement dependence on the parameter and germ at different coordinates')
    
    subplot(2,1,2)
    % Plot the response xi(germ)-u
    plot(x_i, u)
    xlabel('theta')
    ylabel('u_z')
end