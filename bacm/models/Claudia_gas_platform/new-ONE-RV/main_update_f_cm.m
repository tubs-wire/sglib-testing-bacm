clear
clc
grey = [0.8  0.8  0.8];
rand_seed(8562);
Colors=set_my_favorite_colors();
%% Options

%Choose update method( EnKF1 = with sampling / EnKF2 = sam pling free )
update_methods = {'MMSE', 'MCMC','EnKF','gpcKF'};
update_method_flag = update_methods{3};
% MMSE esimator order if used
p_phi=3;
% scaling of the displacement field from meters (so that the max true displacement is 1)
scale_u= -1/0.002924353;
% order of the gPCE
gpc_order=5;

% Measurement std (2.0 mms)
sigma_m= 2.0*10^(-4);
% number of posterior samples
N=10000;
% Which plots are needed?
plot_response_surf_flag=false;
plot_qunatiles_and_meas_flag=false;
plot_histograms_flag=true;
save_histograms_flag=true;
%Do update with low rank representation of the measureable?
project_flag=false;

%% Set path where the blackbox results are saved

% Pathname for loading solutions and predifined indices
if isunix
    data_pathname=[char(java.lang.System.getProperty('user.home')) '/Documents/Gas_platform_Claudia/RV'];
elseif ispc
    data_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
end

%% Maps of response and measurement error for better update

% apply a map on the displacement field (for better update, or other
% reason)
response_map=@(u)(u*scale_u); %for no map;
% Define the inverse map
inv_response_map=@(u)(u/scale_u);

%% Get proxi model and prior

% Generate GPC surrogate model
[u_j_alpha, V_u, Q]=get_proxi_model(gpc_order,...
    'plot_flag',plot_response_surf_flag,...
    'sols_pathname', data_pathname, 'map_u', response_map);
% Statistics of the displacement field
[u_mean, u_var] = gpc_moments(u_j_alpha, V_u);
%u_quant=(gpc_quantiles(V_u, u_j_alpha, 'quantiles', [0.023, 0.977]))';

%% Define 'true displacement'

% Specicify the 'true' value of the parameter fcm
f_cm_true = 1.89;
theta_true = Q.params2germ(f_cm_true);

T=load(fullfile(data_pathname, 'displacement_f_cm_scalar_1_89.mat'));
u_true=response_map(T.u_true(1:60,1));

%% Create synthetic data

% Use only points where displacement is not too close to 0
ind_meas=u_true>0.01;
% Number of measurements
n_meas=sum(ind_meas);
% Get the true disp at the assimilation points
y_true=u_true(ind_meas);

% gPCE of the measurable
y_j_alpha=u_j_alpha(ind_meas,:);
V_y=V_u;

% Error model
% make a separate simparamset for the measurement errors
sigma_E=ones(n_meas,1)*sigma_m*abs(scale_u);
E_m=generate_stdrn_simparamset(sigma_E);
E=generate_stdrn_simparamset(sigma_E*1.2);
E_func = @(xi)(E.germ2params(xi));
y_m=y_true+E_m.sample(1);

%% If projection (low rank representation)
if project_flag
    % find eigenfunctions and eigenmodes and number of the modes
    y_j_mean=gpc_moments(y_j_alpha, V_y);
    %     [r_i_k, sigma_k]=svd(y_j_alpha_fluct);
    %
    %     sigma_k=diag(sigma_k);
    %     plot(sigma_k(2:end));
    n_mode=3;
    [r_i_k, sigma_k, y_j_alpha_fluct]=...
        low_rank_from_gPCE(y_j_alpha, V_y, n_mode);
    
    %plot(1:size(y_j_alpha,1), r_i_k(:,1:n_mode)', 'x-')
    %semilogy(sigma_k, 'x')
    
    % project with the A matrix:
    A=r_i_k(:,1:n_mode)';
    % change Y
    y_j_alpha=A*y_j_alpha_fluct;
    % change measurement
    y_mp=A*(y_m - y_j_mean);
    % change measurement model
    C_eps=A*diag(E.var)*A';
    L = chol(C_eps, 'lower');
    % Get transformation from standard normal distributed E to eps
    E=generate_stdrn_simparamset(ones(n_mode,1));
    %E_1=generate_stdrn_simparamset(ones(1,n_node));
    E_func = @(xi)(L*E.germ2params(xi));
end

%% Plot quantiles, evaluate proxi model from true response

if plot_qunatiles_and_meas_flag
    %% Plot Mean and Variance from the proxi model
    figure;
    plot_u_mean_quant(1:60, u_mean, u_quant(:,1), u_quant(:,2), ...
        'xlabel', 'i_x', 'ylabels', {'displacement'}, 'fill_color', 'c');
    hold on
    %plot_u_mean_var(1:60,response_map(u_true), sigma_e.^2,...
    %'times_sigma',2,'fill_color','c', 'transparency', 0.4)
    plot_u_mean_var(find(ind_meas),y_true, sigma_E.^2,...
        'times_sigma',2,'fill_color','b', 'transparency', 0.4)
    plot(1:60, u_true, 'r', 'LineWidth', 2);
    plot(find(ind_meas), y_m, 'kX', 'LineWidth', 2)
    
    legend('95% confidence region', 'Mean displacement',...
        '95% confidence region of measurement', 'Measurement mean', 'True displacement', 'Measurements')
    
    %%    Plot Realizations
    figure
    xi_i = Q.params2germ(Q.sample(100));
    u_i = inv_response_map(gpc_evaluate(u_j_alpha,V_u,xi_i));
    hold on
    h3=plot(1:60, u_i, 'color', grey);
    % Plot the true
    u_true_gpce = inv_response_map(gpc_evaluate(u_j_alpha, V_u, theta_true));
    hold on
    h1=plot(1:60,u_true_gpce,'c','LineWidth',2);
    h2=plot(1:60,inv_response_map(u_true),'m:','LineWidth',2);
    title('Realizations of displacement')
    xlabel('i')
    ylabel('displacement')
    legend([h1,h2, h3(1)], 'gPCE at f_{cm true}', 'True displacement', 'Realizations')
    
end

if project_flag
    y_m=y_mp;
end


%% Updating
% map from germ to measurable
%Y_func = @(xi)gpc_evaluate(y_j_alpha, V_y, xi);
Y_func = funcreate(@gpc_evaluate, y_j_alpha, V_y, @funarg);

% Choose a method
switch(update_method_flag)
    %%
    case 'MMSE'
        % MMSE update (of Elmar)
        
        % E_func = @(xi)(E.germ2params(xi));
        E_func = funcreate(@germ2params, E, @funarg);
        
        
        % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();
        
        % gPCE of Theta
        V_theta = gpcbasis_create(V_y{1}, 'p', 1);
        theta_i_alpha=zeros(gpcbasis_size(V_theta))';
        theta_i_alpha(:,2:end)=identity(gpcbasis_size(V_theta,2));
        
        % set MMSE integration orders
        p_theta = gpcbasis_info(V_theta, 'total_degree');
        p_gpc= gpcbasis_info(V_y, 'total_degree');
        p_int_mmse=max(p_gpc*p_phi+1,ceil((p_theta+p_phi*p_gpc+1)/2));
        
        % Update
        [thetam_func, ~, theta_m, Y_m_func] = my_mmse_gpc(theta_i_alpha, ...
            Y_func, V_theta, y_m, E_func, V_e, p_phi, p_int_mmse,...
            'int_grid', 'full_tensor');
        
        % posterior mean
        f_cm_post_mean = Q.germ2params(theta_m);
        
        % samples of the posterior
        e_m_i=E.sample(N);
        theta_prior_i =gpc_sample(theta_i_alpha, V_theta, N);
        theta_post_samples=theta_prior_i+...
            binfun(@plus,-funcall(thetam_func, [theta_prior_i; e_m_i]), theta_m);
        f_cm_post_samples=Q.germ2params(theta_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        
        %% MCMC from proxi model
    case 'MCMC'
        % get distribution of the germ
        [~, dist]=gpc_registry('get', V_y{1});
        prior_dist = {dist};
        prop_dist={NormalDistribution(0,0.2)};
        % MCMC update
        if project_flag
            pdf_error_func=@(u,z)(multi_normal_pdf( u-z, zeros(n_mode, 1), C_eps));
            [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
                pdf_error_func, 'prop_dist', prop_dist);
        else
            [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
                E, 'prop_dist', prop_dist, 'N_burn', 0);
        end
        f_cm_post_samples = Q.germ2params(theta_samples_post);
        f_cm_post_mean = mean(f_cm_post_samples);
        f_cm_post_var  = var(f_cm_post_samples);
        
        % plot results
        plot(theta_samples_post(1:10000), 'LineWidth', 2)
        hold on
        y_lim=ylim()
        plot([100,100], y_lim)
        legend('MCMC random walk chain', 'end of burn-in period')
        xlabel('i')
        ylabel('\theta_i')
        
        %% Updating with the Ensemble Kalman filter
        %% Elmar's cool translation of ENKF
    case 'EnKF'
        xi_samples=gpcgerm_sample(V_y, N);
        if project_flag
            e_samples=L*E.sample(N);
        else
            e_samples=E.sample(N);
        end
        theta_samples_post=enKF(xi_samples, Y_func, e_samples, y_m);
        
        % map to f_cm
        f_cm_post_samples=Q.germ2params(theta_samples_post);
        %Posterior mean
        f_cm_post_mean=mean(f_cm_post_samples,2);
        %Posterior variance
        f_cm_post_var=var(f_cm_post_samples, [],2);
        
        %     case 'EnKF1'
        %         f_cm_post_samples=update_q_enKF(Q, E, y_i_alpha, V_y, y_m, N);
        %         f_cm_post_mean = mean(f_cm_post_samples);
        %         f_cm_post_var  = var(f_cm_post_samples);
        
        %% updating the coefficients (without samples) - Bojana's update
    case 'gpcKF'
        % gPCE of the meas error model
        [E_j_beta, V_E]=E.gpc_expand();
        if project_flag
            E_j_beta=L*E_j_beta;
        end
        % Define identity map: germ to Theta
        Theta_i_alpha=zeros(gpcbasis_size(V_y))';
        Theta_i_alpha(2)=1;
        %gPCE of the updated Theta
        [Thetap_i_gamma, V_Thetap] = gpc_KF(Theta_i_alpha, y_j_alpha, V_y, E_j_beta, V_E, y_m);
        %sample from the updated THETA
        theta_samples_post=gpc_sample(Thetap_i_gamma, V_Thetap, N);
        % map to f_cm
        f_cm_post_samples=Q.germ2params(theta_samples_post);
        %Posterior mean
        f_cm_post_mean=mean(f_cm_post_samples,2);
        %Posterior variance
        f_cm_post_var=var(f_cm_post_samples, [],2);
end

%% Write and plot update results

f_cm_prior_samples=Q.sample(N);
switch update_method_flag
    case 'MMSE'
        display(strvarexpand('UPDATE RESULTS WITH $update_method_flag$, p_\phi = $p_phi$'));
    otherwise
        display(strvarexpand('UPDATE RESULTS WITH $update_method_flag$'));
end
display(strvarexpand('f_cm_true=$f_cm_true$'));
display(strvarexpand('f_cm_post_mean=$f_cm_post_mean$'));
display(strvarexpand('f_cm_prior_var=$Q.var$'));
display(strvarexpand('f_cm_post_var=$f_cm_post_var$'));
display(strvarexpand('error_mean=$f_cm_post_mean-f_cm_true$'));
if plot_histograms_flag
    %% plot prior
    
    hprior=figure;
    
    h1=histogram(f_cm_prior_samples);
    hold on
    h1.Normalization = 'pdf';
    h1.EdgeColor =h1.FaceColor;
    h1.BinWidth = 0.6;
    y_limits=ylim;
    hmean=plot([Q.mean, Q.mean], y_limits, ':','color', Colors.navy_blue, 'LineWidth', 2);
    xlabel('f_{cm}')
    ylabel('probability');
    legend('prior', 'prior mean')
    xlim([0,35])
    if save_histograms_flag
        pos=hprior.Position;
        pos(3)=pos(3)*3.1;
        hprior.Position=pos;
        savefig(hprior, 'prior_probability.fig')
        print(hprior, 'prior_probability', '-depsc')
        print(hprior, 'prior_probability', '-dpdf')
    end
    %% plot prior and posterior histograms
    hpripost=figure;
    %subplot(2,1,1)
    h1=histogram(f_cm_prior_samples);
    hold on
    %[f1,p1]=plot_density(f_cm_prior_samples);
    %hp1=plot(f1, p1, 'b' ,'LineWidth',2);
    h2=histogram(f_cm_post_samples);
    %hp2=plot_density(f_cm_post_samples,'r' ,'LineWidth',2);
    h1.Normalization = 'pdf';
    h1.BinWidth = 0.1;
    h1.EdgeColor =h1.FaceColor;
    h2.Normalization = 'pdf';
    h2.BinWidth = 0.05;
    h2.EdgeColor =h2.FaceColor;
    h2.FaceAlpha =0.4;
    
    y_limits=ylim;
    plot([Q.mean, Q.mean], y_limits,  ':','color', Colors.navy_blue, 'LineWidth', 2)
    plot([f_cm_post_mean, f_cm_post_mean], y_limits, 'k', 'LineWidth', 2)
    plot([f_cm_true, f_cm_true], y_limits, 'k--', 'LineWidth', 3)
    ylim(y_limits);
    xlim([0,10]);
    legend('prior', 'posterior','prior mean', 'posterior mean', 'true value');
    xlabel('f_{cm}')
    ylabel('probability');
    switch update_method_flag
        case 'MMSE'
            title(strvarexpand('$update_method_flag$, esimator polynomial order: $p_phi$'));
        otherwise
            title(strvarexpand('$update_method_flag$'));
    end
    if save_histograms_flag
        switch update_method_flag
            case 'MMSE'
                fname=strvarexpand('$update_method_flag$p$p_phi$');
            otherwise
                if project_flag
                    fname=strvarexpand('$update_method_flag$_low_rank');
                else
                    fname=strvarexpand('$update_method_flag$');
                end
        end
        savefig(hpripost, strvarexpand('$fname$.fig'))
        print(hpripost, fname, '-depsc')
        print(hpripost, fname, '-dpdf')
    end
    
    %% calculate pics and display
    [val, post_pick_ind]=max(h2.Values);
    pick_bounds=h2.BinEdges([post_pick_ind, post_pick_ind+1]);
    display(strvarexpand('Pick of posterior is between $pick_bounds(1)$ and $pick_bounds(2)$'));
    %% plot posterior
    hfpost=figure;
    hpost=histogram(f_cm_post_samples);
    hpost.FaceColor=[1 .5 0];
    hpost.EdgeColor=hpost.FaceColor;
    y_limits=ylim;
    hold on
    plot([f_cm_post_mean, f_cm_post_mean], y_limits, 'k', 'LineWidth', 2)
    plot([f_cm_true, f_cm_true], y_limits, 'k--', 'LineWidth', 3)
    %title('Posterior distribution')
    ylim(y_limits);
    xlim([1, 3.0]);
    xlabel('f_{cm}')
    ylabel('probability');
    legend('prior', 'posterior mean', 'true value')
    switch update_method_flag
        case 'MMSE'
            title(strvarexpand('$update_method_flag$, esimator polynomial order: $p_phi$'));
        otherwise
            title(strvarexpand('$update_method_flag$'));
    end
    
    if save_histograms_flag
        fname=[fname, '_posterior'];
        savefig(hfpost, strvarexpand('$fname$.fig'))
        print(hfpost, fname, '-depsc')
        print(hfpost, fname, '-dpdf')
    end
end





