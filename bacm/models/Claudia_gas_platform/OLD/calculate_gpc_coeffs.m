function u_i_alpha = calculate_gpc_coeffs(V_u,  x, u, w, varargin)
%METHOD: optional input 'PROJECT'(default) or 'COLLOC'

options=varargin2options(varargin);
[method,options]=get_option(options, 'method', 'project');
check_unsupported_options(options, mfilename);

num_vars = size(u,1); % nnode

M = gpcbasis_size(V_u, 1);
Q = length(w);


%% Projection with full tensor grid
switch method
    case 'project'
        u_i_alpha = zeros(num_vars, M);
        for j = 1:Q
            x_j = x(:, j);
            u_i_j=u(:,j);
            psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
            u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
        end
    case 'colloc'
        % compute the (generalised) Vandermonde matrix
        A=gpcbasis_evaluate(V_u, x);
        u_i_alpha = u/A;
end
