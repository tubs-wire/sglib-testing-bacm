function test_update_methods()
% notation
% Q: object collecting the parameters that are random in the problem
% y: predicted observable
% y_m: sysnthetic observation
% n_meas: number of synthetic observations along the bar
% x_meas: coordinates of the places where observations obtained (it is generated randomly) 
clear

%% Generate the parameter set for the prior model
Q=MySimParamSet();
mean_f_cm = 5.5;
var_f_cm = 10.0;

mu = log((mean_f_cm^2)/sqrt(var_f_cm+mean_f_cm^2));
sig = sqrt(log(var_f_cm/(mean_f_cm^2)+1));

Q.add('f_cm', LogNormalDistribution(mu, sig));


%% Generate the model;


%% Generate some artificial truth we want to identify which is (for the
% moment) close to the mean of the prior model
n_meas=20; 
x_meas=(x(randi([1,length(x)-1], n_meas,1)))'+max(diff(x))*rand(n_meas,1); %randomly generated coordinates of observations
x_meas=sort(x_meas);
model=model.set_x_meas(x_meas);
% generate randomly the artificial truth
q_true = mean(Q.sample(6),2);
% Compute artificial measurements by adding random error to the true values
y_true=model.compute_measurements(model.compute_response(q_true));
sigma_true_err=0.01/50;
y_m = y_true + normal_sample(n_meas, 0, sigma_true_err);

% Plot 'true response' and synthetic measurements
figure
plot(x,model.compute_response(q_true),'g', 'LineWidth', 2 )
hold on
plot(x_meas,y_m, 'x')

%% Generate the error model
% Let's assume a bit biger error variance then the one used for the synthetic measurement 
sigma_eps = 1.1*sigma_true_err;
% plot error variance
plot_u_mean_var(x', model.compute_response(q_true), sigma_eps^2*ones(size(x')), 'transparency', 0.4, 'fill_color', 'c')
% make a serperate simparamset for the measurement errors
E=generate_stdrn_simparamset(ones(n_meas,1)*sigma_eps);

%% Forward method
% Define approximating basis with gaussian germs
p_gpc=4;
syschars=repmat('h',1,Q.num_params);
V_ug=gpcbasis_create(syschars, 'p', p_gpc);

% map from germ to params
xi2params_func=@(xi)(Q.stdnor2params(xi));

% generate surrogate model
p_int=5;
[surr_model, surr_model_infosg] = mygenerate_surrogate_model...
    (model, Q,  xi2params_func, V_ug,'myprojection',...
    {p_int, 'grid', 'full_tensor'});

[u_mean, u_var]=get_moments(surr_model);
[partial_var, I_s, ratio_by_index, ratio_by_order]=get_sobol_partial_vars(surr_model, 'max_index', 1);

plot_u_mean_var(x, u_mean, u_var, 'transparency', 0.4, 'fill_color', 'r')

%figure
%plot_multi_response_surface(surr_model, length(x), 'germ_index', [2,3],'germ2param', xi2params_func, 'name_of_RVs', Q.param_plot_names, 'N', 40)
%plot_multi_response_surface(surr_model, length(x), 'germ_index', [2,3],'name_of_RVs', Q.param_plot_names)

%% Ensemble Kalman Filter with XIU's notation, only deflection update
% ensemble sample number
N=100;
ua_i=update_response_enKF(model, Q, E, surr_model, y_m, N);
plot(x',ua_i(:,1:100), 'b')

%% The same, but with parameter estimation
qa_i=update_q_enKF(model, Q, E, surr_model, y_m, N);

% Check forward method
[ua_mean, ua_var, calc_model] = caculate_MCmoments_with_model(model, qa_i);
plot_u_mean_var(x,ua_mean, ua_var, 'fill_color', 'y', 'transparency', 0.4)

%% By updating the coefficients (without samples) - Bojana's update
[xi_a_i_alpha, V_xi_a] = gpc_update_enKF(model, Q, E, surr_model, y_m);
xi2params_func=@(xi)Q.stdnor2params(gpc_evaluate(xi_a_i_alpha,V_xi_a, xi));
[ua_surr_model, ua_surr_model_infos] = mygenerate_surrogate_model...
    (model, Q,  xi2params_func, V_xi_a,'myprojection',...
    {p_int, 'grid', 'full_tensor'});

[ua_mean, ua_var]=ua_surr_model.get_moments;
plot_u_mean_var(x, ua_mean, ua_var, 'fill_color', 'm', 'transparency', 0.4)

%% with Elmar's cool MMSE
% Here the germs of the gPCE used are the 'natural' germs belonging to the 
% parameter distributions and not necessarily Gaussians 
g_func = @(q)(model.compute_measurements(model.compute_response(q)));
Y_func = @(xi)(compute_measurement(xi, g_func, Q));
E_func = @(xi)(E.germ2params(xi));
[e_i_alpha, V_e] = E.gpc_expand();

[Q_i_alpha, V_q] = Q.gpc_expand();
% now we don't need Gaussian germs, so we calculate a new model with the
% 'natural germs' and polynomials
p_gpc=2;
V_u=gpcbasis_modify(V_q, 'p', p_gpc);

% map from germ to params
xi2params_func=@(xi)(Q.germ2params(xi));

% generate surrogate model
p_int=3;
[u_surr_model, u_surr_model_infos] = mygenerate_surrogate_model...
    (model, Q,  xi2params_func, V_u,'myprojection',...
    {p_int, 'grid', 'full_tensor'});

%plot_multi_response_surface(u_i_alpha(50,:), V_u);
p_phi=1;
p_pn=2;
p_int_mmse=3;
p_int_proj=4;
[Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q, y_m, E_func, V_e, p_phi, p_int_mmse, p_pn, p_int_proj, 'int_grid', 'smolyak');

xi2params_func=@(xi)gpc_evaluate(Qn_i_beta, V_qn, xi);
[ua_surr_model, ua_surr_model_infos] = mygenerate_surrogate_model...
    (model, Q,  xi2params_func, V_qn,'myprojection',...
    {p_int, 'grid', 'smolyak'});
[ua_mean, ua_var]=ua_surr_model.get_moments;
plot_u_mean_var(x, ua_mean, ua_var, 'fill_color', 'g', 'transparency', 0.4)
%% Compute some stuff with updated parameters
[q_mean, q_var] = gpc_moments(Q_i_alpha, V_q);
[qn_mean, qn_var] = gpc_moments(Qn_i_beta, V_qn);
gpc_covariance(Q_i_alpha, V_q)
chopabs(gpc_covariance(Qn_i_beta, V_qn, [], 'corrcoeffs', true))
chopabs([q_mean-q_true, sqrt(q_var), qn_mean-q_true, sqrt(qn_var)])
chopabs([g_func(q_mean)-y_m, g_func(qn_mean)-y_m, y_true-y_m ])
end

function ua_i=update_response_enKF(model, Q, E, surr_model, y_m, N)
% generate ensemble of solution realizations
q_i=Q.sample(N);
u_i=surr_model.compute_response(q_i, 'params2germ_func', @(q)Q.params2stdnor(q));
% The Kalman gain
K=get_Kalman_gain(model, Q, E, surr_model);
% Update the ensemble;
ua_i=u_i+K*(binfun(@minus,y_m,model.compute_measurements(u_i)));
end

function qa_i=update_q_enKF(model, Q, E, surr_model, y_m, N)

%The Kalman gain
[Ke, xi_i_alpha,V_xi]=get_Kalman_gain(model, Q, E, surr_model, 'flag_extended', true);
%sample from the germ and calculate the response at the samples points
xi_i=gpc_sample(xi_i_alpha,V_xi, N);
q_i=Q.stdnor2params(xi_i);
u_i=surr_model.compute_response(q_i, 'params2germ_func', @(q)Q.params2stdnor(q));
%update of the xi samples
xia_i=xi_i+Ke*(binfun(@minus,y_m,model.compute_measurements(u_i)));
%map xi samples to q samples
qa_i=Q.stdnor2params(xia_i);
end

function [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(model, Q, E, surr_model, y_m)
% gpc of the synthetic measurement
y_i_alpha=model.compute_measurements(surr_model.coeffs);
ym_i_alpha=zeros(size(y_i_alpha));
ym_i_alpha(:,1)=y_m;
%The Kalman gain
[Ke, xi_i_alpha,V_xi, ind_xi]=get_Kalman_gain(model, Q, E, surr_model, 'flag_extended', true);
U=Ke*(ym_i_alpha-y_i_alpha);
%expand gpc basis of xi
xi_a_i_alpha=zeros(size(U));
xi_a_i_alpha(:,ind_xi)=xi_i_alpha;
%update gPCE of xi
xi_a_i_alpha=xi_a_i_alpha+U;
V_xi_a=surr_model.basis;
end
function [K, xi_i_alpha,V_xi, ind_xi, ind_y]=get_Kalman_gain(model, Q, E, surr_model, varargin)
options=varargin2options(varargin, mfilename);
[flag_extended,options]=get_option(options, 'flag_extended', false);
check_unsupported_options(options);

% The measurement operator
H=model.compute_measurement_operator;
% gPCE of the measurement
[y_i_alpha, V_y]=get_gpce_coeffs_of_meas(surr_model);
C_y=gpc_covariance(y_i_alpha, V_y);
P=get_response_covariance(surr_model);
% Get gpc of the error
[e_i_alpha, V_e] = E.gpc_expand();
% Error covariance (C_epsilon)
R = gpc_covariance(e_i_alpha, V_e);
if flag_extended
    XI=generate_stdrn_simparamset(ones(Q.num_params));
    [xi_i_alpha,V_xi]=XI.gpc_expand();
    % Calculation of C_xi_yf with combining the gpces
    [C_xi_y, ind_xi, ind_y]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_y, V_xi);
    % the extended Kalman gain
    K=C_xi_y/(C_y+R);
else
    %C_y=H*P*H';
    % The Kalman gain
    K=P*H'/(C_y+R);
end
end
function [C_xi_y, ind_xi, ind_y]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_u, V_xi)
n_meas=size(y_i_alpha, 1);
n_params=size(xi_i_alpha,1);
% extend the basis to a common basis
[V_xiyf, ind_xi, ind_y]=gpcbasis_combine(V_xi,V_u, 'inner_sum');
% xi coefficients in the new basis
xi_ext_i_alpha=zeros(n_params,gpcbasis_size(V_xiyf,1));
xi_ext_i_alpha(:, ind_xi)=xi_i_alpha;
y_ext_i_alpha=zeros(n_meas, gpcbasis_size(V_xiyf,1));
y_ext_i_alpha(:, ind_y)=y_i_alpha;

% The covariance matrix C_xiy
C_xi_y=gpc_covariance(xi_ext_i_alpha, V_xiyf, y_ext_i_alpha);
end

function [y_i_alpha, V_y]=get_gpce_coeffs_of_meas(surr_model)
y_i_alpha=surr_model.compute_measurements(surr_model.coeffs);
V_y=surr_model.basis;
end