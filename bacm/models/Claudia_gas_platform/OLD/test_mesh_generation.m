

% x = linspace(0, 14000, 15);
% y = linspace(0, 10000, 11);

x = linspace(-500, 14500, 16);
y = linspace(-500, 10500, 12);


xm = 0.5*(x(1:end-1)+x(2:end));
ym = 0.5*(y(1:end-1)+y(2:end));

[pos, els] = create_rect_mesh(x, y);

% z = sin(3*pi*pos(1,:)/14000) .* cos(2*pi*pos(2,:)/10000);
% z = pos(1,:) .* cos(2*pi*pos(2,:)/10000);
% z = sin(3*pi*pos(1,:)/14000) .* pos(2,:);
% z = z';
% plot_field(pos, els, z)

% Choose nu between 1 and 2 (realisations with nu=1 would be continuous
% functions and realisations with nu=2 would be one times continously
% differentiable).
nu = 2; 
l_c = 4000;
sigma = 1;

cov_func = funcreate(@matern_covariance, nu, funarg, funarg, l_c, sigma);
%cov_func = funcreate(@gaussian_covariance, funarg, funarg, l_c, sigma);
C = covariance_matrix(pos, cov_func);

%%
% for i=1:size(pos,2)
%     plot_field(pos, els, C(:,i));
%     drawnow;
%     pause(0.05)
% end

%%
G_N = mass_matrix(pos, els);
L = 11;
[r_i_k,sigma_k,L]=kl_solve_evp(C, G_N, L);
multiplot_init(4,3);
multiplot
plot(sigma_k, '.')
for i = 1:min(L,11)
    multiplot
    plot_field(pos, els, r_i_k(:,i));
    axis equal
    %view(3)
    drawnow;
    %pause(0.2);
    %userwait;
end

D = (max(x)-min(x))*(max(y)-min(y));
var_ex = D * sigma^2; 
var_act = sum(sigma_k.^2);
err_var = (var_ex - var_act) / var_ex;
strvarexpand('Error in variance: $err_var*100$%');

% keep the variance high (kind of cheating)
sigma_k = sigma_k * sqrt(var_ex / var_act);

%%
% Make a PCE from the KLE 

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);

% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

u_i_k = binfun(@times, r_i_k, sigma_k);

%%
mean_u = 5.5;
var_u = 10.0;

mu = log((mean_u^2)/sqrt(var_u+mean_u^2));
sig = sqrt(log(var_u/(mean_u^2)+1));

% x = linspace(0, mean_u+2*var_u);
% clf
% plot(x, lognormal_pdf(x, mu, sig));

[mean_u_, var_u_] = lognormal_moments(mu, sig)

%%
n_samples = 16;
multiplot_init(n_samples)
for i=1:n_samples
    multiplot
    g = kl_pce_field_realization(u_i_k, theta_k_alpha, I);
    u = lognormal_stdnor(g, mu, sig);
    [min(u), max(u), mean(u), var(u)]
    plot_field(pos, els, u)
    if i==1
        colorbar
    end
end
multiplot_adjust_range


%%
% create and show integration points
V = gpcbasis_create('H', 'I', I);
[xi,w] = gpc_integrate([], V, 2, 'grid', 'smolyak');  % sparse grid
%[xi,w] = gpc_integrate([], V, 6, 'grid', 'tensor'); % full tensor
clf;
plot3(xi(1,:), xi(2,:), xi(3,:), '.');

%%
% compute mean and variance of the lognormal field and compare to values
% that we initially specified

g = kl_pce_field_realization(u_i_k, theta_k_alpha, I, xi);
[mean_g, var_g] = kl_pce_moments(u_i_k, theta_k_alpha, I);

u = lognormal_stdnor(g, mu, sig);
mean_u2 = u*w;
plot_field(pos, els, mean_u2)
zlim([0,9]); view(3)

u_bar = binfun(@minus, u, mean_u2);
var_u2 = u_bar.^2*w

% u_i_alpha = ...
% u_i_alpha2 = u_i_alpha + 
    

%%
n_pts = min(length(w),30)
multiplot_init(n_pts)
g = zeros(length(x)*length(y),n_pts);
u = zeros(length(x)*length(y),n_pts);
for i=1:n_pts
    multiplot
    g(:,i) = kl_pce_field_realization(u_i_k, theta_k_alpha, I, xi(:,i));
    u(:,i) = exp(mu+sig*g(:,i));
    plot_field(pos, els, u(:,i));
end

%%
% select internal points
ind = (pos(1,:)>=0 & pos(1,:)<=14000 & pos(2,:)>=0 & pos(2,:)<=10000);
ui = u(:, 1);
ui(~ind) = [];
sum(ind)
clf
plot_field(pos, els, ui)




