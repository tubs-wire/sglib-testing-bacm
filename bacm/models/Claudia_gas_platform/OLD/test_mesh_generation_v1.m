function [f_i_alpha, V_f, Q, pos, els, ind]=get_proxi_model()

nx = 16;
ny = 12;

[xm, x] = linspace_midpoints(-500, 14500, nx-1);
[ym, y] = linspace_midpoints(-500, 10500, ny-1);

[pos, els] = create_rect_mesh(x, y);

% Choose nu between 1 and 2 (realisations with nu=1 would be continuous
% functions and realisations with nu=2 would be one times continously
% differentiable).
nu = 2; 
l_c = 4000;
sigma = 1;

cov_func = funcreate(@matern_covariance, nu, funarg, funarg, l_c, sigma);
%cov_func = funcreate(@gaussian_covariance, funarg, funarg, l_c, sigma);
C = covariance_matrix(pos, cov_func);

%%
G_N = mass_matrix(pos, els);
L = 11;
[r_i_k,sigma_k,L]=kl_solve_evp(C, G_N, L);

D = (max(x)-min(x))*(max(y)-min(y));
var_ex = D * sigma^2; 
var_act = sum(sigma_k.^2);
err_var = (var_ex - var_act) / var_ex;
%strvarexpand('Error in variance: $err_var*100$%');

% keep the variance high (kind of cheating)
sigma_k = sigma_k * sqrt(var_ex / var_act);

%%
% Make a PCE from the KLE 

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);

% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

f_i_k = binfun(@times, r_i_k, sigma_k);

%%
mean_f = 5.5;
var_f = 10.0;

mu = log((mean_f^2)/sqrt(var_f+mean_f^2));
sig = sqrt(log(var_f/(mean_f^2)+1));

% x = linspace(0, mean_u+2*var_u);
% clf
% plot(x, lognormal_pdf(x, mu, sig));

[mean_f_, var_f_] = lognormal_moments(mu, sig)
Q=SimParamSet();
Q.add('f_cm', LogNormalDistribution(mu,sig));

%%
% create and show integration points
V_f = gpcbasis_create('H', 'I', I);
[xi,w] = gpc_integrate([], V_f, 2, 'grid', 'smolyak');  % sparse grid

f_i_alpha=f_i_k* theta_k_alpha;
% field f_cm from integration points

logf_int_field=gpc_evaluate(f_i_alpha, V_f, xi);
f_int_field=exp(sig*f_int_field+mu);

%%
% select internal points
ind = (pos(1,:)>=0 & pos(1,:)<=14000 & pos(2,:)>=0 & pos(2,:)<=10000);
f_int_field(~ind,:)=0;
%%

V_u=V_f;
u_i_alpha = calculate_gpc_coeffs(V_u,  x, u, w, 'method', 'integration');
end



