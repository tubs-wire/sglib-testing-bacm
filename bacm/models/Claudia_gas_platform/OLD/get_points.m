function [x_i,phi_i, w_i]=get_points(V_phi, phi_alpha, V_u, p_int)
[x_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');
phi_i=gpc_evaluate(phi_alpha, V_phi, x_i);
end