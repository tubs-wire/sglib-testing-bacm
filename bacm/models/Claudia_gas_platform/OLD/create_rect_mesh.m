function [pos, els]=create_rect_mesh(x, y, varargin)
% n: number of element in the X direction
% m: number of element in the Y direction

% options=varargin2options( varargin );
% check_unsupported_options( options, mfilename );

%% Mesh definition
% 
% % X and Y coordinates
if length(x)==1 
    n = x;
    x = linspace(0,1,n);
else
    n = length(x);
end
if length(y)==1 
    m = y;
    y = linspace(0,1,m);
else
    m = length(y);
end

% meshgrid
[X,Y]=ndgrid(x,y);
pos=[X(:)'; Y(:)'];

% defining and ploting elements in the mesh
num_pts=n*m;
num_els=(n-1)*(m-1)*2;
els=zeros(3,num_els);

for j=1:m-1
    els(1, ((j-1)*(n-1)*2+1):2:(2*j*(n-1)))=(j-1)*n+1:j*n-1;
    els(1, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=(j-1)*n+1:j*n-1;
    els(2, ((j-1)*(n-1)*2+1):2:(2*j*(n-1)))=(j)*n+2:(j+1)*n;
    els(2, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=(j-1)*n+2:j*n;
    els(3, ((j-1)*(n-1)*2+2):2:(2*j*(n-1)+1))=j*n+2:j*n+n;
    els(3, ((j-1)*(n-1)*2+3):2:(2*j*(n-1)))=j*n+2:j*n+n-1;
    els(3, (j-1)*2*(n-1)+1)=j*n+1;
end

