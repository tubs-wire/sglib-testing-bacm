function main_post_claudia_v1(Q)
%% Define true
f_cm_true=1.89;
n_meas=30;
sigma_meas = 0.00025;
% choose randomly n_meas points from the 60, where we generate measurement
ind_meas=randi(60,[n_meas,1]);
%% Load the truth and generate from it synthetic measurement
S=load('/home/noefried/Gas_platform_Claudia/displacement_f_cm_scalar_1_89.mat');
n_u=length(S.u_true);
plot_u_mean_var(1:n_u, S.u_true, sigma_meas^2*ones(n_u,1), 'line_color', 'r', 'transparency', 0.4, 'fill_color', 'g')

u_true=S.u_true(ind_meas);
true_meas_err=generate_stdrn_simparamset(ones(n_meas,1)*sigma_meas);
u_meas=u_true+true_meas_err.sample(1);
plot(ind_meas, u_meas, 'or')

%% Surrogate model
% number of the samples of 'u' for the ensemble Kalman
n = 100;
% load initial gpc bases (V_u) and integration points (X_i) and
% weights (W_i)
load('/home/noefried/Gas_platform_Claudia/pre_info.mat');
% load solution from the simulation
%[X  Y  u_z]= load_results_to_matlab(save_pathname);
load('/home/noefried/Gas_platform_Claudia/output_all.mat')

% calculate gPCE of the displacement
u_i_alpha = calculate_gpc_coeffs(V_u, x_i, u_z, w_i, 'method', 'project');
% check the surrogate model
u_true_gpc=gpc_evaluate(u_i_alpha, V_u, Q.params2germ(f_cm_true));
% Define map from f_cm to the measurable
g_func = @(q)gpc_evaluate(u_i_alpha(ind_meas,:), V_u, Q.params2germ(q));

% check on the plot whether surrogate model is ok
plot(1:n_u, u_true_gpc, 'c')

%% Statistics 
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
plot_u_mean_var(1:n_u, u_mean, u_var, 'line_color', 'black', 'transparency', 0.4, 'fill_color', 'b')

%% Generate the error model
% Let's assume a bit biger error variance then the one used for the synthetic measurement 
sigma_eps = sigma_meas;
% make a separate simparamset for the measurement errors
E=generate_stdrn_simparamset(ones(n_meas,1)*sigma_eps);

%% MMSE update (of Elmar)

Y_func = @(xi)gpc_evaluate(u_i_alpha(ind_meas,:), V_u, xi);
E_func = @(xi)(E.germ2params(xi));
[e_i_alpha, V_e] = E.gpc_expand();

[Q_i_alpha, V_q] = Q.gpc_expand();

%plot_multi_response_surface(u_i_alpha(50,:), V_u);
p_phi=1;
p_pn=2;
p_int_mmse=3;
p_int_proj=4;
[Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q, u_meas, E_func, V_e, p_phi, p_int_mmse, p_pn, p_int_proj, 'int_grid', 'smolyak');
%mean of the updated f_cm
q_a_mean=gpc_moments(Qn_i_beta, V_qn);
% ensemble of assimilated solutions


sample_q=gpc_sample(Qn_i_beta, V_qn, 1000);
u_a_gpc=gpc_evaluate(u_i_alpha, V_u, Q.params2germ(sample_q));
u_a_mean=mean(u_a_gpc,2);
u_a_var=var(u_a_gpc,[], 2);
plot_u_mean_var(1:n_u, u_a_mean, u_a_var, 'line_color', 'black', 'transparency', 0.4, 'fill_color', 'm')
figure
hist(sample_q,100)
%xi2params_func=@(xi)gpc_evaluate(Qn_i_beta, V_qn, xi);

%% ensemble Kalman filter

V_m=V_u;
meas_i_alpha=u_i_alpha(ind_meas,:);
qa_i=update_q_enKF(Q, E, meas_i_alpha, V_m, u_meas, N);
% Check forward method
[ua_mean, ua_var, calc_model] = caculate_MCmoments_with_model(model, qa_i);
plot_u_mean_var(x,ua_mean, ua_var, 'fill_color', 'y', 'transparency', 0.4)

%% By updating the coefficients (without samples) - Bojana's update
[xi_a_i_alpha, V_xi_a] = gpc_update_enKF(model, Q, E, surr_model, y_m);



end
function qa_i=update_q_enKF(Q, E,  u_i_alpha, V_u, H, y_m, N)
    %The Kalman gain
    [Ke, xi_i_alpha,V_xi]=get_Kalman_gain(Q, E,  u_i_alpha, V_u, H);
    %sample from the germ and calculate the response at the samples points
    xi_i=gpc_sample(xi_i_alpha,V_xi, N);
    % Compute observable from the germ samples
    u_i=gpc_evaluate(meas_i_alpha, V_m, xi_i);
    % Update samples
    xia_i=xi_i+Ke*(binfun(@minus,y_m,model.compute_measurements(u_i)));
    % map xi samples to q samples
    qa_i=Q.stdnor2params(xia_i);
end

function [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(Q, E, y_i_alpha, V_y, y_m)
% gpc of the synthetic measurement
ym_i_alpha=zeros(size(y_i_alpha));
ym_i_alpha(:,1)=y_m;
%The Kalman gain
[Ke, xi_i_alpha,V_xi, ind_xi]=get_Kalman_gain(model, Q, E, y_i_alpha, V_y);
U=Ke*(ym_i_alpha-y_i_alpha);
%expand gpc basis of xi
xi_a_i_alpha=zeros(size(U));
xi_a_i_alpha(:,ind_xi)=xi_i_alpha;
%update gPCE of xi
xi_a_i_alpha=xi_a_i_alpha+U;
V_xi_a=V_xi;
end


function [K, xi_i_alpha,V_xi, ind_xi, ind_y]=get_Kalman_gain(Q, E,  u_i_alpha, V_u, H)

% gPCE of the measurement
C_y=gpc_covariance(y_i_alpha, V_y);
    %P=get_response_covariance(surr_model);
% Get gpc of the error
[e_i_alpha, V_e] = E.gpc_expand();
% Error covariance (C_epsilon)
R = gpc_covariance(e_i_alpha, V_e);

    XI=generate_stdrn_simparamset(ones(Q.num_params));
    [xi_i_alpha,V_xi]=XI.gpc_expand();
    % Calculation of C_xi_yf with combining the gpces
    [C_xi_y, ind_xi, ind_y]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_y, V_xi);
    % the extended Kalman gain
    K=C_xi_y/(C_y+R);
end
function [C_xi_y, ind_xi, ind_y]=caculate_C_xi_y(y_i_alpha, xi_i_alpha, V_u, V_xi)
n_meas=size(y_i_alpha, 1);
n_params=size(xi_i_alpha,1);
% extend the basis to a common basis
[V_xiyf, ind_xi, ind_y]=gpcbasis_combine(V_xi,V_u, 'inner_sum');
% xi coefficients in the new basis
xi_ext_i_alpha=zeros(n_params,gpcbasis_size(V_xiyf,1));
xi_ext_i_alpha(:, ind_xi)=xi_i_alpha;
y_ext_i_alpha=zeros(n_meas, gpcbasis_size(V_xiyf,1));
y_ext_i_alpha(:, ind_y)=y_i_alpha;

% The covariance matrix C_xiy
C_xi_y=gpc_covariance(xi_ext_i_alpha, V_xiyf, y_ext_i_alpha);
end