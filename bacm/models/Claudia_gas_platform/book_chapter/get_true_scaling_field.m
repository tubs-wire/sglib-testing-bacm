function f_cm_true = get_true_scaling_field()
data_pathname = get_data_pathname('RField');
X=load(fullfile(data_pathname,'cheat_xi_true'));
xi_true=X.xi_true;
% X = generate_stdrn_simparamset(ones(11,1));
% rand_seed(7688);
% xi_true = X.sample(1);
 map=get_maps();
 f_cm_true = map.xi2f(xi_true);
% plot_field(pos, els, f_cm_true, 'colormap', magma())
% %f_cm_true = map.xi2f(xi_true);
% 
 function map = get_maps()
 [upsilon_k_alpha, V_y, Q, map, pos, els] = ...
     get_prior_and_proxi_from_example_3_and_4();