%% Compute statistics from the PCE surrogate model
% for this example first Example 1 has to be run
% 
%% Compute the mean and the variance of the displacdments
[y_mean, y_var] = gpc_moments(upsilon_k_alpha, V_y);
% the mean displacement at the 25th assimilation point
display(y_mean(k))
% and the variance of the displacement at the 25th assimilation point
display(y_var(k))

%% Evaluate displacement with any value of the scaling factor (now at  f_cm = 4.768067957746560)
% Map to the reference random variable
xi = f_cm.param2germ(4.768067957746560);
% Compute displacement
y = gpc_evaluate(upsilon_k_alpha, V_y, xi);
% Displacement at the 25th point
display(y(k))
