%% 0) Initiate problem
Initiate_updates_RV()
method = 'PCE KF';

%% 1) Write $Q$, $\vec E$ and $\vec Y$ in a PCE form.
q_j = Q.sample(N);
epsilon_j = E.sample(N);

%% 2) Generate a combined PCE basis
xi_j = q2xi(q_j);
y_j = gpc_evaluate(upsilon_k_alpha, V_y, xi_j);

%% Rewrite the PCE coefficients in the extended basis
%% Compute the Kalman gain
    
    % Autocovariance of the parameter
    C_y=covariance_sample(y_j);
    
    % Covariance of $Q$ and $Y$
    C_qy=covariance_sample(q_j, y_j);

    % Covariance of the measurement noise
    C_e=covariance_sample(epsilon_j);
    
	%% 3b) Compute samples of the measurement model
    z_j= y_j + epsilon_j;
    
    %% 3c) Compute the Kalman gain
    
	% Covariance of the measurment model
    C_z = C_y+C_e;
    % The Kalman gain
    K = C_qy/C_z;
    
%% 4) Compute samples of the posterior (update)

q_j = q_j + K*binfun(@minus, z_m, z_j);

%% 5) Map samples to the scaling factor

f_cm_j = q2f(q_j);

%% Compute statistics
% mean of the posterior
f_cm_mean  = mean(f_cm_j ,2);
% variance of the density
f_cm_var  = var(f_cm_j , [],2);

%% Show statistics
display(strvarexpand('Prior mean: $F_cm.mean$'));
display(strvarexpand('Prior variance: $F_cm.var$'));
display(strvarexpand('True value: $f_cm_true$'));
display(strvarexpand('Posterior mean: $f_cm_mean$'));
display(strvarexpand('Posterior variance: f_cm_var=$f_cm_var$'));

%% Plot prior and posterior densities
bin_width = 0.1;
plot_prior_and_posterior_densities
