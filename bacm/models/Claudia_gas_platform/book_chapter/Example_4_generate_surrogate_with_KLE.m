%% Example 4 -- generate surrogate model by using the KLE of the random field
% This example is a continuation of Example 3., where the seperated
% respresentation was computed.

%% 1) Define prior distribution and map from $\xi$ to $f_{cM}$
% see Example 3 for the maps from $q$ to $f_{cM}$

% The Q is directly defined by the identity map of the germs $\xi$
q2xi = @(q)(q);
xi2q = @(xi)(xi);

% The map from $\xi$ to $f$
xi2f = @(xi) (q2f(xi2q(xi)));
% And its inverse
f2xi = @(f) (q2xi(f2q(f)));

%% 2) Specify the approximating basis

% Define multiindex set
I = multiindex(L, 1);
% Character corresponding to the Hermite polynomials
sys_char = 'H';
V_y = gpcbasis_create(sys_char, 'I', I);
% number of basis functions
M = size(I, 1);
% Show basis polynomials
display(gpcbasis_polynomials(V_y))
% number of polynomials
M = gpcbasis_size(V_y, 1)

%% 3) Compute the squared norms of the basis polynomials
h_alpha = gpcbasis_norm(V_y, 'sqrt', false);
display(h_alpha');

%% 4) Get the integration points and weights
% order of 1D integration rule
p_int = 2;
% integration points and weights
[xi_i_j,w_j] = gpc_integrate([], V_y, p_int, 'grid', 'smolyak');  % sparse grid
% number of integration points
N = length(w_j);
% size of xi
size(xi_i_j)
% graph the ingegration points in the coordinate system defined by X1, X2,
% X3
plot3(xi_i_j(1,:), xi_i_j(2,:), xi_i_j(3,:), 'x')
xlabel('\xi_1')
ylabel('\xi_2')
zlabel('\xi_3')

%% 5) Map the integration points to the random field of the scaling factor
f_cm_k_j = xi2f(xi_i_j);

%% plot the f_cm field at the first 12 integration points
hff= figure();
hff.Position = [210 65 1450 900];
ha=multiplot_init(3,4);

for j=1:12
    multiplot
    colormap(ha(j), 'jet')
    plot_field(pos, els, f_cm_k_j(:,j), 'colormap', plasma(),'view', 3 )
    xlabel('x')
    ylabel('y')
    zlabel(strvarexpand('f_{cM}(x,y, \xi_{$j$})'))
    shading interp
    zlim([1,12])
end
reduce_gap_between_plots(ha, 0.01, 0.05, 0.02, 0.05)
% save figure
save_png(hff, 'fcm_field_realizations', 'figdir', 'figs', 'res', 600)

%% 6) Compute the measurable response
% select internal points
ind = (pos(1,:)>=0 & pos(1,:)<=14000 & pos(2,:)>=0 & pos(2,:)<=10000);
y_k_j = call_FEM_solver_with_inhomogenous_field(f_cm_k_j(ind, :));

%% 7) Evaluate the basis functions at all integration points
psi_alpha_j = gpcbasis_evaluate(V_y, xi_i_j);
display(psi_alpha_j)

%% 8) Compute the PCE coefficients
% Computation of the PCE coefficients
upsilon_k_alpha = y_k_j*(diag(w_j)* psi_alpha_j'/(diag(h_alpha)));
% Show PCE coefficients for the 25th assimilation point
k = 25;
display(upsilon_k_alpha(k, :));
