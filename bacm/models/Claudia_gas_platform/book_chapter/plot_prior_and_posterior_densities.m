%% Plot prior and posterior density
hpp = figure();
% plot prior density
f = linspace(F_cm.dist.invcdf(0), F_cm.dist.invcdf(0.99), 500);
p = F_cm.dist.pdf(f);
h_pri = plot(f,p, '-','color', 'b', 'LineWidth', 2);
hold on
hpp.Position = [680 680 500 320];
h1=histogram(f_cm_j);
h1.Normalization = 'pdf';
h1.EdgeColor = [1 0.5000 0.2000];
h1.FaceColor = [1 0.5000 0.2000];
h1.BinWidth = bin_width;
%ylim([0,8])
xlim([0, 10])
[f,p] = plot_density(f_cm_j);
h_pdf = plot(f, p, '-','color', 'r', 'LineWidth', 2);
y_limits=ylim;
h_mean = plot([f_cm_mean, f_cm_mean], y_limits, ':','color', [0 0 0.5020], 'LineWidth', 2);
h_true = plot([f_cm_true, f_cm_true], y_limits, '-','color', 'k', 'LineWidth', 2);
xlabel('f_{cM}')
ylabel('Probability density');
if ~(strcmp(method, 'MMSE_p1') ||  (strcmp(method, 'MMSE_p2')))
    legend([h_pri, h1, h_pdf, h_mean, h_true],'Prior density', 'Posterior histogram', 'Posterior density', 'Posterior mean', 'True')
end
title(method)
save_png(hpp, ['prior_and_posterior_', method], 'figdir', 'figs', 'res', 600)

%% plot only posterior
hp = figure();
hp.Position = [680 680 500 320];
h1=histogram(f_cm_j);
hold on
h1.Normalization = 'pdf';
h1.EdgeColor = [1 0.5000 0.2000];
h1.FaceColor = [1 0.5000 0.2000];
h1.BinWidth = 0.01;
[f,p] = plot_density(f_cm_j);
h_pdf = plot(f, p, '-','color', 'r', 'LineWidth', 2);
y_limits=ylim;
h_mean = plot([f_cm_mean, f_cm_mean], y_limits, ':','color', [0 0 0.5020], 'LineWidth', 2);
h_true = plot([f_cm_true, f_cm_true], y_limits, '-','color', 'k', 'LineWidth', 2);
xlabel('f_{cM}')
xlim([1,3]);
ylabel('Probability density');
if strcmp(method, 'MCMC') ||(strcmp(method, 'MMSE_p3')||strcmp(method, 'MCMC_low_rank'))
    leg_loc = 'Northeast';
else
    leg_loc = 'Northwest';
end
if ~(strcmp(method, 'MMSE_p1') ||  (strcmp(method, 'MMSE_p2')))
    legend([h1, h_pdf, h_mean, h_true],...
        'Posterior histogram', 'Posterior density', 'Posterior mean', 'True', ...
        'Location', leg_loc)
end
title(method)
save_png(hp, ['posterior_', method], 'figdir', 'figs', 'res', 600)