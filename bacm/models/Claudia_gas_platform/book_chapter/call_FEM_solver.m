function [y_i_j, X, Y] = call_FEM_solver(f_cm_j)
n = length(f_cm_j);
data_pathname=get_data_pathname('RV');

if n==1 && f_cm_j ==1.89
    T=load(fullfile(data_pathname, 'displacement_f_cm_scalar_1_89.mat'));
    y_i_j=T.u_true(1:60,1);
else
    file_name=fullfile(data_pathname,'GEPS3D-Sim-LogNorm', strvarexpand('n$n$-integr-points'));
    [X,  Y,  u_z]= load_results_to_matlab(file_name, n);
    y_i_j = u_z;
end

end