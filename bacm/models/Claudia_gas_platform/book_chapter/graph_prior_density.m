[Q, f_cm] = set_prior();
f = f_cm.linspace(500);
p = f_cm.pdf(f);
h = figure();
plot(f, p, 'LineWidth', 3);
xlabel('f_{cM}')
ylabel('pdf')
xlim([min(f), max(f)])
h.Position=[680 655 432 323];
savefig(h, 'figs/prior_distribution')
save_png(h, 'prior_distribution', 'figdir', 'figs', 'res', 600)
