%% PCE based Kalman filter
%
% In this example it is shown how to do the update of the PCE coefficient
% of the input variable $Q$ using the PCE based Kalman-filter. (See Example
% 7 in the book chapter).

%% 0) Initiate problem
Initiate_updates_RField()
method = 'PCE KF';
% number of samples for the pdf plots (this is only used for estimating the
% mean and quantiles of $F_{cM}$ instead of computing with an analytical form)
N= 50000;

%% 1) Write $Q$, $\vec E$ in a PCE form.
[q_i_alpha_1, V_q] = Q.gpc_expand()
[e_l_beta, V_e] = E.gpc_expand();

%% 2) Generate a combined PCE basis

% First we bring the $Q$s to the PCE basis of $Y_h$ (this in not really
% needed, because they are already defined in the same basis)
[V_qy, Pr_V_q, Pr_V_qy] = ...
     gpcbasis_combine(V_q, V_y, 'inner_sum', 'as_operators', true);
 q_i_alpha=q_i_alpha_1*Pr_V_q;
 upsilon_k_alpha=upsilon_k_alpha* Pr_V_qy;
 
% The combined basis of $Q$, $Y$ and $E$
[V_c, Pr_V_qy, Pr_V_e] = ...
    gpcbasis_combine(V_qy, V_e, 'outer_sum', 'as_operators', true);

%% 3) Rewrite the PCE coefficients in the extended basis
% Please, note, that the order of the basis functions are different here
% then the one used in the chapter and thus the coefficient matrices deviate from the one given in the chapter) 

q_i_gamma=q_i_alpha*Pr_V_qy;
z_j_gamma=upsilon_k_alpha*Pr_V_qy + e_l_beta*Pr_V_e;
zm_j_gamma = zeros(size(z_j_gamma));
zm_j_gamma(:, 1) = z_m;

%% 4) Compute the Kalman gain
    %% 4a) Compute the covariance matrices
    
    % Autocovariance of the measurable response
    C_y=gpc_covariance(upsilon_k_alpha, V_y);

    % Covariance of $Q$ and $Y$
    C_qy=gpc_covariance(q_i_alpha, V_y, upsilon_k_alpha);

    % Covariance of the measurement noise
    C_e=gpc_covariance(e_l_beta, V_e);
    
    %% 4b) Compute the Kalman gain
    
	% Covariance of the measurment model
    C_z = C_y+C_e;
    % The Kalman gain
    K = C_qy/C_z;
    
%% 5) Compute the coefficients of the updated input variables $\vec Q'$ 

q_p_i_gamma = q_i_gamma + K* (zm_j_gamma-z_j_gamma);

%% 5) Generate samples of the scaling factor

% sample from the PCE of the updated input parameter
q_j = gpc_sample(q_p_i_gamma, V_c, N);
% map to the scaling factor
f_cm_j = map.q2f(q_j);

%% Compute statistics

% mean of the posterior density
f_cm_mean  = mean(f_cm_j ,2);
% variance of the posterior density
f_cm_var  = var(f_cm_j , [],2);

%%  Plot true field, mean field, std of the field and error of posterior mean
[h_f, h_fields] = plot_true_mean_var_and_error(pos, els, ...
    f_cm_true, f_cm_mean, f_cm_var, ind_y, z_m);
save_png(h_f, ['true_mean_std_err', method], 'figdir', 'figs', 'res', 600)

%% Plot the prior and the posterior mean and quantiles
h_quant= plot_prior_and_posterior_mean_and_quantiles(f_cm_j, pos, els, mu, sig);
save_png(h_quant, ['field_prior_mean_and_quant', method], 'figdir', 'figs', 'res', 600)