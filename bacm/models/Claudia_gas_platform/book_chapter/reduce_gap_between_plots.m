function reduce_gap_between_plots(mh, hs, vs, hs_left, vs_bottom_or_up, varargin)

options=varargin2options(varargin);
[more_space, options]=get_option(options, 'more_space', 'bottom');
check_unsupported_options(options, mfilename);

if nargin < 5 
    vs_bottom_or_up = [];
end
if nargin < 4
    hs_left = [];
end
if nargin < 3 || isempty(vs)
    vs = 0.01;
end
if nargin < 2 || isempty(hs)
    hs = 0.01;
end
    
if isempty(hs_left)
    hs_left = hs;
end
if isempty(vs_bottom_or_up)
    vs_bottom_or_up = vs;
end
    
[n,m] = size(mh);
hspaces=hs*ones(m+1,1);
hspaces(1)=hs_left;
vspaces=0.01*ones(n+1,1);
switch(more_space)
    case 'top'
        vspaces(1)=vs_bottom_or_up;
    case 'bottom'
        vspaces(n+1)=vs_bottom_or_up;
end
multiplot_modify_spacing(mh, hspaces, vspaces);
