function q_i = bayes_mcmc_single_chain(x2likelihood, Q, N, P, q0)
% BAYES_MCMC Test code for the Bayes MCMC sampling method
% X: Prior SimParamset
% P: Proposal Simparamset (with parameters having the proposal
% distribution)
% x2likelihood function from q to likelihood (log of the likelihood when
% LOG_FlAG is on)
% N: sample number
% x0: starting point of random walk
%
% If LOG_FLAG is true, the X"LIKELIHOOD function needs to give the loglikelihood
%
%   Noemi Friedman & Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

q2pdf_func=@(q) (funcall(x2likelihood, q).*Q.pdf(q));
%% Run metropolis hasting sampler
q_i=q0;
Qs=[];
for i=1:N + N_burn
    xn=q_i+Prop_params.sample(1);
    a=funcall(q2pdf_func,xn)/funcall(q2pdf_func,q_i);
    a=a*Prop_params.pdf(q_i-xn)/Prop_params.pdf(xn-q_i);
    if a>=1 || rand<a
        q_i=xn;
    end
    if i>N_burn
        Qs=[Qs q_i];
    end
end
