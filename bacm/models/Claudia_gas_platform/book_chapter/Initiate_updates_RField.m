%% Initiate update for scenario II (inhomogenous field)
% This code initiates the measurement model and the synthetic measurement
% for scenario II for all the different update methods

%% Get prior and proxi model from Example 4. 
% This task was done in Example 3 and 4.

% Define prior distribution with maps collected in a structure, generate 
% GPC surrogate model
[upsilon_k_alpha, V_y, Q, map, pos, els, xm, ym, ind, ind_y, mu, sig] = ...
    get_prior_and_proxi_from_example_3_and_4();

%% Generate synthetic 'truth'
f_cm_true = get_true_scaling_field();
% Plot the true scaling factor
h =figure();
h.Position = [680   558   560   420];
hf=plot_field(pos, els, f_cm_true, 'colormap', plasma());
% other options for the colormap are MAGMA, INFERNO, PLASMA, VIRIDIS
colorbar()
title('true f_{cM} field')
save_png(h, 'true_scaling_factor_field', 'figdir', 'figs', 'res', 600, 'notitle', false)

y_true = call_FEM_solver_with_inhomogenous_field(f_cm_true);
% number of assimilation points
n_y = size(y_true, 1);

%% Generate the synthetic measurement
% Define the set of SimParameters for the measurement errors

% standard deviation of the measurements
sigma_em = abs(y_true)*0.05;
% Set of parameters with the measurement error
E_m=generate_stdrn_simparamset(sigma_em);
% Fix random seed to have the same measurements for all the methods
rand_seed(736)
% The synthetic measurement
z_m=y_true+E_m.sample(1);

% plot the assimilated displacements and the generated measurements
h = figure();
plot(y_true, 'LineWidth', 2)
hold on
plot(z_m, 'x', 'LineWidth', 2)
xlabel('Index of assimilation point k')
ylabel('Displacement [m]')
legend('True', 'Synthetic measurement', 'Location','SouthEast')
save_png(h, 'Measurements_for_field', 'figdir', 'figs', 'res', 600)

%% Define the error model which is used for the update
%sigma_e = 2.4*10^-4+ones(n_meas,1);
sigma_e = abs(z_m)*0.15;
E = generate_stdrn_simparamset(sigma_e);
%E = generate_stdrn_simparamset(sigma_em*1.25);
