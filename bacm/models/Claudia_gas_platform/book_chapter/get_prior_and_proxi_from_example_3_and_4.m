function [upsilon_k_alpha, V_y, Q, map, pos, els, xm, ym, ind, ind_y, mu, sig] = ...
    get_prior_and_proxi_from_example_3_and_4()
%% Example 3 -- Seperated represenation of the random field $F_{cM}$
% This example shows the KLE of a random field $F_{cM}$

%% 1) Choose a spatial mesh with nodes

% number of nodes in the x direction
nx = 16;
% number of nodes in the y direction
ny = 12;
% meshnodes
[xm, x] = linspace_midpoints(-500, 14500, nx-1);
[ym, y] = linspace_midpoints(-500, 10500, ny-1);
% create the KLE mesh
[pos, els] = create_rect_mesh(x, y);

% The Gramian
G = mass_matrix(pos, els);
% coordinates of measrements
[Xa, Ya]= meshgrid(x(4:end-3), y(4:end-3));
ind_y=ismember(pos', [Xa(:), Ya(:)], 'rows');

%% Plot KLE mesh, FEM mesh and the assimilation points
% hm = figure();
% hm.Position = [680   575   540   400];
% % Plot KLE mesh
% h_KLE = plot_mesh(pos, els, 'width', 3);
% % Plot FEM mesh
% hold on
% [X, Y] = meshgrid(xm, ym);
% h_FEM = mesh(X, Y, X*0-1);
% set(h_FEM,'EdgeColor',[0.6, 0.6, 0.6])
% set(h_FEM, 'FaceColor', [0.9, 0.9, 0.9])
% set(h_FEM,'LineWidth',1.5)
% xlim ([-500, 14500]);
% ylim([-500, 10500])
% % Plot assimilation points
% [Xa, Ya]= meshgrid(x(4:end-3), y(4:end-3));
% ha = plot3(Xa, Ya, Xa*0+1, '.', 'MarkerSize', 30, 'Color', 'c');
% % legend and labels
% legend([h_KLE, h_FEM(1), ha(1)], 'KLE mesh', 'FEM mesh', 'assimilation points')
% xlabel('x')
% ylabel('y')
% save_png(hm, 'KLE_mesh', 'figdir', 'figs', 'res', 600)


%% 2) Compute the Gramian matrix $\vec G$ of the nodal basis
%G = mass_matrix(pos, els);

%% 3) Compute the covariance matrix $\vec C$  from the covariance function

% Define parameters of the Metern-covariance function
% nu = 2; 
% l_c = 4000;
sigma = 1;
% % Define the covariance function
% cov_func = funcreate(@matern_covariance, nu, funarg, funarg, l_c, sigma);
% % Compute covariance matrix
% C = covariance_matrix(pos, cov_func);

%% 4) Solve the generalized eigenvalue problem

% Number of the eigenfunctions and eigenvalues to compute
L = 11;
% Solve the generalized eigenvalue problem
%[v_k_i,sigma_i]=kl_solve_evp(C, G, L);
data_pathname = get_data_pathname('RField');
R=load(fullfile(data_pathname, 'r_i_k.mat'));
S=load(fullfile(data_pathname,'sigma_k.mat'));
v_k_i=R.r_i_k;
sigma_i=S.sigma_k;

D = (max(x)-min(x))*(max(y)-min(y));
var_ex = D * sigma^2; 
var_act = sum(sigma_i.^2);
%err_var = (var_ex - var_act) / var_ex;
%strvarexpand('Error in variance: $err_var*100$%');

% keep the variance high (kind of cheating)
sigma_i = sigma_i * sqrt(var_ex / var_act);

%% 6a) $\vec Q$

Q = generate_stdrn_simparamset(ones(L,1));
q2xi = @(q)(q);
xi2q = @(xi)(xi);

%% 6b) Seperate represenation of the underlying Gaussian field $\theta$

% The matrices collecting the eigenvectors and the eigenvalues
V = v_k_i;
S = diag(sigma_i);
% The map from $q$ to $\theta$
q2theta = @(q) (V*S*q);
% And its inverse map, the theta field projected onto the eigenbasis
theta2q = @(theta) (S\V'*G*theta);

%% 6c) Seperated representation of the scaling factor $F_{cm}$

% Define prior distribution of f_cm
mean_f = 5.5;
var_f = 10.0;

% Parameters of the lognormal field
mu = log((mean_f^2)/sqrt(var_f+mean_f^2));
sig = sqrt(log(var_f/(mean_f^2)+1));

% The maps between the scaling factor and the underlying gaussian field 
theta2f=@(theta)exp(binfun(@plus, theta*sig, mu));
% And the inverse map
f2theta=@(f)(binfun(@minus, log(f), mu)/sig);

% And the ones from q to f and vica versa
q2f = @(q)theta2f(q2theta(q));
f2q = @(f)theta2q(f2theta(f));


% The map from $\xi$ to $f$
xi2f = @(xi) (q2f(xi2q(xi)));
% And its inverse
f2xi = @(f) (q2xi(f2q(f)));

%% Generate surrogate model
%% 2) Specify the approximating basis

% Define multiindex set
I = multiindex(L, 1);
% Character corresponding to the Hermite polynomials
sys_char = 'H';
V_y = gpcbasis_create(sys_char, 'I', I);

%% 3) Compute the squared norms of the basis polynomials
h_alpha = gpcbasis_norm(V_y, 'sqrt', false);

%% 4) Get the integration points and weights
% order of 1D integration rule
p_int = 2;
% integration points and weights
[xi_i_j,w_j] = gpc_integrate([], V_y, p_int, 'grid', 'smolyak');  % sparse grid

%% 5) Map the integration points to the random field of the scaling factor
f_cm_k_j = xi2f(xi_i_j);

%% plot the f_cm field at the first 12 integration points
if false
hff = figure();
hff.Position = [210 65 1450 900];
ha=multiplot_init(3,4);

for j=1:12
    multiplot
    colormap(ha(j), 'jet')
    plot_field(pos, els, f_cm_k_j(:,j), 'view', 3 )
    xlabel('x')
    ylabel('y')
    zlabel(strvarexpand('f_{cM}(x,y, \xi_{$j$})'))
    shading interp
    zlim([1,12])
end
reduce_gap_between_plots(ha, 0.01, 0.05, 0.02, 0.05)
% save figure
 save_png(hff, 'fcm_field_realizations_Claudia', 'figdir', 'figs', 'res', 600)
end
%% 6) Compute the measurable response
% select internal points
ind = (pos(1,:)>=0 & pos(1,:)<=14000 & pos(2,:)>=0 & pos(2,:)<=10000);
y_k_j = call_FEM_solver_with_inhomogenous_field(f_cm_k_j(ind, :));

%% 7) Evaluate the basis functions at all integration points
psi_alpha_j = gpcbasis_evaluate(V_y, xi_i_j);

%% 8) Compute the PCE coefficients
% Computation of the PCE coefficients
upsilon_k_alpha = y_k_j*(diag(w_j)* psi_alpha_j'/(diag(h_alpha)));

%% Store all the maps
map.xi2q = xi2q;
map.q2xi = q2xi;
map.q2f = q2f;
map.f2q = f2q;
map.xi2f = xi2f;
map.f2xi = f2xi;