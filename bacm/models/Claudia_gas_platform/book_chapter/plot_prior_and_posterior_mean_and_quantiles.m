function h_quant = plot_prior_and_posterior_mean_and_quantiles(f_cm_i, pos, els, mu, sig)
%% Plot the prior and the posterior mean and quantiles
% compute mean and quantile
f_cm_mean = mean(f_cm_i,2);
f_cm_quant= quantile(f_cm_i', [0.05, 0.95]);
h_quant = figure();
h_quant.Position = [680 600 720 360];
multiplot_init(1,2)
multiplot
dist = LogNormalDistribution(mu, sig);
f1 = plot_field(pos, els, dist.invcdf(0.05)*ones(size(f_cm_mean)), 'colormap', magma());
f1.EdgeColor ='none';
hold on
f2 = plot_field(pos, els, dist.mean*ones(size(f_cm_mean)), 'colormap', magma());
f2.EdgeColor ='none';
f3 = plot_field(pos, els, dist.invcdf(0.95)*ones(size(f_cm_mean)), 'colormap', magma());
f3.EdgeColor ='none';
xlabel('x')
xlabel('y')
zlabel('f_{cM}')
view([-20.5000   25.0000]);

multiplot
f1 = plot_field(pos, els, f_cm_quant(1,:)', 'colormap', magma());
f1.EdgeColor ='none';
hold on
f2 = plot_field(pos, els, f_cm_mean, 'colormap', magma());
f2.EdgeColor ='none';
f3 = plot_field(pos, els, f_cm_quant(2,:)', 'colormap', magma());
f3.EdgeColor ='none';
xlabel('x')
xlabel('y')
zlabel('f`_{cM}')
view([-20.5000   25.0000]);
%reduce_gap_between_plots(h_quant)
%multiplot_modify_spacing(h_quant, 0.25, 0.17)