function y_k_j = call_FEM_solver_with_inhomogenous_field(f_cm_k_j)

% For the true field
if size(f_cm_k_j,2)==1
    f_cm_true = f_cm_k_j;
    [upsilon_k_alpha, V_y, Q, map] = ...
    get_prior_and_proxi_from_example_3_and_4();
    y_true = gpc_evaluate(upsilon_k_alpha, V_y, map.f2xi(f_cm_true));
    y_k_j = y_true;
else
    
    data_pathname=get_data_pathname('RField');
    S=load(fullfile(data_pathname,'displfield'));
    y_k_j=S.displfield;
end

