function data_pathname=get_data_pathname(type)

switch(type)
    case 'RField'
        
        if isunix
            home_name =char(java.lang.System.getProperty('user.home'));
            if strcmp(home_name, '/home/noemi')
                data_pathname=[home_name, '/windoc/Projects/Claudia_gas_platform/RField'];
            else
                data_pathname=[home_name, '/Projects/Claudia_gas_platform/RField'];
            end
        elseif ispc
            data_pathname='F:\noemi\Documents\Claudia_gas_platform\RField';
        end
        
    case 'RV'
        if isunix
            home_name =char(java.lang.System.getProperty('user.home'));
            if strcmp(home_name, '/home/noemi')
                data_pathname=[home_name, '/windoc/Projects/Claudia_gas_platform/RV'];
            else
                data_pathname=[home_name, '/Projects/Claudia_gas_platform/RV'];
            end
        elseif ispc
            data_pathname='F:\noemi\Documents\Claudia_gas_platform\RV';
        end
        
end