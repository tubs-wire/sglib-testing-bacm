%% Example 11 -- parameter update by the MMSE using low-rank representation of the measurement model
%
% This is an example on how to update the homogenous scaling factor using
% the Minimium Mean Square Estimator

%% 1) Write measurement parameter $Q$, error model $E$ and the response $Y$ in a PCE form.

method = 'MMSE_p2';
% We use the PCE of the projected error and response model computed in
% Example 10.
Example_9_low_rank_approx_of_the_measurement_model;
% number of the measurements (now the projected one)
n_y_r = R;

% The PCE of the input parameter
[q_i_alpha_1, V_q] = Q.gpc_expand;

%% 2) Generate a combined PCE basis

% First we bring the $Q$s to the PCE basis of $Y_h$
[V_qy, Pr_V_q, Pr_V_y] = ...
     gpcbasis_combine(V_q, V_y, 'inner_sum', 'as_operators', true);
% Unified basis
[V_z, Pr_V_qy, Pr_V_e] = ...
    gpcbasis_combine(V_y, V_e_r, 'outer_sum', 'as_operators', true);

 %% 3) Rewrite PCE coefficients in the modified basis
 
% The PCE coefficients of the inpute parameter with the unified basis
q_i_alpha=q_i_alpha_1*Pr_V_q;
q_i_gamma = q_i_alpha*Pr_V_qy;
% The PCE coefficients of the projected response $Y'$
upsilon_k_alpha = upsilon_r_k_alpha* Pr_V_y;
upsilon_k_gamma = upsilon_k_alpha*Pr_V_qy;
% The PCE coefficients of the projected measurement errror
e_k_gamma = e_r_k_beta*Pr_V_e;
% The PCE coefficients of the measurement model
z_k_gamma = upsilon_k_gamma + e_k_gamma;

%% 4) Determine the approximating basis for $\varphi$

p_phi = 2;
% This is not really a GPCE basis, but the function system comes handy
% System character corresponding to the monomials
syschar = 'M';
V_phi=gpcbasis_create(syschar, 'm', n_y_r, 'p', p_phi);
gpcbasis_polynomials(V_phi, 'symbols', {'z1`', 'z2`', 'z3`'})

%% 5a) and 6a) Get integration points and weights
% Polynomials degree of the PCE of Q
p_q = gpcbasis_info(V_q, 'total_degree');
% Polynomial degree of the PCE of Y
p_gpc= gpcbasis_info(V_y, 'total_degree');
% Itengration order needed for integrating elements of A and b
p_int=max(p_gpc*p_phi+1,ceil((p_q+p_phi*p_gpc+1)/2));
% The integration points to compute matrix $\vec A$ and vector $\vec b$ 
[zeta_k_j, w_j] = gpc_integrate([], V_z, p_int, 'grid', 'full_tensor');

%% 5b) and 6b) Compute the elements of A  and b      
% Evaluate Q and Z at the integration points
q_i_j = gpc_evaluate(q_i_gamma, V_z, zeta_k_j);
z_j_k = gpc_evaluate(z_k_gamma, V_z, zeta_k_j);
% Evaluate \phi of z at integration points
Psi_delta_k = gpcbasis_evaluate(V_phi, z_j_k);
% Compute matrix A
wPsi_delta_k = binfun(@times, Psi_delta_k, w_j');
A = Psi_delta_k * wPsi_delta_k';
% Evaluate matrix b
b = q_i_j * wPsi_delta_k';

%% 7) Solve the system of equations
% The coefficients of the estimator
phi_i_delta = (A\b')';

%% 8) Generate samples of the scaling factor

% Sample from the unified germ zeta_j = [xi, eta]
zeta_j = gpcgerm_sample(V_z, N);
% Compute prior parameter at the samples
q_j = gpc_evaluate(q_i_gamma, V_z, zeta_j);
% Compute measurement model at the samples
z_j = gpc_evaluate(z_k_gamma, V_z, zeta_j);
% Mapped measurement model
phi_of_z_j = gpc_evaluate(phi_i_delta, V_phi, z_j);
% Compute the best estimate of q
q_m = gpc_evaluate(phi_i_delta, V_phi, z_r_m);
% sample from the posterior 
qp_j= q_j - phi_of_z_j + q_m;

%% Map samples to the scaling factor

f_cm_j = q2f(qp_j);

%% Compute statistics

% mean of the posterior density
f_cm_mean  = mean(f_cm_j ,2);
% variance of the posterior density
f_cm_var  = var(f_cm_j , [],2);
% value at the best estimate of q
f_cm_best = q2f(q_m);

%% Show statistics
display(strvarexpand('Prior mean: $F_cm.mean$'));
display(strvarexpand('Prior variance: $F_cm.var$'));
display(strvarexpand('True value: $f_cm_true$'));
display(strvarexpand('Posterior mean: $f_cm_mean$'));
display(strvarexpand('Posterior variance: f_cm_var=$f_cm_var$'));
display(strvarexpand('Best estimate: f_cm_b=$f_cm_best$'));

%% Plot prior and posterior densities
bin_width = 0.1;
plot_prior_and_posterior_densities
