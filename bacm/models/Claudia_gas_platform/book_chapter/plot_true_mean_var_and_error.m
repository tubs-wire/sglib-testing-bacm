function [h_f, h_fields]=plot_true_mean_var_and_error(pos, els, f_cm_true, f_cm_mean, f_cm_var, ind_y, z_m) 

%% Plot true field, mean field, variance and error
h_f = figure();
h_f.Position = [10 420 1600 550];
h_fields = multiplot_init(2,4);
% Plot true
multiplot;
plot_field(pos, els, f_cm_true, 'colormap', magma())
% hold on
% c = abs(z_m)/max(abs(z_m))*max(f_cm_true);
% scatter3(pos(1, ind_y), pos(2,ind_y), 8*ones(size(z_m')), [], c', 'filled')
xlabel('x')
ylabel('y')
title('f_{cM,m}');
colorbar();

multiplot;
plot_field(pos, els, f_cm_true, 'colormap', magma())
xlabel('x')
ylabel('y')
zlabel('f_{cM,m}')
view([-20.5000   25.0000]);

% Plot mean of the posterior
multiplot;
plot_field(pos, els, f_cm_mean, 'colormap', magma())
colorbar();
% hold on
% c = abs(z_m)/max(abs(z_m))*max(f_cm_mean);
% scatter3(pos(1, ind_y), pos(2,ind_y), 6*ones(size(z_m')), [], c', 'filled')
xlabel('x')
ylabel('y')
title('E[f´_{cM}]')

multiplot;
plot_field(pos, els, f_cm_mean, 'colormap', magma())
view([-20.5000   25.0000]);
xlabel('x')
ylabel('y')
zlabel('E[f´_{cM}]')
%h_fields(4).ZLim=h_fields(2).ZLim;

% Plot standard deviation
multiplot;
sigma =  sqrt(f_cm_var);
plot_field(pos, els, sigma, 'colormap', magma())
hold on
c = abs(z_m)/max(abs(z_m))*max(sigma);
scatter3(pos(1, ind_y), pos(2,ind_y), 3*ones(size(z_m')), [], c', 'filled')
colorbar();
title('\sigma_f´')
xlabel('x')
ylabel('y')

multiplot;
plot_field(pos, els, sqrt(f_cm_var), 'colormap', magma())
view([-20.5000   25.0000]);
zlabel('\sigma_{f´}')
xlabel('x')
ylabel('y')

% Plot mean-true
multiplot;
plot_field(pos, els, abs(f_cm_mean-f_cm_true), 'colormap', magma())
colorbar();
hold on
c = abs(z_m)/max(abs(z_m))*max(abs(f_cm_mean-f_cm_true));
scatter3(pos(1, ind_y), pos(2,ind_y), 8*ones(size(z_m')), [], c', 'filled')
xlabel('x')
ylabel('y')
title('e=|f_{cM,m}-f_{cM,mean}|');

multiplot;
plot_field(pos, els, abs(f_cm_mean-f_cm_true), 'colormap', magma())
view([-20.5000   25.0000]);
xlabel('x')
ylabel('y')
zlabel('e');

%reduce_gap_between_plots(h_fields, 0.01, 0.01, 0.02, 0.1, 'more_space', 'top')
multiplot_modify_spacing(h_fields, 0.25, 0.17)