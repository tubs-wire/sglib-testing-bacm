% Set random variable and make a gPCE of it
Q=MySimParamSet();
Q.add('f_cm', UniformDistribution(1,10));

% Set approximating subspace for the proxi model
V_q=Q.get_germ();
gpc_order=3;
V_u=gpcbasis_modify(V_q,'p', gpc_order);
% 
% Call integration rule
p_int=gpc_order+1;
[xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

f_cm_i=Q.germ2params(xi_i);

%save('C:\Users\Claudia\Documents\Dottorato\Collaborazione-Germania\Code\sglib-testing-master\pre_info.mat', 'xi_i','f_cm_i' 'w_i', 'V_u', 'Q');