function[r_i_k, sigma_k]=get_eigenfunc_and_vals(u_i_alpha, V_u, L)

C=gpc_covariance(u_i_alpha, V_u);
[r_i_k,sigma_k]=kl_solve_evp(C, [], L);
