%% get snapshots and mesh from FenICs
clear
clc

% Define folder where snapshots are saved
if isunix
    root_folder='/home/noefried/Documents/SFB880/JP5/flow-fenics/old';
elseif ispc
    root_folder='F:\noemi\Documents\SFB880\SFB_JP\flow-fenics\old';
end

% read snapshots from file
folder_names={...
    'uncontrolled_skew_symm_scheme',...
    'controlled_skew_symm_scheme',...
    'controlled_with_chirp_cordier',...
    'controlled_with_chirp_skew_symm',...
    'uncontrolled_cordier'...
    };

file_index=1;

sol_folder=[root_folder, filesep, folder_names{file_index}, filesep, 'tmp'];
[u,v,p]=get_snapshots(sol_folder);

% load mesh
mesh=get_mesh([root_folder, filesep, folder_names{file_index}]);
pos=mesh.coord';
els=double(mesh.elem_nodes+1)';

N_x=size(pos,2);
N_s=size(u,2);
%% ROM
% put responses in one vector
U=[u;v;p];
%G=mass_matrix(pos, els);
L=40;
[phi_j_k, lambda_k, e_perc]=ROM(U, L);
[phi_j_ks, lambda_ks, e_percs]=ROM(U, L, [], 'method', 'snapshots');
subplot(1,2,1)
plot_field(pos, els, phi_j_k(1:N_x,1), 'show_mesh', false)
subplot(1,2,2)
plot_field(pos, els, phi_j_ks(1:N_x,1), 'show_mesh', false)

phi_j_k=reshape(phi_j_k, N_x, 3, []);

phi_u=squeeze(phi_j_k(:,1,:));
phi_v=squeeze(phi_j_k(:,2,:));
phi_p=squeeze(phi_j_k(:,3,:));

%% plot eigenfunctions

% First plot captured energy level
figure
plot(e_percs(1:20), 'x')
hold on
x_lim=xlim();
plot(x_lim, [95, 95])
hold on
xlabel('number of eigenmodes')
ylabel('energy level captured [%]')

% Plot mean flow and first 5 eigenfunctions
figure
poslim=minmax(pos);
for k=1:12
    
    subplot(3,4,k)
    if k==1
        phi_U=[mean(u,2), mean(v,2)];
    else
        phi_U=[phi_u(:,k-1), phi_v(:,k-1)];
    end
    plot_field(pos, els, diag(sqrt(phi_U*phi_U')), 'show_mesh', false)
    title(strvarexpand('k=$k-1$'))
    
    xlim(poslim(1,:))
    axis equal
    ylim(poslim(2,:))
end
%%

%% Statistics


%% Plot mean and var of responses

%% make a video
snapshots_to_video(sol_folder, mesh, N_s, ['cylinder_wake',folder_names{file_index}])

