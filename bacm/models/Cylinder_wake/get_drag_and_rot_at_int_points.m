function [lift, drag, rot_norm, t, u]=get_drag_and_rot_at_int_points(fenics_folder)
act_path=pwd;
cd(fenics_folder)
dirfiles=dir(fenics_folder);
file_names={dirfiles.name};
file_names=file_names(3:end);
% get rid of figures folder if there is one
ind=strcmp(file_names, 'figures');
file_names=file_names(~ind);
% get only the right files
S=load(file_names{1});
t=S.t;
N_t=length(t);
% initialize memory
N=length(file_names);
lift=zeros(N_t, N);
drag=zeros(N_t, N);
rot_norm=zeros(N_t, N);
if nargout>4
    u=zeros(N_t, N, 2);
end

for i=1:length(file_names)
    S=load(file_names{i});
    lift(:,i)=S.lift_and_drag(:,2);
    drag(:,i)=S.lift_and_drag(:,1);
    rot_norm(:,i)=S.rot;
    if nargout>4
        u(:, i, :)=S.u;
    end
end

cd(act_path)