function [phi_j_k, lambda_k, e_perc]=ROM(u, L, G, varargin)

options=varargin2options( varargin );
[method,options]=get_option( options, 'method', 'normal');
check_unsupported_options( options, mfilename );
if nargin<3
    G=[];
end

% mean velocity
u_mean=mean(u,2);
% fluctuating part of the velocity
uf_j_k=binfun(@minus, u, u_mean);
switch method
    % normal spatial covariance
    case 'normal'
        C=covariance_sample(uf_j_k);
        [r_i_k, sigma_k, L]=kl_solve_evp(C, G, L);
        % Method of snapshots (covariance from the snapshots)
    case 'snapshots'
        C=uf_j_k'*uf_j_k;
        [r_i_k, sigma_k, L]=kl_solve_evp(C, [], L);
end

lambda_k=sigma_k.^2;
%plot(lambda_k(1:10));
if nargout>2
    e_perc=cumsum(lambda_k)/sum(lambda_k)*100;
end
plot(1:20,e_perc(1:20), 'x')
switch method
    % normal spatial covariance
    case 'normal'
        phi_j_k=r_i_k;
        % Method of snapshots (covariance from the snapshots)
    case 'snapshots'
        % take lin comb from snapshots
        phi_j_k=uf_j_k*r_i_k;
end


% normalise
phi_j_k=row_col_mult( phi_j_k, 1./sqrt(diag(phi_j_k'*phi_j_k)') );