function [q_j, w_j, Q, lift_j, drag_j, rot_norm_j, t]=get_points_and_solutions(p_int, load_or_get,...
    root_folder, subfolder_name, save_points_flag, extend_with_uncontrolled_flag)

if ~iscell(subfolder_name)
    subfolder_name={subfolder_name};
end

N=length(subfolder_name);
q_j=[];
w_j=cell(1,N);
Q=cell(1,N);
if nargout>3
    lift_j=[];
    drag_j=[];
    rot_norm_j=[];
end
for i=1:N
    subfolder_i=subfolder_name{i};
    
    %% Load integration points
    Points_i=struct;
    [Points_i.q_j, Points_i.w_j, Q{i}]=get_integration_points...
        (p_int, load_or_get, extend_with_uncontrolled_flag, save_points_flag, root_folder,subfolder_i);
    q_j=[q_j, Points_i.q_j];
    w_j{i}=Points_i.w_j;
    
    %% Load drag and rot results
    
    if nargout>3
        Sol_i=struct;
        if extend_with_uncontrolled_flag
            n=length(unique(Points_i.q_j(2,:)));
        else
            n=0;
        end
        [Sol_i.lift, Sol_i.drag, Sol_i.rot_norm, t]=get_solution(root_folder,subfolder_i, n);
        lift_j=[lift_j, Sol_i.lift];
        drag_j=[drag_j, Sol_i.drag];
        rot_norm_j=[rot_norm_j, Sol_i.rot_norm];
    end
    if N==1
        Q=Q{1};
        w_j=w_j{1};
    end
end
end
function [q_j, w_j, Q]=get_integration_points(p_int, load_or_get, extend_with_uncontrolled_flag, save_points_flag, root_folder,subfolder_name)
[q_j, w_j, Q]=define_control_params(p_int, load_or_get, save_points_flag, root_folder, subfolder_name);
Stf_unique=unique(q_j(2,:));
if extend_with_uncontrolled_flag
    n=length(Stf_unique);
    A_noncontr=zeros(1,n);
    q_j=[q_j, [A_noncontr; Stf_unique]];
    w_j=[w_j; zeros(n,1)];
end
end
function [lift, drag, rot_norm, t]=...
    get_solution(root_folder,subfolder_name, n)
switch subfolder_name
    case 'integration_points_on_small_domain'
        file_name= 'Results';
    case 'integration_points_on_medium_domain'
        file_name= 'Results_medium_domain';
    case 'integration_points_on_large_domain'
        file_name='Results_large_domain';
    case 'integration_points_at natural frequency1'
        file_name='Results_nat_freq_1';
    case 'integration_points_at natural frequency2'
        file_name='Results_nat_freq_2';
    case 'integration_points_qmc'
        file_name='Results_qmc';
    case 'A10Stf1_qmc'
        file_name='Results_A10Stf1_qmc4';
    case 'qmc_points_around_f1'
        file_name='Results_qmc_points_around_f1';
    otherwise
        file_name=[];
end
fenics_folder=[root_folder,filesep, file_name];
[lift, drag, rot_norm, t]=get_drag_and_rot_at_int_points(fenics_folder);
% add to the int points some points with A=0 (uncontrolled cylinder)
if n>0
    S=load([root_folder, filesep, 'uncontrolledsolinfo.mat']);
    lift=[lift, repmat(S.lift_and_drag(:,2), 1,n)];
    drag=[drag, repmat(S.lift_and_drag(:,1), 1,n)];
    rot_norm=[rot_norm, repmat(S.rot', 1, n)];
end
end