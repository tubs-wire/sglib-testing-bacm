function try_kriging()
root_folder=define_root_folder();

p_int=6;
subfolder_names=...
    {'integration_points_on_small_domain',...
    'integration_points_on_medium_domain',...
    'integration_points_on_large_domain',...
    'integration_points_at natural frequency1',...
    'integration_points_at natural frequency2',...
    'integration_points_qmc',...
    'qmc_points_around_f1',...
    'A10Stf1_qmc'};

% subfolder_name=subfolder_names(1,[1,2,4]);
% [q_i, w_i, Q, lift_i, drag_i, rot_norm_i, t]=get_points_and_solutions(p_int,...
%     root_folder, subfolder_name, false, true);

subfolder_name=subfolder_names(1,[1,2,4, 5, 6,7]);
[q_i, w_i, Q, lift_i, drag_i, rot_norm_i, t]=get_points_and_solutions(p_int,'load',...
    root_folder, subfolder_name, false, true);

stable_ind=find(t>4);
Ab=30;
Stfb=4.5;
Stfa=1;
ind=(q_i(1,:)>Ab |q_i(2,:)>Stfb);
    q_i=q_i(:,~ind);
    lift_i=lift_i(:,~ind);
    drag_i=drag_i(:,~ind); 
    rot_norm_i=rot_norm_i(:,~ind);
mean_lift_i=mean(lift_i(stable_ind,:));
mean_drag_i=mean(drag_i(stable_ind,:));
mean_rot_i=mean(rot_norm_i(stable_ind,:));


xs=q_i
S=xs;
Y=[mean_lift_i;mean_drag_i;mean_rot_i];
Y=Y';
S=S';
%<<<<<<< HEAD
regr=@regpoly1;
%Kriging proxi model
[dmodel, perf]=dacefit(S, Y, regr, @corrgauss, 0.1);

[pos, els]=create_mesh_2d_rect(5);
pos(1, :)=pos(1,:)*Ab;
pos(2, :)=pos(2,:)*(Stfb-Stfa)+Stfa
Y_pred=predictor(pos', dmodel);
clf
plot_field(pos, els, -Y_pred(:,2), 'show_mesh', false) 
hold on
plot3(q_i(1,:), q_i(2,:), -mean_drag_i, 'kx')
xlabel('A')
ylabel('Stf')
zlabel('<C_D>')

%=======
%>>>>>>> 2e34ba0d2becf93163fce0769d2fc263809ffdc6

m=size(S,2);
corrf={@corrgauss, @correxp, @correxpg, @corrlin, @corrcubic, @corrspherical, @corrspline};
n_c=length(corrf);
regr={@regpoly0, @regpoly1, @regpoly2};
n_r=length(regr);
theta=linspace(0.01,2, 20);
n_t=length(theta);
err_L2=zeros(n_c, n_r, n_t);
lob=0.1*ones(6,1);
upb=20*ones(6,1);
for k=4:n_c
    display(k)
    for j=1:n_r
        regr_j=regr{j};
        corf_k=corrf{k};
        display(j)
        for i=1:length(theta)
            %display(i)
            [dmodel, perf]=dacefit(S, Y, regr_j, corf_k, theta(i));
            Y_pred=predictor(S_check, dmodel);
            err_L2(k,j,i)=norm(Y_pred -Y_check);
        end
    end
end
%prediction at best point
for k=1:n_c
    plot(theta, squeeze(err_L2(k,1,:)))
    hold on
    plot(theta, squeeze(err_L2(k,2,:)), '--')
    plot(theta, squeeze(err_L2(k,3,:)), '-o' )
end

[ind, opt_err]=min(squeeze(err_L2(k,3,:)))
opt_theta=theta(11)

plot(ypred)
hold on; plot(ub, 'red', 'LineWidth', 2)
p_gpc=8;
[V_u, u_i_alpha, sensitivities, RVs, meas, coord, x, u, u_mean, u_var]=main_UQ(p_gpc, 'point_type', 'scan_point', 'prior_dist','default');
u_gpc_b=gpc_evaluate(u_i_alpha, V_u, RVs.params2germ(xb));
plot(u_gpc_b, 'c')