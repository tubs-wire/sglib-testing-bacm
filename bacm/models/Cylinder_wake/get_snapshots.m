function [u,v,p]=get_snapshots(root_folder, varargin)
%N_x: number length(Mesh.coord(:,1))
%N_s: number of snapshots n_snapshot

options=varargin2options( varargin );
[max_N_s,options]=get_option( options, 'max_snapshots', 'default');
check_unsupported_options( options, mfilename );


dirinfo=dir([root_folder,filesep, 'sol*.mat']);
file_names={dirinfo.name};
N_s=length(file_names);
file_name=strcat(root_folder, filesep, file_names{1});
   sol=load(file_name);
N_x=size(sol.u,1);
if  strcmp(max_N_s,'default')
    max_N_s=N_s;
end
    

p=zeros(N_x,N_s);
u=zeros(N_x,N_s);
v=zeros(N_x,N_s);
for i=1:min(N_s, max_N_s)
    file_name=strcat(root_folder, filesep, 'sol', num2str(i,'%05d'));
    sol=load(file_name);
    p(:,i)=sol.p;
    u(:,i)=sol.u(:,1);
    v(:,i)=sol.u(:,2);
end