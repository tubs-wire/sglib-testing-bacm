function snapshots_to_video(sol_folder, Mesh, n_snapshot, video_name)
% video_name=e.g. 'cylinder_flow.avi'
% Mesh with structure Mesh.elem_nodes, Mesh.coord
% n_snapshot: number of snapshot files
% root folder: where the snapshots are saved
act_path=pwd;
cd(sol_folder)
h_fig=figure;
v = VideoWriter([video_name,'.avi']);
open(v);

for i=1:n_snapshot
    file_name=strcat(sol_folder, filesep, 'sol', num2str(i,'%05d'));
    Sol=load(file_name);
    subplot(2,1,1)
    my_plot_field(Mesh.coord', (Mesh.elem_nodes+1)', diag(sqrt(Sol.u*Sol.u')), 'show_mesh', false, 'change_colorscale', false)
    if i==1
        cau=caxis;
    end
    caxis(cau);
    title(strvarexpand('Velocity, timestep: $i$'))
    axis equal
    drawnow
    subplot(2,1,2)
    my_plot_field(Mesh.coord', (Mesh.elem_nodes+1)', Sol.p', 'show_mesh', false, 'change_colorscale', false)
    if i==1
        cap=caxis;
    end
    caxis(cap);
    title(strvarexpand('Pressure, timestep: $i$'))
    axis equal
    %F(i) = getframe(h_fig);
    frame=getframe(h_fig);
    writeVideo(v,frame);
    drawnow
end

close(v)
cd(act_path)