function fft_analysis()
Col=set_my_favorite_colors();
root_folder=define_root_folder();

%% Define signal for fft
% Diameter of the cylinder
D=0.1;
% Reference speed
u_0=1;
% Density
rho=1;
% Load simulation data
%S=load([root_folder, filesep 'old/Results_uncontrolled_long/sol_longsolinfo.mat']);
S=load([root_folder, filesep 'old/Results_uncontrolled_long/sol_longsolinfo.mat']);
t=S.t;
% Drag coefficient
drag =S.lift_and_drag(:,1);
drag =-2*drag/(rho*u_0^2*D);
% Velocities 0.35m after cylinder 
ux=S.u(:,1);
uy=S.u(:,2);
rot= S.rot;

% test function for checking
testu = 0.8*sin(5*pi*2*t)+0.7*sin(30*pi*2*t);
%% Define window for fft analysis (cut the beginning of the signal)
% window for fft analysis (when the flow is stabilised)
t_min=4;
% cut the beginning of the sinal
ind=(t>t_min);
% to make even number of samples
if mod(sum(ind),2)==1
    ind(end)=false;
end
xt=t(ind);
% the signals
xdrag =drag(ind);
xux=ux(ind);
xuy=uy(ind);
xrot=rot(ind);
xtestu=testu(ind);
%% FFT
% Length of signal
L = length(xdrag);
% Sample time
dt = xt(2)-xt(1); 
% Sampling frequency
Fs = 1/dt;                    

%% Compute single-sided spectrums
%drag
Ydrag = fft(xdrag)/L;
Ydrag_s1=create_one_sided_spectrum(abs(Ydrag));

%rot
Yrot = fft(xrot)/L;
Yrot_s1=create_one_sided_spectrum(abs(Yrot));

%velocities
Yux= fft(xux)/L;
Yux_s1=create_one_sided_spectrum(abs(Yux));
Yuy= fft(xuy)/L;
Yuy_s1=create_one_sided_spectrum(abs(Yuy));

%testfunction
Ytest= fft(xtestu)/L;
Ytest_s1=create_one_sided_spectrum(abs(Ytest));

%% and the corresponding frequency

f = Fs*(0:(L/2))/L;

%% test fft
% figure
% plot(f,Ytest_s1, 'LineWidth', 2)

%% Plot single-sided amplitude spectrum of instantinous drag
figure
subplot(2,1,1)
plot(t,drag)
y_lim=ylim();
y_lim(1)=0;
ylim(y_lim);

rectangle('position',[t_min, y_lim(1), max(t)-t_min, y_lim(2)-y_lim(1)],...
    'facecolor',Col.grey)
hold on
% Plot arrow of fft window
y_pos_of_arrow=(y_lim(2)-y_lim(1))/2+y_lim(1);
plot([t_min, max(t)],[y_pos_of_arrow, y_pos_of_arrow], 'k', 'LineWidth', 1.5);
plot(t_min, y_pos_of_arrow, 'k<')
plot(max(t), y_pos_of_arrow, 'k>')
plot(t,drag, 'LineWidth', 2,  'Color', Col.navy_blue)
title('Instantinious drag in the time domain')
xlabel('time')
ylabel('C_D')

subplot(2,1,2)
plot(f,Ydrag_s1, 'LineWidth', 2)
xlim([2,6]);
ylim([0,0.2]);
title('Instantinious drag in the frequency domain')
[ampl,ind]=findpeaks(Ydrag_s1);
hold on
f1=f(ind(1));
f2=f(ind(2));
y_lim=ylim();
plot([f1, f1], y_lim)
plot([f2, f2], y_lim)
axis=gca;
x_tick=axis.XTick;
x_tick=sort([x_tick, f1, f2]);
axis.XTick=x_tick;
xlabel('frequency[Hz]')
ylabel('Amplitude spectrum |C_D(f)|')

%% Plot single-sided amplitude spectrum of ux
figure
subplot(2,1,1)
plot(t,ux)
hold on
plot(t, uy)
y_lim=ylim();
clf
subplot(2,1,1)
rectangle('position',[t_min, y_lim(1), max(t)-t_min, y_lim(2)-y_lim(1)],...
    'facecolor',Col.grey)
hold on

h(1)=plot(t, ux, 'LineWidth', 2,  'Color', Col.navy_blue);
h(2)=plot(t, uy, 'LineWidth', 2,  'Color', Col.orange);

% Plot arrow of fft window
y_pos_of_arrow=(y_lim(2)-y_lim(1))/2+y_lim(1);
plot([t_min, max(t)],[y_pos_of_arrow, y_pos_of_arrow], 'k', 'LineWidth', 1.5);
plot(t_min, y_pos_of_arrow, 'k<')
plot(max(t), y_pos_of_arrow, 'k>')

legend(h, {'U_x', 'U_y'})
title('Instantinious velocity in the time domain')
xlabel('time')
ylabel('U')

subplot(2,1,2)
hf(1)=plot(f,Yux_s1, 'LineWidth', 2, 'Color', Col.navy_blue);
hold on
hf(2)=plot(f,Yuy_s1, 'LineWidth', 2, 'Color', Col.orange);
xlim([1,6]);
legend(hf, {'U_x', 'U_y'})
title('Instantinious velocity in the frequency domain')
[ampl,ind]=findpeaks(Yux_s1);
[ampl,ind]=findpeaks(Yuy_s1);
hold on
y_lim=ylim();
plot([f1, f1], y_lim)
plot([f2, f2], y_lim)
axis=gca;
x_tick=axis.XTick;
x_tick=sort([x_tick, f1, f2]);
axis.XTick=x_tick;
xlabel('frequency[Hz]')
ylabel('Amplitude spectrum |U(f)|')

%% Plot single-sided amplitude spectrum of rot
figure
subplot(2,1,1)
plot(t,rot)
y_lim=ylim();


rectangle('position',[t_min, y_lim(1), max(t)-t_min, y_lim(2)-y_lim(1)],...
    'facecolor',Col.grey)
hold on
% Plot arrow of fft window
y_pos_of_arrow=(y_lim(2)-y_lim(1))/2+y_lim(1);
plot([t_min, max(t)],[y_pos_of_arrow, y_pos_of_arrow], 'k', 'LineWidth', 1.5);
plot(t_min, y_pos_of_arrow, 'k<')
plot(max(t), y_pos_of_arrow, 'k>')
plot(t,rot, 'LineWidth', 2,  'Color', Col.navy_blue)
title('Instantinious vorticity')
xlabel('time')
ylabel('vorticity')

subplot(2,1,2)
plot(f,Yrot_s1, 'LineWidth', 2)
xlim([2,6]);
%ylim([0,0.2]);
title('Instantinious vorticity in the frequency domain')

[ampl,ind]=findpeaks(Yrot_s1);
[ampl,iind]=sort(ampl);
hold on
f1=f(ind(iind(end)));
f2=f(ind(iind(end-1)));
y_lim=ylim();
plot([f1, f1], y_lim)
plot([f2, f2], y_lim)
axis=gca;
x_tick=axis.XTick;
x_tick=sort([x_tick, f1, f2]);
axis.XTick=x_tick;
xlabel('frequency[Hz]')
ylabel('Amplitude spectrum |vorticity(f)|')


end
function sY1=create_one_sided_spectrum(sY)
L =length(sY);
sY1 = sY(1:L/2+1);
sY1(2:end-1) = 2*sY1(2:end-1);
end