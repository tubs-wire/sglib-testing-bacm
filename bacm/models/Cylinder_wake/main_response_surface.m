% Define folder where snapshots are saved
clear
clc
root_folder=define_root_folder();
Col=set_my_favorite_colors();
% diagonal of the cylinder
D=0.1;
% Viscosity
nu=0.0005;
ro=1;
u_inf=1;

%% Get points and solutions
p_int=6;
subfolder_names=...
    {'integration_points_on_small_domain',...
    'integration_points_on_medium_domain',...
    'integration_points_on_large_domain',...
    'integration_points_at natural frequency1',...
    'integration_points_at natural frequency2',...
    'integration_points_qmc',...
    'qmc_points_around_f1',...
    'A10Stf1_qmc'};

subfolder_name=subfolder_names(1,[2,4,5,7]);
save_points_flag=false;
extend_with_uncontrolled_flag=false;
[q_i, w_i, Q, lift_i, drag_i, rot_norm_i, t]=get_points_and_solutions(p_int,'load',...
    root_folder, subfolder_name, save_points_flag, extend_with_uncontrolled_flag);

% Undimensionalize the outputs:
scale_force=2/(ro*u_inf*D);
drag_i=drag_i*-scale_force;
lift_i=lift_i*scale_force;

%% Make surrogate model
if iscell(Q)
    Q=MySimParamSet();
    Aa=(min(q_i(1,:)));
    Ab=max(q_i(1,:));
    %Ab=30;
    Q.add(MySimParameter('A', UniformDistribution(Aa,Ab)));
    Stfa=(min(q_i(2,:)));
    %Stfa=1;
    %Stfb=4.0;
    Stfb=max(q_i(2,:));
    %Stfb=ceil(max(q_i(2,:)));
    Q.add(MySimParameter('f', UniformDistribution(Stfa, Stfb)));
    % filter points that are larger then the limits
    ind=(q_i(1,:)>Ab |q_i(2,:)>Stfb);
    q_i=q_i(:,~ind);
    lift_i=lift_i(:,~ind);
    drag_i=drag_i(:,~ind); 
    rot_norm_i=rot_norm_i(:,~ind);
    method='colloc';
elseif ~extend_with_uncontrolled_flag;
    method='project';
end
u_i=zeros(size(lift_i, 1), 3, size(lift_i, 2));
u_i(:,1,:)=lift_i;
u_i(:,2,:)=drag_i;
u_i(:,3, :)=rot_norm_i;
%u_i=[lift_i;drag_i;rot_norm_i];
n_q=size(q_i,2);

stable_ind=find(t>4);
mean_lift_i=mean(lift_i(stable_ind,:));
mean_drag_i=mean(drag_i(stable_ind,:));
mean_rot_i=mean(rot_norm_i(stable_ind,:));
max_drag_i=max(abs(drag_i(stable_ind,:)));

%% surrogate model for mean drag
p_gpc=min(floor(sqrt(n_q)-1), 12);
p=1:p_gpc;
K=20; %20-fold cross validation
n_s=0.9;
% cross validation for drag:
gPCE_options={'method', method};
gPCE_meandrag  =train_and_cross_val_gPCEs(Q, q_i, mean_drag_i, K, p, n_s, [], 'gPCE_options', gPCE_options);
p_gpc_drag=gPCE_meandrag.opt.order;
[V_d, d_i_alpha]=...
    generate_gPCE_surrogate(q_i, mean_drag_i, p_gpc_drag, Q,  w_i, 'method', method);

%% surrogate model for mean rot
gPCE_meanrot  =train_and_cross_val_gPCEs(Q, q_i, mean_rot_i, K, p, n_s, [], 'gPCE_options', gPCE_options);
p_gpc_rot=gPCE_meanrot.opt.order;
[V_r, r_i_alpha]=...
    generate_gPCE_surrogate(q_i, mean_rot_i, p_gpc_rot, Q, w_i, 'method', method);

gPCE  =train_and_cross_val_gPCEs(Q, q_i, u_i, K, p, n_s, [], 'gPCE_options', gPCE_options);

p_gpc=gPCE.opt.order;
[V_u, u_i_alpha, params2germs_func, germs2param_func]=...
    generate_gPCE_surrogate(q_i, u_i, p_gpc, Q, w_i, 'method', method);

%% surrogate model for max drag

gPCE_maxdrag  =train_and_cross_val_gPCEs(Q, q_i, max_drag_i, K, p, n_s, [], 'gPCE_options', gPCE_options);
p_gpc_maxdrag=gPCE_maxdrag.opt.order;
[V_dmax, dmax_i_alpha]=...
    generate_gPCE_surrogate(q_i, max_drag_i, p_gpc_rot, Q, w_i, 'method', method);




%% Combined gpc for mean drag, mean rot and max drag
dmeanmaxrot_i=[mean_drag_i;max_drag_i; mean_rot_i];

gPCEdmeanmaxrot  =train_and_cross_val_gPCEs(Q, q_i, dmeanmaxrot_i, K, p, n_s, [], 'gPCE_options', gPCE_options);

p_gpc=gPCEdmeanmaxrot.opt.order;
[V_dmeanmaxrot, dmeanmaxrot_i_alpha, params2germs_func, germs2param_func]=...
    generate_gPCE_surrogate(q_i, dmeanmaxrot_i, p_gpc, Q, w_i, 'method', method);

%% statistics

[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);
u_quant=gpc_quantiles(V_u, u_i_alpha);

%% Plot response surface
% mean drag
figure
myplot_response_surface(d_i_alpha, V_d, 'germ2param', @(xi)Q.germ2params(xi),...
    'name_of_response', '<C_D>', 'type', 'surf', 'name_of_RVs', Q.param_names)
hold on
scatter3(q_i(1,:), q_i(2,:), mean_drag_i, 'x')
% mean rot
figure
myplot_response_surface(r_i_alpha, V_r, 'germ2param', @(xi)Q.germ2params(xi),...
    'name_of_response', '<||rot(u||>', 'type', 'surf', 'name_of_RVs', Q.param_names)
hold on
scatter3(q_i(1,:), q_i(2,:), mean_rot_i, 'x')

% max drag
figure
myplot_response_surface(dmax_i_alpha, V_dmax, 'germ2param', @(xi)Q.germ2params(xi),...
    'name_of_response', 'C_{D max}', 'type', 'surf', 'name_of_RVs', Q.param_names)
hold on
scatter3(q_i(1,:), q_i(2,:), -max_drag_i*2/0.1, 'x')

%% restructure data and plot mean and quantiles
u_mean=reshape(u_mean,[], 3);
u_var=reshape(u_var,[], 3);
u_quant_l=reshape(u_quant(1,:), [], 3);
u_quant_u=reshape(u_quant(2,:), [], 3);
figure
plot_u_mean_quant(t', u_mean, u_quant_l, u_quant_u, ...
    'ylabels', {'C_L', 'C_D', '||rot(u)||'})
% index of noncontrolled_flow
ind=find(q_i(1,:)==0);
% Plot the uncontrolled behanior
subplot(3,1,1)
hold on
plot(t, lift_i(:,ind(1)), 'r', 'LineWidth',2)
subplot(3,1,2)
hold on
plot(t, drag_i(:,ind(1)), 'r', 'LineWidth',2)
subplot(3,1,3)
hold on
plot(t, rot_norm_i(:,ind(1)), 'r', 'LineWidth',2)

%% do MCMC update
opt_drag=drag_i(1,1);
opt_max_drag=drag_i(1,1);
opt_rot=rot_norm_i(1,1);

Y_func = @(xi)gpc_evaluate(dmeanmaxrot_i_alpha, V_dmeanmaxrot, xi);

N=10000;
y_m=[opt_drag;opt_max_drag;opt_rot];
E=SimParamSet();
E.add(SimParameter('Emeandrag', NormalDistribution(0,0.2)))
E.add(SimParameter('Emaxdrag', NormalDistribution(0,0.3)))
E.add(SimParameter('Erot', NormalDistribution(0,25)))
% get distribution of the germ
syschars=V_dmeanmaxrot{1};
dist=cell(length(syschars),1);

for i=1:length(syschars)
        [~, dist{i}]=gpc_registry('get', syschars(1));       
        dist1=fix_bounds(dist{i}, -0.8, 0.8);
        dist{i}=dist1;
end
        prior_dist=dist;
        %prop_dist={NormalDistribution(0,0.5)};
        % MCMC update
        [theta_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,E);
       
        q_post = Q.germ2params(theta_samples_post);
        q_post_mean = mean(q_post, 2);
        q_post_var  = var(q_post, [], 2);
        q_post_peak=sample_density_peak(q_post);

plot_grouped_scatter(q_post,  'Labels', {'A', 'f'})
f_cm_prior=Q.sample(N*100);
%% Make a figure with peak, mean and prior, posterior pdfs
q_prior=Q.sample(N*10);
for i=1:Q.num_params
    figure
    [~, ~, h_post]=my_plot_density(q_post(i,:));
    hold on
    [~, ~, h_prior]=my_plot_density(q_prior(i,:));
    h_post.LineWidth=3;
    h_post.Color=Col.bordeau;
    h_prior.LineWidth=2;
    h_prior.Color=Col.cadet_blue;
    h_prior.LineStyle='--';
    y_lim=ylim();
    plot(q_post_mean(i)*ones(1,2), y_lim, 'LineWidth', 1.5)
    plot(q_post_peak(i)*ones(1,2), y_lim, '-.' ,'LineWidth', 1.5)
    ylabel('PDF')
    xlabel(Q.param_names{i})
    legend({'Prior', 'Posterior', 'Posteror mean', 'MAP'})
end

