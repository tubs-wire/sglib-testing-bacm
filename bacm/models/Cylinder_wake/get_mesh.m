function mesh=get_mesh(root_folder)
    mesh=load(strcat(root_folder, filesep, 'mesh.mat'));
end