function [Q,D,b] = ROM_coeffs(u, v, phi_j_k, Re, coord, pos, n_mod)

u_m=mean(u,2);
v_m=mean(v,2);

M=mass_matrix(pos,els);
K=stiffness_matrix(pos,els);

%% a_i
%-(psi_i,(u_m . nabla)u_m)

% -1/Re*(nabla Phi_i, nabla u_m)
a2_u=-1/Re*phi_u(:,1:n_mod)'*K*u_m;
a2_v=-1/Re*phi_v(:,1:n_mod)'*K*v_m;
%  -1/Re*(Psi_i, nabla u_m)at boundary


Q = cell(n,1);
parfor i = 1:n
    for j = 1:n
        for k = 1:n
            Q{i}(j,k)=sum(sum(dx.*u{i}.*(u{j}.*u x{k} + v {j}.*u y{k}) + ...
                
        dx.*v{i}.*(u{j}.*v x{k} + v {j}.*v y{k}) ...
            ));
        end
    end
end

D = zeros(n,n);
parfor i = 1:n
    for j = 1:n
        D(i,j)=sum(sum(nu*dx.*u{i}.*(u xx{j} + u yy{j}) + ...
            nu*dx.*v{i}.*(v xx{j} + v yy{j}) + ...
            ( dx.*u{i}.*(u o.*u x{j} + v o.*u y{j}) + ...
            dx.*v{i}.*(u o.*v x{j} + v o.*v y{j}) + ...
            dx.*u{i}.*(u{j}.*u o x + v{j}.*u o y) + ...
            dx.*v{i}.*(u{j}.*v o x + v{j}.*v o y) )));
    end
end
b = zeros(n,1);
parfor i=1:n
    b(i,1)=sum(sum(nu*dx.*u{i}.*(u o xx + u o yy) + ...
        nu*dx.*v{i}.*(v o xx + v o yy) + ...
        ( dx.*u{i}.*(u o.*u o x + v o.*u o y) + ...
        dx.*v{i}.*(u o.*v o x + v o.*v o y) )));
end
end