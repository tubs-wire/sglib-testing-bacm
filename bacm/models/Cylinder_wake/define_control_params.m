function [q_i, w, Q]=define_control_params(p_int, load_or_get, flag_save_results, root_folder, subfolder_name)

switch subfolder_name
    case 'integration_points_on_small_domain'
        method='gauss_points';
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,4)));
        Q.add(MySimParameter('Stf', UniformDistribution(1,8)));
    case 'integration_points_on_medium_domain'
        method='gauss_points';
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,30)));
        Q.add(MySimParameter('Stf', UniformDistribution(1,4)));
    case 'integration_points_on_large_domain'
        method='gauss_points';
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,300)));
        Q.add(MySimParameter('Stf', UniformDistribution(1,4)));
    case 'integration_points_at natural frequency1'
        method='gauss_points';
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,35)));
        Q.add(MySimParameter('Stf', UniformDistribution(1.5,3)));
    case 'integration_points_at natural frequency2'
        method='gauss_points';
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,35)));
        Q.add(MySimParameter('Stf', UniformDistribution(3.0,4.5)));
    case 'integration_points_qmc'
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(2,15)));
        Q.add(MySimParameter('Stf', UniformDistribution(0,4.5)));
        
        method='qmc';
        switch load_or_get
            case 'get'
                N=p_int^2;
            case 'load'
                p_int=5;
                N=p_int^2;
        end
    case 'qmc_points_around_f1'
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(0,30)));
        Q.add(MySimParameter('Stf', UniformDistribution(2,3)));
        
        method='qmc';
        switch load_or_get
            case 'get'
                N=p_int^2;
            case 'load'
                p_int=6;
                N=p_int^2;
        end
        
    case 'A10Stf1_qmc'
        method='qmc';
        switch load_or_get
            case 'get'
                N=p_int^2;
            case 'load'
                p_int=4;
                N=p_int^2;
        end
        Q=MySimParamSet();
        Q.add(MySimParameter('A', UniformDistribution(8,12)));
        Q.add(MySimParameter('Stf', UniformDistribution(0,1.5)));
end

switch method
    case 'gauss_points'
        [q_i, w]=Q.get_integration_points(p_int, 'grid', 'full_tensor');
    case 'qmc'
        switch load_or_get
            case 'get'
                q_i=Q.sample(N, 'mode', 'qmc');
            case 'load'
                load([root_folder, filesep, subfolder_name, filesep, strvarexpand('$subfolder_name$$p_int$'),'.mat'])
        end
        w=1/N*ones(N,1);
end
if flag_save_results
    act_path=pwd;
    cd(root_folder);
    mkdir(subfolder_name);
    save([root_folder, filesep, 'int_points', filesep, strvarexpand('$subfolder_name$$p_int$'),'.mat'], 'q_i')
    cd(act_path)
end
