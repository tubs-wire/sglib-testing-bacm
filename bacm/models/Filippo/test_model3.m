function test_model3

% Spatial model
Nx = 5;
[x, els] = create_mesh_1d(0, 1, Nx);

cov_func = @(x1, x2, l)(gaussian_covariance(x1, x2, l));


%%
Theta = SimParamSet();
Theta.add_parameter(SimParameter('l_G', UniformDistribution(0.1, 0.3)));
Theta.add_parameter(SimParameter('l_M', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('mu_M', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('var_M', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('l_B', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('mu_B', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('var_B', UniformDistribution(0.6, 1.3)));
Theta.set_fixed('l_B', 1);

theta_true = Theta.sample(1);
[mu_G_true, beta_G_true, Sigma_GG_true] = sample_and_compute_intermediate_stuff(theta_true, {x, cov_func});

g = sample_snowloads(mu_G_true, beta_G_true, Sigma_GG_true);


theta_true'
for theta = [theta_true, theta_true + [0.2;1;1;1;1;1;1], Theta.sample(1), theta_true]
    rand_seed(12345);
    N = 10;
    L_g = 0;
    for i=1:N
        [mu_G, beta_G, Sigma_GG] = sample_and_compute_intermediate_stuff(theta, {x, cov_func});
        
        gg = normal_invcdf(EV1_cdf((g-mu_G)./beta_G, 0, 1), 0, 1);
        L_g_mb = multi_normal_pdf(gg, 0, Sigma_GG);
        L_g = L_g + L_g_mb/N;
        if isnan(L_g); keyboard; end
    end
    strvarexpand('L_g=$L_g$');
end
    
%
%clf
%plot(x, G_sample, x, mu_G, x, sqrt(beta_G))
%legend({'Snowload', 'location', 'scale'})


function G_sample = sample_snowloads(mu_G, beta_G, Sigma_GG)
GG_sample = multi_normal_sample(1, 0, Sigma_GG);
GU_sample = normal_cdf(GG_sample, 0, 1);
G_sample = (EV1_invcdf(GU_sample, 0, 1).*beta_G) + mu_G;


function [mu_G, beta_G, Sigma_GG] = sample_and_compute_intermediate_stuff(theta, stuff)
[l_G, l_M, mu_M, var_M, l_B, mu_B, var_B]=deal_vector(theta);
l_B = l_M;

[x, cov_func]=stuff{:};

Sigma_M = var_M * covariance_matrix(x, @(x1,x2)(cov_func(x1,x2,l_M)));
mu_G = exp(multi_normal_sample(1, mu_M, Sigma_M));

% Model for the random field B (logNormal) (repr. beta in the Gumbel)
Sigma_B = var_B * covariance_matrix(x, @(x1,x2)(cov_func(x1,x2,l_B)));
beta_G = exp(multi_normal_sample(1, mu_B, Sigma_B));

Sigma_GG = covariance_matrix(x, @(x1,x2)(cov_func(x1,x2,l_G)));



function [varargout]=deal_vector(v)
ca = num2cell(v(:));
varargout=ca;
