clear

multiplot_init(4, 2, 'ordering', 'row');


d = 1;

N = 2;
x = linspace(0, 1, N);
c = @(x1, x2, l)(gaussian_covariance(x1, x2, l));
mu = sin(x');

l = 6;

SigmaZ = covariance_matrix(x, @(x1, x2)(c(x1,x2,l)));
SigmaU = [1 0; 0 4]/3;

M = 1000;

Z = zeros(2,0);
U = zeros(2,0);
for j=1:M
    zj = multi_normal_sample(1, mu, SigmaZ);
    uj = multi_normal_sample(10, zj, diag(zj)*SigmaU*diag(zj));
    Z = [Z, zj];
    U = [U, uj];
end

multiplot
%plot(Z(1,:), Z(2,:), '.', 'MarkerSize', 0.001);
scatter(Z(1,:), Z(2,:), 0.1);
xlim([-5,5]);ylim([-5,5]);
axis equal

multiplot
scatter(U(1,:), U(2,:), 0.1);
xlim([-5,5]);ylim([-5,5]);
axis equal


[Zgrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));
Zpdf = multi_normal_pdf(Zgrid, mu, SigmaZ)';
multiplot
plot_field(Zgrid, els, Zpdf, 'show_mesh', false)
xlim([-5,5]);ylim([-5,5]);
axis equal



%%
[Ugrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));
Updf = zeros(size(Zpdf));


dz = max(diff(Zgrid'));


for j=1:size(Zgrid,2)
    zj = Zgrid(:,j);
    fzj = Zpdf(j);
    fuz = multi_normal_pdf(Ugrid, zj, diag(zj)*SigmaU*diag(zj))';
    Updf = Updf + fuz * fzj * prod(dz);
end

multiplot
plot_field(Ugrid, els, Updf, 'show_mesh', false)
xlim([-5,5]);ylim([-5,5]);


%%
[Ugrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));
for M=[30, 1000]
    Updf = zeros(size(Zpdf));
    dz = 1/M;
    Zsamples = multi_normal_sample(M, mu, SigmaZ);
    
    for j=1:size(Zsamples,2)
        zj = Zsamples(:,j);
        fuz = multi_normal_pdf(Ugrid, zj, diag(zj)*SigmaU*diag(zj))';
        Updf = Updf + fuz * prod(dz);
    end
    
    multiplot
    plot(Zsamples(1,:), Zsamples(2,:), '.');
    xlim([-5,5]);ylim([-5,5]);

    multiplot
    plot_field(Ugrid, els, Updf, 'show_mesh', false)
    xlim([-5,5]);ylim([-5,5]);
end



%multiplot_adjust_range('separate', 'cols', 'axes', 'xy');

