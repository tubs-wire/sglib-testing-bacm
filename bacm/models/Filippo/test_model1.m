clear

multiplot_init(4, 2, 'ordering', 'row');

% 1d problem
d = 1;

% Define Z random field with trigonometric mean, and gaussian correlated
% field with correlation length 6
N = 2;
x = linspace(0, 1, N);
c = @(x1, x2, l)(gaussian_covariance(x1, x2, l));
mu = sin(x');

l = 6;

SigmaZ = covariance_matrix(x, @(x1, x2)(c(x1,x2,l)));

% Define correlation of u
SigmaU = [1 0; 0 4]/3;

M = 1000;

Z = zeros(2,0);
U = zeros(2,0);
for j=1:M
    % sample from Z
    zj = multi_normal_sample(1, mu, SigmaZ);
    % sample from u using the mean zj sample
    uj = multi_normal_sample(10, zj, SigmaU);
    Z = [Z, zj];
    U = [U, uj];
end

multiplot
% plot the samples of z
plot(Z(1,:), Z(2,:), '.');
xlim([-5,5]);ylim([-5,5]);
axis equal

multiplot
% plot the samples of u
plot(U(1,:), U(2,:), '.');
xlim([-5,5]);ylim([-5,5]);
axis equal

% Make a mesh for pdf plot of z and plot the pdf at the meshgrid points
[Zgrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));
Zpdf = multi_normal_pdf(Zgrid, mu, SigmaZ)';
multiplot
plot_field(Zgrid, els, Zpdf, 'show_mesh', false)
xlim([-5,5]);ylim([-5,5]);
axis equal


%Make a mesh for the pdf plot of u 
[Ugrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));
% Initiate matrix with probabilities at Ugrid
Updf = zeros(size(Zpdf));
% dz elementsize from the mesh [dz1, dz2]
dz = max(diff(Zgrid'));

% compute conditional probability \int f(u|z)f(z|l) dz
for j=1:size(Zgrid,2)
    zj = Zgrid(:,j);
    %f(z|l)
    fzj = Zpdf(j);
    %f(u|z)
    fuz = multi_normal_pdf(Ugrid, zj, SigmaU)';
    % sum up with numerical integration
    Updf = Updf + fuz * fzj * prod(dz);
end
% plot pdf of u
multiplot
plot_field(Ugrid, els, Updf, 'show_mesh', false)
xlim([-5,5]);ylim([-5,5]);


%% Compute the same probabilities differently without computing the pdf of fzj
% Instead of computing f(z|l)dz the samples are used
%mesh for u
[Ugrid,els]=create_mesh_from_grid(linspace(-5,5,30), linspace(-5,5,30));

% Try with different sample numbers 3 and 100
for M=[3, 100]
    Updf = zeros(size(Zpdf));
    dz = 1/M;
    % M_i samples of Z
    Zsamples = multi_normal_sample(M, mu, SigmaZ);
    % Compute probability f(u|l) by integration
    for j=1:size(Zsamples,2) %loop over z samples
        zj = Zsamples(:,j);
        fuz = multi_normal_pdf(Ugrid, zj, SigmaU)';
        Updf = Updf + fuz * prod(dz);
    end
% or the same without loop:
 %Updf1=sum(multi_normal_tensor_pdf(Ugrid, reshape(Zsamples,N,[],M), SigmaU),2)*dz;
    % plot z samples
    multiplot
    plot(Zsamples(1,:), Zsamples(2,:), '.');
    % plot pdf of u
    xlim([-5,5]);ylim([-5,5]);
    multiplot
    plot_field(Ugrid, els, Updf, 'show_mesh', false)
    xlim([-5,5]);ylim([-5,5]);
end

%multiplot_adjust_range('separate', 'cols', 'axes', 'xy');

%% Or just sample from u
U_samples=multi_normal_tensor_sample(10, Zsamples, repmat(SigmaZ, 1,1,M, M));

