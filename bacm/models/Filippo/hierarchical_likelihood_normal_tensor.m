function L_g=hierarchical_likelihood_normal_tensor(g ,theta, pos, N, cov_func, varargin)
% Compute Likelihood given measurements (g) at each point of the spatial
% model defined by pos and els. N is the number of samples to compute the
% integral
options=varargin2options(varargin);
[log_prob, options]=get_option(options, 'log_prob', false);
check_unsupported_options(options, mfilename);


mu_G = sample_and_compute_interm_normal_stuff_tensor(theta, pos, cov_func, N);
C_g=repmat(eye(max(size(pos))), 1, 1, size(theta,2), size(theta,2));
L_g = multi_normal_tensor_pdf(g, mu_G, C_g./100, 'log_flag', log_prob)/N;

if log_prob
    L_g=log(sum(exp(L_g),1));
else
    L_g=(sum(L_g,1));
end

