clear all

%% Definition of the Spatial model
Nx = 200;
[pos, els] = create_mesh_1d(0, 1, Nx);

cov_func = @(x, l, sigma)(gaussian_covariance(x1, x2, l));


%% Add prior knoweldge about hyperparameters
Theta = SimParamSet();
% HyperParameters for the location (Gumbel) Random Field 
Theta.add_parameter(SimParameter('l_M', UniformDistribution(0.6, 1.3)));
Theta.add_parameter(SimParameter('mu_M', UniformDistribution(0, 0.5)));
Theta.add_parameter(SimParameter('var_M', UniformDistribution(0.1, 0.3)));

theta_true = Theta.sample(1);
mu_G_true= sample_and_compute_intermediate_normal_stuff(theta_true, pos, cov_func);

g = multi_normal_sample(1,mu_G_true, eye(Nx));

%Definition of the likelihood
N_int=30;
parpool();

xlikelihood=@(theta)(hierarchical_likelihood_normal_par(g,theta, pos, N_int,cov_func));
% xlikelihood=@(theta)(hierarchical_likelihood_nointegration(g,theta, pos, els,cov_func));
% xlikelihood=@(theta)(hierarchical_likelihood_nocorrelation(g,theta, pos, els, N_integral,cov_func));
%Trials
a=Theta.sample(1);
L_1=xlikelihood(a)
p_1=Theta.pdf(a)


N=1000;
T=100;
[hyperparameters_post,acc_rate]=bayes_mcmc_3(xlikelihood,Theta,N,[],[],'parallel',false,'plot',false,'T',T);
matlabpool('close');

figure
a=Theta.sample(N);
% plot_grouped_scatter_2({a,hyperparameters_post},theta_true,'Labels',{'l_G','l_M','\mu_M','\sigma_M','\mu_\beta','\sigma_\beta'},'Legends',{'prior','posterior','truth'});
plot_grouped_scatter_2({a,hyperparameters_post},theta_true,'Labels',{'l_G','l_M','\mu_M','\sigma_M'},'Legends',{'prior','posterior','truth'});

%% Plot truth and posterior of the field for alpha parameter
%Truth
cov_func = @(x1, x2, l,sigma)(gaussian_covariance(x1, x2, l,sigma));
G = mass_matrix(pos, els);
G = full(G);
m=5;
[g_i_alpha_true, I_g_true, C_true, sigma__true] = expand_gaussian_field_pce(@(x1,x2)(cov_func(x1, x2, theta_true(2,1),theta_true(4,1))), pos, G, m );
g_i_alpha_true(:,1)=[];
I_g_true(1,:)=[];
mu_i_true=pce_field_realization(g_i_alpha_true, I_g_true);
mu_i_m_true=theta_true(3,1)+ mu_i_true;

%Posterior
hyperparameters_post_mean=mean(hyperparameters_post,2);
[g_i_alpha_post, I_g_post, C_post, sigma__post] = expand_gaussian_field_pce(@(x1,x2)(cov_func(x1, x2, hyperparameters_post_mean(2,1),hyperparameters_post_mean(4,1))), pos, G, m );
g_i_alpha_post(:,1)=[];
I_g_post(1,:)=[];
mu_i_post=pce_field_realization(g_i_alpha_post, I_g_post);
mu_i_m_post=hyperparameters_post_mean(3,1)+ mu_i_post;

figure
% p2=trisurf(els',pos(1,:),pos(2,:),mu_i_m_true);
% set(p2,'Facecolor','b')
plot(pos,mu_i_m_true,'-b');
hold on
% p1=trisurf(els',pos(1,:),pos(2,:),mu_i_m_post);
% set(p1,'Facecolor','r')
plot(pos,mu_i_m_post,'-r');
legend('\alpha true','\alpha posterior')
xlabel('x [km]')
% ylabel('y [km]')
ylabel('Q [kN/m^2]')
% axis([0,100,0,100,0,1])
title('Random field for location parameter \alpha')




