clear all
clf

%% Definition of the Spatial model

Nx = 100;
[pos, els] = create_mesh_1d(0, 1, Nx);
cov_func = @(x, l, sigma)(get_gaussian_covariance_tensor(x, l, sigma));

% number of measurements used
Nmx=20;
ind=sort(randperm(Nx, Nmx));

%% Add prior knoweldge about hyperparameters

Theta = SimParamSet();
Theta.add_parameter(SimParameter('l_M', UniformDistribution(0.8, 0.9)));
%Theta.add_parameter(SimParameter('mu_M', UniformDistribution(0, 0.5)));
%Theta.add_parameter(SimParameter('var_M', UniformDistribution(2, 2.6)));

%Theta.set_to_mean('mu_M')
%Theta.set_to_mean('var_M')
var_M=1;
mu_M=0;
var_G=0.1;

%% Set the true
l_true = 1.5/2;
theta_true = [l_true];

Sigma_M = cov_func(pos, l_true, sqrt(var_M));
mu_G_true = (multi_normal_sample(1, mu_M, Sigma_M));
g = multi_normal_sample(1, mu_G_true, eye(Nx)*sqrt(var_G));

%% Compute likelihood
n_theta=100;
l=linspace(0.5, 2, n_theta);

N=100000;

Sigma_M = cov_func(pos, l, sqrt(var_M));
mu_G = (multi_normal_tensor_sample(N, mu_M, Sigma_M));
L_g = multi_normal_tensor_pdf(g, mu_G, eye(Nx)*sqrt(var_G));
L_g=sum(L_g,1);
figure
plot(l, L_g)
hold on
plot([l_true, l_true], ylim)

% or differently (without the broadcasting tensor complicated stuff)
L_g=zeros(n_theta, 1);
for i=1:n_theta;
    Sigma_M = cov_func(pos, l(i), sqrt(var_M));
    mu_G = (multi_normal_tensor_sample(N, mu_M, Sigma_M));
    L_g_i = multi_normal_pdf(g, mu_G, eye(Nx)*sqrt(var_G));
    L_g(i)=sum(L_g_i);
end
plot(l, L_g)
plot([l_true, l_true], ylim)

