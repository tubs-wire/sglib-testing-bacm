%% Get gPCE model and prior

% gPCE of the logfield and solution, mesh information, and mapt to get the f_cm field
[w_i_alpha, V_w, logfield2f_cm, f_cm2logfield, pos, els, ind, u_i_alpha, V_u, r_i_k]...
    = test_mesh_generation_v1();
% Statistics of the displacement field
[u_mean, u_var] = gpc_moments(u_i_alpha, V_u);
u_quant=(gpc_quantiles(V_u, u_i_alpha, 'quantiles', [0.023, 0.977]))';
% Check sensitivities (how the variance of the solution is influenced by
% the different scaling of the eigenfunctions
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(u_i_alpha, V_u, 'max_index',1);
n_germ=gpcbasis_size(V_lf, 2);

%% Get the true reference displ (u)
% Specicify the fcm true
%F=load(fullfile(data_pathname,'true-field-ln-1.89-5.0.mat'));
%f_cm_true = F.u_true;
% Load the solution for fcm_true
    %U=load(fullfile(data_pathname,'true_displ_field.mat'));
    %u_true=U.Trueuzfieldln1(:,3);
    %logfcm_true = f_cm2logfield(f_cm_true);
% Instead I cheat,and create the measurement from the proxi (the sample
% from the germ is saved and here loaded from the data file)
    X=load(fullfile(data_pathname,'cheat_xi_true'));
    xi_true=X.xi_true;
    u_true = inv_response_map(gpc_evaluate(u_i_alpha, V_u, xi_true));
    lf_cm_true=gpc_evaluate(lf_i_alpha, V_lf, xi_true);
    f_cm_true=logfield2f_cm(lf_cm_true);
    %% Create synthetic data from the reference
% Get the true reference at the assimilation points
[y_true, ind_meas]=shuffle_select_sort(u_true, 1, n_meas);
y_true_maped=response_map(y_true);
y_i_alpha=u_i_alpha(ind_meas,:);
V_y=V_u;
% Measurement std
sigma_m=0.0005*abs(u_true)/max(abs(u_true));
sigma_meas = 0.0005*abs(y_true)/max(abs(y_true));
% make a separate simparamset for the measurement errors
E_meas=generate_stdrn_simparamset(sigma_meas);
y_m=y_true+E_meas.sample(1);
y_m=response_map(y_m);

%% Error model 
 %sigma_e=J_func(u_true).*sigma_m;    
 %sigma_eps=J_func(y_true).*sigma_meas;
 if exist('sigma_func', 'var')
     sigma_e  =sigma_func(u_true,sigma_m)*1.2;
     sigma_eps=sigma_func(y_true,sigma_meas)*1.2;
 else
     sigma_e  =J_func(u_true).*sigma_m;
     sigma_eps=J_func(y_true).*sigma_meas;
 end
 E=generate_stdrn_simparamset(sigma_eps);
 % PCE of the error
        [e_i_alpha, V_e] = E.gpc_expand();