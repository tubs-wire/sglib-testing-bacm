function L_g=hierarchical_likelihood_normal_par(g ,theta, pos, N, cov_func)
% Compute Likelihood given measurements (g) at each point of the spatial
% model defined by pos and els. N is the number of samples to compute the
% integral

L_g = zeros(N,1);
parfor i=1:N
    mu_G = sample_and_compute_intermediate_normal_stuff(theta, pos, cov_func);
    L_g(i,1) = multi_normal_pdf(g, mu_G, eye(max(size(pos))))/N;
    if isnan(L_g(i,1)); keyboard; end
end
L_g=sum(L_g);
end

