function y=EV1_cdf( x, location, scale )
% EV1_CDF Probability distribution function of the Extreme Value Distribution Type1.
%   Y=EV1_CDF( X, location, scale ) computes the cdf for the Extreme Value Distribution Type1 dist. for
%   all values in X, which may be a vector. 
%   
% Example 
%   x=linspace(-1,5);
%   f=EV1_pdf(x,2,.5);
%   F=EV1_cdf(x,2,.5);
%   plot(x,f,x(2:end)-diff(x(1:2)/2),diff(F)/(x(2)-x(1)))
%

%   
%   Copyright 2016, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

z=(x-location)/scale;
y=exp(-exp(-z));
