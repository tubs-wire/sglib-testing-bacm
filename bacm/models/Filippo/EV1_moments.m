function [mean,var,skew,kurt]=EV1_moments(location,scale)
% EV1_MOMENTS Compute moments of the EV1 distribution.
%   [MEAN,VAR,SKEW,KURT]=EV1_MOMENTS( MU, SIGMA ) computes the moments of the
%   EV1 distribution. This is of course pretty trivial but for
%   completenesses sake...
%
% 

%   
%   Copyright 2016, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



mean=location+scale*0.57721;
var=pi^2*scale^2/6;
skew=1.14;
kurt=3+12/5;

