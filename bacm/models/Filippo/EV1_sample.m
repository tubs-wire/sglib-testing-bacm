function xi=EV1_sample(n, location, scale)
% EV1_SAMPLE Draws random samples from the EV1 distribution.
%   XI=EV1_SAMPLE(N, MU, SIGMA) draws N random samples from the EV1
%   distribution with parameters loacation  and scale. If N is a scalar value
%   XI is a column vector of random samples of size [N,1]. If N is a vector
%   XI is a matrix (or tensor) of size [N(1), N(2), ...].
%
%   Example 

%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

if isscalar(n)==1
    % Generate uniform random values, and apply the extreme value inverse CDF.
    xi = -log( -log(rand(n,1)) ) .* scale + location;
else
    xi = -log( -log(rand(n)) ) .* scale + location;
end

