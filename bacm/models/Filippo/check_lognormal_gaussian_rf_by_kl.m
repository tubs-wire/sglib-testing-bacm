%% This is a trial to find out covariance of the gaussian RVs from the Covariance of the lognormal one

%% Represent a Gaussian random field
clear
clf
mean_wanted=[1;1];

mu = log((mean_f^2)/sqrt(var_f+mean_f^2));

Cov_wanted=[1, 0.5
  0.5, 1];

vars_wanted=diag(Cov_wanted);

sigmas_x = sqrt(log(vars_wanted./(mean_wanted.^2)+1));


Cov_x=2/(sum(sigmas_x))*log(Cov_wanted+exp(0.5*(sum(sigmas_x))))-1;
% standard deviations of each variable
S = diag(sqrt(diag(Cov_x)));
SInv = inv(S);
% Correlation matrix
Cor_x = SInv * Cov_x * SInv;
L = chol(Cor_x, 'lower');
 

% rho=log(0.5+exp(1))-1;
% C=[1, rho
%   rho, 1];


snor_samples=randn(2,1000000);
new_samples=S*L*snor_samples;
new_C=cov(new_samples');

logn_samples=exp(new_samples);
C_logn=cov(logn_samples');
% Setup the domain
% a = 0;
% b = 1;
% [x,els,bnd]=create_mesh_1d(a, b, 2);


% N = size(x, 2);

% Compute the KL expansion


G = mass_matrix(x, els);
[r_i_k, sigma_k] = kl_solve_evp(C, eye(2), 2);

% Setup the multiindex for m = L Gaussian random variables
m = L;
I = multiindex(m, 1);

% The corresponding coefficients have 
k = 1:m;
theta_k_alpha = zeros(L,m+1);
theta_k_alpha(k,k+1) = eye(m);

% Convert to compact form and include mean
% [u2_i_k, theta2_k_alpha] = kl_pce_to_compact_form(sin(pi*x'), r_i_k, sigma_k, theta_k_alpha);

% The conversion made explicit
% a) Multiply in the sigmas 
u_i_k = binfun(@times, r_i_k, sigma_k);

% b) Put the mean inside
u_i_k = [sin(pi*x'), u_i_k];
theta_k_alpha = [1, zeros(1,m); theta_k_alpha];


% Generate some normally distributed random numbers for plotting some
% realisations of the random field
xi = randn(m, 40);
clf
plot(x, kl_pce_field_realization(u_i_k, theta_k_alpha, I, xi))

% Compute the mean and standard deviation of the KL and add it to the plot
[mu_r, sigma_r] = kl_pce_moments(u_i_k, theta_k_alpha, I);
hold all;
plot(x, mu_r, 'b', 'LineWidth', 2);
plot(x, [mu_r + sigma_r, mu_r - sigma_r], 'b-.', 'LineWidth', 2);
hold off;

%plot_kl_pce_realizations_1d( x, u_i_k, theta_k_alpha, I, 'colormap', 'cool', 'realizations', 100);
