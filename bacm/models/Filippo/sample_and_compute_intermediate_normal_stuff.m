function mu_G = sample_and_compute_intermediate_normal_stuff(theta, pos, cov_func)

% [l_G, l_M, mu_M, var_M, mu_B, var_B]=deal_vector(theta);
[l_M, mu_M, var_M]=deal_vector(theta);

% Model for the random field M (logNormal) (repr. mu in the Gumbel)
Sigma_M = cov_func(pos, l_M, sqrt(var_M));
%Sigma_M = var_M * covariance_matrix(pos, @(x1,x2)(cov_func(x1,x2,l_M)));
mu_G = (multi_normal_tensor_sample(1, mu_M, Sigma_M));


function [varargout]=deal_vector(v)
ca=mat2cell(v, ones(size(v,1), 1), size(v,2));
%ca = num2cell(v(:));
varargout=ca;
