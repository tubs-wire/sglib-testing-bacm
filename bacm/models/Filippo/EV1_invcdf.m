function x=EV1_invcdf(y, location, scale)
% EV1_INVCDF Inverse CDF of the EV1 distribution.
%   X=EV1_INVCDF(Y, LOCATION, SCALE) computes the inverse cumulative distribution
%   function of the EV1 distribution with parameters LOCATION and SCALE for the
%   values in Y, which should all be in [0,1]. This function can be used to
%   transform [0,1] uniformly distributed random numbers into EV1
%   distributed random numbers. 
%
% Example
%   N=10000;
%   y=rand(N, 1);
%   x=EV1_invcdf(y, 2, 4);
%   hist(x, 30);
%


%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

x=-log(-log(y))*scale+location;
