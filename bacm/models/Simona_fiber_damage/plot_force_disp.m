
file_name = 'Prss-2-damage';
save_pathname = '/home/noefried/FEAP/ver84/XFEM-element/input-files';
[R  u  t]= import_FEAP_sum_and_disp(file_name, save_pathname);
plot(u, -R)
hold on

file_name = 'Prss-0-damage';
[R  u  t]= import_FEAP_sum_and_disp(file_name, save_pathname);
plot(u, -R)

title('Force-displacement diagram')
xlabel('u')
ylabel('Force')

legend('with fiber', 'without fiber')

