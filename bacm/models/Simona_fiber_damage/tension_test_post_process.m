Colors=set_my_favorite_colors();

% load variable t
load('t.mat');
n_t = length(t);
% load variable u
load('u.mat');
% load the indices corresponding to failed simulations
load('not_converging_indices.mat')

%% Define MC details
NN = 100; % number of simulations per stored data
N =900; % total number of simulations

%% Initiate solution vectors
R = zeros(n_t, N);
Ub = zeros(n_t, 9, N);
S = zeros(n_t, 7, N);
E = zeros(n_t, 8, N);

%% Put together solution files

for n_i = 1:ceil(N/NN)
    load(strvarexpand('Sol$n_i$.mat'));
    ind_i = binfun(@plus, 1:NN , (n_i-1)*NN);
    R(:, ind_i) = Sol.R;
    Ub(:,:,ind_i) = Sol.Ub;
    S(:,:,ind_i) =Sol.S;
    E(:,:,ind_i) = Sol.E;
end

%% Ignore not running simulations
% indices of running simulations
g_i = setdiff(1:N, unique(NaN_i));
R = R(:, g_i);
Ub = Ub(:,:, g_i);
S = S(:,:, g_i);
E = E(:,:, g_i);

N = length(g_i);

%% Plot global behavior
% Plot force displacement diagram
f = figure;

R_mean = mean(R,2);
R_quant = quantile(R', [0.025, 0.975]);

hold on
% plot confidence region
h1=plot_u_mean_quant(u, [], -R_quant(1,:)', -R_quant(2,:)', ...
    'fill_color', Colors.metal_grey, 'line_width', 3,...
    'ylabels', {'R: force'}, 'xlabel', 'u: displacement');
% plot some realizations
h2=plot(u, -R(:,1:20), 'LineWidth', 0.5, 'Color', Colors.grey);
% plot mean
h3 = plot(u, -R_mean, 'LineWidth', 3, 'Color', 'k');
legend([h1,h2(1), h3], {'95 % confidence region',  'some relizations', 'mean'})
% save figure
saveas(f, 'Figures/Force-displacement-diagram', 'fig')
saveas(f, 'Figures/Force-displacement-diagram', 'eps')
saveas(f, 'Figures/Force-displacement-diagram', 'pdf')


%% Plot bond slip displacement

Ub_mean = mean(Ub,3);
Ub_quant = reshape((quantile( (reshape(Ub, [n_t*9,N]))',[0.025, 0.975]))', [n_t, 9, 2]);
for i = 1: 9
    if i== 1  || i==9
        clf
        hold on
        % plot mean and quantile
        h1=plot_u_mean_quant(t, [], squeeze(Ub_quant(:,i,1)), squeeze(Ub_quant(:,i,2)), ...
            'fill_color', Colors.metal_grey, 'line_width', 3,...
            'ylabels', {'du: bond-slip displacement'}, 'xlabel', 't: pseudo-time');
        % plot some realizations
        h2 = plot(t, squeeze(Ub(:,i,1:20)), 'LineWidth', 1, 'Color', Colors.grey );
        % plot mean
        h3 = plot(t, squeeze(Ub_mean(:,i)), 'LineWidth', 3, 'Color', 'k');
        % legend
        legend([h1,h2(1), h3], {'95 % confidence region',  'some relizations', 'mean'})
        if i == 1
            saveas(f, 'Figures/Bond-slip-displacement-left', 'fig')
            saveas(f, 'Figures/Bond-slip-displacement-left', 'eps')
            saveas(f, 'Figures/Bond-slip-displacement-left', 'pdf')
            
        elseif i==9
            saveas(f, 'Figures/Bond-slip-displacement-right', 'fig')
            saveas(f, 'Figures/Bond-slip-displacement-right', 'eps')
            saveas(f, 'Figures/Bond-slip-displacement-right', 'pdf')
        end
    end
end

%% Plot time-bond-slip stress diagram

S_mean = mean(S,3);
S_quant = reshape((quantile( (reshape(S, [n_t*7,N]))',[0.025, 0.975]))', [n_t, 7, 2]);
for i = 1: 7
    if i== 1  || i==7
        clf
        hold on
        % plot mean and quantile
        h1=plot_u_mean_quant(t, [], squeeze(S_quant(:,i,1)), squeeze(S_quant(:,i,2)), ...
            'fill_color', Colors.metal_grey, 'line_width', 3,...
            'ylabels', {'\sigma_{bs}: bond-slip stress'}, 'xlabel', 't: pseudo-time');
        % plot some realizations
        h2 = plot(t, squeeze(S(:,i,1:20)), 'LineWidth', 1, 'Color', Colors.grey );
        % plot mean
        h3 = plot(t, squeeze(S_mean(:,i)), 'LineWidth', 3, 'Color', 'k');
        % legend
        legend([h1,h2(1), h3], {'95 % confidence region',  'some relizations', 'mean'})
        if i == 1
            saveas(f, 'Figures/Bond-slip-stress-left', 'fig')
            saveas(f, 'Figures/Bond-slip-stress-left', 'eps')
            saveas(f, 'Figures/Bond-slip-stress-left', 'pdf')
            
        elseif i==7
            saveas(f, 'Figures/Bond-slip-stress-right', 'fig')
            saveas(f, 'Figures/Bond-slip-stress-right', 'eps')
            saveas(f, 'Figures/Bond-slip-stress-right', 'pdf')
        end
    end
end
%% Plot time-bond-slip strain diagram
E_mean = mean(E,3);
E_quant = reshape((quantile( (reshape(E, [n_t*8,N]))',[0.025, 0.975]))', [n_t, 8, 2]);
for i = 1: 8
    if i== 1  || i==8
        clf
        hold on
        % plot mean and quantile
        h1=plot_u_mean_quant(t, [], squeeze(E_quant(:,i,1)), squeeze(E_quant(:,i,2)), ...
            'fill_color', Colors.metal_grey, 'line_width', 3,...
            'ylabels', {'\tau: bond-slip strain'}, 'xlabel', 't: pseudo-time');
        % plot some realizations
        h2 = plot(t, squeeze(E(:,i,1:20)), 'LineWidth', 1, 'Color', Colors.grey );
        % plot mean
        h3 = plot(t, squeeze(E_mean(:,i)), 'LineWidth', 3, 'Color', 'k');
        % legend
        legend([h1,h2(1), h3], {'95 % confidence region',  'some relizations', 'mean'})
        if i == 1
            saveas(f, 'Figures/Bond-slip-strain-left', 'fig')
            saveas(f, 'Figures/Bond-slip-strain-left', 'eps')
            saveas(f, 'Figures/Bond-slip-strain-left', 'pdf')
            
        elseif i==8
            saveas(f, 'Figures/Bond-slip-strain-right', 'fig')
            saveas(f, 'Figures/Bond-slip-strain-right', 'eps')
            saveas(f, 'Figures/Bond-slip-strain-right', 'pdf')
        end
    end
end
%% Plot bond-slip diplacement along fiber

Ubs = squeeze(Ub(end,:,:));
Ubs_mean = mean(Ubs,2);
Ubs_quant = quantile(Ubs', [0.025, 0.975]);
clf
hold on
% plot confidence region
h1=plot_u_mean_quant(1:0.25:3, [], Ubs_quant(1,:)', Ubs_quant(2,:)', ...
    'fill_color', Colors.metal_grey, 'line_width', 3,...
    'ylabels', {'du: bond-slip displacement'}, 'xlabel', 'u: displacement');
% plot some realizations
h2=plot(1:0.25:3, Ubs(:,1:20), 'LineWidth', 0.5, 'Color', Colors.grey);
% plot mean
h3 = plot(1:0.25:3,Ubs_mean, 'LineWidth', 3, 'Color', 'k');
legend([h1,h2(1), h3], {'95 % confidence region',  'some relizations', 'mean'})
% save figure
saveas(f, 'Figures/Bond-slip-disp-x', 'fig')
saveas(f, 'Figures/Bond-slip-disp-x', 'eps')
saveas(f, 'Figures/Bond-slip-disp-x', 'pdf')



