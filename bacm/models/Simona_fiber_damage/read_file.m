function [T, S] = read_file(file_name)

fileID=fopen(file_name);

T = [];
S = [];
while ~feof(fileID)
    temp1 = fgetl(fileID);
    if isempty(temp1)
        break
    else
        temp1 = regexprep(temp1, '([0-9])([+-])([0-9])', '$1E$2$3');
        inputt = textscan(temp1, '%f');
        inputt = inputt{1};
        T = [T; inputt(1)];
        si = inputt(2:end);
        S = [S; si'];
    end
    
end
fclose(fileID);
end