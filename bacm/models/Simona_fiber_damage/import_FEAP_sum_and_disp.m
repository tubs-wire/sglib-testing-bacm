function [R  u  t]= import_FEAP_sum_and_disp(file_name, save_pathname)
%% Loading and Reading the GEPS3D simulation files
if nargin<2
    save_pathname = pwd;
end
pathname=pwd;
cd(save_pathname)
[T1, R] = read_file([file_name,'a.sum']);
[T2, u] = read_file([file_name,'a.dis']);
t = T1;

if all(T1==0)
    warning('T1 and T2 are not equal, T1 is given for the t')
end

cd(pathname)
end

