clear all
clf

%% Boundary values
min_x = 0;
max_x = 680;
edge_x = max_x - min_x;

min_y = 0;
max_y = 100;
edge_y = max_y - min_y;

incr = 10; % size of the element

%% Generate the original FE mesh
x = linspace(0, max_x, max_x/incr + 1);
y = linspace(0, max_y, max_y/incr + 1);
% [pos,els] = create_mesh_from_grid(x, y);
% axis equal
% plot_mesh(pos, els);

%% Extend the original FE mesh
[pos_ext, els_ext, pos, els, inner_ind] = create_mesh_from_grid_extend(x,y,1,1);
%pos_ext(:,inner_ind) == pos 
% x and y are coordinates from original mesh, 3rd entry is the number of
% squares for which the mesh should be extended, 4th entry is setting up
% the scaling
plot_mesh(pos, els, 'width', 2)

%% Generate new FE mesh needed for KLE
n_el = size(els_ext, 2);
pos_p = zeros(2, n_el);

for i= 1:n_el
    pos_p(:,i) = sum(pos_ext(:, els_ext(:, i)),2)/3;
end

% Coordinates of barycenters of triangles (from original mesh) which will be coordinates of a new mesh
x_p = pos_p(1,:);
y_p = pos_p(2,:);

% check indices corresponding to inner elements
el_inner_ind=sum(ismember(els_ext, inner_ind))==3;

% % get map from POS_P to ELS
el_not_sorted = els_ext(:, el_inner_ind); % origianal elements with extented node numbers
[~, el_not_sorted] =   ismember(el_not_sorted, inner_ind); % origianal elements with original node numbers

if el_not_sorted ~= els
 error('elements have to be sorted, EL_INNER_IND has to be redefined')
end

%hold on
%plot(x_p, y_p, 'x')

% Generating new FEs using Delaunay triangulation
%els_p = (delaunay(x_p,y_p))';
pos_new = [pos_p, pos];
els_new = delaunay(pos_new(1,:), pos_new(2, :));

%plot_mesh(pos_p, els_p);
plot_mesh(pos_new, els_new', 'color', 'r')
axis equal
% once the KLE is done, the values needed are X(EL_INNER_IND)
el_inner_ind = [el_inner_ind, false(1, size(pos,2))];
hold on
plot(pos_new(1, el_inner_ind), pos_new(2, el_inner_ind), 'bx')