function [j, n_j, r, r_e] = read_Lfile(file_name)

fileID=fopen(file_name);

j = [];
n_j = [];
r = [];
r_e = [];
tt{1} = [];

while ~(feof(fileID) || strcmp(tt{1}, repmat('-', 1, 86)))
    
    temp1 = fgetl(fileID);
    if ~isempty(temp1)
        inputtext = textscan(temp1, '%s');
        tt=[inputtext{:}];
    end
end

while ~feof(fileID)
    
        temp1 = fgetl(fileID);
        if isempty(temp1)
            break
        else
            stext =  textscan(temp1,'%s');
            if iscell(stext{1}) && strcmp(stext{1}{1}, '*ERROR*')
                break
            end
        end
        temp1 = regexprep(temp1, '([0-9])([+-])([0-9])', '$1E$2$3');
        inputtext = textscan(temp1, ['%d %d %d ', repmat('%f ',1, 6)]);
        j = [j; inputtext{1}(1)];
        n_j = [n_j; inputtext{2}];
        r = [r; inputtext{7}];
        r_e = [r_e; inputtext{9}];
    
end
fclose(fileID);
end