matfeap_folder = '/home/noefried/FEAP/matfeap-0.8';
feap_input_folder = '/home/noefried/FEAP/ver84-tea/xfem/input_files/';
this_folder = pwd;
addpath(this_folder);

%% Define stochastic parameters


%% Define FEAP parameters
param.n = 10;
param.a = 4.5;    %sigf
param.b = 1000;   %Kh
param.c = 10;     %sigu
param.d = 20;     %beta/sigu
param.e = 0.3;    %sigs/sigu (ratio shear/tension)   
%param.f = -250;   %Kbs
param.f = -100;   %Kbs
param.g = 6;      %tauy
param.h = 0 ;     %flag    
param.verbose = 1;

%% initiate matfeap
cd(matfeap_folder)
matfeap_init;
%% rund feap code
cd(feap_input_folder)
tic
p = feapstart('irss-2-damage-stoch', param);
feapquit(p);
toc

%% Read output
% Read in output files
file_name = 'Prss-2-damage-stoch';
[t, R] = read_file([file_name,'a.sum']); % time and force
[~, U] = read_file([file_name,'a.dis'], 10); %displacements
u = U(:,1); % controlled displacement at endpoint
Ub = U(:, 2:end); % bond slip nodal displacements
[~, SS] = read_file([file_name,'a.str'], 16); % stress and strain
S = SS(:, [1:3,5:8]); %bond-slip stresses
E = SS(:, 9:16); %bond-slip strains

% Plot force displacement diagram
subplot(5,1, 1)
plot(u, -R, 'LineWidth', 2)

% Plot bond slip displacement
subplot(5,1,2)
plot(t, Ub, 'LineWidth', 2)

% Plot time-bond-slip stress diagram
subplot(5,1, 3)
plot(t, S, 'LineWidth', 2)
% Plot time-bond-slip strain diagram
subplot(5,1, 4)
plot(t, E, 'LineWidth', 2)

%Plot bond-slip diplacement along fiber
subplot(5,1,5)
plot(1:0.25:3, Ub(end, :), 'LineWidth', 2)

%% Read output - stress and strain diagrams



