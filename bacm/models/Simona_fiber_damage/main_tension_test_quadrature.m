clear all

% save_folder_name = 'Results_quadrature';
% matfeap_folder = '/home/simdobri/FEAP/matfeap-0.8';
% feap_input_folder = '/home/simdobri/FEAP/ver84-tea/xfem/input-files';



if strcmp(computer, 'GLNXA64')
    save_folder_name = 'Results_quadrature';
    matfeap_folder = '/home/noemi/sglib/sglib-testing-bacm/matfeap-0.8';
    feap_input_folder = '/home/noemi/FEAP/ver84-tea/xfem/input_files/';
else
    save_folder_name = 'Results_quadrature';
    matfeap_folder = '/home/noefried/FEAP/matfeap-0.8';
    feap_input_folder = '/home/noefried/FEAP/ver84-tea/xfem/input_files/';
end
this_folder = fileparts(mfilename('fullpath'));
addpath(this_folder);
addpath(genpath(feap_input_folder))
% create directory for results
mkdir([feap_input_folder, filesep, save_folder_name]);

%% Define stochastic parameters
Q = SimParamSet();
Q.add(SimParameter('K', fix_bounds(BetaDistribution(3,3), 150, 1000)));
Q.add(SimParameter('K_bs', fix_bounds(BetaDistribution(3,3), -300, -100)));
Q.add(SimParameter('Beta', UniformDistribution(10,22)));

% sample with QMC
%q_i = Q.sample(N, 'mode', 'qmc');
p_int = 4;
[q_i, w_i, xi_i]=Q.get_integration_points(p_int, 'grid', 'full_tensor');
N = length(w_i);
%scatter3(q_i(1,:), q_i(2,:), q_i(3,:))
 
%% Define deterministic FEAP parameters
%param.n = 10;
param.a = 4.5;    %sigf
param.c = 10;     %sigu
param.e = 0.3;    %sigs/sigu (ratio shear/tension)
param.g = 6;      %tauy
param.h = 0 ;     %flag
param.verbose = 0;



%% Initiate memory for solution vectors
n_t = 501; % number of snapshots
NN = N; % number of subsamples

Sol = struct();
Sol.R = zeros(n_t, NN);
Sol.Ub = zeros(n_t, 9, NN);
Sol.S = zeros(n_t, 7, NN);
Sol.E = zeros(n_t, 8, NN);
Sol.n_j = zeros(n_t-1, NN);
Sol.r = zeros(n_t-1, NN);
Sol.r_e = zeros(n_t-1, NN);
NaN_i=[];
%% loop over sample points

for i = 1:N
    %% initiate matfeap
    cd(matfeap_folder)
    matfeap_init;
    cd(feap_input_folder)
    display(strvarexpand('$i$/$N$'))
    n_i = ceil(i/NN); %ceil(X) rounds each element of X to the nearest integer greater than or equal to that element
    j = rem(i, NN);
    if j == 0
        j = NN;
    end
    %% Define stochastic FEAP parameters
    
    param.b = q_i(1,i);   %Kh
    param.d = q_i(3,i);   %beta/sigu
    %param.f = -250;   %Kbs
    param.f = q_i(2,i);   %Kbs
    
    
    %% run feap code
    try
        tic
        p = feapstart('irss-2-damage-stoch', param);
        feapquit(p);
        toc
    catch ME
        display(strvarexpand('FEAP failed to converge with param $q(:,i)$'));
        NaN_i=[NaN_i,i];
        
    end
    
    
    %% Read output
    % Read in output files
    file_name = 'Prss-2-damage-stoch';
    [jj, n_j, r, r_e] = read_Lfile('Lrss-2-damage-stoch'); % convergence info
    
    [t, R] = read_file([file_name,'a.sum']); % time and force
    [~, U] = read_file([file_name,'a.dis']); %displacements
    [~, SS] = read_file([file_name,'a.str']); % stress and strain
    
    %jth step, n_j tangent stiffness computation, r: residual, r_e energy residual
    
    u = U(:,1); % controlled displacement at endpoint
    Ub = U(:, 2:end); % bond slip nodal displacements
    
    S = SS(:, [1:3,5:8]); %bond-slip stresses
    E = SS(:, 9:16); %bond-slip strains
    
    
    %% Store in solution vectors
    % if failed to converge put NaN values to not computed values
    if ~isempty(NaN_i) && NaN_i(end)== i
        t_ind = 1:(max(jj)+2);
        NaN_ind = setdiff(1:n_t, t_ind);
        Sol.R(NaN_ind,j) = NaN;
        Sol.Ub(NaN_ind,:,j) = NaN;
        Sol.S(NaN_ind,:,j) = NaN;
        Sol.E(NaN_ind,:,j) = NaN;
        Sol.n_j(NaN_ind(1:end-1),j) = NaN;
        Sol.r(NaN_ind(1:end-1),j) = NaN;
        Sol.r_e(NaN_ind(1:end-1), j) = NaN;
    % store computed values    
        Sol.R(t_ind,j) = R;
        Sol.Ub(t_ind,:,j) = Ub;
        Sol.S(t_ind,:,j) = S;
        Sol.E(t_ind,:,j) = E;
        Sol.n_j(t_ind(1:end-2),j) = n_j;
        Sol.r(t_ind(1:end-2),j) = r;
        Sol.r_e(t_ind(1:end-2), j) = r_e;
    else
        t_ind = 1:n_t;
        
        Sol.R(t_ind,j) = R;
        Sol.Ub(t_ind,:,j) = Ub;
        Sol.S(t_ind,:,j) = S;
        Sol.E(t_ind,:,j) = E;
        Sol.n_j(t_ind(1:end-1),j) = n_j;
        Sol.r(t_ind(1:end-1),j) = r;
        Sol.r_e(t_ind(1:end-1), j) = r_e;
    end
    
    
    %% Save results after every NNth sample and cancer solution vectors
    if rem(i,NN) == 0
        % Save result
        save([feap_input_folder, filesep, save_folder_name, strvarexpand('Sol$n_i$.mat')], 'Sol');
        save(([feap_input_folder, filesep, save_folder_name, 'not_converging_indices.mat']), 'NaN_i')
        if n_i==1
            save([feap_input_folder, filesep, save_folder_name, '/t.mat'], 't')
            save([feap_input_folder, filesep, save_folder_name, '/u.mat'], 'u')
            save([feap_input_folder, filesep, save_folder_name, '/q_i.mat'], 'q_i')
            save([feap_input_folder, filesep, save_folder_name, '/Q.mat'], 'Q')
            save([feap_input_folder, filesep, save_folder_name, '/N.mat'], 'N', 'NN')
        end
        % Zero out solution vectors
        Sol.R = zeros(n_t, NN);
        Sol.Ub = zeros(n_t, 9, NN);
        Sol.S = zeros(n_t, 7, NN);
        Sol.E = zeros(n_t, 8, NN);
        Sol.res = zeros(n_t, 3, NN);
        Sol.n_j = zeros(n_t-1, NN);
        Sol.r = zeros(n_t-1, NN);
        Sol.r_e = zeros(n_t-1, NN);
        
    end
end