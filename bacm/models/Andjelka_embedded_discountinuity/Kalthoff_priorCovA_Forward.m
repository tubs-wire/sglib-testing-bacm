function [v_alpha_th, V_th, maps, P, surr_model, z_m, E, true_vals] =Kalthoff_priorCovA_Forward()

%% Define prior random variables (P), and the underlying gaussian (theta) in a PCE form
[v_alpha_th, V_th, maps, P] = set_prior_distribution_and_maps();


%% Set chosen assimilation points and time steps
AssimPoint_Set = [2,4,6,8,10,13,15,17,19,21];
TimeStep_Set = [41,43,45,47,49];

%% The synthetic truth
[u_x_true, u_y_true, p_true] = get_synthetic_true(AssimPoint_Set, TimeStep_Set);
% put important values in a structure
true_vals.p_true = p_true;
true_vals.u_x_true = [u_x_true; u_y_true];


%% Generate the synthetic measurements (u_x)
% standard deviation of the measurements
sigma_em = abs(u_x_true)*0.15;
% Set of parameters with the measurement error
E_m_x=generate_stdrn_simparamset(sigma_em);
% The synthetic measurement
z_m_x=u_x_true+E_m_x.sample(1);

% standard deviation of the measurements
sigma_em_y = abs(u_y_true)*0.15;
% Set of parameters with the measurement error
E_m_y=generate_stdrn_simparamset(sigma_em_y);
% The synthetic measurement
z_m_y=u_y_true+E_m_y.sample(1);

z_m = [z_m_x; z_m_y];
% plot the assimilated displacements and the generated measurements
figure();
plot(u_x_true(:,1), 'LineWidth', 2)
hold on
plot(z_m_x(:,1), 'x', 'LineWidth', 2)
xlabel('Index of assimilation point k')
ylabel('Displacement u_{x} [m]')
legend('True', 'Synthetic measurement', 'Location','SouthEast')
%save_png(h, 'Measurements', 'figdir', 'figs', 'res', 600)

% plot the assimilated displacements and the generated measurements
figure();
plot(u_y_true(:,1), 'LineWidth', 2)
hold on
plot(z_m_y(:,1), 'x', 'LineWidth', 2)
xlabel('Index of assimilation point k')
ylabel('Displacement u_{y} [m]')
legend('True', 'Synthetic measurement', 'Location','SouthEast')
%save_png(h, 'Measurements', 'figdir', 'figs', 'res', 600)

% figure();
% for i=1:2
%     subplot(2,1,i);
%     plot(u_x_true_KF(:,i), 'LineWidth', 2)
%     %plot(z_m(:,i), 'x', 'LineWidth', 2)
%     xlabel('Index of assimilation point k')
%     ylabel('Displacement [m]')
%     legend('True', 'Synthetic measurement', 'Location','SouthEast')
% end


%% Define the error model which is used for the update
%sigma_e = 2.4*10^-4+ones(n_meas,1);
sigma_e = abs(z_m_x)*0.15;
E_x = generate_stdrn_simparamset(sigma_e);
%E = generate_stdrn_simparamset(sigma_em*1.25);

sigma_e_y = abs(z_m_y)*0.15;
E_y = generate_stdrn_simparamset(sigma_e_y);

E = combine_simparamsets(E_x, E_y);

%% The PCE surrogate model

p_gpc = 6;
surr_model = get_PCE_expansion_of_measurable(p_gpc, AssimPoint_Set, TimeStep_Set);

end
