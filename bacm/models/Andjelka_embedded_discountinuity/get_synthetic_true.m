function [u_x_true_KF, u_y_true_KF, p_true] = get_synthetic_true(AssimPoint_Set, TimeStep_Set)

[p_true, u_x_true, u_y_true, simprop_true] = Kalthoff_priorCovA_get_TrueModel_results();

% AssimPoint_Set = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
% % TimeStep_Set = [41,42,43,44,45,46,47];
% TimeStep_Set = [45];
n_AsP = length(AssimPoint_Set); % number of chosen assimilation points
n_TS = length(TimeStep_Set);    % number of chosen time steps

u_x_true_KF=zeros(n_AsP*n_TS,1);
u_y_true_KF=zeros(n_AsP*n_TS,1);
% u_x_true_KF=zeros(n_AsP,n_TS);
% u_y_true_KF=zeros(n_AsP,n_TS);

for j=1:n_AsP
    AsP_ID = AssimPoint_Set(1,j);
    for i=1:n_TS
        Time_ID = TimeStep_Set(1,i);
        u_x_true_KF((j-1)*n_TS+i,1) = u_x_true((AsP_ID-1)*55+Time_ID,1);
        u_y_true_KF((j-1)*n_TS+i,1) = u_y_true((AsP_ID-1)*55+Time_ID,1);
%         u_x_true_KF(j,i) = u_x_true((AsP_ID-1)*55+Time_ID,1);
%         u_y_true_KF(j,i) = u_y_true((AsP_ID-1)*55+Time_ID,1);
    end
end
