function surr_model = get_PCE_expansion_of_measurable(p_gpc, AssimPoint_Set, TimeStep_Set)

[xi_i, u_x, u_y, simprop] = Kalthoff_priorCovA_get_sim_results_Gauss(p_gpc);
%!!!!!!!!!!!!!!!!!!!!!!! You should not redefine P!!!!!! it should be
%defined from set_prior_distribution_and_maps!
[P, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCovA_Build_Proxy_Model(p_gpc); 

% Q .............. set of data on random variables and their pdf
% V_u ............ set of the basis gpc functions
% v_alpha_u_x .... set of the gpc coefficients for the surrogate model of u_x
% v_alpha_u_y .... set of the gpc coefficients for the surrogate model of u_y

% display(gpcbasis_polynomials(V_u));  % Show basis polynomials


% AssimPoint_Set = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
% % TimeStep_Set = [41,42,43,44,45,46,47];
% TimeStep_Set = [45];
n_AsP = length(AssimPoint_Set); % number of chosen assimilation points
n_TS = length(TimeStep_Set);    % number of chosen time steps

M = length(v_alpha_u_x(1,:));  % Number of basis polynomials;
length(v_alpha_u_x(:,1));


v_alpha_u_x_KF=zeros(n_AsP*n_TS,length(v_alpha_u_x(1,:)));
v_alpha_u_y_KF=zeros(n_AsP*n_TS,length(v_alpha_u_y(1,:)));

for j=1:n_AsP
    AsP_ID = AssimPoint_Set(1,j);
    for i=1:n_TS
        Time_ID = TimeStep_Set(1,i);
        v_alpha_u_x_KF((j-1)*n_TS+i,:) = v_alpha_u_x((AsP_ID-1)*55+Time_ID,:);
        v_alpha_u_y_KF((j-1)*n_TS+i,:) = v_alpha_u_y((AsP_ID-1)*55+Time_ID,:);
%         u_x_true_KF(j,i) = u_x_true((AsP_ID-1)*55+Time_ID,1);
%         u_y_true_KF(j,i) = u_y_true((AsP_ID-1)*55+Time_ID,1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
v_alpha_u = [v_alpha_u_x_KF; v_alpha_u_y_KF];
% put everything in a structure
surr_model.v_alpha_u = v_alpha_u;
surr_model.V_u =V_u;
surr_model.xi_i = xi_i;