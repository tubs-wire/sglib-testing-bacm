function [maxSq_err_u_x, maxRel_Sq_err_u_x, maxSq_err_u_y, maxRel_Sq_err_u_y, Rel_Sq_Err_u_x_QMC, Rel_Sq_Err_u_y_QMC] = Kalthoff_priorCov_Val_ProxyModel(Q, p, v_alpha_u_x, v_alpha_u_y)

%% Kalthoff test - prior Covariance matrix - Validating the proxy model
%% Validation set: 500 samples from QMC

%% 1) load data
% load sample points and simulation results (QMC)
[q_i_QMC, u_x_QMC, u_y_QMC, simprop_QMC] = Kalthoff_priorCov_get_sim_results();
% number of simulations
N_QMC = size(q_i_QMC, 2);
% number of timesteps
n_t_QMC = max(simprop_QMC(1,:));
u_x_QMC_FEM = u_x_QMC;
u_y_QMC_FEM = u_y_QMC;

%% 2) Evaluate surrogate model in the QMC points (the coefficients are computed by the proper orthogonal decomposition)
V_u_Surrogate = gpcbasis_modify(Q.get_germ(), 'p', p);
%xi_j_QMC = Q.params2germ(q_i_QMC);
xi_j_QMC = zeros(2,N_QMC);
for i=1:N_QMC
    xi_j_QMC(:,i) = Kalthoff_priorCov_InternalMappings('p2g', transpose(q_i_QMC(:,i)));
end
phi_alpha_QMC = gpcbasis_evaluate(V_u_Surrogate, xi_j_QMC);
u_x_QMC_Surr = v_alpha_u_x*phi_alpha_QMC;
u_y_QMC_Surr = v_alpha_u_y*phi_alpha_QMC;

%% 3) Compare FEM values and the correspodning values from the surrogate model, i.e. compute the error of the surrogate
% u_x_QMC_FEM  ... values from the FEM solver
% u_x_QMC_Surr  ... values from the surrogate model
% Dimension of u_x_QMC_FEM and u_x_QMC_Surr is [594] x [100] = [27 x 22] x [100] = [27 time steps x 22 assimilation points] x [100 samples]

Sq_Err_u_x_QMC = sum((u_x_QMC_FEM - u_x_QMC_Surr).^2,2);
Sq_u_x_QMC_FEM = sum((u_x_QMC_FEM).^2,2);
Rel_Sq_Err_u_x_QMC = Sq_Err_u_x_QMC./Sq_u_x_QMC_FEM;

maxSq_err_u_x = max(Sq_Err_u_x_QMC);
maxRel_Sq_err_u_x = max(Rel_Sq_Err_u_x_QMC);

Sq_Err_u_y_QMC = sum((u_y_QMC_FEM - u_y_QMC_Surr).^2,2);
Sq_u_y_QMC_FEM = sum((u_y_QMC_FEM).^2,2);
Rel_Sq_Err_u_y_QMC = Sq_Err_u_y_QMC./Sq_u_y_QMC_FEM;

maxSq_err_u_y = max(Sq_Err_u_y_QMC);
maxRel_Sq_err_u_y = max(Rel_Sq_Err_u_y_QMC);

% [err_u_x, err_u_y, max_err_u_x, max_err_u_y] = Kalthoff_Val_ProxyModel(Q, p, upsilon_alpha)

end