function E = Kalthoff_defineError_paramset()

%%  Define the prior distribution of the uncertain parameters K and G  
mean_ErrK = 0;
mean_ErrG = 0;
% variance of the longnormally distributed parameter
sig_ErrK = 0.15*20;
sig_ErrG = 0.15*10;


%% Define random variables
% Define the parameter with the desired distribution
ErrK = SimParameter('ErrK', NormalDistribution(mean_ErrK,sig_ErrK));
ErrG = SimParameter('ErrG', NormalDistribution(mean_ErrG,sig_ErrG));

E = SimParamSet();
E.add(ErrK);
E.add(ErrG);

% if plot_pdf_flag
% %% Plot densities
% k = linspace(10,250,200);
% pK = K.pdf(k);
% pG = G.pdf(k);
% figure
% % plot the distribution of 'K' and 'G'
% plot(k,pK,k,pG)
% legend({'pdf K','pdf G'},'Location','northeast')
% end

end