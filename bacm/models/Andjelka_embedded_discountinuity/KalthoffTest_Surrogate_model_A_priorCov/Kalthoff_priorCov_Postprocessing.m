%% KALTHOFF TEST
%% Post-processing

clear variables

p=3;
[Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p);

%% CROSS VALIDATION 1 (use measurements from the QMC samples)

N_SurrModels=length(v_alpha_u_x(:,1)); 

errorset=zeros(4,4);    % we have data (simulations) for 4 different degree of Gauss integration, i.e. p_gpc={2,3,4,5}
errorset2_u_x=zeros(N_SurrModels,4);  % 594 is a total number of surrogate models (594 = 22 x 27, where 22 assimilation points and 27 is a number of observed time steps)
errorset2_u_y=zeros(N_SurrModels,4);  % 4 is a number of considered gpc levels of Gauss integration points (for sampling)
for p=2:5
    [Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p);
    [maxSq_err_u_x, maxRel_Sq_err_u_x, maxSq_err_u_y, maxRel_Sq_err_u_y, Rel_Sq_Err_u_x, Rel_Sq_Err_u_y] = Kalthoff_priorCov_Val_ProxyModel(Q, p, v_alpha_u_x, v_alpha_u_y);
    errorset(p-1,:)=[maxSq_err_u_x, maxRel_Sq_err_u_x, maxSq_err_u_y, maxRel_Sq_err_u_y];
    errorset2_u_x(:,p-1) = Rel_Sq_Err_u_x;
    errorset2_u_y(:,p-1) = Rel_Sq_Err_u_y;
end

display(errorset)

figure()
%plot(rel_err)
hold on
plot(errorset2_u_x(:,1))
plot(errorset2_u_x(:,2))
plot(errorset2_u_x(:,3))
plot(errorset2_u_x(:,4))
legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('Proxy models for u_{x}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 1250 0 0.5])


figure()
%plot(rel_err)
hold on
plot(errorset2_u_y(:,1))
plot(errorset2_u_y(:,2))
plot(errorset2_u_y(:,3))
plot(errorset2_u_y(:,4))
legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('Proxy models for u_{y}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 1250 0 0.5])


%% CROSS VALIDATION 2 (use measurements from the QMC samples)
% in validation N?2 the error is computed as the ratio between A and B. 
% A is the max absolute difference between surrogate model and the FEM displacement
% B is the max displacement computed by FEM (set of all displacements for all samples and all time steps and all assimilatio points)
N_SurrModels=length(v_alpha_u_x(:,1)); 

errorset=zeros(4,4);    % we have data (simulations) for 4 different degree of Gauss integration, i.e. p_gpc={2,3,4,5}
errorset2_u_x=zeros(N_SurrModels,4);  % 594 is a total number of surrogate models (594 = 22 x 27, where 22 assimilation points and 27 is a number of observed time steps)
errorset2_u_y=zeros(N_SurrModels,4);  % 4 is a number of considered gpc levels of Gauss integration points (for sampling)
for p=2:5
    [Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p);
    [max_Err_u_x, maxRel_Err_u_x, max_Err_u_y, maxRel_Err_u_y, Rel_Err_max_u_x_QMC, Rel_Err_max_u_y_QMC] = Kalthoff_priorCov_Val2_ProxyModel(Q, p, v_alpha_u_x, v_alpha_u_y);
    errorset(p-1,:)=[max_Err_u_x, maxRel_Err_u_x, max_Err_u_y, maxRel_Err_u_y];
    errorset2_u_x(:,p-1) = Rel_Err_max_u_x_QMC;
    errorset2_u_y(:,p-1) = Rel_Err_max_u_y_QMC;
end

display(errorset)

figure()
%plot(rel_err)
hold on
plot(errorset2_u_x(:,1))
plot(errorset2_u_x(:,2))
plot(errorset2_u_x(:,3))
plot(errorset2_u_x(:,4))
legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{x}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 620 0 0.5])


figure()
%plot(rel_err)
hold on
plot(errorset2_u_y(:,1))
plot(errorset2_u_y(:,2))
plot(errorset2_u_y(:,3))
plot(errorset2_u_y(:,4))
legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{y}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 1250 0 0.5])

figure()
%plot(rel_err)
hold on
plot(errorset2_u_x(:,3))
plot(errorset2_u_x(:,4))
legend({'Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{x}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 620 0 0.5])


figure()
%plot(rel_err)
hold on
plot(errorset2_u_y(:,3))
plot(errorset2_u_y(:,4))
legend({'Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{y}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 1250 0 0.5])



N_Sampl = 55*11; % due to the symmetry, show the results for first 11 assim points 

figure()
%plot(rel_err)
hold on
%plot(errorset2_u_x(1:N_Sampl,1))
%plot(errorset2_u_x(1:N_Sampl,2))
plot(errorset2_u_x(1:N_Sampl,3))
plot(errorset2_u_x(1:N_Sampl,4))
%legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
legend({'Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{x}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 620 0 0.5])


figure()
%plot(rel_err)
hold on
%plot(errorset2_u_y(1:N_Sampl,1))
%plot(errorset2_u_y(1:N_Sampl,2))
plot(errorset2_u_y(1:N_Sampl,3))
plot(errorset2_u_y(1:N_Sampl,4))
%legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
legend({'Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('VAL2 -> Proxy models for u_{y}: relMaxErr')
xlabel('Index of the proxy model')
ylabel('relErr')
% axis([0 1250 0 0.5])


%% THE PCE SURROGATE MODEL (POD method to compute coefficients)
% POD = Proper orthogonal decomposition

p_gpc = 4;
[q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results_Gauss(p_gpc);
[Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p_gpc); 

% Q .............. set of data on random variables and their pdf
% V_u ............ set of the basis gpc functions
% v_alpha_u_x .... set of the gpc coefficients for the surrogate model of u_x
% v_alpha_u_y .... set of the gpc coefficients for the surrogate model of u_y

% display(gpcbasis_polynomials(V_u));  % Show basis polynomials

M = gpcbasis_size(V_u, 1); % number of polynomials

%% GET THE (ARTIFICIAL) TRUE MODEL
[q_exp, u_x_exp, u_y_exp, simprop_exp] = Kalthoff_priorCov_get_experimental_results();


figure()
%plot(rel_err)
hold on
plot(u_x_exp(:,1))
legend({'Gauss p_{gpc} = 2','Gauss p_{gpc} = 3','Gauss p_{gpc} = 4','Gauss p_{gpc} = 5'},'Location','northwest')
title('True model for u_{x}')
xlabel('Index of the proxy model')
ylabel('u_{x}')
% axis([0 1250 0 0.6])



%% COMPUTE STATISTICS from the PCE surrogate model

%% Compute the mean and the variance of the displacements
[u_x_mean, u_x_var] = gpc_moments(v_alpha_u_x, V_u);
[u_y_mean, u_y_var] = gpc_moments(v_alpha_u_y, V_u);

u_x_quant = gpc_quantiles(V_u, v_alpha_u_x, 'quantiles',[0.025, 0.975]);
u_y_quant = gpc_quantiles(V_u, v_alpha_u_y, 'quantiles',[0.025, 0.975]);


n_u = 22; % number of measurable responses
n_t = max(simprop(1,:));   % number of timesteps
time = simprop(3,:)';      % time steps instances
k = 20; % index of the assimilation point, i.e. ID = [1,2,3,...,21,22];


u_x_k_mean = u_x_mean(((k-1)*n_t+1):(k*n_t),:);
u_x_k_var = u_x_var(((k-1)*n_t+1):(k*n_t),:);
u_x_k_quant = u_x_quant(:,((k-1)*n_t+1):(k*n_t));
u_x_k_exp = u_x_exp(((k-1)*n_t+1):(k*n_t),:);


figure()
hold on;
quant1 = u_x_k_quant(1,:);
quant2 = u_x_k_quant(2,:);
gr1=plot(time, quant1, 'y', 'LineWidth', 0.5);
gr2=plot(time, quant2, 'y', 'LineWidth', 0.5);
time2 = [transpose(time), fliplr(transpose(time))];
inBetween = [quant1, fliplr(quant2)];
gr3=fill(time2, inBetween, 'y');
gr4=plot(time, u_x_k_mean,'k-','LineWidth',1);
gr5=plot(time, u_x_k_exp,'r-','LineWidth',1);
xlabel('Time [\mus]');
ylabel(['u_{x}^{',num2str(k),'} [mm]']);
title(['\fontsize{10} Proxy model for u_{x} in the assim point k=',num2str(k)]);
hold off
legend([gr3 gr4 gr5],{'95% confidence region', 'Mean','True model'},'Location','southeast');


u_y_k_mean = u_y_mean(((k-1)*n_t+1):(k*n_t),:);
u_y_k_var = u_y_var(((k-1)*n_t+1):(k*n_t),:);
u_y_k_quant = u_y_quant(:,((k-1)*n_t+1):(k*n_t));
u_y_k_exp = u_y_exp(((k-1)*n_t+1):(k*n_t),:);

figure()
hold on;
quant1 = u_y_k_quant(1,:);
quant2 = u_y_k_quant(2,:);
gr1=plot(time, quant1, 'c', 'LineWidth', 0.5);
gr2=plot(time, quant2, 'c', 'LineWidth', 0.5);
time2 = [transpose(time), fliplr(transpose(time))];
inBetween = [quant1, fliplr(quant2)];
gr3=fill(time2, inBetween, 'c');
gr4=plot(time, u_y_k_mean,'k-','LineWidth',1);
gr5=plot(time, u_y_k_exp,'r-','LineWidth',1);
xlabel('Time [\mus]');
ylabel(['u_{y}^{',num2str(k),'} [mm]']);
title(['\fontsize{10} Proxy model for u_{y} in the assim point k=',num2str(k)]);
hold off
legend([gr3 gr4 gr5],{'95% confidence region', 'Mean','True model'},'Location','northwest');




%% SOBOL SENSITIVITY

%gpc_sobol_partial_vars()
[u_x_par_var, u_x_I_s, u_x_ratio_by_index, u_x_ratio_by_order]=gpc_sobol_partial_vars(v_alpha_u_x, V_u);
[u_y_par_var, u_y_I_s, u_y_ratio_by_index, u_y_ratio_by_order]=gpc_sobol_partial_vars(v_alpha_u_y, V_u);


n_u = 22; % number of measurable responses
n_t = max(simprop(1,:));   % number of timesteps
time = simprop(3,:)';      % time steps instances
k = 22; % index of the assimilation point, i.e. ID = [1,2,3,...,21,22];

u_x_k_par_var = u_x_par_var(((k-1)*n_t+1):(k*n_t),:);
u_x_k_ratio_by_index = u_x_ratio_by_index(((k-1)*n_t+1):(k*n_t),:);

u_y_k_par_var = u_y_par_var(((k-1)*n_t+1):(k*n_t),:);
u_y_k_ratio_by_index = u_y_ratio_by_index(((k-1)*n_t+1):(k*n_t),:);

figure()
hold on
plot(time, u_x_k_par_var(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
plot(time, u_x_k_par_var(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
xlabel('Time [\mus]');
ylabel('Partial variances');
title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{x} in the assim point k=',num2str(k)]);

figure()
hold on
plot(time, u_x_k_ratio_by_index(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
plot(time, u_x_k_ratio_by_index(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
xlabel('Time [\mus]');
ylabel('Sobol coefficients');
title(['\fontsize{10}Sobol sensitivity: Surrogate model for u_{x} in the assim point k=',num2str(k)]);

figure()
hold on
plot(time, u_y_k_par_var(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
plot(time, u_y_k_par_var(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
xlabel('Time [\mus]');
ylabel('Partial variances');
title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{y} in the assim point k=',num2str(k)]);

figure()
hold on
plot(time, u_y_k_ratio_by_index(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
plot(time, u_y_k_ratio_by_index(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
xlabel('Time [\mus]');
ylabel('Sobol coefficients');
title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{y} in the assim point k=',num2str(k)]);

% 
% figure()
% subplot(2,2,1);
% plot(time, u_x_k_par_var(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
% subplot(2,2,1);
% plot(time, u_x_k_par_var(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
% legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
% xlabel('Time [\mus]');
% ylabel('Partial variances');
% title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{x} in the assim point k=',num2str(k)]);
% 
% subplot(2,2,2);
% plot(time, u_x_k_ratio_by_index(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
% plot(time, u_x_k_ratio_by_index(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
% legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
% xlabel('Time [\mus]');
% ylabel('Sobol coefficients');
% title(['\fontsize{10}Sobol sensitivity: Surrogate model for u_{x} in the assim point k=',num2str(k)]);
% 
% subplot(2,2,3);
% plot(time, u_y_k_par_var(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
% plot(time, u_y_k_par_var(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
% legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
% xlabel('Time [\mus]');
% ylabel('Partial variances');
% title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{y} in the assim point k=',num2str(k)]);
% 
% subplot(2,2,4);
% plot(time, u_y_k_ratio_by_index(:,1:2));   % (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
% plot(time, u_y_k_ratio_by_index(:,3), 'LineWidth', 2);  % (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
% legend(Q.param_names{1}, Q.param_names{2}, ['K & G']);   % (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
% xlabel('Time [\mus]');
% ylabel('Sobol coefficients');
% title(['\fontsize{10}Sobol sensitivity: Proxy model for u_{y} in the assim point k=',num2str(k)]);
% 
% 
% 
% 
% 
% for i=1:4
%     germs_i = set_x_j((2*(i-1)+1):(2*(i-1)+2),1:set_N_x_j(i,1));
%     N_germs_i = set_N_x_j(i,1);
%     for j=1:N_germs_i
%         F_j((2*(i-1)+1):(2*(i-1)+2),j) = germs2param(transpose(germs_i(:,j)));
%     end
%     subplot(4,2,2*(i-1)+1);
%     plot(germs_i(1,1:N_germs_i), germs_i(2,1:N_germs_i), 'm*')
%     axis([-4 4 -4 4])
%     if i==1
%         title('Samples from Gauss int points (reference RVs)')
%     end
%     xlabel('germs for K')
%     ylabel('germs for G')
%     subplot(4,2,2*i);
%     plot(F_j((2*(i-1)+1),1:N_germs_i), F_j((2*(i-1)+2),1:N_germs_i), '.')
%     axis([125 190 60 110])
%     if i==1
%         title('Samples from Gauss int points (parameter RVs)')
%     end
%     xlabel('K')
%     ylabel('G')
% end
% 
% 
% 
% 
%Calculating Sobol coefficients/Sobol indices:
% [partial_vars_i, I_i, sobol_sensitivity_i] = gpc_sobol_partial_vars(a_i_alpha, V_r, 'max_index', 3);

% where
% partial vars = PARTIAL VARIANCES
% I_i = MULTI-INDEX SET (here you actually have information how these Sobol coefficients are given in "partial_vars_i", where is just one 1 and others are 0 on that place should be partial wariance of just that param)
% sobol_sensitivity_i = SOBOL COEFFICIENTS
% a_i_alpha = PCE COEFFICIENTS
% V_r = POLYNOMIAL BASIS FUNCTIONS IN PCE
% 'max_index' = TOTAL INDEX OF POLYNOMIALS (optional input)


%Plotting:
% plot(u_i, partial_vars_i);            (I actually don't know if this line is really important)
% plot(u_i, partial_vars_i(:,1:3));   (these are partial variances of 1st order Sobol coeffs - when you vary just 1 parameter)
% hold on
% plot(u_i, partial_vars_i(:,4:6), 'LineWidth', 2);   (these are partial variances of higher order Sobol coeffs - when you vary more than 1 parameter)
% legend(Q.param_names{1}, 'K_{bs}', Q.param_names{3}, ['K & K_{bs}'], ['K & Beta'], ['K_{bs} & Beta']);   (small remark: if you have subscripts in your parameters' names you need to write it like you would in Latex, e.g. K_{bs}; if you don't have you can just write e.g. [Q.param_names{2} & Q.param_names{3}])
% 
% where
% u_i = TIME OR IMPOSED DISPLACEMENT (depends on how you want to plot your response)












