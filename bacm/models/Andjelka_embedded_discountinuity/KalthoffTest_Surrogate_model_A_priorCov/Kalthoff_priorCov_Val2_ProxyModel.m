function [max_Err_u_x, maxRel_Err_u_x, max_Err_u_y, maxRel_Err_u_y, Rel_Err_max_u_x_QMC, Rel_Err_max_u_y_QMC] = Kalthoff_priorCov_Val2_ProxyModel(Q, p, v_alpha_u_x, v_alpha_u_y)

%% Kalthoff test - prior Covariance matrix - Validating the proxy model
%% Validation set: 500 samples from QMC

% p=3;
% [Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p);

%% 1) load data
% load sample points and simulation results (QMC)
[q_i_QMC, u_x_QMC, u_y_QMC, simprop_QMC] = Kalthoff_priorCov_get_sim_results();
% number of simulations
N_QMC = size(q_i_QMC, 2);
% number of timesteps
n_t_QMC = max(simprop_QMC(1,:));
u_x_QMC_FEM = u_x_QMC;
u_y_QMC_FEM = u_y_QMC;

%% 2) Evaluate surrogate model in the QMC points (the coefficients are computed by the proper orthogonal decomposition)
V_u_Surrogate = gpcbasis_modify(Q.get_germ(), 'p', p);
xi_j_QMC = zeros(2,N_QMC);
for i=1:N_QMC
    xi_j_QMC(:,i) = Kalthoff_priorCov_InternalMappings('p2g', transpose(q_i_QMC(:,i)));
end
% xi_j_QMC = Q.params2germ(q_i_QMC);
phi_alpha_QMC = gpcbasis_evaluate(V_u_Surrogate, xi_j_QMC);
u_x_QMC_Surr = v_alpha_u_x*phi_alpha_QMC;
u_y_QMC_Surr = v_alpha_u_y*phi_alpha_QMC;

%% 3) Compare FEM values and the correspodning values from the surrogate model, i.e. compute the error of the surrogate
% u_x_QMC_FEM  ... values from the FEM solver
% u_x_QMC_Surr  ... values from the surrogate model
% Dimension of u_x_QMC_FEM and u_x_QMC_Surr is [594] x [100] = [27 x 22] x [100] = [27 time steps x 22 assimilation points] x [100 samples]

Err_u_x_QMC = abs(u_x_QMC_FEM - u_x_QMC_Surr);
max_u_x_QMC_FEM=max(max(abs(u_x_QMC_FEM)));
Rel_Err_max_u_x_QMC = max(Err_u_x_QMC,[],2)./max_u_x_QMC_FEM;

max_Err_u_x = max(max(Err_u_x_QMC));
maxRel_Err_u_x = max(max(Rel_Err_max_u_x_QMC));

Err_u_y_QMC = abs(u_y_QMC_FEM - u_y_QMC_Surr);
max_u_y_QMC_FEM=max(max(abs(u_y_QMC_FEM)));
Rel_Err_max_u_y_QMC = max(Err_u_y_QMC,[],2)./max_u_y_QMC_FEM;

max_Err_u_y = max(max(Err_u_y_QMC));
maxRel_Err_u_y = max(max(Rel_Err_max_u_y_QMC));


end