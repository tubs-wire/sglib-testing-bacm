function [q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results_Gauss(intDeg)

%% Kalthoff test - prior Covariance matrix - get simulation results - Full Gauss

%% SET OF RESULTS (of all simulations together)
% KALTHOFF TEST
% Sampling method: Gauss integration points (polynomial of degree up to 2, consequently 9 samples)
% Total number of sample points: 55  (i.e. number of time steps)
% Number of assimilation points: 22
% Numerical simulations are performed in Wolfram Mathematica/AceFEM
% Numerical simulation: dynamic analysis

% clear variables 

% intDeg ... Level of the Gauss integration. intDeg = {2,3,4}
switch intDeg
    case 2   % Level of the Gauss integration
        intDegree=2;intDegreeText=['Gauss Int Degree = ',num2str(intDegree)];disp(intDegreeText);
        DisplUX=load('Kalthoff_priorCov_GaussInt2_AssimPointDisplX');         % UX = set of horizontal displacements in the assimilation points (unit: mm)
        DisplUY=load('Kalthoff_priorCov_GaussInt2_AssimPointDisplY');         % UY = set of vertical displacements in the assimilation points (unit: mm)
        q_samplesLoad = load('Kalthoff_samples_priorCov_qFullGauss2'); % input data {K, G} for sample points (unit: GPa)
        q_samples=q_samplesLoad.q_intGauss2;
        SimProp=load('Kalthoff_priorCov_GaussInt2_SimObsDisplProp');          % properties of the observation: index, stepID, Time in the instance of reading (unit: microsecond)
    case 3
        intDegree=3;intDegreeText=['Gauss Int Degree = ',num2str(intDegree)];disp(intDegreeText);
        DisplUX=load('Kalthoff_priorCov_GaussInt3_AssimPointDisplX');
        DisplUY=load('Kalthoff_priorCov_GaussInt3_AssimPointDisplY');
        q_samplesLoad = load('Kalthoff_samples_priorCov_qFullGauss3');
        q_samples=q_samplesLoad.q_intGauss3;
        SimProp=load('Kalthoff_priorCov_GaussInt3_SimObsDisplProp');
    case 4
        intDegree=4;intDegreeText=['Gauss Int Degree = ',num2str(intDegree)];disp(intDegreeText);
        DisplUX=load('Kalthoff_priorCov_GaussInt4_AssimPointDisplX');
        DisplUY=load('Kalthoff_priorCov_GaussInt4_AssimPointDisplY');
        q_samplesLoad = load('Kalthoff_samples_priorCov_qFullGauss4');
        q_samples=q_samplesLoad.q_intGauss4;
        SimProp=load('Kalthoff_priorCov_GaussInt4_SimObsDisplProp');
    otherwise
        intDegree=5;intDegreeText=['Gauss Int Degree = ',num2str(intDegree)];disp(intDegreeText);
        DisplUX=load('Kalthoff_priorCov_GaussInt5_AssimPointDisplX');
        DisplUY=load('Kalthoff_priorCov_GaussInt5_AssimPointDisplY');
        q_samplesLoad = load('Kalthoff_samples_priorCov_qFullGauss5');
        q_samples=q_samplesLoad.q_intGauss5;
        SimProp=load('Kalthoff_priorCov_GaussInt5_SimObsDisplProp');
end

list_samples=q_samples;
list_Obs=DisplUX.AssimP1;

N_Samp=length(list_samples(1,:));   % Number of samples
N_Step=length(list_Obs(:,1));      % Number of time steps (from FEMsolver)      

%% Rename and rearrange the simulation results

DisplUXfield = fieldnames(DisplUX);
DisplUYfield = fieldnames(DisplUY);
N_Assim = length(fieldnames(DisplUX));  % Number of assimilation points
u_x = zeros(N_Assim*N_Step,N_Samp);
u_y = zeros(N_Assim*N_Step,N_Samp);

for i=1:N_Assim
    u_x((i-1)*N_Step+1:i*N_Step,:) = DisplUX.(DisplUXfield{i});
end

for i=1:length(fieldnames(DisplUY))
    u_y((i-1)*N_Step+1:i*N_Step,:) = DisplUY.(DisplUYfield{i});
end

simprop = zeros(3,N_Step);
SimPropfield = fieldnames(SimProp);
for i=1:length(SimPropfield)
    simprop(i,:) = SimProp.(SimPropfield{i});
end
q_i = q_samples;

end