function Q = Kalthoff_define_paramset(plot_pdf_flag)

%%  Define the prior distribution of the uncertain parameters K and G  
mean_K = 157;
mean_G = 85;
% variance of the longnormally distributed parameter
var_K = 400;
var_G = 100;
% compute parameters of the distribution (mean and std of the underlying
% Gaussian distribution)
muK = log((mean_K^2)/sqrt(var_K+mean_K^2));
muG = log((mean_G^2)/sqrt(var_G+mean_G^2));

sigK = sqrt(log(var_K/(mean_K^2)+1));
sigG = sqrt(log(var_G/(mean_G^2)+1));

%% Define random variables
% Define the parameter with the desired distribution
K = SimParameter('K', LogNormalDistribution(muK,sigK));
G = SimParameter('G', LogNormalDistribution(muG,sigG));

Q = SimParamSet();
Q.add(K);
Q.add(G);

if plot_pdf_flag
%% Plot densities
k = linspace(10,250,200);
pK = K.pdf(k);
pG = G.pdf(k);
figure
% plot the distribution of 'K' and 'G'
plot(k,pK,k,pG)
legend({'pdf K','pdf G'},'Location','northeast')
end

end