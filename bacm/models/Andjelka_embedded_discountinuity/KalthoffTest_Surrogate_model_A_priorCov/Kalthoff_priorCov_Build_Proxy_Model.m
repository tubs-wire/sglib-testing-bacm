function [Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p)

%% Kalthoff test - prior Covariance matrix - get simulation results

%% KALTHOFF TEST

%% BULDING THE SURROGATE MODEL

%% Generate surrogate model for homogenous input fields K and G
%
% This code shows how to generate a surrogate model when the uncertain
% input is just a random vector (two random variables K and G)

%%  1) Define the prior distribution of the uncertain parameters K and G
% load simparamset
plot_flag = false;
Q = Kalthoff_define_paramset(plot_flag);

% Determine the map from the reference random variables $X$ to {K,G}
Fmap = @(xi)Q.germ2params(xi);


%% 2) Specify the orthogonal basis polynomials.
% max degree of expansion
p_gpc=p;
% Set approximating subspace for the proxi model
V_u = gpcbasis_modify(Q.get_germ(), 'p', p_gpc);
% Show basis polynomials
%display(gpcbasis_polynomials(V_u));
% number of polynomials
M = gpcbasis_size(V_u, 1);


%% 3) Compute the squared norms of the basis polynomials
h_alpha = gpcbasis_norm(V_u, 'sqrt', false);
%display(h_alpha');


%% 4) Get the integration points and weights
% order of 2D integration rule
p_int = p_gpc + 1;

% integration points and weights
[x_j, w_j] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

% number of integration points
N = length(w_j);
%display(x_j);
%display(w_j);


%% 5) Map the integration points to the uncertain parameter
F_j = Fmap(x_j);
%display(F_j);


%% 6) Compute the measurable response by the FEM solver at the integration points
[q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results_Gauss(p_gpc);


%% 7) Evaluate all basis functions at the integration points
psi_alpha_j = gpcbasis_evaluate(V_u, x_j);
% display(psi_alpha_j)

%% 8) Compute the PCE coefficients (Orthogonal projection)
% number of measurable responses
n_u = 22;
n_t = max(simprop(1,:));   % number of timesteps
% initiate matrix for PCE coefficients
% n_u ... total number of assimilation points
% n_t ... total number of timesteps
% M ... number of basis polynomials
% N ... number of integration points

% % % v_alpha_u_x = zeros(n_u*n_t, M);
% % % v_alpha_u_y = zeros(n_u*n_t, M);
% % % for j = 1:N
% % %     v_alpha_u_x = v_alpha_u_x + (w_j(j) * u_x(:,j) * (psi_alpha_j(:,j)./h_alpha)');
% % %     v_alpha_u_y = v_alpha_u_y + (w_j(j) * u_y(:,j) * (psi_alpha_j(:,j)./h_alpha)');
% % % end

% Computing coefficients by vectorization (faster)
v_alpha_u_x = u_x*(diag(w_j)* psi_alpha_j'/(diag(h_alpha)));
v_alpha_u_y = u_y*(diag(w_j)* psi_alpha_j'/(diag(h_alpha)));

end