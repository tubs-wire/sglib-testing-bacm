function [V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_get_prior_and_proxi(p_gpc)

%% THE PCE SURROGATE MODEL (POD method to compute coefficients)
% POD = Proper orthogonal decomposition


[q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results_Gauss(p_gpc);
[Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p_gpc); 

% Q .............. set of data on random variables and their pdf
% V_u ............ set of the basis gpc functions
% v_alpha_u_x .... set of the gpc coefficients for the surrogate model of u_x
% v_alpha_u_y .... set of the gpc coefficients for the surrogate model of u_y

% display(gpcbasis_polynomials(V_u));  % Show basis polynomials

end