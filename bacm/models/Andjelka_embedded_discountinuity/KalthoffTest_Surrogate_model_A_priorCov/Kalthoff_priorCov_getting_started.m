%% KALTHOFF'S TEST
%% SURROGATE MODEL N?1
% Random variables:
%    K ... bulk modulus
%    G ... shear modulus
% Given data: K and G are homogeneous random field over the model domain
% 
% Task to do: Prepare a surrogate model for parameter identification of K and G
%% Example 1: generate surrogate model for homogeneous input fields of K and G
%
% This code shows how to generate a surrogate model when the uncertain
% input is just a random variable

clearvars
clf


%%  1) Define the prior distribution of the uncertain parameters K(x,y) and G(x,y)  (homogeneous field)
mean_K = 157;
mean_G = 85;
% variance of the longnormally distributed parameter
var_K = 400;
var_G = 100;
% compute parameters of the distribution (mean and std of the underlying Gaussian distribution)
muK = log((mean_K^2)/sqrt(var_K+mean_K^2));
muG = log((mean_G^2)/sqrt(var_G+mean_G^2));

sigK = sqrt(log(var_K/(mean_K^2)+1));
sigG = sqrt(log(var_G/(mean_G^2)+1));

mu = [muK,muG];
sig = [sigK,sigG];

% [exp(muK-sigK),exp(muK),exp(muK+sigK)]
% [exp(muG-sigG),exp(muG),exp(muG+sigG)]


%% 2) Define the prior covariance matrix
Kstd = sigK; Gstd = sigG;
C = [Kstd+0.25*Gstd, 0.25*(Kstd+Gstd); 0.25*(Kstd+Gstd), 0.25*Kstd+Gstd];


%% 3) Seperate representation of the underlying Gaussian field
% Cholesky decomposition of the prior covariance matrix. LC is the lower triangular matrix.
LC = chol(C,'lower');

%% 3.1) The mappings from the reference random variables q (=germs) to parameters (RVs K and G)
% The map from q to theta
germs2theta = @(q) (LC*transpose(q));
% And its inverse map, the theta field projected onto the eigenbasis
theta2germs = @(th) (LC\transpose(th));

% The map from the input parameter to the random field
param2theta = @(p) ((log(p)-mu)./sig);
% And the inverse map
theta2param = @(th) (exp(mu+sig.*th));


% The map from germs q to parameters
germs2param = @(g) (exp(mu+sig.*transpose(germs2theta(g))));
% The inverse map: from input parameters to germs q
param2germs = @(q) theta2germs((log(q)-mu)./sig);


%% 4) Define random variables
% Define the parameter with the desired distribution
K = SimParameter('K', LogNormalDistribution(muK,sigK));
G = SimParameter('G', LogNormalDistribution(muG,sigG));

Q = SimParamSet();
Q.add(K);
Q.add(G);


% Plot densities
k = linspace(10,250,200);
pK = K.pdf(k);
pG = G.pdf(k);
figure
plot(k,pK,k,pG)
legend({'pdf K','pdf G'},'Location','northeast')


%% 5) GENERATING SAMPLES: Gauss integration points

p_max=5;  % max degree of considered gpc expansion
N_p_max = 36; %factorial(2 + p_max)/(factorial(2)*factorial(p_max)) % number of polynomials for p_max
set_N_x_j=zeros(p_max-1,1);
set_x_j = zeros((p_max-1)*2,N_p_max);
for i=2:p_max
    % Specify the orthogonal basis polynomials.
    % max degree of expansion
    p_gpc=i;
    % Set approximating subspace for the proxi model
    V_u = gpcbasis_modify(Q.get_germ(), 'p', p_gpc);
    % Show basis polynomials
    % display(gpcbasis_polynomials(V_u));
    % number of polynomials
    M = gpcbasis_size(V_u, 1);
    % order of 2D integration rule
    p_int = p_gpc + 1;
    % integration points and weights
    [x_j, w_j] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');
    % display(x_j)
    % number of integration points
    N = length(w_j);
    set_N_x_j(i-1,1)=N;
    set_x_j((2*(i-2)+1):(2*(i-2)+2),1:N)=x_j;    % set of integration points for considered degrees of gpc
end


%% 5.1) Map the integration points to parameters
F_j = zeros(length(set_x_j(:,1)),length(set_x_j(1,:)));

figure
for i=1:4
    germs_i = set_x_j((2*(i-1)+1):(2*(i-1)+2),1:set_N_x_j(i,1));
    N_germs_i = set_N_x_j(i,1);
    for j=1:N_germs_i
        F_j((2*(i-1)+1):(2*(i-1)+2),j) = germs2param(transpose(germs_i(:,j)));
    end
    subplot(4,2,2*(i-1)+1);
    plot(germs_i(1,1:N_germs_i), germs_i(2,1:N_germs_i), 'm*')
    axis([-4 4 -4 4])
    if i==1
        title('Samples from Gauss int points (reference RVs)')
    end
    xlabel('germs for K')
    ylabel('germs for G')
    subplot(4,2,2*i);
    plot(F_j((2*(i-1)+1),1:N_germs_i), F_j((2*(i-1)+2),1:N_germs_i), '.')
    axis([125 190 60 110])
    if i==1
        title('Samples from Gauss int points (parameter RVs)')
    end
    xlabel('K')
    ylabel('G')
end

q_intGauss2=F_j(1:2,1:set_N_x_j(1,1));
q_intGauss3=F_j(3:4,1:set_N_x_j(2,1));
q_intGauss4=F_j(5:6,1:set_N_x_j(3,1));
q_intGauss5=F_j(7:8,1:set_N_x_j(4,1));

cd
save('Kalthoff_samples_priorCov_qFullGauss2.mat','q_intGauss2');
save('Kalthoff_samples_priorCov_qFullGauss3.mat','q_intGauss3');
save('Kalthoff_samples_priorCov_qFullGauss4.mat','q_intGauss4');
save('Kalthoff_samples_priorCov_qFullGauss5.mat','q_intGauss5');



%% 6) GENERATING SAMPLES: Quasi Monte Carlo

% max degree of expansion
p_gpc=5;
% Set approximating subspace for the proxi model
V_u = gpcbasis_modify(Q.get_germ(), 'p', p_gpc);
% Show basis polynomials
% display(gpcbasis_polynomials(V_u));
% number of polynomials

Ns = 500; % number of samples
xi_qmc = gpcgerm_sample(V_u, Ns,'mode','qmc');
q_qmc = zeros(2,Ns);
for i=1:Ns
    q_qmc(:,i) = germs2param(transpose(xi_qmc(:,i)));
end


figure
subplot(1,2,1);
plot(xi_qmc(1,:), xi_qmc(2,:), 'm*')
axis([-4 4 -4 4])
title('Samples from QMC (reference RVs)')
xlabel('germs for K')
ylabel('germs for G')

subplot(1,2,2);
plot(q_qmc(1,:), q_qmc(2,:), '.')
axis([125 190 60 110])
title('Samples from QMC (parameter RVs)')
xlabel('K')
ylabel('G')

cd
save('Kalthoff_samples_priorCov_QMC.mat','q_qmc');