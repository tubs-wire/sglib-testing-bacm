function [q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results()

%% Kalthoff test - prior Covariance matrix - get simulation results

%% SET OF RESULTS (of all simulations together)
% KALTHOFF TEST
% Sampling method: QMC (500 samples)
% Total number of sample points: 55  (i.e. number of time steps)
% Number of assimilation points: 22
% Numerical simulations are performed in Wolfram Mathematica/AceFEM
% Numerical simulation: dynamic analysis

% clear variables

DisplUX=load('Kalthoff_priorCov_qmc_AssimPointDisplX');  % UX = set of horizontal displacements in the assimilation points (unit: mm)
DisplUY=load('Kalthoff_priorCov_qmc_AssimPointDisplY');  % UY = set of vertical displacements in the assimilation points (unit: mm)
q_samples = load('Kalthoff_samples_priorCov_QMC');       % input data {K, G} for sample points (unit: GPa)
SimProp=load('Kalthoff_priorCov_qmc_SimObsDisplProp');   % properties of the observation: index, stepID, Time in the instance of reading (unit: microsecond)


list_samples=q_samples.q_qmc;
list_Obs=DisplUX.AssimP1;

N_Samp=length(list_samples(1,:));             % Number of samples
N_Step=length(list_Obs(:,1));                 % Number of observed time steps (from FEMsolver)   
N_AssimP = length(fieldnames(DisplUY));       % Number of assimilation points
 
%% Rename and rearrange the simulation results

u_x = zeros(N_AssimP*N_Step,N_Samp);
u_y = zeros(N_AssimP*N_Step,N_Samp);
DisplUXfield = fieldnames(DisplUX);
DisplUYfield = fieldnames(DisplUY);

for i=1:length(fieldnames(DisplUX))
    u_x((i-1)*N_Step+1:i*N_Step,:) = DisplUX.(DisplUXfield{i});
end

for i=1:length(fieldnames(DisplUY))
    u_y((i-1)*N_Step+1:i*N_Step,:) = DisplUY.(DisplUYfield{i});
end

simprop = zeros(3,N_Step);
SimPropfield = fieldnames(SimProp);
for i=1:length(SimPropfield)
    simprop(i,:) = SimProp.(SimPropfield{i});
end
q_i = q_samples.q_qmc;

end