%% SURROGATE MODEL N?1
% Random variables:
%    K ... bulk modulus
%    G ... shear modulus
% Given data: K and G are homogeneous random field over the model domain
% 
% Task to do: Prepare a surrogate model for parameter identification of K and G

%% Example 1: generate surrogate model for homogeneous input fields of K and G
%
% This code shows how to generate a surrogate model when the uncertain
% input is just a random variable

clearvars
clf


%%  1) Define the prior distribution of the uncertain parameters K(x,y) and G(x,y)  (homogeneous field)
mean_K = 157;
mean_G = 85;
% variance of the longnormally distributed parameter
var_K = 400;
var_G = 100;
% compute parameters of the distribution (mean and std of the underlying Gaussian distribution)
muK = log((mean_K^2)/sqrt(var_K+mean_K^2));
muG = log((mean_G^2)/sqrt(var_G+mean_G^2));

sigK = sqrt(log(var_K/(mean_K^2)+1));
sigG = sqrt(log(var_G/(mean_G^2)+1));

mu = [muK,muG];
sig = [sigK,sigG];

% [exp(muK-sigK),exp(muK),exp(muK+sigK)]
% [exp(muG-sigG),exp(muG),exp(muG+sigG)]


%% 2) Define the prior covariance matrix
Kstd = sigK; Gstd = sigG;
C = [Kstd+0.25*Gstd, 0.25*(Kstd+Gstd); 0.25*(Kstd+Gstd), 0.25*Kstd+Gstd];


%% 3) Seperate representation of the underlying Gaussian field
% Cholesky decomposition of the prior covariance matrix. LC is the lower triangular matrix.
LC_gamma = chol(C,'lower');

%% 3.1) The mappings from the reference random variables q (=germs) to parameters (RVs K and G)
% The map from q to theta
germs2theta = @(g) (LC_gamma*transpose(g));
% And its inverse map, the theta field projected onto the eigenbasis
theta2germs = @(th) (LC_gamma\transpose(th));

% The map from the input parameter to the random field
param2theta = @(p) ((log(p)-mu)./sig);
% And the inverse map
theta2param = @(th) (exp(mu+sig.*th));

% The map from germs q to parameters
germs2param = @(q) (exp(mu+sig.*transpose(germs2theta(q))));
% The inverse map: from input parameters to germs q
param2germs = @(p) theta2germs((log(p)-mu)./sig);


%% 4) Define random variables
% Define the parameter with the desired distribution
K = SimParameter('K', LogNormalDistribution(muK,sigK));
G = SimParameter('G', LogNormalDistribution(muG,sigG));

Q = SimParamSet();
Q.add(K);
Q.add(G);


% Plot densities
k = linspace(10,250,200);
pK = K.pdf(k);
pG = G.pdf(k);
figure
plot(k,pK,k,pG)
legend({'pdf K','pdf G'},'Location','northeast')


%% 5) Build the surrogate model for the RVs (Q)
% specify the orthogonal basis polynomials.
% max degree of expansion
q_gpc=2;
% Set approximating subspace for the proxi model
V_p = gpcbasis_modify(Q.get_germ(), 'p', 2);
% Show basis polynomials
display(gpcbasis_polynomials(V_p));
% number of polynomials
M_p = gpcbasis_size(V_p, 1);

%% the given (prior) PCE coefficients
x_0 = [muK; muG];
x_1 = [0.3817174; 0.0573446];
x_2 = [0.1024296; 0.3817174];
x_3 = [0; 0];
x_4 = [0; 0];
x_5 = [0; 0];

v_alpha_p = [x_0,x_1,x_2,x_3,x_4,x_5];


%% 6) The synthetic truth
[p_true, u_x_true, u_y_true, simprop_true] = Kalthoff_priorCov_get_experimental_results();

% Specicify the 'true' value of the parameters K and G
xi_true = param2germs(transpose(p_true));
q_true = transpose(germs2param(transpose(xi_true)));

% set of chosen assimilation points and time steps
AssimPoint_Set = [2,4,6,8,10,13,15,17,19,21];
TimeStep_Set = [41,43,45,47,49];
n_AsP = length(AssimPoint_Set); % number of chosen assimilation points
n_TS = length(TimeStep_Set);    % number of chosen time steps

u_x_true_KF=zeros(n_AsP*n_TS,1);
u_y_true_KF=zeros(n_AsP*n_TS,1);
% u_x_true_KF=zeros(n_AsP,n_TS);
% u_y_true_KF=zeros(n_AsP,n_TS);

for j=1:n_AsP
    AsP_ID = AssimPoint_Set(1,j);
    for i=1:n_TS
        Time_ID = TimeStep_Set(1,i);
        u_x_true_KF((j-1)*n_TS+i,1) = u_x_true((AsP_ID-1)*55+Time_ID,1);
        u_y_true_KF((j-1)*n_TS+i,1) = u_y_true((AsP_ID-1)*55+Time_ID,1);
%         u_x_true_KF(j,i) = u_x_true((AsP_ID-1)*55+Time_ID,1);
%         u_y_true_KF(j,i) = u_y_true((AsP_ID-1)*55+Time_ID,1);
    end
end


%% 6) Generate the synthetic measurements (u_x)
% standard deviation of the measurements
sigma_em = abs(u_x_true_KF)*0.05;
% Set of parameters with the measurement error
E_m=generate_stdrn_simparamset(sigma_em);
% The synthetic measurement
z_m=u_x_true_KF+E_m.sample(1);

% plot the assimilated displacements and the generated measurements
h1 = figure();
plot(u_x_true_KF(:,1), 'LineWidth', 2)
hold on
plot(z_m(:,1), 'x', 'LineWidth', 2)
xlabel('Index of assimilation point k')
ylabel('Displacement [m]')
legend('True', 'Synthetic measurement', 'Location','SouthEast')
%save_png(h, 'Measurements', 'figdir', 'figs', 'res', 600)

% figure();
% for i=1:2
%     subplot(2,1,i);
%     plot(u_x_true_KF(:,i), 'LineWidth', 2)
%     %plot(z_m(:,i), 'x', 'LineWidth', 2)
%     xlabel('Index of assimilation point k')
%     ylabel('Displacement [m]')
%     legend('True', 'Synthetic measurement', 'Location','SouthEast')
% end


%% 7) Define the error model which is used for the update
%sigma_e = 2.4*10^-4+ones(n_meas,1);
sigma_e = abs(z_m)*0.10;
E = generate_stdrn_simparamset(sigma_e);
%E = generate_stdrn_simparamset(sigma_em*1.25);


%% 8) The PCE surrogate model (POD method to compute coefficients)
% POD = Proper orthogonal decomposition

p_gpc = 4;
[q_i, u_x, u_y, simprop] = Kalthoff_priorCov_get_sim_results_Gauss(p_gpc);
[Q, V_u, v_alpha_u_x, v_alpha_u_y] = Kalthoff_priorCov_Build_Proxy_Model(p_gpc); 

% Q .............. set of data on random variables and their pdf
% V_u ............ set of the basis gpc functions
% v_alpha_u_x .... set of the gpc coefficients for the surrogate model of u_x
% v_alpha_u_y .... set of the gpc coefficients for the surrogate model of u_y

% display(gpcbasis_polynomials(V_u));  % Show basis polynomials

% set of chosen assimilation points and time steps
AssimPoint_Set = [2,4,6,8,10,13,15,17,19,21];
TimeStep_Set = [41,43,45,47,49];
n_AsP = length(AssimPoint_Set); % number of chosen assimilation points
n_TS = length(TimeStep_Set);    % number of chosen time steps

M = length(v_alpha_u_x(1,:));  % Number of basis polynomials;
length(v_alpha_u_x(:,1))


v_alpha_u_x_KF=zeros(n_AsP*n_TS,length(v_alpha_u_x(1,:)));
v_alpha_u_y_KF=zeros(n_AsP*n_TS,length(v_alpha_u_y(1,:)));

for j=1:n_AsP
    AsP_ID = AssimPoint_Set(1,j);
    for i=1:n_TS
        Time_ID = TimeStep_Set(1,i);
        v_alpha_u_x_KF((j-1)*n_TS+i,:) = v_alpha_u_x((AsP_ID-1)*55+Time_ID,:);
        v_alpha_u_y_KF((j-1)*n_TS+i,:) = v_alpha_u_y((AsP_ID-1)*55+Time_ID,:);
%         u_x_true_KF(j,i) = u_x_true((AsP_ID-1)*55+Time_ID,1);
%         u_y_true_KF(j,i) = u_y_true((AsP_ID-1)*55+Time_ID,1);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% PCE based Kalman filter

%% 0) Initiate problem
% Initiate_updates_RV()
method = 'PCE_KF';
% number of samples for the pdf plots (this is only used for estimating the
% probability density of $F_{cM}$)
N= 10000;

%% 1) Write $Q$, $\vec E$ in a PCE form.
% [q_i_alpha_1, V_q] = Q.gpc_expand()
q_i_alpha_1 = v_alpha_p;
V_q = V_p;
[e_l_beta, V_e] = E.gpc_expand();

%% 2) Generate a combined PCE basis

V_y = V_u;

% First we bring the Qs to the PCE basis of Y_h
[V_qy, Pr_V_q, Pr_V_qy] = gpcbasis_combine(V_q, V_y, 'inner_sum', 'as_operators', true);
q_i_alpha=q_i_alpha_1*Pr_V_q;
v_alpha_u_x_KF=v_alpha_u_x_KF*Pr_V_qy;
 
% The combined basis of Q, Y and E
[V_c, Pr_V_qy, Pr_V_e] = gpcbasis_combine(V_qy, V_e, 'outer_sum', 'as_operators', true);

%% 3) Rewrite the PCE coefficients in the extended basis
% Please, note, that the order of the basis functions are different here
% then the one used in the chapter and thus the coefficient matrices deviate from the one given in the chapter) 

q_i_gamma=q_i_alpha*Pr_V_qy;
z_j_gamma=v_alpha_u_x_KF*Pr_V_qy + e_l_beta*Pr_V_e;
zm_j_gamma = zeros(size(z_j_gamma));
zm_j_gamma(:, 1) = z_m;

%% 4) Compute the Kalman gain
    %% 4a) Compute the covariance matrices
    
    % Autocovariance of the measurable response
    C_y=gpc_covariance(v_alpha_u_x_KF, V_y);

    % Covariance of $Q$ and $Y$
    C_qy=gpc_covariance(q_i_alpha, V_y, v_alpha_u_x_KF);

    % Covariance of the measurement noise
    C_e=gpc_covariance(e_l_beta, V_e);
    
    %% 4b) Compute the Kalman gain
    
	% Covariance of the measurment model
    C_z = C_y+C_e;
    % The Kalman gain
    K = C_qy/C_z;
    
%% 5) Compute the coefficients of the updated input variables $\vec Q'$ 

q_p_i_gamma     = q_i_gamma       +K* (zm_j_gamma-z_j_gamma);

 

%% 5) Generate samples of the scaling factor
% sample from the PCE of the updated input parameter
q_j = gpc_sample(q_p_i_gamma, V_c, N);

C_p_i_gamma = gpc_covariance(q_p_i_gamma, V_c);

% %% 3) Seperate representation of the underlying Gaussian field
% % Cholesky decomposition of the prior covariance matrix. LC is the lower triangular matrix.
% LC_gamma = chol(C_p_i_gamma,'lower');
% 
% %% 3.1) The mappings from the reference random variables q (=germs) to parameters (RVs K and G)
% % The map from q to theta
% germs2theta = @(g) (LC_gamma*transpose(g));
% % And its inverse map, the theta field projected onto the eigenbasis
% theta2germs = @(th) (LC_gamma\transpose(th));
% 
% % The map from the input parameter to the random field
% param2theta = @(p) ((log(p)-mu)./sig);
% % And the inverse map
% theta2param = @(th) (exp(mu+sig.*th));
% 
% % The map from germs q to parameters
% germs2param = @(q) (exp(mu+sig.*transpose(germs2theta(q))));
% % The inverse map: from input parameters to germs q
% param2germs = @(p) theta2germs((log(p)-mu)./sig);


% map to the scaling factor
f_cm_j = germs2param(transpose(q_j));

%% Compute statistics

% mean of the posterior density
f_cm_mean  = mean(transpose(f_cm_j),2);
% variance of the posterior density
f_cm_var = var(transpose(f_cm_j),[], 2);

%% Show statistics
display(strvarexpand('Prior mean: $Q.mean$'));
display(strvarexpand('Prior variance: $Q.var$'));
display(strvarexpand('True value: $p_true$'));
display(strvarexpand('Posterior mean: $f_cm_mean$'));
display(strvarexpand('Posterior variance: f_cm_var=$f_cm_var$'));

%% Plot prior and posterior densities
% bin_width = 0.1;
% plot_prior_and_posterior_densities

% Define the parameter with the desired distribution
K = SimParameter('K', LogNormalDistribution(muK,sigK));
G = SimParameter('G', LogNormalDistribution(muG,sigG));
% Plot densities
k_K = linspace(70,250,200);
k_G = linspace(20,160,200);
pK = K.pdf(k_K);
pG = G.pdf(k_G);

mu_KG_gamma = log((f_cm_mean.^2)./sqrt(f_cm_var+f_cm_mean.^2));
sigKG_gamma = sqrt(log(f_cm_var./(f_cm_mean.^2)+1));
pK_gamma = pdf('lognormal',k_K,mu_KG_gamma(1,1),sigKG_gamma(1,1));
pG_gamma = pdf('lognormal',k_G,mu_KG_gamma(2,1),sigKG_gamma(2,1));

K_true = q_true(1,1);
G_true = q_true(2,1);

K_post = f_cm_mean(1,1);
G_post = f_cm_mean(2,1);

figure
x_true=[K_true,K_true];
x_post=[K_post,K_post];
y=[0,max(pK_gamma)*1.1];
plot(k_K,pK,k_K,pK_gamma,x_true,y,'k',x_post,y,'b','LineWidth',1)
legend({'Prior pdf K','Posterior pdf K','True K = 165','posterior mean'},'Location','northwest')


figure
x_true=[G_true,G_true];
x_post=[G_post,G_post];
y=[0,max(pG_gamma)*1.1];
plot(k_G,pG,k_G,pG_gamma,x_true,y,'k',x_post,y,'b','LineWidth',1)
legend({'Prior pdf G','Posterior pdf G','True G = 80','posterior mean'},'Location','northwest')


