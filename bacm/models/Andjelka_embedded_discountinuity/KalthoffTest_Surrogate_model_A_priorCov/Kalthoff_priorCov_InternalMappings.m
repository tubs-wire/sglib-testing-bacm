function [out_var] = Kalthoff_priorCov_InternalMappings(map_dir, in_var)


%%  1) Define the prior distribution of the uncertain parameters K(x,y) and G(x,y)  (homogeneous field)
mean_K = 157;
mean_G = 85;
% variance of the longnormally distributed parameter
var_K = 400;
var_G = 100;
% compute parameters of the distribution (mean and std of the underlying Gaussian distribution)
muK = log((mean_K^2)/sqrt(var_K+mean_K^2));
muG = log((mean_G^2)/sqrt(var_G+mean_G^2));

sigK = sqrt(log(var_K/(mean_K^2)+1));
sigG = sqrt(log(var_G/(mean_G^2)+1));

mu = [muK,muG];
sig = [sigK,sigG];

% [exp(muK-sigK),exp(muK),exp(muK+sigK)]
% [exp(muG-sigG),exp(muG),exp(muG+sigG)]


%% 2) Define the prior covariance matrix
Kstd = sigK; Gstd = sigG;
C = [Kstd+0.25*Gstd, 0.25*(Kstd+Gstd); 0.25*(Kstd+Gstd), 0.25*Kstd+Gstd];


%% 3) Seperate representation of the underlying Gaussian field
% Cholesky decomposition of the prior covariance matrix. LC is the lower triangular matrix.
LC = chol(C,'lower');

%% 3.1) The mappings from the reference random variables q (=germs) to parameters (RVs K and G)
% The map from q to theta
germs2theta = @(q) (LC*transpose(q));
% And its inverse map, the theta field projected onto the eigenbasis
theta2germs = @(th) (LC\transpose(th));

% The map from the input parameter to the random field
param2theta = @(p) ((log(p)-mu)./sig);
% And the inverse map
theta2param = @(th) (exp(mu+sig.*th));


% The map from germs q to parameters
germs2param = @(q) (exp(mu+sig.*transpose(germs2theta(q))));
% The inverse map: from input parameters to germs q
param2germs = @(p) theta2germs((log(p)-mu)./sig);


switch map_dir
    case 'g2p'
        out_var = germs2param(in_var);
    case 'p2g'
        out_var = param2germs(in_var);
    otherwise
        display('ERROR: Check map_dir.');
        out_var = in_var;
end




end
