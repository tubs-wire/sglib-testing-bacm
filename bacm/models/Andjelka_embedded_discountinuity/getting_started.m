%% Initiate initial uncertainties

Q = SimParamSet();
% Ultimate stress perpendicular to the crack
dist_1 = fix_bounds(BetaDistribution(3,3), 2.5, 3.1);
Q.add(SimParameter('n', dist_1));
% Ultimate stress parallel to the crack
dist_2 = UniformDistribution(1, 2.8);
Q.add(SimParameter('m', dist_2));
% Fracture energies
Q.add(SimParameter('G_fn', UniformDistribution(.1, .5)));
Q.add(SimParameter('G_fm', UniformDistribution(.1, .5)));

%% Plot densities
% plot the distribution of 'n'
x_i = linspace(2.5, 3.1);
plot(x_i, dist_1.pdf(x_i))
% plot the distribution of 'm'
x_i = linspace(1, 2.8);
plot(x_i, dist_2.pdf(x_i))

%% Run simultaion at QMC sample points
% number of saamples
N = 1000;
q_i_QMC = Q.sample(N, 'mode', 'qmc');
plot_grouped_scatter(q_i_QMC,'Labels', Q.param_names)

%% Run simulation at Gauss quadrature points
V_u = Q.get_germ();
% total degree of the multivariate polynomials
p_gpc = 3;
V_u = gpcbasis_modify(V_u, 'p', p_gpc);
[q_i, w_i] = Q.get_integration_points(p_gpc+1);
%save(int_points....
plot_grouped_scatter(q_i,'Labels', Q.param_names)

