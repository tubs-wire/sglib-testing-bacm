function [K_mean, G_mean, var_K, var_G] = Kalthoff_priorCovA_Init_Guess()
% The initial Guess of the prior distribution of the uncertain parameters K(x,y) and G(x,y)  (homogeneous field)

% mean values of the longnormally distributed parameters
K_mean = 150;
G_mean = 75;

% variances of the longnormally distributed parameters
var_K = 400;
var_G = 100;

% the equivalent Young's modulus and Poisson coefficient
% E_mean = (9*mean_K*mean_G)/(3*mean_K+mean_G);
% Poiss_mean = (3*mean_K-2*mean_G)/(2*(3*mean_K+mean_G));

end
