function [v_alpha_th, V_th, maps, P] = set_prior_distribution_and_maps()

%    K ... bulk modulus
%    G ... shear modulus
%%  1) Define the prior distribution of the uncertain parameters K(x,y) and G(x,y)  (homogeneous field)
% the mean and the variances of the longnormally distributed parameters
[K_mean, G_mean, var_K, var_G] = Kalthoff_priorCovA_Init_Guess();

% compute parameters of the distribution (mean and std of the underlying Gaussian distribution)
muK = log((K_mean^2)/sqrt(var_K+K_mean^2));
muG = log((G_mean^2)/sqrt(var_G+G_mean^2));

sigK = sqrt(log(var_K/(K_mean^2)+1));
sigG = sqrt(log(var_G/(G_mean^2)+1));

%mu = [muK;muG];
%sig = [sigK;sigG];


%% 2) Define the prior covariance matrix
%C = [sigK^2, 0.4*(sigK^2+sigG^2); 0.4*(sigK^2+sigG^2), sigG^2];
% cholesky decomposition
%L = chol(C, 'lower');

%% 4) Define random variables
% Define the parameter with the desired distribution
K = SimParameter('K', LogNormalDistribution(muK,sigK));
G = SimParameter('G', LogNormalDistribution(muG,sigG));

P = SimParamSet();
P.add(K);
P.add(G);

%% 5) Build the surrogate model for the RVs (Q)
% specify the orthogonal basis polynomials.
% max degree of expansion
p_gpc=1;
% Set approximating subspace for the proxi model
V_th = gpcbasis_modify(P.get_germ(), 'p', p_gpc);
% Show basis polynomials
display(gpcbasis_polynomials(V_th));
% number of polynomials
%M_p = gpcbasis_size(V_th, 1);


%% the given (prior) PCE coefficients (surrogate model of th=(logK and logG))
alfa_1 = sqrt(16/15*(sigK^2-0.25*sigG^2));
alfa_2 = sqrt(16/15*(sigG^2-0.25*sigK^2));
x_0 = [muK; muG];
x_1 = alfa_1*[1; 0.5];
x_2 = alfa_2*[0.5; 1];

% Pce coefficients of theta
v_alpha_th = [x_0,x_1,x_2];
 
%C_check=(alfa_1*[1; 0.5])*transpose(alfa_1*[1; 0.5])+(alfa_2*[0.5; 1])*transpose(alfa_2*[0.5; 1]);
%display(C_check - C)

%% 4) The mappings from the reference random variables q (=germs) to parameters (RVs K and G)
% The map from q to theta (theta*sigK)
maps.germs2theta = @(xi) gpc_evaluate(v_alpha_th,V_th,xi);
%LC*q);
% And its inverse map, the theta field projected onto the eigenbasis
maps.theta2germs = @(th) (v_alpha_th(:,2:end)\(th-x_0));

% The map from the input parameter to the random field
maps.p2theta = @(p) (log(p));
% And the inverse map
maps.theta2p = @(th) (exp(th));

% The map from germs q to parameters
maps.germs2p = @(xi) (exp(maps.germs2theta(xi)));
% The inverse map: from input parameters to germs q
maps.p2germs = @(p) maps.theta2germs(log(p));
 
