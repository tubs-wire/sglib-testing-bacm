%% MCMC

%% 0) Initiate problem
% Initiate_updates_RV()
method = 'PCE_KF';
% number of samples
N = 10000;
% get surrogate model, prior, measurement and measurement error model
[v_alpha_th, V_th, maps, P, surr_model, z_m, E, true_vals] = Kalthoff_priorCovA_Forward();

%% Do the update
Y_func =@(xi)gpc_evaluate(surr_model.surr_model.v_alpha_u, surr_model.V_u, xi);
Xi = generate_stdrn_simparamset([1,1]);
xi2likelihood = @(xi)(E.pdf(z_m-Y(xi)));
[xi_j, acc_rate] = bayes_mcmc(xi2likelihood, Xi, N, [], Xi.mean);
p_j = maps.germ2p(xi_j);

%% Compute statistics

% mean of the posterior density
p_mean  = mean(p_j,2);

% variance of the posterior density
p_var = var(p_j,[], 2);

%% Show statistics
display(strvarexpand('Prior mean: $P.mean$'));
display(strvarexpand('Prior variance: $P.var$'));
display(strvarexpand('True value: $true_vals.p_true$'));
display(strvarexpand('Posterior mean: $p_mean$'));
display(strvarexpand('Posterior variance: p_var=$p_var$'));

%% Plot prior and posterior densities

% Plot densities

plot_grouped_scatter({P.sample(N), p_j, true_vals.p_true},...
    'Color', 'bcr', 'Legends', {'prior', 'posterior', 'true'}, 'Labels', P.param_names,...
  'FontSize', 12, 'Type', 'pdf', 'shifted', true);