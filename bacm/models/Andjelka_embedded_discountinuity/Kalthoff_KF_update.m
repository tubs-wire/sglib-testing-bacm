%% PCE based Kalman filter

%% 0) Initiate problem
% Initiate_updates_RV()
method = 'PCE_KF';
% number of samples for the pdf plots (this is only used for estimating the
% probability density of $F_{cM}$)
N = 10000;
% get surrogate model, prior, measurement and measurement error model
[v_alpha_th, V_th, maps, P, surr_model, z_m, E, true_vals] = Kalthoff_priorCovA_Forward();

%% 1) Write E in a PCE form.
% [q_i_alpha_1, V_q] = Q.gpc_expand()

[e_l_beta, V_e] = E.gpc_expand();
[e_l_beta_y, V_e_y] = E_y.gpc_expand();

%% 2) Generate a combined PCE basis

V_y = surr_model.V_u;
% First we bring the Qs to the PCE basis of Y_h
[V_th_y, Pr_V_th, Pr_V_y] = gpcbasis_combine(V_th, V_y, 'inner_sum', 'as_operators', true);
th_i_alpha = v_alpha_th*Pr_V_th;
y_i_alpha = surr_model.v_alpha_u*Pr_V_y;
 
% The combined basis of Theta, Y and E
[V_c, Pr_V_th_y, Pr_V_e] = gpcbasis_combine(V_th_y, V_e, 'outer_sum', 'as_operators', true);

%% 3) Rewrite the PCE coefficients in the extended basis
% Please, note, that the order of the basis functions are different here
% then the one used in the chapter and thus the coefficient matrices deviate from the one given in the chapter) 

th_i_gamma =   th_i_alpha*Pr_V_th_y;

z_j_gamma=y_i_alpha*Pr_V_th_y + e_l_beta*Pr_V_e;

zm_j_gamma = zeros(size(z_j_gamma));
zm_j_gamma(:, 1) = z_m;

%% 4) Compute the Kalman gain
%% 4a) Compute the covariance matrices

% Autocovariance of the measurable response
C_y=gpc_covariance(y_i_alpha, V_y);

% Covariance of $Q$ and $Y$
C_thy=gpc_covariance(th_i_alpha, V_y, y_i_alpha);

% Covariance of the measurement noise
C_e=gpc_covariance(e_l_beta, V_e);

%% 4b) Compute the Kalman gain

% Covariance of the measurment model
C_z = C_y+C_e;

% The Kalman gain
K = C_thy/C_z;

%% 5) Compute the coefficients of the updated input variables $\vec Q'$

th_p_i_gamma     = th_i_gamma       +K* (zm_j_gamma-z_j_gamma);

%% 5) Generate samples of the K and G
% sample from the PCE of the updated input parameter
th_j = gpc_sample(th_p_i_gamma, V_c, N);

C_p_i_gamma = gpc_covariance(th_p_i_gamma, V_c);


% map to the K and G
p_j = maps.th2p(th_j);

%% Compute statistics

% mean of the posterior density
p_mean  = mean(p_j,2);

% variance of the posterior density
p_var = var(p_j,[], 2);

%% Show statistics
display(strvarexpand('Prior mean: $P.mean$'));
display(strvarexpand('Prior variance: $P.var$'));
display(strvarexpand('True value: $true_vals.p_true$'));
display(strvarexpand('Posterior mean: $p_mean$'));
display(strvarexpand('Posterior variance: p_var=$p_var$'));

%% Plot prior and posterior densities

% Plot densities

plot_grouped_scatter({P.sample(N), p_j, true_vals.p_true},...
    'Color', 'bcr', 'Legends', {'prior', 'posterior', 'true'}, 'Labels', P.param_names,...
  'FontSize', 12, 'Type', 'pdf', 'shifted', true);