function [param_V, param_a, param_av]= get_eigenfreq_and_damping(Time, V_TAS, alpha)

% The function fits the signal V_TAS (Time) and alpha(Time)
% as linear combination of two decaying oscillatory motions (eigenmotion)
% in the form
%        V_TAS (Time)=A*u_ph   ; 
%        alpha(Time)=A*u_ph+B*u_sp
% where:
% -u_ph is the phugoid motion:
%         u_ph=exp(alpha_ph*t+i omega_ph*t)
%- u_sp is the short period motion:
%         u_sp=exp(alpha_sp*t+i omega_sp*t) 
% with  t=Time
% 
% Output:
% -param_V=[omega_ph_V; alpha_ph_V; L2_err_V]
%       (parameters and rel. L2 error estimated from V_TAS)
% -param_a=[omega_ph_a; alpha_ph_a; omega_sp_a; alpha_sp_a; L2_err_a]
%       (parameters and rel. L2 error estimated from ALPHA)
% -param_av=[omega_ph; alpha_ph]
%       (parameters estimated from the avaraged value)

%% Get parameters for true air speed (V_TAS)
n=5;
[~, alpha_ph_V, omega_ph_V, ~, L2_err_V] = find_params(Time, V_TAS, 'do_opt', true, 'n', n);
%y_est_speed = ampl_est * exp(-alpha_est*Time).*sin(omega_est*Time+phi_est);

param_V=[omega_ph_V; alpha_ph_V; L2_err_V];

%% Get parameters for angle of attack (alpha) in two phase for the two eigenmotions
n=3000; %Cut off beginning of signal, till this element
[ampl1_est, alpha1_est, omega1_est, phi1_est, ~] = find_params(Time, alpha, 'n', n, 'do_opt', true);
y1_est = ampl1_est * exp(-alpha1_est*Time).*sin(omega1_est*Time+phi1_est);

i=1:1500;
n=5;
[ampl2_est, alpha2_est, omega2_est, phi2_est, ~] = find_params(Time(i), alpha(i)-y1_est(i),'n', n,  'do_opt', true);
%y2_est = ampl2_est * exp(-alpha2_est*Time).*sin(omega2_est*Time+phi2_est);
%y_est_alpha = y1_est + y2_est;

%% L2 fit alpha response
    p0=[ampl1_est; alpha1_est; omega1_est; phi1_est; ampl2_est; alpha2_est; omega2_est; phi2_est];
    [omega_ph_a, alpha_ph_a, omega_sp_a, alpha_sp_a, L2_err_a]=do_optim(Time(5:end)-Time(5), alpha(5:end), p0);
    
 
param_a=[omega_ph_a; alpha_ph_a; omega_sp_a; alpha_sp_a; L2_err_a];

%% Calculate weighted omegas and alphas    
    
% Weights from the reciproc of the L2 error
       wV=1/L2_err_V;
       wa=1/L2_err_a;
       sum_w= wV+ wa;
% Avaraged frequency and decaying ratio
       omega_ph=(omega_ph_a*wa +omega_ph_V*wV)/sum_w;
       alpha_ph=(alpha_ph_a*wa +alpha_ph_V*wV)/sum_w;
       param_av=[omega_ph; alpha_ph];
end

function [omega1, alpha1, omega2, alpha2, L2_error]=do_optim(Time, alpha, p0)
    func = @(p)(optimfunc_dupl(Time, alpha, p(1), p(2), p(3), p(4), p(5), p(6), p(7), p(8)));
        H0=eye(8);
        newton_opts.abstol = 1e-12;
        newton_opts.verbosity = 0;
        [p,~,~] = minfind_quasi_newton(func, p0, H0, newton_opts);
        ampl1 = p(1);
        alpha1 = p(2);
        omega1 = p(3);
        phi1 = p(4);
        ampl2= p(5);
        alpha2 = p(6);
        omega2 = p(7);
        phi2 = p(8);
     alpha_est=ampl1* exp(-alpha1 * Time).*sin(omega1 * Time + phi1)+ampl2* exp(-alpha2 * Time).*sin(omega2 * Time+ phi2);
    L2_error= norm(alpha-alpha_est)/norm(alpha);
end