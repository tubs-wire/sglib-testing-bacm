function [f, df] = optimfunc_dupl1(x, y, ampl1, alpha1, omega1, phi1, ampl2, alpha2, omega2, phi2)

dy = y - ampl1* exp(-alpha1 * x).*sin(omega1 * x + phi1)-ampl2* exp(-alpha2 * x).*sin(omega2 * x + phi2);
f = sum(dy.*dy);
dfdampl1 = 2 * dot(dy, -exp(-alpha1 * x).*sin(omega1 * x + phi1));
%dfdalpha1 = 2 * dot(dy, ampl1*x.*exp(-alpha1 * x).*sin(omega1 * x + phi1));
%dfdomega1 = 2 * dot(dy, -ampl1*x.*exp(-alpha1 * x).*cos(omega1 * x + phi1));
dfdphi1 = 2 * dot(dy, -ampl1*exp(-alpha1 * x).*cos(omega1 * x + phi1));
dfdampl2 = 2 * dot(dy, -exp(-alpha2 * x).*sin(omega2 * x + phi2));
dfdalpha2 = 2 * dot(dy, ampl2*x.*exp(-alpha2 * x).*sin(omega2 * x + phi2));
dfdomega2 = 2 * dot(dy, -ampl2*x.*exp(-alpha2 * x).*cos(omega2 * x + phi2));
dfdphi2 = 2 * dot(dy, -ampl2*exp(-alpha2 * x).*cos(omega2 * x + phi2));

%df = [dfdampl1; dfphi1; dfdampl2; dfdalpha2; dfdomega2; dfdphi2];

df = [dfdampl1; dfdphi1; dfdampl2; dfdalpha2; dfdomega2; dfdphi2];
