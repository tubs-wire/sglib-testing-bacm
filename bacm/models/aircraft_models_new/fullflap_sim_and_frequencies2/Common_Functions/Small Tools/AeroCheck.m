close all
clear all
clc

alpha = (-8/180*pi):.01:(10/180*pi);
% alpha = 0.32142 / 180 * pi;
Lift.C_A0     =  0.33856;
Lift.C_AFRa   =  5.1576;
Lift.C_Aa_H   =  4.0503;
Lift.C_Aeta   =  1.845; %0.51144;
Lift.dC_AdFL  =  [0       3.395       3.395        5.26];
Lift.dC_AadFL =  [0.45045     0.44856     0.44864      0.4454];
Lift.a_1         = 200;                                   % a1 : static stall characteristics of the airfoil
Lift.alpha_star  = 21 * pi/180;                          % alpha star [rad]
Lift.tau_2star   = 3.4;                                 % time constant coefficient 
Drag.C_W0    =  0.018918;
Drag.k1      =  -0.015923;
Drag.k2      =  0.061058;
Drag.dC_WdX =  0.4;                   % neither provided by Fischenberg, IB 111-97/31 nor by DATCOM
Downwash.deps_H_dC_S = 0;
Downwash.deps_H_da   = 0.32267;        
Downwash.depsH_dX    = -0.0;        % from: Fischenberg, IB 111-97/31 (not provided by DATCOM)

SH = 24;
S = 92;
eta = 0;
FlapSetting = 1;
i_H = -7.1272/180*pi;
alphadot = -1;
l_mu = 3.428;
V_TAS = 46,61;

X = 1/2 * (1-tanh(Lift.a_1*(alpha-Lift.tau_2star*(l_mu/V_TAS)*alphadot- Lift.alpha_star) ))* 1
plot(alpha*180/pi,X)
eps_H = Downwash.deps_H_da*alpha + Downwash.depsH_dX*(1-X);
eps_H_lin = Downwash.deps_H_da*alpha + Downwash.depsH_dX;
alpha_H = alpha + i_H - eps_H;
alpha_H_lin = alpha + i_H - eps_H_lin;

C_AH =    Lift.C_Aa_H*alpha_H + Lift.C_Aeta * eta;
C_AH_lin =    Lift.C_Aa_H*alpha_H_lin + Lift.C_Aeta * eta;
alpha_H = alpha_H *180 / pi;
for j = 1:4
    for i = 1:length(X)
        CA(i,j)  =   (Lift.C_A0 + Lift.C_AFRa*((1+sqrt(X(i))/2)^2)*alpha(i))...
               +    SH/S*C_AH(i)...
               +    Lift.dC_AdFL(j)...
               +    Lift.dC_AadFL(j)*alpha(i);
        CW(i,j)  = Drag.C_W0 + Drag.k1*CA(i,j) + Drag.k2*CA(i,j)^2 + Drag.dC_WdX * (1-X(i));
        CA_lin(i,j)  =   (Lift.C_A0 + Lift.C_AFRa*alpha(i))...
               +    SH/S*C_AH_lin(i)...
               +    Lift.dC_AdFL(j)...
               +    Lift.dC_AadFL(j)*alpha(i);
        CW_lin(i,j)  = Drag.C_W0 + Drag.k1*CA(i,j) + Drag.k2*CA(i,j)^2;

    end
end

% 

% figure
% plot(alpha_H*180/pi),hold on
% plot(alpha*180/pi),grid on

figure
for j = 1:4
    plot(alpha*180/pi,CA(:,j)), grid on, hold on
    plot(alpha*180/pi,CA_lin(:,j)), grid on, hold on
end
% figure
% for j = 1:4
%     plot(alpha*180/pi,CW(:,j)), grid on, hold on
% end
