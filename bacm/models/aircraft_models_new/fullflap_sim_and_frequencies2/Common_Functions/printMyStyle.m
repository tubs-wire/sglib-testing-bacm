function printMyStyle(Name,Path)
% function printMyStyle(Name,Path)
% 
% Function to create images and PDF in a defined format
% for use after finalisation of the figure
% 
% Additional figure settings:
%               'Name',Name,...
%               'PaperOrientation','landscape',...
%               'PaperUnits','centimeters', ...
%               'Paperposition',[0 1 28.7 20])
% 
% Output-Properties *.tiff:     '-dtiffn', 
%               Resolution:     '-r600', 
%               Renderer:       '-painters'
% 
% Output-Properties  *.pdf:     '-dpdf', 
%               Resolution:     '-r600', 
%               Renderer:       '-painters'
% 
% Output-Path:  [Path  Name]
%
% Created by:   Jobst H. Diekmann 17-Feb-2012


if ~exist(Path, 'dir')
    mkdir (  Path);
end
set   (  gcf,...
        'Name',Name,...
        'PaperOrientation','landscape',...
        'PaperUnits','centimeters', ...
        'Paperposition',[0 1 28.7 20])
% print (  gcf,'-dtiffn', '-r200', '-painters', [Path  Name] )
% print (  gcf,'-dpdf', '-r200', '-painters', [Path  Name] )
print (  gcf,'-dpng', '-r200', '-painters', [Path  Name] )
end