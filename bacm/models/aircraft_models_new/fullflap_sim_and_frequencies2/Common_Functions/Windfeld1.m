%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2004      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      Windfeld1                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Create Look-Up Tables in Workspace for Dynamic Wind Field Simulation                    *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : C160_Gear_Init                                                                            *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Carmen K�hler                * 15-APR-05 * Basic Design                                            *
%* Christian Raab               * 19-JUL-05 * Adapted to C160 Simulation Structure                    *
%******************************************************************************************************

function [WIND5] = Windfeld1

% Abstand zwischen zwei Punkten mit der Grosskreisformel
D2R = pi/180;                                                         % deg --> rad
Radius = 6371;                                                        % Erdradius [km]
POSL=[]; POSB=[]; EAST=[]; NORTH=[];
POSLm=[]; POSBm=[]; EASTm=[]; NORTHm=[];

DH = 1020;                                                            % Luftdruck HOCH [hpa]
DT = 980;                                                             % Luftdruck TIEF [hpa]


% Lage des Tiefs und Hochs [rad]
LT = -20.1 *D2R;                                                      % L�ngengrad Tief
BT = 60.1 *D2R;                                                       % Breitengrad Tief
LH = 30.1 *D2R;                                                       % L�ngengrad Hoch 
BH = 40.1 *D2R;                                                       % Breitengrad Hoch


% Lage des Tiefs und Hochs [grad]
lt = -20;
bt = 60;
lh = 30;
bh = 40;


% Berechnung der Windgeschwindigkeit
Abstand = Radius*acos(sin(BT)*sin(BH) + cos(BT)*cos(BH)*cos(LH-LT)); % Abstand HOCH-->TIEF mit GROSSKREISFORMEL [km]
dA5hpa = Abstand/(DH-DT) * 5;                                        % 5hpa Abstand [km]
                                                                     % Vwind = 1e3* 1/dA5hpa, % absolute Windgeschwindigkeit [kts]
dA = [100 200 300 400 500 600];                                      % Abstand der 5hpa Isobaren
Vwind = [44 30 25 18 13 5 ];                                         % Windgeschwindigkeit in knoten 
Vwind = interp1( dA, Vwind, dA5hpa);                                 % interpolieren nach dem Abstand 

for posB= [35:2:65] * D2R
    
for posL= [-30:2:40] *D2R
    
AbstandT = Radius*acos(sin(BT)*sin(posB) + cos(BT)*cos(posB)*cos(posL-LT));
AbstandH = Radius*acos(sin(posB)*sin(BH) + cos(posB)*cos(BH)*cos(LH-posL));

if abs(AbstandT) <= abs(AbstandH)
    
    RX = Radius*acos(sin(BT)*sin(BT) + cos(BT)*cos(BT)*cos(posL-LT)) .* sign(posL-LT); % 
    RY = Radius*acos(sin(BT)*sin(posB) + cos(BT)*cos(posB)) .* sign(posB-BT);
    
else
    RX = Radius*acos(sin(posB)*sin(posB) + cos(posB)*cos(posB)*cos(LH-posL)) .* sign(LH-posL); % 
    RY = Radius*acos(sin(posB)*sin(BH) + cos(posB)*cos(BH)) .* sign(BH-posB);
    
end
    East = -RY ./ sqrt(RX.^2+RY.^2) * Vwind;                         % normiert * Vwind
    North = RX ./ sqrt(RX.^2+RY.^2) * Vwind;                         % normiert * Vwind        


POSL= [POSL,posL];
POSB= [POSB,posB];
EAST= [EAST,East];
NORTH= [NORTH,North];

end,

POSLm= [POSLm;POSL]; POSL= [];
POSBm= [POSBm;POSB]; POSB=[];
EASTm= [EASTm;EAST]; EAST=[];
NORTHm= [NORTHm;NORTH]; NORTH=[];

end,

% Matrix bilden
WIND5.POSL= POSLm(1,:)/D2R;                                           % 1. Spalte
WIND5.POSB= POSBm(:,1)/D2R;                                           % 1. Zeile
WIND5.EAST= EASTm;                                                    % Ost Daten
WIND5.NORTH= NORTHm;                                                  % Nord Daten
