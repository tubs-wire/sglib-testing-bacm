% -----------------------------------------------------------------------------
function plotfunc(Namen,Daten,TrimPointInfo)
% -----------------------------------------------------------------------------


[m, n] = size(Daten);
PlotNo = n - 1;

Path = 'Plots';

if ~exist(Path, 'dir')
    mkdir (  Path);
end

% % Bildlage auf dem Papier (Angaben in cm)
% Rand_links =  2.0;
% Rand_unten =  3.0;
% Bildbreite = 23.7;
% Bildhoehe  = 15.0;
% Paper_pos  = [Rand_links Rand_unten Bildbreite Bildhoehe];

% Oeffnen des Bildes 
% figure('PaperOrientation', 'landscape',   'PaperType',     'a4letter', ...
%        'PaperUnits',       'centimeters', 'PaperPosition', Paper_pos);
figure
time = Daten(:,1);
xmax = max(time);
xmin = min(time);
  
for i = 1:PlotNo
  ymax = max(Daten(:,i+1));
  ymin = min(Daten(:,i+1));
  dy   = abs(ymax-ymin);
  if dy<1e-4  
    dy=1.0;
  end
  ymax = ymax + 0.1*dy;
  ymin = ymin - 0.1*dy;
  subplot(ceil((PlotNo)/2),2,i)
  plot(time, Daten(:,i+1), 'k-','linewidth', 1.5);
%   if i == 2 || i == 4 || i == 6 || i == 8
%       axis([xmin, xmax, -1, 1]);
%   elseif i == 10
%       axis([xmin, xmax, 89, 91]);
%   else
%       axis([xmin, xmax, ymin, ymax]);
%   end
  grid on
%   if strcmp(Namen(i+1),'Alt [m] ')     
%       set(gca, 'YLimMode','auto','XTick',[0 100 200 300 400 500])%,'YTick',[10580 10620])
%   elseif strcmp(Namen(i+1),'C_\mu')
%       set(gca, 'YLim',[0.042 0.043],'XTick',[0 100 200 300 400 500])%,'YTick',[0.042 0.043])
%   elseif strcmp(Namen(i+1),'V_T_A_S [m/s]  ')
%       set(gca, 'YLim',[42.7 43.3],'XTick',[0 100 200 300 400 500])%,'YTick',[218 10620])
%   elseif strcmp(Namen(i+1),'q [�/s]     ')
%       set(gca, 'YLimMode','manual','XTick',[0 100 200 300 400 500],'YTick',[-0.6 -0.3 0])
%   elseif strcmp(Namen(i+1),'\theta [�]  ')
%       set(gca, 'YLimMode','manual','XTick',[0 100 200 300 400 500],'YTick',[2.2 2.6 3])
%   else
%       set(gca, 'YLimMode','manual','XTick',[0 100 200 300 400 500])
%   end
  set(gca, 'linewidth', 1.5,'FontSize', 20)
  ylabel(Namen(i+1,:), 'FontSize', 20)
  if   i==PlotNo % ||i==PlotNo-1
    xlabel(Namen(1,:), 'FontSize', 20)
  end
end
timestamp = clock;
set(gcf, 'Color', [1 1 1])
savename = ['Result_' date '_' num2str(timestamp(4)) '_' num2str(timestamp(5))];
% printMyStyle(savename,Path)
diary([Path '\' savename '_Info'])
diary on
    TrimPointInfo
diary off 

% print (gcf,'-dtiff', '-r500', '-opengl', [Path '\' savename] )


