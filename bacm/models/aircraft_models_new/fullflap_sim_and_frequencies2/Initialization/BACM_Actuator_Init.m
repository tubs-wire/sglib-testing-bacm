%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                       BACM_Actuator_Init                                           *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize workspace variables for the actuator subsystem.                              *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Actuator_Init                                                                        *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************
function Actuator = BACM_Actuator_Init

% Control Command Booster
Booster.Aileron.Delay       = 0.09;         % Transport delay for roll command signal               [s]
Booster.Aileron.Gain        = 1;            % Roll booster gain                                   [rad]
Booster.Aileron.Damping     = 0.7;          % Roll booster damping                                  [-]
Booster.Aileron.omega_0     = 60;           % Roll booster eigenfrequency                       [rad/s]

Booster.Elevator.Delay      = 0.09;         % Transport delay for pitch command signal              [s]
Booster.Elevator.Gain       = 1;            % Pitch booster gain                                  [rad]
Booster.Elevator.Damping    = 0.7;          % Pitch booster damping                                 [-]
Booster.Elevator.omega_0    = 60;           % Pitch booster eigenfrequency                      [rad/s]

Booster.Rudder.Delay        = 0.09;         % Transport delay for yaw command signal                [s]
Booster.Rudder.Gain         = 1;            % Yaw booster gain                                    [rad]
Booster.Rudder.Damping      = 0.7;          % Yaw booster damping                                   [-]
Booster.Rudder.omega_0      = 60;           % Yaw booster eigenfrequency                        [rad/s]

Booster.Stabilizer.Delay    = 0.09;         % Transport delay for yaw command signal                [s]
Booster.Stabilizer.Gain     = 1;            % Stabilizer booster gain                             [rad]
Booster.Stabilizer.Damping  = 0.7;          % Stabilizer booster damping                            [-]
Booster.Stabilizer.omega_0  = 60;           % Stablizer  booster eigenfrequency                 [rad/s]

% Moving Actuators
Mov_Act.Spoiler.Delay           = 0.09;     % Transport delay for spoiler actuation                 [s]
Mov_Act.Spoiler.Gain            = 1;        % Spoiler command signal gain                           [-]
Mov_Act.Spoiler.Time_Const      = 0.5;      % Time const. for spoiler actuation PT_1 behavior       [s]
Mov_Act.Spoiler.Max             =  60*pi/180; % Max. spoiler command signal value                 [rad]
Mov_Act.Spoiler.Min             = -60*pi/180; % Min. spoiler command signal value                 [rad]
Mov_Act.Speed_Brake.Delay       = 0.5;      % Transport delay for speed brake actuation             [s]
Mov_Act.Speed_Brake.Gain        = 1;        % Speed brake command signal gain                       [-]
Mov_Act.Speed_Brake.Time_Const  = 0.5;      % Time const. for speed brake actuation PT_1 behavior   [s]
Mov_Act.Speed_Brake.Max         = 1;        % Max. speed brake command signal value                 [-]
Mov_Act.Speed_Brake.Min         = 0;        % Min. speed brake command signal value                 [-]
Mov_Act.Flaps.Delay             = 0.5;      % Transport delay for flap actuation                    [s]
Mov_Act.Flaps.Gain              = 1;        % Flap command signal gain                              [-]
Mov_Act.Flaps.Time_Const        = 0.5; %2;  % Time const. for flap actuation PT_1 behavior          [s]
Mov_Act.Flaps.Max               = 65 * pi/180;% Max. flap command signal value                    [rad]
Mov_Act.Flaps.Min               = 0;        % Min. flap command signal value                      [rad]
Mov_Act.Ramp.Delay              = 0.5;      % Transport delay for ramp door actuation               [s]
Mov_Act.Ramp.Gain               = 1;        % Ramp door command signal gain                         [-]
Mov_Act.Ramp.Time_Const         = 5;        % Time const. for ramp door actuation PT_1 behavior     [s]
Mov_Act.Ramp.Max                = 1;        % Max. ramp command signal value                        [-]
Mov_Act.Ramp.Min                = 0;        % Min. ramp command signal value                        [-]
Mov_Act.Gear.Delay              = 0.5;      % Transport delay for gear actuation                    [s]
Mov_Act.Gear.Gain               = 1;        % Gear command signal gain                              [-]
Mov_Act.Gear.Time_Const         = 2;        % Time const. for gear actuation PT_1 behavior          [s]
Mov_Act.Gear.Max                = 1;        % Max. gear command signal value                        [-]
Mov_Act.Gear.Min                = 0;        % Min. gear command signal value                        [-]

% Surface Deflection Limits
% Aileron Deflection Limits
Surfaces.Aileron.max       =  60 * pi/180;     % Max. Aileron deflection angle                    [rad]
Surfaces.Aileron.min       = -60 * pi/180;     % Min. Aileron deflection angle                    [rad]
Surfaces.Aileron.rate_max  =  30 * pi/180;     % Max. rate of Aileron deflection                [rad/s]
Surfaces.Aileron.rate_min  = -30 * pi/180;     % Min. rate of Aileron deflection                [rad/s]

% Elevator Deflection Limits
Surfaces.Elevator.max       =  20 * pi/180; % Max. Elevator deflection angle                      [rad]
Surfaces.Elevator.min       = -25 * pi/180; % Min. Elevator deflection angle                      [rad]
Surfaces.Elevator.rate_max  =  30 * pi/180; % Max. rate of Elevator deflection                  [rad/s]
Surfaces.Elevator.rate_min  = -30 * pi/180; % Min. rate of Elevator deflection                  [rad/s]

% Stabilizer Deflection Limits
Surfaces.Stabilizer.max       =  90 * pi/180;   % Max. Stabilizer deflection angle                 [rad]
Surfaces.Stabilizer.min       = -90 * pi/180;   % Min. Stabilizer deflection angle                 [rad]
Surfaces.Stabilizer.rate_max  =  5 * pi/180;   % Max. Rate of Stabilizer Deflection             [rad/s]
Surfaces.Stabilizer.rate_min  = -5 * pi/180;   % Min. Rate of Stabilizer Deflection             [rad/s]

% Rudder Deflection Limits
Surfaces.Rudder.max       =  25 * pi/180;      % Max. Rudder deflection angle                     [rad]
Surfaces.Rudder.min       = -25 * pi/180;      % Min. Rudder deflection angle                     [rad]
Surfaces.Rudder.rate_max  =  30 * pi/180;      % Max. rate of Rudder deflection                 [rad/s]
Surfaces.Rudder.rate_min  = -30 * pi/180;      % Min. rate of Rudder deflection                 [rad/s]

% Aileron-Spoiler Connection
Surfaces.Aileron_In  = [-12 -5 5 12]*pi/180;   % Look-Up Table Aileron Input                      [rad]
Surfaces.Spoiler_Out = [-60  0 0 60]*pi/180;   % Look-Up Table Spoiler Output                     [rad]

% Gear Actuators
Booster.Gear.Time        = 5;                        % Time for full retraction of Gear              [s]
Booster.NG_Eta.Delay     = 0.09;                     % Transport delay for NG_Eta command signal     [s] 
Booster.NG_Eta.Gain      = 1;                        % NG_Eta booster gain                           [-]
Booster.NG_Eta.Damping   = 0.7;                      % NG_Eta booster damping                        [-]
Booster.NG_Eta.omega_0   = 60;                       % NG_Eta booster eigenfrequency             [rad/s]
Surfaces.NG_Eta.max      =  55 * pi/180;             % Max. Nosegear deflection angle              [rad]
Surfaces.NG_Eta.min      = -55 * pi/180;             % Min. Nosegear deflection angle              [rad] 
Surfaces.NG_Eta.rate_max =  55 * pi/180;             % Max. Nosegear deflection angle              [rad]
Surfaces.NG_Eta.rate_min = -55 * pi/180;             % Min. Nosegear deflection angle              [rad] 

BLC = load ('BLC_Data.mat');
Surfaces.BLC = BLC.BLC;


% Assemble actuator tructure
Actuator.Booster   = Booster;
Actuator.Mov_Act   = Mov_Act;
Actuator.Surfaces  = Surfaces;

%---------------------------------------------------------------------------------------------------EOF