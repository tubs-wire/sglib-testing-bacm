%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Sim_Init                                                 *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the simulation parameters.                                                  *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Sim_Init                                                                             *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************

function Sim = BACM_Sim_Init

% Initialize Simulation Parameter
Sim.StartTime   = 0.0;                            % Simulation Start Time                       [s]
Sim.EndTime     = inf;                            % Simulation End Time                         [s]
Sim.SampleTime  = 0.001;                          % Simulation Sample Time                      [s]
Sim.Boost_Flag  = 1;                              % Set [1] for Actuator Booster On             [-]
                                                  % Set [0] for Actuator Booster Off            [-]												  												  
% Time Delay Parameters
Sim.Time_Delay.V_min       = 10^(-9); %15;                  % Set Min. Vel. for Downwash Time Delay     [m/s]                                  
Sim.Time_Delay.Aero_check  = 0;

% Zero Threshold Parameters
Sim.Display_Zero_Threshold = 1.0;                 % Velocity threshold for Instrument Disp.   [m/s]
Sim.Div_Zero_Saturation    = 0.001;               % Min. Saturation to Avoid Division by Zero   [-]
%---------------------------------------------------------------------------------------------------EOF