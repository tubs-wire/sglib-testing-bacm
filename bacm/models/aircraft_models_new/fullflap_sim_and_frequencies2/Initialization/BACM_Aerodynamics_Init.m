%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      SFB880_Aerodynamics_Init                                      *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the look-up tables for the aerodynamic coefficients of the SFB880 A/C model. *
%*            A template file is replaced by the values generated with DATCOM.                        *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : Aircraft Name: SFB880_Konfig_28_2_2012                                                      *
%*                                                                                                    *
%*            file created : 2012-02-28 time: 15:19:21                                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : SFB880_Aerodynamics_Init                                                                  *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%* Andre Thurm                  * 23-NOV-10 * generated template files                                *
%******************************************************************************************************
function Aero = BACM_Aerodynamics_Init

% Aerodynamic Database
load AeroCoefficientsNLv01.mat
load AeroCoefficientsMidFlap.mat
load SystemFailure.mat
load Prop_Slip_Aero.mat
load Lateral_Motion.mat
load DroopNose_3D-CFD.mat

% Wind Tunnel Paramters 
Wind_Tunnel_Ma = 0.15;                                                  % Prandtl-Glauert   [-]

% Flaps Setting
Flaps.Setting = [0  30  65] * pi/180;    

% Lookup Table Inputs
Inputs.alpha        = Lateral_Coefficients.Large_VTP.alpha;
Inputs.beta         = Lateral_Coefficients.Large_VTP.beta;
Inputs.p_star       = Lateral_Coefficients.Large_VTP.p_star;
Inputs.r_star       = Lateral_Coefficients.Large_VTP.r_star;
Inputs.zeta         = Lateral_Coefficients.Large_VTP.zeta;
Inputs.xis          = Lateral_Coefficients.Large_VTP.xis;
Inputs.xia          = Lateral_Coefficients.Large_VTP.xia;
Inputs.Lat_Motion   = 1;                                                % Lateral Motion on/off [1/0]

% Lift Parameters
Lift.C_A0           =  AeroCoefficientsNL.CL0;
Lift.C_AFRa         =  AeroCoefficientsNL.CLa;
Lift.C_Aa_H         =  AeroCoefficientsNL.CLa_HTP; % 4.5554;
Lift.C_Aeta         =  AeroCoefficientsNL.CLeta_HTP; 
Lift.dC_AdFL        =  [0   MidFlap.dCL0        AeroCoefficientsNL.dCA0_Flaps ];
Lift.dC_AadFL       =  [0   MidFlap.dCLa        AeroCoefficientsNL.dCAa_Flaps ];
Lift.dC_AdCmu_BLC   =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.BLC.dCL_dCmu ];
Lift.dC_AadCmu_BLC  =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.BLC.dCLa_dCmu ];
Lift.dC_Aa2dCmu_BLC =  [0   MidFlap.Cmu.CL.k1   0];
Lift.dC_AdCmu_CC    =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.CC.dCL_dCmu ];
Lift.dC_AadCmu_CC   =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.CC.dCLa_dCmu ];
Lift.dC_Aa2dCmu_CC  =  [0   MidFlap.Cmu.CL.k1   0];
Lift.dC_A_DN        =  DroopNose.dCL_DN;
Lift.dC_Aa_DN       =  DroopNose.dCLa_DN;
Lift.dC_AdRD        =  0;
Lift.dC_AdSB        =  [0 0 0]; 
Lift.dC_AdLG        =  0; 
Lift.h_BO           =  0;                                    % Starting Ground Effect height             [m]
Lift.dC_A_dh        =  [0 0 0];
Lift.alphamax       =  [15 14 4.5] * pi/180;
Lift.alpha_wfc_max  =  20 * pi/180;
% Lift.alpha_HTP_max  =  inf * pi/180;
% Lift.alpha_HTP_min  = -inf * pi/180;
Lift.alpha_HTP_max  =  AeroCoefficientsNL.a_max_HTP_deg * pi/180;
Lift.alpha_HTP_min  = -AeroCoefficientsNL.a_max_HTP_deg * pi/180;
% Lateral Motion Influences on Lift
Lift.C_Aab          =  Lateral_Coefficients.Large_VTP.CL.CLab * 0;
Lift.f_C_Aab2       =  Lateral_Coefficients.Large_VTP.CL.f_CLab2 * 0;
Lift.C_Ab2          =  Lateral_Coefficients.Large_VTP.CL.CLb2 * 0;
Lift.C_Arb           =  Lateral_Coefficients.Large_VTP.CL.CLrb * 0;
Lift.C_Ap2          =  Lateral_Coefficients.Large_VTP.CL.CLp2 * 0;
Lift.C_Azeta2       =  Lateral_Coefficients.Large_VTP.CL.CLzeta2;
Lift.C_Aksis        =  Lateral_Coefficients.Large_VTP.CL.CLxis;
Lift.C_Aksi2        =  Lateral_Coefficients.Large_VTP.CL.CLxia2 * 0;

% Unsteady Lift
% Reference values (Cmu = 0.033)
Lift.f_Ref      = AeroCoefficientsNL.f_ref;                                   % a1 : static stall characteristics of the airfoil
Lift.a1_Ref     = AeroCoefficientsNL.a1_ref;                                 % time constant coefficient 
Lift.astar_Ref  = AeroCoefficientsNL.astar_ref;                          % alpha star [rad]
% BLC Cmu Increments
Lift.df_dcmu_BLC        = AeroCoefficientsNL.BLC.df_dcmuBLC ;                                   % a1 : static stall characteristics of the airfoil
Lift.da1_dcmu_BLC       = AeroCoefficientsNL.BLC.da1_dcmuBLC ;                                 % time constant coefficient 
Lift.dastar_dcmu_BLC    = AeroCoefficientsNL.BLC.dastar_dcmuBLC ;                          % alpha star [rad]
% CC Cmu Increments
Lift.df_dcmu_CC         = AeroCoefficientsNL.CC.df_dcmuCC;                                   % a1 : static stall characteristics of the airfoil
Lift.da1_dcmu_CC        = AeroCoefficientsNL.CC.da1_dcmuCC;                                 % time constant coefficient 
Lift.dastar_dcmu_CC     = AeroCoefficientsNL.CC.dastar_dcmuCC;                          % alpha star [rad]
% DroopNose increments to reference (Cmu = 0.033)
Lift.df_DN      = DroopNose.dfL_DN;                                   % a1 : static stall characteristics of the airfoil
Lift.da1_DN     = DroopNose.da1L_DN;                                 % time constant coefficient 
Lift.dastar_DN  = DroopNose.dastarL_DN;                          % alpha star [rad]
% DroopNose factors to reference (Cmu = 0.033)
Lift.Ff_DN      = 1 + (DroopNose.dfL_DN     /AeroCoefficientsNL.f_ref);                                   % a1 : static stall characteristics of the airfoil
Lift.Fa1_DN     = 1 + (DroopNose.da1L_DN    /AeroCoefficientsNL.a1_ref);                                 % time constant coefficient 
Lift.Fastar_DN  = 1 + (DroopNose.dastarL_DN /AeroCoefficientsNL.astar_ref);                          % alpha star [rad]

% Mid Flaps
Lift.MidFlap.f1         = MidFlap.Stall.CL.f1;                                   % a1 : static stall characteristics of the airfoil
Lift.MidFlap.a1         = MidFlap.Stall.CL.a1;                                 % time constant coefficient 
Lift.MidFlap.astar      = MidFlap.Stall.CL.aStar;                          % alpha star [rad]

% Aileron not dropped in Mid Flap Position
Aileron0.CL.dCL0       = MidFlap.Ail0.CL.dCL0;
Aileron0.CL.dCLa       = MidFlap.Ail0.CL.dCLa;
Aileron0.CL.f1         = MidFlap.Ail0.CL.f1;
Aileron0.CL.a1         = MidFlap.Ail0.CL.a1;
Aileron0.CL.astar      = MidFlap.Ail0.CL.astar;

Aileron0.CD.dCD0       = MidFlap.Ail0.CD.dCD0;
Aileron0.CD.dCDa       = MidFlap.Ail0.CD.dCDa;
Aileron0.CD.f1         = MidFlap.Ail0.CD.f1;
Aileron0.CD.a1         = MidFlap.Ail0.CD.a1;
Aileron0.CD.astar      = MidFlap.Ail0.CD.astar;

Aileron0.Cm.dCm0       = MidFlap.Ail0.Cm.dCm0;
Aileron0.Cm.dCma       = MidFlap.Ail0.Cm.dCma;
Aileron0.Cm.f1         = MidFlap.Ail0.Cm.f1;
Aileron0.Cm.a1         = MidFlap.Ail0.Cm.a1;
Aileron0.Cm.astar      = MidFlap.Ail0.Cm.astar;

Aileron0.NotDropped    = 0;

% Drag Parameters
Drag.C_W0           =   AeroCoefficientsNL.CD0;
Drag.C_W0_HTP       =   AeroCoefficientsNL.CD0_HTP;
Drag.k1             =   AeroCoefficientsNL.k1;
Drag.k2             =   AeroCoefficientsNL.k2;
Drag.dC_WdFL        =  [0   MidFlap.dCD0                AeroCoefficientsNL.dCD_Flaps ];        
Drag.dC_Wk2dFL      =  [0   MidFlap.dCk2                AeroCoefficientsNL.dk2];        
Drag.dC_Wk1dFL      =  [0  -AeroCoefficientsNL.k1      -AeroCoefficientsNL.k1 ];        
Drag.dC_WdCmuBLC    =  [0   0                           AeroCoefficientsNL.BLC.dCD_dCmu];        
Drag.dC_WdCmuCC     =  [0   0                           AeroCoefficientsNL.CC.dCD_Cmu];        
Drag.dC_Wk2dCmuBLC  =  [0   0                           AeroCoefficientsNL.BLC.dk_dCmu];        
Drag.dC_Wk2dCmuCC   =  [0   0                           AeroCoefficientsNL.CC.dk_dCmu];
Drag.dC_W0_DN       =   DroopNose.dCD_DN;                                   
Drag.dC_Wk1_DN      =   DroopNose.dk1_DN;                                   
Drag.dC_Wk2_DN      =   DroopNose.dk2_DN;                                   
Drag.dC_WdRD        =   0;                                   
Drag.dC_WdSB        =  [0 0 0];                       
Drag.dC_WdLG        =   0;          
Drag.dC_W_dh        =  [0 0 0];
% Lateral Motion Influences on Drag
Drag.C_Wab          =   Lateral_Coefficients.Large_VTP.CD.CDab * 0;
Drag.f_C_Wab2       =   Lateral_Coefficients.Large_VTP.CD.f_CDab2 * 0;
Drag.C_Wb2          =   Lateral_Coefficients.Large_VTP.CD.CDb2 * 0;
Drag.C_Wr           =   Lateral_Coefficients.Large_VTP.CD.CDr;
Drag.C_Wp2          =   Lateral_Coefficients.Large_VTP.CD.CDp2;
Drag.C_Wpb          =   Lateral_Coefficients.Large_VTP.CD.CDpb;
Drag.C_Wzeta2       =   Lateral_Coefficients.Large_VTP.CD.CDzeta2 * 0;
Drag.C_Wzetab       =   Lateral_Coefficients.Large_VTP.CD.CDzetab * 0;
Drag.C_Wksis        =   Lateral_Coefficients.Large_VTP.CD.CDxis;
Drag.C_Wksisb       =   Lateral_Coefficients.Large_VTP.CD.CDxisb * 0;
Drag.C_Wksi2        =   Lateral_Coefficients.Large_VTP.CD.CDxia2;
Drag.C_Wksib        =   Lateral_Coefficients.Large_VTP.CD.CDxiab * 0;



% Pitch Parameters 
Pitch.C_m0          = AeroCoefficientsNL.Cm0 ;   
Pitch.C_maFR        = AeroCoefficientsNL.Cma ;   
Pitch.C_mqFR        = AeroCoefficientsNL.CmqFR ; 
Pitch.C_mqFRdRD     = 0;
Pitch.dC_mdFL       = [0    MidFlap.dCm0        AeroCoefficientsNL.dCm_Flaps  ];
Pitch.dC_madFL      = [0    MidFlap.dCma        AeroCoefficientsNL.dCma_Flaps ];
Pitch.dC_mdCmuBLC   = [0    MidFlap.Cmu.Cm.k3   AeroCoefficientsNL.BLC.dCm_dCmu ];
Pitch.dC_madCmuBLC  = [0    MidFlap.Cmu.Cm.k2   AeroCoefficientsNL.BLC.dCma_dCmu];
Pitch.dC_ma2dCmuBLC = [0    MidFlap.Cmu.Cm.k1   0];
Pitch.dC_mdCmuCC    = [0    MidFlap.Cmu.Cm.k3   AeroCoefficientsNL.CC.dCm_dCmu ];
Pitch.dC_madCmuCC   = [0    MidFlap.Cmu.Cm.k2   AeroCoefficientsNL.CC.dCma_dCmu];
Pitch.dC_ma2dCmuCC  = [0    MidFlap.Cmu.Cm.k1   0];
Pitch.dC_m_DN       = DroopNose.dCm_DN;
Pitch.dC_ma_DN      = DroopNose.dCma_DN;
Pitch.dC_mdSB       = [0 0 0]; 
Pitch.dC_mdRD       = 0;
Pitch.C_mqFRdLG     = 0; 
Pitch.dC_m_dh       = [0 0 0];
% Lateral Motion Influences on Pitch
Pitch.C_mab         = Lateral_Coefficients.Large_VTP.Cm.Cmab * 0;   
Pitch.f_C_mab2      = Lateral_Coefficients.Large_VTP.Cm.f_Cmab2 * 0;
Pitch.C_mb2         = Lateral_Coefficients.Large_VTP.Cm.Cmb2 * 0;   
Pitch.C_mr          = Lateral_Coefficients.Large_VTP.Cm.Cmr;
Pitch.C_mrb         = Lateral_Coefficients.Large_VTP.Cm.Cmrb * 0;
Pitch.C_mp2         = Lateral_Coefficients.Large_VTP.Cm.Cmp2;
Pitch.C_mpb         = Lateral_Coefficients.Large_VTP.Cm.Cmpb * 0;
Pitch.C_mzeta2      = Lateral_Coefficients.Large_VTP.Cm.Cmzeta2;
Pitch.C_mzetab      = Lateral_Coefficients.Large_VTP.Cm.Cmzetab;
Pitch.C_mksis2      = Lateral_Coefficients.Large_VTP.Cm.Cmxis2;
Pitch.C_mksisb      = Lateral_Coefficients.Large_VTP.Cm.Cmxisb * 0;
Pitch.C_mksi        = Lateral_Coefficients.Large_VTP.Cm.Cmxia;
Pitch.C_mksib       = Lateral_Coefficients.Large_VTP.Cm.Cmxiab * 0;

% Unsteady Pitching Moment
% Reference values (Cmu = 0.033)
Pitch.f_Ref_Cm         = AeroCoefficientsNL.f_ref_Cm ;                                   % a1 : static stall characteristics of the airfoil
Pitch.a1_Ref_Cm        = AeroCoefficientsNL.a1_ref_Cm;                                 % time constant coefficient 
Pitch.astar_Ref_Cm     = AeroCoefficientsNL.astar_ref_Cm;                          % alpha star [rad]
% BLC Cmu Inkrements
Pitch.df_dcmu_BLC_Cm        = AeroCoefficientsNL.BLC.df_dcmuBLC_Cm;                                   % a1 : static stall characteristics of the airfoil
Pitch.da1_dcmu_BLC_Cm       = AeroCoefficientsNL.BLC.da1_dcmuBLC_Cm;                                 % time constant coefficient 
Pitch.dastar_dcmu_BLC_Cm    = AeroCoefficientsNL.BLC.dastar_dcmuBLC_Cm;                          % alpha star [rad]
% CC Cmu Inkrements
Pitch.df_dcmu_CC_Cm         = AeroCoefficientsNL.CC.df_dcmuCC_Cm;                                   % a1 : static stall characteristics of the airfoil
Pitch.da1_dcmu_CC_Cm        = AeroCoefficientsNL.CC.da1_dcmuCC_Cm;                                 % time constant coefficient 
Pitch.dastar_dcmu_CC_Cm     = AeroCoefficientsNL.CC.dastar_dcmuCC_Cm;                          % alpha star [rad]
% DroopNose increments to reference (Cmu = 0.033)
Pitch.df_DN      = DroopNose.dfm_DN;                                   % a1 : static stall characteristics of the airfoil
Pitch.da1_DN     = DroopNose.da1m_DN;                                 % time constant coefficient 
Pitch.dastar_DN  = DroopNose.dastarm_DN;                          % alpha star [rad]
% DroopNose factors to reference (Cmu = 0.033)
Pitch.Ff_DN      = 1 + (DroopNose.dfm_DN     /AeroCoefficientsNL.f_ref_Cm);                                   % a1 : static stall characteristics of the airfoil
Pitch.Fa1_DN     = 1 + (DroopNose.da1m_DN    /AeroCoefficientsNL.a1_ref_Cm);                                 % time constant coefficient 
Pitch.Fastar_DN  = 1 + (DroopNose.dastarm_DN /AeroCoefficientsNL.astar_ref_Cm);                          % alpha star [rad]

% Mid Flaps
Pitch.MidFlap.f1            = MidFlap.Stall.Cm.f1;                                   % a1 : static stall characteristics of the airfoil
Pitch.MidFlap.a1            = MidFlap.Stall.Cm.a1;                                 % time constant coefficient 
Pitch.MidFlap.astar         = MidFlap.Stall.Cm.aStar;                          % alpha star [rad]

% Roll Parameters
Roll.C_l0       = 0;
Roll.C_lbdRD    = 0;
Roll.C_lb       = Lateral_Coefficients.Large_VTP.Cl.Clb1;
Roll.C_lb2      = Lateral_Coefficients.Large_VTP.Cl.Clb2;
Roll.C_lb3      = Lateral_Coefficients.Large_VTP.Cl.Clb3;
Roll.C_lba      = Lateral_Coefficients.Large_VTP.Cl.Clba * 0;
Roll.C_lp       = Lateral_Coefficients.Large_VTP.Cl.Clp;
Roll.C_lpa      = 0;
Roll.C_lpb      = Lateral_Coefficients.Large_VTP.Cl.Clpb * 0;
Roll.f_C_lpb2   = Lateral_Coefficients.Large_VTP.Cl.f_Clpb2 * 0;
Roll.C_lr       = Lateral_Coefficients.Large_VTP.Cl.Clr;
Roll.C_lra      = 0;
Roll.C_lrdRD    = 0;
Roll.C_lksi     = Lateral_Coefficients.Large_VTP.Cl.Clxia;          %   -0.26;               
Roll.C_lksib    = Lateral_Coefficients.Large_VTP.Cl.Clxiab * 0;         %   -0.26;               
Roll.C_lzeta    = -Lateral_Coefficients.Large_VTP.Cl.Clzeta;        %   0.058;              
Roll.f_C_lzetaa = -Lateral_Coefficients.Large_VTP.Cl.f_Clzetaa * 0;   	%   0.058;              
Roll.C_lzetaa   = -Lateral_Coefficients.Large_VTP.Cl.Clzetaa * 0;       %   0.058;              
Roll.C_ldSP     = [0 0 0]; 


% Yaw Parameters  
Yaw.C_n0        = 0;
Yaw.C_nbdRD     = 0;
Yaw.C_nbdLG     = 0;
Yaw.C_nb        = Lateral_Coefficients.Large_VTP.Cn.Cnb1; % 0.14903;
Yaw.C_nb3       = Lateral_Coefficients.Large_VTP.Cn.Cnb3; 
Yaw.C_nba       = Lateral_Coefficients.Large_VTP.Cn.Cnba * 0; 
Yaw.C_np        = Lateral_Coefficients.Large_VTP.Cn.Cnp;
Yaw.C_npa       = Lateral_Coefficients.Large_VTP.Cn.Cnpa * 0;
Yaw.C_npdRD     = 0;
Yaw.C_nr        = Lateral_Coefficients.Large_VTP.Cn.Cnr;
Yaw.C_nra       = 0;
Yaw.C_nrdRD     = 0;
Yaw.C_nksi      = Lateral_Coefficients.Large_VTP.Cn.Cnxia; 
Yaw.C_nksia     = Lateral_Coefficients.Large_VTP.Cn.Cnxiaa * 0; 
Yaw.C_nzeta     = Lateral_Coefficients.Large_VTP.Cn.Cnzeta;
Yaw.C_nzetab    = Lateral_Coefficients.Large_VTP.Cn.Cnzetab * 0;
Yaw.C_ndSP      = [0 0 0];

% Side Force Parameters
Side.C_Y0       = 0;
Side.C_YbdRD    = 0;
Side.C_YbdLG    = 0;
Side.C_Yb       = Lateral_Coefficients.Large_VTP.CY.CYb1;
Side.C_Yb3      = Lateral_Coefficients.Large_VTP.CY.CYb3;
Side.C_Yab      = Lateral_Coefficients.Large_VTP.CY.CYab * 0;
Side.C_Yp       = Lateral_Coefficients.Large_VTP.CY.CYp;
Side.C_Ypa      = Lateral_Coefficients.Large_VTP.CY.CYpa * 0;
Side.f_C_Ypa    = Lateral_Coefficients.Large_VTP.CY.f_CYpa * 0;
Side.C_YpdRD    = 0;
Side.C_Yr       = Lateral_Coefficients.Large_VTP.CY.CYr;
Side.C_Yra      = Lateral_Coefficients.Large_VTP.CY.CYra * 0;                   
Side.f_C_Yra    = Lateral_Coefficients.Large_VTP.CY.f_CYra * 0;                   
Side.C_YrdRD    = 0;
Side.C_Yzeta    = Lateral_Coefficients.Large_VTP.CY.CYzeta;
Side.C_Yzetaa   = Lateral_Coefficients.Large_VTP.CY.CYzetaa * 0;
Side.f_C_Yzetaa = Lateral_Coefficients.Large_VTP.CY.f_CYzetaa * 0;
Side.C_Yzetab   = Lateral_Coefficients.Large_VTP.CY.CYzetab * 0;
Side.C_Yksi     = Lateral_Coefficients.Large_VTP.CY.CYxia;
Side.C_Yksia    = Lateral_Coefficients.Large_VTP.CY.CYxiaa * 0;
Side.f_C_Yksia  = Lateral_Coefficients.Large_VTP.CY.f_CYxiaa * 0;
Side.C_YdSP     = 0; 

% Downwash Parameters
Downwash.deps_H_dC_S    = 0;
Downwash.depsH_dX       = 0;
Downwash.epsCAFR0       = -1.6726   * pi/180; %AeroCoefficientsNL.epsCA0;
Downwash.depsdCAFR      = 3.416     * pi/180; %AeroCoefficientsNL.depsdCA; ; 
Downwash.depsfitBLC1    = AeroCoefficientsNL.BLC.depsfit(1);
Downwash.depsfitBLC2    = AeroCoefficientsNL.BLC.depsfit(2);
Downwash.depsCLfitBLC1  = AeroCoefficientsNL.BLC.depsCLfit(1); 
Downwash.depsCLfitBLC2  = AeroCoefficientsNL.BLC.depsCLfit(2); 
Downwash.depsfitCC1     = AeroCoefficientsNL.CC.depsfit(1);
Downwash.depsfitCC2     = AeroCoefficientsNL.CC.depsfit(2);
Downwash.depsCLfitCC1   = AeroCoefficientsNL.CC.depsCLfit(1); 
Downwash.depsCLfitCC2   = AeroCoefficientsNL.CC.depsCLfit(2); 

% Active HTP
Active_HTP.On               =   0;

Active_HTP.Setting          =  [0   30                  65] * pi/180;
Active_HTP.dC_AdFL          =  [0   MidFlap.dCL0        AeroCoefficientsNL.dCA0_Flaps ];
Active_HTP.dC_AadFL         =  [0   MidFlap.dCLa        AeroCoefficientsNL.dCAa_Flaps ];
Active_HTP.dC_AdCmu_BLC     =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.BLC.dCL_dCmu ];
Active_HTP.dC_AadCmu_BLC    =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.BLC.dCLa_dCmu ];
Active_HTP.dC_Aa2dCmu_BLC   =  [0   MidFlap.Cmu.CL.k1   0];
Active_HTP.dC_AdCmu_CC      =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.CC.dCL_dCmu ];
Active_HTP.dC_AadCmu_CC     =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.CC.dCLa_dCmu ];
Active_HTP.dC_Aa2dCmu_CC    =  [0   MidFlap.Cmu.CL.k1   0];

Active_HTP.Cmu_ref          =  [0   MidFlap.Cmu_ref     AeroCoefficientsNL.Cmu_ref];
Active_HTP.Performance      =  100; % HTP blowing performance in [%]

% Engine Influence Parameters
Prop.On             = 0;

Prop.dCL_Nacelle    = Prop_Slip_Aero.FullFLap.dCL0_Nacelle;
Prop.dCLa_Nacelle   = Prop_Slip_Aero.FullFLap.dCLa_Nacelle;
Prop.dCD_Nacelle    = Prop_Slip_Aero.FullFLap.dCD0_Nacelle;
Prop.dk_Nacelle     = Prop_Slip_Aero.FullFLap.dk_Nacelle;
Prop.dCm_Nacelle    = Prop_Slip_Aero.FullFLap.dCm0_Nacelle;
Prop.dCma_Nacelle   = Prop_Slip_Aero.FullFLap.dCma_Nacelle;

Prop.dCL_dT         = [Prop_Slip_Aero.MediFLap.dCL_dT   Prop_Slip_Aero.FullFLap.dCL_dT];
Prop.dCLa_dT        = [Prop_Slip_Aero.MediFLap.dCLa_dT  Prop_Slip_Aero.FullFLap.dCLa_dT];
Prop.dCD_dT         = [Prop_Slip_Aero.MediFLap.dCD_dT   Prop_Slip_Aero.FullFLap.dCD_dT];
Prop.dk_dT          = [Prop_Slip_Aero.MediFLap.dk_dT    Prop_Slip_Aero.FullFLap.dk_dT];
Prop.dCm_dT         = [Prop_Slip_Aero.MediFLap.dCm_dT   Prop_Slip_Aero.FullFLap.dCm_dT];
Prop.dCma_dT        = [Prop_Slip_Aero.MediFLap.dCma_dT  Prop_Slip_Aero.FullFLap.dCma_dT];

Engine.C_S_max      = 3;
Engine.C_S_min      = 0;

% Unsteady Aerodynamics Flag
Unsteady_Flag = 1;           % ON = 0 / OFF = 1

% alpha-step for calculation of dCl/da and dCm/da
dalpha = .01 *pi/180;           % ON = 0 / OFF = 1

% Alpha-Limits
% alpha.HTP.alpha_max =  inf * pi/180;    % identfiziert -18� aus TAU 3D-CFD 
% alpha.HTP.alpha_min = -inf * pi/180;    % identfiziert -18� aus TAU 3D-CFD
alpha.HTP.alpha_max =  AeroCoefficientsNL.a_max_HTP_deg * pi/180;    % identfiziert -18� aus TAU 3D-CFD 
alpha.HTP.alpha_min = -AeroCoefficientsNL.a_max_HTP_deg * pi/180;    % identfiziert -18� aus TAU 3D-CFD
alpha.WFC.alpha_max = -15 * pi/180;
alpha.WFC.alpha_min = -10 * pi/180;

% Cmu-Parameters
Cmu.Cmu_ref     = [0 MidFlap.Cmu_ref AeroCoefficientsNL.Cmu_ref];
Cmu.Cmu_ref_set = Cmu.Cmu_ref(3); % Full Flap Coeeficients
Cmu.MidFlapDev  = [0.015  0.005];
Cmu.FullFlapDev = [0.0086 0.012];
Cmu.Cmu_Var_Flg = 0;
Cmu.Cmu_Const   = 0.045;
Cmu.Cmu_Fail    = 0;

% Droop Nose Integration
Droop.Active    = 1;
Droop.Cmu       = [0.016    0.0245  0.0309  0.0356  0.0433  0.0517];
Droop.da_max    = [8        8       8       9.25    10.75   12.25 ];
Droop.fC_La     = [1.0472   1.0341  1.2210  1.4491  1.2179  6.4142];
Droop.fC_ma     = [0.6871   0.8613  0.8097  0.7431  0.6232  0.3308];

% System Failure
Fail.Deactivated  = 1;
Fail.alpha      = Failure.Flap65deg.alpha;
Fail.alpha_max  = Failure.Flap65deg.almax;
Fail.Flaps      = [0 30 65];
Fail.CL(:,3)    = Failure.Flap65deg.CL;
Fail.CD(:,3)    = Failure.Flap65deg.CD;
Fail.Cm(:,3)    = Failure.Flap65deg.Cm;

Fail.CL(:,2)    = Failure.Flap65deg.CL;
Fail.CD(:,2)    = Failure.Flap30deg.CD;
Fail.Cm(:,2)    = Failure.Flap30deg.Cm;

Fail.CL(:,1)    = Failure.Flap00deg.CL;
Fail.CD(:,1)    = Failure.Flap00deg.CD;
Fail.Cm(:,1)    = Failure.Flap00deg.Cm;

Fail.Time       = 10^9;
Fail.Time_Control = 10^9;

% Data Assembly
Aero.Wind_Tunnel_Ma = Wind_Tunnel_Ma;
Aero.Flaps          = Flaps;
Aero.Inputs         = Inputs;
Aero.Fail           = Fail;
Aero.Droop          = Droop;
Aero.Lift           = Lift;
Aero.Drag           = Drag;
Aero.Side           = Side;
Aero.Roll           = Roll;
Aero.Yaw            = Yaw;
Aero.Pitch          = Pitch;
Aero.Downwash       = Downwash;
Aero.Engine         = Engine;
Aero.Active_HTP     = Active_HTP;
Aero.Prop           = Prop;
Aero.Unsteady_Flag  = Unsteady_Flag;
Aero.dalpha         = dalpha;
Aero.alpha          = alpha;
Aero.Cmu            = Cmu;
Aero.Aileron0       = Aileron0; 
end
%--------------------------------------------------------------------------