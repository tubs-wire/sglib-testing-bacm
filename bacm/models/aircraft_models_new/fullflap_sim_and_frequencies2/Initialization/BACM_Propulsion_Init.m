%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_Propulsion_Init                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the workspace variables for the propulsion subsystem.                        *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Propulsion_Init                                                                      *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-06 * Basic Design                                            *
%******************************************************************************************************
function Propulsion = BACM_Propulsion_Init

% Engine Geometry Data
Geometry.S_Prop    = 12.5664;                       % Propeller disc aera.                                 [m^2]
Geometry.Sigma_T   = 0;                             % Tilt angles of the engines.                          [deg]

Geometry.LH_Inner.x_rp   = -9.639;                  % LH outer engine position x-coord.                      [m]
Geometry.LH_Inner.y_rp   = 4.534;                   % LH outer engine position y-coord.                      [m]
Geometry.LH_Inner.z_rp   = -0.853;                  % LH outer engine position z-coord.                      [m]

Geometry.RH_Inner.x_rp   = -9.639;                  % RH outer engine position x-coord.                      [m]
Geometry.RH_Inner.y_rp   = -4.534;               	% RH outer engine position y-coord.                      [m]
Geometry.RH_Inner.z_rp   = -0.853;                  % RH outer engine position z-coord.                      [m]


% Engine Thrust Look-Up Table Data
LT = load ('TP_Look_Up_REF2');                               % Load MAT-File with look-up table data 

% Engine Operation Mode
Mode = 7;

% One Engine Inoperative
OEI = 0;
BLCOP = 0;
% Engine Data
EngData.Fuel_consumption = 0;%0.3;  % Specific Fuel Consumption                      [(kg/h)/kW]

% Stopped Engine Parameters
Stopped.Drag        = 0;                % Drag coefficient of the stopped propellor              [-] 

% Engine Response Dynamics
Throttle.Max            =  1;           % Max. Throttle Command Signal Value                     [-]
Throttle.Min            =  -0.2;           % Min. Throttle Command Signal Value                     [-]
Throttle.Rate.Max       =  0.4;      % Max. Throttle change rate                            [1/s]
Throttle.Rate.Min       =  -0.4;      % Min. Throttle change rate                            [1/s]
Throttle.Damping        =  1.2;       % Damping coefficient                                    [-]
Throttle.Omega_0        =  1.5;       % Eigen angular frequency                              [1/s]

%Exist-Switch
Exist.LH_Outer          = 0.0;
Exist.LH_Inner          = 0.0;
Exist.RH_Inner          = 0.0;
Exist.RH_Outer          = 0.0;

Propulsion.Cmu_Var          = 1;                        % 1 = C_mu variable / 0 = C_mu constant
Propulsion.Thrust_Var       = 1;                        % 1 = Thrust variable / 0 = Thrust constant
Propulsion.Const_Cmu        = 0;                 % Constant value for C_mu                  [-]
Propulsion.Const_Thrust     = 0;% Constant value for Thrust                [N]
Propulsion.EngineGain       = 1;
Propulsion.Prop_Efficiency  = 1;
Propulsion.Prop_Eff_OEI     = 1;
Propulsion.Prop_Eff_Delay   = 0.5;
% Data Assembly
Propulsion.Mode                   = Mode;
Propulsion.OEI                    = OEI;
Propulsion.BLCOP                  = BLCOP;
Propulsion.Geometry               = Geometry;
Propulsion.LT                     = LT.Thrust_Look_up;
Propulsion.EngData                = EngData;
Propulsion.Stopped                = Stopped;
Propulsion.Throttle               = Throttle;
Propulsion.Exist                  = Exist;

%---------------------------------------------------------------------------------------------------EOF