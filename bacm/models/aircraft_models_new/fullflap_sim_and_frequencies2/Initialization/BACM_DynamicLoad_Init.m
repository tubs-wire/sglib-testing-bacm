%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BACM_DynamicLoad_Init                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *   
%*                                                                                                    *
%* Purpose  : Initialize workspace variables for the a cargo load inside the aircr                    *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Actuator_Init                                                                        *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-06 * Basic Design                                            *
%******************************************************************************************************
function DynLoad = BACM_DynamicLoad_Init


% Dynamic loads init
Load1.Mass    = 0;               % Load Mass                                               [kg]
Load1.CG.x_rp = 0;            % CG Position X-coord. in geomeric refrence frame          [m]      
Load1.CG.y_rp = 0;               % CG Position Y- coord. in geomeric refrence frame         [m]
Load1.CG.z_rp = 0;            % CG Position Z-coord. in geomeric refrence frame          [m]
Load1.Inertia.I_xx =  0.0;       % Inertia xx-component at load CG                      [kgm^2]
Load1.Inertia.I_yy =  0.0;       % Inertia yy-component at load CG                      [kgm^2]
Load1.Inertia.I_zz =  0.0;       % Inertia zz-component at load CG                      [kgm^2]
Load1.Inertia.I_xz =  0.0;       % Inertia xy-component at load CG                      [kgm^2]
Load1.Inertia.I_yx =  0.0;       % Inertia yz-component at load CG                      [kgm^2]
Load1.Inertia.I_zy =  0.0;       % Inertia zx-component at load CG                      [kgm^2]

% Data Assembly
DynLoad.Load1 = Load1;

end
%---------------------------------------------------------------------------------------------------EOF
