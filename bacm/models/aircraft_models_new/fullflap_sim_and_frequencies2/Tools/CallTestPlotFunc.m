%%%%%%%%%%%%%%%%%%%%%%%%%% commented and added by Noemi
slCharacterEncoding('windows-1252')
close_system 'SFB880_Library.mdl'
clear
close all 
clc
%addpath(genpath('D:\SVN-SFB880\Arbeitspakete\AP 1 Flugmechanische Simulation\Simulationsumgebung\Simulationsmodell\SFB 880 REF0-2013 Model MK1 - Uncertainty Freeze\Model 13-01-2014\'))

%%%%%%%%%%%%%%%%%%%%%%
% Go to root directory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%by Noemi
actual_path=pwd;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mfile = mfilename('fullpath');
cd([fileparts(fileparts(mfile)) filesep 'Tools']);

TestCase = 1;   
% These Testcases are already designed for the controller. The controller
% (a combination of AutoPilot and AutoThrottle) is currently switched off. 
% The Altitude and Speed errors will not affect anything. 
% The controller is designed to keep the aircraft on a required Glideslope of 3.5 deg 
% which is created by the Instrument Landing System (ILS) of the airport.
% For the eigenmotion analysis the controller has to remain switched off. 
% A horizontal flight test case is recommended for this analysis.    
% Available Testcases:  (FPA = Flight Path Angle)
% (1) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 850 m, AltError = -40 m, VError = 0 m/s   (Recommended) 
% (2) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 850 m, AltError = -40 m, VError = -5 m/s    
% (3) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 850 m, AltError = -40 m, VError = +5 m/s    
% (4) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 930 m, AltError = +35 m, VError = 0 m/s
% (4) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 930 m, AltError = +35 m, VError = -5 m/s
% (4) - Full Flap, Unaccelerated Horizontal Flight (FPA = 0 deg), V_TAS = 55 m/s, Altitude = 930 m, AltError = +35 m, VError = +5 m/s
% (7) - Full Flap, Unaccelerated Descent (FPA = -3.5 deg), V_TAS = 55 m/s, Altitude = 893 m, AltError = 0 m, VError = +5 m/s
% (8) - Full Flap, Unaccelerated Descent (FPA = -3.5 deg), V_TAS = 55 m/s, Altitude = 893 m, AltError = 0 m, VError = -5 m/s
% (9) - Full Flap, Unaccelerated Descent (FPA = -3.5 deg), V_TAS = 55 m/s, Altitude = 893 m, AltError = 0 m, VError = 0 m/s
                

UncertaintyFactor.CL0    = 1; 
UncertaintyFactor.CLa    = 1;
UncertaintyFactor.CD0    = 1;
UncertaintyFactor.CDi    = 1;
UncertaintyFactor.Cm0    = 1;
UncertaintyFactor.Cma    = 1;
UncertaintyFactor.Cmu    = 1;

StateBias       = [     0           % u Bias     [m/s]
                        0           % w Bias     [m/s]
                        20*pi/180   % q Bias     [rad/s]
                        0];         % Theta Bias [rad]

ShowCheckPlot   = 1;
TestName        = 'TestRun';
TestNo          = 1;
TestDuration    = 10;

evalc('DLR_SFB880_Trim_Quat');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%by Noemi
% if ~strcmp(get_param('DLR_SFB880_Trim_Quat','SimulationStatus'),'stopped')
%     
%     set_param('DLR_SFB880_Trim_Quat', 'SimulationCommand', 'stop');
%     
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Result          = Test_Plot_Func(TestCase, UncertaintyFactor, StateBias, ShowCheckPlot, TestName, TestNo, TestDuration)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%by Noemi
% if ~strcmp(get_param('DLR_SFB880_Trim_Quat','SimulationStatus'),'stopped')
%     
%     set_param('DLR_SFB880_Trim_Quat', 'SimulationCommand', 'stop');
%     
% end
cd(actual_path)
%%%%%%%%%%%%%%%%%%%%%%%%% EOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%