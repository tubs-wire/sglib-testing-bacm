function Controller = build_controller(PID_Gains, Overall)

Controller.PID_Gains            = PID_Gains;% [P I D] Gains
s                               = tf('s');
Controller.TF_PID               =   Controller.PID_Gains(1) +...
                                    Controller.PID_Gains(2) / s +...
                                    Controller.PID_Gains(3) * s;
[Controller.num_PID,...
 Controller.den_PID]            = tfdata(Controller.TF_PID,'v');
Controller.Overall_Gain         = [Overall];                            % Overall        

end