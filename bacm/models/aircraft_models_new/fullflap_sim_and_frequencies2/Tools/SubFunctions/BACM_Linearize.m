%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2010      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                         BACM_Linearize                                             *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Linearize the BACM aircraft model.                                                      *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%* Sub-Functions :                                                                                    *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax :  BACM_Linearize.m                                                                         *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date     *                     Description                       *
%******************************************************************************************************
%* Dominik Niedermeier          * 25-FEB-10 * Adapted to new trim / lin BACM workbench                *
%* �zg�r Atesoglu               * 26-APR-10 * State trans. for quat. -> Euler angles and              *
%*                                            state trans. for body  -> wind frame vel. are added.    *
%*                                            The state, output and input orders are revised.         *    
%* �zg�r Atesoglu               * 10-MAY-10 * Incremental body acceleration outputs (ay,az) are added *
%*                                            to the model and corresponding state, output and        *
%*                                            input orders are revised.                               * 
%* Christian Raab               * 04-JUN-10 * Adapted linearization routine to new output vector      *
%* �zg�r Atesoglu               * 31-JAN-11 * Open loop plant simulations and plots are arranged      *
%* Dominik Niedermeier          * 07-FEB-11 * Actuator states are added to the A-Matrix               *
%*                                            Evalin-Command for setting of trim flag before          *
%*                                            simulation is added                                     * 
%******************************************************************************************************

function [A B C D] = BACM_Linearize(BACM,BACM_Trim,TrimSettings)

% Define the trim point
x_tr=BACM_Trim.Init.X0;
u_tr=BACM_Trim.Init.U0;
y_tr=BACM_Trim.Init.Y0;

% Name definitions
X_Name=BACM_Trim.Names.X_Name;
U_Name=BACM_Trim.Names.U_Name;
Y_Name=BACM_Trim.Names.Y_Name;

%******************************************************************************************************
% Linearization
%******************************************************************************************************
% % Precompile the model
% feval(TrimSettings.MdlName, [], [], [],'compile');

%Assembly of x and u at trim point
x_u = [x_tr; u_tr];

%Index definition of the states and inputs used to linearise the system
%The matrix Jaco contains the subsystems defined by the index i_d and i_y
n_x = length(x_tr);                % Number of states
n_u = length(u_tr);                % Number of inputs         
n_y = length(y_tr);                % Number of outputs
i_x = [1:n_x]';                    % Identify States (Column Vector)
i_u = [1:n_u]';                    % Identify Inputs (Column Vector)
itv = [i_x; i_u+n_x];              % Assembly of index vector for states and inputs
i_d = [1:n_x]';                    % Identify Derivatives
i_y = [1:n_y]';                    % Identify outputs
itr = [i_d;i_y+n_x];               % Assembly of index vector for derivatives and outputs                 
n_u = size(i_u,1);                 % All inputs selected 
n_y = size(i_y,1);                 % All outputs selected ("one" means number of columns)
n_x = size(i_x,1);                 % All states selected

feval (TrimSettings.MdlName, [], [], [], 'compile');

%Calling jj_lin
jaco = jj_lin(TrimSettings.MdlName,x_u,n_x,itv,itr);

feval (TrimSettings.MdlName, [], [], [], 'term');
% "jaco" matrix is defined as

        % | xdot| | A | B | | x |
        % |     |=|---|---|*|   |
        % | y   | | C | D | | u |

% Try several times to release the model
% try
% feval(TrimSettings.MdlName,[],[],[],'term');
% feval(TrimSettings.MdlName,[],[],[],'term');
% feval(TrimSettings.MdlName,[],[],[],'term');
% end

% Extract the matrices of the linear system
% Extract A-matrix
for i=1:n_x
    for j=1:n_x
        A(i,j)=jaco(i,j);
    end
end

% Extract B-matrix
for i=1:n_x
    for j=n_x+1:n_x+n_u
        B(i,j-n_x)=jaco(i,j);
    end
end

% Extract C-matrix
for i=n_x+1:n_x+n_y
    for j=1:n_x
        C(i-n_x,j)=jaco(i,j);
    end
end

% Extract D-matrix
for i=n_x+1:n_x+n_y
    for j=n_x+1:n_x+n_u
        D(i-n_x,j-n_x)=jaco(i,j);
    end
end

%% Reduce System Matrix
further_Calc = 0;

if further_Calc
    % % Extract the configuration
    % flapslat_set = (BACM_Trim.U0.Flaps + BACM_Trim.U0.Slats) * 180/pi;
    % flapslat_set = floor(flapslat_set);
    % CONF         = interp1([0 18 28 37 42 67], BACM.Aerodynamics.Config, flapslat_set);
    % 
    % Calculate Cm_alpha_dot, M_alpha_dot and M_w_dot
    % depdal      = interp1(BACM.Aerodynamics.Config,BACM.Aerodynamics.Lift.DEPDAL,CONF);
    % CA_alh      = interp1(BACM.Aerodynamics.Config,BACM.Aerodynamics.Lift.CAALH,CONF);
    % fxCA_alh    = interp1(BACM.Aerodynamics.Config,BACM.Aerodynamics.Lift.fxCAALH,CONF);
    % pgCA_alh    = interp1(BACM.Aerodynamics.Config,BACM.Aerodynamics.Lift.pgCAALH,CONF);

    if BACM_Trim.Init.Y0(14) <= 0.033
        depdal      = ( BACM.Aerodynamics.Downwash.depsdCAFR ...
                    +   BACM.Aerodynamics.Downwash.depsCLfitBLC1 * BACM_Trim.Init.Y0(14) ...
                    +   BACM.Aerodynamics.Downwash.depsCLfitBLC2 * BACM_Trim.Init.Y0(14).^2) ...
                    * ( BACM.Aerodynamics.Lift.C_AFRa ...
                    +   BACM.Aerodynamics.Lift.dC_AadFL(3) ...
                    +   BACM.Aerodynamics.Lift.dC_AadCmu_BLC(2) * BACM_Trim.Init.Y0(14) ...
                    +   BACM.Aerodynamics.Lift.dC_Aa_DN);
    else
        depdal      = ( BACM.Aerodynamics.Downwash.depsdCAFR ...
                    +   BACM.Aerodynamics.Downwash.depsCLfitCC1 * BACM_Trim.Init.Y0(14) ...
                    +   BACM.Aerodynamics.Downwash.depsCLfitCC2 * BACM_Trim.Init.Y0(14).^2) ...
                    * ( BACM.Aerodynamics.Lift.C_AFRa ...
                    +   BACM.Aerodynamics.Lift.dC_AadFL(3) ...
                    +   BACM.Aerodynamics.Lift.dC_AadCmu_CC(2) * BACM_Trim.Init.Y0(14) ...
                    +   BACM.Aerodynamics.Lift.dC_Aa_DN);
    end

    CA_alh      = BACM.Aerodynamics.Lift.C_Aa_H;
    % fxCA_alh    = BACM.Aerodynamics.Lift.fxCAALH;
    Ma          = BACM_Trim.Init.Y0(15);
    pgCA_alh    = sqrt(1-0.15^2)/sqrt(1-Ma^2);
    q_bar       = BACM_Trim.Init.Y0(25);
    qS          = BACM_Trim.Init.Y0(25)*BACM.Configuration.Geometry.S;
    VTAS        = BACM_Trim.Init.Y0(1);
    xcg_rp      = BACM_Trim.Weight_Balance.AC_Empty.CG.x_rp;
    I_yy        = BACM_Trim.Weight_Balance.AC_Empty.Inertia.I_yy;

    CA_alh_eff      = CA_alh /(sqrt(1-(pgCA_alh*Ma^2)));
    delta_x_cg_rp   = xcg_rp - BACM.Configuration.Geometry.x_rp;
    r_H             = BACM.Configuration.Geometry.x_LH + delta_x_cg_rp;
    Cm_aldot        = -CA_alh_eff*(BACM.Configuration.Geometry.S_H/BACM.Configuration.Geometry.S)...
                    *((BACM.Configuration.Geometry.x_LH*r_H)/BACM.Configuration.Geometry.l_mac^2)...
                    *depdal;
    M_aldot         = q_bar*BACM.Configuration.Geometry.S*BACM.Configuration.Geometry.l_mac^2/...
                    (I_yy*VTAS)*Cm_aldot;
    M_wdot          = M_aldot/VTAS;

    % Define the states of the linear model from X

    %  1 - u_K_b                        [m/s]
    %  2 - v_K_b                        [m/s]
    %  3 - w_K_b                        [m/s]
    %  4 - q_0                            [-]
    %  5 - q_1                            [-]
    %  6 - q_2                            [-]
    %  7 - q_3                            [-]
    % 13 - q_K_b                      [rad/s]
    % 14 - p_K_b                      [rad/s]
    % 15 - r_K_b                      [rad/s]
    % 19 - Stabilizer_Boost             [rad]
    % 21 - Elevator_Boost               [rad]
    % 22 - Rudder_Boost                 [rad]
    % 23 - Aileron_Boost                [rad]
    % 26 - Aileron_Boost_dot          [rad/s]
    % 27 - Roll_Spoiler                 [rad]
    % 28 - Elevator_Boost_dot         [rad/s]
    % 31 - Rudder_Boost_dot           [rad/s]
    % 32 - Stabilizer_Boost_dot       [rad/s]

    IX = [1 2 3 14 13 15 4 5 6 7 23 26 19 32 21 28 22 31];

    % Define the outputs of the linear model from Y

    %  1 - V_TAS      [m/s]
    %  2 - alpha_A    [rad]
    %  3 - q_K_b      [rad/s]
    %  4 - theta      [rad]
    %  5 - Alt        [m]
    %  6 - beta_A     [rad]
    %  7 - phi        [rad]
    %  8 - psi        [rad]
    %  9 - p_K_b      [rad/s]
    % 10 - r_K_b      [rad/s]
    % 11 - gamma      [rad]
    % 12 - VIAS       [m/s]
    IY = [12 2 6 9 3 10 7 4 8 11];                                               

    % Define the inputs of the linear model from U

    %  4  - Elevator                  [rad]
    %  5  - Flaps                     [rad]
    %  6  - Stabilizer                [rad]
    %  7  - Speed Brake                 [-]

    IU = [1 2 4 3 6 5];

    % Extract the state space matrices
    Acoup = A(IX,IX);
    Bcoup = B(IX,IU); 
    Ccoup = C(IY,IX);
    Dcoup = D(IY,IU);
    
    X_Name_Reduced = {X_Name{IX}}
    Y_Name_Reduced = {Y_Name{IY}}
    U_Name_Reduced = {U_Name{IU}}

    % Determine E-Matrix to include M_w_dot effect: E*xdot = A*x+B*u
    E = eye(length(IX),length(IX));

    % E matrix (q,wdot) entry is corrected wrt the new order of the states
    E(5,3) = -M_wdot;
    Acoup = inv(E)*Acoup;
    Bcoup = inv(E)*Bcoup;

    % State space transformation for the states 
    % (u,v,w) -> (V,alpha,beta) and (q0,q1,q2,q3) -> (phi,theta,psi)
    % The orthonormal transformation between Euler angles and quaternions [CEq,CqE]
    % Calculate the Jacobian for quaternion to Euler transformation
    CEq  = Ccoup(7:9,7:10);
    % Calculate the Jacobian for Euler to quaternion transformation
    phi   = BACM_Trim.Init.Y0(7);
    theta = BACM_Trim.Init.Y0(4);
    psi   = BACM_Trim.Init.Y0(8);
    CqE   = 0.5*[-cos(theta/2)*sin(phi/2)*cos(psi/2)+cos(phi/2)*sin(theta/2)*sin(psi/2),...
                 -cos(phi/2)*sin(theta/2)*cos(psi/2)+sin(phi/2)*sin(psi/2)*cos(theta/2),...
                 -sin(psi/2)*cos(phi/2)*cos(theta/2)+sin(phi/2)*sin(theta/2)*cos(psi/2);...
                  cos(phi/2)*cos(theta/2)*cos(psi/2)+sin(phi/2)*sin(theta/2)*sin(psi/2),...
                 -sin(phi/2)*sin(theta/2)*cos(psi/2)-sin(psi/2)*cos(phi/2)*cos(theta/2),...
                 -sin(phi/2)*sin(psi/2)*cos(theta/2)-cos(phi/2)*sin(theta/2)*cos(psi/2);...
                 -sin(phi/2)*sin(theta/2)*cos(psi/2)+sin(psi/2)*cos(phi/2)*cos(theta/2),...
                  cos(phi/2)*cos(theta/2)*cos(psi/2)-sin(phi/2)*sin(theta/2)*sin(psi/2),...
                 -cos(phi/2)*sin(theta/2)*sin(psi/2)+cos(theta/2)*sin(phi/2)*cos(psi/2);...
                 -sin(phi/2)*sin(psi/2)*cos(theta/2)-cos(phi/2)*sin(theta/2)*cos(psi/2),...
                 -cos(phi/2)*sin(theta/2)*sin(psi/2)-cos(theta/2)*sin(phi/2)*cos(psi/2),...
                  cos(phi/2)*cos(theta/2)*cos(psi/2)+sin(phi/2)*sin(theta/2)*sin(psi/2)];
    % The orthonormal transformation between body and wind frame velocity [Cwb,Cbw]
    % Calculate the Jacobian for body to wind transformation
    Cwb = Ccoup(1:3,1:3);
    % Ccalculate the Jacobian for wind to body transformation
    Cbw = inv(Cwb);
    % Calculate the orthonormal transformation matrices
    Cf  = [    Cwb    ,  0*eye(3,3), 0*eye(3,4),0*eye(3,8);
            0*eye(3,3),    eye(3,3), 0*eye(3,4),0*eye(3,8);
            0*eye(3,3),  0*eye(3,3),   CEq     ,0*eye(3,8);
            0*eye(8,3),  0*eye(8,3), 0*eye(8,4),  eye(8,8)];
    Ci  = [    Cbw    ,  0*eye(3,3), 0*eye(3,3),0*eye(3,8);
            0*eye(3,3),    eye(3,3), 0*eye(3,3),0*eye(3,8);
            0*eye(4,3),  0*eye(4,3),    CqE    ,0*eye(4,8);
            0*eye(8,3),  0*eye(8,3), 0*eye(8,3),  eye(8,8)];
    % Calculate the transformed state space matrices
    % ydot = Cf*xdot = Cf*A*Ci*y + Cf*B*u
    % y    = C*Ci*y + D*u
    Acoup = Cf*Acoup*Ci;
    Bcoup = Cf*Bcoup;
    Ccoup = Ccoup*Ci;

    % Sweep the matrices to enhance the minimum realizations
    eps = 1E-9;
    [imax,jmax] = size(Acoup); 
    for i=1:imax,for j=1:jmax,if abs(Acoup(i,j))<=eps,Acoup(i,j) = 0;end;end;end
    [imax,jmax] = size(Bcoup);
    for i=1:imax,for j=1:jmax,if abs(Bcoup(i,j))<=eps,Bcoup(i,j) = 0;end;end;end
    [imax,jmax] = size(Ccoup);
    for i=1:imax,for j=1:jmax,if abs(Ccoup(i,j))<=eps,Ccoup(i,j) = 0;end;end;end


    % Definition of coupled state-space system
    sys = ss(Acoup,Bcoup,Ccoup,Dcoup);

    % Attain state names to the linear state space
    set(sys,'inputname',  {U_Name{IU}},...
            'outputname', {Y_Name{IY}},...
            'statename ', {Y_Name{IY(1:end)},X_Name{IX(end-6:end)}});

    % Linear and nonlinear system comparison
    dt      = 0.01; 
    tf      = 5; 
    t_final = 2*tf;
    t1      = [0:dt:tf]'; 
    t2      = [tf+dt:dt:t_final]'; 
    t       = [t1;t2];

    % Arrange inputs to the nonlinear plant
    u_trim = repmat(BACM_Trim.Init.U0',length(t),1);
    u_nlin = zeros(length(t),13);

    % Elevator input deflection
    de          = 1 / 180*pi; 
    u_nlin(:,1) = -[de*ones(length(t1),1);-de*ones(length(t2),1)];

    % Aileron input deflection
    da          = 1 / 180*pi; 
    u_nlin(:,3) = +[da*ones(length(t1),1);-da*ones(length(t2),1)];
    u_nlin(:,4) = -u_nlin(:,3);
    u_nlin      = u_nlin + u_trim;

    % Nonlinear simulation
    % Set the trim flag to zero
    evalin('base','BACM.Sim.Trim_Flag = 0;');
    % Reassign the actuator parameters
    evalin('base','BACM.Actuator = BACM_Actuator_Init;');
    opt         = simset('Solver','ode4','FixedStep',dt);
    [t,x,y]     = sim(TrimSettings.MdlName,t_final,opt,[t,u_nlin]);

    % Linear simulation
    % Arrange inputs to the linear plant
    u_lin       = zeros(length(t),19);
    % Linear model aileron input
    u_lin(:,3)  = +[da*ones(length(t1),1);-da*ones(length(t2),1)];
    u_lin(:,4)  = -u_lin(:,3);
    % Linear model elevator input
    u_lin(:,5)  =  -[de*ones(length(t1),1);-de*ones(length(t2),1)];
    y_lin       = lsim(sys,u_lin,t);
    y_trim      = repmat(BACM_Trim.Init.Y0(IY)',length(t),1);
    y_lin       = y_lin + y_trim;

    % Plot linear and nonlinear simulation results
    scrsz = get(0,'ScreenSize');
    set(0,'DefaultLineLineWidth',2);
    figure('Name','Longitudinal Plane of Motion','NumberTitle','on',...
           'Position',[scrsz(3)/2 scrsz(4)/10 scrsz(3)/2.5 scrsz(4)/1.25]);
    subplot(511), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itIAS} [kts]');
    % subplot(511), hold; grid; xlabel('{\itt} [s]'); ylabel('{\ith} [ft]');
    subplot(512), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\alpha} [�]');
    subplot(513), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itq} [�/s]');
    subplot(514), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\theta} [�]');
    subplot(515), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\delta}_{\ite} [�]');

    subplot(511); plot(t,y(:,1)/0.5144,t,y_lin(:,1)/0.5144); 
    % subplot(511); plot(t,x(:,1)/0.3048);
    subplot(512); plot(t,y(:,3)*180/pi,t,y_lin(:,2)*180/pi);
    subplot(513); plot(t,y(:,10)*180/pi,t,y_lin(:,5)*180/pi);
    subplot(514); plot(t,y(:,6)*180/pi,t,y_lin(:,8)*180/pi);
    subplot(515); plot(t,u_nlin(:,1)*180/pi);

    figure('Name','Lateral Plane of Motion','NumberTitle','on',...
           'Position',[scrsz(3)/11.00 scrsz(4)/10 scrsz(3)/2.5 scrsz(4)/1.25]);
    subplot(511), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\beta} [�]');
    subplot(512), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itp} [�/s]');
    subplot(513), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itr} [�/s]');
    subplot(514), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\phi} [�]');
    subplot(515), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\delta}_{\ita} [�]');

    subplot(511); plot(t,y(:,4)*180/pi,t,y_lin(:,3)*180/pi); 
    subplot(512); plot(t,y(:,9)*180/pi,t,y_lin(:,4)*180/pi);
    subplot(513); plot(t,y(:,11)*180/pi,t,y_lin(:,6)*180/pi);
    subplot(514); plot(t,y(:,5)*180/pi,t,y_lin(:,7)*180/pi);
    subplot(515); plot(t,(u_nlin(:,3)-u_nlin(:,4))/2*180/pi);
end

end
%---------------------------------------------------------------------------------------------------EOF