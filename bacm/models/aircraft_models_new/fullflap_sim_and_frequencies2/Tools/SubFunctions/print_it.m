function print_it(style,name)
if strcmp(style,'eps')
    print (  gcf,'-depsc', '-r600', '-painters', name )
    saveas(  gcf,name,'fig')
elseif strcmp(style,'png')
    print (  gcf,'-dpng', '-r600', '-opengl', name )
    saveas(  gcf,name,'fig')
elseif strcmp(style,'tif')
    print (  gcf,'-dtiff', '-r600', '-opengl', name )
    saveas(  gcf,name,'fig')
else
    disp('Unknown Datatype')
end
end