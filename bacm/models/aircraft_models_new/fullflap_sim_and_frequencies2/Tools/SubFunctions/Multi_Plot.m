function Multi_Plot(Result,t_end, color_vec,i)
show_figure = [0 0 0 0 0 1 0];
%% Choose Figure Output

%% Figure 1 %%%%%%%%%%%%%%
if show_figure(1)
figure(1)
plot_me(12,2,3,1)
subplot(4,2,1)
plot(Result.Time_s, Result.Alt_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Alt [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,2)
plot(Result.Time_s, Result.alpha_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \alpha [^\circ] $$','Interpreter','latex')
xlim([0 t_end])
ylim([-25 27])

subplot(4,2,3)
plot(   Result.Time_s, Result.V_TAS_mps,color_vec(i),...
        Result.Time_s, Result.V_IAS_mps,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ V_{TAS} [m/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,4)
plot(Result.Time_s, Result.alpha_H_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \alpha_H [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,5)
plot(Result.Time_s, Result.V_Vert_mps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ V_{vert} [m/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,6)
plot(Result.Time_s, Result.beta_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \beta [^\circ/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,7)
plot(Result.Time_s, Result.n_z_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ n_z [-] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,8)
plot(Result.Time_s, Result.gamma_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \gamma [^\circ] $$','Interpreter','latex')
xlim([0 t_end])
end

%% Figure 2 %%%%%%%%%%%%%%
if show_figure(2)
figure(2)
plot_me(12,2,3,1)
subplot(4,2,1)
plot(Result.Time_s, Result.theta_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \Theta [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,2)
plot(Result.Time_s, Result.p_degps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ p [^\circ / s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,3)
plot(Result.Time_s, Result.Elevator_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \eta [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,4)
plot(Result.Time_s, Result.q_degps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ q [^\circ / s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,5)
plot(Result.Time_s, Result.C_mu_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ C_{\mu} [-] $$','Interpreter','latex')
ylim([0 0.05])
xlim([0 t_end])

subplot(4,2,6)
plot(Result.Time_s, Result.r_degps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ r [^\circ / s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,7)
plot(Result.Time_s, Result.Throttle_*100,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Throttle [\%] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,8)
plot(Result.Time_s, Result.Prop_Forces_X_N./1000,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Thrust [kN] $$','Interpreter','latex')
xlim([0 t_end])
end

%% Figure 3 %%%%%%%%%%%%%%
if show_figure(3)
figure(3)
plot_me(12,2,3,1)
subplot(4,2,1)
plot(Result.Time_s, Result.a_z_mps2,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ a_{z} [m/s^2] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,2)
plot(Result.Time_s, Result.MG_F_,color_vec(i),Result.Time_s, Result.NG_F_,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ F_{NG} [N] $$','Interpreter','latex')
xlim([0 t_end])
legend('MG','NG','Location','SouthEast')

subplot(4,2,3)
plot(Result.Time_s, Result.MG_WoW_,color_vec(i),Result.Time_s, Result.NG_WoW_,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ WoW_{MG} [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,4)
plot(Result.Time_s, Result.MG_H_,color_vec(i),Result.Time_s, Result.NG_H_,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ H_{NG} [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,5)
plot(Result.Time_s, Result.x_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ x [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,6)
plot(Result.Time_s, Result.y_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ y [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,7)
plot(Result.Time_s, Result.dist_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Distance [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,8)
plot(Result.Time_s, Result.C_L_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ C_L [-] $$','Interpreter','latex')
xlim([0 t_end])
end

%% Figure 4 %%%%%%%%%%%%%%
if show_figure(4)
figure(4)
plot_me(12,2,3,1)
subplot(4,2,1)
plot(   Result.Time_s, Result.ILS_X_,color_vec(i),...
        Result.Time_s, Result.x_m,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ ILS_X [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,2)
plot(   Result.Time_s, Result.ILS_Y_,color_vec(i),...
        Result.Time_s, Result.y_m,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ ILS_Y [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,3)
plot(   Result.Time_s, Result.ILS_H_,color_vec(i),...
        Result.Time_s, Result.h_m,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ ILS_H [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,4)
plot(Result.Time_s, Result.dLoc_DDM_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ dLoc [DDM] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,5)
plot(Result.Time_s, Result.dGs_DDM_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ dGs [DDM] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,6)
plot(Result.Time_s, Result.Elevator_Cmd_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \eta_{Cmd} [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,7)
plot(Result.Time_s, Result.dy_Loc_Filt_m_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ dy_{Loc_{Filtered}} [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,2,8)
plot(Result.Time_s, Result.dh_Gs_Filt_m_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ dh_{Gs_{Filtered}} [m] $$','Interpreter','latex')
xlim([0 t_end])
end

%% Figure 5 %%%%%%%%%%%%%%
if show_figure(5)
figure(5)
plot_me(12,2,3,1)
% subplot(4,2,1)
plot3(Result.x_m, Result.y_m,Result.h_m,color_vec(i),...
      Result.ILS_X_+Result.x_m,  Result.ILS_Y_ + Result.y_m, Result.ILS_H_+Result.h_m,color_vec(i+1),...
      Result.ILS_X_+Result.x_m,  Result.ILS_Y_ + Result.y_m, Result.ILS_H_+Result.Radar_Alt_m_,color_vec(i+2),'LineWidth',2), hold on, grid on
xlabel('$$ X [m] $$','Interpreter','latex')
ylabel('$$ Y [m] $$','Interpreter','latex')
% xlim([0 t_end])
legend('A/C','ILS','Location','NorthEast')
end
% 

%% Figure 6 %%%%%%%%%%%%%%
if show_figure(6)
figure(6)
plot_me(7,.6,2,1)
subplot(4,4,16)
plot(   Result.Time_s, Result.phi_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \phi [deg] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,3)
plot(   Result.Time_s, Result.V_TAS_mps,color_vec(i),...
        Result.Time_s, Result.V_IAS_mps,color_vec(i+1),...
        Result.Time_s, Result.V_App_mps,color_vec(i+2),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ V_{TAS} [m/s] $$','Interpreter','latex')
xlim([0 t_end])
h1= legend('V_{TAS}','V_{IAS}','Location','Best','Orientation','vertical');
set(h1, 'FontSize', 5)

subplot(4,4,9)
plot(   Result.Time_s, Result.Elevator_deg,color_vec(i),...
        Result.Time_s, Result.Act_Elevator_deg,color_vec(i+1),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \eta [^\circ] $$','Interpreter','latex')
xlim([0 t_end])
h2=legend('Cmd','Actuator','Location','NorthEast');
set(h2, 'FontSize', 5)

subplot(4,4,6)
plot(Result.Time_s, Result.theta_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \Theta [^\circ] $$','Interpreter','latex')
xlim([0 t_end])
% ax1 = gca;
% ax2 = axes('Position',get(ax1,'Position'),...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor',color_vec(i+1));

subplot(4,4,2)
% plot(   Result.Time_s, Result.ILS_H_,color_vec(i),...
%         Result.Time_s, Result.h_m,color_vec(i+1),'LineWidth',2), hold on, grid on
% xlabel('$$ Time [s] $$','Interpreter','latex')
% ylabel('$$ ILS_H [m] $$','Interpreter','latex')
% xlim([0 t_end])
% legend('ILS','A/C','Location','NorthEast')
plot(   Result.Time_s, -Result.ILS_H_+Result.h_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Error_H [m] $$','Interpreter','latex')
xlim([0 t_end])
% legend('ILS','A/C','Location','NorthEast')

subplot(4,4,14)
plot(Result.Time_s, Result.q_degps,'Color',color_vec(i),'LineWidth',2), grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ q [^\circ/s ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,10)
plot(Result.Time_s, Result.Throttle_*100,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Throttle [\%] $$','Interpreter','latex')
xlim([0 t_end])
% ylim([0 100])
% ax1 = gca;
% ax2 = axes('Position',get(ax1,'Position'),...
%            'YAxisLocation','right',...
%            'Color','none',...
%            'XColor','k','YColor',color_vec(i+1));

subplot(4,4,15)
plot(Result.Time_s, Result.dGs_deg_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ dGs [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,11)
plot(Result.Time_s, Result.Prop_Forces_X_N./1000,'Color',color_vec(i),'LineWidth',2), grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Thrust [kN] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,1)
plot(Result.Time_s, Result.Alt_m,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ Alt [m] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,5)
plot(Result.Time_s, Result.alpha_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \alpha [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,7)
plot(Result.Time_s, Result.alpha_H_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \alpha_H [^\circ] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,4)
plot(Result.Time_s, Result.V_Vert_mps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ V_{vert} [m/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,8)
plot(Result.Time_s, Result.beta_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \beta [^\circ/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,13)
plot(Result.Time_s, Result.n_z_,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ n_z [-] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,4,12)
plot(Result.Time_s, Result.gamma_deg,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ \gamma [^\circ] $$','Interpreter','latex')
xlim([0 t_end])
%print_it('eps',Trial_Name)
end
%% Figure 7 %%%%%%%%%%%%%%
if show_figure(7)
figure(7)
plot_me(12,2,3,1)
subplot(4,1,1)
plot(   Result.Time_s, Result.u_w_mps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ u_{Wind} [m/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,1,2)
plot(   Result.Time_s, Result.v_w_mps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ v_{Wind} [m/s] $$','Interpreter','latex')
xlim([0 t_end])

subplot(4,1,3)
plot(   Result.Time_s, Result.w_w_mps,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ w_{Wind} [m/s] $$','Interpreter','latex')
xlim([0 t_end])


V_Wind = sqrt((Result.w_w_mps).^2+(Result.v_w_mps).^2+(Result.u_w_mps).^2);
subplot(4,1,4)
plot(   Result.Time_s, V_Wind,color_vec(i),'LineWidth',2), hold on, grid on
xlabel('$$ Time [s] $$','Interpreter','latex')
ylabel('$$ V_{Wind} [m/s] $$','Interpreter','latex')
xlim([0 t_end])


end