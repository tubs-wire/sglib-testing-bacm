%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                       BACM_Test_Plot                                               *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Script                                                                                  *
%*                                                                                                    *
%* Purpose  : Script for plotting the reaction of the aircraft modell to different input signals on   *
%*            the control surfaces.                                                                   *
%* Version  : 1.2                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : The file plotfunc.m is necessary for execution.                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BACM_Test_Plot
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 27-OCT-09 * Basic Design                                            *
%* Jobst Henning Diekmann       * 02-FEB-12 * Integration of SFB880 Model structure                   *
%* Jobst Henning Diekmann       * 30-JUL-12 * Integration of Feedback Controller Input Values         *
%* Jobst Henning Diekmann       * 30-OKT-13 * Sliced Simulation including state vector manipulation   *
%* Jobst Henning Diekmann       * 15-APR-14 * Simulation MK I Test Plotting Function                  *
%******************************************************************************************************
%% Simulation Initialization
% Clear Workspace and initialize simulation variable structure in workspace
clear all;
close all;
clc

addpath(genpath('D:\SVN-SFB880\Arbeitspakete\AP 1 Flugmechanische Simulation\Simulationsumgebung\Simulationsmodell\SFB 880 REF0-2013 Model MK1 - Uncertainty Freeze\Model 13-01-2014\'))

% Go to root directory
mfile = mfilename('fullpath');
cd([fileparts(fileparts(mfile)) '\Tools']);

color_vec = ['k';'r';'b';'g';'c';'m'];

TrimSettings.SimMode = 1;
for Testcase = 1:9
    clear BACM BACM_Trim
    Trial_Name = ['Autopilot_Tuning'];
    switch Testcase
        case(1) % Altitude Low, Speed good
        TrimSettings.strTrimfile = 'Alt850Ma016FPA009-Jan-2015142.mat'; % dH = -40m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12);
        Trial_Name = [Trial_Name '2Low']
        case(2) % Altitude Low, Speed Low
        TrimSettings.strTrimfile = 'Alt850Ma016FPA009-Jan-2015142.mat'; % dH = -40m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)+5;          
        Trial_Name = [Trial_Name '2Low2Slow']
        case(3) % Altitude Low, Speed High
        TrimSettings.strTrimfile = 'Alt850Ma016FPA009-Jan-2015142.mat'; % dH = -40m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)-5;          
        Trial_Name = [Trial_Name '2Low2Fast']
        case(4) % Altitude High, Speed good
        TrimSettings.strTrimfile = 'Alt930Ma016FPA009-Jan-20151434.mat'; % dH = +35m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12);          
        Trial_Name = [Trial_Name '2High']
        case(5) % Altitude High, Speed Low
        TrimSettings.strTrimfile = 'Alt930Ma016FPA009-Jan-20151434.mat'; % dH = +35m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)+5;          
        Trial_Name = [Trial_Name '2High2Slow']
        case(6) % Altitude High, Speed High
        TrimSettings.strTrimfile = 'Alt930Ma016FPA009-Jan-20151434.mat'; % dH = +35m, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)-5;          
        Trial_Name = [Trial_Name '2High2Fast']
        case(7) % Altitude good, Speed High
        TrimSettings.strTrimfile = 'Alt893Ma016FPA-112-Jan-2015755.mat'; % dH = 0, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)-5;          
        Trial_Name = [Trial_Name '2Fast']
        case(8) % Altitude good, Speed Low
        TrimSettings.strTrimfile = 'Alt893Ma016FPA-112-Jan-2015755.mat'; % dH = 0, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)+5;          
        Trial_Name = [Trial_Name '2Slow']
        case(9) % Altitude good, Speed good
        TrimSettings.strTrimfile = 'Alt893Ma016FPA-112-Jan-2015755.mat'; % dH = 0, V_TAS = 55 m/s
        BACM_Simulation_Init;
        Test_Parameters.V_Approach          = BACM_Trim.Init.Y0(12)+0;          
        Trial_Name = [Trial_Name 'Good']
    end
            

%% Simulation and Test Parameters

    % Set Simulation Parameters
    BACM.Sim.StartTime  =  0.0;                            % Simulation Start Time                       [s]
    BACM.Sim.EndTime    =  200;                             % Simulation End Time                         [s]
    BACM.Sim.SampleTime =  0.01;                           % Simulation Sample Time                      [s]

    % Set Test Parameters
    Test_Parameters.Max_Data    = 100000;                  % Maximum of recorded data points             [-]

    Test_Parameters.Input_Type  = 0;                   % No input in the follwing time slices        [-]
    %*                                                     % Control Input Signal Types:
    %*                                                     % [0] - No Signal
    %*                                                     % [1] - Step
    %*                                                     % [2] - Impulse
    %*                                                     % [3] - Doublet
    %*                                                     % [4] - Sinus-Wave

    %* Step Signal Parameters
    Test_Parameters.Step_Start      = 1;                   % Start Time of Step Input                    [s]
    Test_Parameters.Step_Size       = -2.8;                  % Step Size                                 [deg]
    %
    %* Impulse Signal Paramters
    Test_Parameters.Impulse_Start   = 1;                   % Start Time of Impulse Input                 [s]
    Test_Parameters.Impulse_End     = 2;                   % Stop Time of Impulse Input                  [s]
    Test_Parameters.Impulse_Size    = 3;                   % Impulse Size                              [deg]
    % 
    %* Doublet Signal Parameters
    Test_Parameters.Doublet_Start   = 1;                   % Start Time of Doublet Input                 [s]
    Test_Parameters.Doublet_End     = 3;                   % Stop Time of Doublet Input                  [s]
    Test_Parameters.Doublet_Size    = 20;                   % Doublet Size                              [deg]
    % 
    %* Sinus Wave Parameters
    Test_Parameters.Sinus_Freqency  = 1;                   % Frequency of Sinus Wave Input              [Hz]
    Test_Parameters.Sinus_Start     = 1;                   % Start Time of Sinus Wave Input              [s]
    %
    %* Selection of Control Surface
    Test_Parameters.Aileron_Gain    = 0;                   % Select [1] Aileron as Test Surface.         [-]
    Test_Parameters.Elevator_Gain   = 1;                   % Select [1] Elevator as Test Surface.        [-]
    Test_Parameters.Rudder_Gain     = 0;                   % Select [1] Rudder as Test Surface.          [-]
    Test_Parameters.Stabilizer_Gain = 0;                   % Select [1] Stabilizer as Test Surface.      [-]
    Test_Parameters.Throttle_Gain   = 0;                   % Select [1] Throttle as Test Input Signal.   [-]
    %
%% Simulation Configuration Parameters for additional model functions    
    % Set Cmu Parameters
    BACM.Aerodynamics.Prop.On                   = TrimPointInfo.SetSlipstream;
    BACM.Aerodynamics.Active_HTP.On             = TrimPointInfo.SetHTPactive;
    BACM.Aerodynamics.Active_HTP.Performance    = 1000;                          % Performance in [%]
    BACM.Aerodynamics.Cmu.Cmu_Var_Flg           = 0;
    BACM.Aerodynamics.Cmu.Cmu_Const             = TrimPointInfo.C_mu/2;
    BACM.Aerodynamics.Droop.Active              = TrimPointInfo.SetDroop;
    BACM.Aerodynamics.Inputs.Lat_Motion         = TrimPointInfo.SetLateral;
    BACM.Propulsion.Prop_Efficiency             = 1;
    BACM.Propulsion.Prop_Eff_OEI                = 1;
    BACM.Propulsion.Prop_Eff_Delay              = 1;
    BACM.Propulsion.Mode                        = TrimPointInfo.Engine_Mode;

    % Blowing System Failure
    Test_Parameters.ILS_GS              = -3.5 * pi/180;          
    % Test_Parameters.Throttle_Cmd        = TrimPointInfo.Throttle/100;
    BACM.Aerodynamics.Fail.Deactivated  = 0;                                % Throttle Step Final Value for AHLS Failure 
    BACM.Aerodynamics.Fail.Time         = BACM.Sim.EndTime;          % Time of AHLS Failure 
    BACM.Aerodynamics.Fail.Time_Control = BACM.Sim.EndTime;      % Time of Controller Initialization
    BACM.Aerodynamics.Cmu.Cmu_Fail      = TrimPointInfo.C_mu/2;             % Jet Momentum Coeff after AHLS Failure
    BACM.Environment.Wind.Turbulence_Flag = 1;
    BACM.Environment.Wind.Turbulence_Intensity_Index = 1;

    % Initialize Controller Section
    Controller = BACM_Controller_Init(BACM_Trim); 
   
    % One Engine Inoperative
    Test_Parameters.OneEngineInoperative    = BACM.Propulsion.OEI;          % One Engine Inoperative [0 No / 1 Yes]
    Test_Parameters.BlowingOperative        = BACM.Propulsion.BLCOP;        % Blowing Operative [0 Yes / 1 No]

%% Run Simulation Model
    sim('DLR_SFB880_Test_Quat');

    if exist('Sim_Results.mat')== 2
        load Sim_Results.mat
    end

    Case_Name = ['TECS_Setting_' num2str(Controller.Energy.TECS.Setting)];

%% Result Struct
    Result.(Trial_Name).(Case_Name) = Result_Struct;
    save('Sim_Results.mat','Result')

    t_end = BACM.Sim.EndTime;

    Result.(Trial_Name).(Case_Name).dist_m      = sqrt((Result.(Trial_Name).(Case_Name).y_m).^2 + (Result.(Trial_Name).(Case_Name).x_m).^2);
    i_Obstacle_height               = find(Result.(Trial_Name).(Case_Name).Alt_m <= 11,1,'first');
    i_Stand_still                   = find(Result.(Trial_Name).(Case_Name).V_TAS_mps <= 0.01,1,'first');
    Result.(Trial_Name).(Case_Name).Land_Dist_m = Result.(Trial_Name).(Case_Name).dist_m(i_Stand_still) - Result.(Trial_Name).(Case_Name).dist_m(i_Obstacle_height);
    disp(Result.(Trial_Name).(Case_Name).Land_Dist_m)

%% Multi Plot
    close all
    Color = 1;

    Multi_Plot(Result,Trial_Name,Case_Name,t_end, color_vec,Color)
end
% ---------------------------------------------------------------------------------------------------EOF