function model = highlift_eigen_init2(num_params,varargin)
%HIGHLIFT_EIGEN_INIT Initialises the structure that keeps the internal
% Initiating the simulator for the SFB 880 aircraft
% Initialization of stochastic wrapper

options = varargin2options(varargin);
[t_step, options] =    get_option(options, 't_step',0.01);
[t_min_max, options] = get_option(options, 't_min_max', [0,20]);
check_unsupported_options(options, mfilename);

t_min=t_min_max(1);
t_max=t_min_max(2);

n_t=((t_max-t_min)/t_step+1);
n_var_t=5;

n_var=18;

num_vars=n_t*n_var_t+n_var;
% initialise the model object
model = model_init(num_params, num_vars, ...
    'solve_func', @run_highlift_sim);

%Input of examined time interval
model.t_min=t_min;
model.t_max=t_max;
model.t_step=t_step;
model.sol_names_t={'Altitude [m]';'V_{TAS}: True air speed [m/s]';'\alpha: angle of attack [degree]';'q: pitch rate [degree/s]';'\theta : pitch altitude [degree]' };
model.sol_names={'omega_{ph}'; 'alpha_{ph}';'L_2 error (phugoid)';'omega_{sp}'; 'alpha_{sp}';'L_2 error (ph+sp)'; 'omega_{ph}'; 'alpha_{ph}';'eigeninfo1-8';'WN_Phugoid'; 'D_Phugoid';'Poles_Phugoid';'WN_ShortPeriod'; 'D_ShortPeriod';'Poles_ShortPeriod'};
model.n_t=n_t;
model.n_var_t=n_var_t;
model.n_var=n_var;
end

