function varargout = highlift_eigen_solve(state,sample,varargin)
% Call aircraft simulator
%

% options = varargin2options(varargin);
% [...., options] = get_option(options, '....', ....);
% check_unsupported_options(options, mfilename);

%scale and shift from standard deviation and move to state.actual params
state=scale_and_update_vars(sample, state);


%% Run simulation:
tic
[Time, State_ref]=run_highlift_sim(state.actual_params, state.t_min_max(1), state.t_min_max(2));
toc
if nargout>1
    varargout{1}=Time;
    %={[State_atm(:,1);State_atm(:,2);State_atm(:,3)]};
    varargout{2}=State_ref;
else
    varargout= State_ref;
end
