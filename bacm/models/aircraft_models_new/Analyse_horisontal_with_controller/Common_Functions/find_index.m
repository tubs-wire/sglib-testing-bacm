%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2005      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      sort_vector                                                   *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Helper function to detect the indices of the linear system                              *
%             states within the state vector of the non-linear system                                 *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : [X_Index]=                                                                                *
%*           find_index(X_Names_Lin,cleaned_state_names)                                              *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%*        Dominik Niedermeier   * 22-AUG-05 *                     Basic Design                        *
%******************************************************************************************************


function [X_Index]=find_index(X_Names_Lin, cleaned_state_names)

% Detecting vector sizes
[row_iv col_iv]=size(X_Names_Lin);
[row_ss col_ss]=size(cleaned_state_names);

% Initializing variables
n=0;
X_Index = 0 ;

% Assigning correponding indices to index vector of the linear atate vector
for k_names=1:row_iv
    i_state=strmatch(X_Names_Lin(k_names,:),cleaned_state_names);
    Size_i_state = size(i_state);
    Size_i_state = Size_i_state(1);
        if Size_i_state > 1
           i_check = i_state(1);
           i_check = find(X_Index == i_check);
              if isempty(i_check)
                  i_state = i_state(1);
              else
                  i_state = i_state(2); 
              end
        end
    if ~isempty(i_state)
        X_Index(k_names)=i_state;
    else
        X_Index(k_names)=0;
    end
end;
