function Ax = Multi_Plot_SE(Result,Trial_Name,Case_Name,t_end, color_vec,z, Plot_Vec, figure_established, Ax, Controller_Active, BasePath, FolderName)
%% 

NumberOfPlots   = length(Plot_Vec)-1;
PlotsPerRow     = 3;
Rows = ceil(NumberOfPlots/PlotsPerRow);

Width  = 0.70/PlotsPerRow;
Height = 0.54/Rows;

Col = 0.2/(PlotsPerRow+2);
for i = 2:PlotsPerRow
    Col = [Col (Col(i-1)+ Width + 0.25/PlotsPerRow)];
end
Col=Col+0.05;
Row = 0.35/Rows;
for i = 2:Rows
    Row = [Row (Row(i-1)+ Height + 0.45/Rows)];
end
Row=Row+0.02;

Row = Row(end:-1:1);
LineSize    = 1.2;
MarkerSize  = 5;
FontSize    = 11;
LegendSize  = FontSize-3;
 
% Row = [.87 0.67 0.47 .28 .1];
% Col = [0.1 0.33 0.57 .8];

% Width  = 0.15;
% Height = 0.10;
% figure(6)
set( gcf ,  'Color','w', ...
            'DefaultLineLineWidth',LineSize ,...
            'DefaultLineMarkerSize',MarkerSize,...
            'DefaultAxesFontName','times',...
            'DefaultAxesFontSize',FontSize,...
            'DefaultTextInterpreter','latex')

if ~figure_established
    for i = 1 : NumberOfPlots
            Ax_New(i) = subplot(Rows,PlotsPerRow,i);
            colNo = mod(i,PlotsPerRow);
            rowNo = ceil((i)/PlotsPerRow);
            if colNo == 0
               colNo =  PlotsPerRow;
            end
            set(Ax_New(i), 'position',[Col(colNo) Row(rowNo) Width Height])
            xlim([0 t_end])

    end
end

hd = findall(gcf,'type', 'axes');
for i = 1: NumberOfPlots
        if figure_established
            find_old_Ax = find(hd == Ax(i));
            Ax_ = hd(find_old_Ax);
        else
            Ax_ = hd((i)); 
        end
        axes(Ax_);
        p1 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){Plot_Vec(end - i + 1)}.Value,'LineWidth',LineSize,'Color',color_vec(z + Controller_Active));
        hold on, grid on, box off
        xlabelname = Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Label;
        ylabelname = Result.(Trial_Name).(Case_Name){Plot_Vec(end - i + 1)}.Label;
        xlabel(['$$ ' xlabelname ' $$'],'Interpreter','latex')
        ylabel(['$$ ' ylabelname ' $$'],'Interpreter','latex')
        
        switch (Plot_Vec(end - i + 1))
            case {54} % Aileron LH
                ylabel('$$ \xi~\textrm{in}~^\circ $$')
            case {55}  % Aileron RH
                ylabel('$$ \xi~\textrm{in}~^\circ $$')
            case {61} % Throttle RH
                ylabel('$$ \textrm{Throttle}~\textrm{in}~\% $$')
                ylim([-.05 1.05])
            case {62}  % Throttle LH
                ylabel('$$ \textrm{Throttle}~\textrm{in}~\% $$')
                ylim([-.05 1.05])
            case {65} % Thrust LH
                ylabel('$$ \textrm{Thrust}~\textrm{in}~N $$')
            case {66} % Thrust RH
                ylabel('$$ \textrm{Thrust}~\textrm{in}~N $$')
        end                    

        
        if Controller_Active
            switch (Plot_Vec(end - i + 1))
                case {49} % Elevator
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){48}.Value,'k--','LineWidth',LineSize);
                    l_49 = legend([p2],'Cmd','Location','SouthEast','Orientation','horizontal');
                    set(l_49,'Box','off','FontSize',LegendSize)
                    maxVal = max(Result.(Trial_Name).(Case_Name){48}.Value) + 0.1 * abs(max(Result.(Trial_Name).(Case_Name){48}.Value));
                    minVal = min(Result.(Trial_Name).(Case_Name){48}.Value) - 0.1 * abs(min(Result.(Trial_Name).(Case_Name){48}.Value));
                    ylim([minVal maxVal])
                case {52} % Rudder
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){50}.Value,'k--','LineWidth',LineSize);
                    l_52 = legend([p2],'Cmd','Location','SouthEast','Orientation','horizontal');
                    set(l_52,'Box','off','FontSize',LegendSize)
                    maxVal = max(Result.(Trial_Name).(Case_Name){50}.Value) + 0.1 * abs(max(Result.(Trial_Name).(Case_Name){50}.Value));
                    minVal = min(Result.(Trial_Name).(Case_Name){50}.Value) - 0.1 * abs(min(Result.(Trial_Name).(Case_Name){50}.Value));
                    ylim([minVal maxVal])
                case {54} % Aileron LH
                    p3 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){55}.Value,'LineWidth',LineSize,'Color',color_vec(z+1 + Controller_Active));
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, -Result.(Trial_Name).(Case_Name){51}.Value,'k--','LineWidth',LineSize);
                    l_54 = legend([p1, p3],'LH','RH','Location','SouthEast','Orientation','horizontal');
                    set(l_54,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \xi~\textrm{in}~^\circ $$')
                    maxVal = max(abs(Result.(Trial_Name).(Case_Name){51}.Value) + 0.1 * abs(Result.(Trial_Name).(Case_Name){51}.Value));
                    ylim([-maxVal maxVal])
                case {55}  % Aileron RH
                    p3 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){54}.Value,'LineWidth',LineSize,'Color',color_vec(z+1 + Controller_Active));
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, -Result.(Trial_Name).(Case_Name){51}.Value,'k--','LineWidth',LineSize);
                    l_55 = legend([p1, p3],'RH','LH','Location','SouthEast','Orientation','horizontal');
                    set(l_55,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \xi~\textrm{in}~^\circ $$')
                    maxVal = max(abs(Result.(Trial_Name).(Case_Name){51}.Value) + 0.1 * abs(Result.(Trial_Name).(Case_Name){51}.Value));
                    ylim([-maxVal maxVal])
                case {61} % Throttle RH
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){62}.Value,'LineWidth',LineSize,'Color',color_vec(z+1 + Controller_Active));
                    p1 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){61}.Value,'LineWidth',LineSize,'Color',color_vec(z + Controller_Active));
                    p3 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){63}.Value,'k--','LineWidth',LineSize);
                    p4 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){64}.Value,'k--','LineWidth',LineSize);
                    l_61 = legend([p3],'Cmd','Location','SouthWest','Orientation','horizontal');
                    set(l_61,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \textrm{Throttle}~\textrm{in}~\% $$')
                    ylim([-.05 1.05])
                case {62}  % Throttle LH
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){61}.Value,'LineWidth',LineSize,'Color',color_vec(z+1 + Controller_Active));
                    p1 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){62}.Value,'LineWidth',LineSize,'Color',color_vec(z + Controller_Active));
                    p3 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){63}.Value,'k--','LineWidth',LineSize);
                    p4 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){64}.Value,'k--','LineWidth',LineSize);
                    l_62 = legend([p3],'Cmd','Location','SouthWest','Orientation','horizontal');
                    set(l_62,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \textrm{Throttle}~\textrm{in}~\% $$')
                    ylim([-.05 1.05])
                case {65} % Thrust LH
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){66}.Value,'LineWidth',LineSize,'Color',color_vec(z + 1 + Controller_Active));
                    p1 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){65}.Value,'LineWidth',LineSize,'Color',color_vec(z + Controller_Active));
                    l_65 = legend([p1, p2],'LH','RH','Location','SouthWest','Orientation','horizontal');
                    set(l_65,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \textrm{Thrust}~\textrm{in}~N $$')
                case {66} % Thrust RH
                    p2 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){65}.Value,'LineWidth',LineSize,'Color',color_vec(z + 1 + Controller_Active));
                    p1 = plot(Result.(Trial_Name).(Case_Name){Plot_Vec(1)}.Value, Result.(Trial_Name).(Case_Name){66}.Value,'LineWidth',LineSize,'Color',color_vec(z + Controller_Active));
                    l_66 = legend([p1, p2],'RH','LH','Location','NorthWest','Orientation','horizontal');
                    set(l_66,'Box','off','FontSize',LegendSize)
                    ylabel('$$ \textrm{Thrust}~\textrm{in}~N  $$')
            end                    
        end
        Ax(i) = hd(i);
        xlim([0 t_end])
        
end
    fout = [BasePath FolderName 'Tools\Plots\' Trial_Name];
    print(gcf, '-depsc2', fout)

end