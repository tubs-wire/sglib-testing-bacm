function [samples]=create_UQomega(n)

Q=SimParamSet();

Q.add(SimParameter('Aerodynamics_Lift_Uncert_b05T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Lift_Uncert_b05T1',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Lift_Uncert_b05T2',UniformDistribution(1-20/100,1+20/100)));
Q.add(SimParameter('Aerodynamics_Lift_Uncert_b10T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Lift_Uncert_b10T1',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Lift_Uncert_b10T2',UniformDistribution(1-20/100,1+20/100)));

Q.add(SimParameter('Aerodynamics_Drag_Uncert_b05T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Drag_Uncert_b05T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Drag_Uncert_b05T2',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Drag_Uncert_b10T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Drag_Uncert_b10T1',UniformDistribution(1-20/100,1+20/100)));
Q.add(SimParameter('Aerodynamics_Drag_Uncert_b10T2',UniformDistribution(1-20/100,1+20/100)));

Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b05T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b05T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b05T2',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b10T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b10T1',UniformDistribution(1-20/100,1+20/100)));
Q.add(SimParameter('Aerodynamics_Pitch_Uncert_b10T2',UniformDistribution(1-20/100,1+20/100)));

Q.add(SimParameter('Aerodynamics_Roll_Uncert_b05T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_b05T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_b05T2',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_b10T0',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_b10T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_b10T2',UniformDistribution(1-25/100,1+25/100)));

Q.add(SimParameter('Aerodynamics_Roll_Uncert_Clp',UniformDistribution(1-5/100,1+5/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_Clr',UniformDistribution(1-50/100,1+50/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_Clzeta',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Roll_Uncert_Clxi',UniformDistribution(1-10/100,1+10/100)));

Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b05T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b05T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b05T2',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b10T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b10T1',UniformDistribution(1-25/100,1+25/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_b10T2',UniformDistribution(1-25/100,1+25/100)));

Q.add(SimParameter('Aerodynamics_Yaw_Uncert_Cnp',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_Cnr',UniformDistribution(1-50/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_Cnzeta',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Yaw_Uncert_Cnxi',UniformDistribution(1-10/100,1+10/100)));

Q.add(SimParameter('Aerodynamics_Side_Uncert_b05T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Side_Uncert_b05T1',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Side_Uncert_b05T2',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Side_Uncert_b10T0',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Aerodynamics_Side_Uncert_b10T1',UniformDistribution(1-15/100,1+15/100)));
Q.add(SimParameter('Aerodynamics_Side_Uncert_b10T2',UniformDistribution(1-10/100,1+10/100)));

Q.add(SimParameter('Aerodynamics_Side_Uncert_CYzeta',UniformDistribution(1-10/100,1+10/100)));

Q.add(SimParameter('Controller_Thrust_Uncert',UniformDistribution(1-10/100,1+10/100)));
Q.add(SimParameter('Controller_alpha_Uncert',UniformDistribution(1-20/100,1+20/100)));
Q.add(SimParameter('Controller_beta_Uncert',UniformDistribution(1-20/100,1+20/100)));

samples=(Q.sample(n,'mode','qmc'))';

end

