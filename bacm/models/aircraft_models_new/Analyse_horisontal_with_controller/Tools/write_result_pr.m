function [t_end,Result,Trial_Name,savename,Case_Name] = write_result_pr(run, NrTrimpoint, resultpath)

Trial_Name = ['Round' num2str(run)];                % Random Name Assignment
Case_Name   = 'Case_1';     % Random Struct Identifier
Save_Name   = 'HF2';        % Random Struct Identifier HF = Horizontal Flight
TestCase    = num2str(run);
TestTrim    = num2str(NrTrimpoint);
savename    = [resultpath 'Sim_Results_' Save_Name '_' TestCase '_Trim' TestTrim '.mat'];

Result.(Case_Name) = Result_Struct;

t_end = Result.(Case_Name){9}.Value(end);

end

