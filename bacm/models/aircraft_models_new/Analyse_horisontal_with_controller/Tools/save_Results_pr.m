function [] = save_Results_pr( simo, NrTrimpoint, run, FilePath, resultpath, ind, SeeAllInfluences)

%% Result Struct

Trial_Name = ['Round' num2str(run)];                % Random Name Assignment
Case_Name   = 'Case_1';     % Random Struct Identifier
Save_Name   = 'HF2';        % Random Struct Identifier HF = Horizontal Flight
TestCase    = num2str(run);
TestTrim    = num2str(NrTrimpoint);
savename    = [resultpath 'Sim_Results_' Save_Name '_' TestCase '_Trim' TestTrim '.mat'];
Result=struct();
% Result.(Case_Name) = Result_Struct(outt);

Result.(Case_Name) = Result_Struct_pr(simo);
save_the_result_pr(savename,Result);
t_end = Result.(Case_Name){9}.Value(end);


Plots = [   {'Time_s'};...
            {'Alt_m'};...
            {'V_TAS_mps'};...
            {'gamma_deg'};...
            {'alpha_deg'};...
            {'alpha_H_deg'};...
            {'beta_deg'};...
            {'p_degps'};...
            {'q_degps'};...
            {'r_degps'};...
            {'phi_deg'};...
            {'theta_deg'};...
            {'psi_deg'};...
            {'n_x'};...
            {'n_y'};...
            {'n_z'};...
            {'Act_Aileron_LH_deg'};...
            {'Act_Elevator_deg'};...
            {'Act_Rudder_deg'}];

    SizePlots = size(Plots);
    Plot_Vec = [];

    for i = 1 : SizePlots(1,1)
        for j = 1: 112
            if strcmp(Result.(Case_Name){j}.Name,Plots(i))
                Plot_Vec = [Plot_Vec j];
                Result.(Case_Name){Plot_Vec(end)}.Name;
            end
        end
    end

    color_vec = ['k';'r';'b';'g';'r';'m'];
    
    if run ~=1
        figure_established = 1;
    else
        if ~SeeAllInfluences
                 close all
        end
        figure_established = 0;
        Ax = [];
    end
    z = run;                 % Color Code
    %figure(1)
    
    fout = [FilePath(1:ind(end-1)) 'Plots/' Trial_Name];
    if ~exist([FilePath(1:ind(end-1)) 'Plots/'],'dir')
        mkdir([FilePath(1:ind(end-1)) 'Plots/']);
    end

    %Ax = Multi_Plot_SE(Result,Trial_Name,Case_Name,t_end, color_vec,z, Plot_Vec, figure_established, Ax, BLAC.Controller.Activated, BasePath, FolderName);
    %print(gcf, '-depsc2', fout)
    
    beta  = Result.(Case_Name){15}.Value;
    dbeta = diff(Result.(Case_Name){15}.Value);
    
    ik=1;
    for ibeta = length(dbeta) : -1 : 2
        if dbeta(ibeta) > 0 && dbeta(ibeta-1) < 0 || dbeta(ibeta) < 0 && dbeta(ibeta-1) > 0
            i_beta_Peak(ik) = ibeta;
            Peaks(ik) = beta(ibeta);
            T_beta_Peak(ik) = Result.(Case_Name){9}.Value(ibeta);
            ik = ik +1;
        end
    end
    Sweep_Peaks     = (find(abs(Peaks)>0.01));
    Peaks           = Peaks(Sweep_Peaks(end:-1:1)); % Sweep small peaks
    T_beta_Peak     = T_beta_Peak(Sweep_Peaks(end:-1:1));
    i_beta_Peak     = i_beta_Peak(Sweep_Peaks(end:-1:1));
    T               = mean(diff(T_beta_Peak))*2;
    omega           = (2*pi)/T;
    log_decrement   = log(Peaks(2)/Peaks(4));
    sigma           = -log_decrement/T;
    omega_0         = sqrt(sigma^2+omega^2);
    D               = -sigma/omega_0; 
    D_omega         = omega * D;
    
    if omega > 0.4
        L_Omega = 'Level 1';
    else
        L_Omega = 'Level 3';
    end
    
    if D >= 0.08   
        L_D = 'Level 1';
    elseif  D >= 0.02 && D < 0.08
        L_D = 'Level 2';
    else D < 0.02
        L_D = 'Level 3';
    end
        
    if D_omega >= 0.1
        L_Domega = 'Level 1';
    elseif  D_omega >= 0.05 && D_omega < 0.1
        L_Domega = 'Level 2';
    else
        L_Domega = 'Level 3';
    end
        
    
    disp( '--------------------------------------------------------------------');
    disp(['omega_TS  : ' num2str(omega,     '%11.4f')   ' rad/s --> ' L_Omega   '       L1: > 0.4' ]);
    disp(['omega0_TS : ' num2str(omega_0,   '%11.4f')   ' rad/s'           ]);
    disp(['Damping_TS: ' num2str(D,         '%11.4f')   '       --> ' L_D       '       L1: > 0.08, L2> 0.02']);
    disp(['omega_0*D : ' num2str(D_omega,   '%11.4f')   '       --> ' L_Domega  '       L1: > 0.10, L2> 0.05' ]);
    disp( '--------------------------------------------------------------------');
    toc
    
end

