function []=compare_results(bacm,C2,siz)
diff_max=zeros(siz,1);
diff_min=zeros(siz,1);
for i=1:siz
diff_max(i)=max(max(bacm.Case_1{1,i}.Value-C2.Case_1{1,i}.Value));
diff_min(i)=min(min(bacm.Case_1{1,i}.Value-C2.Case_1{1,i}.Value));
end

max_dif=max(diff_max);
min_dif=min(diff_min);

sprintf('max difference is %d',max_dif)
sprintf('min difference is %d',min_dif)

end

