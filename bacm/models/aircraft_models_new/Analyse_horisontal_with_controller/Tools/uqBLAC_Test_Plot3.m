%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                       BLAC_Test_Plot                                               *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Script                                                                                  *
%*                                                                                                    *
%* Purpose  : Script for plotting the reaction of the aircraft modell to different input signals on   *
%*            the control surfaces.                                                                   *
%* Version  : 1.2                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : The file plotfunc.m is necessary for execution.                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Test_Plot
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 27-OCT-09 * Basic Design                                            *
%* Jobst Henning Diekmann       * 02-FEB-12 * Integration of BLAC Model structure                   *
%* Jobst Henning Diekmann       * 30-JUL-12 * Integration of Feedback Controller Input Values         *
%* Jobst Henning Diekmann       * 30-OKT-13 * Sliced Simulation including state vector manipulation   *
%* Jobst Henning Diekmann       * 15-APR-14 * Simulation MK I Test Plotting Function                  *
%*                                            loop über alle trimfiles                                *
%******************************************************************************************************
%% Simulation Initialization
% BLAC_Environment_Init.m
% Clear Workspace and initialize simulation variable structure in workspace

clear all
close all
clc

% rand('seed',33);
slCharacterEncoding('windows-1252'); 
FilePath    = mfilename('fullpath');
FolderName  = '/REF-2 Uncertain Freeze 12-12-2016/';
PathIndex   = strfind(FilePath,FolderName);

BasePath = FilePath(1:PathIndex-1);

% Go to root directory
cd([fileparts(fileparts(FilePath)), filesep, 'Tools']);

addpath(genpath([BasePath FolderName]))

% Result directory
% resultpath = [BasePath FolderName 'Tools/Results/'];
% new lines added by prem to set a new result path in order to save Results outside the Tools folder
ind=strfind(FilePath,'/');
resultpath = [FilePath(1:ind(end-1)) 'Results_test_rand_seed_33/'] ;
if ~exist(resultpath,'dir')
    mkdir(resultpath);
end

number_of_simus = 10;
uq_min = 1/100*[-10,-15,-20,-10,-15,-20,-15,-25,-25,-15,-20,-20,-15,-25,-25,-15,-20,-20,-15,-25,-25,-15,-25,-25,-5,-50,-10,-10,-10,-25,-25,-10,-25,-25,-15,-50,-10,-10,-10,-15,-10,-10,-15,-10,-10,-10,-20,-20];
uq_max = 1/100*[ 10, 15, 20, 10, 15, 20, 15, 25, 25, 15, 20, 20, 15, 25, 25, 15, 20, 20, 15, 25, 25, 15, 25, 25, 5, 50, 10, 10, 10, 25, 25, 10, 25, 25, 15, 10, 10, 10, 10, 15, 10, 10, 15, 10, 10,10, 20, 20];

%[UQomega]=create_UQomega(number_of_simus);

m = length(uq_max);
UQomega = zeros(number_of_simus,m);

%% Must be 2:number_of_simus and number_of_simus-1 below
 for i=1:m
  UQomega(1:number_of_simus,i) = rand(number_of_simus,1)*(uq_max(i) - uq_min(i)) + uq_min(i);
 end

% UQomega = zeros(2,48);
% UQomega(2,46) = 0.1;
% UQomega(2,47) = 1;
% UQomega(2,29:38) = [0.1 0.25 0.25 -0.1 -0.25 -0.25 0.15 -0.5 0.1 0.0];
% save('conv3.in', 'UQomega', '-ascii');

% UQomega = load('conv3.in');
Controller_active_run   = ones(1,number_of_simus);
SeeAllInfluences = 0;

% NrTrimpoint = 5:48
% run = 1:number_of_simus
% 

for NrTrimpoint = 23:23
 for run = 10:number_of_simus
  
% clear BLAC BLAC_Trim
    
tic
uq_vector = UQomega(run,:);

% TrimSettings.strTrimfile = 'Alt1000_V50_FPA0_Bank0_HF_12122016124608.mat'; % Link Trimfiles here

% SwitchTrimpoints;

%% Set Simulation Parameters

% Blowing Operative [0 Yes / 1 No]

%% Uncertainties

% myUncertainty

%% Simulation Configuration Parameters for additional model functions 

%% Run Simulation Model

run_simulation_pr(uq_vector, NrTrimpoint, run, Controller_active_run, FilePath, resultpath,ind, SeeAllInfluences);


% First Check

if SeeAllInfluences
%     close all

%     color_vec = [0 0 0 ; 0 0 1 ; 0 1 0 ; 1 0 0 ; 0 1 1 ; 1 0 1 ; 1 1 0 ; 0.99 0.40 0];
% 
%     figure(14)
%     set(gcf,'DefaultAxesColorOrder',color_vec)
%     plot(   YawingMoment(:,1), YawingMoment(:,[2 4 5 6 7 8 12 13]), 'LineWidth',3), grid on
%         xlabel('t [s]')
%         ylabel('C_n [-]')
%         legend( 'C_{n25}',...
%                 'C_{n\beta}',...
%                 'C_{np}',...
%                 'C_{nr}',...
%                 'C_{n\xi}',...
%                 'C_{n\zeta}',...
%                 'C_{ndT}',...
%                 'C_{nProp}',...
%                 'Location','NorthWest')
% 
%     figure(15)
%     set(gcf,'DefaultAxesColorOrder',color_vec)
%     plot(   RollingMoment(:,1), RollingMoment(:,[2 4 5 6 7 8 9 10]), 'LineWidth',3), grid on
%         xlabel('t [s]')
%         ylabel('C_l [-]')
%         legend( 'C_{l25}',...
%                 'C_{l\beta}',...
%                 'C_{lp}',...
%                 'C_{lr}',...
%                 'C_{l\xi}',...
%                 'C_{l\zeta}',...
%                 'C_{ldT}',...
%                 'C_{lProp}',...
%                 'Location','NorthWest')
% 
%     figure(16)
%     set(gcf,'DefaultAxesColorOrder',color_vec)
%     plot(   PitchingMoment(:,1), PitchingMoment(:,[15 2 6 7 8 10]), 'LineWidth',3), grid on
%         xlabel('t [s]')
%         ylabel('C_m [-]')
%         legend( 'C_{m25}',...
%                 'C_{mHTP}',...
%                 'C_{ma}',...
%                 'C_{mqFR}',...
%                 'C_{mProp}',...
%                 'C_{m\beta}',...
%                 'Location','NorthWest')
% 
%     figure(17)
%     set(gcf,'DefaultAxesColorOrder',color_vec)
%     plot(   LiftForce(:,1), LiftForce(:,[2 4 7 9 10 13 14 15]), 'LineWidth',3), grid on
%         xlabel('t [s]')
%         ylabel('C_L [-]')
%         legend( 'C_{L}',...
%                 'C_{LHTPges}',...
%                 'C_{La}',...
%                 'C_{LX}',...
%                 'C_{LT}',...
%                 'C_{L\eta}',...
%                 'C_{LHTP}',...
%                 'C_{L\beta}',...
%                 'Location','NorthWest')
% 
%     clear color_vec
end

    
    test = 1;
%     Plots2 =[{'beta_deg'};...
%             {'C_Y_e'};...
%             {'C_l25_e'};...
%             {'C_n25_e'}...
%             ];
% 
%     disp('Check Plots for:');
%     disp(Plots2);
% 
%     SizePlots2 = size(Plots2);
%     Plot_Vec2 = [];
% 
%     for i = 1 : SizePlots2(1,1)
%         for j = 1: 104
%             if strcmp(Result.(Case_Name){j}.Name,Plots2(i))
%                 Plot_Vec2 = [Plot_Vec2 j];
%                 Result.(Case_Name){Plot_Vec2(end)}.Name;
%             end
%         end
%     end
% 
%     color_vec = ['k';'r';'b';'g';'r';'m'];
% %     color_vec = [{[0 0 0]} ; {[0 0 1]} ; {[0 1 0]} ; {[1 0 0]} ; {[0 1 1]} ; {[1 0 1]} ; {[1 1 0]} ; {[0.99 0.40 0]}];
%     
%     if run ~=1
%         figure_established = 1;
%     else
%         figure_established = 0;
%         Ax2 = [];
%     end
%     z = run;                 % Color Code
%     figure(2)
%     Ax2 = Multi_Plot_beta(Result,Trial_Name,Case_Name,t_end, color_vec,z, Plot_Vec2, figure_established, Ax2, BLAC.Controller.Activated);
% %     fout = [pwd '\Plots\' Trial_Name];
% %     print(gcf, '-depsc2', fout)
%     
%     beta_art = -30 : 30;
%     Cn_Cmd   = beta_art * (pi/180) * Test_Parameters.Cnbeta_Desired ;
%     
%     omega_0_Level1  = 0.4;
%     
%     Ix  = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_xx;       % Inertia xx-component at about defined CG.[kgm^2]
%     Iz  = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_zz;       % Inertia xx-component at about defined CG.[kgm^2]
%     Izx = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_xz;       % Inertia xz-component at about defined CG.[kgm^2]
% 
%     Clbeta = diff(Result.(Trial_Name).(Case_Name){103}.Value) ./ diff(Result.(Trial_Name).(Case_Name){15}.Value.*(pi/180));
%     Cnbeta = diff(Result.(Trial_Name).(Case_Name){104}.Value) ./ diff(Result.(Trial_Name).(Case_Name){15}.Value);
%     X = Result.(Trial_Name).(Case_Name){109}.Value(1:end-1) * BLAC.Configuration.Geometry.S * BLAC.Configuration.Geometry.s/(Ix*Iz-Izx^2);
%     Cnb_Level1      = (omega_0_Level1^2 - Izx .* Clbeta .* X) ./ (Ix * X);
% %     Cnb_Level1      = (omega_0_Level1^2) ./ (X);
%     beta = Result.(Trial_Name).(Case_Name){15}.Value;
%     figure(4)
%     plot(beta(1:end-1), Cnb_Level1,'k',beta(1:end-1), Cnbeta, 'r')
%     
%     LineSize    = 1.2;
%     MarkerSize  = 5;
%     FontSize    = 11;
%     LegendSize  = FontSize-3;
% 
%     figure(3)
%     set( gcf ,  'Color','w', ...
%             'DefaultLineLineWidth',LineSize ,...
%             'DefaultLineMarkerSize',MarkerSize,...
%             'DefaultAxesFontName','times',...
%             'DefaultAxesFontSize',FontSize,...
%             'DefaultTextInterpreter','latex')
% 
% 
%     px  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){105}.Value + Result.(Trial_Name).(Case_Name){108}.Value,color_vec(run+3));
%     grid on
%     hold on
%     px1  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){108}.Value,'k-');
%     px3  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){105}.Value,'m-');
%     px2 = plot(beta_art, Cn_Cmd,'k--');
  end
end


% ---------------------------------------------------------------------------------------------------EOF
