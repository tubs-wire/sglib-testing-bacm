function [myroots,myindex] = f_matrixanalysis(sys,plotroots,displayroots,SavePlot)

color = {[0 0 1] ; [0 1 0] ; [1 0 0] ; [0 .5 .5] ; [1 0 1] ; [1 (165/255) 0] ; [0.99 0.40 0] ; [0.5 0.5 0] ; [0.6 0.3 0.8] ; [0.9 0.5 0.3] ; [0 0 0]};


VTAS_index  = 1;
alpha_index = 2;
beta_index  = 3;
p_index     = 4;
q_index     = 5;
r_index     = 6;
phi_index   = 7;
theta_index = 8;
psi_index   = 9;

MyStateNameLatex = [{'$$ V_{IAS} $$'}   ...
                    {'$$ \alpha $$'}    ...
                    {'$$ \beta $$'}     ...
                    {'$$ p $$'}         ...
                    {'$$ q $$'}         ...
                    {'$$ r $$'}         ...
                    {'$$ \Phi $$'}      ...
                    {'$$ \Theta $$'}    ...
                    {'$$ \Psi $$'} ];

ph_influence     = [3,4,6,7,9];
sp_influence     = [2,5,8];
dr_influence     = [3,4,6,7,9];
roll_influence   = [4,7,3];
spiral_influence = [7,9];
%%
[eigenvector,eigenval]                          = eig(sys.a);
eigenvalues                                     = eig(sys.a);
[mynaturalfrequency,mydamping,myeigenvalues]    = damp(sys);
[val1,index1]                                   = sort(myeigenvalues,'ascend');
[val2,index2]                                   = sort(eigenvalues,'ascend');

eigenvector(1,:) = eigenvector(1,:) .* (60/1000);     % V_IAS in km/min
for i = 1:9
    eigenvector(:,i) = eigenvector(:,i)/norm(eigenvector(:,i),2);
end

damping(index2)             = mydamping(index1);
naturalfrequency(index2)    = mynaturalfrequency(index1);
mystates                    = 1:length(eigenvalues);

for ii=mystates
    myroots(ii).eigenvalue = eigenvalues(ii);
    myroots(ii).eigenvector = eigenvector(:,ii);
end

%% search for duplicates
u=unique(abs(eigenvalues));
n=histc(abs(eigenvalues)',u);
my_u = u(n>1); 
myindex.dynamic_list = [];
myindex.aperiodic_list = [];
for ii=1:length(my_u)
    myindex.dynamic_roots(ii,:) = find(abs(eigenvalues)==my_u(ii));
    myindex.dynamic_list        = [myindex.dynamic_list, myindex.dynamic_roots(ii,:)];
    myroots(myindex.dynamic_roots(ii,1)).dynamic         = 1;
    myroots(myindex.dynamic_roots(ii,1)).aperiodic       = 0;
    myroots(myindex.dynamic_roots(ii,2)).dynamic         = 1;
    myroots(myindex.dynamic_roots(ii,2)).aperiodic       = 0;
    myroots(myindex.dynamic_roots(ii,2)).similar         = myindex.dynamic_roots(ii,1);
    myroots(myindex.dynamic_roots(ii,1)).similar         = myindex.dynamic_roots(ii,2);    
    myroots(myindex.dynamic_roots(ii,1)).plot            = 1;
    myroots(myindex.dynamic_roots(ii,2)).plot            = 1; 
    myroots(myindex.dynamic_roots(ii,1)).damping         = damping(myindex.dynamic_roots(ii,1));
    myroots(myindex.dynamic_roots(ii,2)).damping         = damping(myindex.dynamic_roots(ii,2));
    myroots(myindex.dynamic_roots(ii,1)).frequency       = naturalfrequency(myindex.dynamic_roots(ii,1));
    myroots(myindex.dynamic_roots(ii,2)).frequency       = naturalfrequency(myindex.dynamic_roots(ii,2));
end

myindex.aperiodic_roots  = find(ismember(mystates,myindex.dynamic_list)==0);

for ii=1:length(myindex.aperiodic_roots)
    myindex.aperiodic_list        = [myindex.aperiodic_list, myindex.aperiodic_roots(ii)];
    myroots(myindex.aperiodic_roots(ii)).dynamic         = 0;
    myroots(myindex.aperiodic_roots(ii)).aperiodic       = 1;
    myroots(myindex.aperiodic_roots(ii)).plot            = 1;
    myroots(myindex.aperiodic_roots(ii)).similar        = 0;
end

for ii=mystates
    myroots(ii).use = 1;
end


myindex.all_roots_index = [myindex.aperiodic_roots, myindex.dynamic_roots(:,1)'];

%% Labels
for ii=mystates
    myname = ['$$ ' sys.StateName{ii} ' $$'];
    myname = myname(4:findstr(myname,'[')-2);
    mystatename{ii} = myname;
end
%% Find Roots???
abs_eigenvector = abs(eigenvector);
myindex.roots_index = [];
for ii=myindex.all_roots_index
    helpindex = find(abs_eigenvector(:,ii)==1);
    if isempty(helpindex)
        myindex.roots_index = [myindex.roots_index,ii];
    else
        myroots(ii).use  = 0;
        myroots(ii).plot = 0;
        if myroots(ii).similar~=0
            myroots(myroots(ii).similar).plot = 0;
            myroots(myroots(ii).similar).use  = 0;
        end
    end
end
myindex.rootsize = length(myindex.roots_index);

%%
myindex.plot_roots = [];
for ii=mystates
    if myroots(ii).plot==1
        myindex.plot_roots = [myindex.plot_roots,ii];
    end
end

% figure
% set( gcf ,  'Color','w', ...
%             'DefaultLineLineWidth',1.5 ,...
%             'DefaultLineMarkerSize',8,...
%             'DefaultAxesFontName','times',...
%             'DefaultAxesFontSize',12)
% 
% % s = 1.1 * max(1,norm(sys.a));
% % 
% % axis([-s s -s s])
% axis square
% color = {[0 0 0] ; [0 0 1] ; [0 1 0] ; [1 0 0] ; [0 .5 .5] ; [1 0 1] ; [1 (165/255) 0] ; [0.99 0.40 0] ; [0.5 0.5 0] ; [0.6 0.3 0.8] ; [0.9 0.5 0.3]};
% 
% hold on, grid on, box off
% for ii = 1 : 9 
%     subplot(3,3,ii)
%     for i = 1 : 9
%         if real(eigenvector(i,ii)) ~= 0
%             h.Ax = initv([real(eigenvector(i,ii)) imag(eigenvector(i,ii))],mystatename{i},color{i});
%         end
%     end
% end
% 

%% check roots about corresponding motion
check_abs_eigenvector = abs_eigenvector(:,myindex.roots_index);
[states_length motion_length] = size(check_abs_eigenvector);
Motions_index = 1 : motion_length;
Remaining_Motions = Motions_index;
Motions_Found = [];

%% Find Longitudinal Motion
% Phugoid
[maxvalue,maxindex] = max(check_abs_eigenvector(VTAS_index,:));                     % Check Phugoid for V_TAS as most affecting value in the eigenvector
phugoid_index       = myindex.roots_index(maxindex);
myroots(phugoid_index).motion                   = 'phugoid';
myroots(myroots(phugoid_index).similar).motion  = 'phugoid';
myindex.motion.phugoid_index = [phugoid_index, myroots(phugoid_index).similar];
Motions_Found = [Motions_Found maxindex];
Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));

% Short Period
[maxvalue,maxindex] = max(check_abs_eigenvector(q_index,Remaining_Motions));
maxindex            = Remaining_Motions(maxindex);
shortperiod_index   = myindex.roots_index(maxindex);
[sp_eigvec_val, sp_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
if ~isempty(find(sp_eigvec_orderindex(1:4)==1,1))
    vec_pos = find(ismember(sp_eigvec_orderindex(1:4),1)==0);
    [bla,check] = ismember(sp_eigvec_orderindex(vec_pos),sp_influence);
else
    [bla,check] = ismember(sp_eigvec_orderindex(1:3),sp_influence);
end
if ~isempty(find(check==0,1))
%     disp('not all influences found for short period! - Re-Check')
    [sorted_eigvec, i_sorted_eigvec] = sort(check_abs_eigenvector(q_index,Remaining_Motions),'descend');
    maxindex = Remaining_Motions(i_sorted_eigvec(2));
    shortperiod_index   = myindex.roots_index(maxindex);
    [sp_eigvec_val, sp_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
    if ~isempty(find(sp_eigvec_orderindex(1:4)==1,1))
        vec_pos = find(ismember(sp_eigvec_orderindex(1:4),1)==0);
        [bla,check] = ismember(sp_eigvec_orderindex(vec_pos),sp_influence);
    else
        [bla,check] = ismember(sp_eigvec_orderindex(1:3),sp_influence);
    end
end
if ~isempty(find(check==0,1))
%     disp('not all influences found for short period! - Final')
end
if myroots(shortperiod_index).similar==0
%     disp('Aperiodic short period eigenvalue!')
    myroots(shortperiod_index).motion                   = 'short period';
    myindex.motion.shortperiod_index = shortperiod_index;
else
    myroots(shortperiod_index).motion                   = 'short period';
    myroots(myroots(shortperiod_index).similar).motion  = 'short period';
    myindex.motion.shortperiod_index = [shortperiod_index, myroots(shortperiod_index).similar];
end
Motions_Found = [Motions_Found maxindex];
Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));

%% Lateral Motion
if motion_length > 4
    % Dutch Roll
    [maxvalue,maxindex] = max(check_abs_eigenvector(beta_index,Remaining_Motions));
    maxindex            = Remaining_Motions(maxindex);
    dutchroll_index     = myindex.roots_index(maxindex);
    [dr_eigvec_val, dr_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
    if ~isempty(find(dr_eigvec_orderindex(1:6)==1,1))
        vec_pos = find(ismember(dr_eigvec_orderindex(1:6),1)==0);
        [bla,check] = ismember(dr_eigvec_orderindex(vec_pos),dr_influence);
    else
        [bla,check] = ismember(dr_eigvec_orderindex(1:5),dr_influence);
    end
    if ~isempty(find(check==0,1))
%         disp('not all influences found for dutch roll! Re-Check')
        [sorted_eigvec, i_sorted_eigvec] = sort(check_abs_eigenvector(beta_index,Remaining_Motions),'descend');
        maxindex = Remaining_Motions(i_sorted_eigvec(2));
        dutchroll_index     = myindex.roots_index(maxindex);
        [dr_eigvec_val, dr_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
        if ~isempty(find(dr_eigvec_orderindex(1:6)==1,1))
            vec_pos = find(ismember(dr_eigvec_orderindex(1:6),1)==0);
            [bla,check] = ismember(dr_eigvec_orderindex(vec_pos),dr_influence);
        else
            [bla,check] = ismember(dr_eigvec_orderindex(1:5),dr_influence);
        end
    end
    if ~isempty(find(check==0,1))
%         disp('not all influences found for dutch roll! Final')
    end
    if myroots(dutchroll_index).similar==0
%         disp('Beta points to aperiodic dutch roll eigenvalue!')
        myroots(dutchroll_index).motion                   = 'dutch roll';
        myindex.motion.dutchroll_index = dutchroll_index;
    else
        myroots(dutchroll_index).motion                   = 'dutch roll';
        myroots(myroots(dutchroll_index).similar).motion  = 'dutch roll';
        myindex.motion.dutchroll_index = [dutchroll_index, myroots(dutchroll_index).similar];
    end
    Motions_Found = [Motions_Found maxindex];
    Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));

    % Rolling Motion
    [maxvalue,maxindex] = max(check_abs_eigenvector(p_index,Remaining_Motions));
    maxindex            = Remaining_Motions(maxindex);
    rolling_index       = myindex.roots_index(maxindex);
    [rm_eigvec_val, rm_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend'); 
    if ~isempty(find(rm_eigvec_orderindex(1:5)==1,1))
        vec_pos = find(ismember(rm_eigvec_orderindex(1:5),1)==0);
        [bla,check] = ismember(rm_eigvec_orderindex(vec_pos),roll_influence);
    else
        [bla,check] = ismember(rm_eigvec_orderindex(1:4),roll_influence);
    end
    if length(find(check==0,2))>1
%         disp('not all influences found for dutch roll! Re-Check')
        [sorted_eigvec, i_sorted_eigvec] = sort(check_abs_eigenvector(p_index,Remaining_Motions),'descend');
        maxindex            = Remaining_Motions(i_sorted_eigvec(2));
        rolling_index       = myindex.roots_index(maxindex);
        [rm_eigvec_val, rm_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
        if ~isempty(find(rm_eigvec_orderindex(1:5)==1,1))
            vec_pos = find(ismember(rm_eigvec_orderindex(1:5),1)==0);
            [bla,check] = ismember(rm_eigvec_orderindex(vec_pos),roll_influence);
        else
            [bla,check] = ismember(rm_eigvec_orderindex(1:4),roll_influence);
        end
        if length(find(check==0,2))>1
%             disp('not all influences found for dutch roll! Final')
        end
    end
    if myroots(rolling_index).similar~=0
%         disp('p points to dynamic rolling motion eigenvalue!');
        [sortvalue,sortindex] = sort(check_abs_eigenvector(p_index,:),'descend');
        [maxvalue_star,maxindex_star] = max(check_abs_eigenvector(phi_index,:));
        if sortindex(2) == maxindex_star
            rolling_index    = myindex.roots_index(sortindex(2));
            maxindex = maxindex_star;
%             disp('--> Solved with roll angle crosscheck!');
        else
%             disp('Could not be solved with roll angle crosscheck!');    
        end
    end
    [rol_eigvec_val, rol_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
    [bla,check] = ismember(rol_eigvec_orderindex(1:2),roll_influence);
    if ~isempty(find(check==0,1))
%         disp('not all influences found for rolling motion!');
    end
    myroots(rolling_index).motion                   = 'rolling motion';
    myindex.motion.rolling_index = rolling_index;
    Motions_Found = [Motions_Found maxindex];
    Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));

    % Spiral Motion
    [maxvalue,maxindex] = max(check_abs_eigenvector(psi_index,Remaining_Motions));
    maxindex            = Remaining_Motions(maxindex);
    spiral_index   = myindex.roots_index(maxindex);
    [spiral_eigvec_val, spiral_eigvec_orderindex]=sort(check_abs_eigenvector(:,maxindex),'descend');
    if ~isempty(find(spiral_eigvec_orderindex(1:3)==1,1))
        vec_pos = find(ismember(spiral_eigvec_orderindex(1:3),1)==0);
        [bla,check] = ismember(spiral_eigvec_orderindex(vec_pos),spiral_influence);
    else
        [bla,check] = ismember(spiral_eigvec_orderindex(1:2),spiral_influence);
    end
    % [bla,check] = ismember(spiral_eigvec_orderindex(1:2),spiral_influence);
    if ~isempty(find(check==0,1))
%         disp('not all influences found for spiral motion!')
    end
    if myroots(spiral_index).similar~=0
%         disp('psi points to dynamic spiral motion eigenvalue!')
    end
    myroots(spiral_index).motion                   = 'spiral motion';
    myindex.motion.spiral_index = spiral_index;

else
% Dutch Roll
[maxvalue,maxindex] = max(check_abs_eigenvector(alpha_index,Remaining_Motions));
maxindex            = Remaining_Motions(maxindex);
dutchroll_index     = myindex.roots_index(maxindex);

myroots(dutchroll_index).motion                   = 'dutch roll';
myroots(myroots(dutchroll_index).similar).motion  = 'dutch roll';
myindex.motion.dutchroll_index = [dutchroll_index, myroots(dutchroll_index).similar];
Motions_Found = [Motions_Found maxindex];
Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));

% Rolling/Spiral Dynamic Motion
[maxvalue,maxindex] = max(check_abs_eigenvector(psi_index,Remaining_Motions));
maxindex            = Remaining_Motions(maxindex);
rolling_index       = myindex.roots_index(maxindex);

myroots(rolling_index).motion                   = 'rolling/spiral motion';
myroots(myroots(rolling_index).similar).motion  = 'rolling/spiral motion';
myindex.motion.rolling_index = [rolling_index, myroots(rolling_index).similar];
Motions_Found = [Motions_Found maxindex];
Remaining_Motions = Motions_index(find(ismember(Motions_index, Motions_Found)==0));
end




%%
if displayroots
    if motion_length > 4
        disp('*******************************************************');
        disp(['Phugoid motion -> Eigenvalue        :',num2str(myroots(myindex.motion.phugoid_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.phugoid_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.phugoid_index(1)).frequency)]);
        disp(' ');
        disp(['Short Period   -> Eigenvalue        :',num2str(myroots(myindex.motion.shortperiod_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.shortperiod_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.shortperiod_index(1)).frequency)]);
        disp(' ');
        disp(['Dutch Roll     -> Eigenvalue        :',num2str(myroots(myindex.motion.dutchroll_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.dutchroll_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.dutchroll_index(1)).frequency)]); 
        disp(' ');
        disp(['Rolling motion -> Eigenvalue        :',num2str(myroots(myindex.motion.rolling_index).eigenvalue)]);
        disp(' ');
        disp(['Spiral motion  -> Eigenvalue        :',num2str(myroots(myindex.motion.spiral_index).eigenvalue)]);
        disp('*******************************************************');
    else
                disp('*******************************************************');
        disp(['Phugoid motion -> Eigenvalue        :',num2str(myroots(myindex.motion.phugoid_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.phugoid_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.phugoid_index(1)).frequency)]);
        disp(' ');
        disp(['Short Period   -> Eigenvalue        :',num2str(myroots(myindex.motion.shortperiod_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.shortperiod_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.shortperiod_index(1)).frequency)]);
        disp(' ');
        disp(['Dutch Roll     -> Eigenvalue        :',num2str(myroots(myindex.motion.dutchroll_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.dutchroll_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.dutchroll_index(1)).frequency)]); 
        disp(' ');
        disp(['Rolling/Spiral -> Eigenvalue        :',num2str(myroots(myindex.motion.rolling_index(1)).eigenvalue)]);
        disp(['               -> Damping           : ',num2str(myroots(myindex.motion.rolling_index(1)).damping)]);
        disp(['               -> Natural Frequency : ',num2str(myroots(myindex.motion.rolling_index(1)).frequency)]); 
        disp(' ');
        disp('*******************************************************');
    end
end
%% Plotting

myindex.plot_aperiodic_list     = myindex.aperiodic_list(find(ismember(myindex.aperiodic_list,myindex.plot_roots) == 1));
myindex.plot_dynamic_list       = myindex.dynamic_list(find(ismember(myindex.dynamic_list,myindex.plot_roots) == 1));
myindex.plot_aperiodic_roots    = myindex.aperiodic_list(find(ismember(myindex.aperiodic_roots,myindex.plot_roots) == 1));

LineSize = 1.5;
FontSize = 12;
MarkerSize = 7;
LegendFont = FontSize-4;

if plotroots
    figure(815)
    set( gcf ,  'Color','w', ...
                'DefaultLineLineWidth',1.5 ,...
                'DefaultLineMarkerSize',8,...
                'DefaultAxesFontName','times',...
                'DefaultAxesFontSize',12)


    plotNr = 1;
    for ii = 1 : 9 
        if abs(eigenval(ii,ii)) ~= 0
            if ii > 1 && abs(eigenval(ii,ii)) ~= abs(eigenval(ii-1,ii-1))
                Ax      = subplot(3,3,plotNr);
                title(['Eigenvalue :'])
                set(gca,'Title',text('String', ['Eigenvalue: $$ ' num2str(real(eigenval(ii,ii))) '\pm ' num2str(abs(imag(eigenval(ii,ii)))) 'i $$'], 'Interpreter','latex', 'FontSize', FontSize))
                hold on, grid on, box off
                for ik = 1 : 9
                    h.Ax    = bar(mystates(ik), abs_eigenvector(ik,ii));
                    set(h.Ax, 'FaceColor', color{ik})
                end
                xlim([.5 9.5])
                set(Ax,'XTick',[1:9],'XTickLabel',[MyStateNameLatex]);
                plotTickLatex2D
                plotNr = plotNr +1;
            end
        end
    end

    figure(471532),clf
    hold on
    for ii=1:myindex.rootsize
        LineSize    = 1.5;
        set( gcf ,  'Color','w', ...
                    'DefaultLineLineWidth',LineSize ,...
                    'DefaultLineMarkerSize',MarkerSize,...
                    'DefaultAxesFontName','times',...
                    'DefaultAxesFontSize',FontSize,...
                    'DefaultTextInterpreter','latex')
        Ax = subplot(ceil(myindex.rootsize/2),2,ii);
        box off, hold on, grid on
        for ik = 1 : 9
            h.Ax2    =         bar(mystates(ik), abs_eigenvector(ik,myindex.roots_index(ii)));
            set(h.Ax2, 'FaceColor', color{ik})
        end
%         bar(mystates, abs_eigenvector(:,myindex.roots_index(ii)));
        xlim([0.5 9.5])
        title(myroots(myindex.roots_index(ii)).motion);
        set(gca,'Title',text('String', myroots(myindex.roots_index(ii)).motion, 'Interpreter','latex', 'FontSize', FontSize))

        set(Ax,'XTick',[1:9],'XTickLabel',[MyStateNameLatex]);
        plotTickLatex2D
%         set(gca,'Interpreter','latex')    
    end
    if SavePlot
        Savename = ['Sideslip_Eigenmotion_Dependencies'];
        fout = [pwd '\PublicationPlots\' Savename];
        print(gcf, '-depsc2', fout)
    end

    l1_Parts = [];
    figure(471533),clf
    set( gcf ,  'Color','w', ...
                'DefaultLineLineWidth',LineSize ,...
                'DefaultLineMarkerSize',MarkerSize,...
                'DefaultAxesFontName','times',...
                'DefaultAxesFontSize',FontSize)
    Ax = subplot(1,1,1);
    box off
    grid on
    hold on
    indi = 1 : length(myindex.dynamic_roots(:,1));
    indj = 1 : length(myindex.plot_aperiodic_roots);
    
    colorcode = 1;
    for ii = indi
        plot(eigenvalues(myindex.dynamic_roots(ii,1)),'x','MarkerFaceColor',color{colorcode},'MarkerEdgeColor',color{colorcode})
        colorcode = colorcode + 1;
        l1_Parts = [l1_Parts; {myroots(myindex.dynamic_roots(ii,1)).motion}];
    end
    for jj = indj
        plot(eigenvalues(myindex.plot_aperiodic_roots(jj)), 0 ,'x','MarkerFaceColor',color{colorcode},'MarkerEdgeColor',color{colorcode})
        colorcode = colorcode + 1;
        if eigenvalues(myindex.plot_aperiodic_roots(jj)) == 0
            l1_Parts = [l1_Parts; {''}];
        elseif isempty(myroots(myindex.plot_aperiodic_roots(jj)).motion)
            l1_Parts = [l1_Parts; {''}];
        else
            l1_Parts = [l1_Parts; {myroots(myindex.plot_aperiodic_roots(jj)).motion}];
        end
    end
    colorcode = 1;
    for ii = indi
        plot(eigenvalues(myindex.dynamic_roots(ii,1)) ,'x','MarkerFaceColor',color{colorcode},'MarkerEdgeColor',color{colorcode})
        plot(eigenvalues(myindex.dynamic_roots(ii,2)) ,'x','MarkerFaceColor',color{colorcode},'MarkerEdgeColor',color{colorcode})
        colorcode = colorcode + 1;
    end
    
%     xlim([-2,2]);
    axis square;
    xlabel('$$ \sigma $$','Interpreter','latex')
    ylabel('$$ i \omega $$','Interpreter','latex')
    set(Ax,'units','normalized','Position',[.22 .2 .75 .75]);
    l1 = legend(    l1_Parts,'Location','Best');
                    set(l1, 'FontSize', LegendFont, 'Interpreter','latex')
    Axpar1 = get(Ax,{'xlim','ylim'});
    L22 = line(   Axpar1{1,1}, [0 0], 'Color', 'k','LineWidth',0.5*LineSize);                
    Axpar1 = get(Ax,{'xlim','ylim'});
    L21 = line(   [0 0], Axpar1{1,2}, 'Color', 'k','LineWidth',0.5*LineSize);                
    uistack(L21, 'bottom')
    uistack(L22, 'bottom')
    if SavePlot
        Savename = ['Sideslip_Poles'];
        fout = [pwd '\PublicationPlots\' Savename];
        print(gcf, '-depsc2', fout)
    end


end
end
% function h = initv(v,t,color)
%         h.mark = line(v(1),v(i),'marker','.','erase','none','color',color(1));
%         hold on, grid on, box off
%         h.line = line([0 v(1)],'erase','xor','color',color(1));
%         h.text = text(v(1)/2,v(i)/2,t,'fontsize',12,'erase','xor','color',color(1));
%     for i = 2 : length(v)
%         h.mark = line(v(1),v(i),'marker','.','erase','none','color',color(i));
%         h.line = line([0 v(1)],[0 v(i)],'erase','xor','color',color(i));
%         h.text = text(v(i)/2,v(i)/2,t,'fontsize',12,'erase','xor','color',color(i));
%     end
% end
function h = initv(v,t,color)
    h.mark = line(v(1),v(2),'marker','.','erase','none','color',color);
    h.line = line([0 v(1)],[0 v(2)],'erase','xor','color',color);
    h.text = text(v(1)/2,v(2)/2,t,'fontsize',12,'erase','xor','color',color);
end