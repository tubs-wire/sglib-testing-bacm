function [BLAC] = set_config_params_model_funcs_pr(TrimPointInfo, BLAC, uq_vector, BLAC_Trim, Controller_active_run, run)



% Set Cmu Parameters
BLAC.Aerodynamics.Prop.On                   = TrimPointInfo.SetSlipstream;
BLAC.Aerodynamics.Active_HTP.On             = TrimPointInfo.SetHTPactive;
BLAC.Aerodynamics.Active_HTP.Performance    = 100;                          % Performance in [%]
BLAC.Aerodynamics.Cmu.Cmu_Var_Flg           = 0;
BLAC.Aerodynamics.Cmu.Cmu_Const             = TrimPointInfo.C_mu/2;
BLAC.Aerodynamics.Cmu.Comp_Vec              = TrimPointInfo.SetCmuDistrib;
BLAC.Aerodynamics.Droop.Active              = TrimPointInfo.SetDroop;
BLAC.Aerodynamics.Inputs.Lat_Motion         = TrimPointInfo.SetLateral;
BLAC.Propulsion.Prop_Efficiency             = 1;
BLAC.Aerodynamics.Lift.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Drag.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Pitch.CrossCouplFlag      = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Roll.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Yaw.CrossCouplFlag        = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Side.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Propulsion.Mode                        = TrimPointInfo.Engine_Mode;

% Switch Slipstream Effects on/off
BLAC.Aerodynamics.Slipstream.On             = 1;

% Blowing System Failure
% Test_Parameters.ILS_GS                      = -3.5 * pi/180;          
% Test_Parameters.V_Approach                  = 0.15 * 340.294;
BLAC.Aerodynamics.Fail.Deactivated          = 0;                        % Throttle Step Final Value for AHLS Failure 
BLAC.Aerodynamics.Fail.Time                 = BLAC.Sim.EndTime;         % Time of AHLS Failure 
BLAC.Aerodynamics.Fail.Time_Control         = BLAC.Sim.EndTime;         % Time of Controller Initialization
BLAC.Aerodynamics.Cmu.Cmu_Fail              = TrimPointInfo.C_mu/2;     % Jet Momentum Coeff after AHLS Failure
BLAC.Environment.Wind.Turbulence_Flag       = 0;
BLAC.Environment.Wind.Turbulence_Intensity_Index = 1;
BLAC.Environment.Terrain_Elevation          = -50;

%% Aileron Efficiency
BLAC.Aerodynamics.Roll.C_lksi(3)            = -0.24; %BLAC.Aerodynamics.Roll.C_lksi(3) * 2.5; % Apply different C_lxi Aileron efficiencies here 
                                                                                % Default Model Value:
                                                                                % BLAC.Aerodynamics.Roll.C_lksi(3) =
                                                                                % 0.09
                                                                                % C_lxi(ATTAS) = 0.24
Actuator_Rate = 30; 
                                                                                
BLAC.Actuator.Surfaces.Rudder.max           =  30 * pi/180;      % Min. Rudder deflection angle                     [rad]
BLAC.Actuator.Surfaces.Rudder.min           = -30 * pi/180;      % Min. Rudder deflection angle                     [rad]
BLAC.Actuator.Surfaces.Rudder.rate_max      =  Actuator_Rate * pi/180;      % Max. rate of Rudder deflection                 [rad/s]
BLAC.Actuator.Surfaces.Rudder.rate_min      = -Actuator_Rate * pi/180;      % Min. rate of Rudder deflection                 [rad/s]

BLAC.Actuator.Surfaces.Elevator.max         =  30 * pi/180;      % Min. Elevator deflection angle                     [rad]
BLAC.Actuator.Surfaces.Elevator.min         = -30 * pi/180;      % Min. Elevator deflection angle                     [rad]
BLAC.Actuator.Surfaces.Elevator.rate_max    =  Actuator_Rate * pi/180;      % Max. rate of Elevator deflection                 [rad/s]
BLAC.Actuator.Surfaces.Elevator.rate_min    = -Actuator_Rate * pi/180;      % Min. rate of Elevator deflection                 [rad/s]
BLAC.Actuator.Booster.Elevator.omega_0      = 60;   

BLAC.Actuator.Surfaces.Aileron.max          =  30 * pi/180;      % Min. Aileron deflection angle                     [rad]
BLAC.Actuator.Surfaces.Aileron.min          = -30 * pi/180;      % Min. Aileron deflection angle                     [rad]
BLAC.Actuator.Surfaces.Aileron.rate_max     =  Actuator_Rate * pi/180;      % Max. rate of Aileron deflection                 [rad/s]
BLAC.Actuator.Surfaces.Aileron.rate_min     = -Actuator_Rate * pi/180;      % Min. rate of Aileron deflection                 [rad/s]

%% Crosswind
BLAC.Environment.Wind.v_w_active            = 0;                        % Activation of Crosswind Components
BLAC.Environment.Wind.u_Wreq                = 0;                % Change Crosswind Component Value here, 5.144 [m/s] = 10kts
BLAC.Environment.Wind.psi_Wreq              = 90 * pi/180;              % Wind Direction 
BLAC.Environment.Wind.u_W_Step              = 0; %8;
BLAC.Environment.Wind.u_W_Step_Time         = 5;
BLAC.Environment.Wind.v_W_Step              = 5.144 * 2;
BLAC.Environment.Wind.v_W_Step_Time         = 2;
BLAC.Environment.Wind.w_W_Step              = 0; %4;
BLAC.Environment.Wind.w_W_Step_Time         = 10;

%% Controller Gains
BLAC.Controller.Activated = Controller_active_run(run);                          % Major Activation Switch for all Controllers
% BLAC.Controller.Activated = 1;

% Basic Rate Controllers
% Roll Damper
BLAC.Controller.Aileron_pControl_P      =  -5.0 * 1 * BLAC.Controller.Activated; % K_P,p
% Pitch Damper
BLAC.Controller.Elevator_qControl_P     =   3.0 * 1 * BLAC.Controller.Activated; % K_P,q
% Yaw Damper
BLAC.Controller.Rudder_rControl_P       =   2.5 * 1 * BLAC.Controller.Activated; % K_P,r

% Dynamic Inversion
BLAC.Controller.Cnbeta_Desired          =   1.0;                                    % C_nbeta,Cmd      Example: Cnbeta(ATTAS)= 0.4
BLAC.Controller.Rudder_betInvContr_P    =   1.0 * 1 * BLAC.Controller.Activated;            % K_Cnbeta
BLAC.Controller.Rudder_betInvContr_I    =  -1.5 * 1 * BLAC.Controller.Activated;            % K_beta_I

BLAC.Controller.Elevator_betInvContr_P  =   1.0 * 1 * BLAC.Controller.Activated;            % K_Cmbeta
BLAC.Controller.Elevator_LifInvContr_P  =  -1.0 * 0 * BLAC.Controller.Activated;            % K_CLbeta
BLAC.Controller.Elevator_LifInvDelayOn  =         1 * BLAC.Controller.Activated;            % Delay CLbeta

BLAC.Controller.Aileron_betInvContr_P   =   1.0 * 0 * BLAC.Controller.Activated;            % K_Clbeta

% Input Uncertainties
%BLAC.Controller.Thrust_Uncertainty      =   1 + ( 0.10 * Uncertainty_Killswitch(run));  % Thrust Uncertainty
%BLAC.Controller.alpha_Uncertainty       =   1 + ( 1 * Uncertainty_Killswitch(run));  % alpha Uncertainty
%BLAC.Controller.beta_Uncertainty        =   1 + ( 0.00 * Uncertainty_Killswitch(run));  % beta Uncertainty
BLAC.Controller.Thrust_Uncertainty      =   1 + uq_vector(46);  % Thrust Uncertainty
BLAC.Controller.alpha_Uncertainty       =   1 + uq_vector(47);  % alpha Uncertainty
BLAC.Controller.beta_Uncertainty        =   1 + uq_vector(48);  % beta Uncertainty

% Bank Angle Controller
BLAC.Controller.Aileron_PhiContr_P      =  -2  * 1 * BLAC.Controller.Activated;           % K_P,Phi Best Value: -1

% Controller Delay
BLAC.Actuator.Booster.Elevator.Delay    =   0.09; %.2; % Std Val: 0.09 s  
BLAC.Actuator.Booster.Aileron.Delay     =   0.09; %.2; % Std Val: 0.09 s   
BLAC.Actuator.Booster.Rudder.Delay      =   0.09; %.2; % Std Val: 0.09 s   

BLAC.Controller.Elevator_omega          =  (BLAC.Actuator.Booster.Elevator.omega_0) ...
                                        *  sqrt(1-(BLAC.Actuator.Booster.Elevator.Damping)^2);  % Not relevant
BLAC.Controller.Elevator_T              =   2*pi/BLAC.Controller.Elevator_omega;                 % Not relevant
BLAC.Controller.dt_Elevator_Predict     = -(BLAC.Actuator.Booster.Elevator.Delay...
                                        +   0.25 * BLAC.Controller.Elevator_T);                  % Not relevant

% Pitch Attitude Controller
BLAC.Controller.Elevator_ThetaContr_P   =   1.0 * 0 * BLAC.Controller.Activated;            % Not relevant
BLAC.Controller.Theta_Cmd_Type          =   1;
BLAC.Controller.Theta_Cmd_T_step        =   500;
BLAC.Controller.Theta_Cmd               =   BLAC_Trim.Init.Y0(4);
BLAC.Controller.Theta_Cmd_Freq          =   1/4 * 2/pi;
BLAC.Controller.Theta_Cmd_Amp           =   1;

% Counteract Coupling Effects from Control Surfaces
BLAC.Controller.Zeta2XiCoupling         =   1;                                      % Not relevant              

% Phi Command
BLAC.Controller.t_Test_Start            =   0;                                      % Not relevant
BLAC.Controller.Phi_Test_Target         =   0 * pi/180;                             % Not relevant

% Phase Compensated Rate Limiters
BLAC.Controller.Aileron.rate_max        =  30 * pi/180; % Not relevant
BLAC.Controller.Aileron.rate_min        = -30 * pi/180; % Not relevant
BLAC.Controller.Rudder.rate_max         =  30 * pi/180; % Not relevant
BLAC.Controller.Rudder.rate_min         = -30 * pi/180; % Not relevant
BLAC.Controller.Elevator.rate_max       =  30 * pi/180; % Not relevant
BLAC.Controller.Elevator.rate_min       = -30 * pi/180; % Not relevant

BLAC.Controller.RateLimiterBypass       =   0;          % Not relevant

BLAC.Controller.beta_Init               = BLAC_Trim.Init.Y0(6);
BLAC.Controller.theta_Init              = BLAC_Trim.Init.Y0(4);
BLAC.Controller.phi_Init                = BLAC_Trim.Init.Y0(7);
BLAC.Controller.p_Init                  = BLAC_Trim.Init.Y0(9);
BLAC.Controller.q_Init                  = BLAC_Trim.Init.Y0(3);
BLAC.Controller.r_Init                  = BLAC_Trim.Init.Y0(10);

% Controller Initialization Time
BLAC.Controller.Time_Control            = 500;      % Not used
BLAC.Controller.Control                 = 0;        % Not Used



end

