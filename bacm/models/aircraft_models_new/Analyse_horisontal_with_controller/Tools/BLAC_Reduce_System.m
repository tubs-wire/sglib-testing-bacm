function [ReducedSys,ReducedSysACT,ReducedSysLon,ReducedSysLat, ReducedSysLonA, ReducedSysLatA] = BLAC_Reduce_System(BLAC_Trim, BLAC, Checkplot,PlotsOn)
%
%
A = BLAC_Trim.LinSys.A;
B = BLAC_Trim.LinSys.B;
C = BLAC_Trim.LinSys.C;
D = BLAC_Trim.LinSys.D;

%% Reduce System Matrix 
ReducedSys = [];
%% Calculate Cm_alpha_dot, M_alpha_dot and M_w_dot
i_Flaps = find(BLAC.Aerodynamics.Flaps.Setting == BLAC_Trim.Actuator.Flaps_Init); 

if BLAC_Trim.Init.Y0(14) <= 0.033
    depdal      = ( BLAC.Aerodynamics.Downwash.depsdCLFR +   BLAC.Aerodynamics.Downwash.depsdCL_Fl(i_Flaps)) ...
                * ( BLAC.Aerodynamics.Lift.C_LFRa ...
                +   BLAC.Aerodynamics.Lift.dC_LadFL(i_Flaps) ...
                +   BLAC.Aerodynamics.Lift.dC_LadCmu_BLC(i_Flaps) * BLAC_Trim.Init.Y0(14));
else
    depdal      = ( BLAC.Aerodynamics.Downwash.depsdCLFR +   BLAC.Aerodynamics.Downwash.depsdCL_Fl(i_Flaps)) ...
                * ( BLAC.Aerodynamics.Lift.C_LFRa ...
                +   BLAC.Aerodynamics.Lift.dC_LadFL(i_Flaps) ...
                +   BLAC.Aerodynamics.Lift.dC_LadCmu_CC(i_Flaps) * BLAC_Trim.Init.Y0(14));
end

CL_alh      = BLAC.Aerodynamics.Lift.C_La_H;
Ma          = BLAC_Trim.Init.Y0(15);
pgCL_alh    = sqrt(1-0.15^2)/sqrt(1-Ma^2);
q_bar       = BLAC_Trim.Init.Y0(25);
qS          = BLAC_Trim.Init.Y0(25)*BLAC.Configuration.Geometry.S;
VTAS        = BLAC_Trim.Init.Y0(1);
xcg_rp      = BLAC_Trim.Weight_Balance.AC_Empty.CG.x_rp;
I_yy        = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_yy;

CL_alh_eff      = CL_alh /(sqrt(1-(pgCL_alh*Ma^2)));
delta_x_cg_rp   = xcg_rp - BLAC.Configuration.Geometry.x_rp;
r_H             = BLAC.Configuration.Geometry.x_LH + delta_x_cg_rp;
Cm_aldot        = -CL_alh_eff*(BLAC.Configuration.Geometry.S_H/BLAC.Configuration.Geometry.S)...
                *((BLAC.Configuration.Geometry.x_LH*r_H)/BLAC.Configuration.Geometry.l_mac^2)...
                *depdal;
M_aldot         = q_bar*BLAC.Configuration.Geometry.S*BLAC.Configuration.Geometry.l_mac^2/...
                (I_yy*VTAS)*Cm_aldot;
M_wdot          = M_aldot/VTAS;

%% Define the states of the linear model from X
IX      = [ 1  ...  %  1 - u_K_b                        [m/s]
            2  ...  %  2 - v_K_b                        [m/s]
            3  ...  %  3 - w_K_b                        [m/s]
            20 ...  % 20 - p_K_b                      [rad/s]
            19 ...  % 19 - q_K_b                      [rad/s]
            21 ...  % 21 - r_K_b                      [rad/s]
            4  ...  %  4 - q_0                            [-]
            5  ...  %  5 - q_1                            [-]
            6  ...  %  6 - q_2                            [-]
            7  ...  %  7 - q_3                            [-]
            46 ...  %  46 - Aileron_Boost
            52 ...  %  52 - Aileron_Boost_dot
            42 ...  %  42 - Rudder_Boost
            68 ...  %  68 - Rudder_Boost_dot
            39 ...  %  39 - Elevator_Boost
            53 ...  %  53 - Elevator_Boost_dot
            35 ...  %  35 - LH_Trottle
            71 ...  %  71 - LH_Trottle_dot
            36 ...  %  36 - RH_Trottle
            72 ...  %  72 - RH_Trottle_dot
            ];

%% Define the outputs of the linear model from Y
IY  = [ 12  ... % 12 - V_IAS      [m/s]
        2   ... %  2 - alpha_A    [rad]
        6   ... %  6 - beta_A     [rad]
        9   ... %  9 - p_K_b      [rad/s]
        3   ... %  3 - q_K_b      [rad/s]
        10  ... % 10 - r_K_b      [rad/s]    
        7   ... %  7 - phi        [rad]
        4   ... %  4 - theta      [rad]    
        8   ... %  8 - psi        [rad]
        ];                                               

%% Define the inputs of the linear model from U

IU  = [ 1   ... %  1  - LH_Throttle                 [-]
        2   ... %  2  - RH_Throttle                 [-]
        3   ... %  3  - Aileron                   [rad]
        4   ... %  4  - Rudder                    [rad]
        5   ... %  5  - Elevator                  [rad]
        ];

%% Extract the state space matrices
Acoup = A(IX,IX);
Bcoup = B(IX,IU); 
Ccoup = C(IY,IX);
Dcoup = D(IY,IU);

X_Name_Reduced = {BLAC_Trim.Names.X_Name{IX}};
Y_Name_Reduced = {BLAC_Trim.Names.Y_Name{IY}};
U_Name_Reduced = {BLAC_Trim.Names.U_Name{IU}};

%% Determine E-Matrix to include M_w_dot effect: E*xdot = A*x+B*u
E = eye(length(IX),length(IX));

% E matrix (q,wdot) entry is corrected wrt the new order of the states
E(5,3) = -M_wdot;
Acoup = inv(E)*Acoup;
Bcoup = inv(E)*Bcoup;

%% State space transformation for the states 
% (u,v,w) -> (V,alpha,beta) and (q0,q1,q2,q3) -> (phi,theta,psi)
% The orthonormal transformation between Euler angles and quaternions [CEq,CqE]

% Calculate the Jacobian for quaternion to Euler transformation
CEq  = Ccoup(7:9,7:10); %([Phi Theta Psi],[q0 q1 q2 q3])

% Calculate the Jacobian for Euler to quaternion transformation
phi   = BLAC_Trim.Init.Y0(7);
theta = BLAC_Trim.Init.Y0(4);
psi   = BLAC_Trim.Init.Y0(8);
CqE   = 0.5*[-cos(theta/2)*sin(phi/2)*cos(psi/2)+cos(phi/2)*sin(theta/2)*sin(psi/2),...
             -cos(phi/2)*sin(theta/2)*cos(psi/2)+sin(phi/2)*sin(psi/2)*cos(theta/2),...
             -sin(psi/2)*cos(phi/2)*cos(theta/2)+sin(phi/2)*sin(theta/2)*cos(psi/2);...
              cos(phi/2)*cos(theta/2)*cos(psi/2)+sin(phi/2)*sin(theta/2)*sin(psi/2),...
             -sin(phi/2)*sin(theta/2)*cos(psi/2)-sin(psi/2)*cos(phi/2)*cos(theta/2),...
             -sin(phi/2)*sin(psi/2)*cos(theta/2)-cos(phi/2)*sin(theta/2)*cos(psi/2);...
             -sin(phi/2)*sin(theta/2)*cos(psi/2)+sin(psi/2)*cos(phi/2)*cos(theta/2),...
              cos(phi/2)*cos(theta/2)*cos(psi/2)-sin(phi/2)*sin(theta/2)*sin(psi/2),...
             -cos(phi/2)*sin(theta/2)*sin(psi/2)+cos(theta/2)*sin(phi/2)*cos(psi/2);...
             -sin(phi/2)*sin(psi/2)*cos(theta/2)-cos(phi/2)*sin(theta/2)*cos(psi/2),...
             -cos(phi/2)*sin(theta/2)*sin(psi/2)-cos(theta/2)*sin(phi/2)*cos(psi/2),...
              cos(phi/2)*cos(theta/2)*cos(psi/2)+sin(phi/2)*sin(theta/2)*sin(psi/2)];

%% The orthonormal transformation between body and wind frame velocity [Cwb,Cbw]
% Calculate the Jacobian for body to wind transformation
Cwb = Ccoup(1:3,1:3);

% Calculate the Jacobian for wind to body transformation
Cbw = inv(Cwb);

%Calculate the orthonormal transformation matrices
dA = length(Acoup(:,1))-10;

Cf  = [    Cwb      ,  0*eye( 3,3)  , 0*eye( 3,4)   , 0*eye( 3,dA)
        0*eye( 3,3) ,    eye( 3,3)  , 0*eye( 3,4)   , 0*eye( 3,dA)
        0*eye( 3,3) ,  0*eye( 3,3)  ,   CEq         , 0*eye( 3,dA)
        0*eye(dA,3) ,  0*eye(dA,3)  , 0*eye(dA,4)   ,   eye(dA,dA)    ];
    
Ci  = [    Cbw      ,  0*eye( 3,3)  , 0*eye( 3,3)   , 0*eye( 3,dA)
        0*eye( 3,3) ,    eye( 3,3)  , 0*eye( 3,3)   , 0*eye( 3,dA)
        0*eye( 4,3) ,  0*eye( 4,3)  ,   CqE         , 0*eye( 4,dA)
        0*eye(dA,3) ,  0*eye(dA,3)  , 0*eye(dA,3)   ,   eye(dA,dA)    ];

%% Calculate the transformed state space matrices
% ydot = Cf*xdot = Cf*A*Ci*y + Cf*B*u
% y    = C*Ci*y + D*u

Acoup = Cf*Acoup*Ci;
Bcoup = Cf*Bcoup;
Ccoup = Ccoup*Ci;

%% Sweep the matrices to enhance the minimum realizations
eps = 1E-9;
[imax,jmax] = size(Acoup); 
for i=1:imax
    for j=1:jmax
        if abs(Acoup(i,j))<=eps
            Acoup(i,j) = 0;
        end
    end
end

[imax,jmax] = size(Bcoup);
for i=1:imax
    for j=1:jmax
        if abs(Bcoup(i,j))<=eps
            Bcoup(i,j) = 0;
        end
    end
end

[imax,jmax] = size(Ccoup);
for i=1:imax
    for j=1:jmax
        if abs(Ccoup(i,j))<=eps
            Ccoup(i,j) = 0;
        end
    end
end

%% Systemize Reduced System

RedSys = ss(Acoup,Bcoup,Ccoup,Dcoup);

set(RedSys, 'inputname',  {BLAC_Trim.Names.U_Name{IU}},...
            'outputname', {BLAC_Trim.Names.Y_Name{IY}},...
            'statename ', {BLAC_Trim.Names.Y_Name{IY},BLAC_Trim.Names.X_Name{IX(11:end)}});

ReducedSys.A   = Acoup;
ReducedSys.B   = Bcoup;
ReducedSys.C   = Ccoup;
ReducedSys.D   = Dcoup;

ReducedSys.X_Name_Old   = X_Name_Reduced;
ReducedSys.X_Name       = {BLAC_Trim.Names.Y_Name{IY},BLAC_Trim.Names.X_Name{IX(11:end)}};
ReducedSys.Y_Name       = Y_Name_Reduced;
ReducedSys.U_Name       = U_Name_Reduced;


ReducedSys.sys      = RedSys;        
        
       
%% Systemize Reduced System without Actuator Dynamics
%Define variables
IX_ACT = [  1   ...     % V_IAS
            2   ...     % alpha
            3   ...     % beta
            4   ...     % p
            5   ...     % q
            6   ...     % r
            7   ...     % Phi
            8   ...     % Theta
            9   ...     % Psi
            ];
        
IY_ACT = [  1   ...     % V_IAS
            2   ...     % alpha
            3   ...     % beta
            4   ...     % p
            5   ...     % q
            6   ...     % r
            7   ...     % Phi
            8   ...     % Theta
            9   ...     % Psi
            ];     
IU_ACT = [  1   ...     % LH_Trottle
            2   ...     % RH_Throttle
            3   ...     % Aileron
            4   ...     % Rudder
            5   ...     % Elevator
            ];

% Extract matrices
AACT= Acoup(IX_ACT,IX_ACT);
BACT= Bcoup(IX_ACT,IU_ACT); 
CACT= Ccoup(IY_ACT,IX_ACT);
DACT= Dcoup(IY_ACT,IU_ACT);

X_Name_Reduced_ACT = {ReducedSys.X_Name{IX_ACT}};
Y_Name_Reduced_ACT = {ReducedSys.Y_Name{IY_ACT}};
U_Name_Reduced_ACT = {ReducedSys.U_Name{IU_ACT}};

% Definition of state-space system
RedSysACT = ss(AACT,BACT,CACT,DACT);

set(RedSysACT,  'inputname'     ,U_Name_Reduced_ACT,...
                'outputname'    ,Y_Name_Reduced_ACT,...
                'statename'     ,X_Name_Reduced_ACT);
       
ReducedSysACT.A   = AACT;
ReducedSysACT.B   = BACT;
ReducedSysACT.C   = CACT;
ReducedSysACT.D   = DACT;

ReducedSysACT.X_Name   = X_Name_Reduced_ACT;
ReducedSysACT.Y_Name   = Y_Name_Reduced_ACT;
ReducedSysACT.U_Name   = U_Name_Reduced_ACT;

ReducedSysACT.sys      = RedSysACT;      
       
%% Systemize Reduced System Longitudinal Motion
%Define longitudinal variables
IX_lon = [  1   ...     % V_IAS
            2   ...     % alpha
            5   ...     % q
            8   ...     % theta
            14  ...     % Act_Elevator
            15  ...     % Act_Elevator_dot
            16  ...     % LH_Engine
            17  ...     % LH_Engine_dot
            18  ...     % RH_Engine
            19  ...     % RH_Engine_dot
            ];
        
IY_lon =  [ 1   ... % V_IAS
            2   ... % alpha
            3   ... % beta
            4   ... % p
            5   ... % q
            6   ... % r
            7   ... % Phi
            8   ... % Theta
            9   ... % Psi
            ];
IU_lon = [  1   ...     % LH_Trottle
            2   ...     % RH_Throttle
            5   ...     % Elevator
            ];

% Extract longitudinal matrices
Alon= Acoup(IX_lon,IX_lon);
Blon= Bcoup(IX_lon,IU_lon); 
Clon= Ccoup(IY_lon,IX_lon);
Dlon= Dcoup(IY_lon,IU_lon);

X_Name_Reduced_Lon = {ReducedSys.X_Name{IX_lon}};
Y_Name_Reduced_Lon = {ReducedSys.Y_Name{IY_lon}};
U_Name_Reduced_Lon = {ReducedSys.U_Name{IU_lon}};

% Definition of longitudinal state-space system
RedSysLon = ss(Alon,Blon,Clon,Dlon);

set(RedSysLon,  'inputname'     ,U_Name_Reduced_Lon,...
                'outputname'    ,Y_Name_Reduced_Lon,...
                'statename'     ,X_Name_Reduced_Lon);
       
ReducedSysLon.A   = Alon;
ReducedSysLon.B   = Blon;
ReducedSysLon.C   = Clon;
ReducedSysLon.D   = Dlon;

ReducedSysLon.X_Name   = X_Name_Reduced_Lon;
ReducedSysLon.Y_Name   = Y_Name_Reduced_Lon;
ReducedSysLon.U_Name   = U_Name_Reduced_Lon;

ReducedSysLon.sys      = RedSysLon;      
       
%% Systemize Reduced System Longitudinal Motion without Actuators
%Define longitudinal variables
IX_lonA = [ 1   ...     % V_IAS
            2   ...     % alpha
            5   ...     % q
            8   ...     % theta
            ];
        
IY_lonA = [ 1   ... % V_IAS
            2   ... % alpha
            3   ... % beta
            4   ... % p
            5   ... % q
            6   ... % r
            7   ... % Phi
            8   ... % Theta
            9   ... % Psi
            ];
IU_lonA = [ 1   ...     % LH_Trottle
            2   ...     % RH_Throttle
            5   ...     % Elevator
            ];

% Extract longitudinal matrices
AlonA = Acoup(IX_lonA,IX_lonA);
BlonA = Bcoup(IX_lonA,IU_lonA); 
ClonA = Ccoup(IY_lonA,IX_lonA);
DlonA = Dcoup(IY_lonA,IU_lonA);

X_Name_Reduced_LonA = {ReducedSys.X_Name{IX_lonA}};
Y_Name_Reduced_LonA = {ReducedSys.Y_Name{IY_lonA}};
U_Name_Reduced_LonA = {ReducedSys.U_Name{IU_lonA}};

% Definition of longitudinal state-space system
RedSysLonA = ss(AlonA,BlonA,ClonA,DlonA);

set(RedSysLonA,  'inputname'     ,U_Name_Reduced_LonA,...
                'outputname'    ,Y_Name_Reduced_LonA,...
                'statename'     ,X_Name_Reduced_LonA);
       
ReducedSysLonA.A   = AlonA;
ReducedSysLonA.B   = BlonA;
ReducedSysLonA.C   = ClonA;
ReducedSysLonA.D   = DlonA;

ReducedSysLonA.X_Name   = X_Name_Reduced_LonA;
ReducedSysLonA.Y_Name   = Y_Name_Reduced_LonA;
ReducedSysLonA.U_Name   = U_Name_Reduced_LonA;

ReducedSysLonA.sys      = RedSysLonA;      
       
%% Systemize Reduced System Lateral Motion     

%Define lateral variables
IX_lat = [  2   ... % alpha
            3   ... % beta
            4   ... % p
            5   ... % q
            6   ... % r 
            7   ... % Phi
            8   ... % Theta
            9   ... % Psi
            10  ... % Aileron
            11  ... % Aileron_dot 
            12  ... % Rudder
            13  ... % Rudder_dot
            16  ... % LH_Engine
            17  ... % LH_Engine_dot
            18  ... % RH_Engine
            19  ... % RH_Engine_dot
            ]; 
IY_lat =  [ 1   ... % V_IAS
            2   ... % alpha
            3   ... % beta
            4   ... % p
            5   ... % q
            6   ... % r
            7   ... % Phi
            8   ... % Theta
            9   ... % Psi
            ];     
IU_lat = [  1   ... % LH_Engine
            2   ... % RH_Engine
            3   ... % Aileron
            4   ... % Rudder
            5   ... % Elevator
            ];

% Extract longitudinal matrices
Alat= Acoup(IX_lat,IX_lat);
Blat= Bcoup(IX_lat,IU_lat); 
Clat= Ccoup(IY_lat,IX_lat);
Dlat= Dcoup(IY_lat,IU_lat);

X_Name_Reduced_Lat = {ReducedSys.X_Name{IX_lat}};
Y_Name_Reduced_Lat = {ReducedSys.Y_Name{IY_lat}};
U_Name_Reduced_Lat = {ReducedSys.U_Name{IU_lat}};

% Definition of longitudinal state-space system
RedSysLat = ss(Alat,Blat,Clat,Dlat);

set(RedSysLat,  'inputname'     ,U_Name_Reduced_Lat,...
                'outputname'    ,Y_Name_Reduced_Lat,...
                'statename'     ,X_Name_Reduced_Lat);
       
ReducedSysLat.A   = Alat;
ReducedSysLat.B   = Blat;
ReducedSysLat.C   = Clat;
ReducedSysLat.D   = Dlat;

ReducedSysLat.X_Name   = X_Name_Reduced_Lat;
ReducedSysLat.Y_Name   = Y_Name_Reduced_Lat;
ReducedSysLat.U_Name   = U_Name_Reduced_Lat;

ReducedSysLat.sys      = RedSysLat;    
        
%% Systemize Reduced System Lateral Motion without Actuator
%Define lateral variables
IX_latA = [ 3   ... % beta
            4   ... % p
            6   ... % r 
            7   ... % Phi
            ]; 
IY_latA = [ 1   ... % V_IAS
            2   ... % alpha
            3   ... % beta
            4   ... % p
            5   ... % q
            6   ... % r
            7   ... % Phi
            8   ... % Theta
            9   ... % Psi
            ];     
IU_latA = [ 1   ... % LH_Engine
            2   ... % RH_Engine
            3   ... % Aileron
            4   ... % Rudder
            5   ... % Elevator
            ];

% Extract longitudinal matrices
AlatA = Acoup(IX_latA,IX_latA);
BlatA = Bcoup(IX_latA,IU_latA); 
ClatA = Ccoup(IY_latA,IX_latA);
DlatA = Dcoup(IY_latA,IU_latA);

X_Name_Reduced_LatA = {ReducedSys.X_Name{IX_latA}};
Y_Name_Reduced_LatA = {ReducedSys.Y_Name{IY_latA}};
U_Name_Reduced_LatA = {ReducedSys.U_Name{IU_latA}};

% Definition of longitudinal state-space system
RedSysLatA = ss(AlatA,BlatA,ClatA,DlatA);

set(RedSysLatA,  'inputname'    ,U_Name_Reduced_LatA,...
                'outputname'    ,Y_Name_Reduced_LatA,...
                'statename'     ,X_Name_Reduced_LatA);
       
ReducedSysLatA.A   = AlatA;
ReducedSysLatA.B   = BlatA;
ReducedSysLatA.C   = ClatA;
ReducedSysLatA.D   = DlatA;

ReducedSysLatA.X_Name   = X_Name_Reduced_LatA;
ReducedSysLatA.Y_Name   = Y_Name_Reduced_LatA;
ReducedSysLatA.U_Name   = U_Name_Reduced_LatA;

ReducedSysLatA.sys      = RedSysLatA;    
        
% Linear and nonlinear system comparison
if Checkplot
    close all

    dt = 0.01; tf = 1.5; t_final = 2*tf;
    t1 = [0:dt:tf]'; t2 = [tf+dt:dt:t_final]'; t = [t1;t2];

    % Arrange inputs to the nonlinear plant
    u_trim = repmat(BLAC_Trim.Init.U0',length(t),1);
    u_nlin = zeros(length(t),15);
    
    % Elevator input deflection
    de = 0.5 / 180*pi; 
    u_nlin(:,5) = -[de*ones(length(t1),1);-de*ones(length(t2),1)];
    
    % Aileron input deflection
    da = 0.5 / 180*pi; 
    u_nlin(:,3) = +[da*ones(length(t1),1);-da*ones(length(t2),1)];
    u_nlin = u_nlin + u_trim;

    % Nonlinear simulation
    % Set the trim flag to zero
    evalin('base','BLAC.Sim.Trim_Flag = 0;');
    
    % Reassign the actuator parameters
    evalin('base','BLAC.Actuator = BLAC_Actuator_Init;');
    opt = simset('Solver','ode4','FixedStep',dt);
    [t,x,y] = sim('BLAC_Trim_Quat.mdl',t_final,opt,[t,u_nlin]);

    % Linear simulation
    % Arrange inputs to the linear plant
    u_lin = zeros(length(t),5);
    
    % Linear model aileron input
    u_lin(:,3) = +[da*ones(length(t1),1);-da*ones(length(t2),1)];
   
    % Linear model elevator input
    u_lin(:,5) =  -[de*ones(length(t1),1);-de*ones(length(t2),1)];               
    y_lin  = lsim(RedSys,u_lin,t);
    y_trim = repmat(BLAC_Trim.Init.Y0(IY)',length(t),1);    
    y_lin  = y_lin + y_trim;

    % Plot linear and nonlinear simulation results
    scrsz = get(0,'ScreenSize');
    set(0,'DefaultLineLineWidth',2);
    figure('Name','Longitudinal Plane of Motion','NumberTitle','on',...
           'Position',[scrsz(3)/2 scrsz(4)/10 scrsz(3)/2.5 scrsz(4)/1.25]);
    subplot(511), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itIAS} [kts]');
    subplot(512), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\alpha} [�]');
    subplot(513), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itq} [�/s]');
    subplot(514), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\theta} [�]');
    subplot(515), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\delta}_{\ite} [�]');

    subplot(511); plot(t,y(:,12)/0.5144,t,y_lin(:,1)/0.5144); 
    subplot(512); plot(t,y(:,2)*180/pi,t,y_lin(:,2)*180/pi);
    subplot(513); plot(t,y(:,3)*180/pi,t,y_lin(:,5)*180/pi);
    subplot(514); plot(t,y(:,4)*180/pi,t,y_lin(:,8)*180/pi);
    subplot(515); plot(t,u_nlin(:,5)*180/pi);

    figure('Name','Lateral Plane of Motion','NumberTitle','on',...
           'Position',[scrsz(3)/11.00 scrsz(4)/10 scrsz(3)/2.5 scrsz(4)/1.25]);
    subplot(511), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\beta} [�]');
    subplot(512), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itp} [�/s]');
    subplot(513), hold; grid; xlabel('{\itt} [s]'); ylabel('{\itr} [�/s]');
    subplot(514), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\phi} [�]');
    subplot(515), hold; grid; xlabel('{\itt} [s]'); ylabel('{\it\delta}_{\ita} [�]');

    subplot(511); plot(t,y(:,6)*180/pi,t,y_lin(:,3)*180/pi); 
    subplot(512); plot(t,y(:,9)*180/pi,t,y_lin(:,4)*180/pi);
    subplot(513); plot(t,y(:,10)*180/pi,t,y_lin(:,6)*180/pi);
    subplot(514); plot(t,y(:,7)*180/pi,t,y_lin(:,7)*180/pi);
        legend('NLin','Lin','Location','Best')
    subplot(515); plot(t,(u_nlin(:,3))*180/pi);
end
if PlotsOn
    RedZeros = zero(RedSysACT);
    RedPoles = pole(RedSysACT);
    RedLonZeros = zero(RedSysLonA);
    RedLonPoles = pole(RedSysLonA);
    RedLatZeros = zero(RedSysLatA);
    RedLatPoles = pole(RedSysLatA);

    [eigvec, eigval] = eig(RedSysLatA.a);

    figure(543156),clf
    set( gcf ,  'Color','w', ...
                'DefaultLineLineWidth',1.5 ,...
                'DefaultLineMarkerSize',8,...
                'DefaultAxesFontName','times',...
                'DefaultAxesFontSize',12)
    Ax = subplot(1,1,1);
    box off
    grid on
    hold on
    plot(real(RedZeros),imag(RedZeros),'ro'), hold on, grid on, box off
    plot(real(RedPoles),imag(RedPoles),'bx')
    plot(real(RedLonZeros),imag(RedLonZeros),'mo')
    plot(real(RedLonPoles),imag(RedLonPoles),'cx')
    plot(real(RedLatZeros),imag(RedLatZeros),'mo')
    plot(real(RedLatPoles),imag(RedLatPoles),'gx')
    % xlim([-4,3]);
    axis square;
    xlabel('$$ \sigma $$','Interpreter','latex')
    ylabel('$$ i \omega $$','Interpreter','latex')
    set(Ax,'units','normalized','Position',[.22 .2 .75 .75]);
    Axpar1 = get(Ax,{'xlim','ylim'});
    L22 = line(   Axpar1{1,1}, [0 0], 'Color', 'k','LineWidth',0.5*1.5);                
    Axpar1 = get(Ax,{'xlim','ylim'});
    L21 = line(   [0 0], Axpar1{1,2}, 'Color', 'k','LineWidth',0.5*1.5);                
    uistack(L21, 'bottom')
    uistack(L22, 'bottom')
end
end
