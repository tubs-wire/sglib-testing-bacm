function [TrimSettings] = SwitchTrimpoints_pr(trimpoint_number)

switch trimpoint_number
  case 1
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V40_FPA1_Bank0_HF_13122016113827.mat'; % Link Trimfiles here
  case 2
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V40_FPA-3_Bank0_HF_13122016113913.mat'; % Link Trimfiles here
  case 3
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V55_FPA-3_Bank0_HF_13122016115543.mat'; % Link Trimfiles here
  case 4
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V50_FPA-6_Bank0_HF_13122016114231.mat'; % Link Trimfiles here
  case 5
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V55_FPA0_Bank0_HF_13122016114918.mat'; % Link Trimfiles here
  case 6
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V50_FPA0_Bank0_HF_13122016114747.mat'; % Link Trimfiles here
  case 7
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V40_FPA0_Bank0_HF_13122016114448.mat'; % Link Trimfiles here
  case 8
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V45_FPA1_Bank0_HF_13122016113959.mat'; % Link Trimfiles here
  case 9
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V55_FPA-6_Bank0_HF_13122016114403.mat'; % Link Trimfiles here
  case 10
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V40_FPA-3_Bank0_HF_13122016115113.mat'; % Link Trimfiles here
  case 11
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V50_FPA-6_Bank0_HF_13122016114830.mat'; % Link Trimfiles here
  case 12
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V50_FPA-3_Bank0_HF_13122016114808.mat'; % Link Trimfiles here
  case 13
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V45_FPA0_Bank0_HF_13122016114021.mat'; % Link Trimfiles here
  case 14
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V55_FPA1_Bank0_HF_13122016115457.mat'; % Link Trimfiles here
  case 15
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V50_FPA1_Bank0_HF_13122016114127.mat'; % Link Trimfiles here
  case 16
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V40_FPA1_Bank0_HF_13122016114425.mat'; % Link Trimfiles here
  case 17
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V50_FPA-6_Bank0_HF_13122016115433.mat'; % Link Trimfiles here
  case 18
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V55_FPA-3_Bank0_HF_13122016114340.mat'; % Link Trimfiles here
  case 19
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V45_FPA-3_Bank0_HF_13122016115246.mat'; % Link Trimfiles here
  case 20
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V50_FPA-3_Bank0_HF_13122016115411.mat'; % Link Trimfiles here
  case 21
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V50_FPA1_Bank0_HF_13122016114725.mat'; % Link Trimfiles here
  case 22
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V40_FPA-6_Bank0_HF_13122016115139.mat'; % Link Trimfiles here
  case 23
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V55_FPA0_Bank0_HF_13122016114318.mat'; % Link Trimfiles here
  case 24
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V45_FPA-6_Bank0_HF_13122016114105.mat'; % Link Trimfiles here
  case 25
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V45_FPA-6_Bank0_HF_13122016114704.mat'; % Link Trimfiles here
  case 26
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V50_FPA-3_Bank0_HF_13122016114210.mat'; % Link Trimfiles here
  case 27
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V45_FPA-3_Bank0_HF_13122016114043.mat'; % Link Trimfiles here
  case 28
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V40_FPA-6_Bank0_HF_13122016113937.mat'; % Link Trimfiles here
  case 29
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V45_FPA1_Bank0_HF_13122016115202.mat'; % Link Trimfiles here
  case 30
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V40_FPA-6_Bank0_HF_13122016114535.mat'; % Link Trimfiles here
  case 31
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V40_FPA0_Bank0_HF_13122016113850.mat'; % Link Trimfiles here
  case 32
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V45_FPA0_Bank0_HF_13122016115224.mat'; % Link Trimfiles here
  case 33
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V40_FPA-3_Bank0_HF_13122016114511.mat'; % Link Trimfiles here
  case 34
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V50_FPA0_Bank0_HF_13122016115350.mat'; % Link Trimfiles here
  case 35
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V55_FPA1_Bank0_HF_13122016114255.mat'; % Link Trimfiles here
  case 36
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V55_FPA-6_Bank0_HF_13122016115004.mat'; % Link Trimfiles here
  case 37
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V45_FPA1_Bank0_HF_13122016114557.mat'; % Link Trimfiles here
  case 38
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V45_FPA-3_Bank0_HF_13122016114641.mat'; % Link Trimfiles here
  case 39
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V50_FPA1_Bank0_HF_13122016115329.mat'; % Link Trimfiles here
  case 40
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V55_FPA-3_Bank0_HF_13122016114941.mat'; % Link Trimfiles here
  case 41
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V55_FPA1_Bank0_HF_13122016114854.mat'; % Link Trimfiles here
  case 42
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V55_FPA-6_Bank0_HF_13122016115605.mat'; % Link Trimfiles here
  case 43
    TrimSettings.strTrimfile = 'Trimpoint/Alt0_V50_FPA0_Bank0_HF_13122016114149.mat'; % Link Trimfiles here
  case 44
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V40_FPA0_Bank0_HF_13122016115049.mat'; % Link Trimfiles here
  case 45
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V40_FPA1_Bank0_HF_13122016115027.mat'; % Link Trimfiles here
  case 46
    TrimSettings.strTrimfile = 'Trimpoint/Alt305_V45_FPA0_Bank0_HF_13122016114619.mat'; % Link Trimfiles here
  case 47
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V55_FPA0_Bank0_HF_13122016115520.mat'; % Link Trimfiles here
  case 48
    TrimSettings.strTrimfile = 'Trimpoint/Alt914_V45_FPA-6_Bank0_HF_13122016115308.mat'; % Link Trimfiles here
  otherwise
    fprintf('wrong NrTrimpoint %d!!!\n', NrTrimpoint)
    return
end


end

