%% Uncertainty Matrix Lift
BLAC.Aerodynamics.Lift.Uncert_b05T0    = 1 + uq_vector(1); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Lift.Uncert_b05T1    = 1 + uq_vector(2); % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Lift.Uncert_b05T2    = 1 + uq_vector(3); % abs(Beta)= 5  deg, T2 = 0 N: +- 20% 
BLAC.Aerodynamics.Lift.Uncert_b10T0    = 1 + uq_vector(4); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Lift.Uncert_b10T1    = 1 + uq_vector(5); % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Lift.Uncert_b10T2    = 1 + uq_vector(6); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Lift.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Lift.Uncert_b05T0     BLAC.Aerodynamics.Lift.Uncert_b05T1     BLAC.Aerodynamics.Lift.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Lift.Uncert_b10T0     BLAC.Aerodynamics.Lift.Uncert_b10T1     BLAC.Aerodynamics.Lift.Uncert_b10T2 ];

%% Uncertainty Matrix Drag
BLAC.Aerodynamics.Drag.Uncert_b05T0    = 1 + uq_vector(7); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Drag.Uncert_b05T1    = 1 + uq_vector(8); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Drag.Uncert_b05T2    = 1 + uq_vector(9); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Drag.Uncert_b10T0    = 1 + uq_vector(10); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Drag.Uncert_b10T1    = 1 + uq_vector(11); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Drag.Uncert_b10T2    = 1 + uq_vector(12); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Drag.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Drag.Uncert_b05T0     BLAC.Aerodynamics.Drag.Uncert_b05T1     BLAC.Aerodynamics.Drag.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Drag.Uncert_b10T0     BLAC.Aerodynamics.Drag.Uncert_b10T1     BLAC.Aerodynamics.Drag.Uncert_b10T2 ];
                                            
%% Uncertainty Matrix Pitch
BLAC.Aerodynamics.Pitch.Uncert_b05T0    = 1 + uq_vector(13); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Pitch.Uncert_b05T1    = 1 + uq_vector(14); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Pitch.Uncert_b05T2    = 1 + uq_vector(15); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Pitch.Uncert_b10T0    = 1 + uq_vector(16); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Pitch.Uncert_b10T1    = 1 + uq_vector(17); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Pitch.Uncert_b10T2    = 1 + uq_vector(18); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Pitch.Uncertainty_LT  = [ 1                                       1                                       1                                       ;...
                                            BLAC.Aerodynamics.Pitch.Uncert_b05T0    BLAC.Aerodynamics.Pitch.Uncert_b05T1    BLAC.Aerodynamics.Pitch.Uncert_b05T2    ;...
                                            BLAC.Aerodynamics.Pitch.Uncert_b10T0    BLAC.Aerodynamics.Pitch.Uncert_b10T1    BLAC.Aerodynamics.Pitch.Uncert_b10T2   ];
                                            
%% Uncertainty Matrix BLAC.Aerodynamics.Roll
BLAC.Aerodynamics.Roll.Uncert_b05T0    = 1 + uq_vector(19); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Roll.Uncert_b05T1    = 1 + uq_vector(20); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b05T2    = 1 + uq_vector(21); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b10T0    = 1 + uq_vector(22); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Roll.Uncert_b10T1    = 1 + uq_vector(23); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b10T2    = 1 + uq_vector(24); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncertainty_LT  = [  1                                       1                                       1                                    ;...
                                            BLAC.Aerodynamics.Roll.Uncert_b05T0     BLAC.Aerodynamics.Roll.Uncert_b05T1     BLAC.Aerodynamics.Roll.Uncert_b05T2  ;...
                                            BLAC.Aerodynamics.Roll.Uncert_b10T0     BLAC.Aerodynamics.Roll.Uncert_b10T1     BLAC.Aerodynamics.Roll.Uncert_b10T2 ];
                                            
BLAC.Aerodynamics.Roll.Uncert_Clp      = 1 + uq_vector(25); % Clp:    +- 5%
BLAC.Aerodynamics.Roll.Uncert_Clr      = 1 + uq_vector(26); % Clr:    +- 50%
BLAC.Aerodynamics.Roll.Uncert_Clzeta   = 1 + uq_vector(27); % Clzeta: +- 10%
BLAC.Aerodynamics.Roll.Uncert_Clxi     = 1 + uq_vector(28); % Clxi:   +- 10%



%% Uncertainty Matrix Yaw
BLAC.Aerodynamics.Yaw.Uncert_b05T0    = 1 + uq_vector(29); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Yaw.Uncert_b05T1    = 1 + uq_vector(30); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b05T2    = 1 + uq_vector(31); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b10T0    = 1 + uq_vector(32); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Yaw.Uncert_b10T1    = 1 + uq_vector(33); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b10T2    = 1 + uq_vector(34); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncertainty_LT  = [   1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Yaw.Uncert_b05T0      BLAC.Aerodynamics.Yaw.Uncert_b05T1      BLAC.Aerodynamics.Yaw.Uncert_b05T2  ;...
                                            BLAC.Aerodynamics.Yaw.Uncert_b10T0      BLAC.Aerodynamics.Yaw.Uncert_b10T1      BLAC.Aerodynamics.Yaw.Uncert_b10T2  ];
                                            
BLAC.Aerodynamics.Yaw.Uncert_Cnp      = 1 + uq_vector(35); % Cnp:    +- 15%
BLAC.Aerodynamics.Yaw.Uncert_Cnr      = 1 + uq_vector(36); % Cnr:    +10 -50%
BLAC.Aerodynamics.Yaw.Uncert_Cnzeta   = 1 + uq_vector(37); % Cnzeta: +- 10%
BLAC.Aerodynamics.Yaw.Uncert_Cnxi     = 1 + uq_vector(38); % Cnxi:   +- 10%


%% Uncertainty Matrix Sideforce
BLAC.Aerodynamics.Side.Uncert_b05T0    = 1 + uq_vector(39); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b05T1    = 1 + uq_vector(40); % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Side.Uncert_b05T2    = 1 + uq_vector(41); % abs(Beta)= 5  deg, T2 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b10T0    = 1 + uq_vector(42); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b10T1    = 1 + uq_vector(43); % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Side.Uncert_b10T2    = 1 + uq_vector(44); % abs(Beta)= 10 deg, T1 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Side.Uncert_b05T0     BLAC.Aerodynamics.Side.Uncert_b05T1     BLAC.Aerodynamics.Side.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Side.Uncert_b10T0     BLAC.Aerodynamics.Side.Uncert_b10T1     BLAC.Aerodynamics.Side.Uncert_b10T2 ];

BLAC.Aerodynamics.Side.Uncert_CYp      = 1; % not relevant
BLAC.Aerodynamics.Side.Uncert_CYr      = 1; % not relevnat
BLAC.Aerodynamics.Side.Uncert_CYzeta   = 1 + uq_vector(45); % CYzeta: +- 10%      Similar to Cnzeta
BLAC.Aerodynamics.Side.Uncert_CYxi     = 1; % not relevant

