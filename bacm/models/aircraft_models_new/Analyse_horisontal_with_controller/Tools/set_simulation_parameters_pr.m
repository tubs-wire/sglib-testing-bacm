function [BLAC, Test_Parameters] = set_simulation_parameters_pr(run, BLAC, TrimPointInfo)

BLAC.Sim.StartTime  =  0.0;                            % Simulation Start Time                       [s]

BLAC.Sim.EndTime    =  30; 

% if run == 1
%     BLAC.Sim.EndTime    =  30;                         % Simulation End Time (330 at 780m)          [s]
% else
%     BLAC.Sim.EndTime    = 30;
%     % BLAC.Sim.EndTime    =  Result.(Case_Name){9}.Value(end); % Cut Simulation Time, if first run did not reach the default simulation end time
% end

BLAC.Sim.SampleTime =  0.01;                           % Simulation Sample Time                      [s]


% Set Test Parameters
Test_Parameters=struct();
Test_Parameters.Max_Data    = 100000;                  % Maximum of recorded data points             [-]

Test_Parameters.Input_Type  = 0;                       % No input in the follwing time slices        [-]
%*                                                     % Control Input Signal Types:
%*                                                     % [0] - No Signal
%*                                                     % [1] - Step
%*                                                     % [2] - Impulse
%*                                                     % [3] - Doublet
%*                                                     % [4] - Sinus-Wave

%* Step Signal Parameters for Elevator
Test_Parameters.Step_Start          = 5;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial        = TrimPointInfo.Throttle/(100*2);   % Start Time of Step Input                    [s]
Test_Parameters.Step_Size           = .10;                              % Step Size                                 [deg]

%* Step Signal Parameters for RH OEI
Test_Parameters.Step_Start_OEI_RH   = 7;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial_OEI_RH = 1;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Size_OEI_RH    = 1;                                % Step Size                                 [deg]

%* Step Signal Parameters for LH OEI
Test_Parameters.Step_Start_OEI_LH   = 5;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial_OEI_LH = 1;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Size_OEI_LH    = 1;                                % Step Size                                 [deg]

%* Impulse Signal Paramters
Test_Parameters.Impulse_Start   = 1;                                    % Start Time of Impulse Input                 [s]
Test_Parameters.Impulse_End     = 2;                                    % Stop Time of Impulse Input                  [s]
Test_Parameters.Impulse_Size    = 3;                                    % Impulse Size                              [deg]
% 
%* Doublet Signal Parameters
Test_Parameters.Doublet_Start   = 1;                                    % Start Time of Doublet Input                 [s]
Test_Parameters.Doublet_End     = 3;                                    % Stop Time of Doublet Input                  [s]
Test_Parameters.Doublet_Size    = 20;                                   % Doublet Size                              [deg]
% 
%* Sinus Wave Parameters
Test_Parameters.Sinus_Freqency  = 1;                                    % Frequency of Sinus Wave Input              [Hz]
Test_Parameters.Sinus_Start     = 1;                                    % Start Time of Sinus Wave Input              [s]
%

%* Selection of Control Surface
Test_Parameters.Aileron_Gain    = 0;                                    % Select [1] Aileron as Test Surface.         [-]
Test_Parameters.Elevator_Gain   = 0;                                    % Select [1] Elevator as Test Surface.        [-]
Test_Parameters.Rudder_Gain     = 0;                                    % Select [1] Rudder as Test Surface.          [-]
Test_Parameters.Stabilizer_Gain = 0;                                    % Select [1] Stabilizer as Test Surface.      [-]
Test_Parameters.Throttle_Gain   = 0;                                    % Select [1] Throttle as Test Input Signal.   [-]
Test_Parameters.Compressor_Gain = 0;

% One Engine Inoperative
Test_Parameters.OneEngineInoperative        = BLAC.Propulsion.OEI;      % One Engine Inoperative [0 No / 1 Yes]
Test_Parameters.BlowingOperative            = BLAC.Propulsion.BLCOP;  

end

