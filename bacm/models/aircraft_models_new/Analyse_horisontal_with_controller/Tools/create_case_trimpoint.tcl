#!/usr/bin/tclsh -f

set files [glob Trimpoint/*13*];

set data [open "SwitchTrimpoints.m" "w"]
puts $data "switch NrTrimpoint"

set index 1
foreach file $files {
  puts $data "  case $index"
  puts $data "    TrimSettings.strTrimfile = '$file'; % Link Trimfiles here"
  incr index
}
puts $data "  otherwise"
puts $data "    fprintf('wrong NrTrimpoint %d!!!\\n', NrTrimpoint)";
puts $data "    return"
puts $data "end"

close $data
