%% Uncertainty Matrix Lift
BLAC.Aerodynamics.Lift.Uncert_b05T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Lift.Uncert_b05T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Lift.Uncert_b05T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 20% 
BLAC.Aerodynamics.Lift.Uncert_b10T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Lift.Uncert_b10T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Lift.Uncert_b10T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Lift.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Lift.Uncert_b05T0     BLAC.Aerodynamics.Lift.Uncert_b05T1     BLAC.Aerodynamics.Lift.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Lift.Uncert_b10T0     BLAC.Aerodynamics.Lift.Uncert_b10T1     BLAC.Aerodynamics.Lift.Uncert_b10T2 ];

%% Uncertainty Matrix Drag
BLAC.Aerodynamics.Drag.Uncert_b05T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Drag.Uncert_b05T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Drag.Uncert_b05T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Drag.Uncert_b10T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Drag.Uncert_b10T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Drag.Uncert_b10T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Drag.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Drag.Uncert_b05T0     BLAC.Aerodynamics.Drag.Uncert_b05T1     BLAC.Aerodynamics.Drag.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Drag.Uncert_b10T0     BLAC.Aerodynamics.Drag.Uncert_b10T1     BLAC.Aerodynamics.Drag.Uncert_b10T2 ];

%% Uncertainty Matrix Pitch
BLAC.Aerodynamics.Pitch.Uncert_b05T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Pitch.Uncert_b05T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Pitch.Uncert_b05T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Pitch.Uncert_b10T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Pitch.Uncert_b10T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Pitch.Uncert_b10T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
BLAC.Aerodynamics.Pitch.Uncertainty_LT  = [ 1                                       1                                       1                                       ;...
                                            BLAC.Aerodynamics.Pitch.Uncert_b05T0    BLAC.Aerodynamics.Pitch.Uncert_b05T1    BLAC.Aerodynamics.Pitch.Uncert_b05T2    ;...
                                            BLAC.Aerodynamics.Pitch.Uncert_b10T0    BLAC.Aerodynamics.Pitch.Uncert_b10T1    BLAC.Aerodynamics.Pitch.Uncert_b10T2   ];

%% Uncertainty Matrix BLAC.Aerodynamics.Roll
BLAC.Aerodynamics.Roll.Uncert_b05T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Roll.Uncert_b05T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b05T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b10T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
BLAC.Aerodynamics.Roll.Uncert_b10T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncert_b10T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Roll.Uncertainty_LT  = [  1                                       1                                       1                                    ;...
                                            BLAC.Aerodynamics.Roll.Uncert_b05T0     BLAC.Aerodynamics.Roll.Uncert_b05T1     BLAC.Aerodynamics.Roll.Uncert_b05T2  ;...
                                            BLAC.Aerodynamics.Roll.Uncert_b10T0     BLAC.Aerodynamics.Roll.Uncert_b10T1     BLAC.Aerodynamics.Roll.Uncert_b10T2 ];

BLAC.Aerodynamics.Roll.Uncert_Clp      = 1 + (0.0  * Uncertainty_Killswitch(run)); % Clp:    +- 5%
BLAC.Aerodynamics.Roll.Uncert_Clr      = 1 + (0.0  * Uncertainty_Killswitch(run)); % Clr:    +- 50%
BLAC.Aerodynamics.Roll.Uncert_Clzeta   = 1 + (0.0  * Uncertainty_Killswitch(run)); % Clzeta: +- 10%
BLAC.Aerodynamics.Roll.Uncert_Clxi     = 1 + (0.0  * Uncertainty_Killswitch(run)); % Clxi:   +- 10%

%% Uncertainty Matrix Yaw
BLAC.Aerodynamics.Yaw.Uncert_b05T0    = 1 + (+0.1  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Yaw.Uncert_b05T1    = 1 + (+0.25 * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b05T2    = 1 + (+0.25 * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b10T0    = 1 + (-0.1  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Yaw.Uncert_b10T1    = 1 + (-0.25 * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncert_b10T2    = 1 + (-0.25 * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
BLAC.Aerodynamics.Yaw.Uncertainty_LT  = [   1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Yaw.Uncert_b05T0      BLAC.Aerodynamics.Yaw.Uncert_b05T1      BLAC.Aerodynamics.Yaw.Uncert_b05T2  ;...
                                            BLAC.Aerodynamics.Yaw.Uncert_b10T0      BLAC.Aerodynamics.Yaw.Uncert_b10T1      BLAC.Aerodynamics.Yaw.Uncert_b10T2  ];

BLAC.Aerodynamics.Yaw.Uncert_Cnp      = 1 + (0.15 * Uncertainty_Killswitch(run)); % Cnp:    +- 15%
BLAC.Aerodynamics.Yaw.Uncert_Cnr      = 1 + (-0.50 * Uncertainty_Killswitch(run)); % Cnr:    +10 -50%
BLAC.Aerodynamics.Yaw.Uncert_Cnzeta   = 1 + (0.1  * Uncertainty_Killswitch(run)); % Cnzeta: +- 10%
BLAC.Aerodynamics.Yaw.Uncert_Cnxi     = 1 + (0.0  * Uncertainty_Killswitch(run)); % Cnxi:   +- 10%

%% Uncertainty Matrix Sideforce
BLAC.Aerodynamics.Side.Uncert_b05T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b05T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Side.Uncert_b05T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 5  deg, T2 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b10T0    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncert_b10T1    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
BLAC.Aerodynamics.Side.Uncert_b10T2    = 1 + (0.0  * Uncertainty_Killswitch(run)); % abs(Beta)= 10 deg, T1 = 0 N: +- 10% 
BLAC.Aerodynamics.Side.Uncertainty_LT  = [  1                                       1                                       1                                   ;...
                                            BLAC.Aerodynamics.Side.Uncert_b05T0     BLAC.Aerodynamics.Side.Uncert_b05T1     BLAC.Aerodynamics.Side.Uncert_b05T2 ;...
                                            BLAC.Aerodynamics.Side.Uncert_b10T0     BLAC.Aerodynamics.Side.Uncert_b10T1     BLAC.Aerodynamics.Side.Uncert_b10T2 ];

BLAC.Aerodynamics.Side.Uncert_CYp      = 1 + (0.0  * Uncertainty_Killswitch(run)); % not relevant
BLAC.Aerodynamics.Side.Uncert_CYr      = 1 + (0.0  * Uncertainty_Killswitch(run)); % not relevnat
BLAC.Aerodynamics.Side.Uncert_CYzeta   = 1 + (0.1  * Uncertainty_Killswitch(run)); % CYzeta: +- 10%      Similar to Cnzeta
BLAC.Aerodynamics.Side.Uncert_CYxi     = 1 + (0.0  * Uncertainty_Killswitch(run)); % not relevant

