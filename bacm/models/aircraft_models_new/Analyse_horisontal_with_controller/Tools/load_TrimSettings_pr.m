function [BLAC_Trim_pr,X0_pr,U0_pr,tY_pr,TrimPointInfo_pr] = load_TrimSettings_pr( TrimSettings_strTrimfile)

load(TrimSettings_strTrimfile, '-mat');

BLAC_Trim_pr=BLAC_Trim;
X0_pr=X0;
U0_pr=U0;
tY_pr=tY;
TrimPointInfo_pr=TrimPointInfo;

end

