function[]= run_simulation_pr(uq_vector, NrTrimpoint, run, Controller_active_run, FilePath, resultpath, ind, SeeAllInfluences)

TrimSettings=SwitchTrimpoints_pr(NrTrimpoint);

TrimSettings.SimMode = 1;  % Sim Mode = 1 --> Dynamic Simulation Mode, Sim Mode = 2 --> Trim Mode 

[BLAC_Trim, X0, U0, tY, TrimPointInfo, TrimSettings, BLAC]=BLAC_Simulation_Init_pr(TrimSettings);

[BLAC, Test_Parameters]=set_simulation_parameters_pr(run,BLAC, TrimPointInfo);

[BLAC]=set_myUncertainty_pr(uq_vector,BLAC);

[BLAC]=set_config_params_model_funcs_pr(TrimPointInfo, BLAC, uq_vector, BLAC_Trim, Controller_active_run, run);



try
    simout=sim('BLAC_Test_Quat','SaveState','on','SaveOutput', 'on' , 'SrcWorkspace', 'current');
catch ME
    display('Fail Message:')
    display(ME.message)
    ME.stack   
end


save_Results_pr(simout, NrTrimpoint, run, FilePath, resultpath, ind, SeeAllInfluences);


end

