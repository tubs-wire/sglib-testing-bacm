% my_trim.m

List_V     = [40;45;50;55];
List_Alt   = [0;305;914];
List_gamma = [1;0;-3;-6];

tfp = fopen('main_trim.m','w');
fprintf(tfp,'%% Scipt for trimming the model\n\n');
for i1=1:length(List_Alt)
  for i2=1:length(List_V)
    for i3=1:length(List_gamma)
      % Alt    ... m
      % V_TAS  ... m/s
      % gamma  ... rad
      Alt   = List_Alt(i1);
      V_TAS = List_V(i2);
      gamma = List_gamma(i3);
      [Alt,V_TAS,gamma]
      fprintf(tfp,'clear\n');
      fprintf(tfp,'clc\n\n');
      fprintf(tfp,'Alt   = %20.10e\n', List_Alt(i1));
      fprintf(tfp,'V_TAS = %20.10e\n', List_V(i2));
      fprintf(tfp,'gamma = %20.10e * pi/180\n\n', List_gamma(i3));
      fprintf(tfp,'myBLAC_Trim_HF_Quat\n\n');
      fprintf(tfp,'%%%%%%%%%%\n');
    end
  end
end
fclose(tfp);
