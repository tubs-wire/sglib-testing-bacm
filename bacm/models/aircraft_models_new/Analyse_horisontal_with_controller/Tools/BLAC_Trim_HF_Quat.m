%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                         BLAC_Trim_HF_Quat                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Script                                                                                  *
%*                                                                                                    *
%* Purpose  : Setup trim conditions for the BLAC aircraft model. Call the trim routines and save the  *
%*            results in a MAT-File.                                                                  *
%*                                                                                                    *
%* Version  : 1.3                                                                                     *
%*                                                                                                    *
%* Sub-Functions : DLR_BLAC_Trim.mdl, BLAC_Trimming_Quat.m                                            *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax :  BLAC_Trim_HF_Quat                                                                        *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date     *                     Description                       *
%******************************************************************************************************
%* Christian Raab               * 20-NOV-09 * Basic Design                                            *
%* Jobst H. Diekmann            * 16-APR-12 * SFB 880 Application (6 Flaps with BLC-System)           *
%* Jobst Henning Diekmann       * 15-APR-14 * Simulation MK I Trim Function                  *
%******************************************************************************************************
% clear all
clear all
close all
clc

%cd('/home/joarang/SFB880/REF2-Uncertain/')
%addpath(genpath('/home/joarang/SFB880/REF2-Uncertain/'))
slCharacterEncoding('windows-1252') 


FilePath    = mfilename('fullpath');
FolderName  = 'REF-2 Uncertain Freeze 12-12-2016';
PathIndex   = strfind(FilePath,FolderName);

BasePath = FilePath(1:PathIndex-1);

% Go to root directory
cd([fileparts(fileparts(FilePath)) '/Tools']);

addpath(genpath([BasePath FolderName]))

global BLAC;
global BLAC_Trim;
global TrimSettings;

timestamp           = clock;
lin_flag            = 0;
Checkplot           = 0;
DisplayTrimPoint    = 0;

Path = 'Trimpoints';

TrimSettings.SimMode = 2;                           % trimming
TrimSettings.MdlName = 'BLAC_Trim_Quat';
                                                    
BLAC_Simulation_Init;
disp(['Trim flag: ', num2str(BLAC.Sim.Trim_Flag)]);

%******************************************************************************************************
% Setup of trim parameters for unaccelerated horizontal flight                                        
%******************************************************************************************************
Trim_Case = 1;                  % Specify trim case:                                                 
                                 % 1 = Steady horizontal flight, descent or climb at specified gamma  
                                 % 2 = Steady descent or climb at specified power setting             
                                 % 3 = Steady horizontal flight at specified angle of attack          
                                 % 4 = Steady horizontal flight at specified angle of sideslip        
                                 % 5 = Steady turn at specified bank angle                            
                                 % 6 = Steady turn at specified heading rate                          
                                 % 7 = Steady turn at specified bank angle Left  Engine Inoperative                          
                                 % 8 = Steady turn at specified bank angle Right Engine Inoperative                          
                                 % 9 = Steady turn at specified bank angle                          (diff. Trim-Requirements)   
                                 %10 = Steady turn at specified bank angle Left  Engine Inoperative (diff. Trim-Requirements)                          
                                 %11 = Steady horizontal flight, descent or climb at specified gamma with chi as trim req.
                                 
%******************************************************************************************************
% Setup of trim parameters for unaccelerated horizontal flight                                        *
%******************************************************************************************************
Alt              = 1000; %900 * 0.3048;         % Altitude of the aircraft.                        [m]
Lat              = 0.913345000000000;            % Initial latitude position on earth             [rad] %52.31866 * pi/180;           
Long             = 0.187677000000000;            % Initial longitude position on earth            [rad] %10.54649 * pi/180;
Ma               = 0.15; % 0.1583; % 0.1175;     % Mach number                                      [-]
V_TAS            = 50; %Ma   * 340.294;                 % True Air Speed of the aircraft.                [m/s]
alpha            = 0    * pi/180;                   % Angle of attack at specified trim point        [rad]
beta             = 0    * pi/180;                   % Sideslip at specified trim point               [rad]
phi              = -00  * pi/180;                   % Starting value for bank angle                  [rad]
theta            = 0    * pi/180;                   % Starting value for pitch angle                 [rad]
psi              = 180  * pi/180;                 % Starting value for heading angel               [rad]
chi              = 180  * pi/180;
psi_dot          = 0    * pi/180;                   % Heading rate in case of steady turn          [rad/s]
gamma            = -0   * pi/180;                  % Set preferred flight path angle                [rad]
LH_Throttle      = .1;                            % Throttle setting at the spec. trim point.        [-]
RH_Throttle      = .1;                           % Throttle setting at the spec. trim point.        [-]
Delta_Thrust     = 0;
Flaps            = 65 * pi/180;                  % Flaps deflection at the specified trim point.  [rad]
Disable_BLC      = 1;                            % Activation/Deactivation(0/1) of BLC System       [-]    
Stabilizer       = 0  * pi/180;                  % Stabilizer Setting.                            [rad]
Spoiler          = 0;                            % Spoiler Setting.                               [rad]
Speed_Brake      = 0;                            % Speed Brake Setting                                          [-]
Gear             = 1;                            % Status of the landing gear [0] = retracted  / [1] = extended [-]
NG_Steering      = 0;                            % Nosegear Steering Angle                                    [rad]
Parking_Brake    = 0;                            % Parking_Brake [0] = released / [1] = fully applied           [-]
Ramp             = 0;                            % Status of the ramp door [0] = closed / [1] = open.           [-]
Set_Droop        = 1;
SetCmu           = 0.033;
SetCmuDistrib    = 1/12*ones(1,12);
Set_Slipstream   = 1;
Set_HTP_active   = 0;
Set_Lateral      = 1;
Diff_Thr_Active  = 1;
AeroCrossCoupl   = 0;

% Define Mission
Mission = 3;
% 1 = Reference Mission
% 2 = Maximum Payload
% 3 = Maximum Fuel
% 4 = Aircraft Transfer Mission

% Set State of already used Fuel
Fuel = 1;
% 1     = 100%  Fuel left
% 0.5   = 50%   Fuel left
% 0.1   = 10 %  Fuel left

% Set Type of Loading
Loading = 1;
% 1     = Use Fuel from inside Tanks to outside
% 2     = Use Fuel from outside Tanks to inside


% OEW             = 24061; % Ref-0-2013
% MLW             = 38901; % Ref-0-2013
% MTOW            = 40641; % Ref-0-2013
OEW             = 23898.08;     % REF-2
MLW             = 39079.503;    % REF-2
MTOW            = 41028.15;     % REF-2
% Insert 'MLW' or 'MTOW' in  find_Weight_Balance function in Line 180 as
% an optional argument to load MTOW or MLW Inertia

MTest           = 54800;
Test_AC_Load    = (MLW - OEW);
Test_AC_Mass    = (Test_AC_Load + OEW); 
Trim_Name       = 'Solo_Trim';
X_CG            = 13.6;
S_H             = 27;

%******************************************************************************************************
% Setup of Aerodynamics Initialization                                                               *
%******************************************************************************************************
BLAC.Aerodynamics.Cmu.Cmu_Var_Flg       = 0;
BLAC.Aerodynamics.Cmu.Cmu_Const         = SetCmu / 2;
BLAC.Aerodynamics.Cmu.Comp_Vec          = SetCmuDistrib;
BLAC.Aerodynamics.Unsteady_Flag         = 1;
BLAC.Aerodynamics.Droop.Active          = Set_Droop;
BLAC.Aerodynamics.Cmu.Cmu_ref_set       = BLAC.Aerodynamics.Cmu.Cmu_ref(find(BLAC.Aerodynamics.Flaps.Setting==Flaps));
BLAC.Aerodynamics.Slipstream.On         = Set_Slipstream;
BLAC.Aerodynamics.Active_HTP.On         = Set_HTP_active;
BLAC.Aerodynamics.Inputs.Lat_Motion     = Set_Lateral;
BLAC.Aerodynamics.OEI.Active            = Diff_Thr_Active;
BLAC.Aerodynamics.Lift.CrossCouplFlag   = AeroCrossCoupl;
BLAC.Aerodynamics.Drag.CrossCouplFlag   = AeroCrossCoupl;
BLAC.Aerodynamics.Pitch.CrossCouplFlag  = AeroCrossCoupl;
BLAC.Aerodynamics.Roll.CrossCouplFlag   = AeroCrossCoupl;
BLAC.Aerodynamics.Yaw.CrossCouplFlag    = AeroCrossCoupl;
BLAC.Aerodynamics.Side.CrossCouplFlag   = AeroCrossCoupl;

%******************************************************************************************************
% Setup of Propulsion Initialization                                                               *
%******************************************************************************************************
BLAC.Propulsion.Prop_Efficiency     = 1;
BLAC.Propulsion.OEI                 = 0;
BLAC.Propulsion.BLCOP               = 0;

%******************************************************************************************************
% Setup of Weight and Balance Condition                                                               *
%******************************************************************************************************
% Weight and Balance struct assembly
BLAC_Trim.Weight_Balance                       = BLAC_WeightBalance_Init;
BLAC_Trim.Weight_Balance.DynLoad               = BLAC_DynamicLoad_Init;

[BLAC_Trim.Weight_Balance.AC_Empty BLAC_Trim.Weight_Balance.Fuel_Available] = BLAC_Inertia_CG_Init(Gear, Mission, Fuel, Loading, OEW, 'MLW');
% BLAC_Trim.Weight_Balance.Fuel_Available        = Test_AC_Load;
% BLAC_Trim.Weight_Balance.AC_Empty.CG.x_rp      = -X_CG;

BLAC_Trim.Actuator.Surfaces.Blowing_Active     = Disable_BLC;

BLAC.Controller.Activated                   = 0;
BLAC.Controller                             = BLAC_Controller_Init(BLAC.Controller.Activated, BLAC);
BLAC.Controller.beta_Init                   = 0;
BLAC.Controller.theta_Init                  = 0;
BLAC.Controller.phi_Init                    = 0;
BLAC.Controller.p_Init                      = 0;
BLAC.Controller.q_Init                      = 0;
BLAC.Controller.r_Init                      = 0;
% BLAC.Controller.Aileron_PhiContr_P          = 0; % K_P,Phi Best Value: -1
% BLAC.Controller.Zeta2XiCoupling             = 0; 
% BLAC.Controller.Elevator_betInvContr_P      = 0; % K_Cmbeta
BLAC.Controller.Aileron_pControl_P          = 0; % K_P,p
BLAC.Controller.Elevator_qControl_P         = 0; % K_P,q
% BLAC.Controller.Rudder_rControl_P           = 0; % K_P,r

%******************************************************************************************************
% Setup of Wind Conditons
%******************************************************************************************************
% Wind Properties
Environment.Wind.V_W_o     =  0/1.943845;         % Magnitude of the Wind Velocity Vector         [m/s]
Environment.Wind.Direction =  0;                  % Direction of the Wind Veloctiy Vector         [deg]

BLAC.Environment.Wind.u_Wreq            = 0;%5.144; %[m/s] = 10kts
BLAC.Environment.Wind.psi_Wreq          = 90 * pi/180; 
BLAC.Environment.Terrain_Elevation      = -50;

% Environment struct assembly
BLAC_Trim.Environment = Environment;

%******************************************************************************************************
% Setup of initial positons for coord. transformations
%******************************************************************************************************
BLAC_Trim.Rigid_Body_Init.Position.lat      = Lat;                 % WGS-84 Latitude              [rad]
BLAC_Trim.Rigid_Body_Init.Position.long     = Long;                % WGS-84 Longitude             [rad]
        
BLAC.Configuration.Geometry.S_H             = S_H;

BLAC.Sim.Boost_Flag  = 0;

%******************************************************************************************************
% Assignment of Input, Output and State Vector Names                                                  *
%******************************************************************************************************
[X_Name,U_Name,Y_Name]=assignNames;

InputArgs.Alt           = Alt;
InputArgs.Lat           = Lat;
InputArgs.Long          = Long;
InputArgs.V_TAS         = V_TAS;
InputArgs.alpha         = alpha;
InputArgs.beta_A        = beta;
InputArgs.phi           = phi;
InputArgs.theta         = theta;
InputArgs.psi           = psi;
InputArgs.gamma         = gamma;
InputArgs.LH_Throttle   = LH_Throttle;
InputArgs.RH_Throttle   = RH_Throttle;
InputArgs.Delta_Thrust  = Delta_Thrust;
InputArgs.Flaps         = Flaps;
InputArgs.Spoiler       = Spoiler;
InputArgs.Speed_Brake   = Speed_Brake;
InputArgs.Gear          = Gear;
InputArgs.NG_Steering   = NG_Steering;
InputArgs.Parking_Brake = Parking_Brake;
InputArgs.Ramp          = Ramp;
InputArgs.Stabilizer    = Stabilizer;
InputArgs.psi_dot       = psi_dot;
InputArgs.Ma            = Ma;
InputArgs.Cmu           = SetCmu * SetCmuDistrib;
InputArgs.chi           = chi;

%******************************************************************************************************
% Call Trimming function for the BLAC                                                                *
%******************************************************************************************************
[X0, U0, tY,error_flg] = ...
           BLAC_Trimming_Quat(InputArgs,...
           U_Name, X_Name, Y_Name, Trim_Case);

%******************************************************************************************************
%* BLAC_Trim Struct Assembly                                                                          *
%******************************************************************************************************

        % Save names of the input, states and output vector
        BLAC_Trim.Names.U_Name    = U_Name;                        % Input vector names
        BLAC_Trim.Names.X_Name    = X_Name;                        % State vector names
        BLAC_Trim.Names.Y_Name    = Y_Name;                        % Output vector names

        % Save Success
        BLAC_Trim.Error = error_flg;

        % Save Initial Vectors
        BLAC_Trim.Init.X0 = X0;                                    % Initial State Vector
        BLAC_Trim.Init.U0 = U0;                                    % Initial Input Vector
        BLAC_Trim.Init.Y0 = tY;                                    % Initial Ouput Vector

        % Save initial control vector values in struct
        BLAC_Trim.U0.LH_Throttle      = U0(1);                     % [-]
        BLAC_Trim.U0.RH_Throttle      = U0(2);                     % [-]
        BLAC_Trim.U0.Aileron          = U0(3);                     % [rad]
        BLAC_Trim.U0.Rudder           = U0(4);                     % [rad]
        BLAC_Trim.U0.Elevator         = U0(5);                     % [rad]
        BLAC_Trim.U0.Flaps            = U0(6);                     % [rad]
        BLAC_Trim.U0.Stabilizer       = U0(7);                     % [rad]
        BLAC_Trim.U0.Speed_Brake      = U0(8);                     % [-]
        BLAC_Trim.U0.Ramp             = U0(9);                     % [-]
        BLAC_Trim.U0.Wheel_Brake_LH   = U0(10);                    % [-]
        BLAC_Trim.U0.Wheel_Brake_RH   = U0(11);                    % [-]
        BLAC_Trim.U0.NG_Steering      = U0(12);                    % [rad]
        BLAC_Trim.U0.Parking_Brake    = U0(13);                    % [-]
        BLAC_Trim.U0.Gear             = U0(14);                    % [-]
        BLAC_Trim.U0.Compressor       = U0(15);                    % [-]

        % Set Initial State Vector Values
        % Translation
        BLAC_Trim.Rigid_Body_Init.Translation.u_K_b = X0(1);       % [m/s]
        BLAC_Trim.Rigid_Body_Init.Translation.v_K_b = X0(2);       % [m/s]
        BLAC_Trim.Rigid_Body_Init.Translation.w_K_b = X0(3);       % [m/s]

        % Rotation
        BLAC_Trim.Rigid_Body_Init.Rotation.p_K_b    = X0(20);      % [m/s]
        BLAC_Trim.Rigid_Body_Init.Rotation.q_K_b    = X0(19);      % [m/s]
        BLAC_Trim.Rigid_Body_Init.Rotation.r_K_b    = X0(21);      % [m/s]

        % Attitude
        BLAC_Trim.Rigid_Body_Init.Attitude.q_0      = X0(4);       % [rad]
        BLAC_Trim.Rigid_Body_Init.Attitude.q_1      = X0(5);       % [rad]
        BLAC_Trim.Rigid_Body_Init.Attitude.q_2      = X0(6);       % [rad]
        BLAC_Trim.Rigid_Body_Init.Attitude.q_3      = X0(7);       % [rad]

        % Position
        BLAC_Trim.Rigid_Body_Init.Position.h        = X0(8);       % [m]
        BLAC_Trim.Rigid_Body_Init.Position.lat      = X0(9);       % [rad]
        BLAC_Trim.Rigid_Body_Init.Position.long     = X0(51);       % [rad]

        % Propulsion
        BLAC_Trim.Propulsion.LH_Inner_Throttle_Init     = X0(35);        % [-]
        BLAC_Trim.Propulsion.RH_Inner_Throttle_Init     = X0(36);        % [-]
        BLAC_Trim.Propulsion.LH_Inner_Throttle_Init_dot = X0(71);        % [-/s]
        BLAC_Trim.Propulsion.RH_Inner_Throttle_Init_dot = X0(72);        % [-/s]

        % Booster Actuators
        BLAC_Trim.Actuator.Booster.Stabilizer       = X0(37);      % [rad]
        BLAC_Trim.Actuator.Booster.Elevator         = X0(39);      % [rad]
        BLAC_Trim.Actuator.Booster.Rudder           = X0(42);      % [rad]
        BLAC_Trim.Actuator.Booster.Aileron          = X0(46);      % [rad]
        BLAC_Trim.Actuator.Booster.Aileron_Dot      = X0(52);      % [rad/s]
        BLAC_Trim.Actuator.Booster.Elevator_Dot     = X0(53);      % [rad/s]
        BLAC_Trim.Actuator.Booster.Rudder_Dot       = X0(68);      % [rad/s]
        BLAC_Trim.Actuator.Booster.Stabilizer_Dot   = X0(70);      % [rad/s]
        BLAC_Trim.Actuator.Spoiler_Init             = X0(50);      % [rad]
        BLAC_Trim.Actuator.Speed_Brake_Init         = X0(69);      % [-]
        BLAC_Trim.Actuator.Flaps_Init               = X0(34);      % [rad]
        BLAC_Trim.Actuator.Ramp_Init                = X0(49);      % [-]
        BLAC_Trim.Actuator.NG_Eta_Boost             = X0(54);      % [rad]
        BLAC_Trim.Actuator.NG_Eta_Boost_dot         = X0(55);      % [rad/s]
        
        BLAC_Trim.Actuator.Booster.Compressor       = [ X0(22) ... % [-]
                                                        X0(23) ... % [-]
                                                        X0(24) ... % [-]
                                                        X0(25) ... % [-]
                                                        X0(26) ... % [-]
                                                        X0(27) ... % [-]
                                                        X0(28) ... % [-]
                                                        X0(29) ... % [-]
                                                        X0(30) ... % [-]
                                                        X0(31) ... % [-]
                                                        X0(32) ... % [-]
                                                        X0(33)];   % [-]
                                                    
        BLAC_Trim.Actuator.Booster.Compressor_Dot   = [ X0(56) ... % [-/s]
                                                        X0(57) ... % [-/s]
                                                        X0(58) ... % [-/s]
                                                        X0(59) ... % [-/s]
                                                        X0(60) ... % [-/s]
                                                        X0(61) ... % [-/s]
                                                        X0(62) ... % [-/s]
                                                        X0(63) ... % [-/s]
                                                        X0(64) ... % [-/s]
                                                        X0(65) ... % [-/s]
                                                        X0(66) ... % [-/s]
                                                        X0(67)];   % [-/s]

        % Sensors
        BLAC_Trim.Sensors.Temperature               = X0(73);      % [K]
        BLAC_Trim.Sensors.alpha                     = X0(74);      % [rad]
        BLAC_Trim.Sensors.alpha_dot                 = X0(75);      % [rad/s]
        BLAC_Trim.Sensors.beta                      = X0(76);      % [rad]
        BLAC_Trim.Sensors.beta_dot                  = X0(77);      % [rad/s]
        BLAC_Trim.Sensors.nLoc                      = X0(78);      % [DDM]
        BLAC_Trim.Sensors.nGs                       = X0(79);      % [DDM]
        BLAC_Trim.Sensors.oF_frequency              = X0(80);      % [-/s]
        BLAC_Trim.Sensors.oF_time                   = X0(81);      % [s]
        BLAC_Trim.Sensors.dLoc_receive              = X0(82);      % [DDM]
        BLAC_Trim.Sensors.dGs_receive               = X0(83);      % [DDM]
        

        BLAC.Controller.beta_Init                   = BLAC_Trim.Init.Y0(6);
        BLAC.Controller.theta_Init                  = BLAC_Trim.Init.Y0(4);
        BLAC.Controller.phi_Init                    = BLAC_Trim.Init.Y0(7);
        BLAC.Controller.p_Init                      = BLAC_Trim.Init.Y0(9);
        BLAC.Controller.q_Init                      = BLAC_Trim.Init.Y0(3);
        BLAC.Controller.r_Init                      = BLAC_Trim.Init.Y0(10);
   
        % Save Success
        BLAC_Trim.Error = error_flg;
        
if DisplayTrimPoint        
        % Display Results
        disp(['V_TAS         [kt]: ', num2str(BLAC_Trim.Init.Y0(1)*1.9438)]);
        disp(['V_TAS        [m/s]: ', num2str(BLAC_Trim.Init.Y0(1))]);
        disp(['V_IAS         [kt]: ', num2str(BLAC_Trim.Init.Y0(12)*1.9438)]);
        disp(['q_inf      [N/m^2]: ', num2str(BLAC_Trim.Init.Y0(25))]);
        disp(['V_K          [m/s]: ', num2str(BLAC_Trim.Init.Y0(30))]);
        disp(['V_A          [m/s]: ', num2str(BLAC_Trim.Init.Y0(31))]);
        disp(['V_Vert       [m/s]: ', num2str(BLAC_Trim.Init.Y0(29))]);
        disp(['V_Ground     [m/s]: ', num2str(BLAC_Trim.Init.Y0(32))]);
        disp(['Ma             [-]: ', num2str(BLAC_Trim.Init.Y0(15))]);
        disp(['Alt            [m]: ', num2str(BLAC_Trim.Init.Y0(5))]);
        disp(['gamma        [deg]: ', num2str(BLAC_Trim.Init.Y0(11)*180/pi)]);
        disp(['alpha        [deg]: ', num2str(BLAC_Trim.Init.Y0(2)*180/pi)]);
        disp(['beta         [deg]: ', num2str(BLAC_Trim.Init.Y0(6)*180/pi)]);
        disp(['Epsilon      [deg]: ', num2str((BLAC_Trim.Init.Y0(2)*180/pi) + (BLAC_Trim.Init.U0(7)*180/pi) - (BLAC_Trim.Init.Y0(17)*180/pi))]);
        disp(['alpha_H      [deg]: ', num2str(BLAC_Trim.Init.Y0(17)*180/pi)]);
        disp(['phi_dot    [rad/s]: ', num2str(BLAC_Trim.Init.Y0(36))]);
        disp(['theta_dot  [rad/s]: ', num2str(BLAC_Trim.Init.Y0(37))]);
        disp(['psi_dot    [rad/s]: ', num2str(BLAC_Trim.Init.Y0(38))]);
        disp(['beta_dot   [rad/s]: ', num2str(BLAC_Trim.Init.Y0(40))]);
        disp(['alpha_dot  [rad/s]: ', num2str(BLAC_Trim.Init.Y0(41))]);
        disp(['Elevator     [deg]: ', num2str(BLAC_Trim.Init.U0(5)*180/pi)]);
        disp(['Aileron      [deg]: ', num2str(BLAC_Trim.Init.U0(3)*180/pi)]);
        disp(['Rudder       [deg]: ', num2str(BLAC_Trim.Init.U0(4)*180/pi)]);
        disp(['HTP-Trim     [deg]: ', num2str(BLAC_Trim.Init.U0(7)*180/pi)]);
        disp(['C_mu           [-]: ', num2str(BLAC_Trim.Init.Y0(14))]);
        disp(['Flaps        [deg]: ', num2str(BLAC_Trim.Init.U0(6)*180/pi)]);
        disp(['phi          [deg]: ', num2str(BLAC_Trim.Init.Y0(7)*180/pi)]);
        disp(['theta        [deg]: ', num2str(BLAC_Trim.Init.Y0(4)*180/pi)]);
        disp(['psi          [deg]: ', num2str(BLAC_Trim.Init.Y0(8)*180/pi)]);
        disp(['chi          [deg]: ', num2str(BLAC_Trim.Init.Y0(44)*180/pi)]);
%         disp(['Look_Up        [-]: ', num2str(BLAC_Trim.Init.Y0(16))]);
%         disp(['q1             [-]: ', num2str(BLAC_Trim.Init.X0(4))]);
%         disp(['q2             [-]: ', num2str(BLAC_Trim.Init.X0(5))]);
%         disp(['q3             [-]: ', num2str(BLAC_Trim.Init.X0(6))]);
%         disp(['q4             [-]: ', num2str(BLAC_Trim.Init.X0(7))]);
        disp(['LH_Throttle    [%]: ', num2str(BLAC_Trim.Init.U0(1)*100)]);
        disp(['RH_Throttle    [%]: ', num2str(BLAC_Trim.Init.U0(2)*100)]);
%         disp(['Delta_Thrust   [N]: ', num2str(BLAC_Trim.Init.Y0(39))]);
        disp(['Thrust         [N]: ', num2str(BLAC_Trim.Init.Y0(18))]);
        disp(['Thrust Av      [N]: ', num2str(BLAC_Trim.Init.Y0(19)*2)]);
        disp(['C_L            [-]: ', num2str(BLAC_Trim.Init.Y0(20))]);
        disp(['C_D            [-]: ', num2str(BLAC_Trim.Init.Y0(24))]);
        disp(['E              [-]: ', num2str(BLAC_Trim.Init.Y0(20)/BLAC_Trim.Init.Y0(24))]);
        disp(['C_LFR          [-]: ', num2str(BLAC_Trim.Init.Y0(43))]);
        disp(['C_LH           [-]: ', num2str(BLAC_Trim.Init.Y0(21))]);
        disp(['dCL_T          [-]: ', num2str(BLAC_Trim.Init.Y0(26))]);
        disp(['dCm_T          [-]: ', num2str(BLAC_Trim.Init.Y0(45))]);
        disp(['dCl_T          [-]: ', num2str(BLAC_Trim.Init.Y0(27))]);
        disp(['dCn_T          [-]: ', num2str(BLAC_Trim.Init.Y0(46))]);
        disp(['dCY_T          [-]: ', num2str(BLAC_Trim.Init.Y0(28))]);
        disp(['AC-Weight     [kg]: ', num2str(Test_AC_Mass)]);
%         disp(['Set Slip       [-]: ', num2str(Set_Slipstream)]);
%         disp(['Set HTPact     [-]: ', num2str(Set_HTP_active)]);
%         disp(['Error          [-]: ', num2str(BLAC_Trim.Error)]);
%         disp(['h_dotdot   [m/s^2]: ', num2str(BLAC_Trim.Init.Y0(42))]);
%         disp(['h_dot        [m/s]: ', num2str(BLAC_Trim.Init.Y0(33)* sind(BLAC_Trim.Init.Y0(11)*180/pi))]);
        disp(['C_ma           [-]: ', num2str(BLAC_Trim.Init.Y0(33))]);
        disp(['C_La           [-]: ', num2str(BLAC_Trim.Init.Y0(34))]);
        disp(['X_N            [-]: ', num2str(BLAC_Trim.Init.Y0(35))]);
        disp(['CG             [%]: ', num2str(BLAC_Trim.Weight_Balance.AC_Empty.CG.MAC)]);
        disp(['x_CG           [m]: ', num2str(BLAC_Trim.Weight_Balance.AC_Empty.CG.x_rp)]);
end

        TrimPointInfo.V_TAS_kt      = (BLAC_Trim.Init.Y0(1)*1.9438);
        TrimPointInfo.V_TAS_mps     = (BLAC_Trim.Init.Y0(1));
        TrimPointInfo.V_IAS_kt      = (BLAC_Trim.Init.Y0(12)*1.9438);
        TrimPointInfo.Ma            = (BLAC_Trim.Init.Y0(15));
        TrimPointInfo.Alt_m         = (BLAC_Trim.Init.Y0(5));
        TrimPointInfo.gamma_deg     = (BLAC_Trim.Init.Y0(11)*180/pi);
        TrimPointInfo.alpha_deg     = (BLAC_Trim.Init.Y0(2)*180/pi);
        TrimPointInfo.beta_deg      = (BLAC_Trim.Init.Y0(6)*180/pi);
        TrimPointInfo.HTP_Trim_deg  = (BLAC_Trim.Init.U0(7)*180/pi);
        TrimPointInfo.Throttle      = (BLAC_Trim.Init.U0(1)*100);
        TrimPointInfo.Throttle_LH   = (BLAC_Trim.Init.U0(1)*100);
        TrimPointInfo.Throttle_RH   = (BLAC_Trim.Init.U0(2)*100);
        TrimPointInfo.ThrustpEng    = (BLAC_Trim.Init.Y0(18)/2);
        TrimPointInfo.Elevator_deg  = (BLAC_Trim.Init.U0(4)*180/pi);
        TrimPointInfo.Aileron_deg   = (BLAC_Trim.Init.U0(3)*180/pi);
        TrimPointInfo.Rudder_deg    = (BLAC_Trim.Init.U0(4)*180/pi);
        TrimPointInfo.phi_deg       = (BLAC_Trim.Init.Y0(7)*180/pi);
        TrimPointInfo.theta_deg     = (BLAC_Trim.Init.Y0(4)*180/pi);
        TrimPointInfo.psi_deg       = (BLAC_Trim.Init.Y0(8)*180/pi);
        TrimPointInfo.Flaps_deg     = (BLAC_Trim.Init.U0(6)*180/pi);
        TrimPointInfo.C_mu          = (BLAC_Trim.Init.Y0(14));
        TrimPointInfo.SetCmuDistrib =  SetCmuDistrib;
        TrimPointInfo.alpha_H       = (BLAC_Trim.Init.Y0(17)*180/pi);
        TrimPointInfo.Epsilon       = (TrimPointInfo.alpha_deg + TrimPointInfo.HTP_Trim_deg - TrimPointInfo.alpha_H);
        TrimPointInfo.q_0           = (BLAC_Trim.Init.X0(4));
        TrimPointInfo.q_1           = (BLAC_Trim.Init.X0(5));
        TrimPointInfo.q_2           = (BLAC_Trim.Init.X0(6));
        TrimPointInfo.q_3           = (BLAC_Trim.Init.X0(7));
        TrimPointInfo.Error         = (BLAC_Trim.Error);
        TrimPointInfo.AC_Mass       = (BLAC_Trim.Weight_Balance.AC_Empty.Mass + BLAC_Trim.Weight_Balance.Fuel_Available);
        TrimPointInfo.AC_Mass_Empty = (BLAC_Trim.Weight_Balance.AC_Empty.Mass);
        TrimPointInfo.Add_Mass      = (BLAC_Trim.Weight_Balance.Fuel_Available);
        TrimPointInfo.Gear          = (Gear);
        TrimPointInfo.Mission       = (Mission);
        TrimPointInfo.Fuel          = (Fuel);
        TrimPointInfo.Loading       = (Loading);
        TrimPointInfo.SetDroop      = (Set_Droop);
        TrimPointInfo.SetSlipstream = (Set_Slipstream);
        TrimPointInfo.SetHTPactive  = (Set_HTP_active);
        TrimPointInfo.SetLateral    = (Set_Lateral);
        TrimPointInfo.AeroCrossCoupl= (AeroCrossCoupl);
        TrimPointInfo.x_CG          = (BLAC_Trim.Weight_Balance.AC_Empty.CG.x_rp);
        TrimPointInfo.CG            = (BLAC_Trim.Weight_Balance.AC_Empty.CG.MAC);
        TrimPointInfo.S_H           = (BLAC.Configuration.Geometry.S_H);
        TrimPointInfo.C_L           = (BLAC_Trim.Init.Y0(20));
        TrimPointInfo.C_ma          = (BLAC_Trim.Init.Y0(33));
        TrimPointInfo.C_La          = (BLAC_Trim.Init.Y0(34));
        TrimPointInfo.X_N           = (BLAC_Trim.Init.Y0(35));
        TrimPointInfo.C_LH          = (BLAC_Trim.Init.Y0(21));
        TrimPointInfo.F_A           = (BLAC_Trim.Init.Y0(22));
        TrimPointInfo.F_AH          = (BLAC_Trim.Init.Y0(23));
        TrimPointInfo.C_D           = (BLAC_Trim.Init.Y0(24));
        TrimPointInfo.q_inf         = (BLAC_Trim.Init.Y0(25));
        TrimPointInfo.Thrust        = (BLAC_Trim.Init.Y0(18));
        TrimPointInfo.Thrust_avail  = (BLAC_Trim.Init.Y0(19)*2);
        TrimPointInfo.Engine_Mode   = (BLAC_Trim.Init.Y0(16));
        TrimPointInfo.phi_dot       = (BLAC_Trim.Init.Y0(36));
        TrimPointInfo.theta_dot     = (BLAC_Trim.Init.Y0(37));
        TrimPointInfo.psi_dot       = (BLAC_Trim.Init.Y0(38));
        TrimPointInfo.Delta_Thrust  = (BLAC_Trim.Init.Y0(39));
        TrimPointInfo.beta_dot      = (BLAC_Trim.Init.Y0(40));
        TrimPointInfo.alpha_dot     = (BLAC_Trim.Init.Y0(41));
        TrimPointInfo.h_dotdot      = (BLAC_Trim.Init.Y0(42));
        TrimPointInfo.C_LFR         = (BLAC_Trim.Init.Y0(43));
        
%******************************************************************************************************
%% Linearize the model (if lin_flag == 1)                                                             
%******************************************************************************************************
if lin_flag == 1
    [BLAC_Trim.LinSys.A BLAC_Trim.LinSys.B BLAC_Trim.LinSys.C BLAC_Trim.LinSys.D] = BLAC_Linearize(BLAC,BLAC_Trim,TrimSettings);
    [BLAC_Trim.LinSys.ReducedSys BLAC_Trim.LinSys.ReducedSysACT BLAC_Trim.LinSys.ReducedSysLon BLAC_Trim.LinSys.ReducedSysLat BLAC_Trim.LinSys.ReducedSysLonA] = BLAC_Reduce_System(BLAC_Trim, BLAC, Checkplot);
    [myroots,myindex] = f_matrixanalysis(BLAC_Trim.LinSys.ReducedSysACT.sys,0,1,0)
end




%******************************************************************************************************
% Save the Trim Results to a MAT-File                                                                 *
%******************************************************************************************************

                                 % 1 = Steady horizontal flight, descent or climb at specified gamma  
                                 % 2 = Steady descent or climb at specified power setting             
                                 % 3 = Steady horizontal flight at specified angle of attack          
                                 % 4 = Steady horizontal flight at specified angle of sideslip        
                                 % 5 = Steady turn at specified bank angle                            
                                 % 6 = Steady turn at specified heading rate                          
                                 % 7 = Steady turn at specified bank angle Left  Engine Inoperative                          
                                 % 8 = Steady turn at specified bank angle Right Engine Inoperative                          

CaseNames{1} = 'HF';
CaseNames{2} = 'PowerSet';
CaseNames{3} = 'HFAoA';
CaseNames{4} = 'HFSideslip';
CaseNames{5} = 'TurnBank';
CaseNames{6} = 'TurnHeading';
CaseNames{7} = 'OEILH';
CaseNames{8} = 'OEIRH';
CaseNames{9} = 'TurnBankDiffTrim';
CaseNames{10}= 'TurnBankLHOEIDiffTrim';
CaseNames{11}= 'HFchi';

TrimSettings.strTrimfile = [BasePath FolderName '\Tools\Trimpoint\'...
                            'Alt'   num2str(round(BLAC_Trim.Init.Y0(5)))...
                            '_V'    num2str(round(BLAC_Trim.Init.Y0(1)))...
                            '_FPA'  num2str(round(BLAC_Trim.Init.Y0(11)*180/pi))...
                            '_Bank' num2str(round(BLAC_Trim.Init.Y0(7)*180/pi))...
                            '_' CaseNames{Trim_Case} '_' ...
                            datestr(now, 'ddmmyyyyHHMMSS')...
                            '.mat'];       

if ~exist([BasePath FolderName '\Tools\Trimpoint\'],'dir')
    mkdir([BasePath FolderName '\Tools\Trimpoint\']);
end

if(isempty(TrimSettings.strTrimfile) == 1)
    % Ask for filename
    [FileName,PathName] = uiputfile('*.mat','Save Trim-Parameter File');
    TrimSettings.strTrimfile = strcat(PathName, FileName);
end

% Save structure in MAT-File
if(isempty(TrimSettings.strTrimfile) == 1)
    disp('Warning: No file specified. Nothing saved!');
else
    
    save(TrimSettings.strTrimfile, 'BLAC_Trim', 'TrimPointInfo', 'X0', 'tY', 'U0');
    disp(['Save-File: Trim'...
                        'H' num2str(floor(Alt))...
                        'Ma0' num2str(floor(BLAC_Trim.Init.Y0(15)*100))...
                        'FPA' num2str(floor(gamma)) '_'...
                        date '_'...
                        num2str(timestamp(4))...
                        num2str(timestamp(5))])
end

% Clear Workspace
% clear all;
%---------------------------------------------------------------------------------------------------EOF
