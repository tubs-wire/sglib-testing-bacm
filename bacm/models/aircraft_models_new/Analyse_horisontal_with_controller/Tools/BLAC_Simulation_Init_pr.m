function [BLAC_Trim,X0,U0,tY,TrimPointInfo, TrimSettings, BLAC] = BLAC_Simulation_Init_pr(TrimSettings)
%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Simulation_Init                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the general simulation parameters.                                          *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Simulation_Init                                                                      *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************

if(TrimSettings.SimMode == 2)
	% Call Initialization Routines
	BLAC.Configuration  =   BLAC_Configuration_Init;       % Aircraft configuration
	BLAC.Aerodynamics   =   BLAC_Aerodynamics_Init;        % Aerodynamic tables
	BLAC.Propulsion     =   BLAC_Propulsion_Init;          % Propulsion data
	BLAC.Gear           =   BLAC_Gear_Init;                % Landing gear data
	BLAC.Sensors        =   BLAC_Sensors_Init;             % Sensor/Instrument/ILS data
	BLAC.Environment    =   BLAC_Environment_Init;         % Environment constants
	BLAC.Actuator       =   BLAC_Actuator_Init;            % Actuator data
    BLAC.Tail_Bumper    =   BLAC_Tail_Bumper_Init;         % Tail Bumper chracteristics
	BLAC.Sim            =   BLAC_Sim_Init;                 % Simulation init
	BLAC.NavData        =   BLAC_NavData_Init;             % Navigation Data VOR/DME Init
    BLAC.Controller     =   BLAC_Controller_Init(0, BLAC); % Initial Setting Controller (Input: De-/Activate 0/1, Actuator Characteristics)

%     Uncertainty_Fixed

	BLAC.Sim.Trim_Flag  =  1;                              % Set Trim Flag to ON

	BLAC_Trim.Weight_Balance.DynLoad = BLAC_DynamicLoad_Init;

	% Release Control Vector Limits
	BLAC.Actuator.Surfaces.Aileron.max    =  90 * pi/180;  % Max. Aileron deflection angle        [rad]
	BLAC.Actuator.Surfaces.Aileron.min    = -90 * pi/180;  % Min. Aileron deflection angle        [rad]
   
	BLAC.Actuator.Surfaces.Elevator.max   =  90 * pi/180;  % Max. Elevator deflection angle       [rad]
	BLAC.Actuator.Surfaces.Elevator.min   = -90 * pi/180;  % Min. Elevator deflection angle       [rad]

	BLAC.Actuator.Surfaces.Stabilizer.max =  90 * pi/180;  % Max. Stabilizer deflection angle     [rad]
	BLAC.Actuator.Surfaces.Stabilizer.min = -90 * pi/180;  % Min. Stabilizer deflection angle     [rad]

	BLAC.Actuator.Surfaces.Rudder.max     =  90 * pi/180;  % Max. Rudder deflection angle         [rad]
	BLAC.Actuator.Surfaces.Rudder.min     = -90 * pi/180;  % Min. Rudder deflection angle         [rad]

	BLAC.Propulsion.Throttle.Max          =  inf;          % Set throttle limits to inf.            [-]
	BLAC.Propulsion.Throttle.Min          = -inf;          % Set throttle limits to inf.            [-]

	% Set Dummy Control Vector Values
	BLAC_Trim.U0.LH_Throttle          = 0;                           % [-]
	BLAC_Trim.U0.RH_Throttle          = 0;                           % [-]
	BLAC_Trim.U0.Throttle             = 0;                           % [-]
	BLAC_Trim.U0.Aileron              = 0;                           % [rad]
	BLAC_Trim.U0.Elevator             = 0;                           % [rad]
	BLAC_Trim.U0.Rudder               = 0;                           % [rad]
	BLAC_Trim.U0.Flaps                = 0;                           % [rad]
	BLAC_Trim.U0.Stabilizer           = 0;                           % [rad]
	BLAC_Trim.U0.Speed_Brake          = 0;                           % [-]
	BLAC_Trim.U0.Ramp                 = 0;                           % [-]
	BLAC_Trim.U0.NG_Steering          = 0;                           % [rad]
	BLAC_Trim.U0.Parking_Brake        = 0;                           % [-]
	BLAC_Trim.U0.Gear                 = 0;                           % [-]
	BLAC_Trim.U0.Compressor           = 0;                           % [-]

	% Set Dummy State Vector Values
	% Translation
	BLAC_Trim.Rigid_Body_Init.Translation.u_K_b = 77;                % [m/s]
	BLAC_Trim.Rigid_Body_Init.Translation.v_K_b = 0;                 % [m/s]
	BLAC_Trim.Rigid_Body_Init.Translation.w_K_b = 0;                 % [m/s]

	% Rotation
	BLAC_Trim.Rigid_Body_Init.Rotation.p_K_b = 0;                    % [m/s]
	BLAC_Trim.Rigid_Body_Init.Rotation.q_K_b = 0;                    % [m/s]
	BLAC_Trim.Rigid_Body_Init.Rotation.r_K_b = 0;                    % [m/s]

	% Attitude
	BLAC_Trim.Rigid_Body_Init.Attitude.phi   = 0;                    % [rad]
	BLAC_Trim.Rigid_Body_Init.Attitude.theta = 0;                    % [rad]
	BLAC_Trim.Rigid_Body_Init.Attitude.psi   = 0;                    % [rad]
	% Attitude for quaternions
	BLAC_Trim.Rigid_Body_Init.Attitude.q_0   = 1;                    % [-]
	BLAC_Trim.Rigid_Body_Init.Attitude.q_1   = 0;                    % [-]
	BLAC_Trim.Rigid_Body_Init.Attitude.q_2   = 0;                    % [-]
	BLAC_Trim.Rigid_Body_Init.Attitude.q_3   = 0;                    % [-]

	% Position
	BLAC_Trim.Rigid_Body_Init.Position.x_o = 0;                      % [m]
	BLAC_Trim.Rigid_Body_Init.Position.y_o = 0;                      % [m]
	BLAC_Trim.Rigid_Body_Init.Position.z_o = -1000;                  % [m]
    % Position for new navigation equations
    BLAC_Trim.Rigid_Body_Init.Position.h    = 1000;                  % [m]
    BLAC_Trim.Rigid_Body_Init.Position.lat  = 50 * pi/180;           % [rad]
    BLAC_Trim.Rigid_Body_Init.Position.long = 0;                     % [rad]

	% Propulsion
	BLAC_Trim.Propulsion.LH_Outer_Throttle_Init     = 0;             % [-] 
	BLAC_Trim.Propulsion.LH_Inner_Throttle_Init     = 0;             % [-] 
	BLAC_Trim.Propulsion.RH_Outer_Throttle_Init     = 0;             % [-] 
	BLAC_Trim.Propulsion.RH_Inner_Throttle_Init     = 0;             % [-]
	BLAC_Trim.Propulsion.LH_Outer_Throttle_Init_dot = 0;             % [-/s] 
	BLAC_Trim.Propulsion.LH_Inner_Throttle_Init_dot = 0;             % [-/s] 
	BLAC_Trim.Propulsion.RH_Outer_Throttle_Init_dot = 0;             % [-/s] 
	BLAC_Trim.Propulsion.RH_Inner_Throttle_Init_dot = 0;             % [-/s]
	BLAC_Trim.Propulsion.Fuel_Mass_Consumed         = 0;             % [kg]

	% Booster Actuator Dummys
	BLAC_Trim.Actuator.Booster.Aileron        = 0;                   % [rad]
	BLAC_Trim.Actuator.Booster.Aileron_Dot    = 0;                   % [rad/s]
	BLAC_Trim.Actuator.Booster.Rudder         = 0;                   % [rad]
	BLAC_Trim.Actuator.Booster.Rudder_Dot     = 0;                   % [rad/s]
	BLAC_Trim.Actuator.Booster.Elevator       = 0;                   % [rad]
	BLAC_Trim.Actuator.Booster.Elevator_Dot   = 0;                   % [rad/s]
	BLAC_Trim.Actuator.Booster.Stabilizer     = 0;                   % [rad]
	BLAC_Trim.Actuator.Booster.Stabilizer_Dot = 0;                   % [rad/s]
	BLAC_Trim.Actuator.Spoiler_Init           = 0;                   % [rad]
	BLAC_Trim.Actuator.Speed_Brake_Init       = 0;                   % [-]
	BLAC_Trim.Actuator.Flaps_Init             = 0;                   % [rad]
	BLAC_Trim.Actuator.Gear_Init              = 0;                   % [-]
	BLAC_Trim.Actuator.Ramp_Init              = 0;                   % [-]
	BLAC_Trim.Actuator.NG_Eta_Boost           = 0;                   % [rad]
	BLAC_Trim.Actuator.NG_Eta_Boost_dot       = 0;                   % [rad/s]
	BLAC_Trim.Actuator.BLC                    = 0;                   % [rad]
    BLAC_Trim.Actuator.Booster.Compressor     = zeros(1,12);         % [-]
    BLAC_Trim.Actuator.Booster.Compressor_Dot = zeros(1,12);         % [-/s]

	% Sensors
	BLAC_Trim.Sensors.Temperature             = 288.15;              % [K]
	BLAC_Trim.Sensors.alpha                   = 0;                   % [rad]
	BLAC_Trim.Sensors.alpha_dot               = 0;                   % [rad/s]
	BLAC_Trim.Sensors.beta                    = 0;                   % [rad]
	BLAC_Trim.Sensors.beta_dot                = 0;                   % [rad/s]
    BLAC_Trim.Sensors.nLoc                    = 0;                   % [DDM]
    BLAC_Trim.Sensors.nGs                     = 0;                   % [DDM]
    BLAC_Trim.Sensors.oF_frequency            = 0;                   % [-/s]
    BLAC_Trim.Sensors.oF_time                 = 0;                   % [s]
    BLAC_Trim.Sensors.dLoc_receive            = 0;                   % [DDM]
    BLAC_Trim.Sensors.dGs_receive             = 0;                   % [DDM]

	% Assign variable structure to workspace
	% assignin('base', 'BLAC', BLAC);
	% assignin('base', 'BLAC_Trim', BLAC_Trim);
	
elseif(TrimSettings.SimMode < 5)
    global BLAC

	% Call Initialization Routines
	BLAC.Configuration  =   BLAC_Configuration_Init;       % Aircraft configuration
	BLAC.Aerodynamics   =   BLAC_Aerodynamics_Init;        % Aerodynamic tables
	BLAC.Propulsion     =   BLAC_Propulsion_Init;          % Propulsion data
	BLAC.Gear           =   BLAC_Gear_Init;                % Landing gear data
	BLAC.Sensors        =   BLAC_Sensors_Init;             % Sensor/ Instrument data
	BLAC.Environment    =   BLAC_Environment_Init;         % Environment constants
	BLAC.Actuator       =   BLAC_Actuator_Init;            % Actuator data
    BLAC.Tail_Bumper    =   BLAC_Tail_Bumper_Init;         % Tail Bumper chracteristics
	BLAC.Sim            =   BLAC_Sim_Init;                 % Simulation init
	BLAC.NavData        =   BLAC_NavData_Init;             % Navigation Data VOR/DME Init

	BLAC.Sim.Trim_Flag  = 0;                               % Set [1] for Trimming                   [-]
														   % Set [0] for Simulation                 [-] 
	if(isempty(TrimSettings.strTrimfile) == 1)
		% Ask for filename to load Trimm Values
		[FileName,PathName] = uigetfile('*.mat', 'Load Trim-Parameter File');
		TrimSettings.strTrimfile = strcat(PathName, FileName);
	end
	
	if(isempty(TrimSettings.strTrimfile) == 1)
		disp('Warnig: No file specified. Nothing loaded!');
    else
        [BLAC_Trim,X0,U0,tY,TrimPointInfo]=load_TrimSettings_pr(TrimSettings.strTrimfile);
       % load(TrimSettings.strTrimfile, '-mat');
	end

	% assignin('base','BLAC', BLAC);
	% assignin('base','BLAC_Trim', BLAC_Trim);                                   
end

%---------------------------------------------


end

