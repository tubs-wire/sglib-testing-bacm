% analysis_robustness1.m
%
% für 1000 Simus

% Verzeichnisstruktur: graphics/trim1, ...

% Todo: print rein als png
%       Trim Info ... for Schleifen KOnstrukt aus my_trim
%       Folien machen: 



Elements = [10 21 16 14 17 15 26:28 32:34];
%Elements = 10;

cd Results

List_V     = [40;45;50;55];
List_Alt   = [0;305;914];
List_gamma = [1;0;-3;-6];

tfp = fopen('../tex/result_trim.tex', 'w');
trim = 0;
for i1=1:length(List_Alt)
for i2=1:length(List_V)
for i3=1:length(List_gamma)
  trim = trim + 1;
  fprintf(tfp, '\\input tex/result_trim%d.tex\n', trim);
end
end
end
fclose(tfp)
  

trim = 0;
for i1=1:length(List_Alt)
for i2=1:length(List_V)
for i3=1:length(List_gamma)
  trim = trim + 1;
  my_path = sprintf('Trim%d', trim);
  cd(my_path)

  filename_tex = sprintf('../../tex/result_trim%d.tex', trim);
  tfp = fopen(filename_tex, 'w');
  fprintf(tfp, '\\section{Testcase Nr %d}\n\n', trim);

  fprintf(tfp, '\\frame{\n');
  fprintf(tfp, '\\frametitle{Testcase %d}\n', trim);
  fprintf(tfp, '%%\n');
  fprintf(tfp, '\\begin{itemize}\n');
  fprintf(tfp, '\\item\n');
  fprintf(tfp, 'Alt = %f\n', List_Alt(i1));
  fprintf(tfp, '\\item\n');
  fprintf(tfp, 'V = %f\n', List_V(i2));
  fprintf(tfp, '\\item\n');
  fprintf(tfp, 'gamma = %f\n', List_gamma(i3));
  fprintf(tfp, '\\end{itemize}\n');
  fprintf(tfp, '}\n\n');
  
  for kk=1:length(Elements)
    k = Elements(kk);
    for i=1:140
      filename = sprintf('Sim_Results_HF2_%d_Trim%d.mat', i, trim);
      if (exist(filename, 'file')==2)
         x = load(filename);
         plot(x.Result.Case_1{9}.Value, x.Result.Case_1{k}.Value, 'b', 'LineWidth', 2)
         hold on
      end
    end
    filename = sprintf('Sim_Results_HF2_1_Trim%d.mat', trim);
    plot(x.Result.Case_1{9}.Value, x.Result.Case_1{k}.Value, 'r', 'LineWidth', 2)
    hold off
    xlabelname = x.Result.Case_1{9}.Label;
    ylabelname = x.Result.Case_1{k}.Label;
    xlabel(['$$ ' xlabelname ' $$'],'Interpreter','latex')
    ylabel(['$$ ' ylabelname ' $$'],'Interpreter','latex')
    filename_print = sprintf('../../graphics/result_trim%d_value%d.png', trim, kk);
    print('-dpng',filename_print);
    
    fprintf(tfp, '\\frame{\n');
    fprintf(tfp, '\\frametitle{$%s$}\n', x.Result.Case_1{k}.Label);
    fprintf(tfp, '%%\n');
    fprintf(tfp, '\\centerline{\\includegraphics[width=0.8\\textwidth]{graphics/result_trim%d_value%d.png}}\n', trim, kk);
    fprintf(tfp, '%%\n');
    fprintf(tfp, '}\n');
  end
  fclose(tfp);
  cd ..
end
end
end
cd ..


