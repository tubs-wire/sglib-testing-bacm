%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                       BLAC_Test_Plot                                               *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Script                                                                                  *
%*                                                                                                    *
%* Purpose  : Script for plotting the reaction of the aircraft modell to different input signals on   *
%*            the control surfaces.                                                                   *
%* Version  : 1.2                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : The file plotfunc.m is necessary for execution.                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Test_Plot
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 27-OCT-09 * Basic Design                                            *
%* Jobst Henning Diekmann       * 02-FEB-12 * Integration of BLAC Model structure                   *
%* Jobst Henning Diekmann       * 30-JUL-12 * Integration of Feedback Controller Input Values         *
%* Jobst Henning Diekmann       * 30-OKT-13 * Sliced Simulation including state vector manipulation   *
%* Jobst Henning Diekmann       * 15-APR-14 * Simulation MK I Test Plotting Function                  *
%******************************************************************************************************
%% Simulation Initialization
% BLAC_Environment_Init.m
% Clear Workspace and initialize simulation variable structure in workspace
clear all
close all
clc

FilePath    = mfilename('fullpath');
FolderName  = '\REF-2 Uncertain Freeze 12-12-2016\';
PathIndex   = strfind(FilePath,FolderName);

BasePath = FilePath(1:PathIndex-1);

% Go to root directory
cd([fileparts(fileparts(FilePath)) '\Tools']);

addpath(genpath([BasePath FolderName]))

% Result directory
resultpath = [BasePath FolderName 'Tools\Results\'];
if ~exist(resultpath,'dir')
    mkdir(resultpath);
end

Controller_active_run   = [1 1]; % Run 1: Controllers active, Run 2: Controllers active
Uncertainty_Killswitch  = [0 1]; % Run 1: No uncertainties, Run 2: Uncertainties activated

for run = 1:2
clear BLAC BLAC_Trim
    
TrimSettings.SimMode = 1;   % Sim Mode = 1 --> Dynamic Simulation Mode, Sim Mode = 2 --> Trim Mode

TrimSettings.strTrimfile = 'Alt1000_V50_FPA0_Bank0_HF_12122016124608.mat'; % Link Trimfiles here

BLAC_Simulation_Init;

%% Set Simulation Parameters

BLAC.Sim.StartTime  =  0.0;                            % Simulation Start Time                       [s]
if run == 1
    BLAC.Sim.EndTime    =  30;                         % Simulation End Time (330 at 780m)          [s]
else
    BLAC.Sim.EndTime    =  Result.Round1.(Case_Name){9}.Value(end); % Cut Simulation Time, if first run did not reach the default simulation end time
end
BLAC.Sim.SampleTime =  0.01;                           % Simulation Sample Time                      [s]


% Set Test Parameters
Test_Parameters.Max_Data    = 100000;                  % Maximum of recorded data points             [-]

Test_Parameters.Input_Type  = 0;                       % No input in the follwing time slices        [-]
%*                                                     % Control Input Signal Types:
%*                                                     % [0] - No Signal
%*                                                     % [1] - Step
%*                                                     % [2] - Impulse
%*                                                     % [3] - Doublet
%*                                                     % [4] - Sinus-Wave

%* Step Signal Parameters for Elevator
Test_Parameters.Step_Start          = 5;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial        = TrimPointInfo.Throttle/(100*2);   % Start Time of Step Input                    [s]
Test_Parameters.Step_Size           = .10;                              % Step Size                                 [deg]

%* Step Signal Parameters for RH OEI
Test_Parameters.Step_Start_OEI_RH   = 7;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial_OEI_RH = 1;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Size_OEI_RH    = 1;                                % Step Size                                 [deg]

%* Step Signal Parameters for LH OEI
Test_Parameters.Step_Start_OEI_LH   = 5;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Initial_OEI_LH = 1;                                % Start Time of Step Input                    [s]
Test_Parameters.Step_Size_OEI_LH    = 1;                                % Step Size                                 [deg]

%* Impulse Signal Paramters
Test_Parameters.Impulse_Start   = 1;                                    % Start Time of Impulse Input                 [s]
Test_Parameters.Impulse_End     = 2;                                    % Stop Time of Impulse Input                  [s]
Test_Parameters.Impulse_Size    = 3;                                    % Impulse Size                              [deg]
% 
%* Doublet Signal Parameters
Test_Parameters.Doublet_Start   = 1;                                    % Start Time of Doublet Input                 [s]
Test_Parameters.Doublet_End     = 3;                                    % Stop Time of Doublet Input                  [s]
Test_Parameters.Doublet_Size    = 20;                                   % Doublet Size                              [deg]
% 
%* Sinus Wave Parameters
Test_Parameters.Sinus_Freqency  = 1;                                    % Frequency of Sinus Wave Input              [Hz]
Test_Parameters.Sinus_Start     = 1;                                    % Start Time of Sinus Wave Input              [s]
%

%* Selection of Control Surface
Test_Parameters.Aileron_Gain    = 0;                                    % Select [1] Aileron as Test Surface.         [-]
Test_Parameters.Elevator_Gain   = 0;                                    % Select [1] Elevator as Test Surface.        [-]
Test_Parameters.Rudder_Gain     = 0;                                    % Select [1] Rudder as Test Surface.          [-]
Test_Parameters.Stabilizer_Gain = 0;                                    % Select [1] Stabilizer as Test Surface.      [-]
Test_Parameters.Throttle_Gain   = 0;                                    % Select [1] Throttle as Test Input Signal.   [-]
Test_Parameters.Compressor_Gain = 0;

% One Engine Inoperative
Test_Parameters.OneEngineInoperative        = BLAC.Propulsion.OEI;      % One Engine Inoperative [0 No / 1 Yes]
Test_Parameters.BlowingOperative            = BLAC.Propulsion.BLCOP;    % Blowing Operative [0 Yes / 1 No]

%% Uncertainties
Uncertainty

%% Simulation Configuration Parameters for additional model functions 
% Set Cmu Parameters
BLAC.Aerodynamics.Prop.On                   = TrimPointInfo.SetSlipstream;
BLAC.Aerodynamics.Active_HTP.On             = TrimPointInfo.SetHTPactive;
BLAC.Aerodynamics.Active_HTP.Performance    = 100;                          % Performance in [%]
BLAC.Aerodynamics.Cmu.Cmu_Var_Flg           = 0;
BLAC.Aerodynamics.Cmu.Cmu_Const             = TrimPointInfo.C_mu/2;
BLAC.Aerodynamics.Cmu.Comp_Vec              = TrimPointInfo.SetCmuDistrib;
BLAC.Aerodynamics.Droop.Active              = TrimPointInfo.SetDroop;
BLAC.Aerodynamics.Inputs.Lat_Motion         = TrimPointInfo.SetLateral;
BLAC.Propulsion.Prop_Efficiency             = 1;
BLAC.Aerodynamics.Lift.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Drag.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Pitch.CrossCouplFlag      = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Roll.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Yaw.CrossCouplFlag        = TrimPointInfo.AeroCrossCoupl;
BLAC.Aerodynamics.Side.CrossCouplFlag       = TrimPointInfo.AeroCrossCoupl;
BLAC.Propulsion.Mode                        = TrimPointInfo.Engine_Mode;

% Switch Slipstream Effects on/off
BLAC.Aerodynamics.Slipstream.On             = 1;

% Blowing System Failure
% Test_Parameters.ILS_GS                      = -3.5 * pi/180;          
% Test_Parameters.V_Approach                  = 0.15 * 340.294;
BLAC.Aerodynamics.Fail.Deactivated          = 0;                        % Throttle Step Final Value for AHLS Failure 
BLAC.Aerodynamics.Fail.Time                 = BLAC.Sim.EndTime;         % Time of AHLS Failure 
BLAC.Aerodynamics.Fail.Time_Control         = BLAC.Sim.EndTime;         % Time of Controller Initialization
BLAC.Aerodynamics.Cmu.Cmu_Fail              = TrimPointInfo.C_mu/2;     % Jet Momentum Coeff after AHLS Failure
BLAC.Environment.Wind.Turbulence_Flag       = 0;
BLAC.Environment.Wind.Turbulence_Intensity_Index = 1;
BLAC.Environment.Terrain_Elevation          = -50;

%% Aileron Efficiency
BLAC.Aerodynamics.Roll.C_lksi(3)            = -0.24; %BLAC.Aerodynamics.Roll.C_lksi(3) * 2.5; % Apply different C_lxi Aileron efficiencies here 
                                                                                % Default Model Value:
                                                                                % BLAC.Aerodynamics.Roll.C_lksi(3) =
                                                                                % 0.09
                                                                                % C_lxi(ATTAS) = 0.24
Actuator_Rate = 30; 
                                                                                
BLAC.Actuator.Surfaces.Rudder.max           =  30 * pi/180;      % Min. Rudder deflection angle                     [rad]
BLAC.Actuator.Surfaces.Rudder.min           = -30 * pi/180;      % Min. Rudder deflection angle                     [rad]
BLAC.Actuator.Surfaces.Rudder.rate_max      =  Actuator_Rate * pi/180;      % Max. rate of Rudder deflection                 [rad/s]
BLAC.Actuator.Surfaces.Rudder.rate_min      = -Actuator_Rate * pi/180;      % Min. rate of Rudder deflection                 [rad/s]

BLAC.Actuator.Surfaces.Elevator.max         =  30 * pi/180;      % Min. Elevator deflection angle                     [rad]
BLAC.Actuator.Surfaces.Elevator.min         = -30 * pi/180;      % Min. Elevator deflection angle                     [rad]
BLAC.Actuator.Surfaces.Elevator.rate_max    =  Actuator_Rate * pi/180;      % Max. rate of Elevator deflection                 [rad/s]
BLAC.Actuator.Surfaces.Elevator.rate_min    = -Actuator_Rate * pi/180;      % Min. rate of Elevator deflection                 [rad/s]
BLAC.Actuator.Booster.Elevator.omega_0      = 60;   

BLAC.Actuator.Surfaces.Aileron.max          =  30 * pi/180;      % Min. Aileron deflection angle                     [rad]
BLAC.Actuator.Surfaces.Aileron.min          = -30 * pi/180;      % Min. Aileron deflection angle                     [rad]
BLAC.Actuator.Surfaces.Aileron.rate_max     =  Actuator_Rate * pi/180;      % Max. rate of Aileron deflection                 [rad/s]
BLAC.Actuator.Surfaces.Aileron.rate_min     = -Actuator_Rate * pi/180;      % Min. rate of Aileron deflection                 [rad/s]

%% Crosswind
BLAC.Environment.Wind.v_w_active            = 0;                        % Activation of Crosswind Components
BLAC.Environment.Wind.u_Wreq                = 0;                % Change Crosswind Component Value here, 5.144 [m/s] = 10kts
BLAC.Environment.Wind.psi_Wreq              = 90 * pi/180;              % Wind Direction 
BLAC.Environment.Wind.u_W_Step              = 0; %8;
BLAC.Environment.Wind.u_W_Step_Time         = 5;
BLAC.Environment.Wind.v_W_Step              = 5.144 * 2;
BLAC.Environment.Wind.v_W_Step_Time         = 2;
BLAC.Environment.Wind.w_W_Step              = 0; %4;
BLAC.Environment.Wind.w_W_Step_Time         = 10;

%% Controller Gains
BLAC.Controller.Activated = Controller_active_run(run);                          % Major Activation Switch for all Controllers

% Basic Rate Controllers
% Roll Damper
BLAC.Controller.Aileron_pControl_P      =  -5.0 * 1 * BLAC.Controller.Activated; % K_P,p
% Pitch Damper
BLAC.Controller.Elevator_qControl_P     =   3.0 * 1 * BLAC.Controller.Activated; % K_P,q
% Yaw Damper
BLAC.Controller.Rudder_rControl_P       =   2.5 * 1 * BLAC.Controller.Activated; % K_P,r

% Dynamic Inversion
BLAC.Controller.Cnbeta_Desired          =   1.0;                                    % C_nbeta,Cmd      Example: Cnbeta(ATTAS)= 0.4
BLAC.Controller.Rudder_betInvContr_P    =   1.0 * 1 * BLAC.Controller.Activated;            % K_Cnbeta
BLAC.Controller.Rudder_betInvContr_I    =  -1.5 * 1 * BLAC.Controller.Activated;            % K_beta_I

BLAC.Controller.Elevator_betInvContr_P  =   1.0 * 1 * BLAC.Controller.Activated;            % K_Cmbeta
BLAC.Controller.Elevator_LifInvContr_P  =  -1.0 * 0 * BLAC.Controller.Activated;            % K_CLbeta
BLAC.Controller.Elevator_LifInvDelayOn  =         1 * BLAC.Controller.Activated;            % Delay CLbeta

BLAC.Controller.Aileron_betInvContr_P   =   1.0 * 0 * BLAC.Controller.Activated;            % K_Clbeta

% Input Uncertainties
BLAC.Controller.Thrust_Uncertainty      =   1 + ( 0.10 * Uncertainty_Killswitch(run));  % Thrust Uncertainty
BLAC.Controller.alpha_Uncertainty       =   1 + ( 1 * Uncertainty_Killswitch(run));  % alpha Uncertainty
BLAC.Controller.beta_Uncertainty        =   1 + ( 0.00 * Uncertainty_Killswitch(run));  % beta Uncertainty

% Bank Angle Controller
BLAC.Controller.Aileron_PhiContr_P      =  -2  * 1 * BLAC.Controller.Activated;           % K_P,Phi Best Value: -1

% Controller Delay
BLAC.Actuator.Booster.Elevator.Delay    =   0.09; %.2; % Std Val: 0.09 s  
BLAC.Actuator.Booster.Aileron.Delay     =   0.09; %.2; % Std Val: 0.09 s   
BLAC.Actuator.Booster.Rudder.Delay      =   0.09; %.2; % Std Val: 0.09 s   

BLAC.Controller.Elevator_omega          =  (BLAC.Actuator.Booster.Elevator.omega_0) ...
                                        *  sqrt(1-(BLAC.Actuator.Booster.Elevator.Damping)^2);  % Not relevant
BLAC.Controller.Elevator_T              =   2*pi/BLAC.Controller.Elevator_omega;                 % Not relevant
BLAC.Controller.dt_Elevator_Predict     = -(BLAC.Actuator.Booster.Elevator.Delay...
                                        +   0.25 * BLAC.Controller.Elevator_T);                  % Not relevant

% Pitch Attitude Controller
BLAC.Controller.Elevator_ThetaContr_P   =   1.0 * 0 * BLAC.Controller.Activated;            % Not relevant
BLAC.Controller.Theta_Cmd_Type          =   1;
BLAC.Controller.Theta_Cmd_T_step        =   500;
BLAC.Controller.Theta_Cmd               =   BLAC_Trim.Init.Y0(4);
BLAC.Controller.Theta_Cmd_Freq          =   1/4 * 2/pi;
BLAC.Controller.Theta_Cmd_Amp           =   1;

% Counteract Coupling Effects from Control Surfaces
BLAC.Controller.Zeta2XiCoupling         =   1;                                      % Not relevant              

% Phi Command
BLAC.Controller.t_Test_Start            =   0;                                      % Not relevant
BLAC.Controller.Phi_Test_Target         =   0 * pi/180;                             % Not relevant

% Phase Compensated Rate Limiters
BLAC.Controller.Aileron.rate_max        =  30 * pi/180; % Not relevant
BLAC.Controller.Aileron.rate_min        = -30 * pi/180; % Not relevant
BLAC.Controller.Rudder.rate_max         =  30 * pi/180; % Not relevant
BLAC.Controller.Rudder.rate_min         = -30 * pi/180; % Not relevant
BLAC.Controller.Elevator.rate_max       =  30 * pi/180; % Not relevant
BLAC.Controller.Elevator.rate_min       = -30 * pi/180; % Not relevant

BLAC.Controller.RateLimiterBypass       =   0;          % Not relevant

BLAC.Controller.beta_Init               = BLAC_Trim.Init.Y0(6);
BLAC.Controller.theta_Init              = BLAC_Trim.Init.Y0(4);
BLAC.Controller.phi_Init                = BLAC_Trim.Init.Y0(7);
BLAC.Controller.p_Init                  = BLAC_Trim.Init.Y0(9);
BLAC.Controller.q_Init                  = BLAC_Trim.Init.Y0(3);
BLAC.Controller.r_Init                  = BLAC_Trim.Init.Y0(10);

% Controller Initialization Time
BLAC.Controller.Time_Control            = 500;      % Not used
BLAC.Controller.Control                 = 0;        % Not Used

%% Run Simulation Model
try
    sim('BLAC_Test_Quat');
catch ME
    display('Fail Message:')
    display(ME.message)
    ME.stack   
end
% First Check
SeeAllInfluences = 0;
if SeeAllInfluences
%     close all

    color_vec = [0 0 0 ; 0 0 1 ; 0 1 0 ; 1 0 0 ; 0 1 1 ; 1 0 1 ; 1 1 0 ; 0.99 0.40 0];

    figure(14)
    set(gcf,'DefaultAxesColorOrder',color_vec)
    plot(   YawingMoment(:,1), YawingMoment(:,[2 4 5 6 7 8 12 13]), 'LineWidth',3), grid on
        xlabel('t [s]')
        ylabel('C_n [-]')
        legend( 'C_{n25}',...
                'C_{n\beta}',...
                'C_{np}',...
                'C_{nr}',...
                'C_{n\xi}',...
                'C_{n\zeta}',...
                'C_{ndT}',...
                'C_{nProp}',...
                'Location','NorthWest')

    figure(15)
    set(gcf,'DefaultAxesColorOrder',color_vec)
    plot(   RollingMoment(:,1), RollingMoment(:,[2 4 5 6 7 8 9 10]), 'LineWidth',3), grid on
        xlabel('t [s]')
        ylabel('C_l [-]')
        legend( 'C_{l25}',...
                'C_{l\beta}',...
                'C_{lp}',...
                'C_{lr}',...
                'C_{l\xi}',...
                'C_{l\zeta}',...
                'C_{ldT}',...
                'C_{lProp}',...
                'Location','NorthWest')

    figure(16)
    set(gcf,'DefaultAxesColorOrder',color_vec)
    plot(   PitchingMoment(:,1), PitchingMoment(:,[15 2 6 7 8 10]), 'LineWidth',3), grid on
        xlabel('t [s]')
        ylabel('C_m [-]')
        legend( 'C_{m25}',...
                'C_{mHTP}',...
                'C_{ma}',...
                'C_{mqFR}',...
                'C_{mProp}',...
                'C_{m\beta}',...
                'Location','NorthWest')

    figure(17)
    set(gcf,'DefaultAxesColorOrder',color_vec)
    plot(   LiftForce(:,1), LiftForce(:,[2 4 7 9 10 13 14 15]), 'LineWidth',3), grid on
        xlabel('t [s]')
        ylabel('C_L [-]')
        legend( 'C_{L}',...
                'C_{LHTPges}',...
                'C_{La}',...
                'C_{LX}',...
                'C_{LT}',...
                'C_{L\eta}',...
                'C_{LHTP}',...
                'C_{L\beta}',...
                'Location','NorthWest')

    clear color_vec
end
%% Result Struct

Trial_Name = ['Round' num2str(run)];                % Random Name Assignment
Case_Name   = 'Case_1';     % Random Struct Identifier
Save_Name   = 'HF1';        % Random Struct Identifier HF = Horizontal Flight
TestCase    = 1;
savename    = [resultpath 'Sim_Results_' Save_Name '.mat'];

Result.(Trial_Name).(Case_Name) = Result_Struct;
save(savename, 'Result')

t_end = Result.(Trial_Name).(Case_Name){9}.Value(end);

Plots = [   {'Time_s'};...
            {'Alt_m'};...
            {'V_TAS_mps'};...
            {'gamma_deg'};...
            {'alpha_deg'};...
            {'alpha_H_deg'};...
            {'beta_deg'};...
            {'p_degps'};...
            {'q_degps'};...
            {'r_degps'};...
            {'phi_deg'};...
            {'theta_deg'};...
            {'psi_deg'};...
            {'n_x'};...
            {'n_y'};...
            {'n_z'};...
            {'Act_Aileron_LH_deg'};...
            {'Act_Elevator_deg'};...
            {'Act_Rudder_deg'}];

    SizePlots = size(Plots);
    Plot_Vec = [];

    for i = 1 : SizePlots(1,1)
        for j = 1: 112
            if strcmp(Result.(Trial_Name).(Case_Name){j}.Name,Plots(i))
                Plot_Vec = [Plot_Vec j];
                Result.(Trial_Name).(Case_Name){Plot_Vec(end)}.Name;
            end
        end
    end

    color_vec = ['k';'r';'b';'g';'r';'m'];
    
    if run ~=1
        figure_established = 1;
    else
        if ~SeeAllInfluences
                 close all
        end
        figure_established = 0;
        Ax = [];
    end
    z = run;                 % Color Code
    figure(1)
    fout = [BasePath FolderName 'Tools\Plots\' Trial_Name];
    if ~exist([BasePath  FolderName 'Tools\Plots\'],'dir')
        mkdir([BasePath  FolderName 'Tools\Plots\']);
    end

    Ax = Multi_Plot_SE(Result,Trial_Name,Case_Name,t_end, color_vec,z, Plot_Vec, figure_established, Ax, BLAC.Controller.Activated, BasePath, FolderName);
    print(gcf, '-depsc2', fout)
    
    beta  = Result.(Trial_Name).(Case_Name){15}.Value;
    dbeta = diff(Result.(Trial_Name).(Case_Name){15}.Value);
    
    ik=1;
    for ibeta = length(dbeta) : -1 : 2
        if dbeta(ibeta) > 0 && dbeta(ibeta-1) < 0 || dbeta(ibeta) < 0 && dbeta(ibeta-1) > 0
            i_beta_Peak(ik) = ibeta;
            Peaks(ik) = beta(ibeta);
            T_beta_Peak(ik) = Result.(Trial_Name).(Case_Name){9}.Value(ibeta);
            ik = ik +1;
        end
    end
    Sweep_Peaks     = (find(abs(Peaks)>0.01));
    Peaks           = Peaks(Sweep_Peaks(end:-1:1)); % Sweep small peaks
    T_beta_Peak     = T_beta_Peak(Sweep_Peaks(end:-1:1));
    i_beta_Peak     = i_beta_Peak(Sweep_Peaks(end:-1:1));
    T               = mean(diff(T_beta_Peak))*2;
    omega           = (2*pi)/T;
    log_decrement   = log(Peaks(2)/Peaks(4));
    sigma           = -log_decrement/T;
    omega_0         = sqrt(sigma^2+omega^2);
    D               = -sigma/omega_0; 
    D_omega         = omega * D;
    
    if omega > 0.4
        L_Omega = 'Level 1';
    else
        L_Omega = 'Level 3';
    end
    
    if D >= 0.08   
        L_D = 'Level 1';
    elseif  D >= 0.02 && D < 0.08
        L_D = 'Level 2';
    else D < 0.02
        L_D = 'Level 3';
    end
        
    if D_omega >= 0.1
        L_Domega = 'Level 1';
    elseif  D_omega >= 0.05 && D_omega < 0.1
        L_Domega = 'Level 2';
    else
        L_Domega = 'Level 3';
    end
        
    
    disp( '--------------------------------------------------------------------');
    disp(['omega_TS  : ' num2str(omega,     '%11.4f')   ' rad/s --> ' L_Omega   '       L1: > 0.4' ]);
    disp(['omega0_TS : ' num2str(omega_0,   '%11.4f')   ' rad/s'           ]);
    disp(['Damping_TS: ' num2str(D,         '%11.4f')   '       --> ' L_D       '       L1: > 0.08, L2> 0.02']);
    disp(['omega_0*D : ' num2str(D_omega,   '%11.4f')   '       --> ' L_Domega  '       L1: > 0.10, L2> 0.05' ]);
    disp( '--------------------------------------------------------------------');
    
    test = 1;
%     Plots2 =[{'beta_deg'};...
%             {'C_Y_e'};...
%             {'C_l25_e'};...
%             {'C_n25_e'}...
%             ];
% 
%     disp('Check Plots for:');
%     disp(Plots2);
% 
%     SizePlots2 = size(Plots2);
%     Plot_Vec2 = [];
% 
%     for i = 1 : SizePlots2(1,1)
%         for j = 1: 104
%             if strcmp(Result.(Trial_Name).(Case_Name){j}.Name,Plots2(i))
%                 Plot_Vec2 = [Plot_Vec2 j];
%                 Result.(Trial_Name).(Case_Name){Plot_Vec2(end)}.Name;
%             end
%         end
%     end
% 
%     color_vec = ['k';'r';'b';'g';'r';'m'];
% %     color_vec = [{[0 0 0]} ; {[0 0 1]} ; {[0 1 0]} ; {[1 0 0]} ; {[0 1 1]} ; {[1 0 1]} ; {[1 1 0]} ; {[0.99 0.40 0]}];
%     
%     if run ~=1
%         figure_established = 1;
%     else
%         figure_established = 0;
%         Ax2 = [];
%     end
%     z = run;                 % Color Code
%     figure(2)
%     Ax2 = Multi_Plot_beta(Result,Trial_Name,Case_Name,t_end, color_vec,z, Plot_Vec2, figure_established, Ax2, BLAC.Controller.Activated);
% %     fout = [pwd '\Plots\' Trial_Name];
% %     print(gcf, '-depsc2', fout)
%     
%     beta_art = -30 : 30;
%     Cn_Cmd   = beta_art * (pi/180) * Test_Parameters.Cnbeta_Desired ;
%     
%     omega_0_Level1  = 0.4;
%     
%     Ix  = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_xx;       % Inertia xx-component at about defined CG.[kgm^2]
%     Iz  = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_zz;       % Inertia xx-component at about defined CG.[kgm^2]
%     Izx = BLAC_Trim.Weight_Balance.AC_Empty.Inertia.I_xz;       % Inertia xz-component at about defined CG.[kgm^2]
% 
%     Clbeta = diff(Result.(Trial_Name).(Case_Name){103}.Value) ./ diff(Result.(Trial_Name).(Case_Name){15}.Value.*(pi/180));
%     Cnbeta = diff(Result.(Trial_Name).(Case_Name){104}.Value) ./ diff(Result.(Trial_Name).(Case_Name){15}.Value);
%     X = Result.(Trial_Name).(Case_Name){109}.Value(1:end-1) * BLAC.Configuration.Geometry.S * BLAC.Configuration.Geometry.s/(Ix*Iz-Izx^2);
%     Cnb_Level1      = (omega_0_Level1^2 - Izx .* Clbeta .* X) ./ (Ix * X);
% %     Cnb_Level1      = (omega_0_Level1^2) ./ (X);
%     beta = Result.(Trial_Name).(Case_Name){15}.Value;
%     figure(4)
%     plot(beta(1:end-1), Cnb_Level1,'k',beta(1:end-1), Cnbeta, 'r')
%     
%     LineSize    = 1.2;
%     MarkerSize  = 5;
%     FontSize    = 11;
%     LegendSize  = FontSize-3;
% 
%     figure(3)
%     set( gcf ,  'Color','w', ...
%             'DefaultLineLineWidth',LineSize ,...
%             'DefaultLineMarkerSize',MarkerSize,...
%             'DefaultAxesFontName','times',...
%             'DefaultAxesFontSize',FontSize,...
%             'DefaultTextInterpreter','latex')
% 
% 
%     px  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){105}.Value + Result.(Trial_Name).(Case_Name){108}.Value,color_vec(run+3));
%     grid on
%     hold on
%     px1  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){108}.Value,'k-');
%     px3  = plot(Result.(Trial_Name).(Case_Name){15}.Value, Result.(Trial_Name).(Case_Name){105}.Value,'m-');
%     px2 = plot(beta_art, Cn_Cmd,'k--');

end


% ---------------------------------------------------------------------------------------------------EOF