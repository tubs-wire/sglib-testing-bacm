%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2010      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                         BLAC_Linearize                                             *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Linearize the BLAC aircraft model.                                                      *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%* Sub-Functions :                                                                                    *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax :  BLAC_Linearize.m                                                                         *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date     *                     Description                       *
%******************************************************************************************************
%* Dominik Niedermeier          * 25-FEB-10 * Adapted to new trim / lin BLAC workbench                *
%* �zg�r Atesoglu               * 26-APR-10 * State trans. for quat. -> Euler angles and              *
%*                                            state trans. for body  -> wind frame vel. are added.    *
%*                                            The state, output and input orders are revised.         *    
%* �zg�r Atesoglu               * 10-MAY-10 * Incremental body acceleration outputs (ay,az) are added *
%*                                            to the model and corresponding state, output and        *
%*                                            input orders are revised.                               * 
%* Christian Raab               * 04-JUN-10 * Adapted linearization routine to new output vector      *
%* �zg�r Atesoglu               * 31-JAN-11 * Open loop plant simulations and plots are arranged      *
%* Dominik Niedermeier          * 07-FEB-11 * Actuator states are added to the A-Matrix               *
%*                                            Evalin-Command for setting of trim flag before          *
%*                                            simulation is added                                     * 
%******************************************************************************************************

function [A B C D] = BLAC_Linearize(BLAC,BLAC_Trim,TrimSettings)

% Define the trim point
x_tr=BLAC_Trim.Init.X0;
u_tr=BLAC_Trim.Init.U0;
y_tr=BLAC_Trim.Init.Y0;

% Name definitions
X_Name=BLAC_Trim.Names.X_Name;
U_Name=BLAC_Trim.Names.U_Name;
Y_Name=BLAC_Trim.Names.Y_Name;

%******************************************************************************************************
% Linearization
%******************************************************************************************************
% % Precompile the model
% feval(TrimSettings.MdlName, [], [], [],'compile');

%Assembly of x and u at trim point
x_u = [x_tr; u_tr];

%Index definition of the states and inputs used to linearise the system
%The matrix Jaco contains the subsystems defined by the index i_d and i_y
n_x = length(x_tr);                % Number of states
n_u = length(u_tr);                % Number of inputs         
n_y = length(y_tr);                % Number of outputs
i_x = [1:n_x]';                    % Identify States (Column Vector)
i_u = [1:n_u]';                    % Identify Inputs (Column Vector)
itv = [i_x; i_u+n_x];              % Assembly of index vector for states and inputs
i_d = [1:n_x]';                    % Identify Derivatives
i_y = [1:n_y]';                    % Identify outputs
itr = [i_d;i_y+n_x];               % Assembly of index vector for derivatives and outputs                 
n_u = size(i_u,1);                 % All inputs selected 
n_y = size(i_y,1);                 % All outputs selected ("one" means number of columns)
n_x = size(i_x,1);                 % All states selected

% feval (TrimSettings.MdlName, [], [], [], 'compile');

%Calling jj_lin
jaco = jj_lin(TrimSettings.MdlName,x_u,n_x,itv,itr);
% 
% feval (TrimSettings.MdlName, [], [], [], 'term');
% "jaco" matrix is defined as

        % | xdot| | A | B | | x |
        % |     |=|---|---|*|   |
        % | y   | | C | D | | u |

% Try several times to release the model
try
    feval(TrimSettings.MdlName,[],[],[],'term');
    feval(TrimSettings.MdlName,[],[],[],'term');
    feval(TrimSettings.MdlName,[],[],[],'term');
catch
    
end

% Extract the matrices of the linear system
% Extract A-matrix
for i=1:n_x
    for j=1:n_x
        A(i,j)=jaco(i,j);
    end
end

% Extract B-matrix
for i=1:n_x
    for j=n_x+1:n_x+n_u
        B(i,j-n_x)=jaco(i,j);
    end
end

% Extract C-matrix
for i=n_x+1:n_x+n_y
    for j=1:n_x
        C(i-n_x,j)=jaco(i,j);
    end
end

% Extract D-matrix
for i=n_x+1:n_x+n_y
    for j=n_x+1:n_x+n_u
        D(i-n_x,j-n_x)=jaco(i,j);
    end
end

end
%---------------------------------------------------------------------------------------------------EOF