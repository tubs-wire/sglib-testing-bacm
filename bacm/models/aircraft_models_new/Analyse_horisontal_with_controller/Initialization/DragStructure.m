function Drag = DragStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, ModelResult, Longitudinal_Parameters)
% 
% 
%% Uncertainty Matrix
Drag.T_Uncert        = ModelResult.DragModel.SubModel_Wing.T_Vec(1:3);
Drag.beta_Uncert     = [0 5 10]./180*pi;
Drag.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
Drag.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
Drag.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
Drag.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
Drag.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
Drag.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
Drag.Uncertainty_LT = [ 1                   1                   1                   ;...
                        Drag.Uncert_b05T0   Drag.Uncert_b05T1   Drag.Uncert_b05T2   ;...
                        Drag.Uncert_b10T0   Drag.Uncert_b10T1   Drag.Uncert_b10T2   ];

%% Derivatives
Drag.C_D0           =   AeroCoefficientsNL.CD0;
Drag.C_D0_HTP       =   AeroCoefficientsNL.CD0_HTP;
Drag.k1             =   AeroCoefficientsNL.k1;
Drag.k2             =   AeroCoefficientsNL.k2;
Drag.dC_DdFL        =  [0   MidFlap.dCD0                Longitudinal_Parameters.T0.Cmu03.CD.CD0 ];        
Drag.dC_Dk2dFL      =  [0   MidFlap.dCk2                Longitudinal_Parameters.T0.Cmu03.CD.k	];        
Drag.dC_Dk1dFL      =  [0  -AeroCoefficientsNL.k1      	0];        
Drag.dC_DdCmuBLC    =  [0   0                           AeroCoefficientsNL.BLC.dCD_dCmu];        
Drag.dC_DdCmuCC     =  [0   0                           AeroCoefficientsNL.CC.dCD_Cmu];        
Drag.dC_Dk2dCmuBLC  =  [0   0                           AeroCoefficientsNL.BLC.dk_dCmu];        
Drag.dC_Dk2dCmuCC   =  [0   0                           AeroCoefficientsNL.CC.dk_dCmu];
% Drag.dC_D0_DN       =   DroopNose.dCD_DN - 0.03;                                   
% Drag.dC_Dk1_DN      =   DroopNose.dk1_DN;                                   
% Drag.dC_Dk2_DN      =   DroopNose.dk2_DN;                                   
Drag.dC_DdRD        =   0;                                   
Drag.dC_DdSB        =  [0 0 0];                       
Drag.dC_DdLG        =   0;          
Drag.dC_D_dh        =  [0 0 0];

Drag.dC_DdCmu_30    =  [0 0 0];
Drag.dk_CDdCmu_30   =  [0 0 0];
Drag.dC_DdCmu_65    =  Longitudinal_Parameters.LT_0_CD_Cmu;
Drag.dk_CDdCmu_65   =  Longitudinal_Parameters.LT_k_CD_Cmu;

Drag.dC_DdT         = Longitudinal_Parameters.LT_0_CD_T(1:3);
Drag.dk_CDdT        = Longitudinal_Parameters.LT_k_CD_T(1:3);
Drag.T_Vec          = Longitudinal_Parameters.T_Vec(1:3);

% Lateral Motion Influences on Drag %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Drag.C_Dab          =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDab ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDab ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDab];
% Drag.f_C_Dab2       =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.f_CDab2 ...
%                             Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.f_CDab2 ...
%                             Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.f_CDab2];
Drag.C_Db2          =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDb2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDb2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDb2];
Drag.C_Dr2           =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDr2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDr2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDr2];
Drag.C_Dp2          =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDp2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDp2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDp2];
% Drag.C_Dpb(:,:,1)   =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDpb .* 0]; % Switched off
% Drag.C_Dpb(:,:,2)   =   [   Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDpb .* 0]; % Switched off
% Drag.C_Dpb(:,:,3)   =   [   Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDpb .* 0]; % Switched off
Drag.C_Dzeta2       =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDzeta2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDzeta2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDzeta2];
% Drag.C_Dzetab(:,:,1)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDzetab];
% Drag.C_Dzetab(:,:,2)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDzetab];
% Drag.C_Dzetab(:,:,3)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDzetab];
Drag.C_Dksis        =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDxis ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDxis ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDxis];
% Drag.C_Dksisb(:,:,1)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDxisb];
% Drag.C_Dksisb(:,:,2)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDxisb];
% Drag.C_Dksisb(:,:,3)=   [   Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDxisb];
Drag.C_Dksi2        =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDxia2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDxia2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDxia2];
% Drag.C_Dksib(:,:,1) =   [   Lateral_Coefficients.(VerticalTailSize).(Config{1}).CD.CDxiab];
% Drag.C_Dksib(:,:,2) =   [   Lateral_Coefficients.(VerticalTailSize).(Config{2}).CD.CDxiab];
% Drag.C_Dksib(:,:,3) =   [   Lateral_Coefficients.(VerticalTailSize).(Config{3}).CD.CDxiab];

Drag.CrossCouplFlag = 0;

%% New Sideslip Parameters
Drag.Sideslip.Wing.T_Vec       = ModelResult.DragModel.SubModel_Wing.T_Vec;
Drag.Sideslip.Wing.alpha_Vec   = ModelResult.DragModel.SubModel_Wing.alpha_Vec;
Drag.Sideslip.Wing.Cb_lookup   = ModelResult.DragModel.SubModel_Wing.Cb_lookup;
Drag.Sideslip.Wing.F_lookup    = ModelResult.DragModel.SubModel_Wing.F_lookup;
Drag.Sideslip.Wing.K_lookup    = ModelResult.DragModel.SubModel_Wing.K_lookup;

Drag.Sideslip.Body.T_Vec       = ModelResult.DragModel.SubModel_Body.T_Vec;
Drag.Sideslip.Body.alpha_Vec   = ModelResult.DragModel.SubModel_Body.alpha_Vec;
Drag.Sideslip.Body.Cb_lookup   = ModelResult.DragModel.SubModel_Body.Cb_lookup;
Drag.Sideslip.Body.F_lookup    = ModelResult.DragModel.SubModel_Body.F_lookup;
Drag.Sideslip.Body.K_lookup    = ModelResult.DragModel.SubModel_Body.K_lookup;

Drag.Sideslip.HTP.T_Vec       = ModelResult.DragModel.SubModel_HTP.T_Vec;
Drag.Sideslip.HTP.alpha_Vec   = ModelResult.DragModel.SubModel_HTP.alpha_Vec;
Drag.Sideslip.HTP.Cb_lookup   = ModelResult.DragModel.SubModel_HTP.Cb_lookup;
Drag.Sideslip.HTP.F_lookup    = ModelResult.DragModel.SubModel_HTP.F_lookup;
Drag.Sideslip.HTP.K_lookup    = ModelResult.DragModel.SubModel_HTP.K_lookup;

Drag.Sideslip.VTP.T_Vec       = ModelResult.DragModel.SubModel_VTP.T_Vec;
Drag.Sideslip.VTP.alpha_Vec   = ModelResult.DragModel.SubModel_VTP.alpha_Vec;
Drag.Sideslip.VTP.Cb_lookup   = ModelResult.DragModel.SubModel_VTP.Cb_lookup;
Drag.Sideslip.VTP.F_lookup    = ModelResult.DragModel.SubModel_VTP.F_lookup;
Drag.Sideslip.VTP.K_lookup    = ModelResult.DragModel.SubModel_VTP.K_lookup;

%% Drag OEI Parameters
%Drag Full
Drag.OEI.Full.C_bOEI_in      = ModelResult.DragModel.SubModel_Full.OEI.C_bOEI_in;
Drag.OEI.Full.F_tanHb        = ModelResult.DragModel.SubModel_Full.OEI.F_tanHb;
Drag.OEI.Full.K_Switch       = ModelResult.DragModel.SubModel_Full.OEI.K_Switch;
Drag.OEI.Full.F_Rel_far      = ModelResult.DragModel.SubModel_Full.OEI.F_Rel_far;
Drag.OEI.Full.beta_vec(3)    = ModelResult.DragModel.SubModel_Full.OEI.beta_vec(3);
Drag.OEI.Full.C_bOEI_far     = ModelResult.DragModel.SubModel_Full.OEI.C_bOEI_far;

Drag.OEI.Full.C_TOEI_in      = ModelResult.DragModel.SubModel_Full.OEI.C_TOEI_in;
Drag.OEI.Full.T_Vec(2)       = ModelResult.DragModel.SubModel_Full.OEI.T_Vec(2);
Drag.OEI.Full.F_tanHT        = ModelResult.DragModel.SubModel_Full.OEI.F_tanHT;
Drag.OEI.Full.C_TOEI_out     = ModelResult.DragModel.SubModel_Full.OEI.C_TOEI_out;

%Drag Body
Drag.OEI.Body.C_bOEI_in      = ModelResult.DragModel.SubModel_Body.OEI.C_bOEI_in;
Drag.OEI.Body.F_tanHb        = ModelResult.DragModel.SubModel_Body.OEI.F_tanHb;
Drag.OEI.Body.K_Switch       = ModelResult.DragModel.SubModel_Body.OEI.K_Switch;
Drag.OEI.Body.F_Rel_far      = ModelResult.DragModel.SubModel_Body.OEI.F_Rel_far;
Drag.OEI.Body.beta_vec(3)    = ModelResult.DragModel.SubModel_Body.OEI.beta_vec(3);
Drag.OEI.Body.C_bOEI_far     = ModelResult.DragModel.SubModel_Body.OEI.C_bOEI_far;

Drag.OEI.Body.C_TOEI_in      = ModelResult.DragModel.SubModel_Body.OEI.C_TOEI_in;
Drag.OEI.Body.T_Vec(2)       = ModelResult.DragModel.SubModel_Body.OEI.T_Vec(2);
Drag.OEI.Body.F_tanHT        = ModelResult.DragModel.SubModel_Body.OEI.F_tanHT;
Drag.OEI.Body.C_TOEI_out     = ModelResult.DragModel.SubModel_Body.OEI.C_TOEI_out;

%Drag HTP
Drag.OEI.HTP.C_bOEI_in      = ModelResult.DragModel.SubModel_HTP.OEI.C_bOEI_in;
Drag.OEI.HTP.F_tanHb        = ModelResult.DragModel.SubModel_HTP.OEI.F_tanHb;
Drag.OEI.HTP.K_Switch       = ModelResult.DragModel.SubModel_HTP.OEI.K_Switch;
Drag.OEI.HTP.F_Rel_far      = ModelResult.DragModel.SubModel_HTP.OEI.F_Rel_far;
Drag.OEI.HTP.beta_vec(3)    = ModelResult.DragModel.SubModel_HTP.OEI.beta_vec(3);
Drag.OEI.HTP.C_bOEI_far     = ModelResult.DragModel.SubModel_HTP.OEI.C_bOEI_far;

Drag.OEI.HTP.C_TOEI_in      = ModelResult.DragModel.SubModel_HTP.OEI.C_TOEI_in;
Drag.OEI.HTP.T_Vec(2)       = ModelResult.DragModel.SubModel_HTP.OEI.T_Vec(2);
Drag.OEI.HTP.F_tanHT        = ModelResult.DragModel.SubModel_HTP.OEI.F_tanHT;
Drag.OEI.HTP.C_TOEI_out     = ModelResult.DragModel.SubModel_HTP.OEI.C_TOEI_out;

%Drag VTP
Drag.OEI.VTP.C_bOEI_in      = ModelResult.DragModel.SubModel_VTP.OEI.C_bOEI_in;
Drag.OEI.VTP.F_tanHb        = ModelResult.DragModel.SubModel_VTP.OEI.F_tanHb;
Drag.OEI.VTP.K_Switch       = ModelResult.DragModel.SubModel_VTP.OEI.K_Switch;
Drag.OEI.VTP.F_Rel_far      = ModelResult.DragModel.SubModel_VTP.OEI.F_Rel_far;
Drag.OEI.VTP.beta_vec(3)    = ModelResult.DragModel.SubModel_VTP.OEI.beta_vec(3);
Drag.OEI.VTP.C_bOEI_far     = ModelResult.DragModel.SubModel_VTP.OEI.C_bOEI_far;

Drag.OEI.VTP.C_TOEI_in      = ModelResult.DragModel.SubModel_VTP.OEI.C_TOEI_in;
Drag.OEI.VTP.T_Vec(2)       = ModelResult.DragModel.SubModel_VTP.OEI.T_Vec(2);
Drag.OEI.VTP.F_tanHT        = ModelResult.DragModel.SubModel_VTP.OEI.F_tanHT;
Drag.OEI.VTP.C_TOEI_out     = ModelResult.DragModel.SubModel_VTP.OEI.C_TOEI_out;

%Drag Wing
Drag.OEI.Wing.C_bOEI_in      = ModelResult.DragModel.SubModel_Wing.OEI.C_bOEI_in;
Drag.OEI.Wing.F_tanHb        = ModelResult.DragModel.SubModel_Wing.OEI.F_tanHb;
Drag.OEI.Wing.K_Switch       = ModelResult.DragModel.SubModel_Wing.OEI.K_Switch;
Drag.OEI.Wing.F_Rel_far      = ModelResult.DragModel.SubModel_Wing.OEI.F_Rel_far;
Drag.OEI.Wing.beta_vec(3)    = ModelResult.DragModel.SubModel_Wing.OEI.beta_vec(3);
Drag.OEI.Wing.C_bOEI_far     = ModelResult.DragModel.SubModel_Wing.OEI.C_bOEI_far;

Drag.OEI.Wing.C_TOEI_in      = ModelResult.DragModel.SubModel_Wing.OEI.C_TOEI_in;
Drag.OEI.Wing.T_Vec(2)       = ModelResult.DragModel.SubModel_Wing.OEI.T_Vec(2);
Drag.OEI.Wing.F_tanHT        = ModelResult.DragModel.SubModel_Wing.OEI.F_tanHT;
Drag.OEI.Wing.C_TOEI_out     = ModelResult.DragModel.SubModel_Wing.OEI.C_TOEI_out;
end