function Side = SideStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, ModelResult)
% 
% 
%% Uncertainty Matrix
Side.T_Uncert        = ModelResult.SideModel.SubModel_Wing.T_Vec(1:3);
Side.beta_Uncert     = [0 5 10]./180*pi;
Side.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
Side.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
Side.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 10% 
Side.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
Side.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
Side.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 10% 
Side.Uncertainty_LT  = [    1                   1                   1                   ;...
                            Side.Uncert_b05T0   Side.Uncert_b05T1   Side.Uncert_b05T2   ;...
                            Side.Uncert_b10T0   Side.Uncert_b10T1   Side.Uncert_b10T2   ];

Side.Uncert_CYp     = 1.00  ; % not relevant
Side.Uncert_CYr     = 1.00  ; % not relevnat
Side.Uncert_CYzeta  = 1.00  ; % CYzeta: +- 10%      Similar to Cnzeta
Side.Uncert_CYxi    = 1.00  ; % not relevant

%% Derivatives
Side.C_Y0           = 0;
Side.C_YbdRD        = 0;
Side.C_YbdLG        = 0;
Side.C_Yb           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYb1];
Side.C_Yb3          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYb3 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYb3 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYb3];
Side.C_Yab          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYab ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYab ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYab];
Side.C_Yp           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYp];
Side.C_Yp(3)        =   RollingDerivatives.CYp_St;
Side.C_Ypa          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYpa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYpa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYpa];
% Side.f_C_Ypa        = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.f_CYpa ..
%                         Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.f_CYpa ..
%                         Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.f_CYpa];
Side.C_YpdRD        = 0;
Side.C_Yr           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYr];
Side.C_Yra          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYra ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYra ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYra];                   
%Side.f_C_Yra        = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.f_CYra ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.f_CYra ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.f_CYra];                   
Side.C_YrdRD        = 0;
Side.C_Yzeta        = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYzeta];
Side.C_Yzetaa       = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYzetaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYzetaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYzetaa];
%Side.f_C_Yzetaa     = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.f_CYzetaa ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.f_CYzetaa ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.f_CYzetaa];
% Side.C_Yzetab(:,:,1)= [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYzetab];
% Side.C_Yzetab(:,:,2)= [ Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYzetab];
% Side.C_Yzetab(:,:,3)= [ Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYzetab];
Side.C_Yksi         = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYxia];
Side.C_Yksia        = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.CYxiaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.CYxiaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.CYxiaa];
%Side.f_C_Yksia      = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).CY.f_CYxiaa ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).CY.f_CYxiaa ..
%                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).CY.f_CYxiaa];
Side.C_YdSP         = 0; 

Side.CrossCouplFlag = 0;

%% FullFlap CFD Data Update
% Sideslip Side Force Parameters
Side.Beta_LT                        = LateralSlipstream.Derivatives.New_Model.beta_LT  / 180*pi;
Side.T_Ref                          = LateralSlipstream.Derivatives.New_Model.T_ref;
Side.Cmu_Ref                        = LateralSlipstream.Derivatives.New_Model.Cmu_ref;

Side.VTP.LT_C_Y_T0_T1               = LateralSlipstream.Derivatives.New_Model.CY_VTP_LT_T0T1_Cmu0;
Side.VTP.LT_C_Y_T1_Cmu0_Cmu1        = LateralSlipstream.Derivatives.New_Model.CY_VTP_LT_T1_Cmu0Cmu03;
Side.VTP.C_Y_Beta0                  = LateralSlipstream.Derivatives.New_Model.CY_beta_VTP_0 * 180/pi;
Side.VTP.C_Y_BetaCmu                = LateralSlipstream.Derivatives.New_Model.CY_beta_VTP_Cmu * 180/pi;

Side.HTP.LT_C_Y_T0_T1               = LateralSlipstream.Derivatives.New_Model.CY_HTP_LT_T0T1_Cmu0;
Side.HTP.LT_C_Y_T1_Cmu0_Cmu1        = LateralSlipstream.Derivatives.New_Model.CY_HTP_LT_T1_Cmu0Cmu03;
Side.HTP.C_Y_Beta0                  = LateralSlipstream.Derivatives.New_Model.CY_beta_HTP_0 * 180/pi;
Side.HTP.C_Y_BetaCmu                = LateralSlipstream.Derivatives.New_Model.CY_beta_HTP_Cmu * 180/pi;

Side.Wing.C_Y_Beta0                 = LateralSlipstream.Derivatives.New_Model.CY_beta_Wing_0 * 180/pi;
Side.Wing.C_Y_Beta_T                = LateralSlipstream.Derivatives.New_Model.CY_beta_Wing_T * 180/pi;
Side.Wing.C_Y_BetaCmu               = LateralSlipstream.Derivatives.New_Model.CY_beta_Wing_Cmu * 180/pi;

Side.Fuselage.LT_C_Y_T0_T1          = LateralSlipstream.Derivatives.New_Model.CY_Fuse_LT_T0T1_Cmu0;
Side.Fuselage.LT_C_Y_T1_Cmu0_Cmu1   = LateralSlipstream.Derivatives.New_Model.CY_Fuse_LT_T1_Cmu0Cmu03;
Side.Fuselage.C_Y_Beta0             = LateralSlipstream.Derivatives.New_Model.CY_beta_Fuse_0 * 180/pi;
Side.Fuselage.C_Y_BetaCmu           = LateralSlipstream.Derivatives.New_Model.CY_beta_Fuse_Cmu * 180/pi;
Side.C_Yb(3)                        = Side.Fuselage.C_Y_Beta0 + Side.Wing.C_Y_Beta0 + Side.VTP.C_Y_Beta0;

%% New Sideslip Prameters
Side.Sideslip.Wing.T_Vec       = ModelResult.SideModel.SubModel_Wing.T_Vec;
Side.Sideslip.Wing.alpha_Vec   = ModelResult.SideModel.SubModel_Wing.alpha_Vec;
Side.Sideslip.Wing.Cb_lookup   = ModelResult.SideModel.SubModel_Wing.Cb_lookup;
Side.Sideslip.Wing.dB_lookup   = ModelResult.SideModel.SubModel_Wing.dB_lookup;
Side.Sideslip.Wing.F_lookup    = ModelResult.SideModel.SubModel_Wing.F_lookup;
Side.Sideslip.Wing.K_lookup    = ModelResult.SideModel.SubModel_Wing.K_lookup;

Side.Sideslip.Body.T_Vec       = ModelResult.SideModel.SubModel_Body.T_Vec;
Side.Sideslip.Body.alpha_Vec   = ModelResult.SideModel.SubModel_Body.alpha_Vec;
Side.Sideslip.Body.Cb_lookup   = ModelResult.SideModel.SubModel_Body.Cb_lookup;
Side.Sideslip.Body.dB_lookup   = ModelResult.SideModel.SubModel_Body.dB_lookup;
Side.Sideslip.Body.F_lookup    = ModelResult.SideModel.SubModel_Body.F_lookup;
Side.Sideslip.Body.K_lookup    = ModelResult.SideModel.SubModel_Body.K_lookup;

Side.Sideslip.HTP.T_Vec       = ModelResult.SideModel.SubModel_HTP.T_Vec;
Side.Sideslip.HTP.alpha_Vec   = ModelResult.SideModel.SubModel_HTP.alpha_Vec;
Side.Sideslip.HTP.Cb_lookup   = ModelResult.SideModel.SubModel_HTP.Cb_lookup;
Side.Sideslip.HTP.dB_lookup   = ModelResult.SideModel.SubModel_HTP.dB_lookup;
Side.Sideslip.HTP.F_lookup    = ModelResult.SideModel.SubModel_HTP.F_lookup;
Side.Sideslip.HTP.K_lookup    = ModelResult.SideModel.SubModel_HTP.K_lookup;

Side.Sideslip.VTP.T_Vec       = ModelResult.SideModel.SubModel_VTP.T_Vec;
Side.Sideslip.VTP.alpha_Vec   = ModelResult.SideModel.SubModel_VTP.alpha_Vec;
Side.Sideslip.VTP.Cb_lookup   = ModelResult.SideModel.SubModel_VTP.Cb_lookup;
Side.Sideslip.VTP.dB_lookup   = ModelResult.SideModel.SubModel_VTP.dB_lookup;
Side.Sideslip.VTP.F_lookup    = ModelResult.SideModel.SubModel_VTP.F_lookup;
Side.Sideslip.VTP.K_lookup    = ModelResult.SideModel.SubModel_VTP.K_lookup;

% Side.Sideslip.BetaCorrect.T_Vec       = ModelResult.SideModel.SubModel_Full.T_Vec;
% Side.Sideslip.BetaCorrect.alpha_Vec   = ModelResult.SideModel.SubModel_Full.alpha_Vec;
% Side.Sideslip.BetaCorrect.Cb_lookup   = ModelResult.SideModel.SubModel_Full.Cb_lookup;
% Cb_interp = ((ModelResult.SideModel.SubModel_Full.Cb_lookup(3,3) - ModelResult.SideModel.SubModel_Full.Cb_lookup(3,1))/ModelResult.SideModel.SubModel_Full.T_Vec(3))*ModelResult.SideModel.SubModel_Full.T_Vec(2) + ModelResult.SideModel.SubModel_Full.Cb_lookup(3,1);
% Side.Sideslip.BetaCorrect.Cb_lookup(3,2) = Cb_interp;
% Side.Sideslip.BetaCorrect.Cb_lookup(4,2) = Cb_interp;

%% OEI Parameters
%SideForce Wing
Side.OEI.Wing.C_bOEI_in      = ModelResult.SideModel.SubModel_Wing.OEI.C_bOEI_in;
Side.OEI.Wing.F_tanHb        = ModelResult.SideModel.SubModel_Wing.OEI.F_tanHb;
Side.OEI.Wing.K_Switch       = ModelResult.SideModel.SubModel_Wing.OEI.K_Switch;
Side.OEI.Wing.F_Rel_far      = ModelResult.SideModel.SubModel_Wing.OEI.F_Rel_far;
Side.OEI.Wing.beta_vec(3)    = ModelResult.SideModel.SubModel_Wing.OEI.beta_vec(3);
Side.OEI.Wing.C_bOEI_far     = ModelResult.SideModel.SubModel_Wing.OEI.C_bOEI_far;

Side.OEI.Wing.C_TOEI_in      = ModelResult.SideModel.SubModel_Wing.OEI.C_TOEI_in;
Side.OEI.Wing.T_Vec(2)       = ModelResult.SideModel.SubModel_Wing.OEI.T_Vec(2);
Side.OEI.Wing.F_tanHT        = ModelResult.SideModel.SubModel_Wing.OEI.F_tanHT;
Side.OEI.Wing.C_TOEI_out     = ModelResult.SideModel.SubModel_Wing.OEI.C_TOEI_out;

%SideForce VTP
Side.OEI.VTP.C_bOEI_in      = ModelResult.SideModel.SubModel_VTP.OEI.C_bOEI_in;
Side.OEI.VTP.F_tanHb        = ModelResult.SideModel.SubModel_VTP.OEI.F_tanHb;
Side.OEI.VTP.K_Switch       = ModelResult.SideModel.SubModel_VTP.OEI.K_Switch;
Side.OEI.VTP.F_Rel_far      = ModelResult.SideModel.SubModel_VTP.OEI.F_Rel_far;
Side.OEI.VTP.beta_vec(3)    = ModelResult.SideModel.SubModel_VTP.OEI.beta_vec(3);
Side.OEI.VTP.C_bOEI_far     = ModelResult.SideModel.SubModel_VTP.OEI.C_bOEI_far;

Side.OEI.VTP.C_TOEI_in      = ModelResult.SideModel.SubModel_VTP.OEI.C_TOEI_in;
Side.OEI.VTP.T_Vec(2)       = ModelResult.SideModel.SubModel_VTP.OEI.T_Vec(2);
Side.OEI.VTP.F_tanHT        = ModelResult.SideModel.SubModel_VTP.OEI.F_tanHT;
Side.OEI.VTP.C_TOEI_out     = ModelResult.SideModel.SubModel_VTP.OEI.C_TOEI_out;

%SideForce HTP
Side.OEI.HTP.C_bOEI_in      = ModelResult.SideModel.SubModel_HTP.OEI.C_bOEI_in;
Side.OEI.HTP.F_tanHb        = ModelResult.SideModel.SubModel_HTP.OEI.F_tanHb;
Side.OEI.HTP.K_Switch       = ModelResult.SideModel.SubModel_HTP.OEI.K_Switch;
Side.OEI.HTP.F_Rel_far      = ModelResult.SideModel.SubModel_HTP.OEI.F_Rel_far;
Side.OEI.HTP.beta_vec(3)    = ModelResult.SideModel.SubModel_HTP.OEI.beta_vec(3);
Side.OEI.HTP.C_bOEI_far     = ModelResult.SideModel.SubModel_HTP.OEI.C_bOEI_far;

Side.OEI.HTP.C_TOEI_in      = ModelResult.SideModel.SubModel_HTP.OEI.C_TOEI_in;
Side.OEI.HTP.T_Vec(2)       = ModelResult.SideModel.SubModel_HTP.OEI.T_Vec(2);
Side.OEI.HTP.F_tanHT        = ModelResult.SideModel.SubModel_HTP.OEI.F_tanHT;
Side.OEI.HTP.C_TOEI_out     = ModelResult.SideModel.SubModel_HTP.OEI.C_TOEI_out;

%SideForce Body
Side.OEI.Body.C_bOEI_in      = ModelResult.SideModel.SubModel_Body.OEI.C_bOEI_in;
Side.OEI.Body.F_tanHb        = ModelResult.SideModel.SubModel_Body.OEI.F_tanHb;
Side.OEI.Body.K_Switch       = ModelResult.SideModel.SubModel_Body.OEI.K_Switch;
Side.OEI.Body.F_Rel_far      = ModelResult.SideModel.SubModel_Body.OEI.F_Rel_far;
Side.OEI.Body.beta_vec(3)    = ModelResult.SideModel.SubModel_Body.OEI.beta_vec(3);
Side.OEI.Body.C_bOEI_far     = ModelResult.SideModel.SubModel_Body.OEI.C_bOEI_far;

Side.OEI.Body.C_TOEI_in      = ModelResult.SideModel.SubModel_Body.OEI.C_TOEI_in;
Side.OEI.Body.T_Vec(2)       = ModelResult.SideModel.SubModel_Body.OEI.T_Vec(2);
Side.OEI.Body.F_tanHT        = ModelResult.SideModel.SubModel_Body.OEI.F_tanHT;
Side.OEI.Body.C_TOEI_out     = ModelResult.SideModel.SubModel_Body.OEI.C_TOEI_out;

%SideForce Full
Side.OEI.Full.C_bOEI_in      = ModelResult.SideModel.SubModel_Full.OEI.C_bOEI_in;
Side.OEI.Full.F_tanHb        = ModelResult.SideModel.SubModel_Full.OEI.F_tanHb;
Side.OEI.Full.K_Switch       = ModelResult.SideModel.SubModel_Full.OEI.K_Switch;
Side.OEI.Full.F_Rel_far      = ModelResult.SideModel.SubModel_Full.OEI.F_Rel_far;
Side.OEI.Full.beta_vec(3)    = ModelResult.SideModel.SubModel_Full.OEI.beta_vec(3);
Side.OEI.Full.C_bOEI_far     = ModelResult.SideModel.SubModel_Full.OEI.C_bOEI_far;

Side.OEI.Full.C_TOEI_in      = ModelResult.SideModel.SubModel_Full.OEI.C_TOEI_in;
Side.OEI.Full.T_Vec(2)       = ModelResult.SideModel.SubModel_Full.OEI.T_Vec(2);
Side.OEI.Full.F_tanHT        = ModelResult.SideModel.SubModel_Full.OEI.F_tanHT;
Side.OEI.Full.C_TOEI_out     = ModelResult.SideModel.SubModel_Full.OEI.C_TOEI_out;
end