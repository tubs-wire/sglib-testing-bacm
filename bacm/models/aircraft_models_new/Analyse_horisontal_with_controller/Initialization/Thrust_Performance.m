close all
clear all
clc

load TP_Look_Up.mat

Ref     = 1;
Comp    = 7;

Max_Thr7     = Thrust_Look_up.Case7.Total_Thrust;
Max_Thr_VC7  = squeeze(Thrust_Look_up.Case7.Total_Thrust(:,1,:));
Max_Thr_AC7  = squeeze(Thrust_Look_up.Case7.Total_Thrust(:,:,1));
Max_Thr_PC7  = squeeze(Thrust_Look_up.Case7.Total_Thrust(8,:,:));
Min_Thr7     = Thrust_Look_up.Case7.Min_Thrust;
Min_Thr_VC7  = squeeze(Thrust_Look_up.Case7.Min_Thrust(:,1,:));
Min_Thr_AC7  = squeeze(Thrust_Look_up.Case7.Min_Thrust(:,:,1));
Min_Thr_PC7  = squeeze(Thrust_Look_up.Case7.Min_Thrust(8,:,:));
Alt7         = Thrust_Look_up.Case7.Alt;
Ma7          = Thrust_Look_up.Case7.Ma;
PL7          = Thrust_Look_up.Case7.PL;

Max_Thr     = Thrust_Look_up.Case1.Total_Thrust;
Max_Thr_VC  = squeeze(Thrust_Look_up.Case1.Total_Thrust(:,1,:));
Max_Thr_AC  = squeeze(Thrust_Look_up.Case1.Total_Thrust(:,:,1));
Max_Thr_PC  = squeeze(Thrust_Look_up.Case1.Total_Thrust(8,:,:));
Min_Thr     = Thrust_Look_up.Case1.Min_Thrust;
Min_Thr_VC  = squeeze(Thrust_Look_up.Case1.Min_Thrust(:,1,:));
Min_Thr_AC  = squeeze(Thrust_Look_up.Case1.Min_Thrust(:,:,1));
Min_Thr_PC  = squeeze(Thrust_Look_up.Case1.Min_Thrust(8,:,:));
Alt         = Thrust_Look_up.Case1.Alt;
Ma          = Thrust_Look_up.Case1.Ma;
PL          = Thrust_Look_up.Case1.PL;



% figure(1)
% surf(Alt,PL,Max_Thr_VC); grid on; hold on;
% mesh(Alt,PL,Min_Thr_VC); grid on; hold on;
%     xlabel('Altitude [m]')
%     ylabel('Power Lever [%]')
%     zlabel('Thrust [N]')
% 
% figure(2)
% surf(Ma,PL,Max_Thr_AC); grid on; hold on;
% mesh(Ma,PL,Min_Thr_AC); grid on; hold on;
%     xlabel('Ma [-]')
%     ylabel('Power Lever [%]')
%     zlabel('Thrust [N]')
% 

fontsize_Publication = 18;
figure(3)
subplot(3,3,[1:2 4:5])
% surf(Alt,Ma,Max_Thr_PC); grid on; hold on;
% surf(Alt,Ma,Min_Thr_PC); grid on; hold on;
% colormap(jet(128))
%     xlabel('Altitude [m]','fontsize',fontsize_Publication-3)
%     ylabel('Ma [-]','fontsize',fontsize_Publication-3)
%     zlabel('Thrust [N]','fontsize',fontsize_Publication-3)
%     view(135,10)
%     set(    gca,'FontSize',fontsize_Publication)
% subplot(3,2,[2 4])
surf(Alt,Ma,Min_Thr_PC); grid on; hold on;
    xlabel('Altitude [m]','fontsize',fontsize_Publication)
    ylabel('Ma [-]','fontsize',fontsize_Publication)
    zlabel('Thrust [N]','fontsize',fontsize_Publication)
    view(135,10)
    set(    gca,'FontSize',fontsize_Publication)
    set(    gcf,'Name','Minimum Thrust')
    printMyStyle('Min_Thrust', [pwd '\'],'landscape')

    figure(4)
subplot(3,3,[1:2 4:5])
surf(Alt,Ma,Max_Thr_PC); grid on; hold on;
surf(Alt,Ma,Min_Thr_PC); grid on; hold on;
colormap(jet(128))
    xlabel('Altitude [m]','fontsize',fontsize_Publication)
    ylabel('Ma [-]','fontsize',fontsize_Publication)
    zlabel('Thrust [N]','fontsize',fontsize_Publication)
    view(135,10)
    set(    gca,'FontSize',fontsize_Publication)
% subplot(3,2,[2 4])
% surf(Alt,Ma,Min_Thr_PC); grid on; hold on;
%     xlabel('Altitude [m]','fontsize',fontsize_Publication-3)
%     ylabel('Ma [-]','fontsize',fontsize_Publication-3)
%     zlabel('Thrust [N]','fontsize',fontsize_Publication-3)
%     view(135,10)
%     set(    gca,'FontSize',fontsize_Publication)
    set(    gcf,'Name','Thrust')
    printMyStyle('Thrust', [pwd '\'],'landscape')



%     
% figure(4)
% surf(Alt7,Ma7,Max_Thr_PC7); grid on; hold on;
% mesh(Alt7,Ma7,Min_Thr_PC7); grid on; hold on;
%     xlabel('Altitude [m]')
%     ylabel('Ma [-]')
%     zlabel('Thrust [N]')