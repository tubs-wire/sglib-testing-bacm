function Yaw = YawStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, ControlBoost, ModelResult)
% 
% 
%% Uncertainty Matrix
Yaw.T_Uncert        = ModelResult.YawModel.SubModel_Wing.T_Vec(1:3);
Yaw.beta_Uncert     = [0 5 10]./180*pi;
Yaw.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
Yaw.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
Yaw.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
Yaw.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
Yaw.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
Yaw.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
Yaw.Uncertainty_LT  = [    1                   1                   1                   ;...
                            Yaw.Uncert_b05T0   Yaw.Uncert_b05T1   Yaw.Uncert_b05T2   ;...
                            Yaw.Uncert_b10T0   Yaw.Uncert_b10T1   Yaw.Uncert_b10T2   ];

Yaw.Uncert_Cnp     = 1.00  ; % Cnp:    +- 15%
Yaw.Uncert_Cnr     = 1.00  ; % Cnr:    +10 -50%
Yaw.Uncert_Cnzeta  = 1.00  ; % Cnzeta: +- 10%
Yaw.Uncert_Cnxi    = 1.00  ; % Cnxi:   +- 10%

%% Derivatives
Yaw.C_n0            = 0;
Yaw.C_nbdRD         = 0;
Yaw.C_nbdLG         = 0;
Yaw.C_nb            = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnb1]; % 0.14903;
Yaw.C_nba           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnba ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnba ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnba]; 
Yaw.C_np            = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnp];
Yaw.C_np(3)         =   RollingDerivatives.Cnp_St;
Yaw.C_npa           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnpa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnpa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnpa];
Yaw.C_npdRD         = 0;
Yaw.C_nr            = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnr];
Yaw.C_nra           = 0;
Yaw.C_nrdRD         = 0;
Yaw.C_nksi          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnxia] * ControlBoost; % Due to change in the rolling moment CFD Data Update 
Yaw.C_nzeta         = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cn.Cnzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cn.Cnzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cn.Cnzeta];
Yaw.C_ndSP          = [0 0 0];

Yaw.CrossCouplFlag = 0;

% Sideslip Yaw Parameters
Yaw.Beta_LT                         = LateralSlipstream.Derivatives.New_Model.beta_LT / 180*pi;
Yaw.T_Ref                           = LateralSlipstream.Derivatives.New_Model.T_ref;
Yaw.Cmu_Ref                         = LateralSlipstream.Derivatives.New_Model.Cmu_ref;

Yaw.VTP.LT_C_n_T0_T1                = LateralSlipstream.Derivatives.New_Model.Cn_VTP_LT_T0T1_Cmu0;
Yaw.VTP.LT_C_n_T1_Cmu0_Cmu1         = LateralSlipstream.Derivatives.New_Model.Cn_VTP_LT_T1_Cmu0Cmu03;
Yaw.VTP.C_n_Beta0                   = LateralSlipstream.Derivatives.New_Model.Cn_beta_VTP_0 * 180/pi;
Yaw.VTP.C_n_BetaCmu                 = LateralSlipstream.Derivatives.New_Model.Cn_beta_VTP_Cmu * 180/pi;

Yaw.HTP.LT_C_n_T0_T1                = LateralSlipstream.Derivatives.New_Model.Cn_HTP_LT_T0T1_Cmu0;
Yaw.HTP.LT_C_n_T1_Cmu0_Cmu1         = LateralSlipstream.Derivatives.New_Model.Cn_HTP_LT_T1_Cmu0Cmu03;
Yaw.HTP.C_n_Beta0                   = LateralSlipstream.Derivatives.New_Model.Cn_beta_HTP_0 * 180/pi;
Yaw.HTP.C_n_BetaCmu                 = LateralSlipstream.Derivatives.New_Model.Cn_beta_HTP_Cmu * 180/pi;

Yaw.Wing.LT_C_n_T0_T1               = LateralSlipstream.Derivatives.New_Model.Cn_Wing_LT_T0T1_Cmu0;
Yaw.Wing.LT_C_n_T1_Cmu0_Cmu1        = LateralSlipstream.Derivatives.New_Model.Cn_Wing_LT_T1_Cmu0Cmu03;
Yaw.Wing.C_n_Beta0                  = LateralSlipstream.Derivatives.New_Model.Cn_beta_Wing_0 * 180/pi;
Yaw.Wing.C_n_BetaCmu                = LateralSlipstream.Derivatives.New_Model.Cn_beta_Wing_Cmu * 180/pi;

Yaw.Fuselage.LT_C_n_T0_T1           = LateralSlipstream.Derivatives.New_Model.Cn_Fuse_LT_T0T1_Cmu0;
Yaw.Fuselage.LT_C_n_T1_Cmu0_Cmu1    = LateralSlipstream.Derivatives.New_Model.Cn_Fuse_LT_T1_Cmu0Cmu03;
Yaw.Fuselage.C_n_Beta0              = LateralSlipstream.Derivatives.New_Model.Cn_beta_Fuse_0 * 180/pi;
Yaw.Fuselage.C_n_BetaCmu            = LateralSlipstream.Derivatives.New_Model.Cn_beta_Fuse_Cmu * 180/pi;

Yaw.C_nb(3)                         = Yaw.Fuselage.C_n_Beta0 + Yaw.Wing.C_n_Beta0 + Yaw.VTP.C_n_Beta0;

%% New Yaw Parameters
Yaw.Sideslip.Wing.T_Vec       = ModelResult.YawModel.SubModel_Wing.T_Vec;
Yaw.Sideslip.Wing.alpha_Vec   = ModelResult.YawModel.SubModel_Wing.alpha_Vec;
Yaw.Sideslip.Wing.Cb_lookup   = ModelResult.YawModel.SubModel_Wing.Cb_lookup;
Yaw.Sideslip.Wing.dB_lookup   = ModelResult.YawModel.SubModel_Wing.dB_lookup;
Yaw.Sideslip.Wing.F_lookup    = ModelResult.YawModel.SubModel_Wing.F_lookup;
Yaw.Sideslip.Wing.K_lookup    = ModelResult.YawModel.SubModel_Wing.K_lookup;

Yaw.Sideslip.Body.T_Vec       = ModelResult.YawModel.SubModel_Body.T_Vec;
Yaw.Sideslip.Body.alpha_Vec   = ModelResult.YawModel.SubModel_Body.alpha_Vec;
Yaw.Sideslip.Body.Cb_lookup   = ModelResult.YawModel.SubModel_Body.Cb_lookup;
Yaw.Sideslip.Body.dB_lookup   = ModelResult.YawModel.SubModel_Body.dB_lookup;
Yaw.Sideslip.Body.F_lookup    = ModelResult.YawModel.SubModel_Body.F_lookup;
Yaw.Sideslip.Body.K_lookup    = ModelResult.YawModel.SubModel_Body.K_lookup;

Yaw.Sideslip.HTP.T_Vec       = ModelResult.YawModel.SubModel_HTP.T_Vec;
Yaw.Sideslip.HTP.alpha_Vec   = ModelResult.YawModel.SubModel_HTP.alpha_Vec;
Yaw.Sideslip.HTP.Cb_lookup   = ModelResult.YawModel.SubModel_HTP.Cb_lookup;
Yaw.Sideslip.HTP.dB_lookup   = ModelResult.YawModel.SubModel_HTP.dB_lookup;
Yaw.Sideslip.HTP.F_lookup    = ModelResult.YawModel.SubModel_HTP.F_lookup;
Yaw.Sideslip.HTP.K_lookup    = ModelResult.YawModel.SubModel_HTP.K_lookup;

Yaw.Sideslip.VTP.T_Vec       = ModelResult.YawModel.SubModel_VTP.T_Vec;
Yaw.Sideslip.VTP.alpha_Vec   = ModelResult.YawModel.SubModel_VTP.alpha_Vec;
Yaw.Sideslip.VTP.Cb_lookup   = ModelResult.YawModel.SubModel_VTP.Cb_lookup;
Yaw.Sideslip.VTP.dB_lookup   = ModelResult.YawModel.SubModel_VTP.dB_lookup;
Yaw.Sideslip.VTP.F_lookup    = ModelResult.YawModel.SubModel_VTP.F_lookup;
Yaw.Sideslip.VTP.K_lookup    = ModelResult.YawModel.SubModel_VTP.K_lookup;

% Yaw.Sideslip.BetaCorrect.T_Vec       = ModelResult.YawModel.SubModel_Full.T_Vec;
% Yaw.Sideslip.BetaCorrect.alpha_Vec   = ModelResult.YawModel.SubModel_Full.alpha_Vec;
% Yaw.Sideslip.BetaCorrect.Cb_lookup   = ModelResult.YawModel.SubModel_Full.Cb_lookup;
% Cb_interp = ((ModelResult.YawModel.SubModel_Full.Cb_lookup(3,3) - ModelResult.YawModel.SubModel_Full.Cb_lookup(3,1))/ModelResult.YawModel.SubModel_Full.T_Vec(3))*ModelResult.YawModel.SubModel_Full.T_Vec(2) + ModelResult.YawModel.SubModel_Full.Cb_lookup(3,1);
% Yaw.Sideslip.BetaCorrect.Cb_lookup(3,2) = Cb_interp;
% Yaw.Sideslip.BetaCorrect.Cb_lookup(4,2) = Cb_interp;

%% OEI Parameters
%Yaw Wing
Yaw.OEI.Wing.C_bOEI_in      = ModelResult.YawModel.SubModel_Wing.OEI.C_bOEI_in;
Yaw.OEI.Wing.F_tanHb        = ModelResult.YawModel.SubModel_Wing.OEI.F_tanHb;
Yaw.OEI.Wing.K_Switch       = ModelResult.YawModel.SubModel_Wing.OEI.K_Switch;
Yaw.OEI.Wing.F_Rel_far      = ModelResult.YawModel.SubModel_Wing.OEI.F_Rel_far;
Yaw.OEI.Wing.beta_vec(3)    = ModelResult.YawModel.SubModel_Wing.OEI.beta_vec(3);
Yaw.OEI.Wing.C_bOEI_far     = ModelResult.YawModel.SubModel_Wing.OEI.C_bOEI_far;

Yaw.OEI.Wing.C_TOEI_in      = ModelResult.YawModel.SubModel_Wing.OEI.C_TOEI_in;
Yaw.OEI.Wing.T_Vec(2)       = ModelResult.YawModel.SubModel_Wing.OEI.T_Vec(2);
Yaw.OEI.Wing.F_tanHT        = ModelResult.YawModel.SubModel_Wing.OEI.F_tanHT;
Yaw.OEI.Wing.C_TOEI_out     = ModelResult.YawModel.SubModel_Wing.OEI.C_TOEI_out;

%Yaw VTP
Yaw.OEI.VTP.C_bOEI_in      = ModelResult.YawModel.SubModel_VTP.OEI.C_bOEI_in;
Yaw.OEI.VTP.F_tanHb        = ModelResult.YawModel.SubModel_VTP.OEI.F_tanHb;
Yaw.OEI.VTP.K_Switch       = ModelResult.YawModel.SubModel_VTP.OEI.K_Switch;
Yaw.OEI.VTP.F_Rel_far      = ModelResult.YawModel.SubModel_VTP.OEI.F_Rel_far;
Yaw.OEI.VTP.beta_vec(3)    = ModelResult.YawModel.SubModel_VTP.OEI.beta_vec(3);
Yaw.OEI.VTP.C_bOEI_far     = ModelResult.YawModel.SubModel_VTP.OEI.C_bOEI_far;

Yaw.OEI.VTP.C_TOEI_in      = ModelResult.YawModel.SubModel_VTP.OEI.C_TOEI_in;
Yaw.OEI.VTP.T_Vec(2)       = ModelResult.YawModel.SubModel_VTP.OEI.T_Vec(2);
Yaw.OEI.VTP.F_tanHT        = ModelResult.YawModel.SubModel_VTP.OEI.F_tanHT;
Yaw.OEI.VTP.C_TOEI_out     = ModelResult.YawModel.SubModel_VTP.OEI.C_TOEI_out;

%Yaw Body
Yaw.OEI.Body.C_bOEI_in      = ModelResult.YawModel.SubModel_Body.OEI.C_bOEI_in;
Yaw.OEI.Body.F_tanHb        = ModelResult.YawModel.SubModel_Body.OEI.F_tanHb;
Yaw.OEI.Body.K_Switch       = ModelResult.YawModel.SubModel_Body.OEI.K_Switch;
Yaw.OEI.Body.F_Rel_far      = ModelResult.YawModel.SubModel_Body.OEI.F_Rel_far;
Yaw.OEI.Body.beta_vec(3)    = ModelResult.YawModel.SubModel_Body.OEI.beta_vec(3);
Yaw.OEI.Body.C_bOEI_far     = ModelResult.YawModel.SubModel_Body.OEI.C_bOEI_far;

Yaw.OEI.Body.C_TOEI_in      = ModelResult.YawModel.SubModel_Body.OEI.C_TOEI_in;
Yaw.OEI.Body.T_Vec(2)       = ModelResult.YawModel.SubModel_Body.OEI.T_Vec(2);
Yaw.OEI.Body.F_tanHT        = ModelResult.YawModel.SubModel_Body.OEI.F_tanHT;
Yaw.OEI.Body.C_TOEI_out     = ModelResult.YawModel.SubModel_Body.OEI.C_TOEI_out;

%Yaw HTP
Yaw.OEI.HTP.C_bOEI_in      = ModelResult.YawModel.SubModel_HTP.OEI.C_bOEI_in;
Yaw.OEI.HTP.F_tanHb        = ModelResult.YawModel.SubModel_HTP.OEI.F_tanHb;
Yaw.OEI.HTP.K_Switch       = ModelResult.YawModel.SubModel_HTP.OEI.K_Switch;
Yaw.OEI.HTP.F_Rel_far      = ModelResult.YawModel.SubModel_HTP.OEI.F_Rel_far;
Yaw.OEI.HTP.beta_vec(3)    = ModelResult.YawModel.SubModel_HTP.OEI.beta_vec(3);
Yaw.OEI.HTP.C_bOEI_far     = ModelResult.YawModel.SubModel_HTP.OEI.C_bOEI_far;

Yaw.OEI.HTP.C_TOEI_in      = ModelResult.YawModel.SubModel_HTP.OEI.C_TOEI_in;
Yaw.OEI.HTP.T_Vec(2)       = ModelResult.YawModel.SubModel_HTP.OEI.T_Vec(2);
Yaw.OEI.HTP.F_tanHT        = ModelResult.YawModel.SubModel_HTP.OEI.F_tanHT;
Yaw.OEI.HTP.C_TOEI_out     = ModelResult.YawModel.SubModel_HTP.OEI.C_TOEI_out;

%Yaw Full
Yaw.OEI.Full.C_bOEI_in      = ModelResult.YawModel.SubModel_Full.OEI.C_bOEI_in;
Yaw.OEI.Full.F_tanHb        = ModelResult.YawModel.SubModel_Full.OEI.F_tanHb;
Yaw.OEI.Full.K_Switch       = ModelResult.YawModel.SubModel_Full.OEI.K_Switch;
Yaw.OEI.Full.F_Rel_far      = ModelResult.YawModel.SubModel_Full.OEI.F_Rel_far;
Yaw.OEI.Full.beta_vec(3)    = ModelResult.YawModel.SubModel_Full.OEI.beta_vec(3);
Yaw.OEI.Full.C_bOEI_far     = ModelResult.YawModel.SubModel_Full.OEI.C_bOEI_far;

Yaw.OEI.Full.C_TOEI_in      = ModelResult.YawModel.SubModel_Full.OEI.C_TOEI_in;
Yaw.OEI.Full.T_Vec(2)       = ModelResult.YawModel.SubModel_Full.OEI.T_Vec(2);
Yaw.OEI.Full.F_tanHT        = ModelResult.YawModel.SubModel_Full.OEI.F_tanHT;
Yaw.OEI.Full.C_TOEI_out     = ModelResult.YawModel.SubModel_Full.OEI.C_TOEI_out;

%% Artificial Rolling momtent for Roll Performance Investigation
Yaw.Artificial.Delta_Cn_xi             = 0;
Yaw.Artificial.Control_Duration        = 0;    
Yaw.Artificial.Delta_Cn_xi_Slope       = 0;
Yaw.Artificial.Cn_xi_Init              = 0;    
Yaw.Artificial.Cn_xi_t_Init            = 0;    
Yaw.Artificial.Cn_xi_Flag              = 0;

%% Artificial Rolling momtent for Yaw Performance Investigation
Yaw.Artificial.Delta_Cn_zeta           = 0;
Yaw.Artificial.Control_Duration        = 0;    
Yaw.Artificial.Delta_Cn_zeta_Slope     = 0;
Yaw.Artificial.Cn_zeta_Init            = 0;    
Yaw.Artificial.Cn_zeta_t_Init          = 0;    
Yaw.Artificial.Cn_zeta_Flag            = 0;
end