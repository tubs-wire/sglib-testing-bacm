function Lift = LiftStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, ModelResult, Longitudinal_Parameters)
% 
% 
%% Uncertainty Matrix
Lift.T_Uncert        = ModelResult.LiftModel.SubModel_Wing.T_Vec(1:3);
Lift.beta_Uncert     = [0 5 10]./180*pi;
Lift.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 10% 
Lift.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 15% 
Lift.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 20% 
Lift.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 10% 
Lift.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 15% 
Lift.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
Lift.Uncertainty_LT = [ 1                   1                   1                   ;...
                        Lift.Uncert_b05T0   Lift.Uncert_b05T1   Lift.Uncert_b05T2   ;...
                        Lift.Uncert_b10T0   Lift.Uncert_b10T1   Lift.Uncert_b10T2   ];

%% Derivatives
Lift.C_L0           =  AeroCoefficientsNL.CL0;
Lift.C_LFRa         =  AeroCoefficientsNL.CLa;
Lift.C_La_H         =  AeroCoefficientsNL.CLa_HTP; % 4.5554;
Lift.C_Leta         =  AeroCoefficientsNL.CLeta_HTP; 
Lift.dC_LdFL        =  [0   MidFlap.dCL0        Longitudinal_Parameters.T0.Cmu03.CL.CL0 ];
Lift.dC_LadFL       =  [0   MidFlap.dCLa        Longitudinal_Parameters.T0.Cmu03.CL.CLa ];


Lift.dC_LdCmu_BLC   =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.BLC.dCL_dCmu ];
Lift.dC_LadCmu_BLC  =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.BLC.dCLa_dCmu ];
Lift.dC_La2dCmu_BLC =  [0   MidFlap.Cmu.CL.k1   0];
Lift.dC_LdCmu_CC    =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.CC.dCL_dCmu ];
Lift.dC_LadCmu_CC   =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.CC.dCLa_dCmu ];
Lift.dC_La2dCmu_CC  =  [0   MidFlap.Cmu.CL.k1   0];
% 
Lift.dC_LdCmu_30    =  [(-0.05 * Lift.dC_LdCmu_BLC(2)) 0 (0.05 * Lift.dC_LdCmu_CC(2))];
Lift.dC_LadCmu_30   =  [(-0.05 * Lift.dC_LadCmu_BLC(2)) 0 (0.05 * Lift.dC_LadCmu_CC(2))];
Lift.dC_LdCmu_65    =  Longitudinal_Parameters.LT_0_CL_Cmu;
Lift.dC_LadCmu_65   =  Longitudinal_Parameters.LT_al_CL_Cmu;

% Lift.dC_L_DN        =  DroopNose.dCL_DN  - 0.1;
% Lift.dC_La_DN       =  1.1157; % DroopNose.dCLa_DN;
% Lift.dC_La_T        =  0.00003615 * pi/180 /4; % dCLa(T) = dC_La_T * T;
Lift.dC_LdRD        =  0;
Lift.dC_LdSB        =  [0 0 0]; 
Lift.dC_LdLG        =  0; 
Lift.h_BO           =  0;                                    % Starting Ground Effect height             [m]
Lift.dC_L_dh        =  [0 0 0];
Lift.alphamax       =  [15 14 30] * pi/180;
Lift.alpha_wfc_max  =  10^9 * pi/180;
% Lift.alpha_HTP_max  =  inf * pi/180;
% Lift.alpha_HTP_min  = -inf * pi/180;
Lift.alpha_HTP_max  =  AeroCoefficientsNL.a_max_HTP_deg * pi/180;
Lift.alpha_HTP_min  = -AeroCoefficientsNL.a_max_HTP_deg * pi/180;

% Lateral Motion Influences on Lift %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Lift.C_Lab(:,:,1)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config1).CL.CLab];
%Lift.C_Lab(:,:,2)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config2).CL.CLab];
%Lift.C_Lab(:,:,3)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config3).CL.CLab];
% Lift.f_C_Lab2       =  [    Lateral_Coefficients.(VerticalTailSize).(Config1).CL.f_CLab2 ...
%                             Lateral_Coefficients.(VerticalTailSize).(Config2).CL.f_CLab2 ...
%                             Lateral_Coefficients.(VerticalTailSize).(Config3).CL.f_CLab2];
Lift.C_Lb2          =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLb2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLb2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLb2] * 0;
%neu                        
Lift.C_Lr           =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLr  ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLr  ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLr ] * 0;
                        
                        
% Lift.C_Lrb(:,:,1)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config1).CL.CLrb];
% Lift.C_Lrb(:,:,2)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config2).CL.CLrb];
% Lift.C_Lrb(:,:,3)   =  [    Lateral_Coefficients.(VerticalTailSize).(Config3).CL.CLrb];
Lift.C_Lp2          =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLp2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLp2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLp2] * 0;
Lift.C_Lzeta2       =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLzeta2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLzeta2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLzeta2] * 0;
Lift.C_Lksis        =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLxis ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLxis ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLxis] * 0;
Lift.C_Lksi2        =  [    Lateral_Coefficients.(VerticalTailSize).(Config{1}).CL.CLxia2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{2}).CL.CLxia2 ...
                            Lateral_Coefficients.(VerticalTailSize).(Config{3}).CL.CLxia2] * 0;
Lift.CrossCouplFlag = 0;

% Unsteady Lift %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reference values (Cmu = 0.033)
% Lift.f_Ref      = AeroCoefficientsNL.f_ref;                                   % a1 : static stall characteristics of the airfoil
% Lift.a1_Ref     = AeroCoefficientsNL.a1_ref;                                 % time constant coefficient 
% Lift.astar_Ref  = AeroCoefficientsNL.astar_ref;                          % alpha star [rad]
% % BLC Cmu Increments
% Lift.df_dcmu_BLC        = AeroCoefficientsNL.BLC.df_dcmuBLC ;                                   % a1 : static stall characteristics of the airfoil
% Lift.da1_dcmu_BLC       = AeroCoefficientsNL.BLC.da1_dcmuBLC ;                                 % time constant coefficient 
% Lift.dastar_dcmu_BLC    = AeroCoefficientsNL.BLC.dastar_dcmuBLC ;                          % alpha star [rad]
% % CC Cmu Increments
% Lift.df_dcmu_CC         = AeroCoefficientsNL.CC.df_dcmuCC;                                   % a1 : static stall characteristics of the airfoil
% Lift.da1_dcmu_CC        = AeroCoefficientsNL.CC.da1_dcmuCC;                                 % time constant coefficient 
% Lift.dastar_dcmu_CC     = AeroCoefficientsNL.CC.dastar_dcmuCC;                          % alpha star [rad]
% DroopNose increments to reference (Cmu = 0.033)
% Lift.df_DN      = DroopNose.dfL_DN;                                   % a1 : static stall characteristics of the airfoil
% Lift.da1_DN     = DroopNose.da1L_DN;                                 % time constant coefficient 
% Lift.dastar_DN  = DroopNose.dastarL_DN - 4.5;                          % REF2 Aerodatenkorrektur
% DroopNose factors to reference (Cmu = 0.033)
% Lift.Ff_DN      = 1 + (DroopNose.dfL_DN     /AeroCoefficientsNL.f_ref);                                   % a1 : static stall characteristics of the airfoil
% Lift.Fa1_DN     = 1 + (DroopNose.da1L_DN    /AeroCoefficientsNL.a1_ref);                                 % time constant coefficient 
% Lift.Fastar_DN  = 1 + (DroopNose.dastarL_DN /AeroCoefficientsNL.astar_ref);                          % alpha star [rad]

% Lift.dastar_T   = 0.4922;                          % dal_max_T = dastar_T * T^(1/4) d_al = 7� bei 40900 N
% Lift.dastar_T   = 0.4219;                          % dal_max_T = dastar_T * T^(1/4) d_al = 6� bei 40900 N
%% Thrust/Slipstream Influence
Lift.Thrust.Body.k_T    = ModelResult.LiftModel.SubModel_Body.AEO.K_T;
Lift.Thrust.Body.F_T    = ModelResult.LiftModel.SubModel_Body.AEO.F_tanh_T;
Lift.Thrust.Body.dCL_T  = ModelResult.LiftModel.SubModel_Body.AEO.C_dT  ;
Lift.Thrust.Body.dCLa_T = ModelResult.LiftModel.SubModel_Body.AEO.dC_dadT;

Lift.Thrust.Wing.k_T    = ModelResult.LiftModel.SubModel_Wing.AEO.K_T;
Lift.Thrust.Wing.F_T    = ModelResult.LiftModel.SubModel_Wing.AEO.F_tanh_T;
Lift.Thrust.Wing.dCL_T  = ModelResult.LiftModel.SubModel_Wing.AEO.C_dT  ;
Lift.Thrust.Wing.dCLa_T = ModelResult.LiftModel.SubModel_Wing.AEO.dC_dadT;

Lift.Thrust.VTP.k_T     = ModelResult.LiftModel.SubModel_VTP.AEO.K_T;
Lift.Thrust.VTP.F_T     = ModelResult.LiftModel.SubModel_VTP.AEO.F_tanh_T;
Lift.Thrust.VTP.dCL_T   = ModelResult.LiftModel.SubModel_VTP.AEO.C_dT  ;
Lift.Thrust.VTP.dCLa_T  = ModelResult.LiftModel.SubModel_VTP.AEO.dC_dadT;

Lift.Thrust.HTP.k_T     = ModelResult.LiftModel.SubModel_HTP.AEO.K_T;
Lift.Thrust.HTP.F_T     = ModelResult.LiftModel.SubModel_HTP.AEO.F_tanh_T;
Lift.Thrust.HTP.dCL_T   = ModelResult.LiftModel.SubModel_HTP.AEO.C_dT  ;
Lift.Thrust.HTP.dCLa_T  = ModelResult.LiftModel.SubModel_HTP.AEO.dC_dadT;

Lift.Thrust.Full.k_T    = ModelResult.LiftModel.SubModel_Full.AEO.K_T;
Lift.Thrust.Full.F_T    = ModelResult.LiftModel.SubModel_Full.AEO.F_tanh_T;
Lift.Thrust.Full.dCL_T  = ModelResult.LiftModel.SubModel_Full.AEO.C_dT  ;
Lift.Thrust.Full.dCLa_T = ModelResult.LiftModel.SubModel_Full.AEO.dC_dadT;

Lift.Thrust.Simple.T_Vec         = Longitudinal_Parameters.T_Vec(1:3);
Lift.Thrust.Simple.dCLdT         = Longitudinal_Parameters.LT_0_CL_T(1:3);
Lift.Thrust.Simple.dCLadT        = Longitudinal_Parameters.LT_al_CL_T(1:3);

%% Jet Momentum and Thrust/Slipstream Influences on Stall
Lift.T_Vec          = Longitudinal_Parameters.T_Vec(1:3);

Lift.f_T0_Cmu03     = Longitudinal_Parameters.T0.Cmu03.CL.F_Tanh;
Lift.a1_T0_Cmu03    = Longitudinal_Parameters.T0.Cmu03.CL.k;
Lift.astar_T0_Cmu03 = Longitudinal_Parameters.T0.Cmu03.CL.dalpha;

Lift.f_LT_Cmu       = Longitudinal_Parameters.Stall.LT_F_CL_Cmu;                                   % a1 : static stall characteristics of the airfoil
Lift.f_LT_T         = Longitudinal_Parameters.Stall.LT_F_CL_T(1:3);                                   % a1 : static stall characteristics of the airfoil
Lift.a1_LT_Cmu      = Longitudinal_Parameters.Stall.LT_k_CL_Cmu;                                   % a1 : static stall characteristics of the airfoil
Lift.a1_LT_T        = Longitudinal_Parameters.Stall.LT_k_CL_T(1:3);                                   % a1 : static stall characteristics of the airfoil
Lift.astar_LT_Cmu   = Longitudinal_Parameters.Stall.LT_da_CL_Cmu;                                   % a1 : static stall characteristics of the airfoil
Lift.astar_LT_T     = Longitudinal_Parameters.Stall.LT_da_CL_T(1:3);                                   % a1 : static stall characteristics of the airfoil

% Mid Flaps
Lift.MidFlap.f1         = MidFlap.Stall.CL.f1;                                   % a1 : static stall characteristics of the airfoil
Lift.MidFlap.a1         = MidFlap.Stall.CL.a1;                                 % time constant coefficient 
Lift.MidFlap.astar      = MidFlap.Stall.CL.aStar;                          % alpha star [rad]

%% New Siedslip Parameters
Lift.Sideslip.Wing.T_Vec       = ModelResult.LiftModel.SubModel_Wing.T_Vec;
Lift.Sideslip.Wing.alpha_Vec   = ModelResult.LiftModel.SubModel_Wing.alpha_Vec;
Lift.Sideslip.Wing.Cb_lookup   = ModelResult.LiftModel.SubModel_Wing.Cb_lookup;
Lift.Sideslip.Wing.F_lookup    = ModelResult.LiftModel.SubModel_Wing.F_lookup;
Lift.Sideslip.Wing.K_lookup    = ModelResult.LiftModel.SubModel_Wing.K_lookup;

Lift.Sideslip.Body.T_Vec       = ModelResult.LiftModel.SubModel_Body.T_Vec;
Lift.Sideslip.Body.alpha_Vec   = ModelResult.LiftModel.SubModel_Body.alpha_Vec;
Lift.Sideslip.Body.Cb_lookup   = ModelResult.LiftModel.SubModel_Body.Cb_lookup;
Lift.Sideslip.Body.F_lookup    = ModelResult.LiftModel.SubModel_Body.F_lookup;
Lift.Sideslip.Body.K_lookup    = ModelResult.LiftModel.SubModel_Body.K_lookup;

Lift.Sideslip.HTP.T_Vec       = ModelResult.LiftModel.SubModel_HTP.T_Vec;
Lift.Sideslip.HTP.alpha_Vec   = ModelResult.LiftModel.SubModel_HTP.alpha_Vec;
Lift.Sideslip.HTP.Cb_lookup   = ModelResult.LiftModel.SubModel_HTP.Cb_lookup;
Lift.Sideslip.HTP.F_lookup    = ModelResult.LiftModel.SubModel_HTP.F_lookup;
Lift.Sideslip.HTP.K_lookup    = ModelResult.LiftModel.SubModel_HTP.K_lookup;

Lift.Sideslip.VTP.T_Vec       = ModelResult.LiftModel.SubModel_VTP.T_Vec;
Lift.Sideslip.VTP.alpha_Vec   = ModelResult.LiftModel.SubModel_VTP.alpha_Vec;
Lift.Sideslip.VTP.Cb_lookup   = ModelResult.LiftModel.SubModel_VTP.Cb_lookup;
Lift.Sideslip.VTP.F_lookup    = ModelResult.LiftModel.SubModel_VTP.F_lookup;
Lift.Sideslip.VTP.K_lookup    = ModelResult.LiftModel.SubModel_VTP.K_lookup;

%% Lift OEI Parameters
%Lift Full
Lift.OEI.Full.C_bOEI_in      = ModelResult.LiftModel.SubModel_Full.OEI.C_bOEI_in;
Lift.OEI.Full.F_tanHb        = ModelResult.LiftModel.SubModel_Full.OEI.F_tanHb;
Lift.OEI.Full.K_Switch       = ModelResult.LiftModel.SubModel_Full.OEI.K_Switch;
Lift.OEI.Full.F_Rel_far      = ModelResult.LiftModel.SubModel_Full.OEI.F_Rel_far;
Lift.OEI.Full.beta_vec(3)    = ModelResult.LiftModel.SubModel_Full.OEI.beta_vec(3);
Lift.OEI.Full.C_bOEI_far     = ModelResult.LiftModel.SubModel_Full.OEI.C_bOEI_far;

Lift.OEI.Full.C_TOEI_in      = ModelResult.LiftModel.SubModel_Full.OEI.C_TOEI_in;
Lift.OEI.Full.T_Vec(2)       = ModelResult.LiftModel.SubModel_Full.OEI.T_Vec(2);
Lift.OEI.Full.F_tanHT        = ModelResult.LiftModel.SubModel_Full.OEI.F_tanHT;
Lift.OEI.Full.C_TOEI_out     = ModelResult.LiftModel.SubModel_Full.OEI.C_TOEI_out;

%Lift Body
Lift.OEI.Body.C_bOEI_in      = ModelResult.LiftModel.SubModel_Body.OEI.C_bOEI_in;
Lift.OEI.Body.F_tanHb        = ModelResult.LiftModel.SubModel_Body.OEI.F_tanHb;
Lift.OEI.Body.K_Switch       = ModelResult.LiftModel.SubModel_Body.OEI.K_Switch;
Lift.OEI.Body.F_Rel_far      = ModelResult.LiftModel.SubModel_Body.OEI.F_Rel_far;
Lift.OEI.Body.beta_vec(3)    = ModelResult.LiftModel.SubModel_Body.OEI.beta_vec(3);
Lift.OEI.Body.C_bOEI_far     = ModelResult.LiftModel.SubModel_Body.OEI.C_bOEI_far;

Lift.OEI.Body.C_TOEI_in      = ModelResult.LiftModel.SubModel_Body.OEI.C_TOEI_in;
Lift.OEI.Body.T_Vec(2)       = ModelResult.LiftModel.SubModel_Body.OEI.T_Vec(2);
Lift.OEI.Body.F_tanHT        = ModelResult.LiftModel.SubModel_Body.OEI.F_tanHT;
Lift.OEI.Body.C_TOEI_out     = ModelResult.LiftModel.SubModel_Body.OEI.C_TOEI_out;

%Lift HTP
Lift.OEI.HTP.C_bOEI_in      = ModelResult.LiftModel.SubModel_HTP.OEI.C_bOEI_in;
Lift.OEI.HTP.F_tanHb        = ModelResult.LiftModel.SubModel_HTP.OEI.F_tanHb;
Lift.OEI.HTP.K_Switch       = ModelResult.LiftModel.SubModel_HTP.OEI.K_Switch;
Lift.OEI.HTP.F_Rel_far      = ModelResult.LiftModel.SubModel_HTP.OEI.F_Rel_far;
Lift.OEI.HTP.beta_vec(3)    = ModelResult.LiftModel.SubModel_HTP.OEI.beta_vec(3);
Lift.OEI.HTP.C_bOEI_far     = ModelResult.LiftModel.SubModel_HTP.OEI.C_bOEI_far;

Lift.OEI.HTP.C_TOEI_in      = ModelResult.LiftModel.SubModel_HTP.OEI.C_TOEI_in;
Lift.OEI.HTP.T_Vec(2)       = ModelResult.LiftModel.SubModel_HTP.OEI.T_Vec(2);
Lift.OEI.HTP.F_tanHT        = ModelResult.LiftModel.SubModel_HTP.OEI.F_tanHT;
Lift.OEI.HTP.C_TOEI_out     = ModelResult.LiftModel.SubModel_HTP.OEI.C_TOEI_out;

%Lift VTP
Lift.OEI.VTP.C_bOEI_in      = ModelResult.LiftModel.SubModel_VTP.OEI.C_bOEI_in;
Lift.OEI.VTP.F_tanHb        = ModelResult.LiftModel.SubModel_VTP.OEI.F_tanHb;
Lift.OEI.VTP.K_Switch       = ModelResult.LiftModel.SubModel_VTP.OEI.K_Switch;
Lift.OEI.VTP.F_Rel_far      = ModelResult.LiftModel.SubModel_VTP.OEI.F_Rel_far;
Lift.OEI.VTP.beta_vec(3)    = ModelResult.LiftModel.SubModel_VTP.OEI.beta_vec(3);
Lift.OEI.VTP.C_bOEI_far     = ModelResult.LiftModel.SubModel_VTP.OEI.C_bOEI_far;

Lift.OEI.VTP.C_TOEI_in      = ModelResult.LiftModel.SubModel_VTP.OEI.C_TOEI_in;
Lift.OEI.VTP.T_Vec(2)       = ModelResult.LiftModel.SubModel_VTP.OEI.T_Vec(2);
Lift.OEI.VTP.F_tanHT        = ModelResult.LiftModel.SubModel_VTP.OEI.F_tanHT;
Lift.OEI.VTP.C_TOEI_out     = ModelResult.LiftModel.SubModel_VTP.OEI.C_TOEI_out;

%Lift Wing
Lift.OEI.Wing.C_bOEI_in      = ModelResult.LiftModel.SubModel_Wing.OEI.C_bOEI_in;
Lift.OEI.Wing.F_tanHb        = ModelResult.LiftModel.SubModel_Wing.OEI.F_tanHb;
Lift.OEI.Wing.K_Switch       = ModelResult.LiftModel.SubModel_Wing.OEI.K_Switch;
Lift.OEI.Wing.F_Rel_far      = ModelResult.LiftModel.SubModel_Wing.OEI.F_Rel_far;
Lift.OEI.Wing.beta_vec(3)    = ModelResult.LiftModel.SubModel_Wing.OEI.beta_vec(3);
Lift.OEI.Wing.C_bOEI_far     = ModelResult.LiftModel.SubModel_Wing.OEI.C_bOEI_far;

Lift.OEI.Wing.C_TOEI_in      = ModelResult.LiftModel.SubModel_Wing.OEI.C_TOEI_in;
Lift.OEI.Wing.T_Vec(2)       = ModelResult.LiftModel.SubModel_Wing.OEI.T_Vec(2);
Lift.OEI.Wing.F_tanHT        = ModelResult.LiftModel.SubModel_Wing.OEI.F_tanHT;
Lift.OEI.Wing.C_TOEI_out     = ModelResult.LiftModel.SubModel_Wing.OEI.C_TOEI_out;
end