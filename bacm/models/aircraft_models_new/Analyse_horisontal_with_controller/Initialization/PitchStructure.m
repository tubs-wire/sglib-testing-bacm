function Pitch = PitchStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, ModelResult, Longitudinal_Parameters)
% 
% 
%% Uncertainty Matrix
Pitch.T_Uncert        = ModelResult.PitchModel.SubModel_Wing.T_Vec(1:3);
Pitch.beta_Uncert     = [0 5 10]./180*pi;
Pitch.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
Pitch.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
Pitch.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
Pitch.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
Pitch.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
Pitch.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 20% 
Pitch.Uncertainty_LT = [    1                       1                       1                     ;...
                            Pitch.Uncert_b05T0      Pitch.Uncert_b05T1      Pitch.Uncert_b05T2    ;...
                            Pitch.Uncert_b10T0      Pitch.Uncert_b10T1      Pitch.Uncert_b10T2   ];

%% Derivatives
Pitch.C_m0          = AeroCoefficientsNL.Cm0;   
Pitch.C_maFR        = AeroCoefficientsNL.Cma ;   
Pitch.C_mqFR        = AeroCoefficientsNL.CmqFR ; 
Pitch.C_mqFRdRD     = 0;
Pitch.C_mqFRdLG     = 0; 
Pitch.dC_mdFL       = [0    MidFlap.dCm0        Longitudinal_Parameters.T0.Cmu03.Cm.Cm0  ];
Pitch.dC_madFL      = [0    MidFlap.dCma        Longitudinal_Parameters.T0.Cmu03.Cm.Cma ];
Pitch.dC_ma2dCmuBLC = [0    MidFlap.Cmu.Cm.k1   0];
Pitch.dC_ma2dCmuCC  = [0    MidFlap.Cmu.Cm.k1   0];

Pitch.dC_mdCmu_30    =  [(-0.05 * MidFlap.Cmu.Cm.k3)  0 (0.05 * MidFlap.Cmu.Cm.k3)];
Pitch.dC_madCmu_30   =  [(-0.05 * MidFlap.Cmu.Cm.k2)  0 (0.05 * MidFlap.Cmu.Cm.k2)];
Pitch.dC_mdCmu_65    =  Longitudinal_Parameters.LT_0_Cm_Cmu;
Pitch.dC_madCmu_65   =  Longitudinal_Parameters.LT_al_Cm_Cmu;

Pitch.dC_mdSB       = [0 0 0]; 
Pitch.dC_mdRD       = 0;
Pitch.dC_m_dh       = [0 0 0];

% Lateral Motion Influences on Pitch %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Pitch.C_mab             = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmab ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmab ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmab]*0;   
Pitch.C_mb2             = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmb2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmb2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmb2]*0;   
Pitch.C_mr2              = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmr2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmr2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmr2]*0;
Pitch.C_mp2             = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmp2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmp2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmp2]*0;
Pitch.C_mpb2            = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmpb2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmpb2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmpb2]*0;
Pitch.C_mzeta2          = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmzeta2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmzeta2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmzeta2]*0;
Pitch.C_mksis2          = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmxis2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmxis2 ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmxis2]*0;
Pitch.C_mksi            = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmxia ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmxia ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmxia]*0;
Pitch.C_mksib           = [     Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cm.Cmxiab ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cm.Cmxiab ...
                                Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cm.Cmxiab]*0;

Pitch.CrossCouplFlag = 0;

%% Thrust/Slipstream Influence
Pitch.Thrust.Body.k_T    = ModelResult.PitchModel.SubModel_Body.AEO.K_T;
Pitch.Thrust.Body.F_T    = ModelResult.PitchModel.SubModel_Body.AEO.F_tanh_T;
Pitch.Thrust.Body.dCm_T  = ModelResult.PitchModel.SubModel_Body.AEO.C_dT  ;
Pitch.Thrust.Body.dCma_T = ModelResult.PitchModel.SubModel_Body.AEO.dC_dadT;

Pitch.Thrust.Wing.k_T    = ModelResult.PitchModel.SubModel_Wing.AEO.K_T;
Pitch.Thrust.Wing.F_T    = ModelResult.PitchModel.SubModel_Wing.AEO.F_tanh_T;
Pitch.Thrust.Wing.dCm_T  = ModelResult.PitchModel.SubModel_Wing.AEO.C_dT  ;
Pitch.Thrust.Wing.dCma_T = ModelResult.PitchModel.SubModel_Wing.AEO.dC_dadT;

Pitch.Thrust.VTP.k_T     = ModelResult.PitchModel.SubModel_VTP.AEO.K_T;
Pitch.Thrust.VTP.F_T     = ModelResult.PitchModel.SubModel_VTP.AEO.F_tanh_T;
Pitch.Thrust.VTP.dCm_T   = ModelResult.PitchModel.SubModel_VTP.AEO.C_dT  ;
Pitch.Thrust.VTP.dCma_T  = ModelResult.PitchModel.SubModel_VTP.AEO.dC_dadT;

Pitch.Thrust.HTP.k_T     = ModelResult.PitchModel.SubModel_HTP.AEO.K_T;
Pitch.Thrust.HTP.F_T     = ModelResult.PitchModel.SubModel_HTP.AEO.F_tanh_T;
Pitch.Thrust.HTP.dCm_T   = ModelResult.PitchModel.SubModel_HTP.AEO.C_dT  ;
Pitch.Thrust.HTP.dCma_T  = ModelResult.PitchModel.SubModel_HTP.AEO.dC_dadT;

Pitch.Thrust.Full.k_T    = ModelResult.PitchModel.SubModel_Full.AEO.K_T;
Pitch.Thrust.Full.F_T    = ModelResult.PitchModel.SubModel_Full.AEO.F_tanh_T;
Pitch.Thrust.Full.dCm_T  = ModelResult.PitchModel.SubModel_Full.AEO.C_dT  ;
Pitch.Thrust.Full.dCma_T = ModelResult.PitchModel.SubModel_Full.AEO.dC_dadT;

Pitch.Thrust.Simple.T_Vec         = Longitudinal_Parameters.T_Vec(1:3);
Pitch.Thrust.Simple.dCmdT         = Longitudinal_Parameters.LT_0_Cm_T(1:3);
Pitch.Thrust.Simple.dCmadT        = Longitudinal_Parameters.LT_al_Cm_T(1:3);

%% Jet Momentum and Thrust/Slipstream Influences on Stall
Pitch.T_Vec          = Longitudinal_Parameters.T_Vec(1:3);

Pitch.f_T0_Cmu03     = Longitudinal_Parameters.T0.Cmu03.Cm.F_Tanh;
Pitch.a1_T0_Cmu03    = Longitudinal_Parameters.T0.Cmu03.Cm.k;
Pitch.astar_T0_Cmu03 = Longitudinal_Parameters.T0.Cmu03.Cm.dalpha;

Pitch.f_LT_Cmu       = Longitudinal_Parameters.Stall.LT_F_Cm_Cmu;                                   % a1 : static stall characteristics of the airfoil
Pitch.f_LT_T         = Longitudinal_Parameters.Stall.LT_F_Cm_T(1:3);                                   % a1 : static stall characteristics of the airfoil
Pitch.a1_LT_Cmu      = Longitudinal_Parameters.Stall.LT_k_Cm_Cmu;                                   % a1 : static stall characteristics of the airfoil
Pitch.a1_LT_T        = Longitudinal_Parameters.Stall.LT_k_Cm_T(1:3);                                   % a1 : static stall characteristics of the airfoil
Pitch.astar_LT_Cmu   = Longitudinal_Parameters.Stall.LT_da_Cm_Cmu;                                   % a1 : static stall characteristics of the airfoil
Pitch.astar_LT_T     = Longitudinal_Parameters.Stall.LT_da_Cm_T(1:3);                                   % a1 : static stall characteristics of the airfoil

% Mid Flaps
Pitch.MidFlap.f1            = MidFlap.Stall.Cm.f1;                                   % a1 : static stall characteristics of the airfoil
Pitch.MidFlap.a1            = MidFlap.Stall.Cm.a1;                                 % time constant coefficient 
Pitch.MidFlap.astar         = MidFlap.Stall.Cm.aStar;                          % alpha star [rad]

% Sideslip Pitch Parameters
Pitch.Beta_LT                       = LateralSlipstream.Derivatives.New_Model.beta_LT / 180*pi;
Pitch.T_Ref                         = LateralSlipstream.Derivatives.New_Model.T_ref;
Pitch.Cmu_Ref                       = LateralSlipstream.Derivatives.New_Model.Cmu_ref;

Pitch.Fuselage.LT_C_m_T0_T1         = LateralSlipstream.Derivatives.New_Model.Cm_Fuse_LT_T0T1_Cmu0;
Pitch.Fuselage.LT_C_m_T1_Cmu0_Cmu1  = LateralSlipstream.Derivatives.New_Model.Cm_Fuse_LT_T1_Cmu0Cmu03;


%% New Siedslip Parameters
Pitch.Sideslip.k_2 = 3;

Pitch.Sideslip.Wing.T_Vec       = ModelResult.PitchModel.SubModel_Wing.T_Vec;
Pitch.Sideslip.Wing.alpha_Vec   = ModelResult.PitchModel.SubModel_Wing.alpha_Vec;
Pitch.Sideslip.Wing.Cb_lookup   = ModelResult.PitchModel.SubModel_Wing.Cb_lookup;
Pitch.Sideslip.Wing.F_lookup    = ModelResult.PitchModel.SubModel_Wing.F_lookup;
Pitch.Sideslip.Wing.K_lookup    = ModelResult.PitchModel.SubModel_Wing.K_lookup;

Pitch.Sideslip.Body.T_Vec       = ModelResult.PitchModel.SubModel_Body.T_Vec;
Pitch.Sideslip.Body.alpha_Vec   = ModelResult.PitchModel.SubModel_Body.alpha_Vec;
Pitch.Sideslip.Body.Cb_lookup   = ModelResult.PitchModel.SubModel_Body.Cb_lookup;
Pitch.Sideslip.Body.F_lookup    = ModelResult.PitchModel.SubModel_Body.F_lookup;
Pitch.Sideslip.Body.K_lookup    = ModelResult.PitchModel.SubModel_Body.K_lookup;

Pitch.Sideslip.HTP.T_Vec       = ModelResult.PitchModel.SubModel_HTP.T_Vec;
Pitch.Sideslip.HTP.alpha_Vec   = ModelResult.PitchModel.SubModel_HTP.alpha_Vec;
Pitch.Sideslip.HTP.Cb_lookup   = ModelResult.PitchModel.SubModel_HTP.Cb_lookup;
Pitch.Sideslip.HTP.F_lookup    = ModelResult.PitchModel.SubModel_HTP.F_lookup;
Pitch.Sideslip.HTP.K_lookup    = ModelResult.PitchModel.SubModel_HTP.K_lookup;

Pitch.Sideslip.VTP.T_Vec       = ModelResult.PitchModel.SubModel_VTP.T_Vec;
Pitch.Sideslip.VTP.alpha_Vec   = ModelResult.PitchModel.SubModel_VTP.alpha_Vec;
Pitch.Sideslip.VTP.Cb_lookup   = ModelResult.PitchModel.SubModel_VTP.Cb_lookup;
Pitch.Sideslip.VTP.F_lookup    = ModelResult.PitchModel.SubModel_VTP.F_lookup;
Pitch.Sideslip.VTP.K_lookup    = ModelResult.PitchModel.SubModel_VTP.K_lookup;

%% OEI Parameters
%Pitch Full
Pitch.OEI.Full.C_bOEI_in      = ModelResult.PitchModel.SubModel_Full.OEI.C_bOEI_in;
Pitch.OEI.Full.F_tanHb        = ModelResult.PitchModel.SubModel_Full.OEI.F_tanHb;
Pitch.OEI.Full.K_Switch       = ModelResult.PitchModel.SubModel_Full.OEI.K_Switch;
Pitch.OEI.Full.F_Rel_far      = ModelResult.PitchModel.SubModel_Full.OEI.F_Rel_far;
Pitch.OEI.Full.beta_vec(3)    = ModelResult.PitchModel.SubModel_Full.OEI.beta_vec(3);
Pitch.OEI.Full.C_bOEI_far     = ModelResult.PitchModel.SubModel_Full.OEI.C_bOEI_far;

Pitch.OEI.Full.C_TOEI_in      = ModelResult.PitchModel.SubModel_Full.OEI.C_TOEI_in;
Pitch.OEI.Full.T_Vec(2)       = ModelResult.PitchModel.SubModel_Full.OEI.T_Vec(2);
Pitch.OEI.Full.F_tanHT        = ModelResult.PitchModel.SubModel_Full.OEI.F_tanHT;
Pitch.OEI.Full.C_TOEI_out     = ModelResult.PitchModel.SubModel_Full.OEI.C_TOEI_out;

%Pitch Body
Pitch.OEI.Body.C_bOEI_in      = ModelResult.PitchModel.SubModel_Body.OEI.C_bOEI_in;
Pitch.OEI.Body.F_tanHb        = ModelResult.PitchModel.SubModel_Body.OEI.F_tanHb;
Pitch.OEI.Body.K_Switch       = ModelResult.PitchModel.SubModel_Body.OEI.K_Switch;
Pitch.OEI.Body.F_Rel_far      = ModelResult.PitchModel.SubModel_Body.OEI.F_Rel_far;
Pitch.OEI.Body.beta_vec(3)    = ModelResult.PitchModel.SubModel_Body.OEI.beta_vec(3);
Pitch.OEI.Body.C_bOEI_far     = ModelResult.PitchModel.SubModel_Body.OEI.C_bOEI_far;

Pitch.OEI.Body.C_TOEI_in      = ModelResult.PitchModel.SubModel_Body.OEI.C_TOEI_in;
Pitch.OEI.Body.T_Vec(2)       = ModelResult.PitchModel.SubModel_Body.OEI.T_Vec(2);
Pitch.OEI.Body.F_tanHT        = ModelResult.PitchModel.SubModel_Body.OEI.F_tanHT;
Pitch.OEI.Body.C_TOEI_out     = ModelResult.PitchModel.SubModel_Body.OEI.C_TOEI_out;

%Pitch HTP
Pitch.OEI.HTP.C_bOEI_in      = ModelResult.PitchModel.SubModel_HTP.OEI.C_bOEI_in;
Pitch.OEI.HTP.F_tanHb        = ModelResult.PitchModel.SubModel_HTP.OEI.F_tanHb;
Pitch.OEI.HTP.K_Switch       = ModelResult.PitchModel.SubModel_HTP.OEI.K_Switch;
Pitch.OEI.HTP.F_Rel_far      = ModelResult.PitchModel.SubModel_HTP.OEI.F_Rel_far;
Pitch.OEI.HTP.beta_vec(3)    = ModelResult.PitchModel.SubModel_HTP.OEI.beta_vec(3);
Pitch.OEI.HTP.C_bOEI_far     = ModelResult.PitchModel.SubModel_HTP.OEI.C_bOEI_far;

Pitch.OEI.HTP.C_TOEI_in      = ModelResult.PitchModel.SubModel_HTP.OEI.C_TOEI_in;
Pitch.OEI.HTP.T_Vec(2)       = ModelResult.PitchModel.SubModel_HTP.OEI.T_Vec(2);
Pitch.OEI.HTP.F_tanHT        = ModelResult.PitchModel.SubModel_HTP.OEI.F_tanHT;
Pitch.OEI.HTP.C_TOEI_out     = ModelResult.PitchModel.SubModel_HTP.OEI.C_TOEI_out;

%Pitch VTP
Pitch.OEI.VTP.C_bOEI_in      = ModelResult.PitchModel.SubModel_VTP.OEI.C_bOEI_in;
Pitch.OEI.VTP.F_tanHb        = ModelResult.PitchModel.SubModel_VTP.OEI.F_tanHb;
Pitch.OEI.VTP.K_Switch       = ModelResult.PitchModel.SubModel_VTP.OEI.K_Switch;
Pitch.OEI.VTP.F_Rel_far      = ModelResult.PitchModel.SubModel_VTP.OEI.F_Rel_far;
Pitch.OEI.VTP.beta_vec(3)    = ModelResult.PitchModel.SubModel_VTP.OEI.beta_vec(3);
Pitch.OEI.VTP.C_bOEI_far     = ModelResult.PitchModel.SubModel_VTP.OEI.C_bOEI_far;

Pitch.OEI.VTP.C_TOEI_in      = ModelResult.PitchModel.SubModel_VTP.OEI.C_TOEI_in;
Pitch.OEI.VTP.T_Vec(2)       = ModelResult.PitchModel.SubModel_VTP.OEI.T_Vec(2);
Pitch.OEI.VTP.F_tanHT        = ModelResult.PitchModel.SubModel_VTP.OEI.F_tanHT;
Pitch.OEI.VTP.C_TOEI_out     = ModelResult.PitchModel.SubModel_VTP.OEI.C_TOEI_out;

%Pitch Wing
Pitch.OEI.Wing.C_bOEI_in      = ModelResult.PitchModel.SubModel_Wing.OEI.C_bOEI_in;
Pitch.OEI.Wing.F_tanHb        = ModelResult.PitchModel.SubModel_Wing.OEI.F_tanHb;
Pitch.OEI.Wing.K_Switch       = ModelResult.PitchModel.SubModel_Wing.OEI.K_Switch;
Pitch.OEI.Wing.F_Rel_far      = ModelResult.PitchModel.SubModel_Wing.OEI.F_Rel_far;
Pitch.OEI.Wing.beta_vec(3)    = ModelResult.PitchModel.SubModel_Wing.OEI.beta_vec(3);
Pitch.OEI.Wing.C_bOEI_far     = ModelResult.PitchModel.SubModel_Wing.OEI.C_bOEI_far;

Pitch.OEI.Wing.C_TOEI_in      = ModelResult.PitchModel.SubModel_Wing.OEI.C_TOEI_in;
Pitch.OEI.Wing.T_Vec(2)       = ModelResult.PitchModel.SubModel_Wing.OEI.T_Vec(2);
Pitch.OEI.Wing.F_tanHT        = ModelResult.PitchModel.SubModel_Wing.OEI.F_tanHT;
Pitch.OEI.Wing.C_TOEI_out     = ModelResult.PitchModel.SubModel_Wing.OEI.C_TOEI_out;
end