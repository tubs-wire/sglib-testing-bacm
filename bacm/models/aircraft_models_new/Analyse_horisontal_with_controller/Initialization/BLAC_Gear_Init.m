%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum für Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Gear_Init                                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the worspace variables for the gear subsystem.                               *
%*                                                                                                    *
%* Version  : 2.5                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Gear_Init                                                                            *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Dennis Vechtel               * 17-Nov-08 * Adapted to A320                                         *
%* Dennis Vechtel               * 23-Feb-09 * Adapted to new RP                                       *
%* Dennis Vechtel               * 12-Mar-09 * Renamed from A320 to BLAC                               *
%* Christian Raab               * 26-May-10 * Changed LG positons accrd. to new RP                    *
%* Dietrich Fischenberg         * 17-April-13 * New OLEO-damping, new OLEO-spring                     *
%******************************************************************************************************

function Gear = BLAC_Gear_Init

% % ATRA DATA
% 
% Geometry.MGR.x = -20.2525;                  % Main landing gear RH position x-coord.                [m]
% Geometry.MGR.y = 3.795;                     % Main landing gear RH position y-coord.                [m]
% Geometry.MGR.z = 4.331;                     % Main landing gear touch point RH position z-coord.    [m]
%                                             % uncompressed
% 
% Geometry.MGL.x = -20.2525;                  % Main landing gear LH position x-coord.                [m]
% Geometry.MGL.y = -3.795;                    % Main landing gear LH position y-coord.                [m]
% Geometry.MGL.z = 4.331;                     % Main landing gear touch point LH position z-coord     [m]
%                                             % uncompressed
% 
% 
% Geometry.NG.x = -7.6130;                    % Nose landing gear position x-coord.                   [m]
% Geometry.NG.y = 0;                          % Nose landing gear position y-coord.                   [m]
% Geometry.NG.z = 4.331;                      % Nose landing gear touch point position z-coord.       [m]
%                                             % uncompressed

% SFB DATA

Geometry.MGR.x = -14.569;                   % Main landing gear RH position x-coord.                [m]
Geometry.MGR.y = 1.93;                      % Main landing gear RH position y-coord.                [m]
Geometry.MGR.z = 3.187;                     % Main landing gear touch point RH position z-coord.    [m]
                                            % uncompressed
                                            
Geometry.MGL.x = -14.569;                   % Main landing gear LH position x-coord.                [m]
Geometry.MGL.y = -1.93;                     % Main landing gear LH position y-coord.                [m]
Geometry.MGL.z = 3.187;                     % Main landing gear touch point LH position z-coord     [m]
                                            % uncompressed
                                            
Geometry.NG.x = -2.997;                     % Nose landing gear position x-coord.                   [m]
Geometry.NG.y = 0;                          % Nose landing gear position y-coord.                   [m]
Geometry.NG.z = 3.187;                      % Nose landing gear touch point position z-coord.       [m]
                                            % uncompressed
% Runway condition
Data.RWY      = 1;                          % 1 = Dry, 2 = Wet, 3 = Flooded, 4 = Icy

%Data
%Data.VC       = 0.3;                        % Umschaltgeschwindigkeit zwischen linearer
Data.VC       = 0.8;                        % Umschaltgeschwindigkeit zwischen linearer
%                                           % und quadratischer Oleo-Dämpfung

% Rescaling for SFB 880 Aircraft
Factor_Damp_MG_SFB   = (40/77) * 1.4; % 40/77; % MTOW_SFB/MTOW_ATRA [t]/[t]
Factor_Damp_NG_SFB   = (40/77) * 1.2; % 40/77; % MTOW_SFB/MTOW_ATRA [t]/[t]
Factor_Spring_MG_SFB = (40/77) * 1.4; % 40/77; % MTOW_SFB/MTOW_ATRA [t]/[t]
Factor_Spring_NG_SFB = (40/77) * 1.2; % 40/77; % MTOW_SFB/MTOW_ATRA [t]/[t]

% Oleo+tire Nose Gear
Data.DK1_NG     =   50000 * Factor_Damp_NG_SFB;                  % new 17.4.2013 (Fi): Dämpferkonstante Oleo Nosegear (Kompression)   [N/m/s)^2]
Data.DK2_NG     = 10 * Data.DK1_NG * Factor_Damp_NG_SFB;         % new 17.4.2013 (Fi): Dämpferkonstante Oleo Nosegear (De-Kompression)[N/m/s)^2]
Data.fmaxOleoNG = 0.200;                    % SFB maximale Einfederung NG Oleo                          [m] 
% Data.fmaxOleoNG = 0.43;                   % ATRA maximale Einfederung NG Oleo                          [m] 
Data.pTireNG    = 178;                      % Reifendruck                                         [psi]
% Data.rTireNG    = 30 * 0.0254 * 0.5;      % ATRA Radius des NG Reifens                                 [m]
Data.rTireNG    = 0.2795;                   % SFB  Radius des NG Reifens                                 [m]

% Oleo+tire Main Gear
Data.DK1_MG     =  70000 * Factor_Damp_MG_SFB;                   % new 17.4.2013 (Fi): Dämpferkonstante Oleo MG (Kompression)         [N/m/s)^2]
Data.DK2_MG     = 10 * Data.DK1_MG * Factor_Damp_MG_SFB;         % new 17.4.2013 (Fi): Dämpferkonstante Oleo MG (De-Kompression)      [N/m/s)^2]
Data.fmaxOleoMG = 0.40;                     %  SFB maximale Einfederung MG Oleo                          [m] 
% Data.fmaxOleoMG = 0.47;                   % ATRA maximale Einfederung MG Oleo                          [m] 
Data.pTireMG    = 175;                      % Reifendruck                                         [psi]
% Data.rTireMG    = 46 * 0.0254 * 0.5;      % ATRA Radius des MG Reifens                                 [m]
Data.rTireMG    = 0.509;                   % SFB  Radius des MG Reifens                                 [m]


% Oleo Data
 load BLAC_OLEO_Data;
 scal = [0 : 1/20 :1];                    % new 17.4.2013 (Fi)
 Data.NG.deflection  = NG_stroke * 0.20/0.43; % SFB/ATRA NG Stroke length
 Data.NG.load        = scal .* NG_F_load  * Factor_Spring_NG_SFB; % new 17.4.2013 (Fi)

 Data.MG.deflection  = MG_stroke * 0.40/0.47; % SFB/ATRA MG Stroke length
 Data.MG.load        = scal .* MG_F_load  * Factor_Spring_MG_SFB; % new 17.4.2013 (Fi)

%**************************************************************************
% Gear Failures (defaults)

Failure.No_X_Force         = 0;    % Gear X-Forces enable/disable - [0]/[1]
Failure.No_Y_Force         = 0;    % Gear Y-Forces enable/disable - [0]/[1]
Failure.Gearbrake_Time     = 0;    % Gearbrake Failure Time             [s]
Failure.Gearbrake_Initial  = 0;    % Initial (no)/failure: (0)/1        [-]
Failure.Gearbrake_Final    = 0;    % Final                              [-]
Failure.Retraction_Time    = 0;    % Retraction-Mechanism-Failure Time  [s] 
Failure.Retraction_Initial = 0;    % Initial (no)/failure: (0)/1        [-] 
Failure.Retraction_Final   = 0;    % Final                              [-]
Failure.Steerangle_Time    = 0;    % Steerangle-Failure Time            [s] 
Failure.Steerangle_Value   = 0*pi/180;% Steerangle-Failure Value        [rad] 
Failure.Steerangle_active  = 0;    % Steerangle_Failure active          [-]    

% Assembly of Data Variables
Gear.Failure  = Failure;
Gear.Geometry = Geometry;
Gear.Data     = Data;

end
%-------------------------------------------------------------------------------------------------------------------EOF