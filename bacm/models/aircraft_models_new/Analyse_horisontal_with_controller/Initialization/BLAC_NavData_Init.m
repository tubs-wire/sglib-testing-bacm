%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2010      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_NavData_Init                                             *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the workspace variables for the Navigation Database Block                    *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_NavData_Init                                                                         *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 09-JUL-10 * Basic Design                                            *
%******************************************************************************************************
function NavData = BLAC_NavData_Init

% Distance Measuring Equipment DME
DME1.lat    = 0;                                   % DME1 position latitude                       [rad]
DME1.long   = 0;                                   % DME1 position longitude                      [rad]
DME1.alt    = 0;                                   % DME1 position altitude                       [m]
DME1.active = 0;                                   % DME1 is tuned and active                     [-]

DME2.lat    = 0;                                   % DME2 position latitude                       [rad]
DME2.long   = 0;                                   % DME2 position longitude                      [rad]
DME2.alt    = 0;                                   % DME2 position altitude                       [m]
DME2.active = 0;                                   % DME2 is tuned and active                     [-]

% Non-Directional Beacon NDB
NDB1.lat    = 0;                                   % NDB1 position latitude                       [rad]
NDB1.long   = 0;                                   % NDB1 position longitude                      [rad]
NDB1.alt    = 0;                                   % NDB1 position altitude                       [m]
NDB1.active = 0;                                   % NDB1 is tuned and active                     [-]

NDB2.lat    = 0;                                   % NDB2 position latitude                       [rad]
NDB2.long   = 0;                                   % NDB2 position longitude                      [rad]
NDB2.alt    = 0;                                   % NDB2 position altitude                       [m]
NDB2.active = 0;                                   % NDB2 is tuned and active                     [-]

% Very High Frequency Omnidirectional Radio Range VOR
VOR1.lat    = 0;                                   % VOR1 position latitude                       [rad]
VOR1.long   = 0;                                   % VOR1 position longitude                      [rad]
VOR1.alt    = 0;                                   % VOR1 position altitude                       [m]
VOR1.active = 0;                                   % VOR1 is tuned and active                     [-]

VOR2.lat    = 0;                                   % VOR2 position latitude                       [rad]
VOR2.long   = 0;                                   % VOR2 position longitude                      [rad]
VOR2.alt    = 0;                                   % VOR2 position altitude                       [m]
VOR2.active = 0;                                   % VOR2 is tuned and active                     [-]

% Assemble environment structure
NavData.DME1 = DME1;
NavData.DME2 = DME2;
NavData.NDB1 = NDB1;
NavData.NDB2 = NDB2;
NavData.VOR1 = VOR1;
NavData.VOR2 = VOR2;
%---------------------------------------------------------------------------------------------------EOF