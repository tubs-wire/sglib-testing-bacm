%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Aerodynamics_Init                                        *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize Tail Bumper Parameters.                                                      *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_TailBumper_Init                                                                      *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************
function Tail_Bumper = BLAC_TailBumper_Init

Tail_Bumper.on_off    = 1;           % Tail bumper on[1] off[0]                                     [-]
Tail_Bumper.ff_on_off = 1;           % Tail bumper friction on[1] off[0]                            [-]
Tail_Bumper.kK_ground = 10000000;    % Tail bumper spring constant                                [N/m]
Tail_Bumper.bK_ground = 10000;       % Tail bumper damping                                       [Ns/m]
Tail_Bumper.mue       = 0.8;         % Tail bumper friction coefficient                             [-]

% Tailbumper Position in Reference Frame
Tail_Bumper.x_rp = -23.887;          % Tail bumper x-coord. in reference frame                      [m]
Tail_Bumper.y_rp = 0;                % Tail bumper y-coord. in reference frame                      [m]
Tail_Bumper.z_rp = 1; %2.2;              % Tail bumper z-coord. in reference frame                      [m]
%---------------------------------------------------------------------------------------------------EOF