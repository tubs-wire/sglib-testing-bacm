%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Aerodynamics_Init                                      *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize the look-up tables for the aerodynamic coefficients of the BLAC A/C model. *
%*            A template file is replaced by the values generated with DATCOM.                        *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : Aircraft Name: BLAC_Konfig_28_2_2012                                                      *
%*                                                                                                    *
%*            file created : 2012-02-28 time: 15:19:21                                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Aerodynamics_Init                                                                  *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%* Andre Thurm                  * 23-NOV-10 * generated template files                                *
%* Jobst Henning Diekmann       * 22-Mar-16 * SFB 880 REF-2 Aerodynamic Update                        *
%******************************************************************************************************
function Aero = BLAC_Aerodynamics_Init
%% Aerodynamic Database
load AeroCoefficientsNLv01.mat
load AeroCoefficientsMidFlap.mat
load SystemFailure.mat
load Prop_Slip_Aero.mat
load Lateral_Motion.mat
% load DroopNose_3D-CFD.mat
load DownwashCoeff2015.mat
load Lateral_Slipstream.mat
load RollingDerivatives.mat
load REF-2_Aerodata.mat
Longitudinal_Parameters = Model;
load ModelLateralMotion.mat
Sideslip_OEI_Parameters = ModelResult;

%% Config- and VTP-Cases for Lateral Motion
VTP = 1;
switch VTP
    case 1
        VerticalTailSize = 'VTP_large';
    case 2
        VerticalTailSize = 'VTP_small';
end

Config       = {'Cruise' ; 'Intermediate' ; 'FullFlap'};

%% Unsteady Aerodynamics Flag
Unsteady_Flag = 1;                                  % ON = 0 / OFF = 1

%% Wind Tunnel Paramters 
Wind_Tunnel_Ma = 0.15;                                                  % Prandtl-Glauert   [-]

%% Flaps Setting
Flaps.Setting = [0  30  65] * pi/180;    

%% Boost Control Derivatives
ControlBoost = 1;


%% Lookup Table Inputs
Inputs.alpha        = Lateral_Coefficients.(VerticalTailSize).(Config{1}).alpha;
Inputs.beta         = Lateral_Coefficients.(VerticalTailSize).(Config{1}).beta;
Inputs.p_star       = Lateral_Coefficients.(VerticalTailSize).(Config{1}).p_star;
Inputs.r_star       = Lateral_Coefficients.(VerticalTailSize).(Config{1}).r_star;
Inputs.zeta         = Lateral_Coefficients.(VerticalTailSize).(Config{1}).zeta;
Inputs.xis          = Lateral_Coefficients.(VerticalTailSize).(Config{1}).xis;
Inputs.xia          = Lateral_Coefficients.(VerticalTailSize).(Config{1}).xia;
Inputs.Lat_Motion   = 1;                                                % Lateral Motion on/off [1/0]

%% Lift Parameters
Lift = LiftStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, Sideslip_OEI_Parameters, Longitudinal_Parameters);

%% Drag Parameters
Drag = DragStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, Sideslip_OEI_Parameters, Longitudinal_Parameters);

%% Pitch Parameters 
Pitch = PitchStructure(AeroCoefficientsNL, MidFlap, Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, Sideslip_OEI_Parameters, Longitudinal_Parameters);

%% Roll Parameters
Roll = RollStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, ControlBoost, Sideslip_OEI_Parameters);

%% Yaw Parameters  
Yaw = YawStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, ControlBoost, Sideslip_OEI_Parameters);

%% Side Force Parameters
Side = SideStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, Sideslip_OEI_Parameters);

%% Slipstream Effects on/off
% Datapoint Insertion
Longitudinal_Parameters.T_Vec = [Longitudinal_Parameters.T_Vec (2*83000)];
Longitudinal_Parameters.LT_0_CL_T = [Longitudinal_Parameters.LT_0_CL_T Longitudinal_Parameters.LT_0_CL_T(2)];
Longitudinal_Parameters.LT_0_Cm_T = [Longitudinal_Parameters.LT_0_Cm_T -0.2428];

Lift.Thrust.Simple.T_Vec        = Longitudinal_Parameters.T_Vec(1:3);
Lift.Thrust.Simple.T_VecL       = Longitudinal_Parameters.T_Vec([1:3 5]);       % Vec_Pos(4) is not reliable
Lift.Thrust.Simple.dCLdT        = Longitudinal_Parameters.LT_0_CL_T([1:3 5]);   % Vec_Pos(4) is not reliable
Lift.Thrust.Simple.dCLadT       = Longitudinal_Parameters.LT_al_CL_T(1:3);

Lift.T_Vec                      = Longitudinal_Parameters.T_Vec(1:2);

Lift.f_LT_T                     = Longitudinal_Parameters.Stall.LT_F_CL_T(1:2);                                   % a1 : static stall characteristics of the airfoil
Lift.a1_LT_T                    = Longitudinal_Parameters.Stall.LT_k_CL_T(1:2);                                   % a1 : static stall characteristics of the airfoil
Lift.astar_LT_T                 = Longitudinal_Parameters.Stall.LT_da_CL_T(1:2);                                   % a1 : static stall characteristics of the airfoil

Pitch.Thrust.Simple.T_Vec         = Longitudinal_Parameters.T_Vec(1:3);
Pitch.Thrust.Simple.T_Vecm        = Longitudinal_Parameters.T_Vec([1:3 5]);         % Vec_Pos(4) is not reliable
Pitch.Thrust.Simple.dCmdT         = Longitudinal_Parameters.LT_0_Cm_T([1:3 5]);     % Vec_Pos(4) is not reliable
Pitch.Thrust.Simple.dCmadT        = Longitudinal_Parameters.LT_al_Cm_T(1:3);

Pitch.T_Vec          = Longitudinal_Parameters.T_Vec(1:2);

Pitch.f_LT_T         = Longitudinal_Parameters.Stall.LT_F_Cm_T(1:2);                                   % a1 : static stall characteristics of the airfoil
Pitch.a1_LT_T        = Longitudinal_Parameters.Stall.LT_k_Cm_T(1:2);                                   % a1 : static stall characteristics of the airfoil
Pitch.astar_LT_T     = Longitudinal_Parameters.Stall.LT_da_Cm_T(1:2);                                   % a1 : static stall characteristics of the airfoil

Longitudinal_Parameters.LT_0_CD_T = [Longitudinal_Parameters.LT_0_CD_T  Longitudinal_Parameters.LT_0_CD_T(end)];
Longitudinal_Parameters.LT_k_CD_T = [Longitudinal_Parameters.LT_k_CD_T (Longitudinal_Parameters.LT_k_CD_T(end) * 1.12)];
Drag.T_Vec          = Longitudinal_Parameters.T_Vec([1:3 5]);
Drag.dC_DdT         = Longitudinal_Parameters.LT_0_CD_T([1:3 5]);
Drag.dk_CDdT        = Longitudinal_Parameters.LT_k_CD_T([1:3 5]);

Slipstream.On               = 1;
Slipstream.SaturationDCL    = Longitudinal_Parameters.T_Vec(5) * 2;
Slipstream.SaturationDCLa   = Longitudinal_Parameters.T_Vec(3) * 2;
Slipstream.SaturationDCLOEI = ModelResult.LiftModel.SubModel_Full.OEI.T_Vec(3);
Slipstream.SaturationDCm    = Longitudinal_Parameters.T_Vec(5) * 2;
Slipstream.SaturationDCma   = Longitudinal_Parameters.T_Vec(3) * 2;
Slipstream.SaturationDCmOEI = ModelResult.PitchModel.SubModel_Full.OEI.T_Vec(3);
Slipstream.SaturationDk     = Longitudinal_Parameters.T_Vec(5) * 2;
Slipstream.SaturationDCD0   = Longitudinal_Parameters.T_Vec(3) * 2;
Slipstream.SaturationDCDOEI = ModelResult.DragModel.SubModel_Full.OEI.T_Vec(3);

Slipstream.SaturationStall  = inf; % Longitudinal_Parameters.T_Vec(2) * 2; %Longitudinal_Parameters.T_Vec(3) * 2; % inf

%% One Engine Inoperative Lateral Aero
OEI.Active              = 0;
OEI.T_LT                = LateralSlipstream.Derivatives.OEI.T_Cmu03  * 0;
OEI.C_Y_T0              = LateralSlipstream.Derivatives.OEI.CY_T_0 * 0;
OEI.C_Y_LT_T_Cmu0_Cmu03 = LateralSlipstream.Derivatives.OEI.CY_LT_T_Cmu0_Cmu03 * 0;
OEI.C_l_T0              = LateralSlipstream.Derivatives.OEI.Cl_T_0 * 0;
OEI.C_l_LT_T_Cmu0_Cmu03 = LateralSlipstream.Derivatives.OEI.Cl_LT_T_Cmu0_Cmu03 * 0;
OEI.C_n_T0              = LateralSlipstream.Derivatives.OEI.Cn_T_0 * 0;
OEI.C_n_LT_T_Cmu0_Cmu03 = LateralSlipstream.Derivatives.OEI.Cn_LT_T_Cmu0_Cmu03 * 0;

%% New SideSlip & Slipstream Model Parameter
Eng_Side = LateralSlipstream;
Eng_Side.Model.CL.Body.dCL_T = Eng_Side.Model.CL.Body.dCL_T * 0.8 * 0;
Eng_Side.Model.CL.Wing.dCL_T = Eng_Side.Model.CL.Wing.dCL_T * 0.8 * 0;
Eng_Side.Model.CD.Body.dCD_T = Eng_Side.Model.CD.Body.dCD_T * 0;
Eng_Side.Model.CD.Wing.dCD_T = Eng_Side.Model.CD.Wing.dCD_T * 0;
Eng_Side.Model.Cm.Wing.dCm_T = Eng_Side.Model.Cm.Wing.dCm_T * 1.2 * 0;

Eng_Side.OEI.LT.Switch_LT    = 2;
     
Eng_Side.Prop_On_OFF         = 0;        
%% Downwash Parameters
Downwash.deps_H_dC_S    = 0;
Downwash.depsH_dX       = 0;
Downwash.epsCLFR0       = DownwashStruct.epsCAAl0           * pi/180; 
Downwash.depsdCLFR      = DownwashStruct.depsdCL            * pi/180; 
Downwash.deps_Fl        = [0 0 Longitudinal_Parameters.Downwash.dEps0_dFl]    * pi/180; 
Downwash.depsdCL_Fl     = [0 0 Longitudinal_Parameters.Downwash.dEpsdCL_dFl]  * pi/180;
Downwash.deps_BLC       = [0 0 0*DownwashStruct.dEps_BLC]     * pi/180;
Downwash.deps_CC        = [0 0 0*DownwashStruct.dEps_CC]      * pi/180; 
Downwash.depsdCL_BLC    = [0 0 0*DownwashStruct.dEpsdCL_BLC]  * pi/180; 
Downwash.depsdCL_CC     = [0 0 0*DownwashStruct.dEpsdCL_CC]   * pi/180;
Downwash.C_mu_Fl        = [0 0 DownwashStruct.cmu_ref]; 

%% Active HTP
Active_HTP.On               =   0;

Active_HTP.Setting          =  [0   30                  65] * pi/180;
Active_HTP.dC_LdFL          =  [0   MidFlap.dCL0        AeroCoefficientsNL.dCA0_Flaps ];
Active_HTP.dC_LadFL         =  [0   MidFlap.dCLa        AeroCoefficientsNL.dCAa_Flaps ];
Active_HTP.dC_LdCmu_BLC     =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.BLC.dCL_dCmu ];
Active_HTP.dC_LadCmu_BLC    =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.BLC.dCLa_dCmu ];
Active_HTP.dC_La2dCmu_BLC   =  [0   MidFlap.Cmu.CL.k1   0];
Active_HTP.dC_LdCmu_CC      =  [0   MidFlap.Cmu.CL.k3   AeroCoefficientsNL.CC.dCL_dCmu ];
Active_HTP.dC_LadCmu_CC     =  [0   MidFlap.Cmu.CL.k2   AeroCoefficientsNL.CC.dCLa_dCmu ];
Active_HTP.dC_La2dCmu_CC    =  [0   MidFlap.Cmu.CL.k1   0];

Active_HTP.Cmu_ref          =  [0   MidFlap.Cmu_ref     AeroCoefficientsNL.Cmu_ref];
Active_HTP.Performance      =  100; % HTP blowing performance in [%]

%% Engine Influence Parameters
Prop.On             = 0;
Prop.Nacelle        = 0;

% Prop_Factor         = [1.6 0 2 2 2 2];
% Prop_Factor         = [0 0 0 0 0 0];
Prop_Factor         = [0 0 0 0 0 0];

Prop.dCL_Nacelle    = Prop_Slip_Aero.FullFLap.dCL0_Nacelle  * Prop.Nacelle;
Prop.dCLa_Nacelle   = Prop_Slip_Aero.FullFLap.dCLa_Nacelle  * Prop.Nacelle * 180/pi * 0; %10;
Prop.dCD_Nacelle    = Prop_Slip_Aero.FullFLap.dCD0_Nacelle  * Prop.Nacelle;
Prop.dk_Nacelle     = Prop_Slip_Aero.FullFLap.dk_Nacelle    * Prop.Nacelle;
Prop.dCm_Nacelle    = Prop_Slip_Aero.FullFLap.dCm0_Nacelle  * Prop.Nacelle;
Prop.dCma_Nacelle   = Prop_Slip_Aero.FullFLap.dCma_Nacelle  * Prop.Nacelle * 180/pi;

Prop.dCL_dT         = [Prop_Slip_Aero.MediFLap.dCL_dT   Prop_Slip_Aero.FullFLap.dCL_dT]         * Prop_Factor(1);
Prop.dCLa_dT        = [Prop_Slip_Aero.MediFLap.dCLa_dT  0]                                      * Prop_Factor(2) * 180/pi; % Wertkorrektur aus REF2 Daten
Prop.dCD_dT         = [Prop_Slip_Aero.MediFLap.dCD_dT   Prop_Slip_Aero.FullFLap.dCD_dT]         * Prop_Factor(3);
Prop.dk_dT          = [Prop_Slip_Aero.MediFLap.dk_dT    (0.0132/ (2*40900)) * 2.6]              * Prop_Factor(4);
Prop.dCm_dT         = [Prop_Slip_Aero.MediFLap.dCm_dT   Prop_Slip_Aero.FullFLap.dCm_dT]         * Prop_Factor(5);
Prop.dCma_dT        = [Prop_Slip_Aero.MediFLap.dCma_dT  Prop_Slip_Aero.FullFLap.dCma_dT * 7]    * Prop_Factor(6) * 180/pi;

Engine.C_S_max      = 3;
Engine.C_S_min      = 0;

% Alpha-Limits
alpha.HTP.alpha_max =  AeroCoefficientsNL.a_max_HTP_deg * pi/180;    % identfiziert -18� aus TAU 3D-CFD 
alpha.HTP.alpha_min = -AeroCoefficientsNL.a_max_HTP_deg * pi/180;    % identfiziert -18� aus TAU 3D-CFD
alpha.WFC.alpha_max = -15 * pi/180;
alpha.WFC.alpha_min = -10 * pi/180;
% alpha-step for calculation of dCl/da and dCm/da
alpha.dalpha        = .01 *pi/180;           % ON = 0 / OFF = 1

%% Cmu-Parameters
Cmu.MidFlap     = [0.01  0.015 0.02];
Cmu.FullFlap    = [0.02  0.03  0.04];

Cmu.Cmu_ref     = [0 0.015 0.03];
% Cmu.Cmu_ref_set = Cmu.Cmu_ref(3); % Full Flap Coefficients
Cmu.MidFlapDev  = [0.015  0.005];
Cmu.FullFlapDev = [0.0086 0.012];
Cmu.Cmu_Var_Flg = 0;
Cmu.Cmu_Const   = 0.03;
Cmu.Comp_Vec    = 1/12 * ones(1,12);
Cmu.Cmu_Fail    = 0;

%% System Failure
Fail = [];
Fail = FailStructure(Fail, Failure);

%% Adverse Ground Effect 
GE.slope = 0; 
GE.start_time = 500;
GE.CL_Value = 0;

%% Data Assembly
Aero.Slipstream     = Slipstream;
Aero.Wind_Tunnel_Ma = Wind_Tunnel_Ma;
Aero.Flaps          = Flaps;
Aero.Inputs         = Inputs;
Aero.Fail           = Fail;
% Aero.Droop          = Droop;
Aero.Lift           = Lift;
Aero.Drag           = Drag;
Aero.Side           = Side;
Aero.Roll           = Roll;
Aero.OEI            = OEI;
Aero.Yaw            = Yaw;
Aero.Pitch          = Pitch;
Aero.Eng_Side       = Eng_Side;
Aero.Downwash       = Downwash;
Aero.Engine         = Engine;
Aero.Active_HTP     = Active_HTP;
Aero.Prop           = Prop;
Aero.Unsteady_Flag  = Unsteady_Flag;
% Aero.dalpha         = dalpha;
Aero.alpha          = alpha;
Aero.Cmu            = Cmu;
% Aero.Aileron0       = Aileron0; 
Aero.GE             = GE;
end
%--------------------------------------------------------------------------