%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2009      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Environment_Init                                         *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initialize workspace variables for the environemtn subsystem.                           *
%*                                                                                                    *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : -                                                                                       *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Environment_Init                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-09 * Basic Design                                            *
%******************************************************************************************************
function Environment = BLAC_Environment_Init_pr(BLAC)

% global BLAC
% Earth constants
Earth.f        = 1/298.25722;           % Earth flattening parameter (WGS84)                [-]
Earth.a        = 6378137;               % Earth ellipsoid semi-major axis a (WGS84)         [m]
Earth.b        = Earth.a*(1-Earth.f);   % Earth ellipsoid semi-minor axis b                 [m]
Earth.e = sqrt(Earth.a^2 - Earth.b^2)/Earth.a; % Earth ellipsoid first eccentricity         [-]
Earth.omega_e  = 7.292115e-5;           % Earth rotation angular velocity (WGS84)           [rad/s] 
Earth.R_E      = 6.356766e+06;          % Earth reference radius                            [m]
Earth.R_Eq     = Earth.a;               % Earth equatorial radius (WGS84)                   [m]
Earth.g_n      = 9.80665;               % Earth Acceleration at 45�32'33'' Latitude         [m/s^2]

% Static Atmosphere 
Atmosphere.R_E    = Earth.R_E;          % Earth Reference Radius                            [m]    
Atmosphere.R      = 287.05307;          % Gas Constant                                      [J/kg*K]
Atmosphere.kappa  = 1.4;                % Isentropic Exponent                               [-]
Atmosphere.g_n    = Earth.g_n;          % Earth Acceleration at 45�32'33'' Latitude         [m/s^2]
Atmosphere.T_0    = 288.15;             % Static Temperature at 0 m Geopotential Heigth     [K]
Atmosphere.rho_0  = 1.225000e+00;       % Static Density at 0km Geopotential Height         [kg/m^3]
Atmosphere.H_trop = 11000;              % Height of Troposhperic Layer                      [m]  
Atmosphere.ga_0   = -6.500000e-03;      % Temperature Gradient for H < 11 km                [K/m]
Atmosphere.T_11   = 216.65;             % Static Temperature at 11 km geopotential Height   [K]
Atmosphere.p_0    = 1.013250e+05;       % Static Pressure at 0 m Geopotential Heigth        [N/m^2]
Atmosphere.p_11   = 0.22632e+05;        % Static Pressure at 11 km geopotential Height      [N/m^2]

% Individual Atmospheric Parameters
Atmosphere.T_offset = 0;                % Temperature offset to a standard day              [K]
Atmosphere.p_offset = 0;                % Temperature offset to a standard day              [K]

% Turbulence Dryden Spectrum Properties
Wind.Sigma_u = 5.3;                     % RMS Turbulence Amplitude in X_w Direction         [m/s]  
Wind.Sigma_v = 5.3;                     % RMS Turbulence Amplitude in Y_w Direction         [m/s] 
Wind.Sigma_w = 5.3;                     % RMS Turbulence Amplitude in Y_w Direction         [m/s] 
Wind.Tu      = 1.5;                     % Period of the characteritc Wave in X_w Diection     [s]
Wind.Tv      = 1.5;                     % Period of the characteritc Wave in Y_w Diection     [s]
Wind.Tw      = 1.5;                     % Period of the characteritc Wave in Z_w Diection     [s]

% Static 2-D Wind Properties
Wind.p_W_o = 0;                         % Wind rotation about north axis                    [rad/s]
Wind.q_W_o = 0;                         % Wind rotation about east axis                     [rad/s]
Wind.r_W_o = 0;                         % Wind rotation about down axis                     [rad/s]

% Turbulence model parameters
Wind.Turbulence_Flag            = 0;     % Turbulence Model [0] OFF / [1] ON                        [-]
Wind.Turbulence.seed_u          = fix(12*sum(clock));  % Turbulence seed for u                      [-]
Wind.Turbulence.seed_v          = fix(13*sum(clock));  % Turbulence seed for v                      [-]
Wind.Turbulence.seed_w          = fix(14*sum(clock));  % Turbulence seed for w                      [-]
Wind.Turbulence.Iso_Alt         = 1000;  % Altitude AGL where Turbulence Gets Isotropic            [ft]
Wind.Turbulence_Intensity_Index = 1;     % Set Turbulence Intensity: LIGHT  [1]                     [-]
                                         %                           MEDIUM [2]                     [-]
                                         %                           HEAVY  [3]                     [-]

% Parameters of wind profile (planetary boundary layer; PBL)
% Note: Other parameters are set directly in the Simulink model (A, B, kappa, z0)
Wind.Random_Mean_Wind_on = 0;                            % [1] Random mean wind [0] Constant mean wind
Wind.Wind_Profile_on     = 0;                            % [1] Wind speed changes with altitude [0] No change
Wind.Wind_Ekman_on       = 0;                            % [1] Ekman spiral [0] No wind direction change
Wind.Rw.Dir              = BLAC.Sensors.ILS.Rw.Dir;      % Runway direction [rad]
%Wind.Rw.Dir              = 0;    

% Activate Wind Velocity Components
Wind.u_w_active         = 1;
Wind.v_w_active         = 1;
Wind.w_w_active         = 0;

% Activate Turbulence Velocity Components
Wind.u_Turb_w_active         = 1;
Wind.v_Turb_w_active         = 1;
Wind.w_Turb_w_active         = 0;

if Wind.Random_Mean_Wind_on
    
  % Cumulative Probability Distributions for Wind at 20 ft, Ref: EASA CS-AWO
  Wind.h_Wreq   =  20*0.3048;                            % Wind at 20ft, Ref: CS-AWO, MIL-F-8785C
  % EASA CS-AWO, Figure 4:
  Wind.Vw       = [0.0000,3.8934,6.7623,9.6311,12.705,15.779,17.418,18.852,20.697,22.951,...
                   24.385,25.820,27.459,29.508];
  Wind.Fv       = 1 - [100 80 60 40 20 10 6 4 2 1 0.6 0.4 0.2 0.1]/100;
  % EASA CS-AWO, Figure 5:
  Wind.Aw       = (-180:180/8:180);
  Wind.Fa       = [0.056327,0.107756,0.156736,0.200818,0.249798,0.298778,0.362451,0.442043,...
                   0.544903,0.641638,0.712658,0.757964,0.799594,0.837554,0.882860,0.929391,0.986942];

  Wind.u_Wreq   = interp1(Wind.Fv, Wind.Vw, rand, 'spline', 'extrap') * 3.6/1.852;
  Wind.psi_Wreq = interp1(Wind.Fa, Wind.Aw, rand, 'spline', 'extrap') * pi/180 ...
                          + Wind.Rw.Dir + pi;

else

  Wind.h_Wreq   =  20;                                    % Height for required wind     [m]
  % 1. Note: h_Wreq has to be greater than z0, which is currently set in the Simulink model to 0.15 m.
  % 2. Note: h_Wreq is limited to the surface layer, but that is not checked in the model.
  Wind.u_Wreq   =  5.144; %15 /1.943845;                          % Wind speed                 [m/s]
  % Wind.psi_Wreq =  pi/2 + Wind.Rw.Dir + pi;               % Wind direction             [rad]
  Wind.psi_Wreq = 173*pi/180;                          % Wind direction             [rad]
  % For headwind define psiw_20ft = Wind.Rw.Dir + 180;

end

Wind.psi_Wreq   = mod(Wind.psi_Wreq, 2*pi);

% Headwind and crosswind calculations along the runway at 20 ft
Wind.Vnw      = Wind.u_Wreq*cos(Wind.psi_Wreq);
Wind.Vew      = Wind.u_Wreq*sin(Wind.psi_Wreq);
Wind.Vw_head  = Wind.Vnw*cos(BLAC.Sensors.ILS.Rw.Dir + pi) + ...
                Wind.Vew*sin(BLAC.Sensors.ILS.Rw.Dir + pi);
Wind.Vw_right = Wind.Vnw*cos(BLAC.Sensors.ILS.Rw.Dir + 3*pi/2) +...
                Wind.Vew*sin(BLAC.Sensors.ILS.Rw.Dir + 3*pi/2);

% Wind Step
Wind.u_W_Step              = 0;
Wind.u_W_Step_Time         = 0;

Wind.v_W_Step              = 0;
Wind.v_W_Step_Time         = 0;

Wind.w_W_Step              = 0;
Wind.w_W_Step_Time         = 0;

% Zero Threshold Parameters
Environment.Div_Zero_Saturation = 0.001; % [-] Min. Saturation to Avoid Division by Zero

% Environment Flags
Environment.GeoHeight               = 1; % Geopotential height calculation ON[1] OFF [0].    [-]
Environment.Gravity                 = 1; % Dynamic [1] or constant [0] gravitational accel.  [-] 
Environment.Turbulence_Flag         = 0; % Turbulence ON [1] or OFF [0]                      [-]

% Assemble environment structure
Environment.Earth      = Earth;
Environment.Atmosphere = Atmosphere;
Environment.Wind       = Wind;

% Airfield Elevation
Environment.Terrain_Elevation      = 0; % Airfield Elevation                                 [m]
%---------------------------------------------------------------------------------------------------EOF