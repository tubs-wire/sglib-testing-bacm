clear all
close all
clc

load TP_Look_Up_REF2

SaveName = 'ThrustLookup';
LineSize = 2;
MarkerSize = 5;
FontSize   = 24;
LegendSize = FontSize-4;

Reduced_Look_up         = squeeze(Thrust_Look_up.Case1.Total_Thrust(13,:,:));
Reduced_Look_up_min     = squeeze(Thrust_Look_up.Case1.Min_Thrust(13,:,:));
             
figure
set( gcf ,  'Color','w', ...
            'DefaultLineLineWidth',LineSize ,...
            'DefaultLineMarkerSize',MarkerSize,...
            'DefaultAxesFontName','times',...
            'DefaultAxesFontSize',FontSize,...
            'DefaultTextInterpreter','latex')

surf(Thrust_Look_up.Case1.Alt, Thrust_Look_up.Case1.Ma, Reduced_Look_up./1000)        
grid on, box off, hold on
surf(Thrust_Look_up.Case1.Alt, Thrust_Look_up.Case1.Ma, Reduced_Look_up_min./1000)        
view([135 25])
        
xlabel('$$ H\ddot{o}he [m] $$')
ylabel('$$ M [-] $$')
zlabel('$$ T [kN] $$')
xlim([0 15000])
Ax = gca;
set(Ax,'units','normalized','Position',[.20 .2 .65 .75]);

Savename = [SaveName];
fout = [pwd '\' Savename];
print(gcf, '-depsc2', fout)

figure
set( gcf ,  'Color','w', ...
            'DefaultLineLineWidth',LineSize ,...
            'DefaultLineMarkerSize',MarkerSize,...
            'DefaultAxesFontName','times',...
            'DefaultAxesFontSize',FontSize,...
            'DefaultTextInterpreter','latex')

surf(Thrust_Look_up.Case1.Alt, Thrust_Look_up.Case1.Ma, Reduced_Look_up_min./1000)        
grid on, box off, hold on
view([135 25])
        
xlabel('$$ H\ddot{o}he [m] $$')
ylabel('$$ M [-] $$')
zlabel('$$ T [kN] $$')
xlim([0 15000])
Ax = gca;
set(Ax,'units','normalized','Position',[.20 .2 .65 .75]);

Savename = [SaveName 'MinOnly'];
fout = [pwd '\' Savename];
print(gcf, '-depsc2', fout)


Reduced_Look_up         = squeeze(Thrust_Look_up.Case7.Total_Thrust(13,:,:));
Reduced_Look_up_min     = squeeze(Thrust_Look_up.Case7.Min_Thrust(13,:,:));
             
figure
set( gcf ,  'Color','w', ...
            'DefaultLineLineWidth',LineSize ,...
            'DefaultLineMarkerSize',MarkerSize,...
            'DefaultAxesFontName','times',...
            'DefaultAxesFontSize',FontSize,...
            'DefaultTextInterpreter','latex')

surf(Thrust_Look_up.Case7.Alt, Thrust_Look_up.Case7.Ma, Reduced_Look_up./1000)        
grid on, box off, hold on
surf(Thrust_Look_up.Case7.Alt, Thrust_Look_up.Case7.Ma, Reduced_Look_up_min./1000)        
view([135 25])
        
xlabel('$$ H\ddot{o}he [m] $$')
ylabel('$$ M [-] $$')
zlabel('$$ T [kN] $$')
xlim([0 1000])
Ax = gca;
set(Ax,'units','normalized','Position',[.20 .2 .65 .75]);

Savename = [SaveName 'Case7'];
fout = [pwd '\' Savename];
print(gcf, '-depsc2', fout)

figure
set( gcf ,  'Color','w', ...
            'DefaultLineLineWidth',LineSize ,...
            'DefaultLineMarkerSize',MarkerSize,...
            'DefaultAxesFontName','times',...
            'DefaultAxesFontSize',FontSize,...
            'DefaultTextInterpreter','latex')

surf(Thrust_Look_up.Case7.Alt, Thrust_Look_up.Case7.Ma, Reduced_Look_up_min./1000)        
grid on, box off, hold on
view([135 25])
        
xlabel('$$ H\ddot{o}he [m] $$')
ylabel('$$ M [-] $$')
zlabel('$$ T [kN] $$')
xlim([0 1000])
Ax = gca;
set(Ax,'units','normalized','Position',[.20 .2 .65 .75]);

Savename = [SaveName 'Case7MinOnly'];
fout = [pwd '\' Savename];
print(gcf, '-depsc2', fout)
