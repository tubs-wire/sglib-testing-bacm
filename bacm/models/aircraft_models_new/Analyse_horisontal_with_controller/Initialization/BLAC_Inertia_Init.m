%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Inertia_Init                                           *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the workspace variables that contain basic aircraft mass, CG, inertia.      *
%*            A template file is replaced by the values generated with DATCOM.                        *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : Aircraft Name: BLAC_Ref-2                                                             *
%*                                                                                                    *
%*            file created : 16-Jul-2015                                                              *
%*                           The data is taken from Heinze 	Mass X_CG Sheets                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Inertia_Init                                                             *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date    *                     Description                        *
%******************************************************************************************************
%* Jobst Henning Diekmann       * 15.08.2015 * Create Lookup for Inertia depending on Mass and CG Pos *
%******************************************************************************************************
function Inertia = BLAC_Inertia_Init(Mass, x_CG)

% Moments of inertia of aircraft with fuel but wihtout additonal loads
Inertia.I_xx = 446027.33;       % Inertia xx-component at about defined CG.[kgm^2]
Inertia.I_yy = 982930.45;       % Inertia yy-component at about defined CG.[kgm^2]
Inertia.I_zz = 1327869.07;      % Inertia zz-component at about defined CG.[kgm^2]
Inertia.I_xz = -59255.22;       % Inertia xz-component at about defined CG.[kgm^2]
Inertia.I_yx = 0;               % Inertia yx-component at about defined CG.[kgm^2]
Inertia.I_zy = 0;               % Inertia zy-component at about defined CG.[kgm^2]

end
%---------------------------------------------------------------------------------------------------EOF