function Roll = RollStructure(Lateral_Coefficients, VerticalTailSize, Config, LateralSlipstream, RollingDerivatives, ControlBoost, ModelResult)
% 
% 

%% Uncertainty Matrix
Roll.T_Uncert        = ModelResult.RollModel.SubModel_Wing.T_Vec(1:3);
Roll.beta_Uncert     = [0 5 10]./180*pi;
Roll.Uncert_b05T0    = 1.00 ; % abs(Beta)= 5  deg, T0 = 0 N: +- 15% 
Roll.Uncert_b05T1    = 1.00 ; % abs(Beta)= 5  deg, T1 = 0 N: +- 25% 
Roll.Uncert_b05T2    = 1.00 ; % abs(Beta)= 5  deg, T2 = 0 N: +- 25% 
Roll.Uncert_b10T0    = 1.00 ; % abs(Beta)= 10 deg, T0 = 0 N: +- 15% 
Roll.Uncert_b10T1    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
Roll.Uncert_b10T2    = 1.00 ; % abs(Beta)= 10 deg, T1 = 0 N: +- 25% 
Roll.Uncertainty_LT  = [    1                   1                   1                   ;...
                            Roll.Uncert_b05T0   Roll.Uncert_b05T1   Roll.Uncert_b05T2   ;...
                            Roll.Uncert_b10T0   Roll.Uncert_b10T1   Roll.Uncert_b10T2   ];

Roll.Uncert_Clp     = 1.00  ; % Clp:    +- 5%
Roll.Uncert_Clr     = 1.00  ; % Clr:    +- 50%
Roll.Uncert_Clzeta  = 1.00  ; % Clzeta: +- 10%
Roll.Uncert_Clxi    = 1.00  ; % Clxi:   +- 10%

%% Derivatives                    
Roll.C_l0           = 0;
Roll.C_lbdRD        = 0;
Roll.C_lb           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clb1 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clb1];
Roll.C_lb2          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clb2];
Roll.C_lb3          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clb3 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clb3 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clb3];
Roll.C_lba          = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clba ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clba ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clba];
Roll.C_lp           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clp ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clp];
Roll.C_lp(3)        =   RollingDerivatives.Clp_St;
Roll.C_lpa          =   0;
Roll.C_lpb2         = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clpb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clpb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clpb2];
Roll.C_lr           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clr ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clr];
%neu:
Roll.C_lrb2           = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clrb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clrb2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clrb2];
                    
Roll.C_lra          = 0;
Roll.C_lrdRD        = 0;
Roll.C_lksi         = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clxia ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clxia] * ControlBoost; % Aileron Values not effective enough -> manual increase               
Roll.C_lksi(3)      =   -0.185 / (20 / 180*pi);   % Rollmoment aus Ersten Abschätzungen Keller 
Roll.C_lksib2       = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clxiab2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clxiab2 ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clxiab2];         %   -0.26;               
Roll.C_lzeta        = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clzeta ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clzeta];        %   0.058;              
Roll.C_lzetaa       = [ Lateral_Coefficients.(VerticalTailSize).(Config{1}).Cl.Clzetaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{2}).Cl.Clzetaa ...
                        Lateral_Coefficients.(VerticalTailSize).(Config{3}).Cl.Clzetaa];       %   0.058;              
Roll.C_ldSP         = [ 0 0 0]; 

Roll.CrossCouplFlag = 0;

% Sideslip Roll Parameters
Roll.Beta_LT                        = LateralSlipstream.Derivatives.New_Model.beta_LT / 180*pi;
Roll.T_Ref                          = LateralSlipstream.Derivatives.New_Model.T_ref;
Roll.Cmu_Ref                        = LateralSlipstream.Derivatives.New_Model.Cmu_ref;

Roll.VTP.LT_C_l_T0_T1               = LateralSlipstream.Derivatives.New_Model.Cl_VTP_LT_T0T1_Cmu0;
Roll.VTP.LT_C_l_T1_Cmu0_Cmu1        = LateralSlipstream.Derivatives.New_Model.Cl_VTP_LT_T1_Cmu0Cmu03;
Roll.VTP.C_l_Beta0                  = LateralSlipstream.Derivatives.New_Model.Cl_beta_VTP_0 * 180/pi;
Roll.VTP.C_l_BetaCmu                = LateralSlipstream.Derivatives.New_Model.Cl_beta_VTP_Cmu * 180/pi;

Roll.HTP.LT_C_l_T0_T1               = LateralSlipstream.Derivatives.New_Model.Cl_HTP_LT_T0T1_Cmu0;
Roll.HTP.LT_C_l_T1_Cmu0_Cmu1        = LateralSlipstream.Derivatives.New_Model.Cl_HTP_LT_T1_Cmu0Cmu03;
Roll.HTP.C_l_Beta0                  = LateralSlipstream.Derivatives.New_Model.Cl_beta_HTP_0 * 180/pi;
Roll.HTP.C_l_BetaCmu                = LateralSlipstream.Derivatives.New_Model.Cl_beta_HTP_Cmu * 180/pi;

Roll.Wing.LT_C_l_T0_T1              = LateralSlipstream.Derivatives.New_Model.Cl_Wing_LT_T0T1_Cmu0;
Roll.Wing.LT_C_l_T1_Cmu0_Cmu1       = LateralSlipstream.Derivatives.New_Model.Cl_Wing_LT_T1_Cmu0Cmu03;
Roll.Wing.LT_C_l_T0_Cmu0_Cmu1       = LateralSlipstream.Derivatives.New_Model.Cl_Wing_LT_T0_Cmu0Cmu03;
Roll.Wing.C_l_Beta0                 = LateralSlipstream.Derivatives.New_Model.Cl_beta_Wing_0 * 180/pi;

Roll.Fuselage.LT_C_l_T0_T1          = LateralSlipstream.Derivatives.New_Model.Cl_Fuse_LT_T0T1_Cmu0  ;
Roll.Fuselage.LT_C_l_T1_Cmu0_Cmu1   = LateralSlipstream.Derivatives.New_Model.Cl_Fuse_LT_T1_Cmu0Cmu03;
Roll.Fuselage.C_l_Beta0             = LateralSlipstream.Derivatives.New_Model.Cl_beta_Fuse_0 * 180/pi;
Roll.Fuselage.C_l_BetaCmu           = LateralSlipstream.Derivatives.New_Model.Cl_beta_Fuse_Cmu * 180/pi;

Roll.C_lb(3)                        = Roll.Fuselage.C_l_Beta0 + Roll.Wing.C_l_Beta0 + Roll.VTP.C_l_Beta0 + Roll.HTP.C_l_Beta0;

%% New Sideslip Parameters
Roll.Sideslip.Wing.T_Vec       = ModelResult.RollModel.SubModel_Wing.T_Vec;
Roll.Sideslip.Wing.alpha_Vec   = ModelResult.RollModel.SubModel_Wing.alpha_Vec;
Roll.Sideslip.Wing.Cb_lookup   = ModelResult.RollModel.SubModel_Wing.Cb_lookup;
Roll.Sideslip.Wing.dB_lookup   = ModelResult.RollModel.SubModel_Wing.dB_lookup;
Roll.Sideslip.Wing.F_lookup    = ModelResult.RollModel.SubModel_Wing.F_lookup;
Roll.Sideslip.Wing.K_lookup    = ModelResult.RollModel.SubModel_Wing.K_lookup;

Roll.Sideslip.Body.T_Vec       = ModelResult.RollModel.SubModel_Body.T_Vec;
Roll.Sideslip.Body.alpha_Vec   = ModelResult.RollModel.SubModel_Body.alpha_Vec;
Roll.Sideslip.Body.Cb_lookup   = ModelResult.RollModel.SubModel_Body.Cb_lookup;
Roll.Sideslip.Body.dB_lookup   = ModelResult.RollModel.SubModel_Body.dB_lookup;
Roll.Sideslip.Body.F_lookup    = ModelResult.RollModel.SubModel_Body.F_lookup;
Roll.Sideslip.Body.K_lookup    = ModelResult.RollModel.SubModel_Body.K_lookup;

Roll.Sideslip.HTP.T_Vec       = ModelResult.RollModel.SubModel_HTP.T_Vec;
Roll.Sideslip.HTP.alpha_Vec   = ModelResult.RollModel.SubModel_HTP.alpha_Vec;
Roll.Sideslip.HTP.Cb_lookup   = ModelResult.RollModel.SubModel_HTP.Cb_lookup;
Roll.Sideslip.HTP.dB_lookup   = ModelResult.RollModel.SubModel_HTP.dB_lookup;
Roll.Sideslip.HTP.F_lookup    = ModelResult.RollModel.SubModel_HTP.F_lookup;
Roll.Sideslip.HTP.K_lookup    = ModelResult.RollModel.SubModel_HTP.K_lookup;

Roll.Sideslip.VTP.T_Vec       = ModelResult.RollModel.SubModel_VTP.T_Vec;
Roll.Sideslip.VTP.alpha_Vec   = ModelResult.RollModel.SubModel_VTP.alpha_Vec;
Roll.Sideslip.VTP.Cb_lookup   = ModelResult.RollModel.SubModel_VTP.Cb_lookup;
Roll.Sideslip.VTP.dB_lookup   = ModelResult.RollModel.SubModel_VTP.dB_lookup;
Roll.Sideslip.VTP.F_lookup    = ModelResult.RollModel.SubModel_VTP.F_lookup;
Roll.Sideslip.VTP.K_lookup    = ModelResult.RollModel.SubModel_VTP.K_lookup;

% Roll.Sideslip.BetaCorrect.T_Vec       = ModelResult.RollModel.SubModel_Full.T_Vec;
% Roll.Sideslip.BetaCorrect.alpha_Vec   = ModelResult.RollModel.SubModel_Full.alpha_Vec;
% Roll.Sideslip.BetaCorrect.Cb_lookup   = ModelResult.RollModel.SubModel_Full.Cb_lookup;
% Cb_interp = ((ModelResult.RollModel.SubModel_Full.Cb_lookup(3,3) - ModelResult.RollModel.SubModel_Full.Cb_lookup(3,1))/ModelResult.RollModel.SubModel_Full.T_Vec(3))*ModelResult.RollModel.SubModel_Full.T_Vec(2) + ModelResult.RollModel.SubModel_Full.Cb_lookup(3,1);
% Roll.Sideslip.BetaCorrect.Cb_lookup(3,2) = Cb_interp;
% Roll.Sideslip.BetaCorrect.Cb_lookup(4,2) = Cb_interp;

%% OEI Parameters
%Roll Wing
Roll.OEI.Wing.C_bOEI_in      = ModelResult.RollModel.SubModel_Wing.OEI.C_bOEI_in;
Roll.OEI.Wing.F_tanHb        = ModelResult.RollModel.SubModel_Wing.OEI.F_tanHb;
Roll.OEI.Wing.K_Switch       = ModelResult.RollModel.SubModel_Wing.OEI.K_Switch;
Roll.OEI.Wing.F_Rel_far      = ModelResult.RollModel.SubModel_Wing.OEI.F_Rel_far;
Roll.OEI.Wing.beta_vec       = ModelResult.RollModel.SubModel_Wing.OEI.beta_vec(3);
Roll.OEI.Wing.C_bOEI_far     = ModelResult.RollModel.SubModel_Wing.OEI.C_bOEI_far;

Roll.OEI.Wing.C_TOEI_in      = ModelResult.RollModel.SubModel_Wing.OEI.C_TOEI_in;
Roll.OEI.Wing.T_Vec          = ModelResult.RollModel.SubModel_Wing.OEI.T_Vec(2);
Roll.OEI.Wing.F_tanHT        = ModelResult.RollModel.SubModel_Wing.OEI.F_tanHT;
Roll.OEI.Wing.C_TOEI_out     = ModelResult.RollModel.SubModel_Wing.OEI.C_TOEI_out;

%Roll VTP
Roll.OEI.VTP.C_bOEI_in      = ModelResult.RollModel.SubModel_VTP.OEI.C_bOEI_in;
Roll.OEI.VTP.F_tanHb        = ModelResult.RollModel.SubModel_VTP.OEI.F_tanHb;
Roll.OEI.VTP.K_Switch       = ModelResult.RollModel.SubModel_VTP.OEI.K_Switch;
Roll.OEI.VTP.F_Rel_far      = ModelResult.RollModel.SubModel_VTP.OEI.F_Rel_far;
Roll.OEI.VTP.beta_vec       = ModelResult.RollModel.SubModel_VTP.OEI.beta_vec(3);
Roll.OEI.VTP.C_bOEI_far     = ModelResult.RollModel.SubModel_VTP.OEI.C_bOEI_far;

Roll.OEI.VTP.C_TOEI_in      = ModelResult.RollModel.SubModel_VTP.OEI.C_TOEI_in;
Roll.OEI.VTP.T_Vec          = ModelResult.RollModel.SubModel_VTP.OEI.T_Vec(2);
Roll.OEI.VTP.F_tanHT        = ModelResult.RollModel.SubModel_VTP.OEI.F_tanHT;
Roll.OEI.VTP.C_TOEI_out     = ModelResult.RollModel.SubModel_VTP.OEI.C_TOEI_out;

%Roll HTP
Roll.OEI.HTP.C_bOEI_in      = ModelResult.RollModel.SubModel_HTP.OEI.C_bOEI_in;
Roll.OEI.HTP.F_tanHb        = ModelResult.RollModel.SubModel_HTP.OEI.F_tanHb;
Roll.OEI.HTP.K_Switch       = ModelResult.RollModel.SubModel_HTP.OEI.K_Switch;
Roll.OEI.HTP.F_Rel_far      = ModelResult.RollModel.SubModel_HTP.OEI.F_Rel_far;
Roll.OEI.HTP.beta_vec       = ModelResult.RollModel.SubModel_HTP.OEI.beta_vec(3);
Roll.OEI.HTP.C_bOEI_far     = ModelResult.RollModel.SubModel_HTP.OEI.C_bOEI_far;

Roll.OEI.HTP.C_TOEI_in      = ModelResult.RollModel.SubModel_HTP.OEI.C_TOEI_in;
Roll.OEI.HTP.T_Vec          = ModelResult.RollModel.SubModel_HTP.OEI.T_Vec(2);
Roll.OEI.HTP.F_tanHT        = ModelResult.RollModel.SubModel_HTP.OEI.F_tanHT;
Roll.OEI.HTP.C_TOEI_out     = ModelResult.RollModel.SubModel_HTP.OEI.C_TOEI_out;

%Roll Body
Roll.OEI.Body.C_bOEI_in      = ModelResult.RollModel.SubModel_Body.OEI.C_bOEI_in;
Roll.OEI.Body.F_tanHb        = ModelResult.RollModel.SubModel_Body.OEI.F_tanHb;
Roll.OEI.Body.K_Switch       = ModelResult.RollModel.SubModel_Body.OEI.K_Switch;
Roll.OEI.Body.F_Rel_far      = ModelResult.RollModel.SubModel_Body.OEI.F_Rel_far;
Roll.OEI.Body.beta_vec       = ModelResult.RollModel.SubModel_Body.OEI.beta_vec(3);
Roll.OEI.Body.C_bOEI_far     = ModelResult.RollModel.SubModel_Body.OEI.C_bOEI_far;

Roll.OEI.Body.C_TOEI_in      = ModelResult.RollModel.SubModel_Body.OEI.C_TOEI_in;
Roll.OEI.Body.T_Vec          = ModelResult.RollModel.SubModel_Body.OEI.T_Vec(2);
Roll.OEI.Body.F_tanHT        = ModelResult.RollModel.SubModel_Body.OEI.F_tanHT;
Roll.OEI.Body.C_TOEI_out     = ModelResult.RollModel.SubModel_Body.OEI.C_TOEI_out;

%Roll Full
Roll.OEI.Full.C_bOEI_in      = ModelResult.RollModel.SubModel_Full.OEI.C_bOEI_in;
Roll.OEI.Full.F_tanHb        = ModelResult.RollModel.SubModel_Full.OEI.F_tanHb;
Roll.OEI.Full.K_Switch       = ModelResult.RollModel.SubModel_Full.OEI.K_Switch;
Roll.OEI.Full.F_Rel_far      = ModelResult.RollModel.SubModel_Full.OEI.F_Rel_far;
Roll.OEI.Full.beta_vec       = ModelResult.RollModel.SubModel_Full.OEI.beta_vec(3);
Roll.OEI.Full.C_bOEI_far     = ModelResult.RollModel.SubModel_Full.OEI.C_bOEI_far;

Roll.OEI.Full.C_TOEI_in      = ModelResult.RollModel.SubModel_Full.OEI.C_TOEI_in;
Roll.OEI.Full.T_Vec          = ModelResult.RollModel.SubModel_Full.OEI.T_Vec(2);
Roll.OEI.Full.F_tanHT        = ModelResult.RollModel.SubModel_Full.OEI.F_tanHT;
Roll.OEI.Full.C_TOEI_out     = ModelResult.RollModel.SubModel_Full.OEI.C_TOEI_out;

%% Artificial Rolling momtent for Roll Performance Investigation
Roll.Artificial.Delta_Cl_xi             = 0;
Roll.Artificial.Control_Duration        = 0;    
Roll.Artificial.Delta_Cl_xi_Slope       = 0;
Roll.Artificial.Cl_xi_Init              = 0;    
Roll.Artificial.Cl_xi_t_Init            = 0;    
Roll.Artificial.Cl_xi_Flag              = 0;

%% Artificial Rolling momtent for Yaw Performance Investigation
Roll.Artificial.Delta_Cl_zeta           = 0;
Roll.Artificial.Control_Duration        = 0;    
Roll.Artificial.Delta_Cl_zeta_Slope     = 0;
Roll.Artificial.Cl_zeta_Init            = 0;    
Roll.Artificial.Cl_zeta_t_Init          = 0;    
Roll.Artificial.Cl_zeta_Flag            = 0;
end