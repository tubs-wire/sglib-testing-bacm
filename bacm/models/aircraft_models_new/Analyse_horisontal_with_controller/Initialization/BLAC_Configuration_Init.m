%******************************************************************************************************
%*                                                                                                    *
%*        (c) 2006      Deutsches Zentrum f�r Luft- und Raumfahrt e.V.                                *
%*                               in der Helmholtz-Gemeinschaft                                        *
%*                                                                                                    *
%*                           Institute of Flight Systems, Braunschweig                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%*                                      BLAC_Configuration                                          *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Type     : Function                                                                                *
%*                                                                                                    *
%* Purpose  : Initializes the workspace variables that contain basic aircraft geometries.             *
%*            A template file is replaced by the values generated with DATCOM.                        *
%* Version  : 1.0                                                                                     *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Remarks  : Aircraft Name: BLAC_Konfig_28_2_2012                                                      *
%*                                                                                                    *
%*            file created : 2012-02-28 time: 15:19:22                                                *
%*                                                                                                    *
%******************************************************************************************************
%*                                                                                                    *
%* Syntax : BLAC_Configuration                                                                      *
%*                                                                                                    *
%******************************************************************************************************
%*             Author           *    Date   *                     Description                         *
%******************************************************************************************************
%* Christian Raab               * 29-OCT-06 * Basic Design                                            *
%* Andre Thurm                  * 27-JAN-11 * generated template files                                *
%******************************************************************************************************
function Configuration = BLAC_Configuration_Init
% all Variables are replaced by their corredponding values with the PraDaMa
% script.

% Geometry Data of the BLAC
% Geometry.x_LH                 = 8.23;            % Old Geometry                                  [m]
Geometry.x_LH                 = 17.37;              % Lever arm, horizontal distance from the 
                                                    % neutral point of wing to neutral point of      
                                                    % horizontal tail.                              [m]
Geometry.b                    = 28.775;             % Wing span.                                    [m]
Geometry.s                    = Geometry.b/2;       % Half span.                                    [m]
Geometry.S                    = 95;                 % Reference wing area.                        [m^2]
Geometry.S_H                  = 27;                 % Reference horizontal-tail area Ref-2-2014   [m^2] 
% Geometry.S_H                  = 24;               % Reference horizontal-tail area Ref 0-2013   [m^2] 
Geometry.x_HTW                = 18.745 + 1.14;      % Horizontal distance between stabilizer and
                                                    % Propeller.                                    [m]
Geometry.Lambda               = 9;                  % Aspect ratio of the wing.                     [-]
Geometry.l_mac                = 3.428;              % Mean aerodynamic chord.                       [m]
Geometry.y_l_mac              = 6.083;              % Mean aerodynamic chord.                       [m]
Geometry.l_i                  = 4.4372;             % Wing root chord.                              [m]
Geometry.x_rp                 = -13.131;            % X-coord. moment reference point.              [m]
Geometry.y_rp                 = 0;                  % Y-coord. moment reference point.              [m]
Geometry.z_rp                 = -1.563;             % Z-coord. moment reference point.              [m]
Geometry.x_lmac               = -12.274;            % X-coord. start of the mean chord length.      [m]
Geometry.l_ref_alpha_dot_star = Geometry.l_mac;     % Reference length for alpha_dot normalization. [m]
Geometry.l_ref_beta_dot_star  = Geometry.l_mac;     % Reference length for beta_dot normalization.  [m]
Geometry.l_ref_p_star         = Geometry.s;         % Reference length for p normalization.         [m]
Geometry.l_ref_q_star         = Geometry.l_mac;     % Reference length for q normalization.         [m]
Geometry.l_ref_r_star         = Geometry.s;         % Reference length for r normalization.         [m]

% Data Assembly
Configuration.Geometry = Geometry;         
end
%---------------------------------------------------------------------------------------------------EOF