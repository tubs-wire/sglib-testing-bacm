function [AC_Empty Fuel_Available] = BLAC_Inertia_CG_Init(Gear, Mission, Fuel, Loading, OEW, MaxVal)

if nargin < 6
   if Fuel == 1
        Fuel_Name = 'Fuel_Full';
   elseif Fuel == 0.5
        Fuel_Name = 'Fuel_Half';
   elseif Fuel == 0.1
        Fuel_Name = 'Fuel_Low';
   end
   MaxVal = NaN;
else
    Fuel_Name = MaxVal;
end

if Gear
    Gear_Name = 'Gear_down';
else
    Gear_Name = 'Gear_up';
end
    

if Mission == 1
    Mission_Name = 'Ref_Mission';
    if Loading == 1
        Loading_Name = 'InsideOut';
    elseif Loading == 2
        Loading_Name = 'OutsideIn';
    end
elseif Mission == 2
    Mission_Name = 'Max_Payload';
    if Loading == 1
        Loading_Name = 'InsideOut';
    elseif Loading == 2
        Loading_Name = 'OutsideIn';
    end
elseif Mission == 3
    Mission_Name = 'Max_Fuel';
    if Loading == 1
        Loading_Name = 'InsideOut';
    elseif Loading == 2
        Loading_Name = 'OutsideIn';
    end
elseif Mission == 4
    Mission_Name = 'Transfer';
    Loading_Name = 'InsideOut';
end

load Mass_Inertia_CG.mat

AC_Empty.CG.x_rp        = -Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).x_CG;
AC_Empty.CG.y_rp        = 0;
AC_Empty.CG.z_rp        = -Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).z_CG;

MAC                     = 3.4286;
AC_Empty.CG.MAC         = ((-AC_Empty.CG.x_rp - (13.1304-0.25*MAC))/MAC)*100;

AC_Empty.Inertia.I_xx   =   Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_XX;       % Inertia xx-component at about defined CG.[kgm^2]
AC_Empty.Inertia.I_yy   =   Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_YY;       % Inertia yy-component at about defined CG.[kgm^2]
AC_Empty.Inertia.I_zz   =   Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_ZZ;      % Inertia zz-component at about defined CG.[kgm^2]
AC_Empty.Inertia.I_xz   =  -Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_ZX;       % Inertia xz-component at about defined CG.[kgm^2]
AC_Empty.Inertia.I_yx   =  -Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_XY;       % Inertia yx-component at about defined CG.[kgm^2]
AC_Empty.Inertia.I_zy   =  -Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).I_YZ;       % Inertia zy-component at about defined CG.[kgm^2]

AC_Empty.Mass           = OEW;

Fuel_Available          = Mass_Inertia_CG.(Gear_Name).(Mission_Name).(Fuel_Name).(Loading_Name).Mass - OEW;

end
