function Controller = BLAC_Controller_Init

% Detect Saturated Output Signal
Controller.Int_Sat_Prot             = 1;                            % Switch protection on/off [1/0]

% Auto-Pilot
% Airspeed Deviation
Controller.AP_PID_VTAS.PID_Gains    = [1 0.1 0.3];
Controller.AP_PID_VTAS.Overall      = 0.1;
Controller.AP_PID_VTAS              = build_controller(Controller.AP_PID_VTAS.PID_Gains, Controller.AP_PID_VTAS.Overall);
Controller.AP_PID_VTAS.On_Off       = 1;

% ILS Deviation from Glideslope
Controller.AP_PID_dGs.PID_Gains     = [1 0.1 0.3];
Controller.AP_PID_dGs.Overall       = 60; %50;
Controller.AP_PID_dGs               = build_controller(Controller.AP_PID_dGs.PID_Gains, Controller.AP_PID_dGs.Overall);
Controller.AP_PID_dGs.On_Off        = 1;

% Pitch Attitude Control
Controller.AP_PID_Theta.PID_Gains   = [0 0 1];
Controller.AP_PID_Theta.Overall     = 1; %50;
Controller.AP_PID_Theta             = build_controller(Controller.AP_PID_Theta.PID_Gains, Controller.AP_PID_Theta.Overall);
Controller.AP_PID_Theta.On_Off      = 1;
Controller.AP_PID_Theta.Ctrl_Type   = 1;
Controller.AP_PID_Theta.Alt_Gain    = 2;

% Pitch Damper
Controller.Pitch_DamperPID_Gains    = [3 0 0];
Controller.Pitch_DamperOverall      = 1; %50;
Controller.Pitch_Damper             = build_controller(Controller.Pitch_DamperPID_Gains, Controller.Pitch_DamperOverall);
Controller.Pitch_Damper.On_Off      = 1;

% Vertical Speed
Controller.AP_PID_dH_dot.PID_Gains  = [3 0 0];
Controller.AP_PID_dH_dot.Overall    = .001; %50;
Controller.AP_PID_dH_dot            = build_controller(Controller.AP_PID_dH_dot.PID_Gains, Controller.AP_PID_dH_dot.Overall);
Controller.AP_PID_dH_dot.On_Off     = 1;

% Auto Throttle
Controller.AThr.On_Off              = 1;
Controller.AThr.dGs_Overall         = 150;
Controller.AThr.dGsdt               = 4;
Controller.AThr.dax_Overall         = 0.01;
Controller.AThr.Gain_Out            = .1;
end