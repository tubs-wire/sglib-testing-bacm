function Fail = FailStructure(Fail, Failure)
Fail.Deactivated    = 1;
Fail.alpha          = Failure.Flap65deg.alpha;
Fail.alpha_max      = Failure.Flap65deg.almax;
Fail.Flaps          = [0 30 65];
Fail.CL(:,3)        = Failure.Flap65deg.CL;
Fail.CD(:,3)        = Failure.Flap65deg.CD;
Fail.Cm(:,3)        = Failure.Flap65deg.Cm;

Fail.CL(:,2)        = Failure.Flap65deg.CL;
Fail.CD(:,2)        = Failure.Flap30deg.CD;
Fail.Cm(:,2)        = Failure.Flap30deg.Cm;

Fail.CL(:,1)        = Failure.Flap00deg.CL;
Fail.CD(:,1)        = Failure.Flap00deg.CD;
Fail.Cm(:,1)        = Failure.Flap00deg.Cm;

Fail.Time           = 10^9;
Fail.Time_Control   = 10^9;
end