%===================================================================
% SCRIPT:
%       UQ of eigenfrequency of SFB 880 highlift aircraft
%===================================================================
%
%
%   Noemi Friedman
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%% Init stuff
close_system 'SFB880_Library.mdl'
close_system 'DLR_SFB880_Test_Quat'
close_system 'DLR_SFB880_Trim_Quat'
clear all
clc
pathinfo.act_pathname=pwd;
pathinfo.sglib_dir=(fileparts(which('startup.m')));
pathinfo.root_dir=fileparts(pathinfo.sglib_dir);
pathinfo.save_dir='/home/noefried/SFB880/dynamic_sim_model/vertical_motion_sim_results';
if exist(pathinfo.save_dir,  'dir')==0;
    mkdir(pathinfo.save_dir);
end
cd(pathinfo.sglib_dir);
startup

%% Add path for the model (deterministic solver)
    
pathinfo.parent_dir=fileparts(fileparts( mfilename('fullpath') ));
addpath( fullfile(pathinfo.parent_dir, 'aircraft_models_new') );
addpath(genpath( fullfile(pathinfo.parent_dir, 'aircraft_models_new', 'fullflap_sim_and_frequencies2') ));
addpath(genpath( fullfile(pathinfo.parent_dir, 'aircraft_models_new', 'param_estim') ));
addpath(genpath( fullfile(pathinfo.parent_dir, 'aircraft_models_new', 'FFT') ));
addpath(genpath( fullfile(fileparts(pathinfo.parent_dir), 'methods')));

%% set path for results (in a folder in the root, where sglib is saved)

pathinfo.result_folder='Results_SFB880_eigenfreq_with_Simparams';
pathinfo.saving_path=fullfile(pathinfo.save_dir, pathinfo.result_folder);
mkdir(pathinfo.saving_path);

 %% Run trimming sim in background
 
slCharacterEncoding('windows-1252')
eval('DLR_SFB880_Trim_Quat');

%% Definition of uncertain parameters

% List of uncertain parameters and their description:
% alpha_wfc_max:      maximal wfc (wing/fuselage) angle of attack (not included yet)
%
% C_mu:                     jet momentum coefficient (only for full flap config)
%
% V_TAS:                    initial true air velocity
%
% C_L0:                      lift coefficient at alpha=0
% The slope of the lift coeff C_L_ALPHA is the sum of:
% C_Lalpha:               slope of the lift coeff curve
% C_Lalpha_FL:          slope of the lift coeff curve due to full flap pos
% C_Lalpha_DN:        slope of the lift coeff curve due to full flap pos
%
% C_D0:                     drag coeff at alpha=0
% The linear coeff k1 is the sum of:
% k1:                         coeff to calculate the drag coeff (C_D)
% k1_FL:                    -||- due to full flap positions
% k1_DN:                  -||- due to Droop Nose
% The quadratic coeff k2 is the sum of:
% k2:                         coeff to calculate the drag coeff (C_D)
% k2_FL:                    -||- due to full flap positions
% k2_DN:                  -||- due to Droop Nose
%
% C_m0:                    pitching moment coefficient for zero angle of attack
%
% The slope of the pitching moment curve is the sum of:
% C_malpha:             pitching moment slope of the curve
% C_malpha_FL:        -||- due to full flap positions
% C_malpha_DN:      -||- due to Droop Nose

% Definition of the distribution of parameters
Q=MySimParamSet();
Q.add(MySimParameter('Cmu', ...
    UniformDistribution(0.033*0.95, 0.033*1.05),  'plot_name', 'C_{mu}'));

Q.add(MySimParameter('C_L0',...
    UniformDistribution( 0.344540120967742-0.15, 0.344540120967742+0.15), ...
    'plot_name','C_{L0}'));

Q.add(MySimParameter( 'C_Lalpha',...
    UniformDistribution( 4.734449604654664*0.95, 4.734449604654664*1.05), ...
    'plot_name', 'C_{L\alpha-CL}'));
Q.add(MySimParameter( 'C_Lalpha_FL', ...
    UniformDistribution( -1.236767277669634*1.05, -1.236767277669634*0.95), ...
    'plot_name','C_{L\alpha-FL}'));
Q.add(MySimParameter( 'C_Lalpha_DL', ...
    UniformDistribution( 0.310327867813788*0.95, 0.310327867813788*1.05),...
    'plot_name', 'C_{L\alpha-DN}'));

Q.add(MySimParameter('C_D0', ...
    UniformDistribution(0.020882442004759-0.05, 0.020882442004759+0.05),...
     'plot_name','C_{D0}'));

Q.add(MySimParameter('k1', ...
    UniformDistribution(0.010465923328436*0.9, 0.010465923328436*1.1),...
     'plot_name','k_{1-CL}'));
Q.add(MySimParameter('k1_FL',...
    UniformDistribution(-0.010465923328436*1.1, -0.010465923328436*0.9),...
     'plot_name','k_{1-FL}'));
Q.add(MySimParameter('k1_DN', ...
    UniformDistribution(-0.131599412034937*1.1, -0.131599412034937*0.9), ...
     'plot_name','k_{1-DN}'));

Q.add(MySimParameter('k2', ...
     UniformDistribution(0.035063886442427*0.9, 0.035063886442427*1.1), ...
      'plot_name','k_{2-CL}'));
Q.add(MySimParameter('k2_FL',...
     UniformDistribution(0.006179345655616*0.9, 0.006179345655616*1.1),...
      'plot_name','k_{2-FL}'));
Q.add(MySimParameter('k2_DN', ...
     UniformDistribution(0.020036923921946*0.9, 0.020036923921946*1.1),...
      'plot_name','k_{2-DN}'));

Q.add(MySimParameter('C_m0', ...
     UniformDistribution(-0.119375806451613-0.15, -0.119375806451613+0.15),...
      'plot_name','C_{m0}'));

Q.add(MySimParameter('C_malpha',...
    UniformDistribution(0.986319120553432*0.95, 0.986319120553432*1.05),...
     'plot_name','C_{m\alpha-CL}'));
Q.add(MySimParameter('C_malpha_FL',...
    UniformDistribution(-0.339182978441908*1.05, -0.339182978441908*0.95),...
     'plot_name','C_{m\alpha-FL}'));
Q.add(MySimParameter('C_malpha_DN', ...
    UniformDistribution(0.270784598950292*0.95, 0.270784598950292*1.05),...
     'plot_name','C_{m\alpha-DN}'));

%Q.set_all_to_mean(); %set all parameters to their mean values
%Q.set_not_fixed('C_L0'); %Set P{1} (C_L0) and {2} (C_Lalpha) random
%Q.set_not_fixed('C_Lalpha')

%% Initialize simulation
t_max=100;
t_min=0;
t_step=0.01;

num_params=Q.num_params;
param_names=Q.param_names;
model = highlift_eigen_init2(num_params, 't_min_max', [t_min, t_max], 't_step', t_step);
num_vars=model.model_info.num_vars;

%% Run reference(deterministic) solution

means=Q.mean;
params=Q.params2struct(means);

display('Running_reference (deterministic) solution');
start = tic;
[ref_sol, ref_solve_info]=funcall(model.model_info.solve_func, model, params);
t = toc(start);

 %% Caclulate solution at integration points
 
p_gPCE = 4;        %gPCE order
p_int = 5;      %integration order to calculate all other gpce coeff-s
%grid='full_tensor';
grid='smolyak';

%gpc expansion of parameters
[p_alpha, V_p, varserr]=  Q.gpc_expand();
V_u=gpcbasis_modify(V_p, 'p', p_gPCE);
%V_u=gpcbasis_create(V_p{1}, 'p', p_gPCE, 'full_tensor', false);

[x_ref, w] = gpc_integrate([], V_p, p_int, 'grid', grid);
M = gpcbasis_size(V_u, 1);
Qn = length(w);

a = gpc_evaluate(p_alpha, V_p, x_ref);
A=gpcbasis_evaluate(V_u, x_ref);
saving_fname=strvarexpand('$saving_path$$filesep$p_gpce_$p_gPCE$_p_int_$p_gPCE$$grid$Q_$Qn$');

%solve_options={'steptol', steptol, 'abstol', abstol}
u_ij=zeros(num_vars, Qn);
u_i_alpha = zeros(model.model_info.num_vars, M);
for j = 1:Qn
    display(strvarexpand('$j$/$Q$'));
    a_j = a(:, j);
    params_j=Q.params2struct(a_j);
    [u_i_j, model] = model_solve_s(model, params, 'saving_fname', saving_fname);
    u_ij(:,j)=u_i_j;
    %[u_i_j, model] = model_solve_s(model, params, 'solve_options', solve_options);
    x_j = x_ref(:, j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_u, x_j, 'dual', true);
    u_i_alpha = u_i_alpha + w(j) * u_i_j * psi_j_alpha_dual;
end

%% Projection

u_i_alpha_proj=u_i_alpha;
[u_mean_proj, u_var_proj]=gpc_moments(u_i_alpha_proj, V_u);

%% collocation (interpolation)

u_i_alpha_col=A\u_ij;
[u_mean_col, u_var_col]=gpc_moments(u_i_alpha_proj, V_u);

%% Evaluation of results

p_u = 3;
%[u_mean, u_var, time, other_info] = compute_response_surface_tensor_interpolate(init_func, solve_func, [], p_u, init_options, 'gpc_full_tensor', false, 'grid', 'smolyak', 'saving_path', saving_path);

%ind=(multiindex_order(V_u{2})>=3);
%u_i_alpha(:,ind)=0;
plot_u_mean_var(time, u_mean, u_var, 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)
plot_u_mean_var(time, u_mean(:,1:4), u_var(:,1:4), 'fill_color','magenta', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)


%show_mean_var('Interpolation, tensor (response surface)', u_mean, u_var);
plot_u_mean_var(time,u_mean, u_var, 'fill_color','red', 'line_color','black','transparency', 0.4, 'ylabels', ylabels)

% Plot the response surface
%hold off;
%plot_response_surface(u_i_alpha(1,:), V_u, 'delta', 0.01);

%u=gpc_evaluate(u_i_alpha, V_u, x);
%hold on; plot3(x(1,:), x(2,:), u(1,:), 'rx'); hold off;

%u_tensorcoll_i_alpha = u_i_alpha;
