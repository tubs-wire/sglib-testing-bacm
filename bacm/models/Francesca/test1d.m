clear
clc
clf

V_y = gpcbasis_create('H', 'p', 2);
gpcbasis_polynomials(V_y)

xi_s = 0.03;
xi_m = -3;
q_true = 2;
sigma_eps = 0.3*xi_s;

y_i_alpha = xi_s * [xi_m^2+1, -2*xi_m, 1];
y_m = gpc_evaluate(y_i_alpha, V_y, q_true);

xi = linspace(-4, 6);
plot(xi, gpc_evaluate(y_i_alpha, V_y, xi), xi, xi_s * (xi-xi_m).^2)
hold all;

Y_func = funcreate(@gpc_evaluate, y_i_alpha, V_y, @funarg);

E=generate_stdrn_simparamset(sigma_eps);
E_func = funcreate(@germ2params, E, @funarg);
plot(xi, repmat(y_m + sigma_eps*[-1;0;1], 1, length(xi)))


Q=MySimParamSet();
Q.add('q', NormalDistribution(0, 1));
V_q=Q.get_germ();

q_samples = Q.sample(10000);
plot_density(q_samples);



[e_i_alpha, V_e] = E.gpc_expand();
%%
[Q_i_alpha, V_q] = Q.gpc_expand();
p_phi=2;
p_pn=4;
p_int_mmse=20;
p_int_proj=20;

% Calculate cpu time for the updating
[Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q, y_m, E_func, V_e, p_phi, p_int_mmse, p_pn, p_int_proj, 'int_grid', 'full_tensor');
[Q_post_mean, Q_post_var]=gpc_moments(Qn_i_beta, V_qn);
%%
N=10000;
Q_samples_post=gpc_sample(Qn_i_beta, V_qn, N);
plot_density(Q_samples_post)

line([q_true, q_true], ylim, [-0.1,-0.1], 'LineWidth',2, 'Color',[.8 .8 .8])
