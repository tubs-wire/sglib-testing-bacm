% Set random variable and make a gPCE of it
function [Q, x_i, xi_i, w_i, V_u]=pre_fra_ipw(gpc_order,mux,sigmax)

% Q=MySimParamSet();
Q=SimParamSet();
Q.add('X', NormalDistribution(mux,sigmax));
% Q.add('f', NormalDistribution(0.5,0.1));

% Set approximating subspace for the proxi model
V_q=Q.get_germ();
gpc_order=4;
V_u=gpcbasis_modify(V_q,'p', gpc_order);
% 
% Call integration rule
p_int=gpc_order+1;
[xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

x_i=Q.germ2params(xi_i);

