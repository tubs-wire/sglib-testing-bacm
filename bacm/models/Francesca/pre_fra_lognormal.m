% Set random variable and make a gPCE of it
function [Q, q_i, xi_i, w_i, V_u]=pre_fra_lognormal(gpc_order)
% Calculated lognormal parameters mu and sigma for E
mean_E = 50;
var_E = 10;
mu_E = log((mean_E^2)/sqrt(var_E+mean_E^2));
sig_E = sqrt(log(var_E/(mean_E^2)+1));

% Calculated lognormal parameters mu and sigma for f
mean_f = 1;
var_f = 0.2;
mu_f = log((mean_f^2)/sqrt(var_f+mean_f^2));
sig_f = sqrt(log(var_f/(mean_f^2)+1));

Q=MySimParamSet();
Q.add('E', LogNormalDistribution(mu_E, sig_E));
Q.add('f', LogNormalDistribution(mu_f, sig_f));

% Set approximating subspace for the proxi model
V_q=Q.get_germ();
V_u=gpcbasis_modify(V_q,'p', gpc_order);
% 
% Call integration rule
p_int=gpc_order+1;
[xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

q_i=Q.germ2params(xi_i);

