clear
clc
clf
%% optional inputs
%Choose update method( EnKF1 = with sampling / EnKF2 = sampling free )
update_methods = {'MMSE', 'MCMC', 'KF'};
update_method = update_methods{1};
% Number of samples
N=10000;
%% MMSE inputs
% Estimator order
        p_phi=2;
        % gPCE order of the assimilated Q 
        p_pn=4;
        % Integration orders
        p_int_mmse=20;
        p_int_proj=20;

%% Prior param definition
% Define through simparamset (now it's only one parameter though)
Q=MySimParamSet();
Q.add('q', NormalDistribution(0, 1));
% gpCE basis definition
V_q=Q.get_germ();

%% Define observation function xi->y with gPCE
% gPCE basis
V_y = gpcbasis_create('H', 'p', 2);
%gpcbasis_polynomials(V_y)
xi_s = 0.03;
% Minimum point (and zero point) of the response
xi_m = -3;
% PCE coefficients
y_i_alpha = xi_s * [xi_m^2+1, -2*xi_m, 1];
xi = linspace(-4, 6);
figure
hold all;
%h_y=plot(xi, gpc_evaluate(y_i_alpha, V_y, xi), xi, xi_s * (xi-xi_m).^2);
h_y=plot(xi, gpc_evaluate(y_i_alpha, V_y, xi));
xlim([0,5])
xlabel('\xi = q')
ylabel('y')
h_pdf_xi=plot(xi, gpcgerm_pdf(V_q, xi));

%% Define truth
% True parameter value
q_true = 2;
% True response
y_true = gpc_evaluate(y_i_alpha, V_y, q_true);

%% Generate synthetic observation
% Measurement error
sigma_eps = 0.3*xi_s;
y_m = y_true;

%% Update
% Map from germ to observable
Y_func = funcreate(@gpc_evaluate, y_i_alpha, V_y, @funarg);
% Measurement error model
E=generate_stdrn_simparamset(sigma_eps);
E_func = funcreate(@germ2params, E, @funarg);
[e_i_alpha, V_e] = E.gpc_expand();
% Plot observation
plot(xi, repmat(y_m + sigma_eps*[-1;0;1], 1, length(xi)))
% Plot pdf of xi
h_pdf_xi=plot(xi, gpcgerm_pdf(V_q, xi));
%%
switch(update_method)
    %%
    case 'MMSE'
        %gPCE of Q
        [Q_i_alpha, V_q] = Q.gpc_expand();
        
        % gPCE of the assimilated Q
        [Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q, y_m, E_func, V_e, p_phi, p_int_mmse, p_pn, p_int_proj, 'int_grid', 'full_tensor');
        % Posterior mean and var
        [Q_post_mean, Q_post_var]=gpc_moments(Qn_i_beta, V_qn);
        % sample from posterior dist
        Q_samples_post=gpc_sample(Qn_i_beta, V_qn, N);
        
        %% MCMC from proxi model
    case 'MCMC'
        % get distribution of the germ
        [~, dist]=gpc_registry('get', V_y{1});
        prior_dist = {dist};
        % Proposal distribution
        prop_dist={NormalDistribution(0,1)};
        % MCMC update
        [germ_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m, E, 'prop_dist', prop_dist);
        % Map to Q
        Q_samples_post = Q.germ2params(germ_samples_post);
        % Posterior mean
        Q_post_mean = mean(Q_samples_post);
        % Posterior var
        Q_post_var  = var(Q_samples_post);
        
        %% Updating with the Ensemble Kalman filter
%     case 'EnKF1'
%         % Posterior samples
%         Q_samples_post=update_q_enKF(Q, E, y_i_alpha, V_y, y_m, N);
%         % Posterior mean
%         Q_post_mean = mean(Q_samples_post);
%         % Posterior var
%         Q_post_var  = var(Q_samples_post);
        %% updating the coefficients (without samples) - Bojana's update
    case 'EnKF2'
        %gPCE of the updated germ
        XI=generate_stdrn_simparamset(ones(Q.num_params));
        [xi_i_alpha,V_xi]=XI.gpc_expand();
        
        [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(E,xi_i_alpha, V_xi, y_i_alpha, V_y, y_m);
        %sample from the updated germ
        germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
        % Map to Q
        Q_samples_post = Q.germ2params(germ_samples_post);
        % Posterior mean
        Q_post_mean = mean(Q_samples_post);
        % Posterior var
        Q_post_var  = var(Q_samples_post);
        
end

%% Write and plot update results
% Display results
display(strvarexpand('UPDATE RESULTS WITH $update_method$'));
display(strvarexpand('q_true=$q_true$'));
display(strvarexpand('Q_post_mean=$Q_post_mean$'));
display(strvarexpand('Q_prior_var=$Q.var$'));
display(strvarexpand('Q_post_var=$Q_post_var$'));
% Plot results
Q_samples_prior=Q.sample(N);
[p1,h1]=plot_density(Q_samples_prior);
[p2,h2]=plot_density(Q_samples_post);
h_pr=plot(p1,h1, 'b', 'LineWidth', 1);
h_po=plot(p2,h2, 'r', 'LineWidth', 1);
h_truth=line([q_true, q_true], ylim, [-0.1,-0.1], 'LineWidth',2, 'Color',[.8 .8 .8]);
legend([h_y, h_pr, h_po, h_truth],'response', 'prior PDF', 'posterior PDF', 'true q')
title(strvarexpand('UPDATE WITH $update_method$'));

