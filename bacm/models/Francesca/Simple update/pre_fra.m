% Set random variable and make a gPCE of it
function [Q, x_i, xi_i, w_i, V_u]=pre_fra(gpc_order)

Q=MySimParamSet();
Q.add('E', NormalDistribution(50,10));
Q.add('f', NormalDistribution(0.5,0.1));

% Set approximating subspace for the proxi model
V_q=Q.get_germ();
gpc_order=4;
V_u=gpcbasis_modify(V_q,'p', gpc_order);
% 
% Call integration rule
p_int=gpc_order+1;
[xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

x_i=Q.germ2params(xi_i);

