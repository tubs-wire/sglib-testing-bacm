%function post_enKF_fra()

%% Optional inputs
%choose update method:
update_method='MCMC';
% update_method='MMSE';
% update_method='enKF';
    
%choose sample number for posterior samples
N=10000;
% Define prior distributions, and approximating basis
p_gpce=4;
[Q, q_i, xi_i, w_i, V_y]=pre_fra(p_gpce);
% Show integration points where the model has  to be computed
display(q_i);
% number of observation points
n_meas=2;
% Apply map to displacement for update?
 map_disp='none';
% map_disp='log';
% map_disp='inverse';
% map_disp='log_inverse';

%resp_surf_type='contour';
resp_surf_type='surf';

%% Define maps to be applied 
switch(map_disp)
    case 'log'
    %LOG OF DISPLACEMENT
% apply a map on the displacement field for better update
    response_map=funcreate(@log, @funarg);
% Define the inverse map 
    inv_response_map=funcreate(@exp, @funarg);
% Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(1./u);
    case 'inverse'
    %INVERSE OF DISPLACEMENT
    % apply a map on the displacement field for better update
    response_map=@(u)(1./u);
% Define the inverse map 
    inv_response_map=@(u)(1./u);
% Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(-1*u.^-2);
    case 'log_inverse'
       %LOG OF INVERSE DISPLACEMENT
% apply a map on the displacement field for better update
    response_map=@(u)(log(1./u));
% Define the inverse map 
    inv_response_map=@(u)(1./exp(u));
% Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(1./u*-1*u.^-2);
    
    case 'none'
% apply a map on the displacement field for better update
    response_map=@(x)(x); %for no map;
% Define the inverse map 
    inv_response_map=@(x)(x); %for no map;
% Gradient of the map, so that the maped error can be calculated
    J_func=@(u)(ones(length(u),1)); %for no map;
end

%% Define true response
q_true = [51; 0.52]; %artificial truth close to the mean of the distribution
y_true = [2.4625; 1.4218]; %response of the system for the artificial truth
%% Define the measurement
sigma_true_err=0.01;
% make a seperate simparamset for the measurement errors
E_true=generate_stdrn_simparamset(ones(n_meas,1)*sigma_true_err);
y_m = y_true + E_true.sample(1);% measurement of the response
y_m = funcall(response_map, y_m);

%% Generate the error model
% Let's assume a the same error as the real measurement error
sigma_eps = 1.*sigma_true_err;
% make a seperate simparamset for the measurement errors
maped_sigmas=funcall(J_func, y_m)*sigma_eps;
E=generate_stdrn_simparamset(maped_sigmas);

%% Generate proxi model
% Response from integration points
%A = [4.7236 2.9571 2.1928 1.7167 5.0255 3.146 2.333 1.8265 5.3204 3.3306 2.4699 1.9336 5.652 3.5382 2.6238 2.0541]; %measurements from the first sensor
%B = [2.7309 1.7096 1.2677 0.9925 2.9959	1.8754	1.3908	1.0888	3.2577 2.0394 1.5123 1.184 3.555 2.2257 1.6505 1.2921]; %measurements from the second sensor
A = [5.5173	3.2444	2.3648	1.8604	1.5049	5.6838	3.3423	2.4361	1.9165	1.5503	5.8375	3.4326	2.502	1.9683	1.5922	5.9943	3.5248	2.5692	2.0212	1.635	6.1717	3.6292	2.6452	2.081	1.6834];
B = [3.0866	1.8151	1.323	1.0408	0.8419	3.2303	1.8995	1.3845	1.0892	0.8811	3.3637	1.978	1.4417	1.1342	0.9175	3.5006	2.0585	1.5004	1.1804	0.9548	3.6565	2.1502	1.5672	1.2329	0.9973];
u_z=[A;B];
% map displacement field
y=funcall(response_map, u_z);
% calculate gPCE of the displacement
y_i_alpha = calculate_gpc_coeffs(V_y, xi_i, y, w_i, 'method', 'project');
% check the surrogate model
y_true_gpc=funcall(inv_response_map, ...
    gpc_evaluate(y_i_alpha, V_y, Q.params2germ(q_true)));
% Define map from germ to maped observable
Y_func = @(xi)gpc_evaluate(y_i_alpha, V_y, xi);
% check whether surrogate model is ok:
display(y_true_gpc-y_true);
% Plot response surface with a contour plot
figure
myplot_response_surface(y_i_alpha, V_y,...
    'plot_fix_response', y_m, 'name_of_RVs', Q.param_plot_names,...
    'germ2param', @(xi)Q.germ2params(xi), 'type', resp_surf_type)
h_resp = gca;
h_resp.FontSize=14;

%Plot response surface for first sensor
figure
myplot_response_surface(y_i_alpha(1,:), V_y,...
    'plot_fix_response', y_m(1), 'name_of_RVs', Q.param_plot_names,...
    'germ2param', @(xi)Q.germ2params(xi), 'type', resp_surf_type);
h_resp1=gca;
h_resp1.FontSize=14;

%Plot response surface for second sensor
figure
myplot_response_surface(y_i_alpha(2,:), V_y,...
    'plot_fix_response', y_m(2), 'name_of_RVs', Q.param_plot_names,...
    'germ2param', @(xi)Q.germ2params(xi), 'type', resp_surf_type);
h_resp2=gca;
h_resp2.FontSize=14;

%% Statistics
[y_mean, y_var] = gpc_moments(y_i_alpha, V_y);
% plot_u_mean_var(1:n_u, u_mean, u_var, 'line_color', 'black',...
%     'transparency', 0.4, 'fill_color', 'b')

switch update_method
    case 'enKF'
        %% By updating the coefficients (without samples) - Bojana's update
        % Update germ
        [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(Q, E, y_i_alpha, V_y, y_m);
        % Sample from germ
        germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
        % map to parameter
        Q_samples_post=Q.germ2params(germ_samples_post);
        %Posterior mean
        Q_post_mean=mean(Q_samples_post');
        %Posterior variance
        Q_post_var=var(Q_samples_post');
      
     case 'MMSE'
        %% with Elmar's cool MMSE
        E_func = @(xi)(E.germ2params(xi));
        [e_i_alpha, V_e] = E.gpc_expand();
        [Q_i_alpha, V_q] = Q.gpc_expand();
%         p_phi=1;
%         p_pn=2;
%         p_int_mmse=3;
%         p_int_proj=4;
         p_phi=4;
         p_pn=5;
         p_int_mmse=20;
         p_int_proj=20;
        [Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q,...
            y_m, E_func, V_e, p_phi, p_int_mmse, ...
            p_pn, p_int_proj, 'int_grid', 'full_tensor');
        [Q_post_mean, Q_post_var]=gpc_moments(Qn_i_beta, V_qn);
        Q_samples_post=gpc_sample(Qn_i_beta, V_qn, N);
    case 'MCMC'
        % Get distribution of the germ
        syschars=V_y{1};
        prior_dist=cell(length(syschars),1);
        for i=1:length(syschars)
            [~, prior_dist{i}]=gpc_registry('get', syschars(i));
        end
        % run the update
        [germ_samples_post, acc_rate]=...
            gpc_MCMC(N, Y_func, prior_dist, y_m, E);
        display(acc_rate)
        Q_samples_post = Q.germ2params(germ_samples_post);
        %Posterior mean
        Q_post_mean=mean(Q_samples_post');
        %Posterior variance
        Q_post_var=var(Q_samples_post');
 end
display(strvarexpand...
    ('Posterior mean=$Q_post_mean$, Posterior variance=$Q_post_var$'));
Q_samples_prior=Q.sample(N);

plot_grouped_scatter({Q_samples_prior, Q_samples_post}, 'Color', 'bc', ...
    'Legends', {'Prior', 'Posterior'}, 'Labels', Q.param_names,...
    'FontSize', 15);
%g_samples=[Q_samples_prior, Q_samples_post];
% group=[ones(size(Q_samples_prior,2),1);2*ones(size(Q_samples_post,2),1)];
% figure
% gplotmatrix(g_samples', [] ,group ,'bmg','.ox',[],[],[],...
%     Q.param_plot_names,Q.param_plot_names);
% figure
% gplotmatrix(Q_samples_post')
hold(h_resp, 'all');
plot(h_resp, Q_samples_post(1,:), Q_samples_post(2,:), '.r')
hold(h_resp, 'off');
view(h_resp, [0, 0, -1])
