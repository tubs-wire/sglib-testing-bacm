clear
clc
clf
%% Define prior
Q=SimParamSet();
Q.add(SimParameter('p1', NormalDistribution(0,2)));
[Q_i_alpha, V_Q]=Q.gpc_expand;

%% Synthetic measurement
% true parameters
q_true = 0.8;

% Measurement
sigma_eps = 0.3;
n_meas=5;
E=generate_stdrn_simparamset(sigma_eps*ones(n_meas,1));
y_m=binfun(@plus, q_true^2, E.sample(1));

%% gpc of the measurable

V_Y=gpcbasis_modify(V_Q, 'p', 2);
Y_i_alpha=gpc_projection(@(q)(ones(n_meas,1)*q.^2), V_Y, 3);
qm=zeros(4,1);
for i=1:4;
Y_func = @(xi)(gpc_evaluate(Y_i_alpha, V_Y, xi)).^i;

%% MMSE inputs

E_func = @(xi)(E.germ2params(xi));
% PCE of the error
[e_i_alpha, V_e] = E.gpc_expand();


% set MMSE integration orders
p_phi=3;
p_Q = gpcbasis_info(V_Q, 'total_degree');
p_gpc= gpcbasis_info(V_Y, 'total_degree');
p_int_mmse=max(p_gpc*p_phi+1,ceil((p_Q+p_phi*p_gpc+1)/2));

% Update
[Qm_func, ~, qm(i), ~] = my_mmse_gpc(Q_i_alpha, ...
    Y_func, V_Q, y_m, E_func, V_e, p_phi, p_int_mmse,...
    'int_grid', 'full_tensor');
end

% get distribution of the germ
N=1000;
[~, dist]=gpc_registry('get', V_Y{1});
prior_dist = {dist};
prop_dist={NormalDistribution(0,0.5)};
% MCMC update
[xi_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m,...
    E, 'prop_dist', prop_dist);
q_post = Q.germ2params(xi_post);
q_post_mean = mean(q_post);
q_post_var  = var(q_post);

%% Plot

    % plot prior and posterior histograms
    figure
    subplot(2,1,1)
    h1=histogram(Q.sample(N));
    hold on
    h2=histogram(q_post);
    h1.Normalization = 'pdf';
    h1.BinWidth = 0.05;
    h1.EdgeColor =h1.FaceColor;
    h2.Normalization = 'pdf';
    h2.BinWidth = 0.05;
    h2.EdgeColor =h2.FaceColor;
    h2.FaceAlpha =0.4;
    
    y_limits=ylim;
    plot([f_cm_post_mean, f_cm_post_mean], y_limits, 'k', 'LineWidth', 2)
    plot([f_cm_true, f_cm_true], y_limits, 'k--', 'LineWidth', 3)
    ylim(y_limits);
    xlim([0,10]);
    legend('prior', 'posterior', 'posterior mean', 'true value');
    xlabel('f_{cm}')
    ylabel('Prior and posterior probabilities');
    switch update_method_flag
        case 'MMSE'
            title(strvarexpand('UPDATE RESULTS WITH $update_method_flag$, esimator polynomial order: $p_phi$'));
        otherwise
            title(strvarexpand('UPDATE RESULTS WITH $update_method_flag$'));
    end