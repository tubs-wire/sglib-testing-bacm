
%% Define parameter
Q=MySimParamSet();
Q.add('E', NormalDistribution(50,10));

% gpc expansion
[e_i_alpha, V_e] = Q.gpc_expand();
gpc_order=4;
V_u=gpcbasis_modify(V_e,'p', gpc_order);

% % Call integration rule
% p_int=gpc_order+1;
% [xi_i, w_i] = gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');
% 
% x_i=Q.germ2params(xi_i);

%% Define hyperparameter
% Define prior distributions of coefficient of model error
E=SimParamSet();
E.add(SimParameter('e0', NormalDistribution(e_i_alpha(1), 0.2*e_i_alpha(1))));

% gpc expansion
[e_i_alpha, V_q] = Q.gpc_expand();
gpc_order=4;
V_z=gpcbasis_modify(V_q,'p', gpc_order);

%%

% norm of the basis
h_beta=gpcbasis_norm(V_u);
% Take the tensor product of the 'p_int' point integration rule
p_gpc=gpcbasis_info(V_u, 'max_degree');
p_int=p_gpc+1;
[xi,w]=gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

% Number of the integration points
R=length(w);
switch method
    case 'projection'
        M=gpcbasis_size(V_u,1);
        % Initiate memory for coefficients
        u_i_alpha=zeros(n_out,M);
        % 6) Solve $A(u(\xi_i);\xsi_i)=f(\xi_i)$ for all $i=1.. Q$
        for i=1:R  %for each integration point
            xi_i=xi(:,i);
            e_i=germ2param_func(xi_i);
            u_i=solve_func(e_i); %integration points are values of the coefficients
            % 7) Evaluate basis functions $\psi_\alpha$ at \xi_i
            Psi_i=gpcbasis_evaluate(V_u,xi_i, 'dual', true);
            w_i=w(i);
            % 8) Compute coefficient from $u_\beta=\frac{1}{h_\beta}\sum_{i=1}^Q w_i u_i \psi_\beta(\xi_i)$
            u_i_alpha=u_i_alpha+bsxfun(@times, u_i*w_i*Psi_i, 1./h_beta');
        end
    case 'regression'
        %% The same but with interpolation
        A = gpcbasis_evaluate(V_u, xi, 'dual', true);
        U=zeros(R, n_out);
        for j=1:R
            xi_j=xi(:,j);
            e_j=germ2param_func(xi_j);
            u_j=solve_func(e_j');
            U(j, :)=u_j;
        end
        u_i_alpha=(A\U)';