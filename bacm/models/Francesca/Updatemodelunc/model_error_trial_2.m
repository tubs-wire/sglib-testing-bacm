%function model_error_trial()
%% Load data
% load reference displacement
S=load('F:\noemi\sglib\sglib-testing-bacm\bacm\models\Francesca\Updatemodelunc\displFEM2.mat');
disp_ref=S.disp_ref;
% load displacement from simplified model 
S=load('F:\noemi\sglib\sglib-testing-bacm\bacm\models\Francesca\Updatemodelunc\displFEM0.mat');
disp=S.disp;
% Load error of the displacement of simplified model and the corresponding coordinates (Z_ORIG)
S=load('F:\noemi\sglib\sglib-testing-bacm\bacm\models\Francesca\Updatemodelunc\z_and_eps_z_FEM20.mat');
z_orig=S.z_orig;

%% Initiate model error input 1
err_true=disp_ref-disp;
% Calculate the true model error of the simplified model (deviation from
% reference model)
[eps_approx_func, e_i_alpha, V_e]=true_error_model(err_true, z_orig);
eps_approx=eps_approx_func(e_i_alpha, z_orig);
%
plot(z_orig, err_true);
hold on
plot(z_orig, eps_approx, 'rx', 'LineWidth', 2);
legend('true error model', 'interpolated error model')

%% Initiate model error input 2
% Define prior distributions of coefficient of model error
E=SimParamSet();
E.add(SimParameter('e0', NormalDistribution(0, (abs(e_i_alpha(1) )))));
E.add(SimParameter('e1', NormalDistribution(0, (abs(e_i_alpha(2) )))));
E.add(SimParameter('e2', NormalDistribution(0, (abs(e_i_alpha(3) )))));
E.add(SimParameter('e3', NormalDistribution(0, (abs(e_i_alpha(4) )))));
E.add(SimParameter('e4', NormalDistribution(0, (abs(e_i_alpha(5) )))));


%% Load simplified model's displacement field

solve_func=@(e)(binfun(@plus, disp , eps_approx_func(e', z_orig)'));

%% Define approximating gpc_basis
p_gpc=5;
V_E=E.get_germ;
V_u=gpcbasis_modify(V_E, 'p', p_gpc);

%% Calculate gpc expansion of the displacement+model error from model error coefficient uncertainties
n_out=length(z_orig);
germ2param_func=@(xi)E.germ2params(xi);
u_i_alpha=project_to_gpc_basis(V_u, solve_func, n_out, germ2param_func, 'projection');

%% 9. Get statistics of $u_p$ and evaluate proxi model at any parameter values $q$

% Compute mean and variance of $u_p$:
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);
plot_u_mean_var(z_orig, u_mean, u_var, 'xlabel', 'z');
hold on
plot(z_orig, disp, 'r', 'LineWidth', 3)

% % Compute sensitivity of $u$ to the uncertainties of $k$ and $m$:
% [u_part_vars, I, sobol_index]=gpc_sobol_partial_vars(u_alpha, V_u, 'max_index', 1);
% plot(z_orig, u_part_vars)

%% Generate some artificial truth we want to identify which is (for the
% moment) close to the mean of the prior model
n_meas=52;
% y = datasample(disp,n_meas); %measure without measurement error
y = disp_ref;
%define measurement error
sigma_true_err=0.05*y; %it should be also dependant from x
% make a serperate simparamset for the measurement errors
R_true=generate_stdrn_simparamset(sigma_true_err);
y_m = y + R_true.sample(1); %measure with measurement error;
% plot reference displacement (the truth)
plot(z_orig, disp_ref, 'c', 'LineWidth', 3);
% plot the measurements
plot(z_orig, y_m, 'cx')

%% Generate the error model
% Let's assume a bit biger error variance then the one used for the synthetic measurement
sigma_eps = 1.1*sigma_true_err;
% make a serperate simparamset for the measurement errors
R=generate_stdrn_simparamset(sigma_eps);

%%
%%MCMC
V_y=V_u;
y_i_alpha=u_i_alpha;
Y_func=@(xi)gpc_evaluate(y_i_alpha, V_y, xi);

syschars=V_y{1};
prior_dist=cell(length(syschars),1);
for i=1:length(syschars)
[~, prior_dist{i}]=gpc_registry('get', syschars(i));
end
 % run the update
 N=5000;
[germ_samples_post, acc_rate]=gpc_MCMC(N, Y_func, prior_dist, y_m, R);
display(acc_rate)
E_samples_post = E.germ2params(germ_samples_post);
%Posterior mean
E_post_mean=mean(E_samples_post');
%Posterior variance
E_post_var=var(E_samples_post');
u_post_mean=solve_func(E_post_mean');
plot(z_orig, u_post_mean);
hold on
plot(z_orig, disp_ref, 'r');
plot(z_orig, disp, 'c')
u_samples_post=solve_func(E_samples_post);
plot(z_orig, u_samples_post)
%% By updating the coefficients (without samples) - Bojana's update
% Update germ
[xi_a_i_alpha, V_xi_a] = gpc_update_enKF(E, R, u_i_alpha, V_u, y_m);
% Sample from germ
germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, 100);
% map to parameter
E_samples_post=E.germ2params(germ_samples_post);
%Posterior mean
E_post_mean=mean(E_samples_post');
%Posterior variance
E_post_var=var(E_samples_post');
%end
