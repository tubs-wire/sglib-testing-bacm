function u_i_alpha=project_to_gpc_basis(V_u, solve_func, n_out, germ2param_func, method)
% norm of the basis
h_beta=gpcbasis_norm(V_u);
% Take the tensor product of the 'p_int' point integration rule
p_gpc=gpcbasis_info(V_u, 'max_degree');
p_int=p_gpc+1;
[xi,w]=gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

% Number of the integration points
R=length(w);
switch method
    case 'projection'
        M=gpcbasis_size(V_u,1);
        % Initiate memory for coefficients
        u_i_alpha=zeros(n_out,M);
        % 6) Solve $A(u(\xi_i);\xsi_i)=f(\xi_i)$ for all $i=1.. Q$
        for i=1:R  %for each integration point
            xi_i=xi(:,i);
            e_i=germ2param_func(xi_i);
            u_i=solve_func(e_i); %integration points are values of the coefficients
            % 7) Evaluate basis functions $\psi_\alpha$ at \xi_i
            Psi_i=gpcbasis_evaluate(V_u,xi_i, 'dual', true);
            w_i=w(i);
            % 8) Compute coefficient from $u_\beta=\frac{1}{h_\beta}\sum_{i=1}^Q w_i u_i \psi_\beta(\xi_i)$
            u_i_alpha=u_i_alpha+bsxfun(@times, u_i*w_i*Psi_i, 1./h_beta');
        end
    case 'regression'
        %% The same but with interpolation
        A = gpcbasis_evaluate(V_u, xi, 'dual', true);
        U=zeros(R, n_out);
        for j=1:R
            xi_j=xi(:,j);
            e_j=germ2param_func(xi_j);
            u_j=solve_func(e_j');
            U(j, :)=u_j;
        end
        u_i_alpha=(A\U)';
end