function [eps_true_func, e_i_alpha, V_e]=true_error_model(err_true, z_orig)
% Initiate SimParamSet
Z=SimParamSet();
% Add parameters k and m to the set
Z.add(SimParameter('z', UniformDistribution(0,43485)));

% Define approximating subspace
V=Z.get_germ;
p_gpc=4;
V_e=gpcbasis_modify(V,'p', p_gpc, 'full_tensor', false);

% interpolating function to get error at any coordinate
solve_func=@(z)(spline(z_orig, err_true, z))';
germ2param_func=@(xi)Z.germ2params(xi);
e_i_alpha=project_to_gpc_basis(V_e, solve_func, 1, germ2param_func, 'regression');

eps_true_func=@(e_i_alpha, z)gpc_evaluate(e_i_alpha, V_e, Z.params2germ( z'));
end