function model_error_trial()
%% Load data
% load reference displacement
S=load('C:\Users\Francesca\code\sglib-testing-bacm\sglib-testing\displFEM2.mat');
disp_ref=S.disp_ref;
% load displacement from simplified model 
S=load('C:\Users\Francesca\code\sglib-testing-bacm\sglib-testing\displFEM0.mat');
disp=S.disp;
% Load error of the displacement of simplified model and the corresponding coordinates (Z_ORIG)
S=load('C:\Users\Francesca\code\sglib-testing-bacm\sglib-testing\z_and_eps_z_FEM20.mat');
z_orig=S.z_orig;

%% Initiate model error input 1
err_true=disp_ref-disp;
% Calculate the true model error of the simplified model (deviation from
% reference model)
[eps_approx_func, e_i_alpha, V_e]=true_error_model(err_true, z_orig);
eps_approx=eps_approx_func(e_i_alpha, z_orig);
%
plot(z_orig, err_true);
hold on
plot(z_orig, eps_approx, 'rx', 'LineWidth', 2);
legend('true error model', 'interpolated error model')

%% Initiate model error input 2
% Define prior distributions of coefficient of model error
E=SimParamSet();
% E.add(SimParameter('e0', NormalDistribution(e_i_alpha(1), (0.05*e_i_alpha(1))^2)));
% E.add(SimParameter('e1', NormalDistribution(e_i_alpha(2), (0.3*e_i_alpha(2))^2)));
% E.add(SimParameter('e2', NormalDistribution(e_i_alpha(3), (0.3*e_i_alpha(3))^2)));
% E.add(SimParameter('e3', NormalDistribution(e_i_alpha(4), (0.3*e_i_alpha(4))^2)));
% E.add(SimParameter('e4', NormalDistribution(e_i_alpha(5), (0.3*e_i_alpha(5))^2)));
E.add(SimParameter('e0', NormalDistribution(0, (abs(e_i_alpha(1) )))));
E.add(SimParameter('e1', NormalDistribution(0, (abs(e_i_alpha(2) )))));
E.add(SimParameter('e2', NormalDistribution(0, (abs(e_i_alpha(3) )))));
E.add(SimParameter('e3', NormalDistribution(0, (abs(e_i_alpha(4) )))));
E.add(SimParameter('e4', NormalDistribution(0, (abs(e_i_alpha(5) )))));


%% Load simplified model's displacement field

solve_func=@(e)(disp + (eps_approx_func(e', z_orig))');

%% Define approximating gpc_basis
p_gpc=5;
V_E=E.get_germ;
V_u=gpcbasis_modify(V_E, 'p', p_gpc);

%% Calculate gpc expansion of the displacement+model error from model error coefficient uncertainties
n_out=length(z_orig);
germ2param_func=@(xi)E.germ2params(xi);
u_i_alpha=project_to_gpc_basis(V_u, solve_func, n_out, germ2param_func, 'projection');

%% 9. Get statistics of $u_p$ and evaluate proxi model at any parameter values $q$

% Compute mean and variance of $u_p$:
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);
plot_u_mean_var(z_orig, u_mean, u_var, 'xlabel', 'z');
hold on
plot(z_orig, disp, 'r', 'LineWidth', 3)

% % Compute sensitivity of $u$ to the uncertainties of $k$ and $m$:
[u_part_vars, I, sobol_index]=gpc_sobol_partial_vars(u_alpha, V_u, 'max_index', 1);
plot(z_orig, u_part_vars)

%% Generate some artificial truth we want to identify which is (for the
% moment) close to the mean of the prior model
n_meas=52;
% y = datasample(disp,n_meas); %measure without measurement error
y = disp_ref;
%define measurement error
sigma_true_err=3; %it should be also dependant from x
y_m = y + normal_sample(n_meas, 0, sigma_true_err); %measure with measurement error;
 plot(z_orig, disp_ref, 'c', 'LineWidth', 3);
plot(z_orig, y_m, 'cx')
%% Generate the error model
% Let's assume a bit biger error variance then the one used for the synthetic measurement
sigma_eps = 1.1*sigma_true_err;
% make a serperate simparamset for the measurement errors
R=generate_stdrn_simparamset(ones(n_meas,1)*sigma_eps);


%% By updating the coefficients (without samples) - Bojana's update
% Update germ
[xi_a_i_alpha, V_xi_a] = gpc_update_enKF(E, R, u_i_alpha, V_u, y_m);
% Sample from germ
germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, 100);
% map to parameter
E_samples_post=E.germ2params(germ_samples_post);
%Posterior mean
E_post_mean=mean(E_samples_post');
%Posterior variance
E_post_var=var(E_samples_post');
end
function [eps_true_func, e_i_alpha, V_e]=true_error_model(err_true, z_orig)
% Initiate SimParamSet
Z=SimParamSet();
% Add parameters k and m to the set
Z.add(SimParameter('z', UniformDistribution(0,43485)));

% Define approximating subspace
V=Z.get_germ;
p_gpc=4;
V_e=gpcbasis_modify(V,'p', p_gpc, 'full_tensor', false);

% interpolating function to get error at any coordinate
solve_func=@(z)(spline(z_orig, err_true, z))';
germ2param_func=@(xi)Z.germ2params(xi);
e_i_alpha=project_to_gpc_basis(V_e, solve_func, 1, germ2param_func, 'regression');

eps_true_func=@(e_i_alpha, z)gpc_evaluate(e_i_alpha, V_e, Z.params2germ( z'));
end
function u_i_alpha=project_to_gpc_basis(V_u, solve_func, n_out, germ2param_func, method)
% norm of the basis
h_beta=gpcbasis_norm(V_u);
% Take the tensor product of the 'p_int' point integration rule
p_gpc=gpcbasis_info(V_u, 'max_degree');
p_int=p_gpc+1;
[xi,w]=gpc_integrate([], V_u, p_int, 'grid', 'full_tensor');

% Number of the integration points
R=length(w);
switch method
    case 'projection'
        M=gpcbasis_size(V_u,1);
        % Initiate memory for coefficients
        u_i_alpha=zeros(n_out,M);
        % 6) Solve $A(u(\xi_i);\xsi_i)=f(\xi_i)$ for all $i=1.. Q$
        for i=1:R  %for each integration point
            xi_i=xi(:,i);
            e_i=germ2param_func(xi_i);
            u_i=solve_func(e_i); %integration points are values of the coefficients
            % 7) Evaluate basis functions $\psi_\alpha$ at \xi_i
            Psi_i=gpcbasis_evaluate(V_u,xi_i, 'dual', true);
            w_i=w(i);
            % 8) Compute coefficient from $u_\beta=\frac{1}{h_\beta}\sum_{i=1}^Q w_i u_i \psi_\beta(\xi_i)$
            u_i_alpha=u_i_alpha+bsxfun(@times, u_i*w_i*Psi_i, 1./h_beta');
        end
    case 'regression'
        %% The same but with interpolation
        A = gpcbasis_evaluate(V_u, xi, 'dual', true);
        U=zeros(R, n_out);
        for j=1:R
            xi_j=xi(:,j);
            e_j=germ2param_func(xi_j);
            u_j=solve_func(e_j');
            U(j, :)=u_j;
        end
        u_i_alpha=(A\U)';
end
end