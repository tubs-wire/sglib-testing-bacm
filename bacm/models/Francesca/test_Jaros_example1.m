clf
clear
%% Prior param definition
% Define through simparamset (now it's only one parameter though)
Q=MySimParamSet();
Q.add('q', UniformDistribution(-2, 2));
% gpCE basis definition
V_q=Q.get_germ();

%% Define observation function xi->y with gPCE
% gPCE basis
Q2Y_func = @(q)((q+.5).^3+q/2);
Xi2Y_func = @(xi)Q2Y_func(Q.germ2params(xi));
V_y = gpcbasis_create('H', 'p', 3);
% PCE coefficients
y_i_alpha = gpc_projection(Xi2Y_func, V_y, 4);

%% Plot
q_i = -4:0.1:4;
xi_i = Q.params2germ(q_i);
hold all;
plot(gpc_evaluate(y_i_alpha, V_y, xi_i), q_i);
plot(Q2Y_func(q_i), q_i, 'x');
ylabel('q')
xlabel('y')
plot(Q.pdf(q_i), q_i);
plot(xlim(), [0,0], 'k')
plot([0,0],ylim(), 'k')

%% Define error
% Measurement error
sigma_eps = sqrt(0.04);
% observations
y_m = -10:0.5:10;

%% Update
% Map from germ to observable
Y_func = funcreate(@gpc_evaluate, y_i_alpha, V_y, @funarg);
% Measurement error model
sigma_e = sqrt(0.04);
E = SimParamSet();
E.add(SimParameter('e', UniformDistribution(-2*sigma_e,1)));
%E=generate_stdrn_simparamset(sigma_eps);
E_func = funcreate(@germ2params, E, @funarg);
[e_i_alpha, V_e] = E.gpc_expand();
% Plot observation
%plot(xi_i, repmat(y_m + sigma_eps*[-1;0;1], 1, length(xi_i)))


%% MMSE inputs
%gPCE of Q
[Q_i_alpha, V_q] = Q.gpc_expand();
% For all estimator degrees
for p_phi_i = 1:20
    p_int_mmse=40;
    p_phi_i
    [~, phi_func, ~, ~]=my_mmse_gpc...
        (Q_i_alpha, Y_func, V_q, y_m(1), E_func, V_e, p_phi_i, p_int_mmse,  'int_grid', 'full_tensor');
    plot(y_m, funcall(phi_func, y_m), 'x-')
    xlim([-3,3])
    ylim([-2.5,2.5])
end





