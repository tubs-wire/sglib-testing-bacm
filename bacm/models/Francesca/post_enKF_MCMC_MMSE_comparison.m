%function post_enKF_fra()
clear all

%choose sample number for posterior samples
N=10000;
% Define prior distributions, and approximating basis
p_gpce=3;
Q = SimParamSet();
Q.add(SimParameter('q', NormalDistribution(0,1)));

% Show integration points where the model has  to be computed
display(q_i);
% number of observation points
n_meas=1;

%% Define models
N_mod=4;

acc_rate_all=zeros(1,N_mod);
Q_sample_post_all=zeros(4,N,N_mod);
Q_post_param=zeros(5,2,N_mod);
measure=zeros(N_mod,2);
for m=1:N_mod
    
    %Model 1
    if m==1
        %         model_map=@(x)(1+0.5*x);
        model_map=@(x)(1+5*x);
        modelname='Model 1 - Linear';
    elseif m==2
        %Model 2
        %         model_map=@(x)(1+0.5*x.^2);
        model_map=@(x)(1+2*x.^2);
        modelname='Model 2a - Sligthly Non Linear';
    elseif m==3
        %Model 2
        %         model_map=@(x)(1+0.5*x.^2);
        model_map=@(x)(4-12*x+4*x.^2);
        modelname='Model 2b - Sligthly Non Linear';
    elseif m==4
        % %Model 3
        model_map=@(x)(1+0.5*x.^4);
        modelname='Model 3 - Non Linear';
    elseif m==5
        % %Model 3
        model_map=@(x)((x+0.5)^3+q/2);
        modelname='Model 5 - Jaro';
    end
    
    %% Define true response
    
    x_true = mux+sigmax; %artificial truth
    y_true = funcall(model_map, x_true); %response of the system for the artificial truth
    
    
    %% Define the measurement
    sigma_true_err=y_true/100;
    % make a seperate simparamset for the measurement errors
    E_true=generate_stdrn_simparamset(ones(n_meas,1)*sigma_true_err);
    y_m = y_true + E_true.sample(1);% measurement of the response
    y_m = funcall(response_map, y_m);
    
    
    
    %% Generate the error model
    % Let's assume a the same error as the real measurement error
    sigma_eps = 1.*sigma_true_err;
    % make a seperate simparamset for the measurement errors
    maped_sigmas=funcall(J_func, y_m)*sigma_eps;
    E=generate_stdrn_simparamset(maped_sigmas);
    
    %% Generate proxi model
    % Response from integration points
    u_z=funcall(model_map, q_i);
    y=funcall(response_map, u_z);
    % u_z2=funcall(model2_map, q_i);
    % u_z3=funcall(model3_map, q_i);
    % % map displacement field
    % y1=funcall(response_map, u_z);
    % y2=funcall(response_map, u_z2);
    % y3=funcall(response_map, u_z3);
    % a=Q.params2germ(2);
    
    % calculate gPCE of the displacement
    y_i_alpha = calculate_gpc_coeffs(V_y, xi_i, y, w_i, 'method', 'colloc');
    % y_i_alpha2 = calculate_gpc_coeffs(V_y, xi_i, y2, w_i, 'method', 'colloc');
    % y_i_alpha3 = calculate_gpc_coeffs(V_y, xi_i, y3, w_i, 'method', 'colloc');
    % check the surrogate model
    y_true_gpc=funcall(inv_response_map, gpc_evaluate(y_i_alpha, V_y, Q.params2germ(x_true)));
    % y_true_gpc2=funcall(inv_response_map, gpc_evaluate(y_i_alpha2, V_y, Q.params2germ(x_true)));
    % y_true_gpc3=funcall(inv_response_map, gpc_evaluate(y_i_alpha3, V_y, Q.params2germ(x_true)));
    
    % Define map from germ to maped observable
    Y_func = @(xi)gpc_evaluate(y_i_alpha, V_y, xi);
    % Y_func2 = @(xi)gpc_evaluate(y_i_alpha2, V_y, xi);
    % Y_func3 = @(xi)gpc_evaluate(y_i_alpha3, V_y, xi);
    
    % check whether surrogate model is ok:
    % display(y_true_gpc-y_true);
    % display(y_true_gpc2-y_true);
    % display(y_true_gpc3-y_true);
    
    
    
    %% Statistics
    [y_mean, y_var] = gpc_moments(y_i_alpha, V_y);
    % plot_u_mean_var(1:n_u, u_mean, u_var, 'line_color', 'black',...
    %     'transparency', 0.4, 'fill_color', 'b')
    %% Optional inputs
    %choose update method:
    % update_method='MCMC';
    % update_method='MMSE';
    % update_method='enKF';
    % update_method='gpcKF';
    % update_method='EnKF';
    
    
    for k=1:4
        
        if k==1
            update_method='MCMC';
        elseif k==2
            update_method='EnKF';
        elseif k==3
            update_method='gpcKF';
        elseif k==4
            update_method='MMSE';
        end
        
        
        switch update_method
            %     case 'enKF'
            %         %% By updating the coefficients (without samples) - Bojana's update
            %         % Update germ
            %         [xi_a_i_alpha, V_xi_a] = gpc_update_enKF(Q, E, y_i_alpha, V_y, y_m);
            %         % Sample from germ
            %         germ_samples_post=gpc_sample(xi_a_i_alpha, V_xi_a, N);
            %         % map to parameter
            %         Q_samples_post=Q.germ2params(germ_samples_post);
            %         %Posterior mean
            %         Q_post_mean=mean(Q_samples_post');
            %         %Posterior variance
            %         Q_post_var=var(Q_samples_post');
            
            case 'MMSE'
                %% with Elmar's cool MMSE
                E_func = funcreate(@germ2params, E, @funarg);
                % PCE of the error
                [e_i_alpha, V_e] = E.gpc_expand();
                % gPCE of Theta
                V_theta = gpcbasis_create(V_y{1}, 'p', 1);
                theta_i_alpha=zeros(gpcbasis_size(V_theta))';
                theta_i_alpha(:,2:end)=identity(gpcbasis_size(V_theta,2));
                
                % set MMSE integration orders
                p_phi=4; %to change
                
                p_theta = gpcbasis_info(V_theta, 'total_degree');
                p_gpc= gpcbasis_info(V_y, 'total_degree');
                p_int_mmse=max(p_gpc*p_phi+1,ceil((p_theta+p_phi*p_gpc+1)/2));
                
                % Update
                [thetam_func, ~, theta_m, Y_m_func] = my_mmse_gpc(theta_i_alpha, ...
                    Y_func, V_theta, y_m, E_func, V_e, p_phi, p_int_mmse,...
                    'int_grid', 'full_tensor')
                
                % posterior mean
                Q_post_mean = Q.germ2params(theta_m);
                
                % samples of the posterior
                e_m_i=E.sample(N);
                theta_prior_i =gpc_sample(theta_i_alpha, V_theta, N);
                theta_post_samples=theta_prior_i+...
                    binfun(@plus,-funcall(thetam_func, [theta_prior_i; e_m_i]), theta_m);
                Q_samples_post=Q.germ2params(theta_post_samples);
                Q_post_var  = var(Q_samples_post);
                
                
            case 'MCMC'
                % Get distribution of the germ
                syschars=V_y{1};
                prior_dist=cell(length(syschars),1);
                for i=1:length(syschars)
                    [~, prior_dist{i}]=gpc_registry('get', syschars(i));
                end
                % run the update
                if m==1
                    ratio=10;
                elseif m==2
                    ratio=50;
                elseif m==3
                    ratio=10;
                end
                [germ_samples_post, acc_rate]=...
                    gpc_MCMC(N, Y_func, prior_dist, y_m, E,ratio);
                display(acc_rate)
                Q_samples_post = Q.germ2params(germ_samples_post);
                %Posterior mean
                Q_post_mean=mean(Q_samples_post');
                %Posterior variance
                Q_post_var=var(Q_samples_post');
                
            case 'gpcKF'
                % gPCE of the meas error model
                [E_j_beta, V_E]=E.gpc_expand();
                %                 if project_flag
                %                     E_j_beta=L*E_j_beta;
                %                 end
                % Define identity map: germ to Theta
                Theta_i_alpha=zeros(gpcbasis_size(V_y))';
                Theta_i_alpha(2)=1;
                %gPCE of the updated Theta
                [Thetap_i_gamma, V_Thetap] = gpc_KF(Theta_i_alpha, y_i_alpha, V_y, E_j_beta, V_E, y_m);
                %sample from the updated THETA
                theta_samples_post=gpc_sample(Thetap_i_gamma, V_Thetap, N);
                % map to f_cm
                Q_samples_post=Q.germ2params(theta_samples_post);
                %Posterior mean
                Q_post_mean=mean(Q_samples_post,2);
                %Posterior variance
                Q_post_var=var(Q_samples_post, [],2);
                
            case 'EnKF'
                xi_samples=gpcgerm_sample(V_y, N);
                %                 if project_flag
                %                     e_samples=L*E.sample(N);
                %                 else
                e_samples=E.sample(N);
                %                 end
                theta_samples_post=enKF(xi_samples, Y_func, e_samples, y_m);
                
                % map to f_cm
                Q_samples_post=Q.germ2params(theta_samples_post);
                %Posterior mean
                Q_post_mean=mean(Q_samples_post,2);
                %Posterior variance
                Q_post_var=var(Q_samples_post, [],2);
                
                %case 'EnKF1'
                %f_cm_post_samples=update_q_enKF(Q, E, y_i_alpha, V_y, y_m, N);
                %f_cm_post_mean = mean(f_cm_post_samples);
                %f_cm_post_var  = var(f_cm_post_samples);
                
        end
        display(strvarexpand...
            ('Posterior mean=$Q_post_mean$, Posterior variance=$Q_post_var$'));
        
        Q_sample_post_all(k,:,m)=Q_samples_post;
        Q_post_param(k,:,m)=[Q_post_mean,Q_post_var];
        
    end
    
    acc_rate_all(1,m)=acc_rate;
    
    
    Q_samples_prior=Q.sample(N);
    %Analytical solution
    if m==1
        x_m=(y_m-1)/5;
        %     elseif m==2
        %         x_m=((y_m-1)/0.5)^0.5;
        %     elseif m==3
        %         x_m=((y_m-1)/0.5)^0.25;
        %     end
        muupdate=1/(sigmax^-2+sigma_true_err^-2)*(sigmax^-2*mux+sigma_true_err^-2*(x_m));
        sigmaxupdate=(1/(sigmax^-2+sigma_true_err^-2))^0.5;
        Q_post_param(5,:,m)=[muupdate,sigmaxupdate^2];
        measure(m,:)=[x_m,y_m];
    else
        measure(m,:)=[0,y_m];
    end
    
    U=SimParamSet();
    U.add('Xupdate', NormalDistribution(muupdate,sigmaxupdate));
    U_samples_post=U.sample(N);
    
    figure
    % subplot(3,1,i)
    if m==1
        plot_grouped_scatter({Q_samples_prior, Q_sample_post_all(1,:,m), Q_sample_post_all(2,:,m),...
            Q_sample_post_all(3,:,m),Q_sample_post_all(4,:,m),U_samples_post}, 'Color', 'bcygrk', ...
            'Legends', {'Prior', 'Posterior - MCMC', 'Posterior - EnKF', 'Posterior - gpcKF',...
            'Posterior - MMSE','Posterior - Analytical'}, 'Labels', Q.param_names,...
            'FontSize', 10);
    else
        plot_grouped_scatter({Q_samples_prior, Q_sample_post_all(1,:), Q_sample_post_all(2,:),...
            Q_sample_post_all(3,:),Q_sample_post_all(4,:)}, 'Color', 'bcygrk', ...
            'Legends', {'Prior', 'Posterior - MCMC', 'Posterior - EnKF', 'Posterior - gpcKF',...
            'Posterior - MMSE'}, 'Labels', Q.param_names,...
            'FontSize', 10);
    end
    hold on
    plot([x_true x_true],[0 30],'--r')
    yyaxis right
    x0=sort(Q_samples_prior);
    y0=funcall(model_map, x0);
    plot(x0,y0,'-b')
    hold on
    plot([-1 4],[y_m y_m],'--k');
    title(strvarexpand('$modelname$'),'FontSize',12)
    pbaspect([1 1 1])
    
end

% export_fig(figure(1),strvarexpand('Model1_mu3sigma.tiff'),'-transparent')
% export_fig(figure(2),strvarexpand('Model2_mu3sigma.tiff'),'-transparent')
% export_fig(figure(3),strvarexpand('Model3_mu3sigma.tiff'),'-transparent')
% save('Updatemu3sigma','Q_post_param','acc_rate_all','measure')
