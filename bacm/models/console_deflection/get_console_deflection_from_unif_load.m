function [u,x]=get_console_deflection_from_unif_load(p,E,b,h, x)
% p: uniform load:
% E: Young moduli
% h: Cross section height
% b: Cross section withdth


%Inertia
I=b.*h.^3/12;

%Shear force: EI\frac{d^3}{dx^3}u=\int_0^x p(\xi)d\xi-\int_0^L p(\xi)d\xi
%TEI=p*x-p*L;

%Kappa: EI\frac{d^3}{dx^3}u=\int_0^x p(\xi)d\xi-\int_0^L p(\xi)d\xi
L=max(x);
u=-(p./(E.*I))'*(  1/24*x.^4  -1/6*L*x.^3 +  1/4*L^2*x.^2  );
u=u';
end