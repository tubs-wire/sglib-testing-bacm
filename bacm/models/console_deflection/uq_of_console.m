%UQ console
RVs=MySimParamSet();
RVs.add('p', NormalDistribution(1,0.01));
RVs.add('E', LogNormalDistribution(2,0.01));
RVs.add('h', UniformDistribution(0.9,1.1));
RVs.add('b', LogNormalDistribution(3,0.01));

n=RVs.num_params;

%% II.1.a Generate gPCE basis for the proxi model

% Define approximating subspace
p_gpc=4; %set gpc degree
gpc_full_tensor=false; % complete or full tensor gpc basis
V_u=RVs.get_germ;
V_u=gpcbasis_modify(V_u, 'm', n, 'p',p_gpc );


%% II. Get gPCE coeffs with regression

%get integration points
[x_germ,~]=gpc_integrate([], V_u, p_gpc+1, 'grid', 'full_tensor');
Q=size(x_germ,2);
%Define mapping from RVs to germs (standardly distributed random variables)
x_param=RVs.germ2params(x_germ);
u_ij=zeros(100, Q);
for i=1:Q
    params_i=RVs.params2struct(x_param(:,i));
    if i==1
        [u_ij(:,i),x]=get_console_deflection_from_unif_load(params_i,1);
    else
    u_ij(:,i)=get_console_deflection_from_unif_load(params_i,1);
    end
end

A=gpcbasis_evaluate(V_u, x_germ);
u_i_alpha = u_ij/A;

%% Calculate and plot solution moments
[u_mean, u_var]=gpc_moments(u_i_alpha, V_u);


plot_u_mean_var(x, u_mean, u_var, 'fill_color','c', 'line_color','black','transparency', 0.4, 'ylabels',{'u'}, 'xlabel', 'x')
%% Plot response surface
 if plot_response_flag
    ind_tosee=100;
    germ2param_func=@(xi_i)(RVs.germ2params(xi_i));
    optional_inputs.name_of_response='displacement';
    optional_inputs.name_of_RVs=RVs.param_plot_names;
    optional_inputs.germ2param=germ2param_func;
    %optional_inputs.germ_index=[1,2];
    %optional_inputs.plot_fix_response=meas(maxxidx, maxyidx);
    %optional_inputs.germ_fix_vals=germ_at_best;
    plot_multi_response_surface(u_i_alpha(ind_tosee, :), V_u, optional_inputs)
 end