%notation
% Q: object collecting the parameters that are random in the problem
% y: predicted observable
% y_m: sysnthetic observation
% n_meas: number of synthetic observations along the bar
% x_meas: coordinates of the places where observations obtained (it is generated randomly) 
clear

%% Generate the parameter set for the prior model
Q=MySimParamSet();
Q.add('p', NormalDistribution(1,0.01));
Q.add('E', LogNormalDistribution(2,0.01));
Q.add('h', UniformDistribution(0.9,1.1));
Q.add('b', LogNormalDistribution(3,0.01));

%% Generate the model;
model = ConsoleModel(Q);
x=model.x; %coordinates of the cantilever
%% Generate some artificial truth we want to identify which is (for the
% moment) close to the mean of the prior model
n_meas=20; 
x_meas=(x(randi([1,length(x)-1], n_meas,1)))'+max(diff(x))*rand(n_meas,1); %randomly generated coordinates of observations
x_meas=sort(x_meas);
%measurement operator(q->y)
g_func = @(q)(model.compute_measurements(model.compute_response(q),x_meas));
q_true = mean(Q.sample(6),2);
%q_true_struct=Q.params2struct(q_true);
y_true = g_func(q_true);
y_m = y_true + normal_sample(n_meas, 0, 0.01/50);

plot(x,model.compute_response(q_true))
hold on
plot(x_meas,y_m, 'x')
% Generate measurement function (measurement operator xi->y)
Y_func = @(xi)(compute_measurement(xi, g_func, Q));

%% Generate surrogate model and replace original model with it
surr_model = generate_surrogate_model(model, Q, 5, 'myprojection', {5, 'grid', 'full_tensor'});
surr_model.coeffs

u_i_alpha = surr_model.coeffs;
V_u = surr_model.basis;
%plot_multi_response_surface(u_i_alpha(50,:), V_u);

% model = surr_model

%% Generate the error model
sigma_eps = 0.2;
E = SimParamSet();
for i=1:length(y_m)
    param_name=strvarexpand('E_y$i$');
    E.add(param_name, NormalDistribution(0,sigma_eps));
end
% Get error function and gpc germ
E_func = @(xi)(E.germ2params(xi));
V_e = E.get_germ();
%V_e=gpcbasis_modify(V_e, 'p',1);


%% Generate original parameter GPC and update with the MMSE
[Q_i_alpha, V_q] = Q.gpc_expand();
p_phi=1;
p_pn=2;
p_int_mmse=3;
p_int_proj=4;
[Qn_i_beta, V_qn] = mmse_update_gpc(Q_i_alpha, Y_func, V_q, y_m, E_func, V_e, p_phi, p_int_mmse, p_pn, p_int_proj, 'int_grid', 'smolyak');


%% Compute some stuff with updated parameters
[q_mean, q_var] = gpc_moments(Q_i_alpha, V_q);
[qn_mean, qn_var] = gpc_moments(Qn_i_beta, V_qn);
gpc_covariance(Q_i_alpha, V_q)
chopabs(gpc_covariance(Qn_i_beta, V_qn, [], 'corrcoeffs', true))
chopabs([q_mean-q_true, sqrt(q_var), qn_mean-q_true, sqrt(qn_var)])
chopabs([g_func(q_mean)-y_m, g_func(qn_mean)-y_m, y_true-y_m ])

