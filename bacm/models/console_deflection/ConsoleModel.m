classdef ConsoleModel < BaseModel
    
    properties
        Q=[];
        x=linspace(0,1,100);
        x_meas=[];
        b = 0.7;
        %h = 1;
    end
    
    methods
        function model = ConsoleModel(Q, varargin)
            options = varargin2options(varargin);
            [x, options] = get_option(options, 'x', model.x);
            check_unsupported_options(options, mfilename);
            
            model.x=x;
            model.Q=Q;
          end
        
        function n=response_dim(model)
            n = length(model.x);
        end
        
        function u=compute_response(model, q)
            %             u0 = q(1,:);
            %             v0 = q(2,:);
            %             m  = q(3,:);
            %             d  = q(4,:);
            %             k  = q(5,:);
            %             T = model.T;
            q_struct=model.Q.params2struct(q);
            q_names=fieldnames(q_struct);
            for i=1:length(q_names)
                eval(strvarexpand('$q_names{i}$=q_struct.(q_names{i});'))
            end
            x=model.x;
            b = model.b;
            %h = model.h;
            u=get_console_deflection_from_unif_load(p,E,b,h,x);
        end
        
        function y = compute_measurements(model, u)
            H = compute_measurement_operator(model);
            y = H*u;
        end
        function model=set_x_meas(model,x_meas)
            model.x_meas=x_meas;
        end
        function n=measurement_dim(model)
            n = length(model.x_meas);
        end
        function H=compute_measurement_operator(model)
            %get index of interpolating points
            
            x_m=model.x_meas;
            x=model.x;
            
            n_m=length(x_m);
            n=length(x);
            
            dif_x=repmat(x_m, 1,n)-repmat(x,n_m,1);
            [~, ind1]=min(abs(dif_x), [],2);
            ind2=ind1+1*sign( x_m-x(ind1)'  );
            ind_points=sort([ind1,ind2],2);
            delta_x=(x_m' - x(ind_points(:,1)) )./(  x(ind_points(:,2))-x(ind_points(:,1))  );         
            H = sparse([1:n_m,1:n_m], reshape(ind_points,[],1), [(1-delta_x)';delta_x'], n_m, n);
        end
    end
end
