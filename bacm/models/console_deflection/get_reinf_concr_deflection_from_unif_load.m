function [u,x]=get_console_deflection_from_unif_load(a, delta_zeta)


% Geometrical data
L=5000;

b=200; %[mm] 
h=400; %[mm] 
n_l=4; %number of reinforcing bars
phi_l=20; %[mm] diameter of reinforcing longitudinal bars
A_s=n_l*phi_l^2*pi/4; %[mm²]
% Calculation of the effective height
%phi_s=10; %[mm] diameter of shear reinforcement
%c_u= 20; %[mm] concrete cover
%a=c_u+phi_s+phi_l/2;
a=40;
d=h-a; % [mm]

%Loads
g_k=16; %[kN/m]=[N/mm] self weight
q_k=10; %[kN/m] load
psi_2=0.6;
p_qp=g_k+psi_2*q_k;
%Material properties

% Concrete
E_cm=30*1000; %[N/mm²] Expected value of the Young Moduli of concrete
f_ctm=2.2;  %[N/mm²] Expected value of tensile strength of concrete (for 28 days old concrete)
phi_t=2; %kuszasi tenyezo vegerteke
E_c_eff=1.05*E_cm/(1+phi_t);
e_cs=0.04/100; %zsugorodas

%Reinforcement
E_s=200/1000; %[N/mm²]

%Ratio
alpha_s_eff=E_s/E_c_eff;

%Calculation of homogenized cross section
%without cracs
A_1=b*h+(alpha_s_eff-1)*A_s;
S_x1=b*h^2/2+A_s*(alpha_s_eff-1)*d;
x_1=S_x1/A_1;
I_1=b*x_1^3/3+b*(h-x_1)^3/3+A_s*(alpha_s_eff-1)*(d-x_1)^2;
S_s1=A_s*(d-x_1);

%Check whether the second state has to be calculated
M_cr=f_ctm*I_1/(h-x_1); %[Nmm]
M_cr=M_cr*10^6;
M_qp=p_qp*L^2/8;

%Calculation of homogenized cross section with complete cracs
x_func=@(x_2)( (b*x_2.^2/2+A_s*alpha_s_eff*d)./...
    (b*x_2+alpha_s_eff*A_s)-x_2);
x_2=fsolve(x_func,0);
I_2=b*x_2^3/3+A_s*(alpha_s_eff)*(d-x_2)^2;
S_s2=A_s*(d-x_2);

% Compilation of the stiffness matrix
[x,els,bnd]=create_mesh_1d(0, L, 20);
n_elem=size(els,2);

E=E_c_eff*ones(n_elem,1);
I=I_1*ones(n_elem,1);

K=get_element_stiffness_matrix(x, els, E, I);

%1st state calculation of the curvature (kappa) without cracs
kappa_1M=M_max/(E_c_eff*I_1); %from the force
kappa_1cs=e_cs*alpha_s_eff*S_s1/I_1; %zsugorodasbol
kappa_1=kappa_1M+kappa_1cs;

%2nd state calculation of the curvature (kappa) with cracs
kappa_2M=M_max/(E_c_eff*I_2); %from the force
kappa_2cs=e_cs*alpha_s_eff*S_s2/I_2; %zsugorodasbol
kappa_2=kappa_2M+kappa_2cs;

% stress in the reinforcement from the load (for weighting the inertias)
sigma_s=M_max*(d-x_2)*alpha_s_eff/I_2; %[N/mm²]
%stress in the reinforcement from critical load
sigma_sr=M_cr*(d-x_2)*alpha_s_eff/I_2; %[N/mm²]

% coefficient taking into account the time of the loading
% beta_p=1 %for short term loading
beta_p=0.5; % for long term loading

% the weighting 
zeta=1-beta_p*delta_zeta*(sigma_sr/sigma_s)^2;

% the curvature
kappa=zeta*kappa_2+(1-zeta)*kappa_1;

%Shear force: EI\frac{d^3}{dx^3}u=\int_0^x p(\xi)d\xi-\int_0^L p(\xi)d\xi
%TEI=p*x-p*L;

%Kappa: 
L=max(x);
u=-p/(E*I)*(  1/24*x.^4  -1/6*L*x.^3 +  1/4*L^2*x.^2  );
u=u';
end
function K=get_element_stiffness_matrix(x, els, E, I)

n=size(x,2)*2;
n_elem=size(els,2);

K_e={@(E,I,l)(12*E*I/l^3),  @(E,I,l)(6*E*I/l^2),  @(E,I,l)(-12*E*I/l^3),  @(E,I,l)(6*E*I/l^2);...
     @(E,I,l)(6*E*I/l^2),     @(E,I,l)(4*E*I/l),  @(E,I,l)(-6*E*I/l^2),     @(E,I,l)(2*E*I/l);...
     @(E,I,l)(-12*E*I/l^3), @(E,I,l)(-6*E*I/l^2), @(E,I,l)(12*E*I/l^3),   @(E,I,l)(-6*E*I/l^2);...
     @(E,I,l)(6*E*I/l^2),     @(E,I,l)(2*E*I/l),    @(E,I,l)(-6*E*I/l^2),     @(E,I,l)(4*E*I/l)};
 
l=diff(x);
%global indices (in the stiffness matrix)
gind=[(els(1,:)*2-1);(els(1,:)*2);(els(2,:)*2-1);(els(2,:)*2)];
gind1=permute(repmat(gind',1,1,4),[3,2,1]);
gind2=repmat(gind,4,1);
% get local indices
[ind1,ind2]=meshgrid(1:4,1:4);
lind=repmat([ind1(:), ind2(:)],n_elem,1);
el_numb=reshape(repmat(1:n_elem,16,1), [],1);
% evaluate element stiffness matrix elements
v=zeros(size(lind,1),1);
 for i=1:size(lind,1)
     v(i)=funcall(K_e{lind(i,1), lind(i,2)},E(el_numb(i)),I(el_numb(i)), l(el_numb(i)));
 end
% compile
K=sparse(gind1(:), gind2(:), v, n, n);
end

