%% Init stuff
clc, clear
Colors=set_my_favorite_colors();

%% set path for results (in a folder in the root, where sglib is saved)

if isunix
    % pathname where coefficients are stored
    file_path = ...
    '/home/noefried/Documents/SFB880/UQ_of_POD_coeffs_Seeman/Coefficients/4d-Var';
    % pathname and folder name for results
    root_path='/home/noefried/Documents/SFB880/JP5/';
    
    
elseif ispc
    % pathname where coefficients are stored
    file_path = ...
    'F:\noemi\Documents\SFB880\SFB880_flow_control_POD_coeffs\Coefficients\Coefficients\4d-Var';
    % pathname and folder name for results
    root_path='f:\noemi\Documents\SFB880\SFB_JP\';
end

name_dir='Results_with_simparams_a0_unc';

saving_path=[root_path, filesep, name_dir];
mkdir(saving_path);
surr_modeling_mode = 'load'; %load or save (if calculation were already done, then it loads the saved files)
QMC_sampling_mode = 'load'; %load or save (if calculation were already done, then it loads the saved files)
plot_flag = false;

%% Define ROM model
type = 'MFM'; % Quadratic, Linear or MFM
N_act=3;
ROM_model = SFB_POD_ROM(type, N_act, 'file_path', file_path);
n_t = length(ROM_model.t);
N_modes = ROM_model.N_modes;
t = ROM_model.t;

%% Choose grid for calculating the gPCE coefficient

grid = 'full_tensor';

%% Initiate input random variables
%list_of_RVs = {'L', 'Q', 'a0'};
list_of_RVs = {'a0'};
Theta = ROM_model.define_input_uncertainty('list_of_RVs', list_of_RVs);

%% Generate surrogate models with different polynomial degree
switch type
    case 'Quadratic'
        p = 1:7; % for Quadratic
    case 'MFM'
        p = 1:9; % for MFM
end
V_Theta=Theta.get_germ;
surr_models = cell(length(p),1);

% initiate surrogate models
for i = 1: length(p)
    surr_models{i}.V = gpcbasis_modify(V_Theta, 'p', p(i), 'full_tensor', false);
    surr_models{i}.p_int = p(i)+1;
end
switch surr_modeling_mode
    case 'save'
        % compute different order gPCEs
        parfor j = 1:length(p)
            surr_models{j} = get_surrogate_from_orth_projection(...
                ROM_model, surr_models{j}.V, Theta, surr_models{j}.p_int, grid);
        end
        % save surrogates
        file_name = fullfile(saving_path, strvarexpand('validate_gPCE_$type$_$grid$_N_act$N_act$.mat'));
        save(file_name, 'surr_models');
    case 'load'
        %file_name = fullfile(saving_path, 'validate_gPCE_smolyak.mat');
        %file_name = fullfile(saving_path, 'validate_gPCE.mat');
        file_name = fullfile(saving_path,strvarexpand('validate_gPCE_$type$_$grid$_N_act$N_act$.mat'));
        S =load(file_name);
        surr_models = S.surr_models;
end

%% Validation 1: compute solution with QMC samples
% compute random qMC samples

switch QMC_sampling_mode
    case 'save'
        N=100;
        theta = Theta.sample(N, 'mode', 'qmc');
        a= zeros(n_t, N_modes, N);
        
        for i = 1:N
            display(strvarexpand('$i$/$N$'))
            a_i = ROM_model.compute_response(theta(:,i));
            a(:,:,i) = reshape(a_i, n_t, N_modes);
        end
        
        % save solutions and samples
        file_name = fullfile(saving_path, strvarexpand('MCMC_samples_$type$_N_act_$N_act$.mat'));
        save(file_name, 'a', 'theta');
    case 'load'
        file_name = fullfile(saving_path, strvarexpand('MCMC_samples_$type$_N_act_$N_act$.mat'));
        S =load(file_name);
        theta = S.theta;
        N = size(theta, 2);
        a = S.a;
        
end
%% Plot of QMC points
if plot_flag
    % plot QMC points
    plot_grouped_scatter(theta(end-3:end,:), 'Color', 'b',...
        'Labels', {'da_{01}', 'da_{02}', 'da_{03}', 'da_{04}'}, ...
        'MarkerSize', 5, 'MarkerType', 'o')
end

%% check extreme integration point values and number of the integration points

% plot the integration points, and extreme values of integration points
% univariate roole, numberof points:
p_int = p+1;
% initiate memory
da0_int = cell(length(p),1); % for storing all integration points for da0
da_ext = zeros(size(p)); % max deviation from deterministic value
da_rel = zeros(size(p)); % max relative deviation from deterministic value
Q = zeros(size(p)); % total number of integration points
% max value of mode amplitudes:
a_max =max(ROM_model.compute_ref_modes);

for i = 1: length(p)
    theta_int =Theta.get_integration_points(p_int(i), 'grid', grid);
    % integration points for da0
    da0_int{i}= theta_int(end-3:end, :);
    % max deviation from deterministic value
    da_ext(i)=max(max(da0_int{i}'));
    % relative deviation from deterministic value
    da_rel(i)=da_ext(i)/a_max*100;
    % total number of integration points
    Q(i) = size(da0_int{i},2);
end

%% plot integration points
if plot_flag
    plot_grouped_scatter(da0_int(1:4), 'Color', 'rbgk', ...
        'Labels', {'da_{01}', 'da_{02}', 'da_{03}', 'da_{04}'}, ...
        'MarkerType', 'ox<.','MarkerSize', [10, 5, 3, 3],...
        'Legends', {'p=1', 'p=2', 'p=3', 'p=4'}, 'LineType', {'-', ':', '--', '-.'})
    plot_grouped_scatter(da0_int(5:7), 'Color', 'kcm', ...
        'Labels', {'da_{01}', 'da_{02}', 'da_{03}', 'da_{04}'}, ...
        'MarkerType', 'ox<','MarkerSize', [10, 5, 3],...
        'Legends', {'p=5', 'p=6', 'p=7'}, 'LineType', {'-', ':', '--'})
end

%% Validate model: check norm of the error of proxi modeling
% time indices where the analysis is carried out
t_ind = 1:n_t;
%t_ind = 1:714;
%t_ind = 1: 883;
%t_ind = 1: 1001;
% map from parameters to the germs
theta2germ_func = @(theta)(Theta.params2germ(theta));
% initiate memory
mean_err = zeros(size(p)); % one error value for the total response
mean_err_i = zeros(N_modes, length(p)); %one scalar value for each p and each amplitude
err_t = zeros(length(t_ind), length(p)); % time dependent error for each p and each amplitude
err_t_i = zeros(length(t_ind), N_modes, length(p)); % time dependent error for each p
t_b_ind  = zeros(size(p));% index of snapshot where the solution blows up

for i=1:length(p)
    V = gpcbasis_modify(V_Theta, 'p', p(i), 'full_tensor', false);
    a_i_alpha = surr_models{i}.a_i_alpha;
    a_i_alpha = reshape(a_i_alpha, n_t, N_modes, []);
    % check timeindex where there is a NaN ind the coeffs
    t_flag = ~(any(any(isnan(a_i_alpha), 3),2));
    t_b_ind(i) = max(find(t_flag));
    % cancell the last two values, where the proxi is really bad as well
    %t_ind (max(find(t_ind))) = false;
    %t_ind (max(find(t_ind))) = false;
    %t_ind (max(find(t_ind))) = false;
    %t_ind (max(find(t_ind))) = false;
    %a_i_alpha = reshape(a_i_alpha(t_ind, :,:), sum(t_ind)*N_modes, []);
    a_i_alpha = reshape(a_i_alpha(t_ind, :,:), length(t_ind)*N_modes, []);
    [mean_err(i), mean_err_i(:,i), err_t(:,i), err_t_i(:,:,i)]=...
        compute_error_of_gpc_approx(a_i_alpha, V, theta, a(t_ind,:,:), theta2germ_func, []);
end

% snapshot of the different degree models where time integration blows up 
t_blow= ROM_model.t(t_b_ind);

% make a latex table with the validation results
tab_q=get_latex_table_with_surr_validation...
    (p, p_int, Q, da_ext, da_rel, t_blow', mean_err*100);

%% Plot errors with time
j =8; % plot the p_i-th order error
s_i =33; %sample number to be ploted
p_j =p(j);
V = gpcbasis_modify(V_Theta, 'p', p_j, 'full_tensor', false);
a_i_alpha = surr_models{j}.a_i_alpha;
a_i_alpha = reshape(a_i_alpha, n_t, N_modes, []);
for i = 1:4
    subplot(4,1,i)
    hold on
    % plot a sample (with s_i index)
    plot(t(t_ind), squeeze(a(t_ind,i,s_i)), '-', 'LineWidth', 1, 'Color', Colors.grey) 
    % plot its surrogate
    plot(t(t_ind), gpc_evaluate(squeeze(a_i_alpha(t_ind,i,:)), V,...
        Theta.params2germ(theta(:,s_i))),':',  'LineWidth',2, 'Color', 'k')
    % plot the error
    plot(t(t_ind), squeeze(err_t_i(t_ind,i,j)),'-' , 'LineWidth', 2, 'Color', Colors.orange)
    xlim([0,t(max(t_ind))])
    ylabel(strvarexpand('err a_$i$'))
    if i== 4
        xlabel('time')
    elseif i==1
        legend('a sample', 'a surrogate of the sample', 'average error of the PCE')
    end
end

%% plot sensitivites
a_i_alpha = reshape(a_i_alpha(t_ind, :,:), length(t_ind)*N_modes, []);
[partial_var, I_s, ratio_by_index]=get_sensitivities_from_a0_unc(a_i_alpha, V, t);
