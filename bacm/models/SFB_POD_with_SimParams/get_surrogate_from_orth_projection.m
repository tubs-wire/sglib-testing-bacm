function surr_model = get_surrogate_from_orth_projection(model, V_Theta, Theta, p_int, grid)

[xi,w] = gpc_integrate([], V_Theta, p_int, 'grid', grid);
% evaluate parameter value at integration points
theta=Theta.germ2params(xi);

M = gpcbasis_size(V_Theta, 1);
Q = length(w);
surr_model = struct();
a_i_alpha = zeros(model.response_dim, M);

for j = 1:Q
    theta_j = theta(:, j);
    display(strvarexpand('$j$/$Q$'))
    a_i_j = model.compute_response(theta_j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_Theta, xi(:, j), 'dual', true);
    a_i_alpha = a_i_alpha + w(j) * a_i_j * psi_j_alpha_dual;
end

surr_model.a_i_alpha = a_i_alpha;
