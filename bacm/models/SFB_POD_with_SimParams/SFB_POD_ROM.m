classdef SFB_POD_ROM < BaseModel
    properties
        T = 90 % end of timewindow
        t = [] 
        a0 = [] % initial amplitudes
        L = [] % linear ROM coefficient
        Q =[] % quadratic ROM coefficient
        C = [] % constant ROM coefficient
        B = [] % linear ROM coefficient with control mode
        a_c = [] % control modes
        
        theta2mats = [] % map from vector to coeff matrices
        a_ref = [] % reference mode
        N_modes = [] % number of modes (usually 4)
        N_act = [] % number of actuation modes
        N_theta =[] % number of independent parameters
        type = ''
        
        
    end
    
    methods
        function model = SFB_POD_ROM(type, N_act, varargin)
            options = varargin2options(varargin);
            [T, options] = get_option(options, 'T', model.T);
            [file_path, options] = get_option(options, 'file_path', 'default');
            check_unsupported_options(options, mfilename);
            
            % Number of actuation modes
            model.N_act = N_act;
            model.N_modes = 4;
            model.type = type;
            % get file path where the coefficients are stored
            if strcmp(file_path,'default')
                file_path = 'F:\noemi\Documents\SFB880\SFB880_flow_control_POD_coeffs\Coefficients\Coefficients\4d-Var';
            end
            
            %% Read coefficients
            [C_i, L_ij, Q_ijk, B_ij]=get_ROM_coeffs(N_act, type, file_path);
            if isempty(Q_ijk)
                Q_ijk = zeros(model.N_modes, model.N_modes, model.N_modes);
            end
            
            %% Read time steps, initial condition and controle modes
            if true
                S=load('nut_AM3'); %read data from file sent from Semaan
                % control amplitudes
                a_c = S.a(3-N_act+1:3,:);
                % cheat, and cancel first two switched on controle amplitudes
                a_c(:,645:646)=0;
                % time
                t = S.time;
                % initial condition
                a0 = S.a(5:end,1);
            
            %% optioanally the steps, initial condition and controle modes
            else
                % could be red from Gilles file
                S = load('Ref_modes_Gilles.mat');
                % control amplitudes
                a_c=[S.Ref.cmode3, S.Ref.cmode2, S.Ref.cmode1]';
                a_c = a_c(3-N_act+1:end,:);
                a_c(:,645:646)=0;
                % time
                t=S.Ref.time;
                % initial condition
                a0 =[-0.42779711595446163;...
                    -4.9748974032828318*10^(-2); ...
                    -1.0883596966771526*10^(-2);...
                    -9.1617981596987027*10^(-3)];
            end
            %%            
            [T, ind] = min([T, max(t)]);
            if ind == 1
                t_ind = t <= T;
                t = t(t_ind);
            end
                                     
            %% Save model properties
            model.L = L_ij;
            model.Q = Q_ijk;
            model.C = C_i;
            model.B = B_ij;
            model.a0 = a0;
            model.a_c =a_c;
            
            model.T = T;
            model.t = t;
            [model.theta2mats, model.N_theta] = get_compile_map(model.a0, model.L, model.Q);
            a_ref = model.compute_ref_modes();
            model.a_ref = a_ref;
        end
        
        function Theta = define_input_uncertainty(model, varargin)
            Theta = define_input_RVs(model.a0, model.L, model.Q, varargin{:} );
        end
        
        function a_ref = compute_ref_modes(model)
            a_ref = model.compute_response(zeros(model.N_theta,1));
        end
        function n=response_dim(model)
            n = length(model.t)*model.N_modes;
        end
        
        function [a0, L, Q]=compile_to_matrices(model, q)
            [a0, L, Q]=model.c2mats_map(q);
        end
        
        function a = compute_response(model, theta)
            [da0, dL, dQ]= model.theta2mats(theta);
            Cv=model.C;
            Bv=model.B;
            a0v=model.a0 + da0;
            Lv = model.L + dL;
            Qv = model.Q + dQ;
            a_cv = model.a_c;
            
            %Solve ODE with fixed timesteps:
            a =integrate_ROM(a0v, a_cv, Cv, Lv, Qv, Bv, model.t);
            a = a(:);
        end
        
        
        function y = compute_measurements(model, a)
            y = a;
        end
        
        function h = plot_response(model, a, varargin)
            options = varargin2options(varargin);
            [t_ind, options] = get_option(options, 't_ind', 1:length(model.t));
            %[plot_options] = get_option(options, 'plot_options', {});
            check_unsupported_options(options, mfilename);
            %Plot reference POD modes
            ylabels={{'a_1'}; {'a_2'}; {'a_3'}; {'a_4'}};
            a = reshape(a, length(model.t(t_ind)), model.N_modes, []);
            h=cell(model.N_modes, 1);
            for i=1:model.N_modes
                subplot(model.N_modes,1,i)
                h{i} = plot(model.t(t_ind), squeeze(a(:,i,:)));
                if i==1||i==2
                    ylim([-0.6,0.6])
                elseif i==3
                    ylim([-1,0.2])
                else
                    ylim([-0.6,1])
                end
                ylabel(ylabels{i})
                xlabel('time')
            end
        end
        
        
    end
end
