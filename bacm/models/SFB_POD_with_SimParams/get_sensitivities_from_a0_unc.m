function [partial_var, I_s, ratio_by_index]=get_sensitivities_from_a0_unc(a_i_alpha, V, t, ind )

Colors=set_my_favorite_colors();

%% get partial vars and sensitivity indices
if nargout<4
    ind = 1:length(t);
end

M = gpcbasis_size(V,1);
if length(size(a_i_alpha)) ~= 3 && size(a_i_alpha,1)~=length(t)
    a_i_alpha = reshape(a_i_alpha, length(t), [], M) ;
end
n_var = size(a_i_alpha, 2);
a_i_alpha = reshape(a_i_alpha(ind, :), [], M);
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(a_i_alpha, V, 'max_index',1);
% Indices of params with high ratio

[~, a_var] = gpc_moments(a_i_alpha, V);

%% plot partial variances and sensitivities (when only a0 unc)
part_var= reshape(partial_var, length(t(ind)), n_var, n_var);
a_var = reshape(a_var, length(t(ind)), n_var);
figure
for i = 1:n_var
    subplot(n_var, 1, i)
    hold on
    plot(t(ind),sqrt(squeeze(part_var(:,i,1))), '-', 'LineWidth', 2, 'Color', Colors.navy_blue)
    plot(t(ind),sqrt(squeeze(part_var(:,i,2))), '-.', 'LineWidth', 2, 'Color', Colors.cadet_blue)
    plot(t(ind),sqrt(squeeze(part_var(:,i,3))), ':', 'LineWidth', 2, 'Color', Colors.dark_grey)
    plot(t(ind),sqrt(squeeze(part_var(:,i,4))), '-', 'LineWidth', 2, 'Color', Colors.orange)
    
    plot(t(ind), sqrt(a_var(:,i)), '-', 'LineWidth', 1, 'Color', 'k')
    
    ylabel(strvarexpand('a_{$i$}'), 'FontSize', 12)
    if i ==1
        legend({'da_{01}', 'da_{02}', 'da_{03}', 'da_{04}', 'total'}, 'FontSize', 11, 'Orientation', 'Horizontal')
    end
    if i ==4
        xlabel('time', 'FontSize', 12)
    end
end
