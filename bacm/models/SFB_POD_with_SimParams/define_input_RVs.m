function Theta = define_input_RVs(a0, L, Q, varargin )

options = varargin2options(varargin);
[list_of_RVs, options] = get_option(options, 'list_of_RVs', {'L', 'Q', 'a0'});
check_unsupported_options(options, mfilename);

% Create Simparamset
Theta =SimParamSet();

%% Input Qijk
% sort unique values to force symmetries
[uniq_Q, indq1, indq2]=unique(Q); %uniq_Q=Q(indq1) and Q=reshape(uniq_Q(indq2),4,4,4)
[ind1, ind2, ind3] = ind2sub(size(Q), indq1);
is_Q_fixed = isempty(  strmatch('Q', list_of_RVs) );
num_Q_params = length(indq1);
for i=1: num_Q_params
    param_name_i = strvarexpand('dQ_$ind1(i)$$ind2(i)$$ind3(i)$');
    Theta.add(MySimParameter(param_name_i, NormalDistribution(0,0.02)));
    if is_Q_fixed
        Theta.set_to_mean(param_name_i);
    end
end
  
%% Input Lij
L=reshape(L, [], 1);
ind_L=1:length(L);
[ind1, ind2] = ind2sub([4,4], ind_L);
is_L_fixed = isempty(  strmatch('L', list_of_RVs) );
num_L_params = length(ind_L);
for i=1: num_L_params
    param_name_i = strvarexpand('dL_$ind1(i)$$ind2(i)$');
    Theta.add(MySimParameter(param_name_i, NormalDistribution(0,0.02)));
    if is_L_fixed
        Theta.set_to_mean(param_name_i);
    end
end

%% Input ai
is_a_fixed = isempty(  strmatch('a0', list_of_RVs) );
num_a_params = length(a0);
ind_a = 1: num_a_params;
for i=1: num_a_params
    param_name_i = strvarexpand('da0_$ind_a(i)$');
    Theta.add(MySimParameter(param_name_i, NormalDistribution(0,0.02)));
    if is_a_fixed
        Theta.set_to_mean(param_name_i);
    end
end



