function tab_q=get_latex_table_with_surr_validation(p, p_int, Q, da_ext, da_rel, t_blow, mean_err, varargin)
options=varargin2options(varargin);
[caption, options]=get_option(options, 'caption', '');
[label, options]=get_option(options, 'label', '');
[qdigits, options]=get_option(options, 'qdigits', 4);
[errdigits, options]=get_option(options, 'errdigits', 2);
[label, options]=get_option(options, 'label', '');
check_unsupported_options(options,mfilename);


    n_col=length(p);
    ltable_data=[p; Q; da_ext; da_rel; t_blow; mean_err];
    input.data =ltable_data;
    
    % Set column labels (use empty string for no label):
    input.tableRowLabels = {'polynomial degree'; 'number of int. points';...
        'max dev. of int. point'; 'max rel dev. of int. point'; 'time of loosing stability'; 'rel. err.[\%]'};
    % Set row labels (use empty string for no label):
    %input.tableColLabels = '';
    input.dataFormat = {strvarexpand('%.$qdigits$f'),n_col};
    input.tableColumnAlignment = 'l';
    input.tableBorders = 0;
    input.booktabs = 0;
    % LaTex table caption:
    input.tableCaption = caption;
    input.tableLabel = label;
    % Switch to generate a complete LaTex document or just a table:
    input.makeCompleteLatexDocument = 1;
    % call latexTable:
    tab_q = latexTable(input);
end