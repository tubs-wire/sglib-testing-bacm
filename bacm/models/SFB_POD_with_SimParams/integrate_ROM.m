function POD_modes = integrate_ROM(a0, a_c, C, L, Q, B, t_span, varargin)
% POD:
% dadt(i)=Ci(i)+ sum_j Lij(i,j)*a(j) + sum_jsum_k Qijk(ijk)a(j)a(k) + sum_j
% Bij(i,j)*c(j)
% with
%
% -a(t): modes of the POD model: u(t,x)=u_0+\sum a(t)\psi(x)+\sum c(t)\psi(x)
% -c(j): control modes
%       .- c(-1): shift between controlled and not controlled phase B0
%        - c(-2): oscillatory part of the boundary at the jet
%         (B1*cos(\omega_at)
%        - c(-3): + oscillatory part with phase shift (B1*sin(\omega_a t)
%           (for the control modes the reference modes are used, which can
%           be changed by setting the 'USE_REF_MODES' option 'FALSE'
% POD coefficients
%
% coeffs of the linear term:    Lij : dissipative term
% coeffs of the quadratic term: Qijk :convective term
% coeffs of the actuation:      Bij  :control term
% initial condition:            a_0  :first coeffs of the POD basis
%


%% Solve ODE
a_ct =@(t)interpolate_conrole_ampl(t, t_span, a_c);
POD_modes = ode4(@(t, POD_modes)solve_SFB_POD(t, POD_modes, C, L, Q, B, a_ct), t_span, a0);

end
%% Function for the explicit solver
function POD_modes = solve_SFB_POD(t, a, C, L, Q, B, a_ct)
%POD_modes= var.C + var.L*a + reshape(reshape(var.Q,n*n,n)*a,n,n)*a + var.B*[0.0295;0.0239*cos(0.489*t);0.0239*sin(0.489*t)];
POD_modes= C + L*a + [Q(:,:,1)*a, Q(:,:,2)*a, Q(:,:,3)*a, Q(:,:,4)*a]*a + B*a_ct(t);
end
%% Get control modes with linear interpolation of reference control modes
function a_ct = interpolate_conrole_ampl(t, t_span, a_c)
a_ct = (interp1(t_span, a_c', t))';
end




