%% Init stuff
clc, clear
Colors=set_my_favorite_colors();

%% set path for results (in a folder in the root, where sglib is saved)

% pathname where coefficients are stored
file_path = ...
    'F:\noemi\Documents\SFB880\SFB880_flow_control_POD_coeffs\Coefficients\Coefficients\4d-Var';
% pathname and folder name for results
root_path='f:\noemi\Documents\SFB880\SFB_JP\';
name_dir='Results_with_simparams_a0_unc';

saving_path=[root_path, filesep, name_dir];

type = 'MFM';
N_act = 3;
p_gpc = 2;
%file_name = fullfile(saving_path,strvarexpand('SurrModel_p$p_gpc$$type$_N_act$N_act$.mat'));
file_name = fullfile(saving_path,strvarexpand('SurrModel_p$p_gpc$$type$_N_act$N_act$full_tensor.mat'));
S = load(file_name);
%%
ROM_model = SFB_POD_ROM(type, N_act, 'file_path', file_path);
n_t = length(ROM_model.t);
N_modes = ROM_model.N_modes;
t = ROM_model.t;

%%
t_ind = 1: length(ROM_model.t);
V_Theta = S.surrogate_model.V_Theta;
a_i_alpha = S.surrogate_model.a_i_alpha;
[a_mean, a_var] = gpc_moments(a_i_alpha, V_Theta);
A_mean =reshape(a_mean, [], 4);
A_var = reshape(a_var, [], 4);
t_ind = 1: length(ROM_model.t);
% plot_u_mean_var(ROM_model.t(t_ind), A_mean(t_ind,:), A_var(t_ind, :))
% plot_u_mean_var(ROM_model.t, A_mean, A_var)
u_quant=(gpc_quantiles(V_Theta, a_i_alpha, 'quantiles', [0.01, 0.99]))';
% some corrections for ploting
if false
u_quant(isnan(u_quant(:, 1)), 1)= -10;
u_quant(isnan(u_quant(:, 2)), 2)= 10;
delta = 10^(-3);
a_quant_l = reshape(u_quant(:,1), [], 4)-delta;
a_quant_u = reshape(u_quant(:,2), [], 4)+delta;
else
a_quant_l = reshape(u_quant(:,1), [], 4);
a_quant_u = reshape(u_quant(:,2), [], 4);    
end

plot_u_mean_quant(ROM_model.t(t_ind), A_mean(t_ind,:), ...
    a_quant_l(t_ind,:), a_quant_u(t_ind, :),...
    'ylabels', {'a_1', 'a_2', 'a_3', 'a_4'},...
    'fill_color', Colors.lavender, 'line_color', 'k', ...
    'line_width', 1.5, 'transparency', 0.6);
a_ref = reshape(ROM_model.a_ref, [], 4);

% Compute some realizations with the surrogate:
S = load([root_path, filesep,'Results_with_simparams_a0_unc',filesep,...
    'MCMC_samples_quadratic_N_act_3.mat']);
a = S.a;
for i = 1:4
    subplot(4,1,i)
    %plot(ROM_model.t(t_ind), a_ref(t_ind,i),'-' , 'LineWidth', 1.0, 'Color', Colors.orange)
    plot(ROM_model.t(t_ind), squeeze(a(t_ind,i, 97)),'-' , 'LineWidth', 0.5) %'Color', Colors.grey)
    xlim([0,ROM_model.T])
    if i==1||i==2
        ylim([-1,1])
        if i==1
            legend('99% confidence region', 'mean', 'without perturb')
        end
    elseif i==3
        ylim([-0.6,0.2])
    else
        ylim([-0.6,1])
    end
end



% Check sensitivities (how the variance of the solution is influenced by
% the different scaling of the eigenfunctions
t_ind = 1:1001;
aa_i_alpha = reshape(a_i_alpha, length(ROM_model.t), 4,[]) ;
aa_i_alpha = reshape(aa_i_alpha(t_ind, :), [], size(a_i_alpha,3));
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(aa_i_alpha, V_Theta, 'max_index',1);
% Indices of params with high ratio
ind = max(ratio_by_index)>0.1;
Theta.param_names{ind};
n_germ=gpcbasis_size(V_lf, 2);
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(aa_i_alpha, V_Theta, 'max_index',2);
n_u=720;


%% SVD

% number of eigenmodes
n_eig=10;
% SVD or KLE (the later is weighted with the grammian)
low_rank='SVD';
%% reduce basis

E_m=generate_stdrn_simparamset(0.02*ones(size(aa_i_alpha,1),1));
V_y = V_Theta;
y_i_alpha = aa_i_alpha;
[r_i_k, sigma_k, y_i_alpha_fluct]=low_rank_from_gPCE(y_i_alpha, V_y, 10);
plot(sigma_k);
n_eig=5;
r_i_k = r_i_k(:, 1:n_eig);
ROM_model.plot_response(r_i_k, 't_ind', t_ind);
A=inv(diag(sigma_k(1:n_eig)))*r_i_k(:,1:n_eig)';
%A=r_i_k(:,1:n_modes)';
mean_y=gpc_moments(y_i_alpha, V_y);

Ys_func = @(xi)gpc_evaluate_by_block(A*y_i_alpha_fluct, V_y, xi);

%% Error model
% Covariance matrix of transformed error
C_eps=A*diag(E_m.var)*A';

% standard deviations of each variable
S = diag(sqrt(diag(C_eps)));
S_inv = inv(S);
% Correlation matrix
Cor_eps = S_inv * C_eps * S_inv;
L = chol(Cor_eps, 'lower');

% Get transformation from standard normal distributed E to eps
E=generate_stdrn_simparamset(ones(n_eig));

% map from error germ to error
%E_svd_func = @(xi)(S*L*xi);

% PCE of the error
[e_i_alpha, V_e] = E.gpc_expand();
E_func=mygpc_function(S*L*e_i_alpha, V_e);

% allways update the germs distribution
XI=gpcgerm2simparamset(V_y);
[xi_i_alpha, V_xi]=XI.gpc_expand();