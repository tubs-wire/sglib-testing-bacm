%% Init stuff
clc, clear
Colors=set_my_favorite_colors();

%% set path for results (in a folder in the root, where sglib is saved)

% pathname where coefficients are stored
file_path = ...
    'F:\noemi\Documents\SFB880\SFB880_flow_control_POD_coeffs\Coefficients\Coefficients\4d-Var';
% pathname and folder name for results
root_path='f:\noemi\Documents\SFB880\SFB_JP\';
name_dir='Results_with_simparams_a0_unc';

saving_path=[root_path, filesep, name_dir];
mkdir(saving_path);

%% Define different types of ROM models with different number of actuation modes
type = {'Quadratic', 'MFM', 'Linear'}; % Quadratic, Linear or MFM
N_act=[3, 2, 1];
ROM_models = cell(length(type), length(N_act));
for i = 1:size(ROM_models,1)
    type_i = type{i};
    
    for k = 1:size(ROM_models,2)
        N_act_k =N_act(k);
        % Create ROM model
        ROM_models{i,k} = SFB_POD_ROM(type_i, N_act_k, 'file_path', file_path);
    end
end

%% Initiate input random variables
%list_of_RVs = {'L', 'Q', 'a0'};
list_of_RVs = {'a0'};
Theta = ROM_models{1}.define_input_uncertainty('list_of_RVs', list_of_RVs);

%% Generate surrogate model
% initiate surrogate models
surr_models = cell(size(ROM_models));
p_gpc = 2;
V_Theta=Theta.get_germ;
V_Theta=gpcbasis_modify(V_Theta, 'p',p_gpc, 'full_tensor', false);
p_int =3;

parfor i= 1:length(ROM_models(:))
    surr_models{i} = get_surrogate_from_orth_projection(ROM_models{i}, V_Theta, Theta, p_int, 'full_tensor');
    surr_models{i}.V_Theta = V_Theta;
    surr_models{i}.p_gpc = p_gpc;
    surr_models{i}.p_int = p_int;
end
for i = 1:size(ROM_models,1)
    type_i = type{i};
    
    for k = 1:size(ROM_models,2)
        N_act_k =N_act(k);
        % Save surrogate model
        file_name = fullfile(saving_path,strvarexpand('SurrModel_p$p_gpc$$type_i$_N_act$N_act_k$full_tensor.mat'));
        surrogate_model = surr_models{i,k};
        save(file_name, 'surrogate_model')
    end
end

