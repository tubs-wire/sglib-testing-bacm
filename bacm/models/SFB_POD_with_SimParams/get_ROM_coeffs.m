function [C_i, L_ij, Q_ijk, B_ij]=get_ROM_coeffs(N_act, type, filepath)

% get the folder corresponding to the right ROM model type
switch type
    case 'Quadratic'
        numid = '02';
    case 'Linear'
        numid = '01';
    case 'MFM'
        numid = '03';
end
type_folder = [numid, '_', type];
% get the folder corresponding to the one with the right number
% of actuation modes
N_act_folder =[num2str(N_act), '_act'];
file_name = fullfile(filepath, type_folder,  N_act_folder, 'coeff_4D.dat');
fileID = fopen(file_name);
i=0;
while (~feof(fileID)) %Untill end of file
    temp1 = fgetl(fileID);
    
    if isempty(temp1) || strcmp(temp1,' ')
        i=0;
    else  i=i+1;
        if i==1
            var_name=temp1;
            eval(strcat(var_name,'=[]'));
        else
            inputtext=textscan(temp1, '%f');
            temp_out=[inputtext{:}];
            eval(strcat(var_name,'(end+1)=temp_out;'));
        end
    end
end

C_i=Ci';
L_ij=reshape(Lij,4,4)';
if exist('Qijk','var')
    Q_ijk=reshape(Qijk,4,4,4);
    Q_ijk=permute(Q_ijk,[3,2,1]);
else
    Q_ijk=[];
end
B_ij=reshape(Bji,[],N_act);
end

