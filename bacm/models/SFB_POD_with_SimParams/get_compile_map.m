function [compile_func, n_theta] = get_compile_map(a0, L, Q)

%% Input Qijk
% sort unique values to force symmetries
[~, indq1, indq2]=unique(Q); %uniq_Q=Q(indq1) and Q=reshape(uniq_Q(indq2),4,4,4)
num_Q_params = length(indq1);
Q_ind = 1 : num_Q_params;
compile_to_Q_matrix = @(q_unique)reshape(q_unique(indq2), size(Q));
theta2Q_map = @(theta)theta2var(theta, Q_ind, compile_to_Q_matrix);
  
%% Input Lij

ind_L=1:length(L(:));
num_L_params = length(ind_L);
L_ind = (1: num_L_params) + num_Q_params;
compile_to_L_matrix = @(l)reshape(l, size(L));
theta2L_map = @(theta)theta2var(theta, L_ind, compile_to_L_matrix);

%% Input ai
num_a_params = length(a0);
a_ind = (1: num_a_params) + (num_Q_params + num_L_params);
theta2a_map = @(c)theta2var(c, a_ind, @(a)a);

% Final map
compile_func = @(c)get_matrices(c, theta2Q_map, theta2L_map, theta2a_map);
n_theta = num_a_params + num_Q_params + num_L_params;
end

function V=theta2var(c, ind, compile_to_matrix_func)
v = c(ind);
V = compile_to_matrix_func(v);
end

function [a0, L, Q] = get_matrices(c, c2q_map, c2l_map, c2a_map)
Q = c2q_map(c);
L = c2l_map(c);
a0 = c2a_map(c);
end


