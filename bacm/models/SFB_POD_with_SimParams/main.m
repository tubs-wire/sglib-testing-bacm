%% Init stuff
clc, clear
Colors=set_my_favorite_colors();

%% set path for results (in a folder in the root, where sglib is saved)

% pathname where coefficients are stored
%file_path = ...
%    'F:\noemi\Documents\SFB880\SFB880_flow_control_POD_coeffs\Coefficients\Coefficients\4d-Var';
file_path = ...
   '/home/noefried/Documents/SFB880/UQ_of_POD_coeffs_Seeman/Coefficients/4d-Var';
% pathname and folder name for results
%root_path='f:\noemi\Documents\SFB880\SFB_JP\';
root_path='home/noefried/tmp/';

name_dir='Results_with_simparams_a0_unc';

saving_path=[root_path, filesep, name_dir];
mkdir(saving_path);

%% Create ROM model
type = 'Quadratic'; %'Quadratic, 'Linear'or 'MFM'
N_act = 3; %3,2 or 1
ROM_model = SFB_POD_ROM(type, N_act, 'file_path', file_path);
% plot reference amplitudes
ROM_model.plot_response(ROM_model.a_ref)

%% Initiate input random variables
%list_of_RVs = {'L', 'Q', 'a0'};
list_of_RVs = {'a0'};
Theta = ROM_model.define_input_uncertainty('list_of_RVs', list_of_RVs);

%% Generate surrogate model
p_gpc = 4;
V_Theta=Theta.get_germ;
V_Theta=gpcbasis_modify(V_Theta, 'p',p_gpc, 'full_tensor', false);
p_int =5;
[xi,w] = gpc_integrate([], V_Theta, p_int, 'grid', 'smolyak');

% evaluate parameter value at integration points
theta=Theta.germ2params(xi);

M = gpcbasis_size(V_Theta, 1);
Q = length(w);
a_i_alpha = zeros(ROM_model.response_dim, M);

for j = 1:Q
    theta_j = theta(:, j);
    display(strvarexpand('$j$/$Q$'))
    a_i_j = ROM_model.compute_response(theta_j);
    xi_j = xi(:, j);
    psi_j_alpha_dual = gpcbasis_evaluate(V_Theta, xi_j, 'dual', true);
    a_i_alpha = a_i_alpha + w(j) * a_i_j * psi_j_alpha_dual;
end

surrogate_model=struct();
surrogate_model.a_i_alpha=a_i_alpha;
surrogate_model.V_Theta = V_Theta;
save('surrogate_all_unc.mat', 'surrogate_model') 
save('surrogate_init_cond_unc.mat', 'surrogate_model')
[a_mean, a_var] = gpc_moments(a_i_alpha, V_Theta);
A_mean =reshape(a_mean, [], 4);
A_var = reshape(a_var, [], 4);
t_ind = 1: 720;
plot_u_mean_var(ROM_model.t(t_ind), A_mean(t_ind,:), A_var(t_ind, :))
u_quant=(gpc_quantiles(V_Theta, a_i_alpha, 'quantiles', [0.05, 0.95]))';
a_quant_l = reshape(u_quant(:,1), [], 4);
a_quant_u = reshape(u_quant(:,2), [], 4);
plot_u_mean_quant(ROM_model.t(t_ind), A_mean(t_ind,:), ...
    a_quant_l(t_ind,:), a_quant_u(t_ind, :),...
    'ylabels', {'a_1', 'a_2', 'a_3', 'a_4'}, 'fill_color', Colors.lavender, 'LineWidth', 2);
a_ref = reshape(ROM_model.a_ref, [], 4);
for i = 1:4
    subplot(4,1,i)
    plot(ROM_model.t(t_ind), a_ref(t_ind,i), 'LineWidth', 1.5, 'Color', Colors.orange)
    if i==1||i==2
        ylim([-0.6,0.6])
        if i==1
            legend('mean', '95% confidence region', 'reference')
        end
    elseif i==3
        ylim([-1,0.2])
    else
        ylim([-0.6,1])
    end
end

% Check sensitivities (how the variance of the solution is influenced by
% the different scaling of the eigenfunctions
aa_i_alpha = reshape(a_i_alpha, length(ROM_model.t), 4,[]) ;
aa_i_alpha = reshape(aa_i_alpha(t_ind, :), [], size(a_i_alpha,2));
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(aa_i_alpha, V_Theta, 'max_index',1);
% Indices of params with high ratio
ind = max(ratio_by_index)>0.1;

Theta.param_names{ind};
n_germ=gpcbasis_size(V_Theta, 2);
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(aa_i_alpha, V_Theta, 'max_index',2);
n_u=720;

%% SVD

% number of eigenmodes
n_eig=10;
% SVD or KLE (the later is weighted with the grammian)
low_rank='SVD';
%% reduce basis

E_m=generate_stdrn_simparamset(0.02*ones(size(aa_i_alpha,1),1));
V_y = V_Theta;
y_i_alpha = aa_i_alpha;
[r_i_k, sigma_k, y_i_alpha_fluct]=low_rank_from_gPCE(y_i_alpha, V_y, 10);
plot(sigma_k);
n_eig=5;
r_i_k = r_i_k(:, 1:n_eig);
ROM_model.plot_response(r_i_k, 't_ind', t_ind);
A=inv(diag(sigma_k(1:n_eig)))*r_i_k(:,1:n_eig)';
%A=r_i_k(:,1:n_modes)';
mean_y=gpc_moments(y_i_alpha, V_y);

Ys_func = @(xi)gpc_evaluate_by_block(A*y_i_alpha_fluct, V_y, xi);

%% Error model
% Covariance matrix of transformed error
C_eps=A*diag(E_m.var)*A';

% standard deviations of each variable
S = diag(sqrt(diag(C_eps)));
S_inv = inv(S);
% Correlation matrix
Cor_eps = S_inv * C_eps * S_inv;
L = chol(Cor_eps, 'lower');

% Get transformation from standard normal distributed E to eps
E=generate_stdrn_simparamset(ones(n_eig));

% map from error germ to error
%E_svd_func = @(xi)(S*L*xi);

% PCE of the error
[e_i_alpha, V_e] = E.gpc_expand();
E_func=mygpc_function(S*L*e_i_alpha, V_e);

% allways update the germs distribution
XI=gpcgerm2simparamset(V_y);
[xi_i_alpha, V_xi]=XI.gpc_expand();
