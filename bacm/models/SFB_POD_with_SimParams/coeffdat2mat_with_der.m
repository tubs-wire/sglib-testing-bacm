function coeffs=dat2mat(file_name, n_actuation_mode, filepath)

%fileID = fopen('coeff_4D.dat');
fileID = fopen('coeff_4D_with_derivatives.dat');
n_act=n_actuation_mode;
i=0;
while (~feof(fileID)) %Untill end of file
    temp1 = fgetl(fileID);
    
if isempty(temp1) || strcmp(temp1,' ')
    i=0;
else  i=i+1;
    if i==1
        var_name=temp1;
        eval(strcat(var_name,'=[]'));
    else
        inputtext=textscan(temp1, '%f');
        temp_out=[inputtext{:}];
        eval(strcat(var_name,'(end+1)=temp_out;'));
    end  
end
end

M_ij=reshape(Mij,3,4);
M_ij=M_ij';

Q_ijk=zeros(4,8,8);
i=0;
for ii=1:4
    for jj=1:8
        for kk=1:8
            i=i+1;
            Q_ijk(ii,jj,kk)=Qijk(i);
        end
    end
end
            
