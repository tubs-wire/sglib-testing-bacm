function [xn, W_r, lambda_r, x2xn, xn2x]=low_rank_transform(x, varargin)
options=varargin2options(varargin);
[energy_rate,options]=get_option(options, 'energy_rate', 0.999);
[transpose_x,options]=get_option(options, 'transpose', true);
check_unsupported_options(options, mfilename);

if transpose_x
    x=x';
end
% take fluctuating part
mu_x = mean(x);
x_fluct = binfun(@minus, x, mu_x);
var_x = cov(x_fluct);

% low rank approx
[W, lambda, sumlambda]=pcacov(var_x);
r_ind=sumlambda>1-energy_rate;
W_r=W(:, r_ind);
lambda_r=lambda(r_ind);
xn=x_fluct*W_r*sqrt(diag(1./lambda_r));

if transpose_x
    xn=xn';
    x2xn=@(x)(binfun(@minus, x', mu_x)*W_r*sqrt(diag(1./lambda_r)))';
    xn2x=@(xn)(binfun(@plus, xn'*sqrt(diag(lambda_r))*(W_r)', mu_x))';
else
   x2xn=@(x)binfun(@minus, x, mu_x)*W_r*sqrt(diag(1./lambda_r));
   xn2x=@(xn)binfun(@plus, xn*sqrt(diag(lambda_r))*(W_r)', mu_x); 
end

