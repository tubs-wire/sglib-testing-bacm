function D=my_distance(x, varargin)
% MY_DISTANCE(X) calculates the distances in between the points of x
% D= MY_DISTANCE(X, OPTIONS) creates an N by N by DIM matrix with distances.
% The ijk-th element of D gives abs(x(k,i)-x(k,j)). X is an DIM by N
% matrix.
% If the 'take norm', 'true' optional is given, then D is an N by N matrix,
% and the Eucliclidien norm of the distances are given.
%
% Example (<a href="matlab:run_example my_distance">run</a>)
%
% See also dist, distance

%   Noemi Friedman
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[take_norm,options]=get_option(options, 'take_norm', false);
check_unsupported_options(options, mfilename);

X1=reshape(x',1,size(x',1), size(x',2));
X2=permute(X1,[2,1,3]);
D=binfun(@minus, X1, X2);
if take_norm
    D=sqrt(sum(D.^2, 3));
else
    D=abs(D);
end
