% Frequency analysis of eigenmotions
clear variables
% Load vectors Alpha, Time, V_TAS
load('Signal_for_eigenmotions_long.mat'); 
Alpha = alpha;

sampling_period = 0.01; %dt=1/fs
discard_samples = 0;
frequency_resolution = 0.01;

frequency_analyses(V_TAS, 0, sampling_period, 0.01);
xlim([0,1])
frequency_analyses(Alpha, 0, sampling_period, 0.01);
xlim([0,1])

%% Sinthetic signal
dt = 0.01;
t = 0:dt:100;                   % Time vector
x = 0.7*sin(2*pi*0.5*t) + sin(2*pi*0.25*t)+cos(2*pi*0.75*t); % Sum of a 10 Hz sinusoid and a 25 Hz sinusoid

subplot(2,1,1)
plot(t,x)
title('Signal')% Corrupted with Zero-Mean Random Noise')
xlabel('time')

%% FFT without padding zeros
                    % Sample time
Fs = 1/dt;                   % Sampling frequency
%y = x + 2*randn(size(t));   % Sinusoids plus noise
L = length(x);               % Length of signal
if mod(L,2)~= 0
    x=x(1:end-1);
    L=L-1;
end
    

%NFFT = 2^nextpow2(L); % Next power of 2 from length of y
NFFT = L;
Y = fft(x,NFFT)/L;
f = Fs / 2 * linspace( 0, 1, NFFT/2 + 1 );

% Plot single-sided amplitude spectrum.
subplot(2,1,2)
plot(f,2*abs(Y(1: NFFT/2 + 1)))
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
xlim([0,1])
[ampl,ind]=findpeaks(2*abs(Y(1:100)));

freq=f(ind);
%% Pad it with zeros

x= [x, zeros(1,10000)];
L = length(x);               % Length of signal
if mod(L,2)~= 0
    x=x(1:end-1);
    L=L-1;
end
    

%NFFT = 2^nextpow2(L); % Next power of 2 from length of y
NFFT = L;
Y = fft(x,NFFT)/L;
f = Fs / 2 * linspace( 0, 1, NFFT/2 + 1 );
