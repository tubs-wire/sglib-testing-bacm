%% Generate inthetic signal
dt = 0.01;
t = 0:dt:50;                   % Time vector
x = 0.7*sin(2*pi*0.5*t) + sin(2*pi*0.25*t)+cos(2*pi*0.75*t); % Sum of a 10 Hz sinusoid and a 25 Hz sinusoid

subplot(3,1,1)
plot(t,x)
title('Signal')% Corrupted with Zero-Mean Random Noise')
xlabel('time')

%% FFT without padding zeros
Fs = 1/dt;                   % Sampling frequency
%y = x + 2*randn(size(t));   % Sinusoids plus noise
N = length(x);               % Length of signal

if mod(N,2)~= 0
    x=x(1:end-1);
    N=N-1;
end

Y = fft(x,N)/N;
f = Fs / 2 * linspace( 0, 1, N/2 + 1 );

% Plot single-sided amplitude spectrum.
subplot(3,1,2)
plot(f,2*abs(Y(1: N/2 + 1)))
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('2|Y(f)|')
xlim([0,1])
[ampl,ind]=findpeaks(2*abs(Y(1:100)),'SortStr','descend');
strvarexpand(...
'With padding zeros:freq=$f(ind(1))$ freq=$f(ind(2))$ freq=$f(ind(3))$');

%% Pad it with zeros to increase frequency resolution
% wanted frequency resolution
df = 0.025;
[f ,Y, f_half, Y_abs_half] = frequency_analyses(x, 0, dt, df);

% Plot single-sided amplitude spectrum.
subplot(3,1,3)
plot(f_half,2*Y_abs_half)
title('Single-Sided Amplitude Spectrum of y(t) by padding with zeros')
xlabel('Frequency (Hz)')
ylabel('2|Y(f)|')
xlim([0,1])
[pks,locs,w,p] = findpeaks(2*Y_abs_half);
[ampl,ind] = findpeaks(2*Y_abs_half,'SortStr','descend');
strvarexpand(...
'With padding zeros:freq=$f(ind(1))$ freq=$f(ind(2))$ freq=$f(ind(3))$');

