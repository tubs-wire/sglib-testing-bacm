function [f ,Y, f_half, Y_abs_half] = frequency_analyses(x, i_discard, dt, df)
% x -  signal to be analyzed
% i_discard  - number of first samples to be discarded from the signal
% dt -  the sampling period of the signal (data_vector)
% df: the desired frequency resolution

% Discard first samples if needed
if i_discard > 0
    x(1:i_discard) =[];
end

% Length of the signal
N = (1/dt)/df;

% Pading zeros
X = zeros(N,1);
X(1:length(x)) = x;

f = (0:df:(1/dt)-1);

Y = fft(X(1:N),N)/N;
Y_abs_half = abs(Y(1:N/2+1));
f_half = f(1:N/2+1);    
% figure
% plot(f(1:L/2+1),Y_abs(:,1));
% xlabel('frequency [Hz]');

end