function [x_quant, xp, p]=gpc_pdf_and_quant(x_i_alpha, V_x, y, ind, varargin)
%
%% Check optional inputs

options=varargin2options(varargin);
[pdf_type,options]=get_option(options, 'pdf_type', 'kernel');
[N,options]=get_option(options, 'N', 10000);
check_unsupported_options(options,mfilename);

%% Initialize

n_y=size(y,1);
if isempty(ind)||nargin<3
    ind=1:n_y;
end

%% Sample from gpc

n_ind=length(ind);
x=gpc_sample(x_i_alpha, V_x, N);

%% compute quantiles from samples

x_quant=gpc_quantiles(V_x, x_i_alpha);

%% compute pdf from samples

n=1000;
xp=zeros(n_ind, n);
p=zeros(n_ind, n);

for j=1:n_ind
    x_j=x(ind(j),:);
    [xp(j,:), p(j,:)]=sample_density(x_j, 'type', pdf_type, 'n', n);
end

end
function [xc,y]=sample_density(x, varargin)
options=varargin2options(varargin);
[type, options]=get_option(options, 'type', 'kernel');
[n, options]=get_option(options, 'n', 1000);
[kde_sig, options]=get_option(options, 'kde_sig', []);
check_unsupported_options(options, [mfilename, '/sample']);

switch(type)
    case 'hist'
        [xc,y]=histogram(x, n);
    case {'kernel', 'kde'}
        [xc,y]=kernel_density(x, n, kde_sig);
    otherwise
        error('Possible TYPE options: HIST, KERNEL, KDE');
end
end
function [xc,y]=histogram(x, n)
[nbin,xc]=hist(x, n);
if n>1
    dx = (xc(2) - xc(1));
else
    dx = (max(x) - min(x));
end
y = nbin / sum(nbin) / dx;
end
