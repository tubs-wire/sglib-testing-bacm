function Colors=set_my_favorite_colors()
% Example
% color_names=fieldnames(Colors)
% x=1:10;
% figure
% hold on
% for i=1:length(color_names)
%     eval([color_names{i},'=Colors.(color_names{i})']);
%     plot(x, i/10*x, 'Color', eval(color_names{i}), 'LineWidth', 3)
% end
% legend(color_names)


Colors.dark_grey=[0.4, 0.4, 0.4];
Colors.grey=[205, 205, 193]/255;
Colors.orange = [1, 0.5, 0.2];
Colors.bordeau=[153, 0, 0]/255;
Colors.pastel_blue=[204, 229, 255]/255;
Colors.greenish_grey=[49, 79, 79]/255;
Colors.dark_green=[0, 100, 0]/255;
Colors.metal_grey=[112, 138, 144]/255;
Colors.lavender=[230, 230, 250]/255;
Colors.cadet_blue=[95, 158, 160]/255;
Colors.navy_blue=[0, 0, 128]/255;
Colors.steel_blue=[70, 130, 180]/255;
Colors.light_pink=[255, 182, 193]/255;
