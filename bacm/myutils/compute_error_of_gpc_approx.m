function [mean_err, mean_err_i, err, err_i]=...
    compute_error_of_gpc_approx(u_i_alpha, V_u, q_v, u_v, q2germ_func, x)
% set dimensions
n_var=size(u_v,2);
N=size(u_v,3);
% response with surrogate
u_p=gpc_evaluate(u_i_alpha, V_u, q2germ_func(q_v));
u_p=reshape(u_p,size(u_v));
% diff between responses from surrogate and High Fidelity Model
[rel_err, rel_err_i]=get_rel_deviation(u_p, u_v, x);
err_i = mean(abs(u_p-u_v),3);
err = mean(err_i,2);
rel_err_i=reshape(rel_err_i, n_var, N);

mean_err=mean(rel_err);
mean_err_i=mean(rel_err_i, 2);
end

function [rel_err, rel_err_i, norm_ERR_i]=...
    get_rel_deviation(u, u_val, x, varargin)
% GET_REL_DEVIATION computes the relative deviation of Us from the
% U_val reference values(s).U_val is a matrix of size d1xd2, where d1 corresponds
% to the length of response vector and d2 to the number of responses
% U's dimension is (d1xd2xN), where for each N solution an error is
% computed
% Outputs:
% - NORM_ERR_I = ||u_i-u_val_i||
% - REL_ERR_I = ||u_i-u_val_i||/||u_val_i||
% - REL_ERR =||u(:)-u_mval(:)||/||u_val(:)||(if SEP_RESPONSE = false)
% puts the different responses in one vector and computes one euclidien norm
% -  REL_ERR = avarage(REL_ERR_I)  (if SEP_RESPONSE = true) 
%
%Example
%REL_ERR=GET_REL_DEVIATION_FROM_MEAS(U, U_VAL,'SEP_RESPONSE', FALSE)

options=varargin2options(varargin);
[sep_response,options]=get_option(options, 'sep_response', true);
check_unsupported_options(options, mfilename);

if length(size(u_val))==3;
    [a, b, c]=size(u_val);
else
    [a,b]=size(u_val);
end
if length(size(u))~=3
    u=reshape(u, a,b, []);
end
c=size(u,3);

if ~sep_response
    u=reshape(u, a*b,[]);
    u_val=reshape(u_val, a*b);
end

% get Grammien
if nargin>2 &&~isempty(x)
    [~, els] = create_mesh_1d(0, 1, length(x));
    G = mass_matrix(x', els);
else
    G=eye(a);
end
if ~sep_response
    G=repmat({G},n_out, 1);
    G =blkdiag(G{:});
end
% deviations from measurement
ERR= binfun(@minus, u, u_val);
% norm of different of the measurement
if length(size(u_val))==2
    norm_ref=sqrt (dot(u_val, G*u_val, 1));
else
    norm_ref=sqrt (dot(u_val, reshape(G*reshape(u_val,a,b*c),a,b,c),1));
end
norm_ERR_i=sqrt(dot   (ERR,  reshape(G*reshape(ERR,a,b*c),a,b,c),  1));
% relative errors of the different responses
rel_err_i=binfun(@times, norm_ERR_i , 1./norm_ref);
if sep_response
    % take the avarage of the norm
    rel_err=squeeze(sum(rel_err_i,2))/size(u,2);
end
end