function [rel_err, rel_err_i, norm_ERR_i]=...
    get_rel_deviation_from_meas(u, u_meas, x, varargin)
% GET_REL_DEVIATION_FROM_MEAS computes the relative deviation of U from the
% U_MEAS measurement. U_MEAS is a matrix of size d1xd2, where d1 corresponds
% to the length of response vector and d2 to the number of responses
% U's dimension is (d1xd2xN), where for each N solution an error is
% computed
% Outputs:
% - NORM_ERR_I = ||u_i-u_meas_i||
% - REL_ERR_I = ||u_i-u_meas_i||/||u_meas_i||
% - REL_ERR =||u(:)-u_meas(:)||/||u_meas(:)||(if SEP_RESPONSE = false)
% puts the different responses in one vector and computes one euclidien norm
% -  REL_ERR = avarage(REL_ERR_I)  (if SEP_RESPONSE = true) 
%
%Example
%REL_ERR=GET_REL_DEVIATION_FROM_MEAS(U, U_MEAS,'SEP_RESPONSE', FALSE)

options=varargin2options(varargin);
[sep_response,options]=get_option(options, 'sep_response', true);
[ignore_indices,options]=get_option(options, 'ignore_indices',[]);
[weighted_norm,options]=get_option(options, 'weighted_norm',true);
check_unsupported_options(options, mfilename);

if length(size(u_meas))==3
    [a, b, ~]=size(u_meas);
else
    [a,b]=size(u_meas);
end
if length(size(u))~=3
    u=reshape(u, a,b, []);
end
c=size(u,3);
if ~isempty(ignore_indices)
    a_ind=setdiff(1:a,ignore_indices);
    u=u(a_ind, :, :);
    u_meas=u_meas(a_ind, :, :);
    x=x(a_ind);
    a=length(a_ind);
end

if ~sep_response
    u=reshape(u, a*b,[]);
    u_meas=reshape(u_meas, a*b);
end

% get Grammien
if weighted_norm && nargin>2 &&~isempty(x)
    [~, els] = create_mesh_1d(0, 1, length(x));
    G = mass_matrix(x', els);
else
    G=eye(a);
end
if ~sep_response
    G=repmat({G},n_out, 1);
    G =blkdiag(G{:});
end
% deviations from measurement
ERR= binfun(@minus, u, u_meas);
% norm of different of the measurement
if length(size(u_meas))==2
    norm_ref=sqrt (dot(u_meas, G*u_meas, 1));
else
    norm_ref=sqrt (dot(u_meas, reshape(G*reshape(u_meas,a,b*c),a,b,c),1));
end
norm_ERR_i=sqrt(dot   (ERR,  reshape(G*reshape(ERR,a,b*c),a,b,c),  1));
% relative errors of the different responses
rel_err_i=binfun(@times, norm_ERR_i , 1./norm_ref);
if sep_response
    % take the avarage of the norm
    rel_err=squeeze(sum(rel_err_i,2))/size(u,2);
end