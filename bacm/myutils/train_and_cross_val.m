function [err_i_p, maxerr_i_p]  = train_and_cross_val(Q, train_data, K, model_funcs, n_s)
% This function cross validates the differen order gPCEs
% Example:
% TRAIN_AND_CROSS_VAL_GPCES...
% (simparamset, q_points, u, K-fold, [2,3,4,5], ratiotrain/val, U_REF, coord)
% inputs:
% Q: prior paramset
% train_data:
% K: k-fold cross validation
% model_funcs: 
%      - e.g. model_funcs = ...
%    {@(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 3),
%     @(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 4)}


% n_s: separate ratio (# training samples/# validating samples)
%
%
%
%
%   Noemi Friedman
%   Copyright 2019, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


%% Initiate cross validation
% Fix seed number
%seed_numb=644;
q_j_l = train_data.params;
u_i_l = train_data.values;

% Number of training points
n_t=ceil(size(q_j_l,2)*n_s);

n_var = size(u_i_l, 1);

% Initialize memory
% avarage error of different gpc for all cross-val
err_i_p_k = zeros(n_var,length(model_funcs),K);
maxerr_i_p_k= zeros(n_var,length(model_funcs),K);
%% Cross validate

% k-validation
for k=1:K
    % Get index of training and validating set and the training points (q_t)
    dim_q=2;
    % [q_t, ind_t, ind_v] = ...
    %    shuffle_select_sort(q, dim_q, n_t, 'fixed_randseed', seed_numb);
    [q_j_t, ind_t, ind_v] = ...
        shuffle_select_sort(q_j_l, dim_q, n_t);
    % Training responses
    u_i_t=u_i_l(:,ind_t);
    % Validation points and responses
    q_j_v = q_j_l(:,ind_v);
    u_i_v = u_i_l(:,ind_v);
    % weights
    w_t = ((1.0/size(q_j_t,2)) * ones(size(q_j_t,2), 1)); 
    for ip=1:length(model_funcs)
        display(strvarexpand('Cross val $k$/$K$-fold, $ip$/$length(model_funcs)$ modelfunc'))
        model_func = model_funcs{ip};
        model = model_func(Q, q_j_t, u_i_t, w_t);
        us_i_v = model.compute_response(q_j_v);
        err_i_p_k( :, ip, k) = mean((us_i_v - u_i_v),2);
        maxerr_i_p_k( :, ip, k) = max((us_i_v - u_i_v),[], 2);
    end
    %seed_numb=seed_numb+10;
end 

err_i_p = mean(err_i_p_k, 3);
maxerr_i_p = max(err_i_p_k, [], 3);
end




