function xp=peak_of_sample_probability(x, varargin)

options=varargin2options(varargin);
[kde_sig, options]=get_option(options, 'kde_sig', []);
[N, options]=get_option(options, 'N', 100);
check_unsupported_options(options, [mfilename, '/sample']);

% number of variables
n=size(x,1);
% initialize vector for peak values of the variables
xp=zeros(n,1);
for j=1:n
    x_j=x(j,:);
    [xc, y_j]=kernel_density(x_j, N, kde_sig);
    [~, ind]=max(y_j);
    xp(j)=xc(ind);
end


                   