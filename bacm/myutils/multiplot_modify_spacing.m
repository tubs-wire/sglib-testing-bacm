function h = multiplot_modify_spacing(h, h_space, v_space, varargin)
%H: handles of subplots in a matrix ordered as the subplots
% INPUTS:
%H_SPACE: tot spacewidth/full figwidth OR vector with width of vertical
%gap spaces
%V_SPACE: 1 spaceheight/full figheight OR vector with height of horizontal
%gap spaces
options=varargin2options(varargin);
[double_space_margin, options]=get_option(options, 'double_space_margin', 'true');
check_unsupported_options(options,mfilename);

% number of subplots horizontally
nh=size(h,2);
% number of subplots vertically
nv=size(h,1);

if length(h_space)>1
    h_space_j=h_space;
    h_space=sum(h_space_j);
else
    if double_space_margin
        hgap_j=h_space/(nh+3);
        h_space_j=repmat(hgap_j, 1, nh+1);
        h_space_j(1,[1,nh+1])=2*h_space_j(1,[1,nh+1]);
    else
        hgap_j=h_space/(nh+1);
        h_space_j=repmat(hgap_j, 1, nh+1);
    end
end

if length(v_space)>1
    v_space_i=v_space;
    v_space=sum(v_space_i);
else
    if double_space_margin
        vgap_i=v_space/(nv+3);
        v_space_i=repmat(vgap_i, 1, nv+1);
        v_space_i(1,[1,nv+1])=2*v_space_i(1,[1,nv+1]);
    else
        vgap_i=v_space/(nv+1);
        v_space_i=repmat(vgap_i, 1, nv+1);
    end
end

% width of the subplots
v(3) = (1-h_space)/nh;
% height of the subplots
v(4) = (1-v_space)/nv;
for i=1:nv
    for j=1:nh
        % left normalized coordinate
        v(1)=sum(h_space_j(1:j))+(j-1)*v(3);
        % button normalized coordinate
        v(2)=1-(sum(v_space_i(1:i))+i*v(4));
        % change spacing
        set(h(i,j), 'pos', v)
    end
end
end