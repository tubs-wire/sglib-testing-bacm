function xp=sample_density_peak(x, varargin)

options=varargin2options(varargin);
[type,options]=get_option(options, 'type', 'kde');
check_unsupported_options(options,mfilename);

n=size(x,1);
xp=zeros(n,1);
for j=1:n
    x_j=x(j,:);
    [xc, y_j]=sample_density(x_j, 'type', type);
    [~, ind]=max(y_j);
    xp(j)=xc(ind);
end

end
function [xc,y]=sample_density(x, varargin)
options=varargin2options(varargin);
[type, options]=get_option(options, 'type', 'kernel');
[n, options]=get_option(options, 'n', 1000);
[kde_sig, options]=get_option(options, 'kde_sig', []);
check_unsupported_options(options, [mfilename, '/sample']);

switch(type)
    case 'hist'
        [xc,y]=histogram(x, n);
    case {'kernel', 'kde'}
        [xc,y]=kernel_density(x, n, kde_sig);
    otherwise
        error('Possible TYPE options: HIST, KERNEL, KDE');
end
end
function [xc,y]=histogram(x, n)
[nbin,xc]=hist(x, n);
if n>1
    dx = (xc(2) - xc(1));
else
    dx = (max(x) - min(x));
end
y = nbin / sum(nbin) / dx;
end
