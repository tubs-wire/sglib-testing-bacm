function [x_train, ind_train, ind_valid] = shuffle_select_sort(x, dim, n, varargin)

% Choses randomly along the DIM dimension of X n*100% of the data along
% this dimension and puts it in X_NEW.
% If DIM=2, X_TRAIN = X(:, IND_TRAIN) and X(:, IND_VALID) is part of the
% data that was emitted

options=varargin2options(varargin);
[fixed_randseed,options]=get_option(options, 'fixed_randseed', []);
check_unsupported_options(options, mfilename);

if ~isempty(fixed_randseed)
    rand_seed(fixed_randseed);
end
if isempty(dim)
    dim=1;
end
n_x=size(x,dim);
if nargin<3
    n=n_x;
end
ind_train=randperm(n_x);
ind_train=sort(ind_train(1:n));
ind_valid=setdiff(1:n_x,ind_train);
if dim==2
    x_train=x(:,ind_train);
elseif dim==1
    x_train=x(ind_train,:);
end
end