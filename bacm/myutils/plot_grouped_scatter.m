function mh=plot_grouped_scatter(samples, varargin)
% PLOT_GROUPED_SCATTER Makes 2D scatter subplots in the off-diagonal of the
% a subplot matrix and PDFs or HISTOGRAMS in the diagonal of the subplots
% from the D dimensional SAMPLES.
% SAMPLES is either a matrix of size (N x , or a cell with different type of SAMPLES, but with agreeing dimension.
%   PLOT_DENSITY Long description of plot_density.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example plot_grouped_scatter">run</a>)
% Q1=MySimParamSet();
% Q1.add('p1', NormalDistribution(0, 1));
% Q1.add('p2', BetaDistribution(1, 1));
% Q1.add('p3', ExponentialDistribution(1));
% figure
% plot_grouped_scatter(Q1.sample(1000),...
% 'Color', 'b', 'Labels', Q1.param_names,...
% 'FontSize', 12, 'Type', 'pdf');
%
% Q2=MySimParamSet();
% Q2.add('p1', NormalDistribution(0, 0.1));
% Q2.add('p2', TranslatedDistribution(BetaDistribution(1, 1), -0.1, 0.1));
% Q2.add('p3', ExponentialDistribution(0.1));
% figure
% plot_grouped_scatter({Q1.sample(1000), Q2.sample(1000), Q1.mean},...
% 'Color', 'bcr', 'Legends', {'dist1', 'dist2', 'dist1 mean'}, 'Labels', Q1.param_names,...
% 'FontSize', 12, 'Type', 'pdf', 'shifted', true);
% See also

%   Noemi Friedman
%   Copyright 2014, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


if ~iscell(samples)
    samples={samples};
end
ng=length(samples);



options=varargin2options(varargin);
[title, options]=get_option(options, 'title', '');
[shifted, options]=get_option(options, 'shifted', false);
[plot_symm_part, options]=get_option(options, 'plot_symm_part', false);
[MarkerType,options]=get_option(options, 'MarkerType', repmat('.',1,ng));
[Type,options]=get_option(options, 'Type', 'kde');
[LineType,options]=get_option(options, 'LineType', repmat({'-'},1,ng));
[MarkerSize,options]=get_option(options, 'MarkerSize', repmat(5,1,ng));
[LineWidth,options]=get_option(options, 'LineWidth', repmat(2,1,ng));
[Labels,options]=get_option(options, 'Labels', {});
[Legends,options]=get_option(options, 'Legends', 'default');
[Color,options]=get_option(options, 'Color', 'default');
[FontSize,options]=get_option(options, 'FontSize', 12);
[DensityOptions, options]=get_option(options, 'DensityOptions', {});
check_unsupported_options(options,mfilename);

%% Define colors
if strcmp(Color, 'default')
    colors='brcgyk';
    if length(colors)<ng
        repmat(colors,1,ceil(ng/length(colors)))
    end
    Color=colors(1:ng);
end
if ~iscell(Color)
    Color=mat2cell(Color, 1, ones(length(Color),1));
end

%%
if shifted && plot_symm_part
    warning('for shifted scatter plot plot_symm_part is not an optional input')
    plot_symm_part=false;
end

%%
switch Type
    case 'hist'
        diagtype='Frequency';
    case {'kernel', 'kde'}
        diagtype='PDF';
    case 'pdf'
        diagtype='PDF';
        Type='kde';
    otherwise
        error('for TYPE possible options: KERNEL or KDE (for ploting pdf or HIST for ploting histogram in the diagonal');
end

%% number of variables
n=size(samples{1},1);
Labelsv=cell(n,1);
if isempty(Labels)
    Labelsh=cell(n,1);
    
    for i=1:n
        % Vertical labels
        Labelsv{i}=strvarexpand('p$i$/$diagtype$');
        % Horizontal labels
        Labelsh{i}=strvarexpand('p$i$');
    end
else
    for i=1:n
        Labelsh=Labels;
        Labelsv{i}=strvarexpand('$Labels{i}$/$diagtype$');
    end
end
if shifted
    Labelsv=[Labelsh(2:end); diagtype];
    Labelsh=[diagtype; Labelsh(1:end-1)];
end
%%
ordering='col';
%% get_axis_ranges with taking the union of ranges
r=zeros(n,2,ng);
ind_vlines=false(ng,1);
for ig=1:ng
    samples_i=samples{ig};
    % Get ranges
    r(:,:,ig)=minmax(samples_i);
    % get number of one-point groups
    if size(samples_i,2)==1;
        ind_vlines(ig)=true;
    end
end
% Range length
r_min=min(r(:,1,:), [], 3);
r_max=max(r(:,2,:), [], 3);
rl=r_max-r_min;
% Extend a bit the range
r_min=r_min-0.05*rl;
r_max=r_max+0.05*rl;
%%
mh=multiplot_init(n, n, 'ordering', ordering, 'title', title, 'title_dist', 0);

for ig=1:ng %loop over different sample groups
    samples_i=samples{ig};
    for j=1:n % loop over variables in one direction
        for k=1:n % loop over variables in the other direction
            if shifted || ~plot_symm_part
                if j-k < 0
                    continue
                end
            end
            if shifted
                if j==k
                    if j<n
                        multiplot(n,j+1)
                    else
                        continue
                    end
                else
                    multiplot(j-1,k+1)
                end
            else
                multiplot(j,k)
            end
            if j==k
                % if there is only one point, draw a vertical line instead
                % of the PDF
                if ind_vlines(ig)
                    
                else
                    [~,~,h]=my_plot_density(samples_i(j,:), DensityOptions{:}, 'type', Type);
                    xlim([r_min(j), r_max(j)]);
                    switch Type
                        case 'hist'
                            h.FaceColor=Color{ig};
                        case {'kernel', 'kde', 'pdf'}
                            h.LineWidth=LineWidth(ig);
                            h.LineStyle=LineType{ig};
                            h.Color=Color{ig};
                        otherwise
                            error('for TYPE possible options: KERNEL or KDE (for ploting pdf or HIST for ploting histogram in the diagonal');
                    end
                end
                
                
                %                 if ng>1 && FlagLegend
                %                     if isempty(Legends{ig}); Legends{ig}=strvarexpand('$ig$: ');end
                %                     h.DisplayName=strvarexpand('$Legends{ig}$ PDF of $Labels{j}$');
                %                     legend('-DynamicLegend');
                %                 end
                
            else
                plot(samples_i(k,:), samples_i(j,:), MarkerType(ig),...
                    'MarkerSize', MarkerSize(ig), 'Color', Color{ig}, 'LineWidth', 2)
                xlim([r_min(k), r_max(k)])
                ylim([r_min(j), r_max(j)])
            end
            hold all
        end
    end
    if shifted
        for j=1:n-1
            multiplot(j,1)
            if ind_vlines(ig)
                
            else
                [~,~,h]=my_plot_density(samples_i(j+1,:), DensityOptions{:}, 'type', Type, 'flip', true);
                ylim([r_min(j+1), r_max(j+1)]);
                switch Type
                    case 'hist'
                        h.FaceColor=Color{ig};
                    case {'kernel', 'kde', 'pdf'}
                        h.LineWidth=LineWidth(ig);
                        h.LineStyle=LineType{ig};
                        h.Color=Color{ig};
                    otherwise
                        error('for TYPE possible options: KERNEL or KDE (for ploting pdf or HIST for ploting histogram in the diagonal');
                end
            end
            hold all
        end
        
    end
end


%% draw at the end the vertical lines belonging to one point samples
if sum(ind_vlines)>0;
    full_ind=1:ng;
    for ig=full_ind(ind_vlines)
        samples_i=samples{ig};
        for j=1:n
            if ~shifted
                multiplot(j,j)
            elseif j==n
                continue
            else
                multiplot(n,j+1)
            end
            y_lim=ylim;
            h = plot([samples_i(j,:), samples_i(j,:)], y_lim);
            h.LineWidth=LineWidth(ig);
            h.LineStyle=LineType{ig};
            h.Color=Color{ig};
        end
    end
end
%% do on the left hand side the lines too for the shifted case
if shifted
    if sum(ind_vlines)>0;
        for ig=full_ind(ind_vlines)
            samples_i=samples{ig};
            for j=2:n
                multiplot(j-1,1)
                
                x_lim=xlim;
                h = plot(x_lim, [samples_i(j,:), samples_i(j,:)] );
                h.LineWidth=LineWidth(ig);
                h.LineStyle=LineType{ig};
                h.Color=Color{ig};
            end
        end
    end
end


%%
fig=gcf;
set(findall(fig,'-property','FontSize'),'FontSize',FontSize)
% all_text = findall(fig, 'Type', 'Text');
% set(all_text, 'FontSize', FontSize);
all_axis = findall(fig, 'Type', 'Axes');
set(all_axis, 'FontSize', FontSize-1)%'XGrid', 'on', 'YGrid', 'on');

%% Change spacing between gaps
hspaces=0.0005*ones(n+1,1);
hspaces(1)=0.07;
vspaces=0.005*ones(n+1,1);
vspaces(n+1)=0.07;
if ~isempty(title)
    vspaces(1)=0.07;
end
if ~isempty(Legends)
    hspaces(n+1)=0.07;
    vspaces(1)=0.07;
end
%multiplot_modify_spacing(mh, hspaces, vspaces);

%% ticks, labels and legends
if plot_symm_part
    legend_flag=@(j,k)(j==1 && k==n);
elseif shifted
    legend_flag=@(j,k)(j==1 && (k==1 || k==2));
else
    legend_flag=@(j,k)((j==1 && k==1)||(j==2 && k==1));
end
for j=1:n
    for k=1:n
        multiplot(j,k)
        if k==1
            ylabel(Labelsv{j}, 'FontSize', FontSize+2)
        else
            set(gca,'YTick',[]);
        end
        if j==n
            xlabel(Labelsh{k}, 'FontSize', FontSize+2)
        else
            set(gca,'XTick',[]);
        end
        if  legend_flag(j,k)
            if strcmp(Legends, 'default')
                legend()
            else
                legend(Legends)
            end
        end
    end
end

%% delete upper diagonal figures
if ~plot_symm_part
    data=multiplot_data;
    
    for j=1:n
        for k=1:n
            if j-k<0
                if ~(shifted)
                    delete(data.handles(j,k));
                elseif shifted && (j-k)<-1
                    delete(data.handles(j,k));
                end
            elseif shifted && j==n && k==1
                delete(data.handles(j,k));
            end
        end
    end
end