function [x_min, x_max, x_p, p]=plot_gpc_mean_and_quant(x_i_alpha, V_x, y, ind, varargin)
%
%% Check optional inputs
options=varargin2options(varargin);
[line_color,options]=get_option(options, 'line_color', 'black');
[LineWidth, options]=get_option(options, 'LineWidth', 2);
[subplot_dim,options]=get_option(options, 'subplot_dim', []);
[pdf_type,options]=get_option(options, 'pdf_type', 'kernel');
[N,options]=get_option(options, 'N', 10000);
check_unsupported_options(options,mfilename);


%% Sample from gpc

x=gpc_sample(x_i_alpha, V_x, N);
n_y=size(x,1);
if isempty(ind)||nargin<3
    ind=1:n_y;
end

n_ind=length(ind);
%% compute quantiles from samples

[x_min, x_max]=gpc_quantiles(V_x, x_i_alpha);


%% compute pdf from samples

n=1000;
xp=zeros(n,n_ind);
p=zeros(n, n_ind);

for j=ind
    x_j=x(j,:);
    [xp(:,j), p(:,j)]=sample_density(x_j, 'type', pdf_type, 'n', n);
end

%% plot
plot_u_mean_quant(y, u_mean, u_quant)

end
function [xc,y]=sample_density(x, varargin)
options=varargin2options(varargin);
[type, options]=get_option(options, 'type', 'kernel');
[n, options]=get_option(options, 'n', 1000);
[kde_sig, options]=get_option(options, 'kde_sig', []);
check_unsupported_options(options, [mfilename, '/sample']);

switch(type)
    case 'hist'
        [xc,y]=histogram(x, n);
    case {'kernel', 'kde'}
        [xc,y]=kernel_density(x, n, kde_sig);
    otherwise
        error('Possible TYPE options: HIST, KERNEL, KDE');
end
end
function [xc,y]=histogram(x, n)
[nbin,xc]=hist(x, n);
if n>1
    dx = (xc(2) - xc(1));
else
    dx = (max(x) - min(x));
end
y = nbin / sum(nbin) / dx;
end
