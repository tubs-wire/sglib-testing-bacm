%% Define hyperparameters Theta
clear
Theta=SimParamSet();
% Mean of the distribution
Theta.add(SimParameter('mu', UniformDistribution(2,3)))
% Std of the distribution
Theta.add(SimParameter('sigma', NormalDistribution(0,1)))
% the gpc germs are only needed now for easily defining a mesh on the
% domain of the hyperparameters:
V_theta=Theta.get_germ;

%% Generate artificial data y

% Define true distribution of the 'p' parameter:
mu_t=2.2;
sigma_t=0.5;
p_t=SimParameter('P', NormalDistribution(mu_t, sigma_t));
% data size
N_y=100;
% sample from 'p'
y=p_t.sample(N_y);

%% Probability of theta conditioned on the data (Bayesian identification of the hyperparameters)

% Define likelihood
x2likelihood=@(theta)( prod(normal_pdf_tensor(y, theta(1,:), theta(2, :)), 1));
% with parallel MCMC
theta_s=bayes_mcmc(x2likelihood, Theta, 10000, [], [], ...
    'parallel', true, 'plot', true);
% or with a single MCMC chain
theta_s=bayes_mcmc(x2likelihood, Theta, 1000, [], [], 'plot', true);

%% Expected pdf of p


