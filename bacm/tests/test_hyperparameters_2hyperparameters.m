%% Define hyperparameters Theta

Theta=SimParamSet();
% Mean of the distribution
Theta.add(SimParameter('mu', UniformDistribution(2,3)))
% Std of the distribution
Theta.add(SimParameter('sigma', NormalDistribution(0,0.5)))
% the gpc germs are only needed now for easily defining a mesh on the
% domain of the hyperparameters:
V_theta=Theta.get_germ;

%% Generate artificial data y

% Define true distribution of the 'p' parameter:
mu_t=2.2;
sigma_t=0.5;
p_t=SimParameter('P', NormalDistribution(mu_t, sigma_t));
% data size
N_y=100;
% sample from 'p'
y=p_t.sample(N_y);

%% Probability of theta conditioned on the data (Bayesian identification of the hyperparameters)

% Theta(omega_i) define a meshgrid to compute the posterior distribution
theta=Theta.germ2params(gpcgerm_linspace(V_theta, 100));
[MU, SIGMA]=meshgrid(theta(1,:), theta(2,:));
F=zeros(size(MU));
L=zeros(size(MU));
mu=MU(:);
sigma=SIGMA(:);
for i=1:length(mu)
    dist_p_i=NormalDistribution(mu(i), sigma(i));
    % the likelihood (the probability of measuring y_t when Theta(omega_i)
    L(i)=prod(dist_p_i.pdf(y));
    F(i)=L(i)*Theta.pdf([mu(i); sigma(i)]);
end
% The maximum a-priori probability estimate (MAP)
[fmax, ind]=max(F(:));
mu_MAP=mu(ind); display(strvarexpand('mu MAP: $mu_MAP$,  mu true: $mu_t$'));
sigma_MAP=sigma(ind); display(strvarexpand('sigma MAP: $sigma_MAP$,  sigma true: $sigma_t$' ));

%% Plot likelihood and a-posteriori pdf
subplot(2,1,1)
surf(MU, SIGMA, F, 'EdgeColor','none')
xlabel('mu')
ylabel('sigma')
zlabel('a-posteriori distribution')
title('A posteriori distribution and likelihood of the hyperparameters')
subplot(2,1,2)
surf(MU, SIGMA, L, 'EdgeColor','none')
xlabel('mu')
ylabel('sigma')
zlabel('likelihood')