function [x_beta, y_beta, V_xy, Rest_germ_x, Rest_germ_y]=...
    gpc_combine_normal_gpc_with_hyperparam_gpc(x_alpha, V_x, V_y)

% combined gpc basis (outer product: all combinations of the two gpc basis
% functions)
[V_xy, Pr_V_x, Pr_V_y, Rest_germ_x, Rest_germ_y] = ...
    gpcbasis_combine( V_x, V_y, 'outer_product', 'as_operators', true);
gpcbasis_polynomials(V_xy, 'symbols', 'xyz');
% gpc coeff of x in the new basis
x_beta =  x_alpha * Pr_V_x;
% compute gpc coeff of y
y_beta =  gpc_projection(@(xi)...
    (resp(xi,Rest_germ_y, x_alpha, V_x, Rest_germ_x)), V_xy, 3);
y_beta=chopabs(y_beta);


function y=resp(xi, Rest_germ_y, x_alpha, V_x, Rest_germ_x)
xi_y = Rest_germ_y * xi;
x = gpc_evaluate(x_alpha, V_x, Rest_germ_x * xi);
mu_y = x(1,:);
sigma_y = x(2,:);
y = mu_y + sigma_y .* xi_y;
%y = [mu_y; sigma_y; y];
