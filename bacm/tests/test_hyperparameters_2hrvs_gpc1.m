%% Define hyperparameters Theta
clear
Theta=SimParamSet();
% Mean of the distribution
Theta.add(SimParameter('mu', UniformDistribution(2,3)))
% Std of the distribution
Theta.add(SimParameter('sigma', UniformDistribution(0,1)))
% the gpc expand:
[theta_alpha, V_theta]=Theta.gpc_expand;

%% Generate artificial data y

% Define true distribution of the 'Z' parameter:
mu_t=2.2;
sigma_t=0.5;
Z_t=SimParamSet();
Z_t.add(SimParameter('z', NormalDistribution(mu_t, sigma_t)));
% data size
N_z=100;
% sample from 'p'
y=Z_t.sample(N_z);
% gpc basis
V_z=Z_t.get_germ;
V_z=gpcbasis_modify(V_z, 'p', 1);

%% Representation of the real random variable in a combined gpc basis
% Extend gpc basis
[theta_beta, z_beta, V_theta_z, Rest_germ_theta, Rest_germ_z]= ...
    gpc_combine_normal_gpc_with_hyperparam_gpc(theta_alpha, V_theta, V_z);

% The gpc is the same for each prediction of the measurements:
z_beta=repmat(z_beta, length(y), 1);

% right out the poloynomials of the basis (Here x, y is the germ for mu and
% sigma - representing the epistemic uncertainty --- and z is the germ for
% Z, --- representing the aleatoric uncertainty.
 gpcbasis_polynomials(V_theta_z, 'symbols', 'xyz')
 
 % and its probability distribution
 pdf_func=@(z, xi)normal_pdf_tensor(z',...
    gpc_evaluate(theta_alpha(1,:), V_theta, xi), ...
    gpc_evaluate(theta_alpha(2,:), V_theta, xi));

%% Bayesian identification of the hyperparameters with MCMC

%Bayesian identification by MCMC
% Define likelihood
L_func=@(xi)( prod(pdf_func(y, xi), 1));
Xi=gpcgerm2simparamset(V_theta);
%xi_s=bayes_mcmc(L_func, Xi, [], [], [], 'plot', true);
xi_s=bayes_mcmc(L_func, Xi, [], [], [], 'plot', true, 'parallel', true, 'T', 100);

theta_s=Theta.germ2params(xi_s);
plot_grouped_scatter({Theta.sample(10000), theta_s, [mu_t; sigma_t]}, ...
    'Labels', {'mu', 'sigma'}, 'Legends', {'prior', 'posterior', 'tru value'});
%% Bayesian identification with MMSE - this is not working yet

% function predicting the measurements:
Y_func=@(xi)gpc_evaluate(z_beta, V_theta_z, xi);

% degree of esitmator
p_phi=2;

% computing integration orders, and new gpc's max degree
p_gpc= gpcbasis_info(V_z, 'total_degree');
p_theta = gpcbasis_info(V_theta, 'total_degree');

p_int_mmse=max(p_gpc*p_phi+1,ceil((p_theta+p_phi*p_gpc+1)/2));
% The new gpc basis of theta will have this order (to which the
% composition of Z_func = Y_func + E_func and the PHI_func will be projected) 
p_pn=p_gpc*p_phi;
p_int_proj=p_pn+1;

[Thetan_beta, V_n, phi_func]=...
    my_mmse_update_gpc_basic(theta_beta, Y_func, V_theta_z, y, ...
    p_phi, p_int_mmse, 'int_grid', 'full_tensor');

%% Or with the Kalman filter

