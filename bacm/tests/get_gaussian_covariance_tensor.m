function C=get_gaussian_covariance_tensor(x,l,sigma)

%X: is a vector of positions of size d x NX
%L: is a vector of correlation lengths of length NL
%SIGMA: is a scalar value, or a vector of size Nsigma
% C: covariance matrix of size NX x NX x NL x Nsigma

D=pdist(x');

C=zeros([size(x,2), size(x,2), length(l)]);
for i=1:length(l)
    Dperl =(D./l(i));
    C(:,:,i,:)=squareform(exp( -Dperl.^2));
end
ind=repmat(eye(length(x)),1,1,length(l));
C(ind~=0)=1;

if length(sigma)~=1
    C=binfun(@times, reshape(sigma.^2, 1,1,1,[]), C);
else
    C=binfun(@times, sigma.^2, C);
end