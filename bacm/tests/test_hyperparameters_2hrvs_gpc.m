%% Define hyperparameters Theta
clear
Theta=SimParamSet();
% Mean of the distribution
Theta.add(SimParameter('mu', UniformDistribution(2,3)))
% Std of the distribution
Theta.add(SimParameter('sigma', NormalDistribution(0,1)))
% the gpc expand:
[theta_alpha, V_theta]=Theta.gpc_expand;

%% Generate artificial data y

% Define true distribution of the 'p' parameter:
mu_t=2.2;
sigma_t=0.5;
P_t=SimParameter('P', NormalDistribution(mu_t, sigma_t));
% data size
N_y=100;
% sample from 'p'
y=P_t.sample(N_y);

%% Representation of the real random variable

pdf_func=@(z, xi)normal_pdf_tensor(z,...
    gpc_evaluate(theta_alpha(1,:), V_theta, xi), ...
    gpc_evaluate(theta_alpha(2,:), V_theta, xi));

%% Probability of theta conditioned on the data (Bayesian identification of the hyperparameters)

% Define likelihood
L_func=@(xi)( prod(pdf_func(y, xi), 1));
% A posteriori
xi_post_pdf=@(xi)(L_func(xi).*gpcgerm_pdf(V_theta, xi));

% Evidence
[xi, w] = gpc_integrate([], V_theta, 12);

theta=Theta.germ2params(xi);
evid=0;
for i=1:length(w)
    xi_i=xi(:,i);
    w_i=w(i);
    evid=evid+xi_post_pdf(xi_i)*w_i;
end  
%%
xi_s=gpcgerm_linspace(V_theta, 100);
theta_s=Theta.germ2params(xi_s);

[XI_MU, XI_SIGMA]=meshgrid(xi_s(1,:), xi_s(2,:));
[MU, SIGMA]=meshgrid(theta_s(1,:), theta_s(2,:));

F=reshape(xi_post_pdf([XI_MU(:)'; XI_SIGMA(:)']), size(MU))/evid;
% L=reshape(L_func([XI_MU(:)'; XI_SIGMA(:)']), size(MU));

% The maximum a-posteriori probability estimate (MAP)
[fmax, ind]=max(F(:));
mu_MAP=MU(ind); display(strvarexpand('mu MAP: $mu_MAP$, mu true: $mu_t$'));
sigma_MAP=SIGMA(ind); display(strvarexpand('sigma MAP: $sigma_MAP$,  sigma true: $sigma_t$' ));

surf(MU, SIGMA, F, 'EdgeColor','none')
xlabel('mu')
ylabel('sigma')
zlabel('a-posteriori distribution')
title('A posteriori distribution of the hyperparameters')

%% Project pdf to gpc basis

%% Predicted distribution
V_theta_ex=gpcbasis_modify(V_theta, 'p', 10);
u_i_beta = gpc_projection(xi_post_pdf, V_theta_ex);
f = gpc_evaluate(u_i_beta, V_theta_ex, [XI_MU(:)'; XI_SIGMA(:)']);
surf(MU, SIGMA, reshape(f,size(MU)))

%% Predicted distribution: expected pdf of p

% for this we need the pdf function of the posterior thetas (conditioned on
% the data:
% pdf_theta_y=@kernel_density(theta_s



