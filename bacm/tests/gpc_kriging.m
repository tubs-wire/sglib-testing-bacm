function [predict_func, MMSE_func]=gpc_kriging(q, q2germ_func, y, corr_func, V_regr, theta, varargin)

options=varargin2options(varargin);
[energy_rate, options]=get_option(options, 'energy_rate', 0.999);
check_unsupported_options(options, mfilename);

%% Initialize - define true response surface, design sites, data

% n dim of design space (S in R mxn )
n = size(q,1); %dimension
m = size(q,2); %number of disign sites
xi=q2germ_func(q);
S = xi';

% q dim of response space (Y in R mxq)
dim_y = size(y,1); %number of responses
check_boolean(m==size(y,2), ...
    'the second dimension of the design sites and the response has to be the same', mfilename);
Y = y';

%% normalise response and low rank approx

[Yn, ~, ~, ~, yn2y]=low_rank_transform(Y, ...
    'transpose', false, 'energy_rate', energy_rate);

% mu_S = mean(S);
% var_S = cov(S);
% RY = chol(var_Y);
% Yn = binfun(@minus, Y, mu_Y)/RY;

%% normalise design sites

mu_S = mean(S);
var_S = cov(S);
RS = chol(var_S);
s2sn=@(S)(binfun(@minus, S, mu_S)/RS);
sn2s=@(Sn)(binfun(@plus, mu_S, xn*RS));
Sn = s2sn(S);

%% Rename everything

S = Sn;
Y = Yn;

%% Define basis functions for regresssion model (F is R mxp)

% Define regression functions 
f=@(x)(gpcbasis_evaluate(V_regr, sn2s(x)))';
% Design matrix
F=f(S);
%p = size(F,2);

%% Correlation stuff (R is mxm)

cov_func=@(x)(reshape(corrgauss(theta, ...
    reshape(my_distance(x'), [], size(x,2))), size(x,1), size(x,1)));
R=cov_func(S);

get_submat=@(A, i, j)(A(i,j));

r = @(x)( get_submat(cov_func([S;x]),...
     1:size(S ,1), (size(S ,1)+(1:size(x,1)))));
 
%% Get least square solution (solve for y_i_alpha and sigma2) 
% Cholesky factorization with check for pos. def.
[C, rd] = chol(R);
if  rd
    error(' R is not positive definite')
end

% Get least squares solution
C = C';
Ft = C \ F;
[Q, G] = qr(Ft,0);
if  rcond(G) < 1e-10
  % Check   F  
  if  cond(F) > 1e15 
    error('F is too ill conditioned\nPoor combination of regression model and design sites')
  else  % Matrix  Ft  is too ill conditioned
    return 
  end 
end
Yt = C \ Y;
y_i_alpha = G \ (Q'*Yt);
% compute the variance of the error of the regression model
rho = Yt - Ft*y_i_alpha;
sigma2 = sum(rho.^2)/m;
% transform back the error
gamma=rho' / C;
% regression function
ref=@(x)f(x)*y_i_alpha;
% cost (obj) function (to be minimised):
%detR = prod( full(diag(C)) .^ (2/m) );
%obj = sum(sigma2) * detR;

%% Plot true response surface, data, prediction of response surface, regression model

predict_func=@(x)(yn2y(f(s2sn(x)) * y_i_alpha + r(x)'*gamma'));

%% MSE of the predictor

%phi = @(x)( diag( 1 + c(x)' * (R*c(x) - 2 * r(x)))*sigma2 );

% or differently
rt = @(x)(C \ r(x));
u = @(x)(G \ (Ft.' * rt(x) - f(x)'));
MMSE_func =@(x) (1 + sum(u(s2sn(x)).^2) - sum(rt(s2sn(x)).^2))'*sigma2;







