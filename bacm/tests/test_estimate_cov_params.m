clear
multiplot_init(2,1)


cov_model = @(x, y, theta)(gaussian_covariance(x, y, theta(1), 1));

x1 = 0;
x2 = 10;
[pos, els] = create_mesh_1d(x1, x2, 100);
G_N = mass_matrix(pos, els);

l_true = 1.5/2;
theta_true = [l_true];
m = 60;

n = 17;
ind=sort(randperm(length(pos), n));
ind=round(linspace(1, 30, n));



[g_i_alpha, I_g, C, sigma_g] = expand_gaussian_field_pce(@(x,y)(cov_model(x, y, theta_true)), pos, G_N, m );
g_i_alpha(:,1)=[];
I_g(1,:)=[];

[g_i, xi] = pce_field_realization(g_i_alpha, I_g);

multiplot
plot(pos, g_i)



t_k = pos(ind);
g_k = g_i(ind);

hold all
plot(t_k, g_k, 'xr');



%% estimate theta

t = [];
L = [];
for theta = linspace(theta_true-0.2, theta_true+0.2, 21)
    Rt = covariance_matrix(t_k, @(x,y)(cov_model(x, y, theta)));
    %R = pce_covariance(g_i_alpha(ind,:), I_g)
    %L = chol(R, 'lower');
    %L*L'-R
    %pce_covariance(L\g_i_alpha(ind,:), I_g)
    
    % MLE
    
    %[gt_i_alpha, I_gt] = expand_gaussian_field_pce(@(x,y)(cov_model(x, y, theta)), pos, G_N, m );
    %gt_i_alpha(:,1)=[];
    %I_gt(1,:)=[];
    
    %Rt = pce_covariance(gt_i_alpha(ind, :), I_gt);
    p = multi_normal_pdf(g_k, zeros(size(g_k)), Rt);
    
    t(end+1) = theta;
    L(end+1) = p;
    strvarexpand('theta=$theta$, p=$p$');
end
multiplot
plot(t,L)

    % f_gauss(g_k, Rt)
    
    
