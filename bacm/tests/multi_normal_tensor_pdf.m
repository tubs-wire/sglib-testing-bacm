function y=multi_normal_tensor_pdf( x, mu, Sigma, varargin )

options=varargin2options(varargin);
[log_flag,options]=get_option(options, 'log_flag', false);
check_unsupported_options(options, mfilename);

% NORMAL_PDF Probability distribution function of the normal distribution.
%   Y=NORMAL_PDF( X, MU, SIGMA ) computes the pdf for the normal dist. for
%   all values in X, which may be a vector. MU and SIGMA can be specified
%   optionally.
%
% Example (<a href="matlab:run_example normal_pdf">run</a>)
%   x=linspace(-1,5);
%   f=normal_pdf(x,2,.5);
%   F=normal_cdf(x,2,.5);
%   plot(x,f,x(2:end)-diff(x(1:2)/2),diff(F)/(x(2)-x(1)))
%
% See also NORMAL_CDF

%   Elmar Zander
%   Copyright 2006, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


if nargin<2
    mu=0;
end
if nargin<3
    Sigma=1;
end
%n = size(x,1);
dx = binfun(@plus, x, -mu);
dx(dx==Inf)=1e100;
dx(dx==-Inf)=-1e100;

y=zeros(size(dx, 2), size(dx, 3));
if size(dx,3)~=size(Sigma,3)
    if size(dx,3)==1
        dx=repmat(dx,1,1,size(Sigma,3));
    elseif size(Sigma,3)==1
        Sigma=repmat(Sigma, 1,1,size(dx,3));
        if size(Sigma,4)==1
            Sigma=repmat(Sigma, 1,1,1, size(dx,3) );
        end
    else
        error('Matrix dimensions do not match')
    end
end
for i=1:size(dx,3)
    Sigma_i=squeeze(Sigma(:,:,i,i));
    dx_i=dx(:,:,i);
    if log_flag
        y(:,i)= -sum( dx_i.*(inv(Sigma_i)*dx_i ),1)/2  -1/2*log(det ( 2*pi*Sigma_i)) ;
    else
        y(:,i)=exp( -sum( dx_i.*(inv(Sigma_i)*dx_i ),1)/2 )./(sqrt( det ( 2*pi*Sigma_i) ));
    end
end


