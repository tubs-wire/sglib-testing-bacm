%% Initialise -  determine response surface and set data

% true response
y = @(x)(sum((x-0.5).^2,2) + 1);

% n dim of design space (S in R mxn = 4x2)
n = 2;
m = 5;
S = rand(m, n);

% q dim of response space (Y in R mxq = 4x1)
q = 1;
Y = y(S);

%% Normalise sites and response

mu_S = mean(S);
var_S = cov(S);
RS = chol(var_S);
Sn = binfun(@minus, S, mu_S)/RS;

mu_Y = mean(Y);
var_Y = cov(Y);
RY = chol(var_Y);
Yn = binfun(@minus, Y, mu_Y)/RY;

yn = @(xn)(binfun(@minus, y(binfun(@plus, mu_S, xn*RS)), mu_Y)/RY);

%% Rename everything 

% Normalised design sites
S = Sn;
% Transformed, normalised design sites
Y = Yn;
% map from s to y
y = yn;

%% Set regresssion model

% regression model
f=@(x)[ones(1, size(x,2)); x];
%design matrix
F=f(S')';
% number of basis functions (F is R mxp)
p = size(F,2);

%% Correlation stuff (R is mxm)

% supposed correlation length of regression error
theta=6.6;
l = theta;
% supposed variance of regression error
sigma2 = 1.6;

% compute correlation of error of the regression model (at design sites)
R_func=@(x,w)gaussian_covariance(x, w, l, 1);
R = covariance_matrix( S', R_func );

% define r(x) giving the correlation at given sites with the design sites
get_submat=@(A, i, j)(A(i,j));
r = @(x)( get_submat(covariance_matrix...
    ( [S', x], R_func ), 1:size(S',2), (size(S',2)+(1:size(x,2)))));

%% Solve Langrangian stuff (determine 'c(x)' and 'lambda' from minimising MSE))

%Solve systen if equation L*[c(x); lambda(x)]=[r(x); f(x)]
%1. Define L matrix
L = [R F; F' zeros(p, p)];
% Solve system of eq cl=[c(x); lambda(x)]
cl = @(x)(L\[r(x); f(x)]);
% Get the 'c' part from cl
c = @(x)( get_submat(cl(x), 1:m, 1:size(x,2) ) );
%c([0 1; 0 1])

%% regression model

% coeffs of the regression model
beta=(F'/R*F)\(F'/R*Y);
% regression model
reg_mod=@(x)(f(x'))'*beta;


%% Plot true response surface and data

% define meshgrid for normalised s
[S1m,S2m]=meshgrid(linspace(min(S(:))-0.2,max(S(:))+0.2,20));
% evaluate function on meshgrid
eval_on_grid = @(f, X, Y)(reshape(f([X(:), Y(:)]), size(X)));

%
subplot(2,1,1)
hold off;
% True resonse evaluated at meshgrid
Ys=eval_on_grid(yn, S1m, S2m);
h(1)=surf(S1m, S2m, Ys, 'FaceAlpha', 0.5);
hold on;
dY = 0.5;
% data (shifted slightly with dY)
h(2)=plot3(S(:,1), S(:,2), Y+dY, 'rx', 'MarkerSize', 10, 'LineWidth', 2)
foo = reshape([S(:,1), S(:,2), Y+dY, S(:,1), S(:,2), Y, nan(m, 3)]', 3, [])';
line(foo(:,1), foo(:,2), foo(:,3), 'Color', 'r');
% Predicted response surface evaluated at meshgrid
yhat = @(x)( c(x')' * Y);
h(3)=surf(S1m, S2m, 0.4+eval_on_grid(yhat, S1m, S2m));
% Check whether prediction is correct at design sites
h(4)=surf(S1m, S2m, eval_on_grid(reg_mod, S1m, S2m), 'FaceAlpha', 0.5);
y(S)-yhat(S)
xlabel('s1')
ylabel('s2')
zlabel('y')
legend(h, {'true response surface', 'data', 'predicted response surface', 'regression model'})
title('Response surface');


%% MSE of the predictor

Y_hat=eval_on_grid(yhat, S1m, S2m);
% MSE function:
phi = @(x)( sigma2 * diag( 1 + c(x')' * (R*c(x') - 2 * r(x'))));
subplot(2, 1, 2)
% plot MSE of predictionat meshgrid
surf(S1m, S2m, eval_on_grid(phi, S1m, S2m));
hold on;
% check MSE at design sites
phi(S)
% plot data MSE
plot3(S(:,1), S(:,2), zeros(size(S, 1),1), 'rx', 'MarkerSize', 10, 'LineWidth', 2)
legend('MSE of predictor', 'data')
title('Squared error of prediction')
xlabel('s1')
ylabel('s2')
zlabel('E[(y-y_p)^2]')



