%% Initialize - define true response surface, design sites, data
clf
clear
% true response
y = @(x)([sum((x-0.5).^2,2) + 1, sum((x-0.4).^3 , 2) + 1]);

% n dim of design space (S in R mxn = 4x2)
n = 2; %dimension
m = 20; %number of disign sites
S = rand(m, n);

% q dim of response space (Y in R mxq = 4x1)
q = 2; %number of responses
Y = y(S);

%% normalise response and low rank approx

[Yn, W, lambda, y2yn, yn2y]=low_rank_transform(Y, 'transpose', false);

% mu_S = mean(S);
% var_S = cov(S);
% RY = chol(var_Y);
% Yn = binfun(@minus, Y, mu_Y)/RY;

%% normalise design sites

mu_S = mean(S);
var_S = cov(S);
RS = chol(var_S);
Sn = binfun(@minus, S, mu_S)/RS;

%% Rename everything

yn = @(xn)(y2yn(y(binfun(@plus, mu_S, xn*RS))));
S = Sn;
Y = Yn;

%% Define basis functions for regresssion model (F is R mxp)

% Define regression functions 
f=@(x)[ones(size(x,1),1), x];
% Design matrix
F=f(S);
p = size(F,2);

%% Correlation stuff (R is mxm)
theta=0.1;
l = theta;

%R_func=@(x,w)gaussian_covariance(x, w, l, 1);
%R = covariance_matrix( S', R_func );
cov_func=@(x)(reshape(corrgauss(theta, reshape(my_distance(x'), [], size(x,2))), size(x,1), size(x,1)));
R=cov_func(S);
%D=my_distance(S');
%R=reshape(corrgauss(theta, reshape(D, [], n)), m, m);

get_submat=@(A, i, j)(A(i,j));
%r = @(x)( get_submat(covariance_matrix( [S', x], R_func ),...
%     1:size(S',2), (size(S',2)+(1:size(x,2)))));

r = @(x)( get_submat(cov_func([S;x]),...
     1:size(S ,1), (size(S ,1)+(1:size(x,1)))));
 
%% Get least square solution (solve for beta and sigma2) 
% Cholesky factorization with check for pos. def.
[C, rd] = chol(R);
if  rd
    error(' R is not positive definite')
end

% Get least squares solution
C = C';
Ft = C \ F;
[Q, G] = qr(Ft,0);
if  rcond(G) < 1e-10
  % Check   F  
  if  cond(F) > 1e15 
    error('F is too ill conditioned\nPoor combination of regression model and design sites')
  else  % Matrix  Ft  is too ill conditioned
    return 
  end 
end
Yt = C \ Y;
beta = G \ (Q'*Yt);
% compute the variance of the error of the regression model
rho = Yt - Ft*beta;
sigma2 = sum(rho.^2)/m;
% transform back the error
gamma=rho' / C;
% regression function
ref=@(x)f(x)*beta;
%detR = prod( full(diag(C)) .^ (2/m) );
%obj = sum(sigma2) * detR;


%% Solve Langrangian stuff
% minimise the MLE by solving the system of equations:
% L[c(x);lambda(x)]=[r(x); f(x)]

% matrix L
L = [R F; F' zeros(p, p)];
% solve system of eq.
cl = @(x)(L\[r(x); f(x)']);
% take c
c = @(x)( get_submat(cl(x), 1:m, 1:size(x,1) ) );
%c([0 1; 0 1])

%% Plot true response surface, data, prediction of response surface, regression model

% meshgrid for s
[S1m,S2m]=meshgrid(linspace(min(S(:))-0.2,max(S(:))+0.2,20));
eval_on_grid = @(f, X, Y, dim)(reshape(f([X(:), Y(:)]), size(X, 1), size(X, 2), dim));

% Plot true response surface
subplot(1,2,1)
hold off;
Ys=eval_on_grid(yn, S1m, S2m, q);
h(1)=surf(S1m, S2m,squeeze(Ys(:,:,1)) , 'FaceAlpha', 0.5);
hold on;
% Plot data
dY = 0.2;
h(2)=plot3(S(:,1), S(:,2), Y(:,1)+dY, 'rx', 'MarkerSize', 10, 'LineWidth', 2);
foo = reshape([S(:,1), S(:,2), Y(:,1)+dY, S(:,1), S(:,2), Y(:,1), nan(m, 3)]', 3, [])';
line(foo(:,1), foo(:,2), foo(:,3), 'Color', 'r')
% Plot regression model
Yr=eval_on_grid(ref, S1m, S2m, q);
h(3)=surf(S1m, S2m, squeeze(Yr(:,:,1)), 'FaceAlpha', 0.5);
% Plot prediction of response surface
yhat = @(x)( c(x)' * Y);
Yhat=0.1+eval_on_grid(yhat, S1m, S2m, q);
h(4)=surf(S1m, S2m, squeeze(Yhat(:,:,1)));
display(yn(S)-yhat(S));
% Plot prediction differently
% Scaled predictor 
Yhatv = eval_on_grid(@(x)(f(x) * beta + r(x)'*gamma'), S1m, S2m, q);
h(5)=surf(S1m, S2m, squeeze(Yhatv(:,:,1)));

%% Check with dace
dmodel=dacefit(S, Y, @regpoly1, @corrgauss, theta);

%% MSE of the predictor

phi = @(x)( diag( 1 + c(x)' * (R*c(x) - 2 * r(x)))*sigma2 );

% or differently
rt = @(x)(C \ r(x));
u = @(x)(G \ (Ft.' * rt(x) - f(x)'));
phiv =@(x) (1 + sum(u(x).^2) - sum(rt(x).^2))'*sigma2;

%% Plot square error estimator

subplot(1, 2, 2)
PHI=eval_on_grid(phi, S1m, S2m, q);
surf(S1m, S2m, squeeze(PHIV(:,:,1)), 'FaceAlpha', 0.5);
hold on
% or using the different approach
PHIV=eval_on_grid(phiv, S1m, S2m, q);
surf(S1m, S2m, squeeze(PHI(:,:,1)));

plot3(S(:,1), S(:,2), zeros(size(S,1),1), 'rx', 'MarkerSize', 10, 'LineWidth', 2);







