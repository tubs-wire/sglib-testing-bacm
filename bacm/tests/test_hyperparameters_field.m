%% Introduction
% let's suppose we have a gaussian random field U(x, \omega_a)~N(\mu(x),I)
% where we have no exact knowledge about the mean of the field, so we model
% it as a random field Z(x, omega_e)=\Sum_i sigma^2(l,sigma_z) z_i(x, l) \xi_i
% where we model xi_i are some independent RVs, let's suppose we know the
% variance, the

%% Generate artificial truth
multiplot_init(2,1)
cov_model = @(x, y, theta)(gaussian_covariance(x, y, theta(1), 1));

x1 = 0;
x2 = 10;
[pos, els] = create_mesh_1d(x1, x2, 100);
G_N = mass_matrix(pos, els);

l_true = 1;
theta_true = [l_true];
m = 30; %number of eigenfunctions
n = 50; %number of data points used
ind=sort(randperm(length(pos), n));
%ind=round(linspace(1, 30, n));



[g_i_alpha, I_g, C, sigma_g] = expand_gaussian_field_pce(@(x,y)(cov_model(x, y, theta_true)), pos, G_N, m );
g_i_alpha(:,1)=[];
I_g(1,:)=[];
XI=generate_stdrn_simparamset(ones(m,1));
xi_true=XI.sample(1);
[g_i, xi] = pce_field_realization(g_i_alpha, I_g, xi_true);
multiplot
plot(pos, g_i)

t_k = pos(ind);
g_k = g_i(ind);

hold all
plot(t_k, g_k, 'xr');
%% find theta with MCMC
t = [];
L1 = [];
L2=[];
for theta = linspace(theta_true-0.5, theta_true+0.5, 40)
    Rt = covariance_matrix(t_k, @(x,y)(cov_model(x, y, theta)));
    p = multi_normal_pdf(g_k, zeros(size(g_k)), Rt);
    %or differently
    [gj_i_alpha, I_gj, Rt, sigmaj_g] = expand_gaussian_field_pce(@(x,y)(cov_model(x, y, theta)), pos, G_N, m );
    xi_j=g_k\gj_i_alpha(ind, :);
    p2=XI.pdf(xi_j(2:end)');
    t(end+1) = theta;
    L1(end+1) = p;
    L2(end+1) =p2;
    strvarexpand('theta=$theta$, p=$p$');
end

multiplot
plot(t,L1)
hold on
%plot(t, L2)