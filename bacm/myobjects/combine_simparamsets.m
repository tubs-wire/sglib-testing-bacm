function [Q1Q2, ind_q1, ind_q2] = combine_simparamsets(Q1, Q2)
if iscell(Q1)
    n_Q = length(Q1);
    ind_q1 = cell(n_Q,1);
    for i=1:n_Q-1
        if i==1
            Q1_i = Q1{i};
        else
            Q1_i = Q1Q2;
        end
        Q2_i = Q1{i+1};
        if i==1
            [Q1Q2, ind_q1{i}, ind_q1{i+1}] = combine_two_simparamsets(Q1_i, Q2_i);
        else
            [Q1Q2, ~, ind_q1{i+1}] = combine_two_simparamsets(Q1_i, Q2_i);
        end
    end
    
else
    [Q1Q2, ind_q1, ind_q2] = combine_two_simparamsets(Q1, Q2);
end
end
function [Q1Q2, ind_q1, ind_q2] = combine_two_simparamsets(Q1, Q2)
Q1Q2 = MySimParamSet();
for i = 1:Q1.num_params
    param_i = Q1.get_param(i);
    Q1Q2.add(param_i);
end
for i = 1:Q2.num_params
    param_i = Q2.get_param(i);
    Q1Q2.add(param_i);
end

ind_q1 = 1:Q1.num_params;
ind_q2 = Q1.num_params+1:Q1.num_params+Q2.num_params;
end
