function XI=gpcgerm2simparamset(V)

dists=gpcgerm_dists(V);
XI=dists2simparamset(dists);
end
