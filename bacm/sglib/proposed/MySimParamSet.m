classdef MySimParamSet < SimParamSet
 %%
 methods
     function add(set, param, varargin)
            % ADD Add a parameter to the param set.
            if ischar(param)
                param = MySimParameter(param, varargin{:});
            end
            check_type(param, 'SimParameter', true, 'Inputs of SimParamSet', mfilename);
            if set.param_map.iskey(param.name)
                warning('sglib:gpcsimparams_add_parameter', 'The given SimParameter name is already the name of a parameter in the SimParameterSet, and will be overwritten')
            end
            set.param_map.add(param.name, param);
     end
 end
    
    
    %%
     methods
        function param=params2struct(set, x)
            %PARAM2STRUCT(SET,X) puts the X values of the parameters to a
            %structure PARAM.(name of ith parameter in the set)=x(i)
            %the structure is usefull, when the parameters in the black box
            %has to passed in a structure format
            check_boolean( size(x,1)==set.num_params, strvarexpand('x need to have $set.num_params$ rows'), mfilename );
            param=struct;
            m=set.num_params;
            
            for i=1:m
                name_i=set.param_map.values{i}.name;
                param.(name_i)=x(i,:);
            end
        end
        function param=stdnor2params(set, xi)
            check_boolean( size(xi,1)==set.num_params, strvarexpand('xi need to have $set.num_params$ rows'), mfilename );
            N=size(xi,2);
            m=set.num_params;
            param=zeros(m,N);
                        
            for i=1:m
                dist_i=set.param_map.values{i}.dist;
                if isa(dist_i, 'NormalDistribution')
                    param(i,:)=base2dist(dist_i, xi(i,:));
                else
                    param(i,:)=dist_i.stdnor(xi(i,:));
                end
            end
        end
        function xi=params2stdnor(set, params)
            check_boolean( size(params,1)==set.num_params, strvarexpand('xi need to have $set.num_params$ rows'), mfilename );
            N=size(params,2);
            m=set.num_params;
            xi=zeros(m,N);
                        
            for i=1:m
                dist_i=set.param_map.values{i}.dist;
                if isa(dist_i, 'NormalDistribution')
                    xi(i,:)=dist2base(dist_i, params(i,:));
                else
                    xi(i,:)=dist_i.dist2stdnor(params(i,:));
                end
            end
        end
        
        function p_q=logpdf(set,q)
            % PDF Gives the log of the probability density of the parameters.
            %   P_Q=LOGPDF(SET, Q) returns the log of the probability density function of
            %   the parameter set at point Q
            m=set.num_params;
            assert(size(q,1)==m);
            n=size(q,2);
            params = set.get_params();
            p_q = zeros(1,n);
            for i=1:m
                p_q=p_q+params{i}.logpdf(q(i,:));
            end
        end
        
        function set_all_to_mean(set)
            %SET_ALL_TO_MEAN Set all parameters fixed to the mean value of its distribution.
            %   SET_ALL_TO_MEAN) fixes the parameter to the mean
            %   value of the distribution.
            % See also SET_FIXED, SET_NOT_FIXED
            m=set.num_params;
            ind_rv=1:m;
            ind_rv=ind_rv(set.find_rv);
            
            for i=ind_rv
                param_i=set.param_map.values{i};
                param_i.set_fixed(mean(param_i.dist));
            end
        end
        
        
     end
end