function dists=gpcgerm_dists(V)

n_germ=gpcbasis_size(V,2);
syschars=V{1};
if length(syschars)==1 && n_germ~=1
    syschars=repmat(syschars,1,n_germ);
end
dists=cell(n_germ,1);
for i=1:n_germ
    [~, dists{i}]=gpc_registry('get', syschars(i));
end