function  a_i_j=gpc_evaluate_by_block( a_i_alpha, V_a, xi, N_max )
if nargin<4
    N_max=10^4;
end
N=size(xi, 2);
if N>N_max
    n_loop=ceil(N/N_max);
    a_i_j=cell(1,n_loop);
    for i=1:n_loop
        nn=(i-1)*N_max + 1;
        xi_i=xi(: ,  nn: min([i*N_max, N]));
        a_i_j{i} = gpc_evaluate(a_i_alpha, V_a, xi_i);
    end
    a_i_j=cell2mat(a_i_j);
else
    a_i_j = gpc_evaluate( a_i_alpha, V_a, xi);
end
end