function [V_u, u_i_alpha, params2germs_func, germs2param_func]=generate_gPCE_surrogate(q, u, p_gpc, Q, w, varargin)
%
% Inputs:
% -q: param points (num_params x N) N:number of sample points
% -u: response (if there are several type of responses (length_resp_vect x
% num_resp x N), owtherwise (length_resp_vect, N)
%
options=varargin2options(varargin);
[flag_PCE, options]=get_option(options, 'flag_PCE',false);
[wp, options]=get_option(options, 'weight_pow',1);
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting',[]);
[method, options]=get_option(options, 'method','project'); %or 'colloc'
[weighted_regr, options]=get_option(options, 'weighted_regr',false);
check_unsupported_options(options, mfilename);

%% Generate gPCE basis for the proxi model

% Define approximating subspace
gpc_full_tensor=false; % complete or full tensor gpc basis
if flag_PCE %if gaussian germs are needed
    V_u=gpcbasis_create(repmat('h',1,Q.num_params),'p',p_gpc, 'full_tensor', gpc_full_tensor);
else
    V_u=Q.get_germ;
    V_u=gpcbasis_modify(V_u, 'p',p_gpc, 'full_tensor', gpc_full_tensor);
end

%% Map parameters to reference coordinate
%Define mapping from RVs to germs (standardly distributed random variables)
if flag_PCE
    params2germs_func=@(q)Q.params2stdnor(q);
    germs2param_func=@(xi)Q.stdnor2params(xi);
else
    params2germs_func=@(q)Q.params2germ(q);
    germs2param_func=@(xi)Q.germ2params(xi);
end
x_ref=params2germs_func(q);

%% II. Get gPCE coeffs with regression
if length(size(u))==3
    u_ij=reshape(u, size(u,1)*size(u,2), []);
else
    u_ij=u;
end
u_i_alpha = calculate_gpc_coeffs(V_u,  x_ref, u_ij, w, 'method', method, ...
'weight_pow', wp, 'ignored_indices_for_weighting', ind_ign,...
'weighted_regr', weighted_regr);


