function [xi_p_mean, C_xi_p]=KF(xi_mean, C_xi, C_y, H, y_m)

M=size(H,2);
K=C_xi * H'* inv(H*C_xi*H' + C_y);
xi_p_mean=xi_mean+K*(y_m-H*xi_mean);
C_xi_p = (eye(M) - K*H)*C_xi;
