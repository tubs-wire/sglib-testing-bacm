function h=gpc_plot_sensitivities(I, ratio_by_index, germ_names, varargin)

options=varargin2options(varargin);
[max_index,options]=get_option( options, 'max_index', inf);
[type,options]=get_option( options, 'type', 'plot');
[min_ratio,options]=get_option( options, 'min_ratio', 0);
check_unsupported_options(options,mfilename);


%% min and max order

I=I(multiindex_order(I)<max_index,:);
d_min=min(multiindex_order(I));
d_max=min(max_index, max(multiindex_order(I)));
d=d_min:d_max;

%% param names corresponding to the different indices

p_names=gpc_multiindex2param_names(I, germ_names);

%% seperate multiindex set with different degree

% initialize memory
I_d=cell(size(d));
ind_d=cell(size(d));
ratio_d=cell(size(d));
p_names_d=cell(size(d));
h=cell(size(d));
% seperate by order and sort in descending order
for i=d
    ind_d{i}=sum(I,2)==i;
    [ratio_d{i}, ind]=sort(ratio_by_index(ind_d{i}), 'descend');
    trunc_ind=ratio_d{i}>min_ratio;
    ratio_d{i}=ratio_d{i}(trunc_ind);
    
    I_i=I(ind_d{i}, :);
    I_d{i}=I_i(ind(trunc_ind), :);
    
    p_names_i=p_names(ind_d{i});
    p_names_d{i}=p_names_i(ind(trunc_ind));
end

%% Pie plots or table of sensitivities
switch type
    case {'tab','table'}
        
        for i=d
            p_names_l=put_to_latex_format(p_names_d{i});
            if i==1
                col_labels=[p_names_l{:}, {'second order', 'higher order'}];
                caption='First Sobol-indices and higher order indicies';
                label='tab: first_sobols';
                table_data=[ratio_d{i}, sum(ratio_d{2}), sum(ratio_by_index(~(ind_d{1}|ind_d{2})))];
            else
                col_labels=p_names_l;
                caption=strvarexpand('Sobol-indices of order $i$');
                table_data=ratio_d{i};
            end
            h{i}=latex_table(table_data, col_labels, caption, label);
        end
    case {'plot','pieplot'}
        for i=d
            figure
            if i==1
                h{i}=pie3([ratio_d{1}, sum(ratio_d{2}), sum(ratio_by_index(~(ind_d{1}|ind_d{2})))], ...
                    [p_names_d{i}{:}, {'second order', 'higher order'}]);
                title('First Sobol-indices and higher order indicies')
            else
                h{i}=pie3(ratio_d{i}, p_names_d{i});
                title(strvarexpand('Sobol-indices of order $i$'))
            end
        end
end
end

function tab=latex_table(ltable_data, col_labels, caption, label)
% best parameters and the relative error

input.data =ltable_data;

% Set column labels (use empty string for no label):
input.tableColLabels = col_labels;
% Set row labels (use empty string for no label):
input.tableRowLabels = {'Sobol-sensitivity'};
input.dataFormat = {'%.2f',size(ltable_data,2)};
input.tableColumnAlignment = 'l';
input.tableBorders = 0;
input.booktabs = 0;
% LaTex table caption:
input.tableCaption = caption;
if nargin>3
    input.tableLabel = label;
end
% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 0;
% call latexTable:
tab = latexTable(input);
end

function p_names=put_to_latex_format(p_names)
p_names=strcat('$', p_names, '$');

for i=1:length(p_names)
    tf = isspace(p_names{i});
    p_names{i}(tf)='-';
end
end
