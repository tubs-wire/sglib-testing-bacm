function [indexed_param_names]=gpc_multiindex2param_names(I_s, param_names)

indexed_param_names=cell(size(I_s,1),1);
max_d=max(sum(I_s,2));
min_d=min(sum(I_s,2));

% loop over sobol orders
ind_start=1;
for i=min_d:max_d
    % multiindex set with ith degree
    I_s_i=I_s(sum(I_s,2)==i, :);
    l_i=size(I_s_i,1);
    [indr, indc]=(find(I_s(sum(I_s,2)==i, :)==1));
    IND=(reshape((sortrows([indr, indc]))',2*i,[]))';
    indc=2:2:2*i;
    IND=IND(:, indc);
    param_names_i=(strcat(param_names(IND)));
    % put inner columns with spaces
    if i~=1
        p_names_i=cell(l_i, i*2-1);
        p_names_i(:,1:2:2*i-1)=param_names_i;
        p_names_i(:,2:2:2*i-1)={' '};
        p_names_i=p_names_i';
        indexed_param_names(ind_start:ind_start+l_i-1)=...
        mat2cell([p_names_i{:}],1,sum(cellfun('length',p_names_i)));
    else
        indexed_param_names(ind_start:ind_start+l_i-1)=param_names_i;
    end    
    
    ind_start=ind_start+l_i;
end

