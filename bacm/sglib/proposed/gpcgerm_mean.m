function means=gpcgerm_mean(V_u,varargin)
% GPCGERM_MEAN gives the mean values of the germs corresponding to the given
% gpc basis.
%   GPCGERM_MEAN(VARARGIN) Long description of gpcgerm_mean.
%
% Example (<a href="matlab:run_example gpcgerm_mean">run</a>)
%
% See also

syschars=V_u{1};
I = V_u{2};
n_germ=size(I,2);
if length(syschars)==1&& n_germ>1
    syschars=repmat(syschars,1,gpcbasis_size(V_u,2));
end

means=zeros(n_germ,1);
for i=1:n_germ
    dist_i=polysys_dist(syschars(i));
    means(i)=gendist_moments(dist_i);
end


%   Noemi Friedman
%   Copyright 2015, , Institute of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

