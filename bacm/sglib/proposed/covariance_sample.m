function C=covariance_sample(A, B)

N=size(A,2);
A_fluct=binfun(@minus, A, mean(A,2));
if nargin<2
    B_fluct=A_fluct;
else
    B_fluct=binfun(@minus, B, mean(B,2));
end
C=1/(N-1) * (A_fluct*B_fluct');
