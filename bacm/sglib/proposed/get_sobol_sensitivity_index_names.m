function names = get_sobol_sensitivity_index_names(I_s, param_names)
% GET_SOBOL_SENSITIVITY_INDEX_NAMES gives the names corresponding to the
% different sobol sensitivity indices.
% I_S is the index set defining the Sobol indices
% PARAM_NAMES is the name of the germs/parameters
%
%
% Example (<a href="matlab:run_example get_sobol_sensitivity_index_names">run</a>)
% V_u = gpcbasis_create('hhh', 'p', 3);
% u_i_alpha = rand(5, gpcbasis_size(V_u, 1));
% [partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(u_i_alpha, V_u);
% param_names ={'p1', 'p2', 'p3'};
% names = get_sobol_sensitivity_index_names(I_s, param_names);
% form = repmat('%-10s' , 1, size(I_s,1));
% fprintf([form,'\n'],names{:});
% form = repmat('%-10.4f' , 1, size(I_s,1));
% fprintf([form,'\n'], ratio_by_index);
%
% See also MULTIINDEX, GPC_BASIS_CREATE, GPC_MOMENTS,

%   Noemi Friedman
%   Copyright 2018, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


s = cell(size(I_s));
m = size(I_s, 2);


for i= 1:m
    s(I_s(:, i), i) = param_names(i);
    % add a '-' if other param names are followed
    if i<m
        ind = sum(I_s(:, i+1:m),2)>0 & I_s(:, i);
        s (ind, i) = strcat(s(ind,i), '-');
    end
    s(~I_s(:,i), i) = {''};
    if i==1
        names = s(:, i);
    else
        names = strcat(names, s(:, i));
    end
end
