function q_p_en=enKF(q_en, y_of_q, eps_en, z_m, varargin)
% X_P_EN=ENKF(Q_EN, Y_OF_Q, EPS_EN, Z_M) updates ensemble of some
% parameter XI from measurements of a measurable Y=Y_OF_Q(Q) poisened by
% some measurement noise ensemble EPS_EN using the ensemble Kalman Filter
%
% Outputs:
% - q_p_en: ensemble of posterior
%
% Inputs:
% - q_en: ensemble of the input RV (N_xi x N) (can be Gamma, Theta, or maybe even Q)
% - y_of_q: map q to what we can measure (y) (H)
% - eps_en: measurement noise ensemble (N_y x N)
% - z_m: actual measurement
%
% Other notation
%  - N_y=N_eps: size of the measureable
%  - N: ensemble size
%  - Y: the prediction of what we can measure from a mathematical model (without measurement noise)
%  - Z: the prediction of what we can measure (Z  = Y + E)

options=varargin2options(varargin, mfilename);
[cz_from_z, options]=get_option(options, 'cz_from_z', true);
check_unsupported_options(options);

%% Ensemble of y and of z
% ensemble of the prediction (from the prior samples)
y_en=funcall(y_of_q, q_en);
% Covariance of the prediction
C_y=covariance_sample(y_en);
% add measurement ensemble
z_en=y_en+eps_en;

%% cov(xi,y)
C_q_y=covariance_sample(q_en, y_en);

%% Measurement (data)
% Covariance of the measurement noise
C_eps=covariance_sample(eps_en);

%% update
if cz_from_z
    % or maybe better
    C_z=covariance_sample(z_en);
else
    C_z=C_y+C_eps;
end

q_p_en=q_en+C_q_y*(C_z\(binfun(@minus, z_m, z_en)));
