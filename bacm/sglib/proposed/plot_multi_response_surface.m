function xi = plot_multi_response_surface(u_i_alpha, V_u,  varargin)
% .
%   PLOT_MULTI_RESPONSE_SURFACE plots response surfaces for paired up
%   GPCgerms in a matrix plot. In the plot the germs not plotted are fixed
%   to mean values by default. It is also possible to get plots for the
%   quantiles as well with the optional input, but this is not implemented yet...
%
% Example 1 (<a href="matlab:run_example   plot_multi_response_surface 1">run</a>)
%   V_u = gpcbasis_create('U', 'm', 2, 'p', 3)
%   u_i_alpha = rand(3, gpcbasis_size(V_u, 1))
%   plot_multi_response_surface(u_i_alpha, V_u);
%
% Example 2 (<a href="matlab:run_example   plot_multi_response_surface 2">run</a>)
%
%   a1_dist=gendist_fix_bounds(gendist_create('beta', {1.2, 2}),0.5, 5);
%   a2_dist=gendist_fix_bounds(gendist_create('beta', {0.6, 0.3}),50, 150);
%   a3_dist=gendist_create('uniform', {1.2, 2});
%
%   [a1_alpha, V1]=gpc_param_expand(a1_dist, 'u');
%   [a2_alpha, V2]=gpc_param_expand(a2_dist, 't');
%   [a3_alpha, V3]=gpc_param_expand(a3_dist, 'p');
%
%   [ai_alpha12, V12]=gpc_combine_inputs(a1_alpha, V1, a2_alpha, V2);
%   [ai_alpha123, V123]=gpc_combine_inputs(ai_alpha12, V12, a3_alpha, V3);
%
%   plot_multi_response_surface(ai_alpha123(3,:), V123)
%
% See also PLOT_MULTIRESPONSE_SURFACE,  PLOT_RESPONSE_SURFACE, GPC,
% GPC_EVALUATE
%
%
% See also GPC, PLOT_RESPONSE_SURFACE,  GPC_REDUCE_BASIS_DIM

%   Noemi Friedman and Elmar Zander
%   Copyright 2015, Inst. of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Optional inputs

options=varargin2options(varargin);
[germ2param, options]=get_option(options, 'germ2param', {});
[germ_index, options]=get_option(options, 'germ_index', 1:gpcbasis_size(V_u,2));
[response_index, options]=get_option(options, 'response_index', 1:size(u_i_alpha,1));
[germ_fix_vals, options]=get_option(options, 'germ_fix_vals', []);
[name_of_RVs, options]=get_option(options, 'name_of_RVs', {});
[prior_gpc_base, options]=get_option(options, 'prior_gpc_base', []);
[plot_fix_response, options]=get_option(options, 'plot_fix_response', []);
[name_of_response, options]=get_option(options, 'name_of_response', '');
[figtitle, options]=get_option(options, 'figtitle', '');
[N, options]=get_option(options, 'N', 20);
[type, options]=get_option(options, 'type', 'surf');
check_unsupported_options(options,mfilename);


%% check input, initialize
% Number of random variables (germs):
n_germ=gpcbasis_size(V_u,2);
% Responses to plot
N_u=size(u_i_alpha,1);


if ~isempty(response_index)
    u_i_alpha=u_i_alpha(response_index,:);
else
    check_boolean(N_u<7, 'to many response surfaces has to be plotted', mfilename);
end
% check fixed values of germs/params
if isempty(germ_fix_vals)
    germ_fix_vals=gpcgerm_moments(V_u);
end
% check MAP_GERM2RV
if isempty(germ2param)
    germ2param=@(x)(eye(n_germ)*x);
end

% check NAME_OF_RVS
if ~isempty(name_of_RVs)
    if isa(name_of_RVs, 'char')
        name_of_RVs={name_of_RVs};
    elseif isa(name_of_RVs, 'cell')
        check_boolean(length(name_of_RVs)==n_germ, 'length of NAME_OF_RVS does not match with the number of germs', mfilename);
    end
else
    name_of_RVs=cell(1,n_germ);
    for i=1:n_germ
        name_of_RVs{i}=strvarexpand('p$i$');
    end
end
% check NAME_OF_RESPONSE
if ~isempty(name_of_response)
    if isa(name_of_response, 'char')
        name_of_response={name_of_response};
    elseif isa(name_of_RVs, 'cell')
        check_boolean(length(name_of_response)==N_u, 'length of NAME_OF_RESPONSE does not match with the number of responses', mfilename);
    end
else
    name_of_response=cell(1,n_germ);
    for i=1:size(u_i_alpha,1)
        name_of_response{i}=strvarexpand('u$i$');
    end
end
% Check figure title name, and create one automaticly if not defined
if isempty(figtitle)
    if N_u>1
        figtitle='Dependence of random parameters on the responses';
    else
        figtitle=strvarexpand('Dependence of random parameters on $name_of_response$');
    end
end

if isempty(plot_fix_response)&&N_u==1; legendflag=false;else legendflag=true;end

%% Plot response surfaces


multiplot_init(length(germ_index), length(germ_index), 'ordering', 'col', 'title', figtitle, 'title_dist', 0);
for i=germ_index;
    for j=germ_index
        if i==j; dim_ind=i; else dim_ind=[i,j]; end
        multiplot;
        %get indexed
        RV_name_ij=get_indexed_elements(name_of_RVs, dim_ind);
        %Define mapping from germ to param
        germ2param_ij=@(xi_lk)(  get_indexed_elements( germ2param(get_germs(germ_fix_vals, dim_ind, xi_lk )), dim_ind  ) );
        fixed_germ_ind=~ismember(1:n_germ, dim_ind);
        if i~=j
            [u_i_alpha_red, V_u_red]=gpc_reduce_basis_dim(u_i_alpha, V_u, [i,j], 'fixed_vals', germ_fix_vals(fixed_germ_ind));
            if ~isempty(prior_gpc_base)
                pr_gpc_ij= prior_gpc_base(dim_ind);
            else
                pr_gpc_ij= prior_gpc_base;
            end
            %define optional inputs for response surface plot
            optional_input.germ2param=germ2param_ij;
            optional_input.name_of_RVs=RV_name_ij;
            optional_input.name_of_response=name_of_response;
            optional_input.plot_fix_response=plot_fix_response;
            optional_input.prior_gpc_base=pr_gpc_ij;
            optional_input.legend_flag=legendflag;
            optional_input.zlabel_flag= false;
            optional_input.N=N;
            optional_input.type=type;
            
            %plot response surface
            myplot_response_surface(u_i_alpha_red, V_u_red, optional_input)
            
        else
            [u_i_alpha_red, V_u_red]=gpc_reduce_basis_dim(u_i_alpha, V_u, i, 'fixed_vals', germ_fix_vals(fixed_germ_ind));
            N_x=100;
            x=linspace(0,1,N_x);
            %x_i = gpcgerm_sample(V_u_red, N, 'rand_func', @(m, n)(x(:)));
            %x_i = polysys_sample_rv(V_u_red{1}, x);
            x_i = gpcgerm_linspace(V_u_red,100);
            u = gpc_evaluate(u_i_alpha_red, V_u_red, x_i);
            x_i=feval(germ2param_ij, x_i);
            plot(x_i, u)
            if ~isempty(plot_fix_response)
                hold on
                plot(x_i, repmat(plot_fix_response, size(x_i)))
            end
            xlim([min(x_i), max(x_i)])
            xlabel(RV_name_ij{1});
            if legendflag
                if isempty(plot_fix_response)
                    legend(name_of_response)
                else
                    legend(name_of_response, 'measurment mean');
                end
            end
        end
        
    end
end

end

function indexed_var=get_indexed_elements(var, ind)
if min(size(var))==1
    indexed_var=var(ind);
else
    indexed_var=var(ind,:);  
end
end
function germ_vals_l_k=get_germs(fixed_vals, dim_ind, xi_l_k)
n_k=size(xi_l_k,2);
germ_vals_l_k=repmat(fixed_vals, 1, n_k);
germ_vals_l_k(dim_ind,:)=xi_l_k;
end