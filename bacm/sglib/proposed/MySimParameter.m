classdef MySimParameter < SimParameter
    
    methods
        function param = MySimParameter(name, dist_or_num, varargin)
            param@SimParameter(name, dist_or_num, varargin);
        end
        function [q_alpha, V_q, varerr]=gpc_expand(param, varargin)
            % Expands the parameter in the default
            % polynomyal system of the distribution (optionaly defined by
            % POYSYS). See EXPAND_OPTIONS more in GPC_PARAM_EXPAND
            options=varargin2options(varargin);
            [normalized,options]=get_option(options, 'normalized', true);
            [syschar,options]=get_option(options, 'syschar', param.get_gpc_syschar(normalized));
            [expand_options,options]=get_option(options, 'expand_options', {});
            check_unsupported_options(options, mfilename);
            
            [q_alpha, V_q, varerr]=gpc_param_expand(param.dist, syschar, expand_options);
        end
        
        function x = linspace(param, n)
            y = [0,1];
            x_bounds = param.dist.invcdf(y);
            if x_bounds(1) == -inf
                x_bounds(1) = param.dist.invcdf(0.01);
            end
            if x_bounds(2) == inf
                x_bounds(2) = param.dist.invcdf(0.99);
            end
            x = linspace(x_bounds(1), x_bounds(2), n);
        end
        function y=logpdf(param, x)
            % PDF Return the pdf of the parameter.
            if param.is_fixed
                abstol = 1e-10;
                reltol = 1e-10;
                x0 = param.fixed_val;
                y = log(double(abs(x-x0)<=abstol+abs(x0)*reltol));
            else
                y = log(param.dist.pdf(x));
            end
        end
    end
end