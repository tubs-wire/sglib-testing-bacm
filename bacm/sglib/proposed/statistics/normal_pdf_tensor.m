function y=normal_pdf_tensor( x, mu, sigma )
% NORMAL_PDF Probability distribution function of the normal distribution.
%   Y=NORMAL_PDF( X, MU, SIGMA ) computes the pdf for the normal dist. for
%   all values in X, which may be a vector. MU and SIGMA can be specified
%   optionally.
%
% Example 1 (<a href="matlab:run_example normal_pdf_tensor 1">run</a>)
%   x=linspace(-1,5);
%   f=normal_pdf_tensor(x,2,.5);
%   F=normal_cdf(x,2,.5);
%   plot(x,f,x(2:end)-diff(x(1:2)/2),diff(F)/(x(2)-x(1)))
%
% Example 2 (<a href="matlab:run_example normal_pdf_tensor 2">run</a>)
%   x=linspace(-1,5)';
%   mu=linspace(3,4);
%   [X, MU]=meshgrid(x, mu);
%   sigma=1;
%   f=normal_pdf_tensor(X,MU, sigma);
%   surf(x, mu, f)
%   xlabel('x'); ylabel('mu'); zlabel('f')
% 
% Example 3 (<a href="matlab:run_example normal_pdf_tensor 3">run</a>)
%   x=linspace(-1,5)';
%   mu=linspace(3,4);
%   sigma=zeros(1, 1, 20);
%   sigma(:,:,:)=linspace(0.2, 0.3, 20);
%   [X, MU]=meshgrid(x, mu);
%   f=normal_pdf_tensor(x,mu, sigma);
%
% See also NORMAL_CDF

%   Elmar Zander, modified by Noemi Friedman 2016 (to tensor format)
%   Copyright 2006, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


if nargin<2
    mu=0;
end
if nargin<3
    sigma=1;
end
    x_fluct=binfun(@minus, x, mu);
    y=exp(binfun(@rdivide, -x_fluct.^2, (2*sigma.^2) ));
    y=binfun(@rdivide, y, (sqrt(2*pi).*sigma));
    
    % give value 1 if sigma=0 (no uncertainty) and x is mu
    y(binfun(@and, binfun(@eq, sigma, 0),  binfun(@eq, x, mu)))=1;
    y(binfun(@and, binfun(@eq, sigma, 0),  binfun(@ne, x, mu)))=0;