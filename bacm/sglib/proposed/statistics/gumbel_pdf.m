function y=gumbel_pdf( x, mu, beta )
% BETA_PDF Probability distribution function of the beta distribution.
%   Y=BETA_PDF( X, A, B ) computes the pdf for the beta distribution for
%   all values in X, which may be a vector.
%
% Example (<a href="matlab:run_example gumbel_pdf">run</a>)
%   x=linspace(-0.2,1.2);
%   f=gumbel_pdf(x,2,3);
%   F=gumbel_cdf(x,2,3);
%   plot(x,f,x(2:end)-diff(x(1:2)/2),diff(F)/(x(2)-x(1)));
%
% See also gumbel_CDF


%   Noemi Friedman
%   Copyright 2016, Institute of Scientific Computing, TU Braunschweig.
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


if nargin<2
    mu=0;
end
if nargin<3
    beta=1;
end

z=(x-mu)/beta;
y=1/beta*exp( -(z+exp(-z)));
