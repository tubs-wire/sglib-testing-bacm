classdef  MySurrogateModel < SurrogateModel
    properties
        params2germ_func
    end
    
    methods
        function model = MySurrogateModel(coeffs, basis, params, orig_model, params2germ_func)
            model@SurrogateModel(coeffs, basis, params, orig_model);
            if nargin<5 || isempty(params2germ_func)
                params2germ_func=@(q)model.params.params2germ(q);
            end
            model.params2germ_func = params2germ_func;
        end
        function u=compute_response(model, q)
            %options=varargin2options(varargin);
            %[params2germ_func,options]=get_option( options, 'params2germ_func', {} );
            %check_unsupported_options(options,mfilename);
                  
            
            xi = funcall(model.params2germ_func, q);
            func = funcreate(@gpc_evaluate, @funarg, model.basis, xi);
            u = multivector_map(func, model.coeffs);
        end
    end
    
    %statistics, sensitivities ..etc
    methods
        function [u_mean, u_var, u_skew, u_kurt]=get_moments(model, varargin)
           
            [u_mean, u_var, u_skew, u_kurt]=gpc_moments...
               ( model.coeffs, model.basis, varargin );
        end
        
        function [partial_var, I_s, ratio_by_index, ratio_by_order]=get_sobol_partial_vars(model, varargin)
            [partial_var, I_s, ratio_by_index, ratio_by_order]=...
                gpc_sobol_partial_vars(model.coeffs, model.basis, varargin);
        end
        
        function P=get_response_covariance(model)
            P=gpc_covariance(model.coeffs, model.basis);
        end
        
        function plot_multi_response_surface(model, u_ind, varargin)
            V_u=model.basis;
            n_germ=gpcbasis_size(V_u,2);
            
            if n_germ == 2
                plot_response_surface(model.coeffs(u_ind,:), V_u);
            else
                plot_multi_response_surface(model.coeffs(u_ind,:), model.basis, varargin);
            end 
        end
    end
end
    