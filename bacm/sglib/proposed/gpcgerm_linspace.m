function xi = gpcgerm_linspace(V, n, varargin)
% GPCGERM_LINSPACE Sample from a probability distribution.
%   XI = GPCGERM_LINSPACE(V_U, N) samples from the
%   probability distribution corresponding to the system of orthogonal
%   polynomials defined by V_u{1}. An array of size M x N of independently generated
%   samples is returned, where M is the number of the GPC GERMS.
%
%
% Example (<a href="matlab:run_example gpcgerm_linspace">run</a>)
%    N = 100; 
%    V=gpcbasis_create('hpltu','p',4)
%    u_alpha = gpc_rand_coeffs(V, 1);
%    xi=gpcgerm_linspace(V, N, 1);
%
% See also GPCGERM_SAMPLE, RAND, RANDN

%   Noemi Friedman
%   Copyright 2015, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

syschars=V{1};
m=gpcbasis_size(V,2);
xi=zeros(m,n);
for j=1:m
    syschar=syschars(j);
    dist = polysys_dist(syschar);
    if iscell(dist)&&strcmpi(dist{1},'none')
        error('sglib:gpc', 'err_monomials, there is no distribution associated with Monomials')
    end
    if any(strcmpi(syschar, {'p','t','u'}))
        x_bounds=[0,1];
    elseif strcmpi(syschar, 'h')
        x_bounds=[0.01,0.99];
    elseif strcmpi(syschar, {'m', 'l'})
        error('This does not work with monomials or with exponential distrib')
    elseif strcmpi(syschar, {'h'})
        x_bounds=[0,0.99];
    else 
         x_bounds=[0,1];
    end   
    xi_minmax=gendist_invcdf(x_bounds,dist);
    xi(j,:)=linspace(xi_minmax(1), xi_minmax(2),n);
    
end
