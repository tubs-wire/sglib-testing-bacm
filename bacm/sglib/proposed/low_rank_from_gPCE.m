function[r_i_k, sigma_k, u_i_alpha_fluct]=...
    low_rank_from_gPCE(u_i_alpha, V_u, L, pos)
%% if POS (coordinates) are empty, then it makes POD, otherwise KLE
if nargin>3 && ~isempty(pos)
    [~, els] = create_mesh_1d(0, 1, length(pos));
    if length(pos)~=size(u_i_alpha,1)
        n_out=size(u_i_alpha,1)/length(pos);
    end
end
% Subtract mean
u_i_alpha_fluct=u_i_alpha;
u_i_alpha_fluct(:,1)=0;

% Compute covariance
C=gpc_covariance(u_i_alpha_fluct, V_u);

% The mass matrix
if nargin>3 && ~isempty(pos)
    if size(pos,2)==1;
        pos=pos';
    end
    G = mass_matrix(pos, els);
    G_N=repmat({G},n_out, 1);
    G_N =blkdiag(G_N{:});
else
    G_N=[];
end
%

[r_i_k,sigma_k]=kl_solve_evp(C, G_N, L);
