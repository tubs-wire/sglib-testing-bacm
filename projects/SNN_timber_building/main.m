%% Load params, freqs and modes, reorder
[Q, q_i, response_names, freqs_i, ux, uy] = get_params1_qmc_data();
[Q, q_i, response_names, freqs_ordered, u_ordered, ind_problem_5] = ...
    get_params1_qmc_reordered_data();
[n_node, n_mode, n] = size(u_ordered);

%% Plot original and reordered mode shapes
% plot pdf of original frequencies
h_distfreq = figure();
hold on
for i = 1:size(freqs_i, 1)
    [x, y, h] = my_plot_density(freqs_i(i,:));
    h.LineWidth = 2;
end
legend({'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mode 5', 'mode 6'});
xlabel('frequency')
ylabel('probability')
title('Distribution of tangled frequencies')
saveas(h_distfreq, 'figures/tangled_frequencies.jpg')
close(h_distfreq)


% plot original modes
h_orig = figure();
plot_modes(cat(  1,  reshape(ux, n_node/2, n_mode, n),...
    reshape(uy, n_node/2, n_mode, n)  ))
saveas(h_orig, 'figures/tangled_modes.jpg')
close(h_orig)

% plot reordered modes
figure()
plot_modes(u_ordered)

%% Further filter mode 5
[q_i_filtered, u_filtered, freqs_filtered, ind_filtered] = ...
    filter_out_outliers_from_mode_5(Q, q_i, u_ordered, freqs_i, ind_problem_5);

u_ordered(:, 5, ind_filtered) = NaN;
freqs_ordered(:, ind_filtered) = NaN;

%% Plot finally filtered modes and frequencies
h_ordered = figure();
plot_modes(u_ordered)
saveas(h_ordered, 'figures/detangled_modes.jpg')
close(h_ordered);

% plot reordered frequencies
h_distfreq = figure();
hold on
for i = 1:size(freqs_ordered, 1)
    indnan = isnan(freqs_ordered(i,:));
    [x, y, h] = my_plot_density(freqs_ordered(i,~indnan));
    h.LineWidth = 2;
end

legend({'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mode 5', 'mode 6'});
xlabel('frequency')
ylabel('probability')
title('Distribution of tangled frequencies')
saveas(h_distfreq, 'figures/tangled_frequencies.jpg')
close(h_distfreq)

%% UQ - cross validation for eigenmodes
% degrees for which proxi model should be checked
p = 1:8;
k= 10;
train_rate = 0.85;
% cross validation for modes 1:4
results_1_4  = ...
    gpc_train_and_cross_val_mode_shapes(Q, q_i, u_ordered(:,1:4,:),...
    k, p, train_rate);
% cross validation for mode 5
results_5  = ...
    gpc_train_and_cross_val_mode_shapes(Q, q_i_filtered, ...
    u_ordered(:, 5, ~ind_filtered), k, p, train_rate);

results_mode = combine_cv_results(results_1_4, results_5);

%% plot cross validation results
plot_cross_validation_results(results_mode, p,...
    {'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mode 5'}, 'mode shapes')
ylim([0, 1.5])
saveas(gcf, 'figures/conv_gpc_of_modes.jpg')
close(gcf)


plot_cross_validation_results(results_1_4, p,...
    {'mode 1', 'mode 2', 'mode 3', 'mode 4'}, 'mode shapes 1-4')
saveas(gcf, 'figures/conv_gpc_of_modes_1_4.jpg')
close(gcf)

plot_cross_validation_results(results_5, p, {'mode 5'}, 'mode shape 5')
ylim([0,1])
saveas(gcf, 'figures/conv_gpc_of_mode_5.jpg')
close(gcf)

%% UQ - cross validation for eigenfrequencies
% degrees for which proxi model should be checked
p = 1:8;
k= 10;
train_rate = 0.85;
% cross validation for modes 1:4
results_1_4  = ...
    gpc_train_and_cross_val_mode_shapes(Q, q_i, freqs_ordered(1:4,:),...
    k, p, train_rate);

% cross validation for mode 5
results_5  = ...
    gpc_train_and_cross_val_mode_shapes(Q, q_i_filtered, ...
    freqs_filtered, k, p, train_rate);

results_freqs = combine_cv_results(results_1_4, results_5);

%% plot cross validation results
plot_cross_validation_results(results_freqs, p,...
    {'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mode 5'}, 'frequencies')
ylim([0,0.1])
saveas(gcf, 'figures/conv_gpc_of_freqs.jpg')
close(gcf)


plot_cross_validation_results(results_1_4, p,...
    {'mode 1', 'mode 2', 'mode 3', 'mode 4'}, 'frequencies 1-4')
saveas(gcf, 'figures/conv_gpc_of_freqs_1_4.jpg')
close(gcf)

plot_cross_validation_results(results_5, p, {'mode 5'}, 'frequency 5')
ylim([0.15,0.3])
saveas(gcf, 'figures/conv_gpc_of_freq_5.jpg')
close(gcf)

%% UQ - final surrogate models
% gpc for eigenmodes
p_gpc = [6,6,6,6,4];
n_mode_to_include = 5;
model_modes = cell(1, n_mode_to_include);
for i = 1: n_mode_to_include
    if i==5
        model_modes{i} = GPCSurrogateModel.fromInterpolation(Q,...
            q_i(:, ~ind_filtered), squeeze(u_ordered(:, i, ~ind_filtered)), p_gpc(i));
    else
        model_modes{i} = GPCSurrogateModel.fromInterpolation(Q,...
            q_i, squeeze(u_ordered(:, i, :)), p_gpc(i));
    end
end
% gpc for eigenfrequencies
p_gpc = [4,4,4,4,3];
model_freqs = cell(1, n_mode_to_include);
for i=1:n_mode_to_include
    if i == 5
        model_freqs{i} = GPCSurrogateModel.fromInterpolation(Q,...
            q_i_filtered, freqs_filtered, p_gpc(i));
    else
        model_freqs{i} = GPCSurrogateModel.fromInterpolation(Q,...
            q_i, freqs_i(i,:), p_gpc(i));
    end
end

%% UQ - compute stats and sensitivities of modes
max_index = [3, 3, 3, 3, 3];
stats_mode = cell(1, n_mode_to_include);
stats_freqs = cell(1, n_mode_to_include);
for i = 1: n_mode_to_include
    stats_mode{i} = ...
        compute_stats_and_sensitivities(model_modes{i}, max_index(i));
    stats_freqs{i} = ...
        compute_stats_and_sensitivities(model_freqs{i}, max_index(i));
end

%% Plot statistics
% plot prior uncertainties of frequencies (pdfs)
figure()
plot_uncertainty_of_freqs(model_freqs)
saveas(gcf, 'figures/stats_of_freqs.jpg')
close(gcf)
% Plot uncertainties of the modes
figure()
plot_uncertainties_of_modes(model_modes)
saveas(gcf, 'figures/stats_of_modes.jpg')
close(gcf)

%% Plot sensitivities
% Plot sensitivities of modes
figure()
plot_all_mode_sensitivities(stats_mode, Q)
saveas(gcf, 'figures/sensitivity_of_modes.jpg')
close(gcf)
% Plot sensitivities of frequencies
figure()
plot_all_frequency_sensitivities(stats_freqs, Q)
saveas(gcf, 'figures/sensitivity_of_freqs.jpg')
close(gcf)
%% Measurement model
% Plot experimental modes
flag_filter = true;
% Mean and variance of the measurement model
[mu, sigma] = get_experimental_mean_and_std(flag_filter);
sigma = sigma + 0.005;
% sigma = sigma*5;


% Measured value of the frequency
freqs_mu = [2.85, 2.93, 3.13, 3.63, 6.73, 8.74]';
% Variance of the frequency
%var_freqs = [0.0137, 0.0006, 0.0047, 0.0027, 0.0356, 0.1182];
%std_freqs = sqrt(var_freqs);
freqs_sigma = freqs_mu*0.005;
% Plot sample distribution and measurement model

%% Plot prior uncertainties and measurement model
% plot mode samples and measurement model
figure()
plot_mode_samples_and_experiments(u_ordered, mu, sigma)
saveas(gcf, 'figures/mode_samples_and_experiment.jpg')
close(gcf)
% plot 95% confidence regions
figure()
plot_uncertainty_of_freqs(model_freqs, {freqs_mu, freqs_sigma})
saveas(gcf, 'figures/prior_and_experimental_freqs.jpg')
close(gcf)
figure()
plot_uncertainties_of_modes(model_modes, {mu, 2*sigma})
saveas(gcf, 'figures/prior_and_experimental_modes.jpg')
close(gcf)


%% Forward model and Error model
update_modes = 1:5;
%features_to_update = 'only frequencies';
%features_to_update = 'only modes';
features_to_update = 'frequencies and modes';
switch features_to_update
    case  'frequencies and modes'
        q_to_response = get_joined_surrogate_model(model_freqs(update_modes), model_modes(update_modes));
        E = generate_stdrn_simparamset([freqs_sigma((update_modes));...
            reshape(sigma(:,update_modes),[], 1)]);
        y_m = [freqs_mu(update_modes); reshape(mu(:,update_modes),[], 1)];
    case 'only frequencies'
        q_to_response = get_joined_surrogate_model(model_freqs(update_modes), []);
        E = generate_stdrn_simparamset(freqs_sigma(update_modes));
        y_m = freqs_mu(update_modes);
    case 'only modes'
        q_to_response = get_joined_surrogate_model([], model_modes(update_modes));
        E = generate_stdrn_simparamset(reshape(sigma(:,update_modes),[], 1));
        y_m =reshape(mu(:,update_modes),[], 1);
end

%% Bayesian update
% LogLikelihood
log_flag = true;
if log_flag
    q_to_likelihood = @(q)(E.logpdf(q_to_response(q)-y_m));
else
    q_to_likelihood = @(q)(E.pdf(q_to_response(q)-y_m));
end
% set proposal density
prior_vars = Q.var;
P = generate_stdrn_simparamset(sqrt(prior_vars/50));
% number of samples
N = 10000;
%[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, Q, N, P, Q.sample(round(N/50)), 'parallel', true, 'plot', true, 'plot_dim', [1,4, 5], 'T', 50, 'T_burn', 600);
figure()
[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, Q, N, P,[], ...
    'log_flag', log_flag, 'parallel', true, 'plot', true, ...
    'plot_dim', [1, 4, 5], 'T', 10, 'T_burn', 100);

q_MAP = sample_density_peak(q_post_i);

%% Plot priors and posteriors parameters
figure()
plot_grouped_scatter({Q.sample(10000), q_post_i, q_MAP}, 'Legends', {'prior', 'posterior', 'MAP'}, 'Labels', Q.param_names)
saveas(gcf, 'figures/prior_and_posterior_samples.jpg')
close(gcf)


% Plot prior and posterior uncertainty of frequencies
figure()
multiplot_init(2,3,  'title', 'Prior and posterior distributions')
m=Q.num_params;
params = Q.get_params();
for i=1:m
    %subplot(2, 3, i)
    multiplot
    set(gcf,'DefaultLineLineWidth',2)
    plot_density(params{i}.dist);
    hold on
    plot_density(q_post_i(i, :));
    % set x axis limits
    ab = params{i}.dist.b-params{i}.dist.a;
    xlim([params{i}.dist.a-ab*0.1,  params{i}.dist.b + ab*0.1])
    xlabel(params{i}.name)
    %ylabel('pdf')
    if i==m
        legend('prior', 'posterior')
    end
end
saveas(gcf, 'figures/prior_and_posterior_distributions.jpg')
close(gcf)
%% Plot prior and posterior uncertainties
figure()
plot_uncertainty_of_freqs(model_freqs, {freqs_mu, freqs_sigma}, q_post_i)
saveas(gcf, 'figures/prior_and_posterior_distributions_of_freqs.jpg')
close(gcf)
%title('Prior and posterior distributions of the frequencies')
figure()
plot_uncertainties_of_modes(model_modes, {mu, 2*sigma}, q_post_i)
% without the measurement model
plot_uncertainties_of_modes(model_modes, [], q_post_i)
