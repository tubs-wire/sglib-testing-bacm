function plot_mode_samples_and_experiments(u, u_mu, u_std)
colors_struct = set_my_favorite_colors;
[n_node, n_mode, ~] = size(u);
for i = 1:n_mode
    % plot ux
    subplot(n_mode,2,2*i-1)
    hold on
    ind_nan = all(isnan(squeeze(u(:,i,:))),1);
    plot(1:n_node/2, squeeze(u( 1:n_node/2, i, ~ind_nan)));
    h = plot_between(1:n_node/2, u_mu( 1:n_node/2, i)-2*u_std( 1:n_node/2,i),...
        u_mu( 1:n_node/2, i)+2*u_std( 1:n_node/2,i), colors_struct.grey);
    plot(1:n_node/2, squeeze(u_mu( 1:n_node/2, i)), 'k', 'LineWidth', 2);
    h.FaceAlpha = 0.6;
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ ux'))
    % plot uy
    subplot(n_mode,2,2*i)
    xlim([0,13])
    hold on
    h_samples = plot(1:n_node/2, squeeze(u( n_node/2+1:end, i,  ~ind_nan)));
    h = plot_between(1:n_node/2, u_mu( n_node/2+1:end, i)-2*u_std(n_node/2+1:end,i),...
        u_mu( n_node/2+1:end, i)+2*u_std(n_node/2+1:end,i), colors_struct.grey);
    h.FaceAlpha = 0.6;
    % plot experimental mean
    h_mean = plot(1:n_node/2, squeeze(u_mu( n_node/2+1:end, i)), 'k', 'LineWidth', 2);
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    if i==1
        legend([h_samples(1), h, h_mean], 'FEM samples', 'experiment 95% conf. region','experiment mean')
    end
    title(strvarexpand('Mode $i$ uy'))
    xlim([0,13])
end
end