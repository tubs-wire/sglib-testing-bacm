%% plot u
function plot_modes(u)
[n_node, n_mode, ~] = size(u);
for i = 1:n_mode
    [u_x, u_y] = part_to_x_y_directions(u, 1);
    % plot ux
    subplot(n_mode,2,2*i-1)
    hold on
    plot(1:n_node/2, squeeze(u_x(:, i, :)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ ux'))
    ylim([-0.5, 0.5]);
    % plot uy
    subplot(n_mode,2,2*i)
    xlim([1,13])
    hold on
    plot(1:n_node/2, squeeze(u_y(:, i, :)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ uy'))
    xlim([1,13])
    ylim([-0.5, 0.5]);
end
end