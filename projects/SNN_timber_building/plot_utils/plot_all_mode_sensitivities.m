function plot_all_mode_sensitivities(stats_mode, Q)

for i = 1: length(stats_mode)
    subplot(length(stats_mode),2,2*i-1)
    fig_title = strvarexpand('mode $i$');
    plot_sensitivities_of_mode(stats_mode{i}, Q, 'x', fig_title)
    subplot(length(stats_mode),2,2*i)
    plot_sensitivities_of_mode(stats_mode{i}, Q, 'y', fig_title)
end