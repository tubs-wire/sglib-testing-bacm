function plot_cross_validation_results(results, p, response_names, fig_title)

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);


for i = 1:size(errmin,1)
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
if size(errmin,1) >1
    h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
    legend(h, [response_names, {'mean'}])
else
    legend(h, response_names)
end
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title(['Cross validation of gPCE of ', fig_title])
