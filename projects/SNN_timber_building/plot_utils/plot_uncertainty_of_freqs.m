function plot_uncertainty_of_freqs(model_freqs, meas_dist, q_post)
%% Compute measurement model's mean and quantiles
if nargin > 1 && ~isempty(meas_dist)
    f_mean = meas_dist{1};
    f_std = meas_dist{2};
end

%% Compute MAP estimate
if nargin > 2 && ~isempty(q_post)
    q_MAP = sample_density_peak(q_post);
end

n_mode = length(model_freqs);
% Plot uncertainty of frequencies
%multiplot_init(1,5, 'title', 'Uncertainties of frequencies')
for i = 1: n_mode
    %multiplot
    subplot(1,n_mode, i)
    set(gcf,'DefaultLineLineWidth',2)
    hold on
    % sample from the gpc of the frequencies
    f_i = gpc_sample(model_freqs{i}.u_i_alpha, model_freqs{i}.V_u, 10000);
    % plot density of the prior
    plot_density(f_i)
    if nargin > 1 && ~isempty(meas_dist)
        dist = gendist_create('normal', {f_mean(i), f_std(i)});
        plot_density(dist, {'dq', 0.0005})
    end
    if nargin > 2 && ~isempty(q_post)
        f_post = model_freqs{i}.compute_response(q_post);
        f_MAP = model_freqs{i}.compute_response(q_MAP);
        plot([f_MAP,f_MAP], ylim()) 
        plot_density(f_post)
    end
    
    xlabel(strvarexpand('f$i$'))
    if i == n_mode
        if nargin == 1
            legend('prior')
        elseif nargin == 2
            legend('prior', 'measured')
        elseif nargin == 3 && isempty(meas_dist)
            legend('prior', 'MAP', 'posterior')
        else
            legend('prior', 'measured', 'MAP', 'posterior')
        end
    end
    
end