function plot_uncertainties_of_modes(model_modes, meas_dist, q_post)
n_mode = length(model_modes);
n_node = size(model_modes{1}.u_i_alpha, 1);
x = 1:n_node/2;

%multiplot_init(5,2, 'ordering', 'col')
p_quant = [0.025, 0.975];

%% Define colors
colors_struct = set_my_favorite_colors;

%% Compute measurement model's mean and quantiles
if nargin > 1 && ~isempty(meas_dist)
    [e_mean_x, e_mean_y] = part_to_x_y_directions(meas_dist{1},1);
    [e_std_x, e_std_y] = part_to_x_y_directions(meas_dist{2},1);
end

%% Compute MAP estimate
if nargin > 2 && ~isempty(q_post)
    q_MAP = sample_density_peak(q_post);
end
for i = 1: n_mode
    %% Compute mean and quantiles
    
    % compute quantiles
    u_quant = gpc_quantiles(model_modes{i}.V_u, model_modes{i}.u_i_alpha,...
        'quantiles', p_quant);
    [u_quant_x, u_quant_y] = part_to_x_y_directions(u_quant, 2);
    
    % compute mean
    u_mean = gpc_moments(model_modes{i}.u_i_alpha, model_modes{i}.V_u);
    [u_mean_x, u_mean_y] =  part_to_x_y_directions(u_mean, 1);
    
    % compute posterior quantiles
    if nargin > 2 && ~isempty(q_post)
        u_post = model_modes{i}.compute_response(q_post);
        u_quant_p = quantile(u_post', p_quant);
        [u_post_x, u_post_y] = part_to_x_y_directions(u_quant_p, 2);
        u_MAP = model_modes{i}.compute_response(q_MAP);
        [u_MAP_x, u_MAP_y] = part_to_x_y_directions(u_MAP, 1);
    end
    xlim([1, n_node/2])
    %% x direction
    subplot(5,2,2*i-1)
    hold on
    
    % plot prior confidence region
    h = plot_between(x, u_quant_x(1,:), u_quant_x(2,:),...
        colors_struct.bordeau, 'FaceAlpha', 0.6);
    % plot mean
    plot(x, u_mean_x, 'Color', colors_struct.bordeau, 'LineWidth', 2)
    % plot measurement model's mean and confidence region
    if nargin > 1 && ~isempty(meas_dist)
        % confidence region
        h = plot_between(x, e_mean_x(:,i)-2*e_std_x(:,i), ...
            e_mean_x(:,i)+2*e_std_x(:,i),...
            colors_struct.grey, 'FaceAlpha', 0.6);
        % mean
        plot(x, e_mean_x(:,i), 'k:', 'LineWidth', 2)
    end
    % plot posterior confidence region
    if nargin > 2 && ~isempty(q_post)
        % compute quantile of posterior
        
        h = plot_between(x, u_post_x(1,:), u_post_x(2,:),...
            colors_struct.navy_blue, 'FaceAlpha', 0.6);
        plot(x, u_MAP_x, '--', 'Color', colors_struct.navy_blue, 'LineWidth', 2)
    end
    %plot(x, squeeze(u_ordered(x, i,:)))
    xlabel(strvarexpand('u_x mode $i$'))
    xlim([1, n_node/2])
    ax = gca;
    set(ax, 'YTick', 0.) % unique([0., get(ax, 'YTick')]));
    ax.YGrid = 'on';
    
    
    %% y direction
    %multiplot
    subplot(5,2,2*i)
    hold on
    
    % plot prior confidence region
    plot_between(x, u_quant_y(1,:), u_quant_y(2,:),...
        colors_struct.bordeau, 'FaceAlpha', 0.6)
    % plot mean
    plot(x, u_mean_y, 'Color', colors_struct.bordeau, 'LineWidth', 2)
    if nargin > 1 && ~isempty(meas_dist)
        
        % confidence region
        h = plot_between(x, e_mean_y(:,i)-2*e_std_y(:,i), ...
            e_mean_y(:,i)+2*e_std_y(:,i),...
            colors_struct.grey, 'FaceAlpha', 0.6);
        % mean
        plot(x, e_mean_y(:,i), 'k:', 'LineWidth', 2)
    end
    % plot posterior confidence region
    if nargin > 2 && ~isempty(q_post)
        % compute quantile of posterior
        
        h = plot_between(x, u_post_y(1,:), u_post_y(2,:),...
            colors_struct.navy_blue, 'FaceAlpha', 0.6);
        plot(x, u_MAP_y, '--', 'Color', colors_struct.navy_blue, 'LineWidth', 2)
    end
    %plot(x, squeeze(u_ordered(n_node/2+1:end, i,:)))
    xlabel(strvarexpand('u_y mode $i$'))
    if i == 1
        if nargin == 1
            legend('95% prior conf. region', 'mean')
        elseif nargin == 2
            legend('95% prior conf. region', 'prior mean', 'meas 95% conf. region', 'meas mean')
        elseif nargin == 3 && isempty(meas_dist)
            legend('95% prior conf. region', 'prior mean',  'post 95% conf. region', 'MAP')
        else
            legend('95% prior conf. region', 'prior mean', 'meas 95% conf. region', 'meas mean', 'post 95% conf. region', 'MAP')
        end
    end
    xlim([1, n_node/2])
    ax = gca;
    set(ax, 'YTick', 0.) % unique([0., get(ax, 'YTick')]));
    ax.YGrid = 'on';
end