function plot_sensitivities_of_mode(stats, Q, dir, fig_title)
%% Define colors
colors_struct = set_my_favorite_colors;
color_array = [];
cols = fieldnames(colors_struct);
for i =1:length(cols)
    color_array = cat(1, color_array, colors_struct.(cols{i}));
end
%% Prep
n_d = size(stats.u_mean,1);
x = 1:n_d/2;
switch dir
    case 'x'
        ind_x = 1:n_d/2;
    case 'y'
        ind_x = n_d/2 + 1:n_d;
end
% from index to paramset-name
names = get_sobol_sensitivity_index_names(stats.index, Q.param_names);
% chose only variables with significant partial variances
ind = max(stats.partial_var(ind_x,:))>max(stats.u_var)*5*10^-2;

%% Plot partial variances
hold on
h1 = plot(x, stats.u_var(ind_x), 'k', 'LineWidth', 2);
h2 = area(x, stats.partial_var(ind_x,ind));
for i =1:length(h2)
    h2(i).FaceColor = colors_struct.(cols{i});
end
% plot y = 0 line
plot(xlim(), [0,0], 'k', 'LineWidth', 2)
xlim([1, n_d/2])
xlabel(strvarexpand('u_$dir$ $fig_title$'))
ylabel('var./partial var.')

param_imp= names(ind);
legend([h1, h2], 'Total variance', param_imp{:}, 'Orientation', 'horizontal')
%title(fig_title)