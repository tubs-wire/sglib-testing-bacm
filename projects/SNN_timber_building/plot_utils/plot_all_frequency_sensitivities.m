function plot_all_frequency_sensitivities(stats_freqs, Q)

for i = 1: length(stats_freqs)
    subplot(length(stats_freqs),1,i)
    fig_title = strvarexpand('mode $i$');
    plot_sensitivities_of_freqs(stats_freqs{i}, Q, fig_title)
end