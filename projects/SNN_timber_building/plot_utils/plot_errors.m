function plot_errors(u_v, u_err, s)
[n_d, n_var, n] = size(u_v);
u_mean = mean(u_v, 3);
for i = 1:n_var
    subplot(n_var, 1, i)
    hold on
    h1 = plot(1:n_d, u_mean(:,i), 'LineWidth', 2, 'DisplayName', 'mean');
    h2 = plot(1:n_d, squeeze(u_err(:, i, :))*s);
    if s ~=1
        h2(1).DisplayName = strvarexpand('error * $s$');
    else
        h2(1).DisplayName = 'error';
    end
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    legend([h1, h2(1)])
end
end
