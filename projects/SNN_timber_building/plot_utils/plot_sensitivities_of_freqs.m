function plot_sensitivities_of_freqs(stats, Q, fig_title)
%% Define colors
colors_struct = set_my_favorite_colors;
color_array = [];
cols = fieldnames(colors_struct);
for i =1:length(cols)
    color_array = cat(1, color_array, colors_struct.(cols{i}));
end

%% Plot sensitivity
% from index to paramset-name
names = get_sobol_sensitivity_index_names(stats.index, Q.param_names);
% chose only variables with significant partial variances
ind = stats.partial_var>stats.u_var*10^-2;
pie(stats.ratio_by_index(:,ind), names(ind))
title(fig_title)