function startup(varargin)
% STARTUP Startup file for Vitam, setting the paths and stuff
%   STARTUP(VARARGIN) Long description of startup.
%
% Example (<a href="matlab:run_example startup">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

run( fullfile( '..', '..', 'sglib-testing', 'startup' ) );

basepath=fileparts( mfilename('fullpath') );

addpath( fullfile( basepath, 'io') )
addpath(genpath( fullfile( basepath, 'data') ))
addpath( fullfile( basepath, 'util') )
addpath( fullfile( basepath, '0_filter_and_reorder_freqs_and_modes') )
addpath( fullfile( basepath, '1_UQ') )
addpath( fullfile( basepath, '2_Bayesian_inversion') )
addpath( fullfile( basepath, 'plot_utils') )


% add some more functions from noemi's stuff
bacmpath=fullfile(fileparts(fileparts(basepath)), 'bacm');
addpath(bacmpath);
addpath(fullfile(bacmpath, 'sglib', 'proposed'));
addpath(fullfile(bacmpath, 'sglib', 'proposed', 'statistics'));
addpath(fullfile(bacmpath, 'mymethods'));
addpath(fullfile(bacmpath, 'mymethods', 'update'));
addpath(fullfile(bacmpath, 'myutils'));
addpath(fullfile(bacmpath, 'myutils', 'eliduenisch-latexTable-cb53f04'));
addpath(fullfile(bacmpath, 'myobjects'));