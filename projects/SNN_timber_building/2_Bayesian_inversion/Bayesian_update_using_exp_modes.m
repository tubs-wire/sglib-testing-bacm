function Bayesian_update_using_exp_modes()
[f, u, Q, q_i, response_names] = reading_in_and_reordering_freqs_and_modes();
ind_to_keep = filter_out_outliers_from_mode_5();
[n_node, n_mode, n] = size(u);

%% Cross validation to find optimal gpc degree
% p = 1:9;
% for freqencies mode 1-4
% get_cross_validation_results(Q, q_i, f(1:4,:), p, response_names.freqs(1:4))
% for frequency 5
% get_cross_validation_results(Q, q_i(:,ind_to_keep), f(5,ind_to_keep), p, response_names.freqs{5})

%% The forward problem: surrogate modeling
% gpc for eigenmodes
p_gpc = [5,5,5,5,7];
n_mode_to_include = 3;
model_modes = cell(1, n_mode_to_include);
for i = 1: n_mode_to_include
    model_modes{i} = GPCSurrogateModel.fromInterpolation(Q,...
    q_i, squeeze(u(:,i, :)), p_gpc(i));
end
% gpc for eigenfrequencies
p_gpc = [5,5,5,5,3];
model_freqs = cell(1, n_mode_to_include);
for i=1:n_mode_to_include
    model_freqs{i} = GPCSurrogateModel.fromInterpolation(Q,...
    q_i, f(i,:), p_gpc(i));
end


% Joined surrogate of forward model
%q_to_response = @(q)(get_forward_surrogate(model_freqs, model_modes, q));
%q_to_response = @(q)(get_freq_surrogate(model_freqs, q));
q_to_response = @(q)(get_mode_surrogate(model_modes, q));

%% Measurement model
% Plot experimental modes
flag_filter = false;
[mu, sigma] = get_experimental_mean_and_std(flag_filter);

%% Measured value of the frequency
freq_m = [2.85, 2.93, 3.13, 3.63, 6.73, 8.74]';
var_freqs = [0.0137, 0.0006, 0.0047, 0.0027, 0.0356, 0.1182];

%E = generate_stdrn_simparamset(sqrt(var_freqs));



%% Error model
%E = generate_stdrn_simparamset([freq_m(1:n_mode_to_include)*0.005;...
%    reshape(sigma(:,1:n_mode_to_include),[], 1)]);
%y_m = [freq_m(1:n_mode_to_include); reshape(mu(:,1:n_mode_to_include),[], 1)];
% E = generate_stdrn_simparamset(freq_m(1:n_mode_to_include)*0.05);
% y_m = freq_m(1:n_mode_to_include);
E = generate_stdrn_simparamset(reshape(sigma(:,1:n_mode_to_include),[], 1));
y_m =reshape(mu(:,1:n_mode_to_include),[], 1);
%% Likelihood
q_to_likelihood = @(q)(E.pdf(q_to_response(q)-y_m));


%%
% set proposal density
prior_vars = Q.var;
P = generate_stdrn_simparamset(sqrt(prior_vars/30));
% number of samples
N = 100000;
%[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, Q, N, P, Q.sample(round(N/50)), 'parallel', true, 'plot', true, 'plot_dim', [1,4, 5], 'T', 50, 'T_burn', 600);
[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, Q, N, P,[], ...
    'log_flag', true, 'parallel', true, 'plot', true, ...
    'plot_dim', [1,4, 5], 'T', 50, 'T_burn', 1000);

q_MAP = sample_density_peak(q_post_i);
plot_grouped_scatter({Q.sample(10000), q_post_i, q_MAP}, 'Legends', {'prior', 'posterior', 'MAP'}, 'Labels', Q.param_names)

h = get(groot,'CurrentFigure');
save_figure(h, 'MCMC_results_grouped_scatter_plot.esp')
save_figure(h, 'MCMC_results_grouped_scatter_plot.png')

end
function plot_sim_results_and_error_model(u, mu, sigma)
[n_node, n_mode, ~] = size(u);
for i = 1:n_mode
    %% plot ux
    subplot(n_mode,2,2*i-1)
    hold on
    % plot modes
    if i == 5
        plot(1:n_node/2, squeeze(u( 1:n_node/2, i, ind_to_keep)));
    else
        plot(1:n_node/2, squeeze(u( 1:n_node/2, i, :)));
    end
    % Plot mean of experimental result
    plot(1:n_node/2, mu(1:n_node/2, i), 'r', 'LineWidth', 2.5);
    % Plot 95 percent confidence region
    plot_between(1:n_node/2, mu(1:n_node/2,i)-2*sigma(1:n_node/2,i),...
        mu(1:n_node/2,i)+2*sigma(1:n_node/2,i), [0.8, 0.8, 0.8], ...
        'FaceAlpha', 0.5);
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ ux'))
    
    %% plot uy
    subplot(n_mode,2,2*i)
    xlim([0,13])
    hold on
    % plot modes
    if i == 5
        h_uy = plot(1:n_node/2, squeeze(u( n_node/2+1:end, i, ind_to_keep)));
    else
        h_uy = plot(1:n_node/2, squeeze(u( n_node/2+1:end, i, :)));
    end
    % plot mean of the experimental result
    h_exp_y =  plot(1:n_node/2, mu(n_node/2+1:end,i), 'r', 'LineWidth', 2.5);
    % Plot 95 percent confidence region
    h_sigma_y = plot_between(1:n_node/2, mu(n_node/2+1:end,i)-2*sigma(n_node/2+1:end,i),...
        mu(n_node/2+1:end,i)+2*sigma(n_node/2+1:end,i), [0.8, 0.8, 0.8], ...
        'FaceAlpha', 0.5);
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ uy'))
    if i==1
        legend([h_uy(1), h_exp_y, h_sigma_y],...
            'modes from simulation', 'mean of exp. modes',...
            '95% conf. region of the exp. modes')
    end
    xlim([0,13])
end

end

function y = get_forward_surrogate(model_freqs, model_modes, q)
n_modes = length(model_freqs);
y_freqs = get_freq_surrogate(model_freqs, q);
y_modes = get_mode_surrogate(model_modes, q);
y = [y_freqs; y_modes];
end

function y = get_freq_surrogate(model_freqs, q)
n_modes = length(model_freqs);
y = [];
for i = 1:n_modes
    y = [y; model_freqs{i}.compute_response(q)];
end
end

function y = get_mode_surrogate(model_modes, q)
n_modes = length(model_modes);
y = [];
for i = 1:n_modes
    y = [y; model_modes{i}.compute_response(q)];
end
end

function plot_plot_sim_results_and_error_model_freq(f, sigmas)
n_modes = size(f,1);
for i = 1:n_modes
    subplot(n_modes,1,i)
    h = histogram(f(i, :), 'Normalization', 'pdf');
    h.BinWidth = 0.01;
    hold on
    dist_i = NormalDistribution(freq_m(i), sigmas);
    y = dist_i.pdf(h.BinEdges);
    plot(h.BinEdges,0.3*y, 'LineWidth', 2)
    xlabel(strvarexpand('frequency of mode $i$'))
    ylabel('scaled pdf')
    if i == 1
        legend('simulation', 'measurement')
    end
end
end