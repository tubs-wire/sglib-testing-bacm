function UQ_freqs_and_mode_shapes()

% Read in data and paramset
[Q, q_i, response_names, freqs_i, ux, uy] = read_in_params1_qmc_output_data();


%% normalize
u = reshape_and_join_u(ux, uy);
u = normalize_and_flip(u, false);
[n_node, n_mode, n] = size(u);

%% Print original mode ordering
% plot_u(u)

%% seperate first the first three modes
[idx, u_mean] = cluster_eigenmodes(u(:,1:3,:), freqs_i(1:3,:), true, false, []);
[u_ordered, f_ordered] = reorder_u_and_f(u(:,1:3,:), freqs_i(1:3,:), idx, u_mean);

%% Find flipped modes in 4th mode
[idx, u_mean] = cluster_eigenmodes(u(:,4,:), [], true, false, []);
u_ordered_4 =reorder_u_and_f(u(:,4,:), freqs_i(4,:),idx, u_mean);
u_ordered = cat(2, u_ordered, u_ordered_4);
figure
plot_u(u_ordered)

%% Seperate modes 5-6
new_ord = repmat([1:2:13, 2:2:13], 1,2);

k = 4;
[idx, u_mean] = cluster_eigenmodes(u(new_ord,5:6,:), freqs_i(5:6,:), true, true, k);

u_modes = {};
clf

u_s = reshape(u(new_ord,5:6,:), [], 2*n);
ind_mode_exists = ~any(isnan(u_mean),2);
for i = 1:k
    if ind_mode_exists(i)
        u_i = u_s(:, idx == i);
        u_modes = [u_modes, u_i];
        subplot(k,1,i)
        hold on
        plot(1:n_node, u_i)
        plot(xlim(), [0,0], 'k', 'LineWidth', 2)
        % plot(1:n_node, C(i,:), 'r', 'LineWidth', 2)
        % plot(1:(n_node+1)*2-1, uf13_start(:,i), 'k', 'LineWidth', 2)
    end
end
figure
for i = 1:k/2
    if ind_mode_exists(i)
        u_i = cat(2, u_s(:, idx == i), -u_s(:, idx == i+k/2));
        subplot(k/2,1,i)
        hold on
        plot(1:n_node, u_i)
        plot(xlim(), [0,0], 'k', 'LineWidth', 2)
        % plot(1:n_node, C(i,:), 'r', 'LineWidth', 2)
        % plot(1:(n_node+1)*2-1, uf13_start(:,i), 'k', 'LineWidth', 2)
    end
end

% plot the derivatives
figure
for i = 1:k
    if ind_mode_exists(i)
        u_i = u_modes{i};
        subplot(k,1,i)
        hold on
        plot(1:n_node-1, diff(u_i))
        plot(xlim(), [0,0], 'k', 'LineWidth', 2)
        % plot(1:n_node, C(i,:), 'r', 'LineWidth', 2)
        % plot(1:(n_node+1)*2-1, uf13_start(:,i), 'k', 'LineWidth', 2)
    end
end




%% Read experimental modes
[uex, uey, response_names_e] = read_experimental_modes();

% check whether names are identical
if ~all(strcmp(response_names.mode_x, response_names_e.mode_x)) || ...
        ~all(strcmp(response_names.mode_y, response_names_e.mode_y))
    error('The structure of the experimental modes is different from the one of the samples')
end

%% normalize
u_e = reshape_and_join_u(uex, uey);
u_e = normalize_and_flip(u_e);

%% Print experimental curves
for i = 1:6
    subplot(6,1,i)
    hold on
    plot(1:26, squeeze(u_e(:,i,:)), 'LineWidth', 1.5)
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end



%% Cross validate different degree surrogate models for mode shape 1
% degrees for which proxi model should be checked
p = 1:7;
% cross validation
results  = ...
    gpc_train_and_cross_val_modal_proxi(Q, q_i, squeeze(ux_m(:,1,:)), 10, p, 0.85);

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);

for i = 1:6
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
legend(h, {response_names{1:6}, 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation results')

end
%% plot u
function plot_u(u)
[n_node, n_mode, ~] = size(u);
for i = 1:n_mode
    % plot ux
    subplot(n_mode,2,2*i-1)
    hold on
    plot(1:n_node/2, squeeze(u( 1:n_node/2, i, :)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ ux'))
    % plot uy
    subplot(n_mode,2,2*i)
    xlim([0,13])
    hold on
    plot(1:n_node/2, squeeze(u( n_node/2+1:end, i, :)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    title(strvarexpand('Mode $i$ uy'))
    xlim([0,13])
end
end

%% Clustering nodes
function [idx, u_mean] = cluster_eigenmodes(u, f, flag_derivatives, flag_add_frequencies, k)
[n_node, n_mode, n] = size(u);
% Initiate data
% add derivatives
if flag_derivatives
    u_r = cat(1, u, diff(u));
else
    u_r = u;
end
% chose start point for means from one response chosen randomly
j = randi([1, n],1);
u_start = u_r(:,:,j);
% add flipped modes to start point
u_start = cat(2, u_start, -u_start);
% add frequencies if needed
if flag_add_frequencies && ~isempty(f)
    u_r = cat(1, u_r, reshape(f, [1, n_mode, n]));
    u_start = cat(1, u_start, [f(:,j)',f(:,j)']);
end
% reshape u (put together all modes)
u_r = reshape(u_r, [], n_mode*n);
% Clustering
% number of clusters
if isempty(k)
    k = n_mode*2;
elseif k~=n_mode*2
    u_start = [];
end

[u_red, map_low_rank, map_full_rank] = low_rank(u_r, 6);
if isempty(u_start)
     [idx, u_mean, SUMD_r] = kmeans(u_red', k, 'Distance', 'sqeuclidean', ...
        'MaxIter', 100, 'EmptyAction', 'drop', ...
        'Display', 'final');
else
    [idx, u_mean, SUMD_r] = kmeans(u_red', k, 'Distance', 'sqeuclidean', ...
        'MaxIter', 100, 'EmptyAction', 'drop', ...
        'Display', 'final', 'Start', map_low_rank(u_start)');
end
% Possible distances are: 'sqeuclidean'/'cityblock'/ 'cosine'/'correlation'
u_mean = map_full_rank(u_mean');
u_mean_ = u_mean;
ind_mode_exists = ~any(isnan(u_mean),1);
u_mean_(:,~ind_mode_exists) = 0;
u_mean_unflipped = u_mean_(:,1:n_mode);
u_mean_flipped = u_mean_(:,n_mode+1:2*n_mode);
u_mean_ = (u_mean_unflipped - u_mean_flipped)/2; 
u_mean_(end,:) = (u_mean_unflipped(end,:) + u_mean_flipped(end,:))/2;

%u_r = reshape(u_r, [], n_mode, n);

dists = reshape(dist(u_mean_', u_r), [], n_mode, n);

   
end
%% Reorder modes after clustering to modes and flipped modes
function [u_ordered, f_ordered] = reorder_u_and_f(u, f, idx, u_mean)
ind_mode_exists = ~any(isnan(u_mean),1);
[n_node, n_mode, n] = size(u);
% Initiate solution tensor
u_ordered = zeros(n_node, n_mode, n);
f_ordered = zeros(size(f));
f = f(:);
for i = 1:n_mode
    if ind_mode_exists(i)
        u_i = u(:,idx==i);
        f_i = f(idx==i);
    else
        u_i = [];
    end
    if ind_mode_exists(i+n_mode)
        u_i = cat(2, u_i, -u(:,idx==i+n_mode));
        f_i = cat(1, f_i,  f(idx==i+n_mode));
    end
    u_ordered(:,i,:) = reshape(u_i, size(u_i,1),1,size(u_i,2));
    f_ordered(i,:) = f_i;
end
end

function u_normed = normalize_and_flip(u, flag_flip)
% normalize
norm_u = sqrt(sum(u.^2, 1));
u_normed = u./norm_u;
if flag_flip
    % fliping TODO: not 14, but somehow automatic
    u_normed = sign(u_normed(14,:,:)).*u_normed;
end
end

function [u_r, map_low_rank, map_full_rank] = low_rank(u, r)
C = covariance_sample(u);
R = 20;
[V, sigma_k] = kl_solve_evp(C, [], R);
figure()
plot(cumsum(sigma_k)/sum(sigma_k)*100, 'x-', 'LineWidth', 2)
[V, sigma_k] = kl_solve_evp(C, [], r);
u_r = V'* u;
map_low_rank = @(u)(V'*u);
map_full_rank = @(u_low)(V*u_low);
end



function u = reshape_and_join_u(ux, uy)
ux_m = reshape(ux, 13, 6, []);
uy_m = reshape(uy, 13, 6, []);
u = [ux_m; uy_m];
end