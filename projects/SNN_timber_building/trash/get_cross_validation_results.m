function results = get_cross_validation_results(Q, q_i, u_i, p, response_names)
n_resp = size(u_i,1);
results  = ...
    gpc_train_and_cross_val_modal_proxi(Q, q_i, u_i, 10, p, 0.85);

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);
if n_resp ==1
    col_i = h.Color;
    plot_between(p, errmin, errmax, col_i, 'FaceAlpha', 0.5);
    legend(h, response_names)
else
    for i = 1:n_resp
        col_i = h(i).Color;
        plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
    end
    h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
    legend(h, [response_names, {'mean'}])
end

xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation results')