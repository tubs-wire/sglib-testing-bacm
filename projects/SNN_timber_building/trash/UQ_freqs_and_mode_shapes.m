function UQ_freqs_and_mode_shapes()

% Read in data and paramset
[Q, q_i, response_names, freqs_i, ux, uy] = read_in_params1_qmc_output_data();


%% normalize
u = reshape_and_join_u(ux, uy);
u = normalize_and_flip(u, false);
n_node = size(u,1);

%% Print samples
for i = 1:6
    subplot(6,1,i)
    hold on
    plot(1:26, squeeze(u(:,i,:)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end

u13 = u(:,1:3, :);
u13 =reshape(u13, n_node, []);

u13_start = squeeze(u(:,1:3,10));
u13_start = cat(2, u13_start, -squeeze(u(:,1:3,10)));
[idx, C, SUMD] = kmeans(u13',6, 'Distance', 'sqeuclidean', 'MaxIter', 500,...
    'Display', 'final', 'Start', u13_start');
%'sqeuclidean'/'cityblock'/ 'cosine'/'correlation'

u_modes = {};
clf

for i =1:4
    u_i = u13(:, idx == i);
    u_modes = [u_modes, u_i];
   
    subplot(4,1,i)
    hold on
    plot(1:n_node, u_i)
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    plot(1:n_node, C(i,:), 'r', 'LineWidth', 2)
    plot(1:n_node, u13_start(:,i), 'k', 'LineWidth', 2)
end


%% Further iterate classification due to unsuccessfull results
u_wrong = u_modes{2};
[idx_wrong, C_wrong] = kmeans(u_wrong',2);
clf
for i =1:2
    u_i = u_wrong(:, idx_wrong == i);
    % u_modes = [u_modes, u_i];
    subplot(3,1,i)
    hold on
    plot(1:26, u_i)
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end

u_wrong_wrong = u_wrong(:,idx_wrong == 1);

[min_u_wrong_wrong,  ind_min] = min(u_wrong_wrong(17, :), [], 2);

% %% SVD and then classification
% u13_trick = [u13, -u13];
% C = covariance_sample(u13_trick);
% R = 20;
% [v_k_i,sigma_k] = kl_solve_evp( C, [], R);
% plot(cumsum(sigma_k)/sum(sigma_k)*100, 'x-', 'LineWidth', 2)
% V = v_k_i(:, 1:6);
% u_trick_SVD = V'* u13_trick;
% idx_trick = kmeans(u_trick_SVD',6);
% % project u13_trick onto V




clf
for i =1:6
    subplot(6,1,i)
    hold on
    plot(1:26, u13_trick(:, idx_trick == i))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end


idx_abs = kmeans(abs(u13)',3);

clf
for i =1:3
    subplot(3,1,i)
    hold on
    plot(1:26, abs(u13(:, idx_abs == i)))
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end

plot(u13(:, idx == 1));

%% Read experimental modes
[uex, uey, response_names_e] = read_experimental_modes();

% check whether names are identical
if ~all(strcmp(response_names.mode_x, response_names_e.mode_x)) || ...
        ~all(strcmp(response_names.mode_y, response_names_e.mode_y))
    error('The structure of the experimental modes is different from the one of the samples')
end

%% normalize
u_e = reshape_and_join_u(uex, uey);
u_e = normalize_and_flip(u_e);

%% Print experimental curves
for i = 1:6
    subplot(6,1,i)
    hold on
    plot(1:26, squeeze(u_e(:,i,:)), 'LineWidth', 1.5)
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end








%% Cross validate different degree surrogate models for mode shape 1
% degrees for which proxi model should be checked
p = 1:7;
% cross validation
results  = ...
    gpc_train_and_cross_val_modal_proxi(Q, q_i, squeeze(ux_m(:,1,:)), 10, p, 0.85);

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);

for i = 1:6
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
legend(h, {response_names{1:6}, 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation results')

end



function u_normed = normalize_and_flip(u, flag_flip)
% normalize
norm_u = sqrt(sum(u.^2, 1));
u_normed = u./norm_u;
if flag_flip
% fliping TODO: not 14, but somehow automatic
    u_normed = sign(u_normed(14,:,:)).*u_normed;
end
end

function u = reshape_and_join_u(ux, uy)
ux_m = reshape(ux, 13, 6, []);
uy_m = reshape(uy, 13, 6, []);
u = [ux_m; uy_m];
end