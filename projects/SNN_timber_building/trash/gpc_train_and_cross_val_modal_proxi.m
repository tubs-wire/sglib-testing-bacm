function results  = gpc_train_and_cross_val_modal_proxi(Q, q_j_l, u_i_l, K, p, n_s, varargin)
% This function cross validates the differen order gPCEs
% Example:
% TRAIN_AND_CROSS_VAL_GPCES...
% (simparamset, q_points, u, K-fold, [2,3,4,5], ratiotrain/val, U_REF, coord)
% inputs:
% Q: prior paramset
% q_j_l: simulation parameter values
% u_i_l: model solutions at q
% K: k-fold cross validation
% p: Order of gPCEs
% n_s: separate ratio (# training samples/# validating samples)
%
%
%
%
%   Noemi Friedman
%   Copyright 2019, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
check_unsupported_options(options, mfilename);


%% Initiate cross validation
% Fix seed number
%rand_seed(seed_numb);

% Number of training points
n_t=ceil(size(q_j_l,2)*n_s);

n_var = size(u_i_l, 1);

% Initialize memory
% avarage error of different gpc for all cross-val
err_i_p_k = zeros(n_var,length(p),K);
maxerr_i_p_k= zeros(n_var,length(p),K);

%% Cross validate

% k-validation
for k=1:K
    % Get index of training and validating set and the training points (q_t)
    dim_q=2;
    % [q_t, ind_t, ind_v] = ...
    %    shuffle_select_sort(q, dim_q, n_t, 'fixed_randseed', seed_numb);
    [q_j_t, ind_t, ind_v] = ...
        shuffle_select_sort(q_j_l, dim_q, n_t);
    % Training responses
    u_i_t=u_i_l(:,ind_t);
    % Validation points and responses
    q_j_v = q_j_l(:,ind_v);
    u_i_v = u_i_l(:,ind_v);
    for ip=1:length(p)
        display(strvarexpand('Cross val $k$-fold, $p(ip)$ degree'))
        model = GPCSurrogateModel.fromInterpolation(Q, q_j_t, u_i_t, p(ip), gPCE_options{:});
        us_i_v = model.compute_response(q_j_v);
        err_i_p_k( :, ip, k) = ...
            sqrt(diag((us_i_v - u_i_v)*(us_i_v - u_i_v)')./diag(u_i_v*u_i_v'));
        maxerr_i_p_k( :, ip, k) = ...
            max(abs(us_i_v - u_i_v),[], 2)./max(abs(u_i_v),[], 2);
    end
    %seed_numb=seed_numb+10;
end


results.mean_err_L2_i_p = mean(err_i_p_k, 3);
results.var_err_L2_i_p = var(err_i_p_k,[], 3);
results.max_err_L2_i_p = max(err_i_p_k, [], 3);

results.mean_err_Linf_i_p = mean(maxerr_i_p_k, 3);
results.var_err_Linf_i_p = var(maxerr_i_p_k,[], 3);
results.max_err_Linf_i_p = max(maxerr_i_p_k, [], 3);
end




