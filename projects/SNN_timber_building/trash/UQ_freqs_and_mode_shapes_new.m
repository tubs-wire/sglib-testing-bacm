
[f, u, Q, q_i, response_names] = reading_in_and_reordering_freqs_and_modes();

%% Cross validate different degree surrogate models for mode shape 1
% degrees for which proxi model should be checked
p = 1:7;
% cross validation
results  = ...
    gpc_train_and_cross_val_mode_shapes(Q, q_i, u, 10, p, 0.85);

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);

for i = 1:6
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
legend(h, {'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mode 5', 'mode 6', 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation of gPCE of mode shapes')

%% Plot only first four modes
figure()
h=plot(p, results.mean_err_L2_i_p(1:4,:), 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p(1:4,:)-sqrt(results.var_err_L2_i_p(1:4,:));
errmax = results.mean_err_L2_i_p(1:4,:)+sqrt(results.var_err_L2_i_p(1:4,:));

for i = 1:4
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p(1:4,:)), 'k-x' ,'LineWidth', 3)];
legend(h, {'mode 1', 'mode 2', 'mode 3', 'mode 4', 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation of gPCE of mode shapes')

