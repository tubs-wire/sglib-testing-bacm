function [u_x, u_y] = part_to_x_y_directions(u, dim)
n_d = size(u,dim);
d = length(size(u));

ind_x = 1:n_d/2;
ind_y = n_d/2 + 1:n_d;
if d == 1
    if dim == 1
        u_x = u(ind_x);
        u_y = u(ind_y);
    else
        error('dimension of u is smaller than the given dimension')    
    end
elseif d==2
    if dim == 1
        u_x = u(ind_x,:);
        u_y = u(ind_y,:);
    elseif dim == 2
        u_x = u(:, ind_x);
        u_y = u(:, ind_y);
    else
        error('dimension of u is smaller than the given dimension')  
    end
elseif d==3
    if dim == 1
        u_x = (u(ind_x,:,:));
        u_y = (u(ind_y,:,:));
    elseif dim == 2
        u_x = (u(:, ind_x,:));
        u_y = (u(:, ind_y,:));
    elseif dim ==3
        u_x = (u(:, :, ind_x));
        u_y = (u(:, :, ind_y));
    else
        error('dimension of u is smaller than the given dimension')  
    end
else
    error ('function is only available for max 3 dimensional array')
end
