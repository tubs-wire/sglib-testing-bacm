%% Load input parameter properties (simparamset)

[Q, q_i, response_names, freqs_i, MACs_i] = read_in_input_output_data();

%% Measured value of the frequency
freq_m = [2.85, 2.93, 3.13, 3.63, 6.73, 8.74]';

%% Generate proxi model (based on cross-validation results)
model_freq = GPCSurrogateModel.fromInterpolation(Q, q_i, freqs_i, 4);

%% Error model

var_freqs = [0.0137, 0.0006, 0.0047, 0.0027, 0.0356, 0.1182];

%E = generate_stdrn_simparamset(sqrt(var_freqs));
E = generate_stdrn_simparamset(freq_m*0.005);

%% Likelihood 
q_to_freqs = @(q)(model_freq.compute_response(q));
q_to_likelihood = @(q)(E.pdf(q_to_freqs(q)-freq_m));


%%
% set proposal density
prior_vars = Q.var;
P = generate_stdrn_simparamset(sqrt(prior_vars/30));
% number of samples
N = 100000;
%[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, Q, N, P, Q.sample(round(N/50)), 'parallel', true, 'plot', true, 'plot_dim', [1,4, 5], 'T', 50, 'T_burn', 600);
[q_post_i, acc_rate] = bayes_mcmc(q_to_likelihood, ...
    Q, N, P,[], 'parallel', true, 'plot', true, 'plot_dim', [1,4, 5], 'T', 50, 'T_burn', 600);

q_MAP = sample_density_peak(q_post_i);
plot_grouped_scatter({Q.sample(10000), q_post_i, q_MAP}, 'Legends', {'prior', 'posterior', 'MAP'}, 'Labels', Q.param_names)

h = get(groot,'CurrentFigure');
save_figure(h, 'MCMC_results_grouped_scatter_plot.esp')
save_figure(h, 'MCMC_results_grouped_scatter_plot.png')
