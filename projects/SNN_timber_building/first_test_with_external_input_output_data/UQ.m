[Q, q_i, response_names, freqs_i, MACs_i] = read_in_input_output_data();

%% Cross validate different degree surrogate models for frequency
% degrees for which proxi model should be checked
p = 1:7;
% cross validation
results  = ...
    gpc_train_and_cross_val_modal_proxi(Q, q_i, freqs_i, 10, p, 0.85);

figure()
h=plot(p, results.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);

for i = 1:6
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
legend(h, {response_names{1:6}, 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation results')

%% Make final surrogate model for frequency

model_freq = GPCSurrogateModel.fromInterpolation(Q, q_i, freqs_i, 4);


%% Statistics and sensitivity analysis of frequencies
[freq_mean, freq_var] = gpc_moments(model_freq.u_i_alpha, model_freq.V_u);
display([freq_mean, sqrt(freq_var)])

% compute Sobol partial variances and Sobol indices
[partial_vars, I_s, ratio_by_index, ratio_by_order ] = ...
    gpc_sobol_partial_vars(model_freq.u_i_alpha, model_freq.V_u, 'max_index', 2);
% Plot sensitivities
index_names = get_sobol_sensitivity_index_names(I_s, Q.param_names);
bar3(ratio_by_index)
set(gca,'XTick',1:size(I_s,1))
set(gca,'XTickLabel',index_names)
set(gca,'YTick',1:size(I_s,2))
set(gca,'YTickLabel',{response_names{1:6}})
zlabel('Sobol sensitivity indices')
title('Sensitivities of frequencies to variations of different input parameters')

%% Cross validate different degree surrogate models for MAC values
% degrees for which proxi model should be checked
p = 1:7;
% cross validation
results_MAC  = ...
    gpc_train_and_cross_val_modal_proxi(Q, q_i, MACs_i, 10, p, 0.85);

figure()
h=plot(p, results_MAC.mean_err_L2_i_p, 'LineWidth', 1.5);
hold on
errmin = results_MAC.mean_err_L2_i_p-sqrt(results.var_err_L2_i_p);
errmax = results_MAC.mean_err_L2_i_p+sqrt(results.var_err_L2_i_p);

for i = 1:6
    col_i = h(i).Color;
    plot_between(p, errmin(i,:),errmax(i,:), col_i, 'FaceAlpha', 0.5);
end
h = [h; plot(p, mean(results.mean_err_L2_i_p), 'k-x' ,'LineWidth', 3)];
legend(h, {response_names{7:end}, 'mean'})
xlabel('p: degree of gpc approximation')
ylabel('mean relative error')
title('Cross validation results')

%% Make final surrogate model for MAC values
model_MAC = GPCSurrogateModel.fromInterpolation(Q, q_i, MACs_i, 6);


%% Statistics and sensitivity analysis of MAC values
[MAC_mean, MAC_var] = gpc_moments(model_MAC.u_i_alpha, model_freq.V_u);
display([MAC_mean, sqrt(MAC_var)])

% compute Sobol partial variances and Sobol indices
[partial_vars, I_s, ratio_by_index, ratio_by_order ] = ...
    gpc_sobol_partial_vars(model_MAC.u_i_alpha, model_MAC.V_u, 'max_index', 2);
% Plot sensitivities
index_names = get_sobol_sensitivity_index_names(I_s, Q.param_names);
bar3(ratio_by_index)
set(gca,'XTick',1:size(I_s,1))
set(gca,'XTickLabel',index_names)
set(gca,'YTick',1:size(I_s,2))
set(gca,'YTickLabel',{response_names{7:end}})
zlabel('Sobol sensitivity indices')
title('Sensitivities of MAC values to variations of different input parameters')
% Plot the histogram of the MAC errors


plot_multi_response_surface(u_i_alpha(ind_freq,:), V_u, 'germ2param',...
    @(q)Q.germ2params(q), 'plot_fix_response', freq_m)

plot_multi_response_surface(u_i_alpha(ind_freq,:), V_u)
