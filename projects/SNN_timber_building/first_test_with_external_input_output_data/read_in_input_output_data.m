function [Q, q_i, response_names, freqs_i, MACs_i] = read_in_input_output_data()

%% Read in external input-output data
% Read in data as table
data_filename = '../data/input-output_external/DataPoints.xlsx';
opts = detectImportOptions(data_filename);
opts.VariableNamesRange = 'A1';
T = readtable(data_filename, opts);
% number of sample points
n = size(T,1);

%% Load input parameter properties (simparamset)

Q = generate_prior_paramset();

%% Separate input and output data

% Read from the table the input samples
n_q = Q.num_params;
q_i = zeros(n_q, n);

for i = 1: n_q
    q_i(i,:) = T.(Q.param_names{i});
end

% Read from the table the response samples
response_names = setdiff(T.Properties.VariableNames, Q.param_names);
response_names = setdiff(response_names, {'Name', 'Frequencyerror', 'MACerror'});

m = size(response_names, 2); 

u_i = zeros(m, n);
for i = 1: m
    u_i(i, :) = T.(response_names{i});
    display(response_names{i})
end

freqs_i = u_i(1:6, :);
MACs_i = u_i(7:12, :);