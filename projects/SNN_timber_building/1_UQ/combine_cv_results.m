function results = combine_cv_results(results_1, results_2)


results = results_1;
fn = fieldnames(results);
for i = 1: length(fn)
    fn_i = fn{i};
    results.(fn_i) = [results.(fn_i); results_2.(fn_i)];
end