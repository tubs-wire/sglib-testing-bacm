function results  = gpc_train_and_cross_val_mode_shapes(Q, q, u, K, p, n_s, u_ref, x, varargin)
% This function cross validates the differen order gPCEs
% Example:
% TRAIN_AND_CROSS_VAL_GPCES...
% (simparamset, q_points, u, K-fold, [2,3,4,5], ratiotrain/val, U_REF, coord)
% inputs:
% Q: prior paramset
% q: simulation parameter values
% u: model solutions at q (n_d x n_var xN)
% K: k-fold cross validation
% p: Order of gPCEs
% n_s: separate ratio (# training samples/# validating samples)
%
%
% optional inputs
% - WEIGHT_POW=0/1/2 (=0 without weighting according to distance from DNS
% result, =1 weighting with distance =2 wighting with squared distance
%
%
%   Noemi Friedman
%   Copyright 2016, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin);
[file_identifyer, options]=get_option(options, 'file_identifyer', '');
[save_results, options]=get_option(options, 'save_results', false);
[base_path, options]=get_option(options, 'base_path', 'default');
[gPCE_options, options]=get_option(options, 'gPCE_options', {});
[weighted_error, options]=get_option(options, 'weighted_error', false);
% [weight_pow, options]=get_option(options, 'weight_pow', 1);
% [flag_PCE, options]=get_option(options, 'flag_PCE',false);
[ind_ign, options]=get_option(options, 'ignored_indices_for_weighting', []);
check_unsupported_options(options, mfilename);


%% choose training and validating set

% Fix seed number
%seed_numb=644;

% Number of training points
n_t=ceil(size(q,2)*n_s);
n_v=size(q,2)-n_t;


% number of degrees to compute the gpc for
n_p = length(p);

% If u is not 3 dimensional
if length(size(u))==2
   u= reshape(u, [1,size(u,1), size(u,2)]);
elseif length(size(u))==1
   u= reshape(u, [1, 1, length(u)]);
end

% output types
[n_d, n_var, n] = size(u);

%% Cross validate
% Initialize memory

rel_errs_i_p_k = zeros(n_var, n_p, K*n_v);
maxerr_i_p_k= zeros(n_var,n_p, K*n_v);

% k-validation
for k=1:K
    % Get index of training and validating set and the training points (q_t)
    dim_q=2;
    % [q_t, ind_t, ind_v] = ...
    %    shuffle_select_sort(q, dim_q, n_t, 'fixed_randseed', seed_numb);
    [q_t, ind_t, ind_v] = ...
        shuffle_select_sort(q, dim_q, n_t);
    % Training responses
    u_t=u(:,:,ind_t);
    % Validation points and responses
    q_v = q(:,ind_v);
    u_v = u(:,:,ind_v);
    for ip=1:n_p
        display(strvarexpand('Cross val $k$-fold, $p(ip)$ degree'))
        model = GPCSurrogateModel.fromInterpolation(Q, q_t,...
            reshape(u_t, [], n_t), p(ip), gPCE_options{:});
        
        u_gpc_v = reshape(model.compute_response(q_v), n_d, n_var, []);
        error_p_k = u_v-u_gpc_v;
        % compute the norms
        err_L2 = vecnorm(error_p_k, 2, 1)./vecnorm(u_v, 2, 1);
        rel_errs_i_p_k( :, ip, (k-1)*n_v+1:k*n_v) = err_L2;
        err_L_inf = vecnorm(error_p_k, 1, 1)./vecnorm(u_v, 1, 1);
        maxerr_i_p_k( :, ip, (k-1)*n_v+1:k*n_v) = err_L_inf;
    end
    %seed_numb=seed_numb+10;
end
%% Structure result

results.mean_err_L2_i_p = mean(rel_errs_i_p_k, 3);
results.var_err_L2_i_p = var(rel_errs_i_p_k,[], 3);
results.max_err_L2_i_p = max(rel_errs_i_p_k, [], 3);

results.mean_err_Linf_i_p = mean(maxerr_i_p_k, 3);
results.var_err_Linf_i_p = var(maxerr_i_p_k,[], 3);
results.max_err_Linf_i_p = max(maxerr_i_p_k, [], 3);

end

function plot_errors(u_v, u_err, s)
[n_d, n_var, n] = size(u_v);
u_mean = mean(u_v, 3);
for i = 1:n_var
    subplot(n_var, 1, i)
    hold on
    h1 = plot(1:n_d, u_mean(:,i), 'LineWidth', 2, 'DisplayName', 'mean');
    h2 = plot(1:n_d, squeeze(u_err(:, i, :))*s);
    if s ~=1
        h2(1).DisplayName = strvarexpand('error * $s$');
    else
        h2(1).DisplayName = 'error';
    end
    plot(xlim(), [0,0], 'k', 'LineWidth', 2)
    legend([h1, h2(1)])
end
end

