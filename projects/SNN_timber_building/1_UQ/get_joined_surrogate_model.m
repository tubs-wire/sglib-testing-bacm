function q_to_response = get_joined_surrogate_model(model_freqs, model_modes)

if nargin==1 && ~isempty(model_freqs)
    q_to_response = @(q)(get_freq_surrogate(model_freqs, q));
elseif nargin == 2 && isempty(model_freqs)
    q_to_response = @(q)(get_mode_surrogate(model_modes, q));
else
    q_to_response = @(q)(get_forward_surrogate(model_freqs, model_modes, q));
end
end

function y = get_forward_surrogate(model_freqs, model_modes, q)
% n_modes = length(model_freqs);
y_freqs = get_freq_surrogate(model_freqs, q);
y_modes = get_mode_surrogate(model_modes, q);
y = [y_freqs; y_modes];
end

function y = get_freq_surrogate(model_freqs, q)
n_modes = length(model_freqs);
y = [];
for i = 1:n_modes
    y = [y; model_freqs{i}.compute_response(q)];
end
end

function y = get_mode_surrogate(model_modes, q)
n_modes = length(model_modes);
y = [];
for i = 1:n_modes
    y = [y; model_modes{i}.compute_response(q)];
end
end