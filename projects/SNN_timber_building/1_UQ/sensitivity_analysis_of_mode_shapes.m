function plot_sensitivities(model)

%% Sensitivity analysis
[n_d, n_mode, n] = size(u) ;
model = GPCSurrogateModel.fromInterpolation(Q, q_i, reshape(u, n_d*n_mode, n), 6);
[partial_var, I_s, ratio_by_index]=...
    gpc_sobol_partial_vars(model.u_i_alpha, model.V_u, 'max_index', 1);
[u_mean, u_var] = gpc_moments(model.u_i_alpha, model.V_u);

u_var = reshape(u_var, n_d, n_mode);
partial_var = reshape(partial_var, n_d, n_mode, []);
% higher sensitivities

names = get_sobol_sensitivity_index_names(I_s, Q.param_names);

for i = 1:n_mode
    subplot(n_mode, 1, i)
    hold on
    h1 = plot(1:n_d, u_var(:,i), 'k', 'LineWidth', 2, 'DisplayName', 'mean');
    area(1:n_d, squeeze(partial_var(:,i,:)))
    if i == 1
        legend('Total variance', names{:}, 'Orientation', 'horizontal')
    end
     plot(xlim(), [0,0], 'k', 'LineWidth', 2)
end