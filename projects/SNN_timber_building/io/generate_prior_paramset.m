function Q = generate_prior_paramset()

e1 = MySimParameter('e1', UniformDistribution(6, 12));
e2 = MySimParameter('e2', UniformDistribution(10, 13));
e3 = MySimParameter('e3', UniformDistribution(6, 12));
g1 = MySimParameter('g1', UniformDistribution(400, 750));
g2 = MySimParameter('g2', UniformDistribution(200, 500));
q = MySimParameter('q', UniformDistribution(5, 100));

Q = MySimParamSet();
Q.add_parameter(e1, e2, e3, g1, g2, q);