function write_param_file(filename, Q, params, varargin)
% WRITE_PARAM_FILE Write a file with parameter values.
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



data = {};
params = round_params(params);
hashes = generate_hashes(params);

for i=1:Q.num_params()
    param = Q.get_param(i);
    data = [data {param.name, params(i,:)}]; %#ok<AGROW>
end

data = [data {'SHA1', hashes}];

write_data(filename, data, varargin{:});
