function read_response_by_params(params, varargin)
% READ_RESPONSE_BY_PARAMS Read a response file given the parameter values
%
% Example (<a href="matlab:run_example read_response_by_params">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin, mfilename);
%[version, options]  = get_option(options, 'version', -1);
check_unsupported_options(options);


assert(iscolumn(params), 'params must be column vector');
hash = generate_hashes(params, 'nocell', true);

s=read_response_by_hash(hash);
