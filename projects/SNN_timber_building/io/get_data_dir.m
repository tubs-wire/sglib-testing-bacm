function dir=get_data_dir(dirspec)
% GET_DATA_DIR Return directory where to place output or find input files.
%
% Example (<a href="matlab:run_example get_data_dir">run</a>)
%
% See also

%   Elmar Zander (modified to SNN project by Noemi Friedman, 2020 SZTAKI)
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



dir='./data/';
switch dirspec
    case 'images'
        dir = [dir 'images'];
    case 'params'
        dir = [dir 'paramfiles1'];
    case 'response'
        dir = [dir 'output1'];
        %dir = '/home/ezander/projects/projects/vitam/towire
    case 'update'
        dir = [dir 'update_results'];
        %dir = '/home/ezander/projects/projects/vitam/towire';
    otherwise
        if isdir([dir, dirspec])    
            dir = [dir, dirspec];
        else
            error('sglib:unknown', 'Unknown directory specifictation "what": %s', dirspec);
        end
end
