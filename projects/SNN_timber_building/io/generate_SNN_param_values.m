function generate_SNN_param_values()

N = 4000;
filename = strvarexpand('qmc$N$');
% Generate a paramset of version 1
Q = generate_prior_paramset();
generate_and_write_params(filename, Q, N, 'qmc');

N1 = 10000;
filename = strvarexpand('qmc_$N+1$-$N1$');
% Generate a paramset of version 1
Q = generate_prior_paramset();
ind_to_write = 4001:N;
generate_and_write_params(filename, Q, N1, 'qmc', ind_to_write);

grid = 'smolyak';
for i = 2:5
    filename = strvarexpand('$grid$_int_points_p$i$');
    generate_and_write_int_points(filename, Q, i, grid)
end

grid = 'full_tensor';
for i = 2:5
    filename = strvarexpand('$grid$_int_points_p$i$');
    generate_and_write_int_points(filename, Q, i, grid)
end
end



function generate_and_write_params(filename, Q, N, mode, ind_to_write)
params = Q.sample(N, 'mode', mode);
if nargin>4
    params = params(ind_to_write);
end
write_param_file(filename, Q, params, 'dirspec', 'params', 'ext', 'dat');
write_param_file(filename, Q, params, 'dirspec', 'params', 'ext', 'csv');
end

function generate_and_write_int_points(filename, Q, i, grid)
params = Q.get_integration_points(i, 'grid', grid);
display(size(params));
write_param_file(filename, Q, params, 'dirspec', 'params', 'ext', 'dat');
write_param_file(filename, Q, params, 'dirspec', 'params', 'ext', 'csv');
end