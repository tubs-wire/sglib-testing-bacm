function unittest_prepfilename(varargin)
% UNITTEST_PREPFILENAME Test the PREPFILENAME function.
%
% Example (<a href="matlab:run_example unittest_prepfilename">run</a>)
%   unittest_prepfilename
%
% See also PREPFILENAME, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'prepfilename' );

name = 'foo bar (x = 3.1)';
assert_equals(prepfilename(name), 'foo_bar_x___3p1');
