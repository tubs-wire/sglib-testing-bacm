function s=read_response_by_hash(hash, varargin)

options=varargin2options(varargin, mfilename);
[as_matrix, options]=get_option(options, 'as_matrix', false);
[setup, options]=get_option(options, 'setup', 'bfs');
[dirspec, options]=get_option(options, 'dirspec', 'response');

check_unsupported_options(options);

switch setup
    case 'bfs1'
        prefix = '';
        postfix = '_v1';
    case 'bfs'
        prefix = '';
        postfix = '_v3';
    case 'bump'
        prefix = '';
        %postfix = '_bump';
        postfix = '_v3';
    % example for the delta wing
    case 'delta_wing'
        prefix = '';
        postfix = '_v3';
    otherwise
        prefix = [setup '_'];
        postfix = '';
end


basedir = get_data_dir(dirspec);
filename = fullfile(basedir, [prefix, hash, postfix]);
s = read_vitam_csv_file(filename, 2, 'as_matrix', as_matrix, 'setup', setup);
