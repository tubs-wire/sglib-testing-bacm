function [f_ordered, u_ordered, ind_problem] = ...
    reorder_sim_freqs_and_modes(freqs_i, ux, uy, u_x_init_modes, u_y_init_modes, freq_init)
% REORDER_SIM_FREQS_AND_MODES It loads the row data of
% frequencies and mode shapes and reorders the frequencies and modes using
% the KMEANS clustering of the modes and its derivatives using a low rank 
% representation of these values.
%
%   REORDER_SIM_FREQS_AND_MODES Long description of plot_density.
%

% References
%
% Notes
%
% Example (<a href="matlab:run_example plot_density">run</a>)
%
% See also

%   Noemi Friedman and Blaz Kurent
%   Copyright 2021, SZTAKI and UL
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


%% normalize
u = reshape_and_join_u(ux, uy);
u = normalize_and_flip(u, false);

u_init =  reshape_and_join_u(u_x_init_modes, u_y_init_modes);
u_init = normalize_and_flip(u_init, false);

[n_node, ~, n] = size(u);

%% Print original mode ordering
% plot_u(u)

%% seperate first the first three modes
i_modes = 1:3;
[idx, ~] = cluster_eigenmodes(u(:,i_modes,:), freqs_i(i_modes,:), true, false, [], [], u_init(:,i_modes));
ind_problem = check_classifyer(idx, length(i_modes), n);
if isempty(ind_problem)
    [u_ordered, f_ordered] = reorder_u_and_f(u(:,i_modes,:), freqs_i(i_modes,:), idx);
else
    error('K means classifyer does not work well, for some simulations different modes are classified to be the same')
end

%% Find flipped modes in 4th mode
i_modes = 4;
[idx, ~] = cluster_eigenmodes(u(:,i_modes,:), [], true, false, [], [], u_init(:,i_modes,:));
u_4 = flip_modes(u(:,i_modes,:), idx);
u_ordered = cat(2, u_ordered, u_4);
f_ordered = cat(1, f_ordered, freqs_i(i_modes,:));

%% Seperate modes 5-6
i_modes = 5:6;
% reorder the eigenmodes, so that derivatives are more sensible
new_ord = cat(2, [1:2:13, 2:2:13], [14:2:26, 15:2:26]);
k = 4;
[idx, ~] = cluster_eigenmodes(u(new_ord,i_modes,:), freqs_i(i_modes,:), true, false, k, 10,  u_init(new_ord,i_modes,:));
ind_problem = check_classifyer(idx, length(i_modes), n);

%% Eliminate problematic simulations
idx = reshape(idx, 2, n);
idx = idx(:, ~ind_problem);
u_5_6 = NaN(n_node, length(i_modes), n);
f_5_6 = NaN(length(i_modes), n);

[u_5_6(:, :, ~ind_problem), f_5_6(:,  ~ind_problem)] =...
    reorder_u_and_f(u(:, i_modes, ~ind_problem), ...
    freqs_i(i_modes, ~ind_problem), idx);

u_ordered = cat(2, u_ordered, u_5_6);
f_ordered = cat(1, f_ordered, f_5_6);

%q_ordered = q_i(:, ~ind_problem);


% if false
%     figure
%     plot_modes(u_ordered)
%     H = histogram(f_ordered(1,:)', 'BinWidth', 0.05)
%     hold on
%     H = histogram(f_ordered(2,:)', 'BinWidth', 0.05)
%     H = histogram(f_ordered(3,:)', 'BinWidth', 0.05)
%     legend('Mode 1', 'Mode2', 'Mode3')
%     legend('Mode 1', 'Mode 2', 'Mode 3')
%     xlabel('frequencies')
% end

end


%% Clustering nodes
function [idx, u_mean, P] = cluster_eigenmodes(u, f, flag_derivatives, flag_add_frequencies, k, n_svd, u_init, f_init)
[~, n_mode, n] = size(u);
% Initiate data
% add derivatives
if flag_derivatives
    u_r = cat(1, u, diff(u));
    u_init = cat(1, u_init, diff(u_init));
else
    u_r = u;
end
% chose start point for means from one response chosen randomly
%j = randi([1, n],1);
%j=48;
% add flipped modes to start point
u_start = cat(2, u_init, -u_init);
% add frequencies if needed
if flag_add_frequencies && ~isempty(f)
    u_r = cat(1, u_r, reshape(f, [1, n_mode, n]));
    u_start = cat(1, u_start, [f_init, f_init]);
end
% reshape u (put together all modes)
u_r = reshape(u_r, [], n_mode*n);
% Clustering
% number of clusters
if isempty(k)
    k = n_mode*2;
elseif k~=n_mode*2
    u_start = [];
end
if nargin < 6 || isempty(n_svd)
    n_svd = 8;
end
[u_red, map_low_rank, map_full_rank] = low_rank(u_r, n_svd);
if isempty(u_start)
    [idx, u_mean, SUMD_r] = kmeans(u_red', k, 'Distance', 'sqeuclidean', ...
        'MaxIter', 100, 'EmptyAction', 'drop', ...
        'Display', 'final');
else
    [idx, u_mean, SUMD_r] = kmeans(u_red', k, 'Distance', 'sqeuclidean', ...
        'MaxIter', 100, 'EmptyAction', 'drop', ...
        'Display', 'final', 'Start', map_low_rank(u_start)');
end
% Possible distances are: 'sqeuclidean'/'cityblock'/ 'cosine'/'correlation'

if false
    ind_mode_exists = ~any(isnan(u_mean),2);
    u_red_grouped = {};
    
    for j = 1:k
        if ind_mode_exists(j)
            u_red_grouped = [u_red_grouped, u_red(:, idx == j)];
        end
    end
    figure
    plot_grouped_scatter(u_red_grouped)
end



%% try to appply gauss mixtures
if nargout > 2
    C = zeros(n_svd, n_svd, k);
    for j = 1:k
        C(:,:,j) = covariance_sample(u_red(:,idx == j));
    end
    
    dist_gm = gmdistribution(u_mean, C);
    %idx_gm = dist_gm.cluster(u_red');
    idx = dist_gm.cluster(u_red');
    P = dist_gm.posterior(u_red');
    P = reshape(P,n_mode, n, 4);
    %gmfit = fitgmdist(u_red', k);
    %idx = gmfit.cluster(u_red');
    %P = posterior(gmfit,u_red');
    idx_ = reshape(idx, n_mode, n);
    idx_(idx_==4)=2;
    idx_(idx_==3)=1;
    ind_problem = sum(idx_,1) ~=3;
end
end

function ind_problem = check_classifyer(idx, n_mode, n)
%% check whether classification is ok (for every simulation there should be a mode1, mode2, mode3, mode4..etc)
% reshape classifyer
idx_c = reshape(idx, n_mode, n);
% unite flipped modes
for j = (n_mode +1): (2*n_mode)
    idx_c(idx_c == j) = j-n_mode;
end
% check if every simulation has unique classifyers
valid_cols = perms(1:n_mode)';
if any(~ismember(idx_c', valid_cols', 'rows'))
    ind_problem = ~ismember(idx_c', valid_cols', 'rows');
    warning("classifyer should be corrected, there are simulations for which classifyers are not unique")
else
    disp("Classifyer is great!")
    ind_problem = [];
end

end
%% Reorder modes after clustering to modes and flipped modes
function [u_ordered, f_ordered] = reorder_u_and_f(u, f, idx) %u_mean)
[u_ordered, idx] = flip_modes(u, idx);
u_ordered = reorder_u(u_ordered, idx);
f_ordered = reorder_f(f, idx);
% ind_mode_exists = ~any(isnan(u_mean),2);
% [n_node, n_mode, n] = size(u);
% % Initiate solution tensor
% u_ordered = zeros(n_node, n_mode, n);
% f_ordered = zeros(size(f));
% f = f(:);
% for i = 1:n_mode
%     if ind_mode_exists(i)
%         u_i = u(:,idx==i);
%         f_i = f(idx==i);
%     else
%         u_i = [];
%     end
%     if ind_mode_exists(i+n_mode)
%         u_i = cat(2, u_i, -u(:,idx==i+n_mode));
%         f_i = cat(1, f_i,  f(idx==i+n_mode));
%     end
%     u_ordered(:,i,:) = reshape(u_i, size(u_i,1),1,size(u_i,2));
%     f_ordered(i,:) = f_i;
%end
end

function u_normed = normalize_and_flip(u, flag_flip, norm_type)
% normalize
if nargin < 3
    norm_type = "L2";
end
switch norm_type
    case "L2"
        norm_u = sqrt(sum(u.^2, 1));
    case "L1"
        norm_u = sum(abs(u),1);
end
u_normed = u./norm_u;
if flag_flip
    % fliping TODO: not 14, but somehow automatic
    u_normed = sign(u_normed(14,:,:)).*u_normed;
end
end

function [u_r, map_low_rank, map_full_rank] = low_rank(u, r)
C = covariance_sample(u);
R = 15;
[V, sigma_k] = kl_solve_evp(C, [], R);
figure()
plot(cumsum(sigma_k)/sum(sigma_k)*100, 'x-', 'LineWidth', 2)
[V, sigma_k] = kl_solve_evp(C, [], r);
u_r = V'* u;
map_low_rank = @(u)(V'*u);
map_full_rank = @(u_low)(V*u_low);
end

function [u, idx] = flip_modes(u, idx)
[~, n_mode, n] = size(u);
idx = reshape(idx, [], n);
for i = 1 : n_mode
    u(:,idx == (n_mode + i)) = -u(:, idx == (n_mode + i));
    idx (idx == (n_mode + i)) = i;
end
end
function u_1 = reorder_u(u, idx)
[n_node, n_mode, n] = size(u);
u_1 = zeros(n_node, n_mode, n);
for i = 1:n_mode
    for j = 1:n
        u_1(:,i,j) = u(:,idx(:,j)==i,j);
    end
end
end

function f_1 = reorder_f(f, idx)
[n_mode, n] = size(f);
f_1 = zeros(n_mode, n);
for i = 1:n_mode
    for j = 1:n
        f_1(i,j) = f(idx(:,j)==i,j);
    end
end
end

function u = reshape_and_join_u(ux, uy)
ux_m = reshape(ux, 13, 6, []);
uy_m = reshape(uy, 13, 6, []);
u = [ux_m; uy_m];
end