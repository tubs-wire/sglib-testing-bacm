function [mu, sigma, u_e] = get_experimental_mean_and_std(flag_filter, u_mean_FEM)
% GET_EXPERIMENTAL_MEAN_AND_STD loads the experimental modes flips
% normalizes and flips the mode if needed, 
%
%
%
%
%   Noemi Friedman
%   Copyright 2021, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

if nargin==0
    flag_filter = false;
end

%% Read experimental modes
[uex, uey, response_names_e] = get_experimental_modes();
u_e = reshape_and_join_u(uex, uey);
u_e = normalize_and_flip(u_e, true);
u_e(:,[3,4,5],:) = -u_e(:,[3,4,5],:);

%% Filter out outlyers

if flag_filter
    % filter out 4 bottom outlyer from mode 1
    u_e1 = squeeze(u_e(:,1,:));
    for i = 1:2
        [~, ind] = min(u_e1(1,:));
        u_e1(:, ind) = [];
        [~, ind] = max(u_e1(1,:));
        u_e1(:, ind) = [];
    end
    % filter out 4 bottom outlyer from mode 2
    u_e2 = squeeze(u_e(:,2,:));
    for e = 1:4
        [~, ind] = min(u_e2(1,:));
        u_e2(:, ind) = [];
    end
    % filter out 2 bottom and upper outlyer from mode 3
    u_e3 = squeeze(u_e(:,3,:));
    for i = 1:2
        [~, ind] = min(u_e3(1,:));
        u_e3(:, ind) = [];
        [~, ind] = max(u_e3(1,:));
        u_e3(:, ind) = [];
    end
    % filter out 2 bottom and upper outlyer from mode 4
    u_e4 = squeeze(u_e(:,4,:));
    for i = 1:2
        [~, ind] = min(u_e4(2,:));
        u_e4(:, ind) = [];
        [~, ind] = max(u_e4(2,:));
        u_e4(:, ind) = [];
    end
    % filter out 4 upper outlyer from mode 5
    u_e5 = squeeze(u_e(:,5,:));
    for i = 1:4
        [~, ind] = max(u_e5(1,:));
        u_e5(:, ind) = [];
    end
    % filter out bottom and upper outlyer from mode 6
    u_e6 = squeeze(u_e(:,6,:));
    for i = 1:2
        [~, ind] = min(u_e6(1,:));
        u_e6(:, ind) = [];
        [~, ind] = max(u_e6(1,:));
        u_e6(:, ind) = [];
    end
    % filtered experimental modes:
    u_ef = cat(3, u_e1, u_e2, u_e3, u_e4, u_e5, u_e6);
    u_e = permute(u_ef, [1,3,2]);
end
[n_d, n_mode, n] = size(u_e);

%% Measurement:
u_r = reshape(u_e, [], n);
[mu, sigma] = normfit(u_r');
mu = reshape(mu, n_d, n_mode);
sigma = reshape(sigma, n_d, n_mode);

%% flip modes 1,3,4,5,6
% mu(:,[1,3,4,5,6]) = -mu(:,[1,3,4,5,6]);
end

function u_normed = normalize_and_flip(u, flag_flip, norm_type)
% normalize
if nargin < 3
    norm_type = "L2";
end
switch norm_type
    case "L2"
        norm_u = sqrt(sum(u.^2, 1));
    case "L1"
        norm_u = sum(abs(u),1);
end
u_normed = u./norm_u;
if flag_flip
    % fliping TODO: not 14, but somehow automatic
    u_normed = sign(u_normed(14,:,:)).*u_normed;
end
end

function u = reshape_and_join_u(ux, uy)
ux_m = reshape(ux, 13, 6, []);
uy_m = reshape(uy, 13, 6, []);
u = [ux_m; uy_m];
end


