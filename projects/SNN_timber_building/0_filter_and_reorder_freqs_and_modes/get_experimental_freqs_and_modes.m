function [ux_i, uy_i, response_names] = get_experimental_freqs_and_modes()
% GET_EXPERIMENTAL_FREQS_MODES It loads the experimental frequencies and 
% modes from different phases (now it is 0-30 degrees, probably that is why
% the experimental modes are doubled)
%
%   GET_EXPERIMENTAL_FREQS_MODES Long description of plot_density.
%

% References
%
% Notes
%
% Example (<a href="matlab:run_example plot_density">run</a>)
%
% See also

%   Noemi Friedman and Blaz Kurent
%   Copyright 2020, SZTAKI and UL
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Read in external input-output data
% Read in data as table
data_filename = 'data/experimental_modes/Eigenvectors_experimental.xlsx';
%opts = detectImportOptions(data_filename);
% opts.VariableNamesRange = 'A1';
T = readtable(data_filename);
% Delete rows that are not ready yet
%T=T(~any(ismissing(T),2),:);
n = size(T,1);

mode_names = T.Properties.VariableNames;

mode_x_names = mode_names(contains(mode_names,'_x'));
mode_y_names =  mode_names(contains(mode_names,'_y'));
m_x = size(mode_x_names, 2);
m_y = size(mode_y_names,2);
%%

ux_i = zeros(m_x, n);
for i = 1: m_x
    ux_i(i, :) = T.(mode_x_names{i});
end

uy_i = zeros(m_y, n);
for i = 1: m_y
    uy_i(i, :) = T.(mode_y_names{i});
end

response_names = struct();
response_names.mode_x = mode_x_names;
response_names.mode_y = mode_y_names;
