function mu = get_experimental_mean_and_std() % , u_mean_FEM)
% GET_EXPERIMENTAL_MEAN_AND_STD loads the experimental modes flips
% normalizes and flips the mode if needed, 
%
%
%
%
%   Noemi Friedman
%   Copyright 2021, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


%% Read experimental modes
mu = get_experimental_modes_with_only_mean();
mu = normalize_and_flip(mu, false);

% Flipping and switching only by looking at the mode shapes
% Comment: this should be corrected to an automatized 
mu(:,[1,3]) = -mu(:,[1,3]);


end

function u_normed = normalize_and_flip(u, flag_flip, norm_type)
% normalize
if nargin < 3
    norm_type = "L2";
end
switch norm_type
    case "L2"
        norm_u = sqrt(sum(u.^2, 1));
    case "L1"
        norm_u = sum(abs(u),1);
end
u_normed = u./norm_u;
if flag_flip
    % fliping TODO: not 14, but somehow automatic
    u_normed = sign(u_normed(14,:,:)).*u_normed;
end
end



