function mu_exp = get_experimental_modes_with_only_mean()
% GET_EXPERIMENTAL_MODES It loads the experimental 
% modes from different phases (now it is 0-30 degrees, probably that is why
% the experimental modes are doubled)
%
%   GET_EXPERIMENTAL_MODES Long description of plot_density.
%

% References
%
% Notes
%
% Example (<a href="matlab:run_example plot_density">run</a>)
%
% See also

%   Noemi Friedman and Blaz Kurent
%   Copyright 2020, SZTAKI and UL
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%% Read in external input-output data
% Read in data as table
data_filename = 'data/experimental_modes/experimental_mode_shapes.mat';
%opts = detectImportOptions(data_filename);
% opts.VariableNamesRange = 'A1';
T = load(data_filename);
mu_exp = T.mu;

