function [Q, q_i, response_names, freqs_i, ux_i, uy_i] = ...
    get_params1_qmc_data(varargin)
% READ_IN_PARAMS1_QMC_OUTPUT_DATA Loads first set of QMC params and output data
%   READ_IN_PARAMS1_QMC_OUTPUT_DATA() Long description of startup.
%
% Example (<a href="matlab:run_example startup">run</a>)
%
% See also

%  Noemi Friedman & Blaz Kurent
%   Copyright 2020, SZTAKI, Budapest and UL, Ljubljana
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%%
options=varargin2options(varargin);
[data_filename,options]=get_option(options, 'data_filename', @isdefault);
check_unsupported_options(options, mfilename);

%% Read in external input-output data
% Read in data as table
if isdefault(data_filename)
    data_filename = 'data/output1/qmc4000.xlsx';
end
%opts = detectImportOptions(data_filename);
% opts.VariableNamesRange = 'A1';
  T = readtable(data_filename);
% Delete rows that are not ready yet
T=T(~any(ismissing(T),2),:);
% number of sample points
n = size(T,1);

%% Load input parameter properties (simparamset)

Q = generate_prior_paramset();

%% Input parameters

% Read from the table the input samples
n_q = Q.num_params;
q_i = zeros(n_q, n);

for i = 1: n_q
    q_i(i,:) = T.(Q.param_names{i});
end

%% Read from the table the response names
response_names = setdiff(T.Properties.VariableNames, Q.param_names);
response_names = setdiff(response_names, {'FrequencyError', 'MACerror', 'SHA1'});

freq_names = response_names(contains(response_names,'Freq'));
mode_x_names = response_names(contains(response_names,'_x'));
mode_y_names =  response_names(contains(response_names,'_y'));

response_names = struct();
response_names.mode_x = mode_x_names;
response_names.mode_y = mode_y_names;
response_names.freqs = freq_names;

if nargout > 3
    %%
    
    m_freq = size(freq_names, 2);
    m_x = size(mode_x_names, 2);
    m_y = size(mode_y_names,2);
    
    %% Frequencies
    
    freqs_i = zeros(m_freq, n);
    for i = 1: m_freq
        freqs_i(i, :) = T.(freq_names{i});
    end
    
    if nargout > 4
        %% Modes
        ux_i = zeros(m_x, n);
        for i = 1: m_x
            ux_i(i, :) = T.(mode_x_names{i});
        end
        
        uy_i = zeros(m_y, n);
        for i = 1: m_y
            uy_i(i, :) = T.(mode_y_names{i});
        end
    end
end
end
function b=isdefault(p)
b=isequal(p,@isdefault);
end
