function ind_to_keep = filter_out_outliers_based_on_surrogate(Q, q_i, u)
% FILTER_OUT_OUTLIERS_BASED_ON_SURROGATE Some modes are probably mixed together
% with higher modes. This function filters out from one mode the modes that
% can be considered to be outliers based on generation of proxi models.
% Data where the proxi error is very big cosidered to be outlyer. The filtering
% is done with partitions using training data for the surrogate and
% filtering only the testing data.
%  FILTER_OUT_OUTLIERS_FROM_MODE_5 Long description of plot_density.
%
% Options
%
% References
%
% Notes
%
% Example (<a href="matlab:run_example plot_density">run</a>)
%
% See also

%   Noemi Friedman and Blaz Kurent
%   Copyright 2021, SZTAKI, Univ Ljubljana
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



%% Filter in partititions (train with 8/9 of the data and filter out test data)
% number of sample points
n = size(u, 2) ;
% k-fold
k=9;
c = cvpartition(n,'KFold',k);
lin_ind = 1:n;

% Build surrogate with training points and filter out testing points
ind_to_keep = [];
for i = 1:k
    ind_train = ~c.test(i);
    ind_test = c.test(i);
    lin_ind_test = lin_ind(ind_test);
    
    model = GPCSurrogateModel.fromInterpolation(Q, q_i(:,ind_train), u(:,ind_train), 1);

    u_surr = model.compute_response(q_i(:, ind_test));
    err =  vecnorm(u(:, ind_test)-u_surr);
    % histogram(err_5, 'BinWidth', 0.05)
    
    ind = err<0.07;
    ind_to_keep = cat(2, ind_to_keep, lin_ind_test(ind));
    % disp(sum(ind))
end

ind_to_keep = sort(ind_to_keep);
%ind_to_drop = setdiff(lin_ind, ind_to_keep);

%% Further filtering: drop away outlyers
% Make a surrogate with all the kept points
model = GPCSurrogateModel.fromInterpolation(Q, q_i(:,ind_to_keep), u(:,ind_to_keep), 7);
u_pred = model.compute_response(q_i(:,ind_to_keep));
err = u_pred-u(:,ind_to_keep);
ind_to_keep = ind_to_keep(vecnorm(err) < 0.03);
% ind_to_drop = setdiff(lin_ind, ind_to_keep);