function [q_i_filtered, u_filtered, freqs_filtered, ind_filtered] = ...
    filter_out_outliers_from_mode_5_B(Q, q_i, u, freqs, ind_problem)

data_dir = get_data_dir('response');
file_name = 'qmc10000_filtered_mode_5.mat';
file_path = [data_dir, filesep, file_name];

%% Reorder and store data
if isfile(file_path)
    %% Load data
    S = load(file_path);
    q_i_filtered = S.q_i_filtered;
    u_filtered = S.u_filtered;
    freqs_filtered = S.freqs_filtered;
    ind_filtered = S.ind_filtered;
else
    %% Take only data points where we have an idea about mode 5
    % (see k means clustering)
    d_max = [1,2,3,4,5,6];
    for i=1:length(d_max)
        for d=1:d_max(i)
            q_i_5 = q_i (:, ~ind_problem);
            u_5 = squeeze(u(:, 5, ~ind_problem));
            freqs_5 = freqs(5, ~ind_problem);
            n = size(q_i_5, 2);

            % Filter
            ind_to_keep = filter_out_outliers_based_on_surrogate_B(Q, q_i_5, u_5, d, 0);
            q_i_filtered = q_i_5(:, ind_to_keep);
            u_filtered = u_5(:,ind_to_keep);
            freqs_filtered = freqs_5(:,ind_to_keep);
            % get indices that were problemetic in the begining and the once that
            % are filtered out as ourliers
            ind_to_drop = true(n,1);
            ind_to_drop(ind_to_keep)=false;
            ind_problem(~ind_problem)=ind_to_drop;
        end
    end
    ind_filtered = ind_problem;    
    %% Save to file
    save(file_path, 'q_i_filtered', 'u_filtered', 'freqs_filtered', 'ind_filtered');
end