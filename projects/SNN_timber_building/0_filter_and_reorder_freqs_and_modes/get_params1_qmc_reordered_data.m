function [Q, q_i, response_names, f_ordered, u_ordered, ind_problem_5] = ...
    get_params1_qmc_reordered_data(varargin)

options=varargin2options(varargin);
[data_filename,options]=get_option(options, 'data_filename', @isdefault);
[file_name,options]=get_option(options, 'file_name', @isdefault);
[data_dir,options]=get_option(options, 'data_dir', @isdefault);
check_unsupported_options(options, mfilename);

%%
if isdefault(data_filename)
    data_filename = 'data/output1/qmc4000.xlsx';
end
if isdefault(data_dir)
    data_dir = get_data_dir('response');
end
if isdefault(file_name)
    file_name = 'qmc4000_reordered.mat';
end
file_path = [data_dir, filesep, file_name];

%% Reorder and store data
if isfile(file_path)
    %% Load data
    S = load(file_path);
    Q = S.Q;
    q_i = S.q_i;
    response_names = S.response_names;
    f_ordered = S.f_ordered;
    u_ordered = S.u_ordered;
    ind_problem_5 = S.ind_problem_5;
else
    %% Prior paramset
    % Read in data and paramset
    [Q, q_i, response_names, freqs_i, ux, uy] = get_params1_qmc_data('data_filename', data_filename);
    [~, j_close_to_mean] = min(vecnorm(q_i-Q.mean(),2,1));
    
    %% Reorder frequencies and modes by K means clustering
    % Do some cheating so Blaz is happy with the ordering of the modes
    % Here we change the order of the mode that we use for initial mean, to
    % get the right ordering
    %freqs_i(1:2, j_close_to_mean) = freqs_i([2,1], j_close_to_mean);
    %ux(:, 1:2, j_close_to_mean) = ux([2,1], j_close_to_mean);
    %uy(:, 1:2, j_close_to_mean) = uy([2,1], j_close_to_mean);
    
    
    % choose initial modes for clustering
    ux_init = reshape(ux(:,j_close_to_mean), 13, 6);
    uy_init = reshape(uy(:,j_close_to_mean), 13, 6);
    u_x_init_modes =  ux_init(:, [2,1,3,4,5,6]);
    u_y_init_modes =  uy_init(:, [2,1, 3,4,5,6]);
    % reordering
    [f_ordered, u_ordered, ind_problem_5] = ...
        reorder_sim_freqs_and_modes(freqs_i, ux, uy, u_x_init_modes(:), u_y_init_modes(:));
    
    %% Save data
    save(file_path, 'Q', 'q_i', 'response_names', 'f_ordered', 'u_ordered', 'ind_problem_5')
    
end
end
function b=isdefault(p)
b=isequal(p,@isdefault);
end    