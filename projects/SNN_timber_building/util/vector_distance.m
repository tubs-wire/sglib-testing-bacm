function dist = vector_distance(t1, y1, t2, y2, varargin)
% VECTOR_DISTANCE Computes the vector (space) distance between vectors on overlapping intervals.
% 
%   DIST = VECTOR_DISTANCE(T1, Y1, T2, Y2, VARARGIN) computes the distance
%   between the functions given by the interpolation of Y1 and Y2 on the
%   their common interval, i.e. the intersection of the T1 and T2
%   (intervals given by their endpoints of course).
%
%   Options:
%     p: default 2: the power, e.g. use p=1 for the L1 norm, p=2 for the L2
%     deriv: default false, use true to include the first derivative, i.e.
%     use H1 instead of L2, or W_1^1 instead of L1
%
% Example (<a href="matlab:run_example vector_distance">run</a>)
%     % See unittest
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


options = varargin2options(varargin, mfilename);
[p, options] = get_option(options, 'p', 2);
[deriv, options] = get_option(options, 'deriv', false);
[N, options] = get_option(options, 'N', 10000);
check_unsupported_options(options);

[ti, yi1, yi2] = common_interp(t1, y1, t2, y2, 0);


dist = norm_diff(ti, yi1, yi2, p, N);
if deriv
    dist = dist + norm_diff(ti, ppder(yi1), ppder(yi2), p, N);
end
dist = dist ^ (1/p);


function dist = norm_diff(ti, yi1, yi2, p, N)
% If N is zero, use matlab internal function (however, the simple midpoint
% rule below is a bit faster and usually accurate enough (less though))
if N==0
    dist = norm_diff_by_integral(ti, yi1, yi2, p);
    return
end
% Intervals for integration via linspace
ti = linspace(ti(1), ti(end), N);
% Compute midpoints (we'll use the midpoint rule)
tim = 0.5 * (ti(1:end-1) + ti(2:end));
% compute values of |y_1 - y_2|^p at midpoints
dyp = abs(ppval(yi1, tim) - ppval(yi2, tim)) .^ p;
% approx the integral by multiplying with the interval sizes and summing
dist = sum( dyp .* diff(ti));


function dist = norm_diff_by_integral(ti, yi1, yi2, p)
fun = @(t)(abs(ppval(yi1, t) - ppval(yi2, t)) .^ p);
dist = integral(fun, ti(1), ti(end));
