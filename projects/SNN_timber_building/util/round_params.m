function params = round_params(params)
% ROUND_PARAMS Rounds param values to 13 significant digits so loading and saving is exact.
%
% Example (<a href="matlab:run_example round_params">run</a>)
%   params = round_params(params);
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

significant = 13;

% First round to 15 then to 13 digits (since this equivalent to what
% happened in the first version when storing stuff to ascii files)
params = round(params, 15, 'significant');
params = round(params, significant, 'significant');

v2 = round(params, significant, 'significant');
assert(isequal(params, v2), 'Rouding not idempotent!!!');
