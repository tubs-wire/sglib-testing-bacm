function unittest_round_params(varargin)
% UNITTEST_ROUND_PARAMS Test the ROUND_PARAMS function.
%
% Example (<a href="matlab:run_example unittest_round_params">run</a>)
%   unittest_round_params
%
% See also ROUND_PARAMS, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'round_params' );

rand_seed(1234);

r = rand(100,1); 
rr = round_params(r);
assert_true(all(abs(r-rr)<1e13*abs(r)), [], 'smalldiff1')
assert_equals(r, round_params(r), 'idempotent1')
assert_equals(rr, sscanf(sprintf('%.15g ', rr), '%g '), 'no_io_loss1' )


r = 100000*rand(100,1); 
rr = round_params(r);
assert_true(all(abs(r-rr)<1e13*abs(r)), [], 'smalldiff2')
assert_equals(r, round_params(r), 'idempotent2')
assert_equals(rr, sscanf(sprintf('%.15g ', rr), '%g '), 'no_io_loss2' )

r = 0.00001*rand(100,1); 
rr = round_params(r);
assert_true(all(abs(r-rr)<1e13*abs(r)), [], 'smalldiff3')
assert_equals(r, round_params(r), 'idempotent3')
assert_equals(rr, sscanf(sprintf('%.15g ', rr), '%g '), 'no_io_loss3' )
