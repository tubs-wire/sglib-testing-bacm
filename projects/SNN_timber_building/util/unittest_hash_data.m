function unittest_hash_data(varargin)
% UNITTEST_HASH_DATA Test the HASH_DATA function.
%
% Example (<a href="matlab:run_example unittest_hash_data">run</a>)
%   unittest_hash_data
%
% See also HASH_DATA, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'hash_data' );

% The expected values were computed with the same function, just to make
% sure get consistent hashes (so this is more of a regression test than a
% unittest, but anyway...)

assert_equals( hash_data([1,2,3], 'sha1'), '7a554081131a65a477b78ebd71bbea5f1fc98bd7');
assert_equals( hash_data([], 'sha1'), 'bf14a4957e7c050c374d47e440aef0a6b8b8748c');
assert_equals( hash_data({}, 'sha1'), '3b26707b5fb6d61ed19efe8ff21d79c74c8adc41');
assert_equals( hash_data({'abc', 4}, 'sha1'), '28620f7d70835c8ecf339ff4b05f9a8729e2f58a');



