function unittest_vector_distance(varargin)
% UNITTEST_VECTOR_DISTANCE Test the VECTOR_DISTANCE function.
%
% Example (<a href="matlab:run_example unittest_vector_distance">run</a>)
%   unittest_vector_distance
%
% See also VECTOR_DISTANCE, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'vector_distance' );


int = [0, pi/2];
al = pi/4;

% cos - sin on 0,pi/4
% sin - cos on pi/4, pi/2
% first: sin+cos, second 

t1 = sort(rand(50000,1)*pi/2);
t2 = sort(rand(50000,1)*pi/2);


%% {2*(-1 + sqrt(2)),(-2 + pi)/2,2,(2 + pi)/2}

func1 = @(x)cos(x);
func2 = @(x)sin(x);

v1 = func1(t1);
v2 = func2(t2);

[L1,l2,h1,h2] = deal(2*(-1 + sqrt(2)),(-2 + pi)/2,2,(2 + pi)/2);
H1 = L1 + h1; L2 = sqrt(l2); H2 = sqrt(l2 + h2);

assert_equals(L1, vector_distance(t1, v1, t2, v2, 'p', 1 ), 'L1_a1', 'abstol', 0.01)
assert_equals(L2, vector_distance(t1, v1, t2, v2, 'p', 2 ), 'L2_a1', 'abstol', 0.01)
assert_equals(H1, vector_distance(t1, v1, t2, v2, 'p', 1, 'deriv', true ), 'H1_a1', 'abstol', 0.01)
assert_equals(H2, vector_distance(t1, v1, t2, v2, 'p', 2, 'deriv', true ), 'H2_a1', 'abstol', 0.01)


%% {sqrt(2),pi/2,2*sqrt(2),2*pi}

func1 = @(x)cos(2*x);
func2 = @(x)sin(2*x);

v1 = func1(t1);
v2 = func2(t2);

[L1,l2,h1,h2] = deal(sqrt(2),pi/2,2*sqrt(2),2*pi);
H1 = L1 + h1; L2 = sqrt(l2); H2 = sqrt(l2 + h2);

assert_equals(L1, vector_distance(t1, v1, t2, v2, 'p', 1 ), 'L1_a2', 'abstol', 0.01)
assert_equals(L2, vector_distance(t1, v1, t2, v2, 'p', 2 ), 'L2_a2', 'abstol', 0.01)
assert_equals(H1, vector_distance(t1, v1, t2, v2, 'p', 1, 'deriv', true ), 'H1_a2', 'abstol', 0.01)
assert_equals(H2, vector_distance(t1, v1, t2, v2, 'p', 2, 'deriv', true ), 'H2_a2', 'abstol', 0.01)



%% {2/3*(-1 + 2*sqrt(2)),(-2 + 3*pi)/6,2*(1 + sqrt(2)),3/2*(2 + 3*pi)}

func1 = @(x)cos(3*x);
func2 = @(x)sin(3*x);

v1 = func1(t1);
v2 = func2(t2);

[L1,l2,h1,h2] = deal(2/3*(-1 + 2*sqrt(2)),(-2 + 3*pi)/6,2*(1 + sqrt(2)),3/2*(2 + 3*pi));
H1 = L1 + h1; L2 = sqrt(l2); H2 = sqrt(l2 + h2);

assert_equals(L1, vector_distance(t1, v1, t2, v2, 'p', 1 ), 'L1_a3', 'abstol', 0.01)
assert_equals(L2, vector_distance(t1, v1, t2, v2, 'p', 2 ), 'L2_a3', 'abstol', 0.01)
assert_equals(H1, vector_distance(t1, v1, t2, v2, 'p', 1, 'deriv', true ), 'H1_a3', 'abstol', 0.01)
assert_equals(H2, vector_distance(t1, v1, t2, v2, 'p', 2, 'deriv', true ), 'H2_a3', 'abstol', 0.01)

% use matlab integral function internally
assert_equals(L1, vector_distance(t1, v1, t2, v2, 'p', 1, 'N', 0 ), 'L1_a3', 'abstol', 0.001)
assert_equals(L2, vector_distance(t1, v1, t2, v2, 'p', 2, 'N', 0 ), 'L2_a3', 'abstol', 0.001)
assert_equals(H1, vector_distance(t1, v1, t2, v2, 'p', 1, 'deriv', true, 'N', 0 ), 'H1_a3', 'abstol', 0.001)
assert_equals(H2, vector_distance(t1, v1, t2, v2, 'p', 2, 'deriv', true, 'N', 0 ), 'H2_a3', 'abstol', 0.001)
