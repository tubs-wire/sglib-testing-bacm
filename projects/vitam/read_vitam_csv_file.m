function s=read_vitam_csv_file(filename, skip, varargin)
% READ_VITAM_CSV_FILE Read a vitam CSV file.
%   S=READ_VITAM_CSV_FILE(FILENAME, SKIP, AS_MATRIX) (see also
%   READ_CSV_FILE). If AS_MATRIX is true the raw contents is returned,
%   otherwise (default) a struct array containing different rows for e.g.
%   X, Y, V_X, CP and so on...
%
% Example (<a href="matlab:run_example read_vitam_csv_file">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version.
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin, mfilename);
[as_matrix, options]=get_option(options, 'as_matrix', false);
[setup, options]=get_option(options, 'setup', 'bfs');
check_unsupported_options(options);


switch setup
    % UNIQUE_COLS are the indices of the columns corresponding to the x and
    % z coordinates
    case {'bfs', 'bfs1'}
        unique_cols = [1, 2];
        col_numb = 7;
   case {'bump'}
        unique_cols = [1, 5];
        col_numb = 5;
   case {'delta_wing'}
        unique_cols = [1, 2];  
        col_numb = 3;
    otherwise
        % You need to specify for the setup which columns should be made
        % unique (usually the coordinates columns, so for 3d data it should
        % probably be 1:3).
        error('vitam:read', ['Define data format for setup: ', setup]);
end

data = read_csv_file(filename, skip, 'unique_cols', unique_cols);

if as_matrix
    s = data(:,1:col_numb);
else
    switch setup
        case {'bfs', 'bfs1'}
            s.x = data(:,1);
            s.z = data(:,2);
            s.v_x = data(:,3);
            s.cp = data(:,4);
            s.uw_b = data(:,5);
            s.cf = data(:,6);
            s.cf_err = data(:,7);
        case {'bump'}
            s.x = data(:,1);
            s.v_x = data(:,2);
            s.cp = data(:,3);
            s.uw_b = data(:,4);
            s.z = data(:,5);
        case 'delta_wing'
            s.x = data(:,1);
            s.y = data(:,2);
            s.cp = data(:,3);
        otherwise
            % If there is a new experimental setup either add the data/file
            % format here as a new case, or add the case to the correct
            % existing one.
            error('vitam:read', ['Define data format for setup: ', setup]);
    end
end
