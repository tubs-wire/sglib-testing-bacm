function test_analysis_vitam_dataset1




%% Build surrogate model for single response variables
clear
Q = generate_param_set();
%params = Q.sample(20, 'mode', 'qmc');
params = read_param_file('complete_qmc_corr');
%params = read_param_file('complete_sparse_p3_corr');
filter = 'cp_z0';
[x0,y0] = read_filtered_responses('exp', filter);
[xr,yr] = read_filtered_responses(params, filter);
[xi, yi0, yir] = common_interp(x0, y0, xr, yr, -1);
%plot(xi, yir)

% x=7.5 (first max), 8 (many local min)
ind = find(xi>=7.5, 1, 'first');
plot(params(5,:), yir(ind,:), '.')

V_q = Q.get_germ();
xi_i_k = Q.params2germ(params);

V_q2 = gpcbasis_modify(V_q, 'p', 2);

%%



%%
[pos, els, qs] = create_mesh_from_paramset(Q, {'SARC_C2', 'SARC_C3'});
%[pos, els, qs] = create_mesh_from_paramset(Q, {'PG', 'SAS'});
%[pos, els, qs] = create_mesh_from_paramset(Q, {'SAS', 'PG'});

%%

cost = @(y)(y(ind,:));
%cost = @(y)(y(11,:));
%cost = @(y)(sum(abs(y),1));
cost = @(y)(y);

surr = RBFSurrogateModel.fromDistanceWeighting(Q, params, cost(yir));

params_check = zeros(size(params,1), 0);
%params_check = [params_check, read_param_file('complete_lhs_corr')];
%params_check = [params_check, read_param_file('complete_qmc_corr')];
params_check = [params_check, read_param_file('complete_sparse_p3_corr')];

[xc,yc] = read_filtered_responses(params_check, filter);
[~, ~, yic] = common_interp(x0, y0, xc, yc, -1);
xx=[];
for a=0.2:0.1:5
    surr.set_kernel_params(a);
    xx(:,end+1) = [a; mean_prediction_error(surr, params_check, cost(yic))];
end
plot(xx(1,:), xx(2,:))
[~,a_ind] = min(xx(2,:)); 
a0 = xx(1, a_ind)

%%


surr = RBFSurrogateModel.fromDistanceWeighting(Q, params, yir);

for a=0.3:0.02:a0
    surr.set_kernel_params(a);
    tic
    subplot(2,1,1); 
    surr.normalized_kernel = true;
    us = surr.compute_response(qs);
    plot_field(pos, els, us(ind,:)', 'show_mesh', false )
    colorbar;

    subplot(2,1,2)
    surr.normalized_kernel = false;
    us = surr.compute_response(qs);
    plot_field(pos, els, us(ind,:)', 'show_mesh', false)
    colorbar;
    toc
    %waitforbuttonpress
    drawnow
    pause(0.02)
end
keyboard
%%

%[xi_i, w] = gpc_integrate([], V_q, p_int, varargin{:});
%q_i = germ2params(set, xi_i);


%% Build surrogate model for complete interpolated variables

%% Build surrogate model for cost

%% Compute sensitivities for single response variables

%% Compute optimum for cost function

%% Compute updated distribution



