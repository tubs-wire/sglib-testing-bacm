 function [q_i, q_MAP] = bayes_update_sample_with_interp(phase_numb, model_descr)

%% Load prior params and surrogate model

[Q, model, ~, exp_data, ind] = generate_VITAM_gpc_surrogate(phase_numb, ...
    'model_descr', model_descr, 'flag_interp', true);

%% Define the measurement error model
% define percentages for std of error
switch model_descr
    case 'bump'
        err_ratio = [repmat(0.05, 1, 8), repmat(0.1, 1, 9)];
        % for the plot the number of subplots
        n1 = (exp_data.num_sections-1)/2;
        m1 =2;
        n2 =1;
        m2 =1;
    case 'bfs'
        err_ratio = [0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05];
         % for the plot the number of subplots
        n1 = (exp_data.num_sections-2)/2;
        m1 =2;
        n2 =2;
        m2 =1;
    case 'delta_wing'
        err_ratio = repmat(0.1,1,16);
         % for the plot the number of subplots
        n1 = (exp_data.num_sections)/2;
        m1 =2;
        n2 =2;
        m2 =1;
    otherwise
        error(strvarexpand('no $model_descr$ model is implemented'))
end

% sigmas of the measurement error
sigmas = get_sigmas(err_ratio, exp_data, ind);
% measueremen error model
E = generate_stdrn_simparamset(sigmas);
% plot measurement model
plot_measurement_model(exp_data, E, ind, n1, m1, n2, m2)

%% Bayesian update
% surrogate compute response function
Y_func = @(q)(model.compute_response(q));
% likelihood
%q2likelihood = @(q)(E.pdf(binfun(@minus, Y_func(q), exp_data.values(ind))));
% loglikelihood
q2loglikelihood = @(q)(E.logpdf(binfun(@minus, Y_func(q), exp_data.values(ind))));
% number of sampels
N = 10000;
% number of parallel walks
N_b= 1000;
T = ceil(N/N_b);
% starting points of the random walk
q0 = Q.mean;
% Likelihood function
prior_vars=Q.var;
P=generate_stdrn_simparamset(sqrt(prior_vars/10));
%P=generate_stdrn_simparamset(sqrt(prior_vars/5));
% Bayesian update
%q_i = bayes_mcmc(q2likelihood, Q, N, P, q0, 'parallel', true, 'plot_dim', 3:5, 'plot', true, 'T', 1000);
figure
q_i = bayes_mcmc(q2loglikelihood, Q, N, P, q0, 'parallel', true, ...
    'plot_dim', 3:5, 'plot', true, 'T', T, 'log_flag', true, 'T_burn', 1500);

q_MAP=sample_density_peak(q_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb$'), model_descr];
%write_param_file(qfilename, Q, q_MAP, 'dirspec', 'update');

param_names = strrep(Q.param_names, '_',  ' ');
plot_grouped_scatter({Q.sample(N), q_i, q_MAP}, ...
'Labels', param_names, 'Legends', {'prior', 'posterior', 'est MAP'})
filename = ['bayes_posterior_samples', strvarexpand('phase$phase_numb$'), model_descr];
basedir = get_data_dir('images');
%save_png(gcf, filename, 'figdir', basedir)
disp(filename)

end

function sigmas = get_sigmas(ratios_of_max_value, exp_data, ind)
sigmas = zeros(sum(ind),1);
last_ind = 0;
for i = 1:exp_data.num_sections
    y_exp = exp_data.extract_values(i);
    max_y = max(abs(y_exp));
    ind_i = exp_data.extract_section(ind, i);
    % the three sigma region should be the given percentage deviation
    sigmas(last_ind + 1: last_ind + sum(ind_i)) = max_y*ratios_of_max_value(i)/3;
    last_ind = last_ind + sum(ind_i);
end
end
function plot_measurement_model(exp_data, E, ind, n1, m1, n2, m2)
sigmas = sqrt(E.var);
last_ind = 0;

for i = 1:exp_data.num_sections
    response_names = get_response_names_for_plot(exp_data);
    if i==1
        multiplot_init(n1, m1)
    elseif i==n1*m1+1
        figure()
        multiplot_init(n2, m2)
    end
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    ind_i = exp_data.extract_section(ind, i);
    sigmas_i = sigmas(last_ind + 1 : last_ind + sum(ind_i));
    multiplot()
    plot(t_exp, y_exp)
    hold on
    plot_between(t_exp(ind_i), y_exp(ind_i) - sigmas_i*3, y_exp(ind_i) + sigmas_i*3, [0.3, 0.6, 0.8])
    xlabel(exp_data.sections(i).axis);
    ylabel(response_names{i});
    last_ind = last_ind + sum(ind_i);
end
end