function graph_first_parameter_sample_points()

q_j_k_qmc = read_param_file('complete_qmc_corr', 'dirspec', 'params1');
q_j_k_lhs = read_param_file('complete_lhs_corr', 'dirspec', 'params1');
q_j_k_grid = read_param_file('complete_sparse_p3_corr', 'dirspec', 'params1');

q_j_k_L1 = read_param_file('qmc_L1_second_run', 'dirspec', 'params2');
q_j_k_L2 = read_param_file('qmc_L2_second_run', 'dirspec', 'params2');
q_j_k_w11= read_param_file('qmc_W11_second_run', 'dirspec', 'params2');
q_j_k_w21= read_param_file('qmc_W21_second_run', 'dirspec', 'params2');


plot_grouped_scatter({[q_j_k_qmc ,q_j_k_grid, q_j_k_lhs], q_j_k_w11,q_j_k_w21, q_j_k_L1, q_j_k_L2}, ...
    'Labels', Q.param_names, 'Legends', {'First 100 scan', 'W11', 'W21', 'L1', 'L2'})
