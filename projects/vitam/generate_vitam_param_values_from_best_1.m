function generate_vitam_param_values2

% Generate a paramset of version 2
Q = generate_param_set();

[q_L1, q_L2, q_W11, q_W21]=best_results_for_vitam_param_value1;

%%
params = [q_L1(:,1), q_L2(:,1), q_W11(:,1), q_W21(:,1)];
write_param_file('best_param_values_from_first_run', Q, params);
