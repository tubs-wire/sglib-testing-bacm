clear all
%close all
figure
Q = generate_param_set('version', 3);


%train_data.add_param_set('complete_qmc_corr');
%train_data.add_param_set('complete_lhs_corr');
%train_data.add_param_set('complete_sparse_p3_corr');


exp_data = VitamDataHandlerDeltaWing();
exp_data.read_data()
response_names = {exp_data.sections.name};
%%

colors = 'rkcbm';
pointset = {'qmc_PG_0-10_for_deltawing'};

for j = 1:length(pointset)
    if j>1
        clear train_data
    end
    train_data = VitamDataHandlerDeltaWing();
    pointset_j = pointset{j};
    if iscell(pointset_j) && length(pointset_j) > 1 % if first 100 pointset
        for k = 1:length(pointset_j)
            train_data.add_param_set(pointset_j{k})
        end
    else
        train_data.add_param_set(pointset_j, 'dirspec', 'params1')
    end
    train_data.read_data()
    for i = 1:train_data.num_sections
        
        
        %% Check L1 region outputs
        y = train_data.extract_values(i);
        z = train_data.extract_coords(i);
        
        if j == 1
            y_exp = exp_data.extract_values(i);
            z_exp = exp_data.extract_coords(i);
        end
        hold all
        subplot(train_data.num_sections/2,2,i)
        if j==1
            plot(z_exp, y_exp, 'bx');
        end
        hold on
        y_minmax = [min(y, [], 2), max(y, [], 2)];
        plot(z, y_minmax, 'Color', colors(j));
%         if i < 8
%             xlim([1,2]);
%         else
%             xlim([0,10]);
%         end
        %xlabel()
        ylabel(response_names{i})
    end
end

