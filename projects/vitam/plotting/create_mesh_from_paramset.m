function [pos, els, params]=create_mesh_from_paramset(Q, param_names)
% CREATE_MESH_FROM_PARAMSET Create a mesh (e.g. for plotting) from a PARAMSET.
%   [POS, ELS, PARAMS]=CREATE_MESH_FROM_PARAMSET(Q, PARAM_NAMES) creates a mesh
%   with POS and ELS and from a ParamSet object Q varying the parameters
%   in PARAM_NAMES. There must be exactly 2 elements in PARAM_NAMES, as we
%   can currently only generate 2d meshes (please extend to 1d and 3d, when
%   you have time...). In PARAMS the parameters values from Q corresponding
%   to POS, i.e. parameter q_i corresponds to Q(:,i) and the reponse y_i
%   should be plotted at POS(:,i).
%   
%
% Example (<a href="matlab:run_example create_mesh_from_paramset">run</a>)
%   K = SimParameter('K', UniformDistribution(0, 10));
%   D = SimParameter('D', NormalDistribution(0, 1.5));
%   M = SimParameter('M', UniformDistribution(0, 7));
%   Q = SimParamSet();
%   Q.add_parameter(K, D, M);
%   [pos, els, q]=create_mesh_from_paramset(Q, {'K', 'M'});
%   y = (q(1,:).*q(3,:)-q(2,:))'; % some stupid sample response
%   plot_field(pos, els, y, 'show_mesh', false);
%
% See also SIMPARAMETER, PLOT_FIELD

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


order = [1,2];
if nargin>=2
    fixed = Q.find_fixed();
    for i=1:Q.num_params
        if ~fixed(i)
            Q.set_to_mean(i);
        end
    end
    ind = nan(length(param_names),1);
    for i=1:length(param_names)
        Q.set_not_fixed(param_names{i})
        ind(i) = Q.param_map.find_key(param_names{i});
    end
    [~,order] = sort(ind);
end


rv_ind=find(Q.find_rv());
assert(length(rv_ind)==2, 'Only 2d supported yet. You need to fix all but 2 RVs in the paramset thus.');

q = Q.sample(1000, 'mode', 'lhs');
qmin = min(q(rv_ind,:), [], 2);
qmax = max(q(rv_ind,:), [], 2);

n1 = 100;
n2 = n1;

x1 = linspace(qmin(1), qmax(1), n1);
x2 = linspace(qmin(2), qmax(2), n2);
[X1,X2] = meshgrid(x1, x2);
[pos, els] = create_mesh_from_grid(X1, X2);
params = Q.sample(numel(X1));
params(rv_ind(1),:) = X1(:);
params(rv_ind(2),:) = X2(:);

if ~isempty(order)
    pos(order,:) = pos;
end

