function graph_bfs_params1_overview


Q = generate_param_set('version', 2);

N=20;
params_qmc = Q.sample(N, 'mode', 'qmc');

[params_smo, ~, ~] = Q. get_integration_points(3, 'grid', 'smolyak');

params_lhs = read_param_file('complete_lhs_corr');

fullscreen(clf)
clf

show_all(params_qmc, 'QMC');
basedir = get_data_dir('images');
save_png(gcf, 'bfs_qmc', 'figdir', basedir, 'notitle', false)

show_all(params_smo, 'sparse grid');
basedir = get_data_dir('images');
save_png(gcf, 'bfs_sparse', 'figdir', basedir, 'notitle', false)

show_all(params_lhs, 'LHS');
basedir = get_data_dir('images');
save_png(gcf, 'bfs_lhs', 'figdir', basedir, 'notitle', false)


function show_all(params, paramset, varargin)
multiplot_init(10, [], 'ordering', 'row')
paramset = ['(' paramset ')']; %#ok<NASGU>
multiplot; show_responses(params, 'v_x_x1', strvarexpand('v_x(x=1) $paramset$'), varargin{:});
multiplot; show_responses(params, 'v_x_x4', strvarexpand('v_x(x=4) $paramset$'), varargin{:});
multiplot; show_responses(params, 'v_x_x6', strvarexpand('v_x(x=6) $paramset$'), varargin{:});
multiplot; show_responses(params, 'v_x_x10', strvarexpand('v_x(x=10) $paramset$'), varargin{:});
multiplot; show_responses(params, 'uw_b_x1', strvarexpand('uw_b(x=1) $paramset$'), varargin{:});
multiplot; show_responses(params, 'uw_b_x4', strvarexpand('uw_b(x=4) $paramset$'), varargin{:});
multiplot; show_responses(params, 'uw_b_x6', strvarexpand('uw_b(x=6) $paramset$'), varargin{:});
multiplot; show_responses(params, 'uw_b_x10', strvarexpand('uw_b(x=10) $paramset'), varargin{:});
multiplot; show_responses(params, 'cf_z0', strvarexpand('c_f $paramset$'), varargin{:});
multiplot; show_responses(params, 'cp_z0', strvarexpand('c_p $paramset$'), varargin{:});

function show_responses(params, filter, titlestr)
hold off
[x, y] = read_filtered_responses(params, filter);
for i=1:size(params,2)
    plot(x, y(:,i), '-k')
    hold all
end

[x, y] = read_filtered_responses('exp', filter);
plot(x, y, 'xr', 'linewidth', 2)
title(titlestr)
