function [av_err_p ,max_err_p]= graph_surrogate_accuracy_phase3_delta_wing
%% Load paramset and data
Q = generate_param_set('version', 3);

train_data = VitamDataHandlerDeltaWing();
train_data.add_param_set('qmc_PG_0-10_for_deltawing',  'dirspec', 'params1')
%hardcoded setting for 25 deg case:
            dirspec = 'response_delta_wing_25';
train_data.read_data('dirspec', dirspec);

exp_data = VitamDataHandlerDeltaWing();
exp_data.read_data('dirspec',dirspec);

%% Surrogate model generating functions
model_funcs = {@(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 1),...
               @(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 2),...
               @(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 3)};%,...
               %@(Q, q, u, w)PCSurrogateModel.fromInterpolation(Q, q, u, 1),...
               %@(Q, q, u, w)PCSurrogateModel.fromInterpolation(Q, q, u, 2),...
               %@(Q, q, u, w)PCSurrogateModel.fromInterpolation(Q, q, u, 3)};
model_names = {'GPC p1 Interp', 'GPC p2 Interp','GPC p3 Interp'};

%model_names = {'GPC p1 Interp', 'GPC p2 Interp','GPC p3 Interp', 'PC p1 Interp', 'PC p2 Interp', 'PC p3 Interp'};

%% cross validate different degree gpces
% q_j_l = train_data.params;
% u_i_l = train_data.values;
K = 15;
n_s = 0.8;

[err_i_p, maxerr_i_p]  = train_and_cross_val(Q, train_data, K, model_funcs, n_s);

w=[];
response_names = get_response_names_for_plot(exp_data);

for i=1:train_data.num_sections
    subplot(train_data.num_sections/2, 2, i)
    x = train_data.extract_coords(i);
    v = train_data.extract_section(err_i_p, i);
    vmax = train_data.extract_section(maxerr_i_p, i);
    x_e = exp_data.extract_coords(i);
    u_e = exp_data.extract_values(i);
    plot(x,abs(v)*10)
    hold on
    set(gca,'ColorOrderIndex',1)
    plot(x,abs(vmax)*10, ':', 'LineWidth', 2)
    plot(x_e, u_e, 'LineWidth', 2)
    %ylabel(train_data.sections(i).name)
    ylabel(response_names{i})
    if i==2
        leg_1 = strcat(strrep(model_names, ' Interp', ''), '(av. abs err)*10');
        leg_2 = strcat(strrep(model_names, ' Interp', ''), '(max abs err)*10');
        legend(leg_1{:}, leg_2{:}, 'experimental')
    end
    xlim(minmax(x_e'))
    % store weights for avaraging
    w_i = 1/max(abs(u_e))*ones(size(v,1),1);
    w = [w;w_i];
end

%% save figure
filename = 'surrogate_accuracy_delta_wing_phase3';
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
disp(filename)

%% Compute some avarage error

av_err_p = diag(err_i_p'*diag(w.^2)*err_i_p)
max_err_p = max(maxerr_i_p.*w,[], 1)

