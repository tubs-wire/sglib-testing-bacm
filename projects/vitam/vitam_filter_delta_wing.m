function [s, var, axis]=vitam_filter_delta_wing(s, name)
% VITAM_FILTER Filters a struct given some predefined filters.
%   [S, VAR, AXIS]=VITAM_FILTER(S, NAME) Long description of vitam_filter.
%
% Example (<a href="matlab:run_example vitam_filter">run</a>)
%
% See also

%   Noemi Friedman (modified from bfs model of Elmar Zander)
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

[response_names,x] = read_delta_wing_response_data_struct();

for i = 1:length(response_names)
    x_i = x(i);
    filters.(strvarexpand('x$i$')) = @(s)(s.x == x_i);
end
filters.cp   = @(s)(s.cp~=0);


%filters.z0  = @(s)(s.z== 0);

% Simulation files
% x = 1, 4, 6, 10 goes with z, v_x, cp, uw_b
% z = 0 goes with cp, cf

% Experiment file
% x = 1, 4, 6, 10 goes with z, v_x, uw_b
% z = 0 goes with x, cp
% z = 0 goes with x, cf, cf_error

ind = find(strcmp(response_names,name));
funs={filters.cp, filters.(strvarexpand('x$ind$'))};
 
for i=1:numel(funs)
    s = struct_filter(s, funs{i});
end
var = 'cp';
axis = 'y';

[~,ind] = sort(s.(axis));
s.(axis) = s.(axis)(ind);
s.(var) = s.(var)(ind);


