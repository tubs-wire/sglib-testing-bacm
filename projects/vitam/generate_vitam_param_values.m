function generate_vitam_param_values(N, filename)

% Generate a paramset of version 1
Q = generate_param_set('version', 3);
%N = 300;

% Generate QMC points
Q.reset_fixed();
%generate_and_write_params('only_qmc_PG_0-10', Q, N, 'qmc');
generate_and_write_params(filename, Q, N, 'qmc');


function generate_and_write_params(filename, Q, N, mode)
params = Q.sample(N, 'mode', mode);
write_param_file(filename, Q, params,  'dirspec', 'params1');
