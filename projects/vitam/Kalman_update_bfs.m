function q_i = Kalman_update_bfs()

%% Load prior params and surrogate model
phase_numb = 2;
%model_descr = 'bump';
model_descr = 'bfs';
[Q, model, train_data, exp_data, ind] = generate_VITAM_gpc_surrogate(phase_numb, ...
    'model_descr', model_descr, 'flag_interp', true, 'flag_PCE', true);
% gpc basis and coefficient of the response
V_Y = model.V_u;
Y_j_alpha = model.u_i_alpha;
% Experimental values
y_m =exp_data.values(ind);

%% Define the measurement error model
% define percentages for std of error
switch model_descr
    case 'bump'
        err_ratio = [repmat(0.05, 1, 8), repmat(0.1, 1, 9)];
    case 'bfs'
        err_ratio = [0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05];
    otherwise
        error()
end
% sigmas of the measurement error
sigmas = get_sigmas(err_ratio, exp_data, ind);
% measueremen error model
E = generate_stdrn_simparamset(sigmas);
[E_j_beta, V_E]=E.gpc_expand();
% GPC of the prior param
[Q_i_alpha, V_Q] = Q.gpc_expand();
n_V = gpcbasis_size(V_Y);
Theta_i_alpha = zeros(n_V(2), n_V(1));
Theta_i_alpha(:, 2:n_V(2)+1)= eye(n_V(2));

% [V_QY, ind1, ind2] = ...
%     gpcbasis_combine(V_Q, V_Y, 'inner_sum', 'as_operators', true);
% Q_i_alpha=Q_i_alpha*ind1;
% Y_j_alpha=Y_j_alpha*ind2;
%gPCE of the updated Theta
for i=1:5
%[Q_i_gamma, V_Qp, Z_j_gamma] = gpc_KF(Q_i_alpha, Y_j_alpha, V_QY, E_j_beta, V_E, y_m);
% Q_i_alpha = Q_i_gamma;
% V_QY = V_Qp;
% Y_j_alpha = Z_j_gamma;
[Theta_i_gamma, V, Z_j_gamma] = gpc_KF(Theta_i_alpha, Y_j_alpha, V_Y, E_j_beta, V_E, y_m);
 Theta_i_alpha = Theta_i_gamma;
 V_Y = V;
 Y_j_alpha = Z_j_gamma;
end
%sample from the updated Q
N=10000;
theta_i = gpc_sample(Theta_i_gamma, V, N);
q_i =Q.stdnor2params(theta_i);
% q_i=gpc_sample(Q_i_gamma, V_Qp, N);
% [q_mean, q.var] = gpc_moments(Q_i_gamma, V_Qp);

q_peak=sample_density_peak(q_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
plot_grouped_scatter({Q.sample(N), q_i, q_peak}, ...
'Labels', Q.param_names, 'Legends', {'prior', 'posterior', 'MAP est'})
end
function sigmas = get_sigmas(ratios_of_max_value, exp_data, ind)
sigmas = zeros(sum(ind),1);
last_ind = 0;
for i = 1:exp_data.num_sections
    y_exp = exp_data.extract_values(i);
    max_y = max(abs(y_exp));
    ind_i = exp_data.extract_section(ind, i);
    % the three sigma region should be the given percentage deviation
    sigmas(last_ind + 1: last_ind + sum(ind_i)) = max_y*ratios_of_max_value(i)/3;
    last_ind = last_ind + sum(ind_i);
end
end
