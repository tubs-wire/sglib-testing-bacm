function [x,y,axis] = read_filtered_responses(params, filter, varargin)

options = varargin2options(varargin, mfilename);
[x_ext, options] = get_option(options, 'x', []);
[y_ext, options] = get_option(options, 'y', []);
[model_def, options] = get_option(options, 'model_def', 'bfs'); %bump
[dirspec, options] = get_option(options, 'dirspec', 'response_bfs');

check_unsupported_options(options);


if isempty(params) || isequal(params,'exp')
    s=read_exp_file('setup',model_def);
    switch model_def
        case {'bfs', 'bfs1'}
            [s, var, axis]=vitam_filter(s, filter);
        case 'bump'
            [s, var, axis]=vitam_filter_bump(s, filter);
        case 'delta_wing'
            [s, var, axis]=vitam_filter_delta_wing(s, filter);
    end
    x = s.(axis);
    y = s.(var);
else
    params = round_params(params);
    hashes = generate_hashes(params);
    for i=1:size(params,2)
        base = hashes{i};
        s=read_response_by_hash(base,'setup',model_def, 'dirspec', dirspec);
        switch model_def
            case {'bfs', 'bfs1'}
                [s, var, axis]=vitam_filter(s, filter);
            case 'bump'
                [s, var, axis]=vitam_filter_bump(s, filter);
            case 'delta_wing'
                [s, var, axis]=vitam_filter_delta_wing(s, filter);
        end
        
        if i==1
            x = s.(axis);
            y = zeros(numel(x), size(params,2));
            flag_interp = false;
        else
            %assert( all(x == s.(axis)) );
            if length(x)~=length(s.(axis)) ||~( all(x == s.(axis)) )
                warning('VITAMreadResponse:InterpData',...
                    strvarexpand('The $i$th data of $var$ is interpolated to the original spatial grid'))
                flag_interp = true;
            else
                flag_interp = false;
            end
        end
        if flag_interp
            y(:,i) = interp1(s.(axis), s.(var), x, 'pchip');
        else
            y(:,i) = s.(var);
        end
        
    end
end

if ~isempty(x_ext)
    assert(~isempty(y_ext), 'Cannot extend only x (that just makes no sense)');
    x = [x_ext; x];
    y = [y_ext; y];
end


