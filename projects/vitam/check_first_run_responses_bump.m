Q = generate_param_set();

train_data = VitamDataHandlerBump();

%% Load points
train_data.add_param_set('complete_qmc_corr');
train_data.add_param_set('complete_lhs_corr');
train_data.add_param_set('complete_sparse_p3_corr');
train_data.read_data();

%% Experimental result
exp_data = VitamDataHandlerBump();
exp_data.read_data();

%% Parameter uncertainties
Q = generate_param_set();

%% Plot responses and experiment results
response_names = exp_data.filters;
% For all responses plot the samples
for i = 1:train_data.num_sections
    
    y = train_data.extract_values(i);
    z = train_data.extract_coords(i);
    
    y_exp = exp_data.extract_values(i);
    z_exp = exp_data.extract_coords(i);
       
    hold all
    subplot(6,3,i)
    hold on
    %y_minmax = [min(y, [], 2), max(y, [], 2)];
    plot_between(z, min(y, [], 2), max(y, [], 2), [0.8, 0.8, 0.8]);
    plot(z, y, 'Color', [0.6, 0.6, 0.6]);
    % plot experimental result
    plot(z_exp, y_exp, 'bx', 'LineWidth', 2);
    if i < 17
      xlim([0,0.4]);
    else
        xlim([0.5,2.5]);
    end
    xlabel(exp_data.sections(i).axis)
    ylabel(response_names{i})
end



