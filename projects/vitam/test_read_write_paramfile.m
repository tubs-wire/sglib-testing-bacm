function test_read_write_paramfile


%% Test 1: Fileversion 2 read and write gives equal stuff
Q = generate_param_set();
N = 20;
params = Q.sample(N);
params = round_params(params);
hashes = generate_hashes(params);

write_param_file('test_params', Q, params, 'dirspec', 'params1');
[params2, hashes2] = read_param_file('test_params', 'dirspec', 'params1');
hashes2new = generate_hashes(params2);

assert_equals(params, params2, 'params', 'reltol', 1e-15);
assert_equals(hashes, hashes2, 'hashes');
assert_equals(hashes, hashes2new, 'hashes regenerated');


%% Test 2: QMC actual and reread gives equal stuff
Q = generate_param_set();
N = 20;
params = Q.sample(N, 'mode', 'qmc');
params = round_params(params);
hashes = generate_hashes(params);

[params2, hashes2] = read_param_file('complete_qmc_corr', 'dirspec', 'params1');
%[params2, hashes2] = read_param_file('complete_qmc_corr', 'dirspec', 'data');

assert_equals(params, params2, 'params', 'reltol', 0*1e-15, 'abstol', 0);
assert_equals(hashes, hashes2, 'hashes');

%% Test 3: QMC actual and reread gives equal stuff
Q = generate_param_set();
[params, ~, ~] = Q. get_integration_points(3, 'grid', 'smolyak'); % [q_i, w, xi_i]
%params = round(params, 15, 'significant');
params = round_params(params);
hashes = generate_hashes(params);

[params2, hashes2] = read_param_file('complete_sparse_p3_corr', 'dirspec', 'params1');

assert_equals(params, params2, 'params', 'reltol', 0*1e-15, 'abstol', 0);
assert_equals(hashes, hashes2, 'hashes');

