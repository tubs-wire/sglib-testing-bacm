classdef VitamDataHandlerDeltaWing < VitamDataHandler 

    properties(Constant, Access = private)
        filters = read_delta_wing_response_data_struct();
         %filters = {...
%              'cp.x.03', 'cp.x.05', 'cp.x.07','cp.x.1', 'cp.x.13', 'cp.x.16', ...
%              'cp.x.2', 'cp.x.24', 'cp.x.28', 'cp.x.315', 'cp.x.39', 'cp.x.425',...
%              'cp.x.46', 'cp.x.505', 'cp.x.545', 'cp.x.62'};
    end

    methods(Static)
        
        function data = fromExperiment() 
            %Experimental result from the link: https://turbmodels.larc.nasa.gov/axibump_val.html
            data = VitamDataHandlerBump();
            data.read_data();
        end
    end
    
    methods    
        function read_data(data, varargin)
            options=varargin2options(varargin);
            [dirspec,options]=get_option(options, 'dirspec', 'response_delta_wing');
            check_unsupported_options(options, mfilename);
            for i = 1:length(data.filters)
                filter = data.filters{i};
                [x_i_k,y_i_k, axis] = read_filtered_responses(data.q_j_k, filter, 'model_def', 'delta_wing', 'dirspec', dirspec);
                data.add_section(filter, x_i_k, y_i_k, axis);
            end
        end
        
    end
end
