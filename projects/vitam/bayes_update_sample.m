function q_i = bayes_update_sample()

%% Load prior params and surrogate model
phase_numb = 2;
%model_descr = 'bfs';
model_descr = 'bump';
[Q, model, train_data] = generate_VITAM_gpc_surrogate(phase_numb, 'model_descr', model_descr);

%% Load experimental data
exp_data = VitamDataHandler();
exp_data.read_data()

%% Define the measurement error model
% dummy computation to get the dimensions of the usable experimental data
[~, ind] = dev_from_measurement(train_data.values(:, 1), train_data, exp_data);
% sigmas of the measurement error
sigmas = get_sigmas([0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05], exp_data, ind);
% measueremen error model
E = generate_stdrn_simparamset(sigmas);
% plot measurement model
plot_measurement_model(exp_data, E, ind)
% surrogate compute response function
Y_func = @(q)(model.compute_response(q));
% likelihood
q2likelihood = @(q)(E.pdf(dev_from_measurement(Y_func(q), train_data, exp_data)));


%% Bayesian update
% number of sampels
N = 1000;
% starting points of the random walk
q0 = Q.mean;
% Likelihood function


% Bayesian update
q_i = bayes_mcmc(q2likelihood, Q, N, [], q0, 'parallel', true, 'plot_dim', 3:5, 'plot', true, 'T', 1000);

q_peak=sample_density_peak(q_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
plot_grouped_scatter({Q.sample(N), q_i, q_peak}, ...
'Labels', Q.param_names, 'Legends', {'prior', 'posterior', 'est MAP'})
end
function [dist, ind] = dev_from_measurement(y, traind_data, exp_data)
dist =[];
ind = [];
for i = 1:exp_data.num_sections
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    t_sam = traind_data.extract_coords(i);
    y_sam = traind_data.extract_section(y, i);
    
    [ti, yi1, yi2] = common_interp(t_exp, y_exp, t_sam, y_sam, -1);
    dist_i = yi1 -yi2 ;
    dist = [dist ; dist_i];
    ind = [ind;ismember(t_exp,ti)];
end
ind = logical(ind);
end
function sigmas = get_sigmas(ratios_of_max_value, exp_data, ind)
sigmas = zeros(sum(ind),1);
last_ind = 0;
for i = 1:exp_data.num_sections
    y_exp = exp_data.extract_values(i);
    max_y = max(abs(y_exp));
    ind_i = exp_data.extract_section(ind, i);
    % the three sigma region should be the given percentage deviation
    sigmas(last_ind + 1: last_ind + sum(ind_i)) = max_y*ratios_of_max_value(i)/3;
    last_ind = last_ind + sum(ind_i);
end
end
function plot_measurement_model(exp_data, E, ind)
sigmas = sqrt(E.var);
last_ind = 0;
multiplot_init(exp_data.num_sections/2, 2)
for i = 1:exp_data.num_sections
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    ind_i = exp_data.extract_section(ind, i);
    sigmas_i = sigmas(last_ind + 1 : last_ind + sum(ind_i));
    multiplot()
    plot(t_exp, y_exp)
    hold on
    plot_between(t_exp(ind_i), y_exp(ind_i) - sigmas_i*3, y_exp(ind_i) + sigmas_i*3, [0.3, 0.6, 0.8])
    last_ind = last_ind + sum(ind_i);
end
end