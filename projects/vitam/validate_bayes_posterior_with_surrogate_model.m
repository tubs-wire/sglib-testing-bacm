function val_results = validate_bayes_posterior_with_surrogate_model()

%% Posterior samples with seperate updates
% For bfs
phase_numb = 3;
model_descr = 'bfs';
[Q, model_bfs] = generate_VITAM_gpc_surrogate(phase_numb, 'model_descr', model_descr);
[q_i_bfs, Y_func_bfs, exp_data_bfs, w_bfs]= plot_output(phase_numb, model_descr, model_bfs);
q_MAP_bfs=sample_density_peak(q_i_bfs);
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb$'), model_descr];
write_param_file(qfilename, Q, q_MAP_bfs, 'dirspec', 'update');
plot_posterior_samples(q_i_bfs, Q, q_MAP_bfs, phase_numb, model_descr)

% For bump
phase_numb = 3;
model_descr = 'bump';
[Q, model_bump] = generate_VITAM_gpc_surrogate(phase_numb, 'model_descr', model_descr);
[q_i_bump, Y_func_bump, exp_data_bump, w_bump]= plot_output(phase_numb, model_descr, model_bump);
q_MAP_bump=sample_density_peak(q_i_bump);
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb$'), model_descr];
write_param_file(qfilename, Q, q_MAP_bump, 'dirspec', 'update');
plot_posterior_samples(q_i_bump, Q, q_MAP_bump, phase_numb, model_descr)

% For delta wing
phase_numb = 3;
model_descr = 'delta_wing';
[Q, model_wing, train_data_wing, exp_data_wing] = generate_VITAM_gpc_surrogate(phase_numb, 'model_descr', model_descr);
graph_train_and_experimental_data(train_data_wing, exp_data_wing, 'wing_train_data') 
[q_i_wing, Y_func_wing, exp_data_wing, w_wing]= plot_output(phase_numb, model_descr, model_wing);
q_MAP_wing=sample_density_peak(q_i_wing);
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb$'), model_descr];
write_param_file(qfilename, Q, q_MAP_wing, 'dirspec', 'update');
plot_posterior_samples(q_i_wing, Q, q_MAP_wing, phase_numb, model_descr)

%% Posterior samples with combined updates
phase_numb = {3, 3, 3};
model_descr = {'bfs', 'bump', 'delta_wing'};
[q_i, Y_func, exp_data, w] = bayes_update_sample_of_combined_models(phase_numb, model_descr);
q_MAP=sample_density_peak(q_i);
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb{:}$'), model_descr{:}];
write_param_file(qfilename, Q, q_MAP, 'dirspec', 'update');
plot_posterior_samples(q_i, Q, q_MAP, phase_numb, model_descr)

% plot for bfs
file_name_pre =[model_descr{1}, 'combined_bayes_updated_responses'];
graph_output_with_posterior_samples(q_i, model_bfs, phase_numb{1}, model_descr{1}, file_name_pre)
% plot for bump
file_name_pre =[model_descr{2}, 'combined_bayes_updated_responses'];
graph_output_with_posterior_samples(q_i, model_bump, phase_numb{2}, model_descr{2}, file_name_pre)
% plot for delta_wing
file_name_pre =[model_descr{3}, 'combined_bayes_updated_responses'];
graph_output_with_posterior_samples(q_i, model_wing, phase_numb{3}, model_descr{3}, file_name_pre)

%% Validate update
% errors of seperate update
e_MAP_bfs = get_norm_of_error(Y_func_bfs, q_MAP_bfs, exp_data_bfs, w_bfs);
e_MAP_bump = get_norm_of_error(Y_func_bump, q_MAP_bump, exp_data_bump, w_bump);
e_MAP_wing = get_norm_of_error(Y_func_wing, q_MAP_wing, exp_data_wing, w_wing);

% errors of combined update
e_MAP = get_norm_of_error(Y_func, q_MAP, exp_data, w);
% bfs
e_MAP_c_bfs = get_norm_of_error(Y_func_bfs, q_MAP, exp_data_bfs, w_bfs);
e_MAP_c_bump = get_norm_of_error(Y_func_bump, q_MAP, exp_data_bump, w_bump);
e_MAP_c_wing = get_norm_of_error(Y_func_wing, q_MAP, exp_data_wing, w_wing);

%% store values in a struct
val_results.combined_update.q_MAP = q_MAP;
val_results.combined_update.e_MAP = e_MAP;
val_results.combined_update.e_MAP_bfs = e_MAP_c_bfs;
val_results.combined_update.e_MAP_bump = e_MAP_c_bump;
val_results.combined_update.e_MAP_wing = e_MAP_c_wing;

val_results.bfs_update.q_MAP = q_MAP_bfs;
val_results.bfs_update.e_MAP = e_MAP_bfs;
val_results.bump_update.q_MAP = q_MAP_bump;
val_results.bump_update.e_MAP = e_MAP_bump;
val_results.wing_update.q_MAP = q_MAP_wing;
val_results.wing_update.e_MAP = e_MAP_wing;

basedir = get_data_dir('update');
save([basedir, filesep, 'post_MAPs_and_errors.mat']);

end

function e_MAP_rel = get_norm_of_error(Y_func, q_MAP, exp_data, w)
y_MAP = Y_func(q_MAP);
% Weighted error of MAP estimate
err_MAP = y_MAP - exp_data;
e_MAP = err_MAP'*diag(w.^2)*err_MAP;
n_exp_MAP = exp_data'*diag(w.^2)*exp_data;
e_MAP_rel = e_MAP/n_exp_MAP;
end

function [q_i, Y_func, exp_data, w]= plot_output(phase_numb, model_descr, model)
[q_i, Y_func, exp_data, w] = bayes_update_sample_of_combined_models({phase_numb}, {model_descr});
%q_i = bayes_update_sample_with_interp(phase_numb, model_descr);
file_name_pre =[model_descr, '_bayes_updated_responses'];
filename = ['3d_bayes_posterior_samples', strvarexpand('phase$phase_numb$'), model_descr];
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
disp(filename)
graph_output_with_posterior_samples(q_i,model, phase_numb, model_descr, file_name_pre)
end

function graph_output_with_posterior_samples(q_i, model, phase_numb, model_descr, filename)
y_t = model.compute_response(q_i);
graph_experimental_and_other_response(y_t, phase_numb, model_descr, 'filename_prep', filename)
end

function plot_posterior_samples(q_i, Q, q_MAP, phase_numb, model_descr)
param_names = strrep(Q.param_names, '_',  ' ');
N =10000;
plot_grouped_scatter({Q.sample(N), q_i, q_MAP}, ...
    'Labels', param_names, 'Legends', {'prior', 'posterior', 'est MAP'})
if iscell(phase_numb)
    filename = ['bayes_posterior_samples', strvarexpand('phase$phase_numb{:}$'), model_descr{:}];
else
filename = ['bayes_posterior_samples', strvarexpand('phase$phase_numb$'), model_descr];
end
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
disp(filename)
end