classdef GPCSurrogateModel < BaseModel
    % BASEMODEL Base class for stochastic Models.
    properties
        Q@SimParamSet
        u_i_alpha
        V_u
    end
    
    methods(Static)
        function model = GPCSurrogateModel(Q, u_i_alpha, V_u)
            model.Q = Q;
            model.u_i_alpha = u_i_alpha;
            model.V_u = V_u;
        end
        
        function model = fromRegression(Q, q_j_k, u_i_k, p)
            V_u = gpcbasis_modify(Q.get_germ(), 'p', p);
            xi_j_k = Q.params2germ(q_j_k);
            phi_alpha_k = gpcbasis_evaluate(V_u, xi_j_k);
            u_i_alpha = u_i_k * pinv(phi_alpha_k);
            %norm(u_i_k - gpc_evaluate(u_i_alpha, V_u, xi_j_k))
            model = GPCSurrogateModel(Q, u_i_alpha, V_u);
        end
        
        function model = fromProjection(Q, q_j_k, u_i_k, w_k, p)
            V_u = gpcbasis_modify(Q.get_germ(), 'p', p);
            xi_j_k = Q.params2germ(q_j_k);
            
            phi_alpha_k = gpcbasis_evaluate(V_u, xi_j_k);
            u_i_alpha = u_i_k * diag(w_k) * phi_alpha_k';
            model = GPCSurrogateModel(Q, u_i_alpha, V_u);
        end
    end
    
    methods
        function n=response_dim(model)
            n = size(model.ui,1);
        end
        
        function u=compute_response(model, q_j_k)
            xi_j_k = model.Q.params2germ(q_j_k);

            u = gpc_evaluate(model.u_i_alpha, model.V_u, xi_j_k);
        end
        
        function y=compute_measurements(model, u)
            assert(false, 'Not implemented');
        end
    end
    
end