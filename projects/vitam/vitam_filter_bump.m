function [s, var, axis]=vitam_filter_bump(s, name)
% VITAM_FILTER Filters a struct given some predefined filters.
%   [S, VAR, AXIS]=VITAM_FILTER(S, NAME) Long description of vitam_filter.
%
% Example (<a href="matlab:run_example vitam_filter">run</a>)
%
% See also

%   Noemi Friedman (modified from bfs model of Elmar Zander)
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.



filters.cp   = @(s)(s.cp~=0);
filters.v_x  = @(s)(s.v_x~=0);
filters.uw_b = @(s)(s.uw_b~=0);

filters.x1  = @(s)(s.x== -0.25);
filters.x2  = @(s)(s.x== 0.688);
filters.x3  = @(s)(s.x== 0.813);
filters.x4 = @(s)(s.x==0.938);
filters.x5  = @(s)(s.x == 1);
filters.x6  = @(s)(s.x == 1.125);
filters.x7  = @(s)(s.x == 1.25);
filters.x8  = @(s)(s.x == 1.375);

filters.z0  = @(s)(s.z== 0);

% Simulation files
% x = 1, 4, 6, 10 goes with z, v_x, cp, uw_b
% z = 0 goes with cp, cf

% Experiment file
% x = 1, 4, 6, 10 goes with z, v_x, uw_b
% z = 0 goes with x, cp
% z = 0 goes with x, cf, cf_error


funs = {};
switch(name)
    case 'v_x_x-.25'
        funs={filters.v_x, filters.x1};
    case 'v_x_x.688'
        funs={filters.v_x, filters.x2};
    case 'v_x_x.813'
        funs={filters.v_x, filters.x3};
    case 'v_x_x.938'
        funs={filters.v_x, filters.x4};
    case 'v_x_x1'
        funs={filters.v_x, filters.x5};
    case 'v_x_x1.125'
        funs={filters.v_x, filters.x6};
    case 'v_x_x1.25'
        funs={filters.v_x, filters.x7};
    case 'v_x_x1.375'
        funs={filters.v_x, filters.x8};
        
    case 'uw_b_x-.25'
        funs={filters.uw_b, filters.x1};
    case 'uw_b_x.688'
        funs={filters.uw_b, filters.x2};
    case 'uw_b_x.813'
        funs={filters.uw_b, filters.x3};
    case 'uw_b_x.938'
        funs={filters.uw_b, filters.x4};
    case 'uw_b_x1'
        funs={filters.uw_b, filters.x5};
    case 'uw_b_x1.125'
        funs={filters.uw_b, filters.x6};
    case 'uw_b_x1.25'
        funs={filters.uw_b, filters.x7};
    case 'uw_b_x1.375'
        funs={filters.uw_b, filters.x8};

    case 'cp'
        funs={filters.cp, filters.z0};
        
    otherwise
        error(['Unknown filter ', strvarexpand('$name$')]);
end
for i=1:numel(funs)
    s = struct_filter(s, funs{i});
end

if startsWith(name, 'cp')
    var = 'cp';
    axis = 'x';
elseif startsWith(name, 'v_x')
    var = 'v_x';
    axis = 'z';
elseif startsWith(name, 'uw_b')
    var = 'uw_b';
    axis = 'z';
end

[~,ind] = sort(s.(axis));
s.(axis) = s.(axis)(ind);
s.(var) = s.(var)(ind);


