Q = generate_param_set();

train_data = VitamDataHandlerBump();

%% Load points 
%train_data.add_param_set('complete_qmc');
train_data.add_param_set('complete_lhs_corr');
%train_data.add_param_set('complete_sparse_p3_corr');
train_data.read_data();

%% Generate surrogate model
