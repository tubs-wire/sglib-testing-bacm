if 0
    %%
clc
s=read_exp_file();
s=vitam_filter(s, 'v_x_x4');
plot(s.z, s.v_x)

%%
s=read_exp_file();
s=vitam_filter(s, 'cp');
[s.x, s.z, s.v_x]
plot3(s.x, s.z, s.cp, '.')
grid on

%%
clf
subplot(2,1,1)
s=read_exp_file();
hold all;
%plot_field_3d(s, 'cf');
%plot_field_3d(s, 'cp');
plot_field_3d(s, 'v_x');

end
%%
subplot(2,1,2)
hash = 'fa31db764b5dcc46ad67e8b7e3c242f4dfd909ce';
s=read_response_by_hash(hash);

hold all
plot_field_3d(s, 'cf');
plot_field_3d(s, 'cp');
plot_field_3d(s, 'v_x');

