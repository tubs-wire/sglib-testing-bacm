function test_newton_interp

clf

% Show true function
x = linspace(0,10);
y0 = cos(x);
plot(x, y0)
hold all;
delta = 0.05;

% Get some (10) samples for interpolation
rand_seed(123)
n=10;
xs = [0, sort(rand(1,n-2))*max(x), 10];
ys = cos(xs);
plot(xs, ys, 'x')

%% Use recursive function
a = @(i)(dd(xs,ys,1,i));
y = neval2(a, xs, x);
assert(norm(neval2(a, xs, xs) - ys)<1e-12)
plot(x,y+1*delta)

%% Build table
dys = ys;
for i=1:n
    dys(i+1,1:n-i) = (dys(i,2:n-i+1) - dys(i,1:n-i)) ./ (xs(i+1:n) - xs(1:n-i));
end

a = dys(:,1);
y = neval2(a, xs, x);
assert(norm(neval2(a, xs, xs) - ys)<1e-12)
plot(x,y+2*delta,'--')

%% Compute "Vandermonde" matrix 
V = ones(n,1);
for i=1:n-1
    V(:,i+1) = V(:,i) .* (xs - xs(i))';
end

a = V\(ys');
y = neval2(a, xs, x);
assert(norm(neval2(a, xs, xs) - ys)<1e-12)
plot(x,y+0.1,'.')


%%

function y = neval(a, xs, x)
p = ones(size(x));
y = zeros(size(x));
for i=1:length(xs)
    y = y + a(i)*p;
    p = p .* (x - xs(i));
end

function y = neval2(a, xs, x)
n = length(xs);
y = a(n) * ones(size(x));

for i=n-1:-1:1
    y = a(i) + (x-xs(i)).*y;
end


function dy = dd(xs, ys, i1, i2)

if i1==i2
    dy = ys(i1);
else
    dy = (dd(xs, ys, i1+1, i2) - dd(xs, ys, i1, i2-1)) / (xs(i2) - xs(i1));
end




