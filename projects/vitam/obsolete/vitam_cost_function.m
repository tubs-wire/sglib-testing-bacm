function dist = vitam_cost_function(s, se, norm)

if nargin==0
    test_vitam_cost_function
    return
end
if nargin<3
    norm = 'L2';
    %norm = 'H2';
end

filters = {'v_x_x1', 'v_x_x4', 'v_x_x6', 'v_x_x10', 'uw_b_x1', 'uw_b_x4', 'uw_b_x6', 'uw_b_x10', 'cf_z0', 'cp_z0'};
dist = 0;
for i = 1:length(filters)
    dist = dist + compute_distance(s, se, filters{i}, norm);
end


function dist = compute_distance(s, se, filter, norm)

s1=vitam_filter(s, filter);
[s2, var, axis]=vitam_filter(se, filter);

t1 = s1.(axis); y1 = s1.(var);
t2 = s2.(axis); y2 = s2.(var);

switch norm
    case 'L1'; p = 1; deriv = false;
    case 'L2'; p = 2; deriv = false;
    case 'H1'; p = 1; deriv = true;
    case 'H2'; p = 2; deriv = true;
    otherwise; error('vitam:cost', 'Unknown norm');
end
dist = vector_distance(t1, y1, t2, y2, 'p', p, 'deriv', deriv);
