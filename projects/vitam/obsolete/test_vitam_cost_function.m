function test_vitam_cost_function


%se=read_exp_file();
%[params, hashes] = read_param_file('complete_qmc_corr');

data_sam = load_data({'complete_qmc_corr', 'complete_lhs_corr', 'complete_sparse_p3_corr'});
data_exp = load_data({});


function test_with_norm(norm)
for i=1:data_sam.num_samples
    dist(i) = compute_distance(i, data_sam, data_exp, norm);
end

%%
[~,ind]=sort(dist);
clf
filters = data_sam.filters;
multiplot_init(length(filters), 'ordering', 'row')
for i=1:length(filters)
    filter = filters{i};
    multiplot
    plot_data(data_sam, ind(1), filter, '-'); hold on;
    plot_data(data_sam, ind(end), filter, '-'); hold on;
    plot_data(data_exp, 1, filter, 'x'); hold off;
    legend('good', 'bad', 'true');
    1000*comp_diff(data_sam, ind(1), data_exp, filter)
    1000*comp_diff(data_sam, ind(end), data_exp, filter)
    title(data_sam.get_filter_name(filter))
    %keyboard
end
multiplot_adjust_range('axes', 'y', 'separate', 'rows')

function diff = comp_diff(data, i, datae, filter)
vg = data.values(:, i);
v = data.extract_section(vg, filter);
x = data.extract_coords(filter);
ve = datae.extract_values(filter);
xe = datae.extract_coords(filter);
diff = [...
    vector_distance(x, v, xe, ve, 'p', 1, 'deriv', false),...
    vector_distance(x, v, xe, ve, 'p', 1, 'deriv', true),...
    vector_distance(x, v, xe, ve, 'p', 2, 'deriv', false),...
    vector_distance(x, v, xe, ve, 'p', 2, 'deriv', true)];

function plot_data(data, i, filter, opts)
vg = data.values(:, i);
v = data.extract_section(vg, filter);
x = data.extract_coords(filter);
plot(x, v, opts)



function dist = compute_distance(i, datas, datae, norm)

switch norm
    case 'L1'; p = 1; deriv = false;
    case 'L2'; p = 2; deriv = false;
    case 'H1'; p = 1; deriv = true;
    case 'H2'; p = 2; deriv = true;
    otherwise; error('vitam:cost', 'Unknown norm');
end

filters = datae.filters;

vge = datae.values;
vgs = datas.values(:, i);

dist = 0;
for i = 1:length(filters)
    filter = filters{i};
    ve = datae.extract_section(vge, filter);
    xe = datae.extract_coords(filter);
    vs = datas.extract_section(vgs, filter);
    xs = datas.extract_coords(filter);

    dist = dist + vector_distance(xe, ve, xs, vs, 'p', p, 'deriv', deriv);
end



    
