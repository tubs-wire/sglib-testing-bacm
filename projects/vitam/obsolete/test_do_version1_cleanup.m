function test_do_version1_cleanup(varargin)
% TEST_DO_VERSION1_CLEANUP .

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

%disp('Really need to do that again?');
%return


clc
Q = generate_param_set();
paramfiles = {'complete_lhs', 'complete_qmc', 'complete_sparse_p3'};

for j=1:length(paramfiles)
    paramfile = paramfiles{j};
    disp(paramfile);
    
    % Read original paramfile
    [params, hashes] = read_param_file(paramfile, 'fileversion', 0);
    
    % Write corrected paramfile (with
    params_corr = params;
    params_corr(1,:) = params(1,:)/10;
    params_corr = round_params(params_corr);
    write_param_file([paramfile '_corr'], Q, params_corr)
    hashes_corr = generate_hashes(params_corr);
    
    % Reread paramfile
    params_corr2 = round_params(params_corr);
    hashes_corr2 = generate_hashes(params_corr2);
    [params_corr3, hashes_corr3] = read_param_file([paramfile, '_corr'], 'fileversion', 2, 'dirspec', 'data');
    assert(isequal(hashes_corr, hashes_corr2));
    assert(isequal(params_corr2, params_corr3));
    assert(isequal(hashes_corr2, hashes_corr3));
    
    % Now copy and rename response files
    hashes_corr = generate_hashes(params_corr);
    for i = 1:length(hashes)
        dir = get_data_dir('response');
        filename = fullfile(dir, [hashes{i}, '_v1.csv']);
        filename_corr = fullfile(dir, [hashes_corr{i}, '_v2.csv']);
        copyfile(filename, filename_corr);
    end
end
