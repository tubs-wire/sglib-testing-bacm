subplot(2,1,1)
clf
filter = 'cf_z0';
filter = 'cp_z0';
filter = 'v_x_x10';
filter = 'uw_b_x4';



hash = 'fa31db764b5dcc46ad67e8b7e3c242f4dfd909ce';
s=read_response_by_hash(hash);
[s, var, axis]=vitam_filter(s, filter)
plot(s.(axis), s.(var), 'o')

s=read_exp_file();
s=vitam_filter(s, filter)
hold all
plot(s.(axis), s.(var), 'x', 'linewidth', 2)


%%

plot_field_z(s, 4, 'cf');
%plot_field_x(s, 'cp');
%plot_field_x(s, 'v_x');


%%
subplot(2,1,2)
hash = 'fa31db764b5dcc46ad67e8b7e3c242f4dfd909ce';
s=read_response_by_hash(hash);

hold all
plot_field_3d(s, 'cf');
plot_field_3d(s, 'cp');
plot_field_3d(s, 'v_x');
