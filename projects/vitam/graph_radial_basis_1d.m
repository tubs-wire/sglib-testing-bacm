function graph_radial_basis_1d
clf

x1 = 0;
x2 = pi;
X = SimParameter('x', UniformDistribution(x1, x2));
Q = SimParamSet();
Q.add_parameter(X);


N = 6;
q = linspace(x1, x2, N);
y = cos(q);
hold off;

plot(q, y, 'x');
legend_add('exact');
hold all

surr = RBFSurrogateModel(Q, q, y, eye(N));

qp = linspace(x1, x2, 1000);

plot(qp, cos(qp), 'linewidth', 2)
legend_add('exact');
colormap(jet);

surr.set_kernel_params(0.7)
surr.normalized_kernel = true;
yp = surr.compute_response(qp);
plot(qp, yp)
legend_add('r=0.7, normalized');

surr.set_kernel_params(0.3)
surr.normalized_kernel = true;
yp = surr.compute_response(qp);
plot(qp, yp)
legend_add('r=0.3, normalized');

surr.set_kernel_params(0.2)
surr.normalized_kernel = true;
yp = surr.compute_response(qp);
plot(qp, yp)
legend_add('r=0.2, normalized');

surr.set_kernel_params(0.7)
surr.normalized_kernel = false;
yp = surr.compute_response(qp);
plot(qp, yp)
legend_add('r=0.7, unnormalized');

surr.set_kernel_params(0.2)
surr.normalized_kernel = false;
yp = surr.compute_response(qp);
plot(qp, yp)
legend_add('r=0.2, unnormalized');

