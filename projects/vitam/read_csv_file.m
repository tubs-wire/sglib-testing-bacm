function data=read_csv_file(filename, skip, varargin)
% READ_CSV_FILE Read a CSV file, caching its contents.
%   S=READ_CSV_FILE(FILENAME, SKIP) reads the CSV (comma separated value)
%   file FILENAME, skipping the the first SKIP lines. The contents is
%   stored in memory and if the file is requested again the memory contents
%   is returned (so no duplicate disc access). Note, that if you specify
%   different skip values, this is not considered by the function and you
%   would have to clear the memory manuall, see CLEAR FUNCTIONS.
%
% Example (<a href="matlab:run_example read_csv_file">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin, mfilename);
[unique_cols, options]=get_option(options, 'unique_cols', []);
check_unsupported_options(options);


persistent filename2data_map

if isempty(filename2data_map)
    filename2data_map = containers.Map();
end

if ~endsWith(filename, '.csv')
    filename = [filename, '.csv'];
end

if ~filename2data_map.isKey(filename)
    data=dlmread(filename, ',', skip, 0);
    if ~isempty(unique_cols)
        [~,ind,~]=unique(data(:,unique_cols),'rows');
        data=data(ind,:);
    end
    filename2data_map(filename) = data;
else
    data = filename2data_map(filename);
end
