function sensitivity_analysis
%% Load prior params and surrogate model
phase_numb = 2;
[Q, model, train_data] = generate_VITAM_gpc_surrogate(phase_numb);

%% Compute statistics

[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(model.u_i_alpha, model.V_u, 'max_index', 1);
[u_mean, u_var] = gpc_moments(model.u_i_alpha, model.V_u);
u_quant = (gpc_quantiles(model.V_u, model.u_i_alpha))';

%% Plot sensitivities

param_names = strrep(Q.param_names, '_', ' ');
response_names = strrep({train_data.sections.name}, '_', ' ');
response_names = strrep(response_names, 'v x', 'v_x');
response_names = strrep(response_names, 'wb', 'w_b');


for i = 1:length({train_data.sections.name})
    figure()
    % name of the response
    resp_name_i = train_data.sections(i).name;
    % spatial grid points corresponding to the response
    coord_i = train_data.extract_coords(resp_name_i);
    % partial variances
    partial_var_i = train_data.extract_section(partial_var, resp_name_i);
    % the total variance
    tot_var_i = train_data.extract_section(u_var, resp_name_i);
    % mean of the response
    mean_i = train_data.extract_section(u_mean, resp_name_i);
    % higher sensitivities
    higher_var_i = tot_var_i - sum(partial_var_i, 2);
    % name of the spatial variable
    axis_i = train_data.sections(i).axis;
    % quantiles
    quant_i = train_data.extract_section(u_quant, resp_name_i);
    
    subplot(3,1,1)
    plot_between(coord_i, quant_i(:, 1), quant_i(:, 2), [0.3, 0.6, 0.8])
    hold on
    plot(coord_i, mean_i, 'k', 'LineWidth', 2)
    legend('90% confidence region', 'mean response')
    xlabel(axis_i)
    ylabel(response_names{i});
    
    subplot(3,1,2:3)
    plot( coord_i, tot_var_i, 'k', 'LineWidth', 2 )
    hold on
    area( coord_i, [partial_var_i, higher_var_i], 'LineWidth', 1)
    
    legend('Total variance', param_names{:}, 'mixed contributions')
    xlabel(axis_i)
    ylabel(['Total and partial variances of ',response_names{i}]);
    %xlim([0,2])
    %saveas(gcf, strvarexpand('data/reports/figs/mean_and_partial_vars_of$resp_name_i$.jpg'))
end