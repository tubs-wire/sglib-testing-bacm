function rename_files_from_old_to_new_hash()

%[params, old_hashes] = read_param_file('complete_lhs', 'fileversion', 1);
%[params, old_hashes] = read_param_file('complete_qmc', 'fileversion', 1);
[params, old_hashes] = read_param_file('complete_sparse_p3', 'fileversion', 1);

postfix='_bump.csv';
params(1,:) = params(1,:)/10;
params = round_params(params);
new_hashes = generate_hashes(params);
[st1, st2] = size(old_hashes); 

cd ./data/response_bump/
for i=1:st2
    i
    system(['mv ', old_hashes{i},postfix, ' ', new_hashes{i}, postfix])
end
cd ../.. 