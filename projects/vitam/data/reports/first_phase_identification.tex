\documentclass[]{article}
\usepackage{graphicx}
%opening
\title{VITAM report}
\author{Noemi Friedman, Elmar Zander}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section{Calibration process}
\subsection{Building a surrogate model}
The first step was to start with the simplest model and make a rough calibration of the model coefficients for this specific model. Before the inversion a surrogate model was developed to avoid excessive and not affordable required computational cost. This model represents the dependence of some selected model responses on the model coefficients. This responses here highlighted were the velocity field $v_x$ and the Reynolds stresses $uw_b$ along the channel hight at four different cross sections of the channel, and the $c_p$ and the $c_f$ coefficients along the channel. Different sampling techniques were used to generate values of the model coefficients such as 
\begin{enumerate}
	\item QMC: Quasi Monte Carlo based on the Halton sequence(20 sample points);
	\item LHS: Latin hypercube sampling with random centers (20 sample points);
	\item Sparse (P=3): A Smolyak grid with 3 stages has been generated (61 sample points).
\end{enumerate}
These samples were generated from a 'non-informative' prior distribution of the model coefficients, a uniform distribution in between the bounds given in Table~\ref{table: prior_params}.\\
\begin{table}
\begin{center}
\begin{tabular}{c c c c c c}\label{Tab: prior_params}
&PG	& SARC\_C1 & SARC\_C2  & SARC\_C3  & SAS \\ \hline \hline
left boundary &0	&0 &0 & 0  & 0 \\ 
right boundary &0.8& 1.5	& 10 & 1.5  & 7 \\
\end{tabular}
\caption{Initial intervals of model coefficients}
\label{table: prior_params}
\end{center}
\end{table}
The responses have been computed by the simulation model for all generated coefficient values. For generating the surrogate models either one or two parameter sets have been used to train the model (training data), while the others were used to validate the model (test data), i.e.\ compute $L_1$ and $L_{\infty}$ errors.
\footnote{Computation of $L_2$ based on Smolyak weights was numerically not stable and is therefore not reported.}

For the representation of the surrogate models two different representations were chosen:
\begin{enumerate}
	\item GPC (generalized polynomial chaos) multivariate orthogonal
	polynomials of maximum total degree $p$. Here from $p=1$, i.e.\
	linear, to $p=3$, i.e. cubic polynomials. The orthogonal polynomials
	w.r.t.\ the uniform distribution are the Legendre polynomials.
	\item NRBF Normalized radial basis functions are basis functions that
	are located at some so-called centers and whose values only depend
	on the distance to those centers. Here centers have been chosen to
	correspond to the nodes of the training data and kernel functions
	are Gaussian with varying parameter $r$, i.e. $g(d) =
	\exp(-(d/r)^2)$ and $d=\|x-x_i\|$.
\end{enumerate}

To generate the surrogate model representations from the training data the following methods were employed:
\begin{itemize}
	\item GPC interpolation: standard GPC interpolation if number of samples equals number of GPC basis functions;
	Note that for lower order GPCs this turns into regression and for higher order GPCs shows non-uniqueness;
	\item GPC projection: standard GPC projection on orthogonal basis polynomials. 
	For QMC and LHS training data the uniform weights have been chosen and for the sparse 
	\item NRBF interpolation: standard RBF interpolation with the normalized Gaussian kernel;
	\item NRBF distance weighting: the NRBF kernel is used for distance weighting (giving more weights to close-by training nodes and less weight for those further away)
	\item NRBF $L_\infty$ approximation: newly developed method with guaranteed positive weights (to avoid numberical instabilities) and best $L_\infty$ approximation.
\end{itemize}

Results of the different approximation methods using different parameters and using different data sets for training and validation data have been compared and evaluated. It was concluded that the for further analysis two surrogates may be used:
\begin{itemize}
\item the NRBF surrogate with kernel width 0.7 generated with the normalized Gaussian kernel;
\item The GPC with first degree polynomials.
\end{itemize}
For the final surrogate all 101 sample points were used and the surrogate was derived from interpolation in the case of the NRBF surrogate and regression in the case of the GPC model.\\

\subsection{Sensitivity analysis}
Using the surrogate model a GPC based Sobol sensitivity analysis was carried out from the linear model. This analysis showed that all highlighted responses vary mainly due to the variation of the SAS coefficients and are least sensitive to the variation of the SARC\_C1 coefficient (markable variations are only noticeable for the Reynolds stress values). The sensitivities on the variations of the other three coefficients were of similar magnitude. The Reynold stress and the velocity were more sensitive on variations of the $PG$ coefficient in the first part of the channel and the effects of the SARC\_C2 and SARC\_C3 coefficients were more emphasized around the end of the channel. The partial variances of some of the responses are shown in Figure~\ref{fig: sensitivities}.\\
\begin{figure}
\includegraphics[width=\textwidth]{./figs/cf}
\includegraphics[width=\textwidth]{./figs/CP}
\includegraphics[width=\textwidth]{./figs/uw_x1}
\includegraphics[width=\textwidth]{./figs/uw_x6}
\caption{Sobol partial variances of the $c_f$ and $c_p$ coefficients along the channel and the $uw_b$ stresses along the channel height(the top of the channel is cut due to the ignorable variances in this region)}
\label{fig: sensitivities}
\end{figure}

\subsection{Rough calibration and refinement}
Using the derived NRBF surrogate model a first rough calibration was carried out to try to fit the responses to the experimental data. This rough calibration was done by using cost functions based on four different norms of the deviations of the experimental responses and the computed responses. The $L_1$, $L_2$, $W_{11}$ and $W_{21}$ norms were weighted --- to get error terms of similar magnitude --- and finally summed up. The results of this first rough calibration process is shown in table~\ref{table:rough_calibration}\\
\begin{table}
\begin{center}
\begin{tabular}{ccccc c c}
norm&PG	& SARC\_C1 & SARC\_C2  & SARC\_C3  & SAS \\ \hline \hline
 $L_1$&0.372&0.717&0.002&0.440&1.379\\
$L_2$&0.320&0.9419&0.848&0.2875&1.310\\
$W_{11}$&0.493&0.810&2.978&0.000&1.229\\
$W_{21}$&0.283&0.727&8.172&0.506&1.710
\end{tabular}
\caption{First calibration of model coefficients using cost functions based on different norms}
\label{table:rough_calibration}
\end{center}
\end{table}
As a next step the surrogate model should be refined in the important regions of the coefficient space by generating a second set of QMC samples sampled around this four calibrated points. For this sampling procedure beta distributions were used centered around the calibrated points with intervals 10 times smaller then original prior intervals. 



\end{document}
