\documentclass[12pt]{article}

\usepackage[a4paper,margin=3em]{geometry}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{graphicx}

\let\oldpar=\paragraph
\renewcommand*\paragraph[1]{\oldpar{#1}\leavevmode\\[1ex]}

\newcommand\incplot[1]{\includegraphics[width=0.5\textwidth]{../images/#1}}

\title{Short report on surrogate modelling for the ViTAM-Cal project}
\author{Elmar Zander\\Inst. of Scientific Comuting, TU Braunschweig}
\date{\today}

\begin{document}
\maketitle
\section{Surrogate model generation}

\subsection{Short method description}

\paragraph{Data generation and use}
The 5d parameter values have been computed by the methods listed below.
The responses have been computed by the IFS for the generated parameter values and returned to WiRe.
The identification of parameters and response files is done via the SHA1 (secure hash algorithm 1) based on the parameter values rounded to 13 decimal digits.
For generating the surrogate models either one or two parameter sets have been used to train the model (training data), while the others were used to validate the model (test data), i.e.\ compute $L_1$ and $L_{\infty}$ errors.
\footnote{Computation of $L_2$ based on Smolyak weights was numerically not stable and is therefore not reported.}
\begin{enumerate}
\item QMC: Quasi Monte Carlo based on the Halton sequence (next time maybe Sobol sequence should be tested)
\item LHS: Latin hypercube sampling with random centers.
\item Sparse (P=3): A Smolyak grid with 3 stages has been generated.
\end{enumerate}

\paragraph{Model representations}
For the representation of the surrogate models two different representations were chosen:
\begin{enumerate}
\item GPC (generalized polynomial chaos) multivariate orthogonal
  polynomials of maximum total degree $p$. Here from $p=1$, i.e.\
  linear, to $p=3$, i.e. cubic polynomials. The orthogonal polynomials
  w.r.t.\ the uniform distribution are the Legendre polynomials.
\item NRBF Normalized radial basis functions are basis functions that
  are located at some so-called centers and whose values only depend
  on the distance to those centers. Here centers have been chosen to
  correspond to the nodes of the training data and kernel functions
  are Gaussian with varying parameter $r$, i.e. $g(d) =
  \exp(-(d/r)^2)$ and $d=\|x-x_i\|$.
\end{enumerate}

\paragraph{Approximation methods}
Short description of the methods employed to generate the surrogate model representations from the training data:
\begin{enumerate}
\item GPC interpolation: standard GPC interpolation if number of samples equals number of GPC basis functions. 
Note that for lower order GPCs this turns into regression and for higher order GPCs shows non-uniqueness.
\item GPC projection: standard GPC projection on orthogonal basis polynomials. 
For QMC and LHS training data the uniform weights have been chosen and for the sparse 
\item NRBF interpolation: standard RBF interpolation with the normalized Gaussian kernel.
\item NRBF distance weighting: the NRBF kernel is used for distance weighting (giving more weights to close-by training nodes and less weight for those further away)
\item NRBF $L_\infty$ approximation: newly developed method with guaranteed positive weights (to avoid numberical instabilities) and best $L_\infty$ approximation.
\end{enumerate}

\subsection{Results}  

Results of the different approximation methods using different parameters and using different data sets for training and validation data have been summarised in the following table.
The corresponding plots for the $c_p$ 

\begin{table}[H]
\centering
\begin{tabular}{lllS[table-format=3.2]S[table-format=3.2]}
  \toprule
  Method (parameters) & Training data & Test data & {$L_1$ error} &
                                                                  {$L_{\infty}$
                                                                  error}
  \\
  \midrule
  GPC interpolation (p=1) & LHS & QMC & 11.7 & 0.127\\
GPC interpolation (p=2) & LHS & QMC & 136 & 1.27\\
GPC interpolation (p=3) & LHS & QMC & 275 & 1.11\\
GPC interpolation (p=1) & QMC & LHS & 11.1 & 0.117\\
GPC interpolation (p=2) & QMC & LHS & 85 & 0.72\\
GPC interpolation (p=3) & QMC & LHS & 237 & 1.56\\
GPC projection (p=1) & QMC & LHS & 141 & 0.56\\
GPC projection (p=2) & QMC & LHS & 161 & 1.02\\
GPC projection (p=1) & Sparse (P=3) & LHS & 18.4 & 0.182\\
GPC projection (p=1) & Sparse (P=3) & QMC & 13.7 & 0.169\\
GPC projection (p=1) & Sparse (P=3) & LHS+QMC & 16 & 0.182\\
GPC projection (p=2) & Sparse (P=3) & LHS & 24.1 & 0.336\\
GPC projection (p=3) & Sparse (P=3) & LHS & 34.1 & 0.544\\
NRBF interpolation (r=0.6) & QMC & LHS & 13.4 & 0.107\\
NRBF interpolation (r=1) & QMC & LHS & 11.8 & 0.11\\
NRBF interpolation (r=1.6) & QMC & LHS & 10.2 & 0.121\\
NRBF interpolation (r=0.6) & LHS & QMC & 13.1 & 0.139\\
NRBF interpolation (r=0.6) & LHS+QMC & Sparse (P=3) & 8.75 & 0.0934\\
NRBF interpolation (r=0.6) & Sparse (P=3) & LHS+QMC & 10.1 & 0.107\\
NRBF distance weighting (r=0.6) & LHS+QMC & Sparse (P=3) & 9.01 & 0.0935\\
NRBF distance weighting (r=1) & LHS+QMC & Sparse (P=3) & 10.8 & 0.0831\\
NRBF distance weighting (r=1.6) & LHS+QMC & Sparse (P=3) & 15.7 & 0.0666\\
NRBF $L_\infty$ approx (r=0.6) & QMC & LHS & 13.4 & 0.107\\
NRBF $L_\infty$ approx (r=0.6) & Sparse (P=3) & LHS+QMC & 12.7 & 0.122\\
  \bottomrule
\end{tabular}
\caption{Surrogate models and approximation errors}
\end{table}


\subsection{Plots}

The following plots show the approximation quality of different surrogate models for different samples of the test/validation set.
\begin{enumerate}
\item The blue line is the sample from the test set, that should be approximated by the surrogate model.
\item The red line is the actual prediction by the surrogate model.
Should be as close as possible to the blue line; have no or only little overshoots.
\item Grey area (top) indicates the range (min,max) of the training data. 
For stable approximation methods the red line should stay within this range.
\item Grey area (bottom) absolute difference between red and blue line.
Added because in areas of steep gradient this may be difficult to see and thus visually misleading.
\end{enumerate}

\noindent
%\incplot{gpc_interpolation_p_1_lhs_qmc}
%\incplot{gpc_interpolation_p_2_lhs_qmc}
%\incplot{gpc_interpolation_p_3_lhs_qmc}
%\incplot{gpc_interpolation_p_1_qmc_lhs}
%\incplot{gpc_interpolation_p_2_qmc_lhs}
%\incplot{gpc_interpolation_p_3_qmc_lhs}
%\incplot{gpc_projection_p_1_qmc_lhs}
%\incplot{gpc_projection_p_2_qmc_lhs}
%\incplot{gpc_projection_p_1_sparse_p_3_lhs}
%\incplot{gpc_projection_p_1_sparse_p_3_qmc}
%\incplot{gpc_projection_p_1_sparse_p_3_lhs+qmc}
%\incplot{gpc_projection_p_2_sparse_p_3_lhs}
%\incplot{gpc_projection_p_3_sparse_p_3_lhs}
%\incplot{nrbf_interpolation_r_0p6_qmc_lhs}
%\incplot{nrbf_interpolation_r_1_qmc_lhs}
%\incplot{nrbf_interpolation_r_1p6_qmc_lhs}
%\incplot{nrbf_interpolation_r_0p6_lhs_qmc}
%\incplot{nrbf_interpolation_r_0p6_lhs+qmc_sparse_p_3}
%\incplot{nrbf_interpolation_r_0p6_sparse_p_3_lhs+qmc}
%\incplot{nrbf_distance_weighting_r_0p6_lhs+qmc_sparse_p_3}
%\incplot{nrbf_distance_weighting_r_1_lhs+qmc_sparse_p_3}
%\incplot{nrbf_distance_weighting_r_1p6_lhs+qmc_sparse_p_3}

\end{document}
