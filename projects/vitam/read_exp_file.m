function s=read_exp_file(varargin)
% READ_EXP_FILE Read file with experimental data.
%
% Example (<a href="matlab:run_example read_exp_file">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options=varargin2options(varargin, mfilename);
[as_matrix, options]=get_option(options, 'as_matrix', false);
[setup, options]=get_option(options, 'setup', 'bfs');
check_unsupported_options(options);


switch setup
    case {'bfs', 'bfs1'}
        base = 'exp_mod';
        skip_lines = 1;
        dir = get_data_dir('response');
    case {'bump'}
        base = 'bump_exp_n';
        skip_lines = 1;
        dir = get_data_dir('response_bump');
    case 'delta_wing'
        base = 'delta_wing_exp';
        skip_lines = 1;
        dir = get_data_dir('response_delta_wing');
    otherwise
        base = [setup '_exp'];
        skip_lines = 1;
        dir = get_data_dir('response');
end
filename = fullfile(dir, base);

s = read_vitam_csv_file(filename, skip_lines, ...
    'as_matrix', as_matrix, 'setup', setup);
