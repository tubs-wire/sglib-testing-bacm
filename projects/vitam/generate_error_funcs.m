function [r_i_k, sigma_k]=generate_error_funcs(phase_numb, model_descr)

% Load experimental and training data
[train_data, exp_data] = load_train_and_exp_data(phase_numb, 'model_descr', model_descr);
[dist, ind] = dev_from_measurement(train_data.values, train_data, exp_data);

%% Do POD on distances
dist_mean=mean(dist,2);
dist_fluct = binfun(@minus, dist, dist_mean);
[r_i_k, sigma_k]=svd(dist_fluct*dist_fluct');
sigma_k = diag(sigma_k);
plot(sigma_k(1:15), 'x', 'LineWidth', 3)
ylabel('\sigma_k');
xlabel('k')
basedir = get_data_dir('images');
save_png(gcf, 'errors_singular_values', 'figdir', basedir)

% plot basis functions
n_eig = 3;
sigma_k = [1;sigma_k(1:n_eig)];
r_i_k = [dist_mean, r_i_k(:,1:n_eig)];
r_i_k_ext = NaN(size(exp_data.values, 1), n_eig+1);
r_i_k_ext(ind, :) = r_i_k;
graph_experimental_and_other_response({r_i_k_ext, exp_data, ind}, phase_numb, model_descr, 'filename_prep','model_error_eigenfunctions')

function [dist, ind] = dev_from_measurement(y, train_data, exp_data)
dist =[];
ind = logical([]);
for i = 1:exp_data.num_sections
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    t_sam = train_data.extract_coords(i);
    y_sam = train_data.extract_section(y, i);
    
    [ti, yi1, yi2] = common_interp(t_exp, y_exp, t_sam, y_sam, -1);
    dist_i = yi1 -yi2 ;
    dist = [dist ; dist_i];
    ind = [ind;ismember(t_exp,ti)];
end