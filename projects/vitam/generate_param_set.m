function Q=generate_param_set(varargin)
% GENERATE_PARAM_SET Generate a paramset for the vitam project.
%
% Example (<a href="matlab:run_example generate_param_set">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin, mfilename);
[version, options]  = get_option(options, 'version', 2);
check_unsupported_options(options);

if version==1
    PG = MySimParameter('PG', UniformDistribution(0, 8));
elseif version==2
    PG = MySimParameter('PG', UniformDistribution(0, 0.8));
elseif version==3
    PG = MySimParameter('PG', UniformDistribution(0, 10));
end
    
c1 = MySimParameter('SARC_C1', UniformDistribution(0, 1.5));
c2 = MySimParameter('SARC_C2', UniformDistribution(0, 10));
c3 = MySimParameter('SARC_C3', UniformDistribution(0, 1.5));
SAS = MySimParameter('SAS', UniformDistribution(0, 7));

Q = MySimParamSet();
Q.add_parameter(PG, c1, c2, c3, SAS);
