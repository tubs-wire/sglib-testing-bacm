function graph_sample_points_1()
Colors = set_my_favorite_colors();

%%
Q = generate_param_set();
param_names = strrep(Q.param_names, '_',  ' ');


qmc_data = VitamDataHandler();
qmc_data.add_param_set('complete_qmc_corr');
qmc_data.read_data()
q_qmc = qmc_data.params();

lhs_data = VitamDataHandler();
lhs_data.add_param_set('complete_lhs_corr');
lhs_data.read_data()
q_lhs = lhs_data.params();

grid_data = VitamDataHandler();
grid_data.add_param_set('complete_sparse_p3_corr');
grid_data.read_data()
q_grid = grid_data.params();

%% Plot sample points and circle the ones with the best fitting response
[q_L1, q_L2, q_W11, q_W21]=best_results_for_vitam_param_value1;

plot_grouped_scatter({q_qmc, q_lhs, q_grid, q_L1(:,2), q_L2(:,2), q_W11(:,2), q_W21(:,2)}, ...
    'Legends', {'qmc', 'lhs', 'smolyak','best L1', 'best L2', 'best W11', 'best W21'}, 'Labels', param_names, ...
    'MarkerType', '..xoooo', ...
    'Color', {Colors.cadet_blue, Colors.dark_green, Colors.bordeau, Colors.navy_blue, Colors.lavender, Colors.light_pink, Colors.greenish_grey});
set(gcf, 'units','pixel','innerposition',[0 0 1000 1000])

basedir = get_data_dir('images');
filename = 'sample_points_1';
save_png(gcf, filename, 'figdir', basedir, 'notitle', false)
disp(filename)

%% Plot points with showing the best point according to the surrogate model
figure

plot_grouped_scatter({train_data.params, q_L1(:,1), q_L2(:,1), q_W11(:,1), q_W21(:,1)}, ...
    'Legends', {'samples','best L1', 'best L2', 'best W11', 'best W21'}, 'Labels', param_names, ...
    'MarkerType', '.oooo', ...
    'Color', {Colors.bordeau, Colors.navy_blue, Colors.lavender, Colors.light_pink, Colors.greenish_grey});
set(gcf, 'units','pixel','innerposition',[0 0 1000 1000])


filename = 'sample_points_1_and_best_from_surr';
save_png(gcf, filename, 'figdir', basedir, 'notitle', false)
disp(filename)

%% Plot new sample points
norm ='L1';
filename = ['qmc_', norm, '_second_run'];
q_L1 = read_param_file(filename, 'dirspec', 'data');

norm ='L2';
filename = ['qmc_', norm, '_second_run'];
q_L2 = read_param_file(filename, 'dirspec', 'data');

norm ='W11';
filename = ['qmc_', norm, '_second_run'];
q_W11 = read_param_file(filename, 'dirspec', 'data');

norm ='W21';
filename = ['qmc_', norm, '_second_run'];
q_W21 = read_param_file(filename, 'dirspec', 'data');

figure

plot_grouped_scatter({train_data.params, q_L1, q_L2, q_W11, q_W21}, ...
    'Legends', {'init samples','L1', 'L2', 'W11', 'W21'}, 'Labels', param_names, ...
    'MarkerType', '.....', ...
    'Color', {Colors.grey, Colors.navy_blue, Colors.lavender, Colors.light_pink, Colors.greenish_grey});
set(gcf, 'units','pixel','innerposition',[0 0 1000 1000])


filename = 'old_and_new_sample_points';
save_png(gcf, filename, 'figdir', basedir, 'notitle', false)
disp(filename)
