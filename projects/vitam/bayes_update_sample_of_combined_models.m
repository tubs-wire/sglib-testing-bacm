function [q_i, Y_func, exp_data_c, w] = bayes_update_sample_of_combined_models(phase_numb, model_descr,p_gpc)

%% initiation
%phase_numb = {3, 3, 3};
%model_descr = {'bfs', 'bump', 'delta_wing'};
%assert((size(phase_numb)== size(p_gpc)))
%% Load prior params and surrogate model
n_model = length(model_descr);
model = cell(n_model,1);
exp_data = cell(n_model,1);
ind = cell(n_model,1);

for i=1:n_model
    [Q, model{i}, ~, exp_data{i}, ind{i}] = generate_VITAM_gpc_surrogate(phase_numb{i}, p_gpc{i}, ...
        'model_descr', model_descr{i}, 'flag_interp', true);
end
%% Define the measurement error model
% define percentages for std of error
sigma = [];
w = [];
w_i = cell(n_model,1);
for i=1:n_model
    switch model_descr{i}
        case 'bump'
            err_ratio_i = [repmat(0.05, 1, 8), repmat(0.1, 1, 9)];
        case 'bfs'
            err_ratio_i = [0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05];
        case 'delta_wing'
            err_ratio_i = repmat(0.1, 1, 16);
        case 'delta_wing_25'
            err_ratio_i = repmat(0.1, 1, 16);
        otherwise
            error(strvarexpand('no $model_descr$ model is implemented'))
    end
    % sigmas of the measurement error
    [sigma_i,w_i{i}] = get_sigmas_and_weights(err_ratio_i, exp_data{i}, ind{i});
    sigma = [sigma; sigma_i];
    w = [w; w_i{i}];
    % measueremen error model
    %E{i} = generate_stdrn_simparamset(sigmas_i);
end    
E = generate_stdrn_simparamset(sigma);

%% Bayesian update
% surrogate compute response function
Y_func = @(q)(compute_model_response(model, q));

exp_data_c = combine_exp_data(exp_data, ind);
% likelihood
%q2likelihood = @(q)(E.pdf(binfun(@minus, Y_func(q), exp_data.values(ind))));
% loglikelihood
q2loglikelihood = @(q)(E.logpdf(binfun(@minus, Y_func(q), exp_data_c)));
% number of sampels
N = 10000;
% number of parallel walks
N_b= 1000;
T = ceil(N/N_b);
% starting points of the random walk
q0 = Q.mean;
% Likelihood function
prior_vars=Q.var;
P=generate_stdrn_simparamset(sqrt(prior_vars/10));
%P=generate_stdrn_simparamset(sqrt(prior_vars/5));
% Bayesian update
%q_i = bayes_mcmc(q2likelihood, Q, N, P, q0, 'parallel', true, 'plot_dim', 3:5, 'plot', true, 'T', 1000);
figure(1)
q_i = bayes_mcmc(q2loglikelihood, Q, N, P, q0, 'parallel', true, ...
    'plot_dim', 3:5, 'plot', true, 'T', T, 'log_flag', true, 'T_burn', 1500);
filename = ['3d_posterior_samples',sprintf('%d', cell2mat(phase_numb)),'_', model_descr{:},'_p_gpc',sprintf('%d', cell2mat(p_gpc))];
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
q_MAP=sample_density_peak(q_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
qfilename = ['post_MAP_phase',sprintf('%d', cell2mat(phase_numb)),'_', model_descr{:},'_p_gpc',sprintf('%d', cell2mat(p_gpc))];
write_param_file(qfilename, Q, q_MAP, 'dirspec', 'update');
figure(2)
param_names = strrep(Q.param_names, '_',  ' ');
plot_grouped_scatter({Q.sample(N), q_i, q_MAP}, ...
    'Labels', param_names, 'Legends', {'prior', 'posterior', 'est MAP'})
filename = ['posterior_samples_phase', sprintf('%d', cell2mat(phase_numb)),'_', model_descr{:},'_p_gpc', sprintf('%d', cell2mat(p_gpc))];
%basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)

end

function [sigmas,w] = get_sigmas_and_weights(ratios_of_max_value, exp_data, ind)
sigmas = zeros(sum(ind),1);
w = zeros(sum(ind),1);
last_ind = 0;
for i = 1:exp_data.num_sections
    y_exp = exp_data.extract_values(i);
    max_y = max(abs(y_exp));
    ind_i = exp_data.extract_section(ind, i);
    % the three sigma region should be the given percentage deviation
    sigmas(last_ind + 1: last_ind + sum(ind_i)) = max_y*ratios_of_max_value(i)/3;
    w(last_ind + 1: last_ind + sum(ind_i)) = 1/max_y;
    last_ind = last_ind + sum(ind_i);
end
end

function Y=compute_model_response(models, q)
Y = [];
for i = 1: length(models)
    Y_i = models{i}.compute_response(q);
    Y = [Y; Y_i];
end
end

function exp_data_c = combine_exp_data(exp_data, ind)
exp_data_c = [];
for i = 1: length(exp_data)
    exp_data_c_i = exp_data{i}.values(ind{i});
    exp_data_c= [exp_data_c; exp_data_c_i];
end
end