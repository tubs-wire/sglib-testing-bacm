function plot_field_3d(s, field)
s=struct_filter(s, @(s)(s.(field)~=0));
%[s.x, s.z, s.(field)]
plot3(s.x, s.z, s.(field), '.')
grid on
view(3)