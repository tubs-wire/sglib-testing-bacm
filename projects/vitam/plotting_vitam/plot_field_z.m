function plot_field_z(s, x, field)
s=struct_filter(s, @(s)(s.x == x & s.(field)~=0));
%[s.x, s.z, s.(field)]
plot(s.z, s.(field), '.')
grid on
