function hashes=generate_hashes(values, varargin)
% GENERATE_HASHES Generates hash values for set of values.
%
% Example (<a href="matlab:run_example generate_hashes">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin, mfilename);
[nocell, options]  = get_option(options, 'nocell', false);
check_unsupported_options(options);

N = size(values,2);
if nocell
    assert(N==1, '''nocell'' option works only for *one* value');
    hashes = hash_data(values, 'sha1');
else
    hashes = cell(1,N);
    for j=1:N
        hashes{j} = hash_data(values(:,j), 'sha1');
    end
end
