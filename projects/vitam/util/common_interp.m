function [ti, yi1, yi2, ind] = common_interp(t1, y1, t2, y2, where)
% COMMON_INTERP Does an interpolation of data on overlapping regions.
%   [TI, YI1, YI2] = COMMON_INTERP(T1, Y1, T2, Y2, WHERE) first computes
%   the intersection of the regions where the data is defined from T1 and
%   T2, given a maximal interval [TA, TB] which is included in both T1 and
%   T2 and does interpolation of Y1 and Y2 on this common region. 
%   The exact values that are returned depends on the parameter WHERE:
%
%     WHERE=0: TI contains TA and TB and YI1 and YI2 contain piecewise
%     polynomials (PP) that can be evaluated anywhere in [TA, TB].
%
%     WHERE=-1: TI contains all the points of T1 that are in [TA, TB], YI1
%     contains the corresponding points from Y1 and, YI2 contains the
%     values of Y2 interpolated on TI.
%
%     WHERE=-2: TI contains all the points of T2 that are in [TA, TB], YI2
%     contains the corresponding points from Y2 and, YI1 contains the
%     values of Y1 interpolated on TI.
%
%     WHERE>0: TI contains linearly spaced points in [TA, TB] (i.e.
%     linspace). YI1 and YI2 contain the interpolated of Y1 and Y2,
%     respectively, onto TI.
% 
%
% Example (<a href="matlab:run_example common_interp">run</a>)
%
% See also MKPP, PPVAL, PPDER, PPINT

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


if nargin<5
    where = 100;
end

ta = max(min(t1),min(t2));
tb = min(max(t1),max(t2));

if where==0
    ti = [];
elseif where==-1
    ind = ta<=t1 & t1<=tb;
    ti = t1(ind);
elseif where==-2
    ind = ta<=t2 & t2<=tb;
    ti = t2(ind);
else
    ti = linspace(ta, tb, where);
end

method = 'pchip';
if isempty(ti)
    yi1 = interp1(t1, y1, method, 'pp'); %#ok<*INTRPP>
    yi2 = interp1(t2, y2, method, 'pp');
    ti = [ta, tb];
else
    yi1 = interp1(t1, y1, ti, method);
    yi2 = interp1(t2, y2, ti, method);
end
