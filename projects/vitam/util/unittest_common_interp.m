function unittest_common_interp(varargin)
% UNITTEST_COMMON_INTERP Test the COMMON_INTERP function.
%
% Example (<a href="matlab:run_example unittest_common_interp">run</a>)
%   unittest_common_interp
%
% See also COMMON_INTERP, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'common_interp' );


rand_seed(987);

t1 = sort([0.2; uniform_sample(20, 0.2, 3); 3]);
t2 = sort([0.4; uniform_sample(20, 0.4, 3.2); 3.2]);
y1 = sin(t1);
y2 = cos(t2);

[ti, yi1, yi2] = common_interp(t1, y1, t2, y2, -1);
assert_true(min(ti)>=0.4, 'ta1');
assert_equals(max(ti), 3, 'tb1');
assert_equals(yi1, sin(ti), 'exact1')
assert_equals(yi2, cos(ti), 'interp2', 'abstol', 2e-2)


[ti, yi1, yi2] = common_interp(t1, y1, t2, y2, -2);
assert_equals(min(ti), 0.4, 'ta2');
assert_true(max(ti)<=3, 'tb2');
assert_equals(yi1, sin(ti), 'interp1', 'abstol', 1e-2)
assert_equals(yi2, cos(ti), 'exact2')


[ti, yi1, yi2] = common_interp(t1, y1, t2, y2, 30);
assert_equals(ti, linspace(0.4, 3, 30), 'ti');
assert_equals(yi1, sin(ti), 'interp1', 'abstol', 1e-2)
assert_equals(yi2, cos(ti), 'interp2', 'abstol', 2e-2)


[ti, yi1, yi2] = common_interp(t1, y1, t2, y2, 0);
assert_equals(ti, [0.4,3], 'ti');
assert_equals(ppval(yi1, ti), sin(ti), 'ppval1', 'abstol', 1e-2)
assert_equals(ppval(yi2, ti), cos(ti), 'ppval2', 'abstol', 2e-2)







