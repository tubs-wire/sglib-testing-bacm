function unittest_generate_hashes(varargin)
% UNITTEST_GENERATE_HASHES Test the GENERATE_HASHES function.
%
% Example (<a href="matlab:run_example unittest_generate_hashes">run</a>)
%   unittest_generate_hashes
%
% See also GENERATE_HASHES, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'generate_hashes' );

data1 = [1,2,3];
h1 = generate_hashes(data1);
assert_equals(h1{1}, hash_data(data1(1), 'sha1'), 'd11')
assert_equals(h1{3}, hash_data(data1(3), 'sha1'), 'd13')

h2 = generate_hashes(data1(2), 'nocell', true);
assert_equals(h2, h1{2}, 'd2')


