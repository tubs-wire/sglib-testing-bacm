function [s, ind]=struct_filter(s, fun)
% STRUCT_FILTER Applies a filter to a struct of arrays.
%   [S, IND]=STRUCT_FILTER(S, FUN) applies FUN to which should return either a
%   logical array of indices or of integer indices (e.g. FUN=@(S)(S.x>0) to
%   filter only values relating to x values larger 0). The returned S
%   consists than of all the fields in the orginal S, with only indices in
%   IND retained, where IND is FUN(S).
%
% Example (<a href="matlab:run_example struct_filter">run</a>)
%   [X,Y] = meshgrid(linspace(0,6,20));
%   s = struct('x', X, 'y', Y, 'z', sin(2*X).*cos(3*Y));
%   sn1 = struct_filter(s, @(s)(s.y==0));
%   sn2 = struct_filter(s, @(s)(s.y>0.3 & s.y<0.4));
%   plot(sn1.x, sn1.z, sn2.x, sn2.z);
%
% See also STRUCT

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


ind = fun(s);
fnames = fieldnames(s);
for j = 1:length(fnames)
    s.(fnames{j}) = s.(fnames{j})(ind);
end
