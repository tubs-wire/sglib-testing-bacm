function unittest_struct_filter(varargin)
% UNITTEST_STRUCT_FILTER Test the STRUCT_FILTER function.
%
% Example (<a href="matlab:run_example unittest_struct_filter">run</a>)
%   unittest_struct_filter
%
% See also STRUCT_FILTER, MUNIT_RUN_TESTSUITE 

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

munit_set_function( 'struct_filter' );

[X,Y] = meshgrid(linspace(0,6,20));
s = struct('x', X, 'y', Y, 'z', sin(2*X).*cos(3*Y));
sn1 = struct_filter(s, @(s)(s.y==0));
assert_equals(sn1.y, zeros(20,1), 'filter1y');
assert_equals(sn1.z, sin(2*sn1.x), 'filter1xz');

sn2 = struct_filter(s, @(s)(s.y>0.3 & s.y<0.4));
assert_equals(sn2.z, sin(2*sn2.x)*cos(3*sn2.y(1)), 'filter2xz');
assert_true(all(sn2.y>0.3 & sn2.y<0.4), [], 'filter_matched');
