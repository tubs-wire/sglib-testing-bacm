function [mean_err, err_j] = mean_prediction_error(model, q_i_j, y_i_j)

p = 2;
yp_i_j = model.compute_response(q_i_j);
err_j = sum( abs(yp_i_j - y_i_j).^p, 1).^(1/p);
mean_err = mean(err_j);