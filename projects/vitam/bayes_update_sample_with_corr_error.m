function qEM_i = bayes_update_sample_with_corr_error()

%% Load prior params and surrogate model
phase_numb = 1;
model_descr = 'bump';
%model_descr = 'bfs';
[Q, model, train_data, exp_data, ind] = generate_VITAM_gpc_surrogate(phase_numb, ...
    'model_descr', model_descr, 'flag_interp', true);

%% Define the measurement error model
% define percentages for std of error
switch model_descr
    case 'bump'
        err_ratio = [repmat(0.05, 1, 8), repmat(0.1, 1, 9)];
    case 'bfs'
        err_ratio = [0.05, 0.05, 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.05];
    otherwise
        error()
end

% sigmas of the measurement error
sigmas = get_sigmas(err_ratio, exp_data, ind);
% measueremen error model
E = generate_stdrn_simparamset(sigmas);
% plot measurement model
plot_measurement_model(exp_data, E, ind)
% surrogate compute response function
Y_func = @(q)(model.compute_response(q));
% modeling error model
[r_i_k, sigma_k]=generate_error_funcs(phase_numb, model_descr);
EM = generate_stdrn_simparamset(sigma_k*0.01);
%EM = generate_stdrn_simparamset(repmat(.02, 1, length(sigma_k)));
[QEM, ind_q, ind_EM] = combine_simparamsets(Q, EM);
EM_func = @(e_EM)(r_i_k*e_EM);

% Create evaluation function for measurement + error model (YM_func)
YM_func = @(qem)(Y_func(qem(ind_q,:)) + EM_func(qem(ind_EM,:)));
% likelihood
%q2likelihood = @(q)(E.pdf(binfun(@minus, Y_func(q), exp_data.values(ind))));
% loglikelihood
q2loglikelihood = @(qem)(E.logpdf(binfun(@minus, YM_func(qem), exp_data.values(ind))));
%% Bayesian update
% number of sampels
N = 1000;
% starting points of the random walk
q0 = QEM.mean;
% Likelihood function
prior_vars=QEM.var;
P=generate_stdrn_simparamset(sqrt(prior_vars/10));
%P=generate_stdrn_simparamset(sqrt(prior_vars/5));
% Bayesian update
%q_i = bayes_mcmc(q2likelihood, Q, N, P, q0, 'parallel', true, 'plot_dim', 3:5, 'plot', true, 'T', 1000);
figure()
qEM_i = bayes_mcmc(q2loglikelihood, QEM, N, P, q0, 'parallel', true, ...
    'plot_dim', 3:5, 'plot', true, 'T', 1000, 'log_flag', true);

q_peak=sample_density_peak(qEM_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
param_names = strrep(QEM.param_names, '_',  ' ');
qp_i = QEM.sample(N);
plot_grouped_scatter({qp_i(1:7,:), qEM_i(1:7,:), q_peak(1:7,:)}, ...
    'Labels', param_names(1:7), 'Legends', {'prior', 'posterior', 'est MAP'})
filename = 'bayes_posterior_samples_with_model_error';
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
disp(filename)
end

function sigmas = get_sigmas(ratios_of_max_value, exp_data, ind)
sigmas = zeros(sum(ind),1);
last_ind = 0;
for i = 1:exp_data.num_sections
    y_exp = exp_data.extract_values(i);
    max_y = max(abs(y_exp));
    ind_i = exp_data.extract_section(ind, i);
    % the three sigma region should be the given percentage deviation
    sigmas(last_ind + 1: last_ind + sum(ind_i)) = max_y*ratios_of_max_value(i)/3;
    last_ind = last_ind + sum(ind_i);
end
end
function plot_measurement_model(exp_data, E, ind)
sigmas = sqrt(E.var);
last_ind = 0;
%% Set initial info for plots
% directory to store images
basedir = get_data_dir('images');

% response names
response_names = strrep({exp_data.sections.name}, 'v_x_x',  'v_{x} x=');
response_names = strrep(response_names, 'b_x',  '{b} x=');
response_names = strrep(response_names, '_z',  ' z=');

%%
for i = 1:exp_data.num_sections
    
    figure()
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    ind_i = exp_data.extract_section(ind, i);
    sigmas_i = sigmas(last_ind + 1 : last_ind + sum(ind_i));
    %multiplot()
    plot(t_exp, y_exp)
    hold on
    plot_between(t_exp(ind_i), y_exp(ind_i) - sigmas_i*3, y_exp(ind_i) + sigmas_i*3, [0.3, 0.6, 0.8])
    xlabel(exp_data.sections(i).axis);
    ylabel(response_names{i});
    last_ind = last_ind + sum(ind_i);
    if exp_data.sections(i).axis == 'z'
        set(gcf, 'units','pixel','innerposition',[0 0 400 200])
    else
        set(gcf, 'units','pixel','innerposition',[0 0 1000 200])
        
    end
    %ylim([min(y, y_max])
    filename = ['measurement__bfs',response_names{i}];
    save_png(gcf, filename, 'figdir', basedir)
    disp(filename)
end
end
