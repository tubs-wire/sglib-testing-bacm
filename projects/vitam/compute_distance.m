function dist = compute_distance(i, data_sam, data_exp, norm, weights)
% COMPUTE_DISTANCE Computes the distance between different "DataHandlers"
%   COMPUTE_DISTANCE(I, DATA_SAM, DATA_EXP, NORM, WEIGHTS) computes the
%   distance between DATA_EXP and the I-th column of DATA_SAM in the given
%   NORM (string value, one of 'L1', 'L2' 'W11' ('H1' or 'W21')) and given
%   weights (one for each section of the data handlers, can also be empty).
%
%   Note: if I is a column vector then not the I-th column vector of
%   DATA_SAM is taken, but rather this vector directly.
%
% Example (<a href="matlab:run_example compute_distance">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

switch norm
    case 'L1'; p = 1; deriv = false;
    case 'L2'; p = 2; deriv = false;
    case 'W11'; p = 1; deriv = true;
    case {'H1', 'W21'}; p = 2; deriv = true;
    otherwise; error('vitam:cost', 'Unknown norm');
end

vge = data_exp.values;
if length(i) > 1
    vgs = i;
else
    vgs = data_sam.values(:, i);
end

dist = 0;
for i = 1:data_exp.num_sections
    t_exp = data_exp.extract_coords(i);
    y_exp = data_exp.extract_section(vge, i);
    t_sam = data_sam.extract_coords(i);
    y_sam = data_sam.extract_section(vgs, i);
    
    if isempty(weights)
        w = 1;
    else
        w = weights(i);
    end
    dist = dist + w * vector_distance(t_exp, y_exp, t_sam, y_sam, 'p', p, 'deriv', deriv);
end

