function generate_vitam_param_values1

% Generate a paramset of version 1
Q = generate_param_set('version', 1);
N = 20;

% Generate QMC points
Q.reset_fixed();
generate_and_write_params('complete_qmc', Q, N, 'qmc');

% Generate LHS points
Q.reset_fixed();
generate_and_write_params('complete_lhs', Q, N, 'lhs');

% Generate Smolyak points
Q.reset_fixed();
[params, ~, ~] = Q. get_integration_points(3, 'grid', 'smolyak'); % [q_i, w, xi_i]
write_param_file('complete_sparse_p3', Q, params);

% Generate some more test sets, where different parameters are held fixed
Q.reset_fixed();
Q.set_fixed('SARC_C1', 0);
Q.set_fixed('SARC_C2', 0);
Q.set_fixed('SARC_C3', 0);
Q.set_fixed('SAS', 0);
generate_and_write_params('pg_only_qmc', Q, N, 'qmc');

Q.reset_fixed();
Q.set_fixed('PG', 0);
Q.set_fixed('SAS', 0);
generate_and_write_params('sarc_only_qmc', Q, N, 'qmc');

Q.reset_fixed();
Q.set_fixed('PG', 0);
Q.set_fixed('SARC_C1', 0);
Q.set_fixed('SARC_C2', 0);
Q.set_fixed('SARC_C3', 0);
generate_and_write_params('sas_only_qmc', Q, N, 'qmc');

Q.reset_fixed();
Q.set_fixed('SAS', 0);
generate_and_write_params('pg_sarc_qmc', Q, N, 'qmc');

Q.reset_fixed();
Q.set_fixed('SARC_C1', 0);
Q.set_fixed('SARC_C2', 0);
Q.set_fixed('SARC_C3', 0);
generate_and_write_params('pg_sas_qmc', Q, N, 'qmc');

Q.reset_fixed();
Q.set_fixed('PG', 0);
generate_and_write_params('sarc_sas_qmc', Q, N, 'qmc');


% A small test for version 2 
Q = generate_param_set('version', 2);
N = 20;
generate_and_write_params('complete_qmc_v2', Q, N, 'qmc');



function generate_and_write_params(filename, Q, N, mode)
params = Q.sample(N, 'mode', mode);
write_param_file(filename, Q, params);
