classdef VitamDataHandlerBump < VitamDataHandler 

    properties(Constant)
        filters = {...
            'v_x_x-.25', 'v_x_x.688', 'v_x_x.813', 'v_x_x.938','v_x_x1','v_x_x1.125' ,'v_x_x1.25','v_x_x1.375'...
            , 'uw_b_x-.25', 'uw_b_x.688', 'uw_b_x.813', 'uw_b_x.938','uw_b_x1','uw_b_x1.125' ,'uw_b_x1.25','uw_b_x1.375'...
            , 'cp'};
    end

    methods(Static)
        
        function data = fromExperiment() 
            %Experimental result from the link: https://turbmodels.larc.nasa.gov/axibump_val.html
            data = VitamDataHandlerBump();
            data.read_data();
        end
    end
    
    methods    
        function read_data(data, varargin)
            options=varargin2options(varargin);
            [dirspec,options]=get_option(options, 'dirspec', 'response_bump');
            check_unsupported_options(options, mfilename);
            for i = 1:length(data.filters)
                filter = data.filters{i};
                [x_i_k,y_i_k, axis] = read_filtered_responses(data.q_j_k, filter, 'model_def', 'bump', 'dirspec', dirspec);
                data.add_section(filter, x_i_k, y_i_k, axis);
            end
        end
        
    end
end
