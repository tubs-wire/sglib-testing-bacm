function graph_experimental_and_other_response(y_in, phase_numb, model_descr, varargin)
%y_in: any vector matching the model in terms of dimension.
%is then plotted against measurements and training data
options=varargin2options(varargin, mfilename);
[filename_prep, options]=get_option(options, 'filename_prep', 'experiment');
[q_ij, options]=get_option(options, 'q_ij_with_surrogate', []);
[p_gpc, options]=get_option(options, 'p_gpc', 3);
check_unsupported_options(options);

if isempty(q_ij)
    % Load experimental and training data
    [train_data, exp_data] = load_train_and_exp_data(phase_numb, 'model_descr', model_descr);
else
    % generate surrogate model if
    [~, surr_model, train_data, exp_data] = generate_VITAM_gpc_surrogate(phase_numb, p_gpc,...
        'model_descr', model_descr, 'flag_interp', false);
    y_in = {surr_model.compute_response(q_ij), train_data};
end

%% Set initial info for plots
% directory to store images
basedir = get_data_dir('images');
response_names = get_response_names_for_plot(exp_data);

%% Plot experimental data and optionally responses
for i = 1:exp_data.num_sections
    figure
    resp_name_i = exp_data.sections(i).name;
    coord_i = exp_data.extract_coords(resp_name_i);
    y_i = exp_data.extract_section(exp_data.values, resp_name_i);
    coord_name_i = exp_data.sections(i).axis;
    if nargin>0
        hold on
        
        %any way, uncoditionally: if ischar(y_in) && strcmp(y_in, 'train_data')
            %coord_in_i = train_data.extract_coords(resp_name_i);
            %y_train_i = train_data.extract_section(train_data.values, resp_name_i);
        %else
        if isa(y_in, 'DataHandler')
            coord_in_i = y_in.extract_coords(resp_name_i);
            y_in_i = y_in.extract_values(resp_name_i);
        elseif isa(y_in, 'cell')
            data_handler = y_in{2};
            coord_in_i = data_handler.extract_coords(resp_name_i);
            y_in_i = data_handler.extract_section(y_in{1}, resp_name_i);
            if length(y_in)==3
                ind =y_in{3};
                ind_i = data_handler.extract_section(ind, resp_name_i);    
                coord_in_i = coord_in_i(ind_i);
                y_in_i = y_in_i(ind_i,:);
            end
        else
            coord_in_i = train_data.extract_coords(resp_name_i);
            y_in_i = train_data.extract_section(y_in, resp_name_i);
            y_train_i = train_data.extract_section(train_data.values, resp_name_i);
       end
        h1=plot(coord_in_i, y_in_i, 'Color', [0.7, .7, .7]);%,'DisplayName','MCMC response')
        h2=plot(coord_in_i, y_in_i(:,1), ':','Color', [0.8, .1, .1]);
        h3=plot(coord_in_i, y_train_i, 'Color', [0.2, .7, .3]);%,'DisplayName','training data')
        h4=plot(coord_in_i, y_train_i(:,1), 'Color', [0.8, .1, .1,]);%'DisplayName','SST response'])
        end
    h5=plot( coord_i, y_i, '-x', 'LineWidth', 2,'DisplayName','measurement')
    legend([h1(1), h2(1), h3(1), h4(1), h5(1)],'MCMC response', 'MAP response','training data', 'SST response','measurement')
    
    xlabel(coord_name_i)
    ylabel(response_names{i});
    xlim([min(coord_i), max(coord_i)])
    if nargin<1 || isempty(y_in)
        y_min = min(y_i);
        y_max = max(y_i);
    else
        y_min = min([min(y_i), min(y_in_i)]);
        y_max = max([max(y_i), max(y_in_i)]);
    end
    if coord_name_i == 'z'
        set(gcf, 'units','pixel','innerposition',[0 0 250 100])
%         if resp_name_i(1) == 'v'
%             ylim ([y_min, 1.1])
%             %hold on
%             %plot(xlim(), [0, 0])
%         else
%             ylim (10^-3*[-12.5, 2])
%         end
    else
        set(gcf, 'units','pixel','innerposition',[0 0 1000 150])
        
    end
    ylim([y_min, y_max])  
    filename = [filename_prep, resp_name_i];
    save_png(gcf, filename, 'figdir', basedir)
    disp(filename)
end
close all