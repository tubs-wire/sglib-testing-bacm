function test_bfs_best_params


Q = generate_param_set();

train_data = VitamDataHandler();
train_data.add_param_set('complete_qmc_corr');
train_data.add_param_set('complete_lhs_corr');
train_data.add_param_set('complete_sparse_p3_corr');
train_data.read_data()

exp_data = VitamDataHandler();
exp_data.read_data()

kernel_width = 0.7;
model = RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], {kernel_width});

weights = [1,1,1,1,100,100,100,100,100,5];
norm = 'L1';
norm = 'L2';
norm = 'W11';
norm = 'W21';

[q0, q_worst] = find_best_sample_response(train_data, exp_data, norm, weights);

q_min = find_best_model_response(model, train_data, exp_data, norm, weights);
y_min = model.compute_response(q_min);

y0 = model.compute_response(q0);
y_mean = model.compute_response(Q.mean());
y_bad = model.compute_response(q_worst);

figure
plot_exp_and_sample(exp_data, train_data, [y_bad, y_mean, y0, y_min], {'bad', 'mean', 'y0', 'min'})

format long g
disp(norm)
[q_min, q0, q_worst]


function q_best = find_best_model_response(model, train_data, exp_data, norm, weights)
vitam_cost_fun = @(q)(cost_fun(q, model, train_data, exp_data, norm, weights));

q0 = find_best_sample_response(train_data, exp_data, norm, weights);
options = optimset('Display', 'iter', 'PlotFcns', @optimplotfval, 'MaxIter', 160);
q_best = fminsearch(vitam_cost_fun, q0, options);



function [q_best, q_worst] = find_best_sample_response(train_data, exp_data, norm, weights)
n = train_data.num_params();
c = nan(1,n);
for i = 1:n
    c(i) = compute_distance(i, train_data, exp_data, norm, weights);
end
[~, ind_best] = min(c);
[~, ind_worst] = max(c);
q_best = train_data.get_param(ind_best);
q_worst = train_data.get_param(ind_worst);




function dist=cost_fun(q, model, train_data, exp_data, norm, weights)
y = model.compute_response(q);
dist = compute_distance(y, train_data, exp_data, norm, weights);

% we add a penalty if the parameter gets on or over the boundary 
p = model.Q.cdf(q); % there should probably be a SimParamset.in_range() function 
if any(p==0 | p==1)
    dist = dist + 1000;
end


function plot_exp_and_sample(data_exp, data_sam, y_s, legend_entries)
filters = data_sam.filters;
multiplot_init(length(filters), 'ordering', 'row', 'separate_figs', true)
plot_opts = {'LineWidth', 2};

for i=1:length(filters)
    filter = filters{i};
    multiplot
    plot_data(data_exp, 1, filter, ['kx', plot_opts]); legend_add('true'); hold on;
    plot_data(data_sam, y_s, filter, ['-', plot_opts]); legend_add(legend_entries); hold on;
    title(data_sam.get_filter_name(filter))
end
multiplot_adjust_range('axes', 'y', 'separate', 'rows', 'rows', [1,2])



function plot_data(data, i, filter, opts)

for j = 1:size(i,2)
    if size(i,1)>1
        vg = i(:,j);
    else
        vg = data.values(:, i(j));
    end
    v = data.extract_section(vg, filter);
    x = data.extract_coords(filter);
    plot(x, v, opts{:})
end