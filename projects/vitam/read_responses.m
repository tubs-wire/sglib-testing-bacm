function s = read_responses(params)

params = round_params(params);
hashes = generate_hashes(params);
for i=1:size(params,2)
    base = hashes{i};
    s(i)=read_response_by_hash(base);
end
