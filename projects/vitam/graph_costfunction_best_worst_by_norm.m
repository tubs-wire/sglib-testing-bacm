function graph_costfunction_best_worst_by_norm


%se=read_exp_file();
%[params, hashes] = read_param_file('complete_qmc_corr');

data_sam = load_data({'complete_qmc_corr', 'complete_lhs_corr', 'complete_sparse_p3_corr'});
data_exp = load_data({});


fullscreen(clf)
clf

norms = {'L1', 'L2', 'W11', 'W21'};
%weights = []; wn = ''
weights = [1,1,1,1,100,100,100,100,100,5]; wn = 'w_';
for i = 1:length(norms)
    norm = norms{i};
    test_with_norm(data_sam, data_exp, norm, weights);
    basedir = get_data_dir('images');
    save_png(gcf, ['costfun_comp_', wn, norm], 'figdir', basedir, 'notitle', false)
end


function test_with_norm(data_sam, data_exp, norm, weights)
dist = zeros(1,data_sam.num_samples);
for i=1:data_sam.num_samples
    dist(i) = compute_distance(i, data_sam, data_exp, norm, weights);
end

%%
[~,ind]=sort(dist);
clf
filters = data_sam.filters;
multiplot_init(length(filters), 'ordering', 'row')
plot_opts = {'LineWidth', 2};

disp(norm)
disp([data_sam.q_j_k(:, ind(1)), data_sam.q_j_k(:,ind(end))])
disp(ind([1,end]))

for i=1:length(filters)
    filter = filters{i};
    multiplot
    plot_data(data_exp, 1, filter, ['kx', plot_opts]); legend_add('true'); hold on;
    plot_data(data_sam, ind(1), filter, ['-', plot_opts]); legend_add('best'); hold on;
    plot_data(data_sam, ind(end), filter, ['-', plot_opts]); legend_add('worst'); hold off;
    %1000*comp_diff(data_sam, ind(1), data_exp, filter)
    %1000*comp_diff(data_sam, ind(end), data_exp, filter)
    title(data_sam.get_filter_name(filter))
    %keyboard
end
multiplot_adjust_range('axes', 'y', 'separate', 'rows', 'rows', [1,2])

function diff = comp_diff(data, i, datae, filter) %#ok<DEFNU>
vg = data.values(:, i);
v = data.extract_section(vg, filter);
x = data.extract_coords(filter);
ve = datae.extract_values(filter);
xe = datae.extract_coords(filter);
diff = [...
    vector_distance(x, v, xe, ve, 'p', 1, 'deriv', false),...
    vector_distance(x, v, xe, ve, 'p', 1, 'deriv', true),...
    vector_distance(x, v, xe, ve, 'p', 2, 'deriv', false),...
    vector_distance(x, v, xe, ve, 'p', 2, 'deriv', true)];

function plot_data(data, i, filter, opts)
vg = data.values(:, i);
v = data.extract_section(vg, filter);
x = data.extract_coords(filter);
plot(x, v, opts{:})





    
function data = load_data(names)
data = VitamDataHandler();
for i=1:length(names)
    data.add_param_set(names{i});
end
data.read_data();


function data = load_exp_data(names) %#ok<DEFNU>
data = VitamDataHandler();
for i=1:length(names)
    data.add_param_set(names{i});
end
data.read_data();
