function [response_names,x] = read_delta_wing_response_data_struct()

basedir = get_data_dir('response_delta_wing');
filename = fullfile(basedir, '0a6df40fac00fa2deab2d58ab582687e72fa2bea_v3.csv');
s = read_vitam_csv_file(filename, 2, 'as_matrix', false, 'setup', 'delta_wing');
x = unique(s.x);
n = length(x);
response_names = cell(n,1);
for i = 1:n
    response_names{i} = strvarexpand('cp.x$x(i)$');
end