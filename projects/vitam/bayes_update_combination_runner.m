%this script serves for running combined update for a set of combinations
%of situations and pce degrees.
%the entries' order must correspond in these three fields
phase_nr_set =  {{3},{3},{3},   {3,3,3},{3,3,3,3},{3,3,3,3}}
p_gpc_set =     {{3},{3},{3},   {3,3,3},{3,3,2,2},{3,3,3,3}}
model_descr_set = {{'bfs'}, {'bump'},{'delta_wing'}, {'bfs', 'bump','delta_wing'},{'bfs', 'bump','delta_wing','delta_wing_25'},{'bfs', 'bump','delta_wing','delta_wing_25'}}
for i=1:size(phase_nr_set,2)
    [q_i, Y_func, exp_data_c, w] = bayes_update_sample_of_combined_models(phase_nr_set{i}, model_descr_set{i},p_gpc_set{i})
    filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr_set{i})),'_', model_descr_set{i}{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc_set{i}))];
    save(filename,'q_i')
 end