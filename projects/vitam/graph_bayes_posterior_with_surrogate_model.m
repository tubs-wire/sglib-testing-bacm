function graph_bayes_posterior_with_surrogate_model()

%% Posterior samples with seperate updates
% For bfs
if 0
phase_nr = {3};
p_gpc={3}
model_descr = {'bfs'};
[Q, model_bfs, train_data_bfs] = generate_VITAM_gpc_surrogate(phase_nr{1}, p_gpc{1}, 'model_descr', model_descr{1});
%This nomenclatura follows the saving of the data in bayes_update_combination_runner.m
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i = load(filename)
[  q_MAP_bfs]= plot_output(phase_nr, model_descr,  q_i, model_bfs);

% Apply bfs model to  bump
phase_nr = {3};
model_descr = {'bump'};
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i_bump = load(filename)
[  q_MAP_bump]= plot_output(phase_nr, model_descr,  q_i_bump, model_bfs);

% For delta wing
phase_nr = {3};
model_descr = {'delta_wing'};
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i_delta = load(filename)
p_gpc={2}
[ q_MAP_wing]= plot_output(phase_nr, model_descr, q_i_delta,  model_bfs);

%% Posterior samples with combined updates
phase_nr = {3, 3, 3};
p_gpc={3,3,3}
model_descr = {'bfs', 'bump', 'delta_wing'};
[Q, model_bfs_bump_delta, train_data_dummy] = generate_VITAM_gpc_surrogate(phase_nr{3}, p_gpc{3}, 'model_descr', model_descr{3});
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i_bfs_bump_delta = load(filename)
[ q_MAP_wing]= plot_output(phase_nr, {model_descr{3}}, q_i_bfs_bump_delta, model_bfs_bump_delta);
end
if 1
phase_nr = {3, 3, 3,3};
p_gpc={3,3,3,3}
model_descr = {'bfs', 'bump', 'delta_wing', 'delta_wing_25'};
[Q, model_bfs_bump_delta, train_data_dummy] = generate_VITAM_gpc_surrogate(phase_nr{4}, p_gpc{4}, 'model_descr', model_descr{4});
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i_bfs_bump_delta = load(filename)
[ q_MAP_wing]= plot_output(phase_nr, model_descr, q_i_bfs_bump_delta, model_descr{4}, model_bfs_bump_delta);
end
%Samples from MCMC excluding delta_25, passed to delta_25 surrogate.
phase_nr = {3, 3, 3};
p_gpc={3,3,3}
model_descr = {'bfs', 'bump', 'delta_wing'};
[Q, model_bfs_bump_delta, train_data_dummy] = generate_VITAM_gpc_surrogate(phase_nr{3}, 3, 'model_descr','delta_wing_25');
filename = ['data/update_results/post_samples', sprintf('%d', cell2mat(phase_nr)),'_', model_descr{:},'_','p_gpc', sprintf('%d', cell2mat(p_gpc))];
q_i_bfs_bump_delta = load(filename)
[ q_MAP_wing]= plot_output(phase_nr, model_descr, q_i_bfs_bump_delta, {'delta_wing_25'},model_bfs_bump_delta);

[Q, model, train_data, exp_data] = generate_VITAM_gpc_surrogate(phase_nr, 'model_descr', model_descr, p_gpc);
y_t = model.compute_response(q_i);
graph_experimental_and_other_response(y_t, phase_nr, model_descr, 'filename_prep','bfs_bayes_updated_responses_with_combined_models')
% bump
phase_nr = 3;
p_gpc={3}
model_descr = 'bump';
[Q, model, train_data, exp_data] = generate_VITAM_gpc_surrogate(phase_nr, 'model_descr', model_descr, p_gpc);
y_t = model.compute_response(q_i);
graph_experimental_and_other_response(y_t, phase_nr, model_descr, 'filename_prep','bump_bayes_updated_responses_with_combined_models')
% delta_wing
y_t = model.compute_response(q_i);
graph_experimental_and_other_response(y_t, phase_nr, model_descr, 'filename_prep','delta_wing_bayes_updated_responses_with_combined_models')
q_MAP=sample_density_peak(q_i);
% q_peak =   [0.3473; 0.5581; 7.9978; 0.0219; 1.5797]
qfilename = ['bayes_post_MAP', strvarexpand('phase$phase_numb$'), model_descr{:}];
write_param_file(qfilename, Q, q_MAP, 'dirspec', 'update');

param_names = strrep(Q.param_names, '_',  ' ');
plot_grouped_scatter({Q.sample(N), q_i, q_MAP}, ...
    'Labels', param_names, 'Legends', {'prior', 'posterior', 'est MAP'})
filename = ['bayes_posterior_samples', strvarexpand('phase$phase_numb$'), model_descr{:}];
basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir)
disp(filename)

%% Validate update
% combined response at MAP estimate
y_MAP = Y_func(q_MAP);
% Weighted error of MAP estimate
err_MAP = y_MAP - exp_data_c;
e_MAP = err_MAP'*diag(w.^2)*err_MAP;

e_MAP_i = [];
for i = 1: n_model
    Y_MAP_i = model{i}.compute_response(q_MAP);
    err_MAP_i = Y_MAP_i - exp_data_c;
    e_MAP_i = [e_MAP_i, err_MAP_i'*diag(w_i{i}.^2)*err_MAP_i];
end
end
%before: function [q_i, Y_func, exp_data, w, q_MAP]=...
%Now avoiding repeating the Update. 
function [  q_MAP]= plot_output(phase_numb, surr_descr, q_i, model_descr, model)
%[q_i, Y_func, exp_data, w] = bayes_update_sample_of_combined_models(phase_numb, model_descr, p_gpc);
%q_i = bayes_update_sample_with_interp(phase_numb, model_descr);
q_MAP=sample_density_peak(q_i.q_i);
file_name_pre =[surr_descr{:}, '_bayes_updated_responses'];
graph_output_with_posterior_samples(q_i,model, phase_numb{1}, model_descr, file_name_pre)

end
function graph_output_with_posterior_samples(q_i, model, phase_numb, experimental_model_descr, filename)
q_MAP=sample_density_peak(q_i.q_i);
y_t = model.compute_response([q_MAP,q_i.q_i]);

graph_experimental_and_other_response(y_t, phase_numb, experimental_model_descr, 'filename_prep', filename)
end