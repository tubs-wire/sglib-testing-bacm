function e_MAP_rel = get_norm_of_error(Y_func, q_MAP, exp_data, w)
y_MAP = Y_func(q_MAP);
% Weighted error of MAP estimate
err_MAP = y_MAP - exp_data;
e_MAP = err_MAP'*diag(w.^2)*err_MAP;
n_exp_MAP = exp_data'*diag(w.^2)*exp_data;
e_MAP_rel = e_MAP/n_exp_MAP;
end