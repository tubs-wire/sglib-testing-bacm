function [train_data, exp_data] = load_train_and_exp_data(phase_numb, varargin)

options=varargin2options(varargin, mfilename);
[model_descr, options]=get_option(options, 'model_descr', 'bfs');
%[response_dir, options]=get_option(options, 'response', 'bfs');
check_unsupported_options(options);

%% Load model, points and solutions
switch model_descr
    case 'bfs'
        data_handler_func = @()(VitamDataHandler());
    case 'bump'
        data_handler_func = @()(VitamDataHandlerBump());
    case 'delta_wing'
        data_handler_func = @()(VitamDataHandlerDeltaWing());
    case 'delta_wing_25'
        data_handler_func = @()(VitamDataHandlerDeltaWing());
    otherwise
        error(strvarexpand('No $model_descr$ model is implemented in surrogate modeling'))
end

train_data = data_handler_func();
if phase_numb == 1
    train_data.add_param_set('complete_qmc_corr');
    train_data.add_param_set('complete_lhs_corr');
    train_data.add_param_set('complete_sparse_p3_corr');
    switch model_descr
        case 'bfs'
            dirspec = 'response';
        case 'bump'
            dirspec = 'response';
        otherwise
            error(strvarexpand('No training data folder is defined for $model_descr$ with phase number $phase_numb$'))
    end
elseif phase_numb == 2
    warning('off', 'VITAMreadResponse:InterpData') % switch of warning the data is interpolated to the original spatial grid
    train_data.add_param_set('complete_qmc_corr');
    train_data.add_param_set('complete_lhs_corr');
    train_data.add_param_set('complete_sparse_p3_corr');
    train_data.add_param_set('qmc_L1_second_run',  'dirspec', 'params2');
    train_data.add_param_set('qmc_L2_second_run',  'dirspec', 'params2');
    train_data.add_param_set('qmc_W11_second_run',  'dirspec', 'params2');
    train_data.add_param_set('qmc_W21_second_run_v',  'dirspec', 'params2');
    
    switch model_descr
        case 'bfs'
            dirspec = 'response2';
        case 'bump'
            dirspec = 'response_bump';
        otherwise
            error(strvarexpand('No training data folder is defined for $model_descr$ with phase number $phase_numb$'))
    end
elseif phase_numb == 3
    
    switch model_descr
        case 'bfs'
            train_data.add_param_set('only_qmc_PG_0-10',  'dirspec', 'params1');
            dirspec = 'response_bfs';
        case 'bump'
            train_data.add_param_set('only_qmc_PG_0-10',  'dirspec', 'params1');
            dirspec = 'response_bump_new';
        case 'delta_wing'
            train_data.add_param_set('qmc_PG_0-10_for_deltawing',  'dirspec', 'params1');
            dirspec = 'response_delta_wing';
        case 'delta_wing_25'
            train_data.add_param_set('qmc_PG_0-10_for_deltawing',  'dirspec', 'params1');
            train_data.add_param_set('zero_params',  'dirspec', 'params1');
            dirspec = 'response_delta_wing_25';
       otherwise
            error(strvarexpand('No training data folder is defined for $model_descr$ with phase number $phase_numb$'))
    end
end

%%
% load solution at training points
train_data.read_data('dirspec', dirspec)

%% Load experimental data
exp_data = data_handler_func();
exp_data.read_data('dirspec', dirspec);