classdef RBFSurrogateModel < BaseModel
    % BASEMODEL Base class for stochastic Models.
    
    properties(Constant)
        INTERPOLATE = 0
        NORM_INTERPOLATE = 1
        DISTANCE_WEIGHTING = 2
        L1_MINIMIZATION = 3
    end
    
    properties
        q_j_k
        u_i_k
        Q@SimParamSet
        
        mode
        kernel_func
        kernel_params
        normalized_kernel
    end
    
    properties
        w_j
        W_k_k
    end
    
    methods(Static)
        function model = RBFSurrogateModel(Q, q_j_k, u_i_k, ...
                W_k_k, kernel_func, kernel_params, normalized_kernel)
            model.Q = Q;
            model.q_j_k = q_j_k;
            model.u_i_k = u_i_k;
            model.W_k_k = W_k_k;
            model.w_j = 1./sqrt(Q.var(true)); % Determine variances for weights
            
            if nargin<5 || isempty(kernel_func); kernel_func = @RBFSurrogateModel.gaussian_kernel; end
            if nargin<6 || isempty(kernel_params); kernel_params = {1}; end
            if nargin<7 || isempty(normalized_kernel); normalized_kernel=true; end
            model.kernel_func = kernel_func;
            model.kernel_params = kernel_params;
            model.normalized_kernel = normalized_kernel;
        end
        
        function model = fromNormInterpolation(Q, q_j_k, u_i_k, varargin)
            model = RBFSurrogateModel(Q, q_j_k, u_i_k, [], varargin{:});
            model.normalized_kernel = true;
            A_k_k = model.evaluate_kernel2(model.q_j_k);
            A_k_k = A_k_k + 10^-8*norm(A_k_k)*eye(size(A_k_k));
            model.W_k_k = pinv(A_k_k);
        end
        
        function model = fromInterpolation(Q, q_j_k, u_i_k, varargin)
            model = RBFSurrogateModel(Q, q_j_k, u_i_k, [], varargin{:});
            model.normalized_kernel = false;
            A_k_k = model.evaluate_kernel2(model.q_j_k);
            A_k_k = A_k_k + 10^-8*norm(A_k_k)*eye(size(A_k_k));
            model.W_k_k = pinv(A_k_k);
        end
        
        function model = fromDistanceWeighting(Q, q_j_k, u_i_k, varargin)
            num_samples = size(q_j_k, 2);
            W_k_k = eye(num_samples);
            model = RBFSurrogateModel(Q, q_j_k, u_i_k, W_k_k, varargin{:});
        end
        
        function model = fromL1Minimisation(Q, q_j_k, u_i_k, varargin)
            % Min problem || u_i_k - u_i_k * W_k_k * z_k_k ||_1
            % with respect W_k_k
            % subject to sum(W_k_k,2)==1, 0<=W_k_k<=1

            % Reformulate to
            % 1_i_k : g_i_k
            % s.t. u_i_k * (I-W_k_k*z_k_k) >= g_i_k
            % and

            
            model = RBFSurrogateModel(Q, q_j_k, u_i_k, [], varargin{:});
            model.normalized_kernel = true;
            model.W_k_k = model.computeOptimalWeights(true);
        end
        
        function model = fromLInftyMinimisation(Q, q_j_k, u_i_k, varargin)
            model = RBFSurrogateModel(Q, q_j_k, u_i_k, [], varargin{:});
            model.normalized_kernel = true;
            model.W_k_k = model.computeOptimalWeights(false);
        end
        
    end
    
    methods
        function W_k_k = computeOptimalWeights(model, doL1)
            z_k_k = model.evaluate_kernel2(model.q_j_k);
            U = model.u_i_k;
            v = std(U,[],2); vs=sort(v,'descend');
            ind = (v>=vs(20));
            U = U(ind,:);
            
            if doL1
                Z = z_k_k;
                I2 = eye(size(Z,1));
                I1 = speye(numel(U));
                one = ones(size(z_k_k,1),1);
                
                A = [revkron(U, Z'), -I1; -revkron(U, Z'), -I1];
                b = [U(:); -U(:)];
                %Aeq = [revkron(one', I2), zeros(size(Z,1),1)];
                Aeq = [revkron(one', I2), sparse(size(Z,1),numel(U))];
                beq = one;
                f = [zeros(numel(Z),1); ones(numel(U),1)];
                lb = [zeros(numel(Z),1); zeros(numel(U),1)];
                ub = [ones(numel(Z),1); inf(numel(U),1)];
            else
                Z = z_k_k;
                I2 = eye(size(Z,1));
                I1 = ones(numel(U),1);
                one = ones(size(z_k_k,1),1);
                
                A = [revkron(U, Z'), -I1; -revkron(U, Z'), -I1];
                b = [U(:); -U(:)];
                %Aeq = [revkron(I2, one'), zeros(size(Z,1),1)];
                Aeq = [revkron(one', I2), zeros(size(Z,1),1)];
                beq = one;
                f = [zeros(numel(Z),1); 1];
                lb = [zeros(numel(Z),1); 0];
                ub = [ones(numel(Z),1); inf];
            end
            
            opts = optimoptions('linprog');
            %opts.Display = 'iter';
            opts.Display = 'final';
            %opts.ConstraintTolerance = 0.003;
            opts.MaxTime = 20;
            %opts.Algorithm = 'interior-point';
            
            x = linprog(f, A, b, Aeq, beq, lb, ub, opts);
            W_k_k = reshape(x(1:numel(Z)), size(Z));
        end
        
        function showWeightingMatrix(model, wait)
            if nargin<2; wait=true; end;
            if wait; f=figure; end
            spy2(model.W_k_k, 'color_scale', 'linear'); 
            if wait
                waitforbuttonpress; 
                close(f);
            end
        end
    end
    
    methods
        
        function n=response_dim(model)
            n = size(model.u_i_k,1);
        end
        
        function m=num_samples(model)
            m  = size(model.q_j_k,2);
        end
        
        
        function u_i_l=compute_response(model, q_j_l)
            %assert(all(w>=0), 'Weights should be positive');
            z_k_l = model.evaluate_kernel2(q_j_l);
            w_k_l = model.W_k_k * z_k_l;
            u_i_l = model.u_i_k * w_k_l;
        end
        
        function y=compute_measurements(model, u)
            assert(false, 'Not implemented');
        end
    end
    
    methods
        function model = set_kernel_params(model, varargin)
            model.kernel_params = varargin;
        end
        
        function z_k_l = evaluate_kernel2(model, q_j_l, normalized)
            if nargin<3
                normalized = model.normalized_kernel;
            end
            r_k_l = pairwise_distance(model.q_j_k, q_j_l, 'weights', model.w_j);
            z_k_l = evaluate_kernel(model, r_k_l);
            if normalized
                z_k_l = binfun(@times, z_k_l, 1./sum(z_k_l,1));
            end
        end
        
        function z_k_l = evaluate_kernel(model, r_k_l)
            z_k_l = model.kernel_func(r_k_l, model.kernel_params{:});
        end
        
    end
    
    methods(Static)
        function s=gaussian_kernel(r,a)
            s = exp(-(r./a).^2);
        end
        
    end
    
end