function generate_vitam_param_values2

% Generate a paramset of version 2
Q = generate_param_set();
N = 75;

[q_L1, q_L2, q_W11, q_W21]=best_results_for_vitam_param_value1

%%
q_mean = q_W21(:,1);
norm = 'W21';
for i = 1:Q.num_params
    dist = Q.get_param(i).dist;
    q_min = dist.invcdf(0);
    q_max = dist.invcdf(1);
    dq = (q_max - q_min) / 10;
    dist_new = BetaDistribution(2,2);
    
    q_min_new = max(q_min, q_mean(i) - dq);
    q_max_new = min(q_max, q_mean(i) + dq);
    
    dist_new = dist_new.fix_bounds(q_min_new, q_max_new);
    %hist(dist_new.sample(100000), 100)
    Q.set_dist(i, dist_new)
end
generate_and_write_params(['qmc_', norm, '_second_run'],Q, N, 'qmc')


function generate_and_write_params(filename, Q, N, mode)
params = Q.sample(N, 'mode', mode);
write_param_file(filename, Q, params);