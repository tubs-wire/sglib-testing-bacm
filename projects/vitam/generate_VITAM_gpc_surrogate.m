function [Q, model, train_data, exp_data, ind] = generate_VITAM_gpc_surrogate(phase_numb,p_gpc, varargin)

options=varargin2options(varargin, mfilename);
[flag_interp, options]=get_option(options, 'flag_interp', false);
[model_descr, options]=get_option(options, 'model_descr', 'bfs');
[flag_PCE, options]=get_option(options, 'flag_PCE', false);
check_unsupported_options(options);

%% Load prior random variables of the coefficients
if all(phase_numb==3)
    v = 3;
elseif all(phase_numb<3)
    v = 2;
else
    error('phase numb 3 together with other phase numbers are not compatible, they are defined on different prior bounds')
end
Q = generate_param_set('version', v);

%% Load training and experimental data
[train_data, exp_data] = load_train_and_exp_data(phase_numb, 'model_descr', model_descr);

%% Generate surrogate model
%kernel_width = 0.7;
%model = RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], {kernel_width});
% if phase_numb == 1
%     p_gpc = 1;
% elseif phase_numb == 2
%     p_gpc = 2;
% elseif phase_numb ==3
%     p_gpc = 2;
% end
% if contains( model_descr, 'delta_wing')
%     p_gpc = 3;
% end
if flag_interp
    % Load experimental data
    
    [y_train, ind]=interpolate_to_measurement(train_data, exp_data);
    if flag_PCE
       model = PCSurrogateModel.fromInterpolation(Q, train_data.q_j_k, y_train, p_gpc); 
    else
        model = GPCSurrogateModel.fromInterpolation(Q, train_data.q_j_k, y_train, p_gpc);
    end
else
    if flag_PCE
        model = PCSurrogateModel.fromInterpolation(Q, train_data.data{:}, p_gpc);
    else
        model = GPCSurrogateModel.fromInterpolation(Q, train_data.data{:}, p_gpc);
    end
end
end

function [y_train, ind] = interpolate_to_measurement(train_data, exp_data)
y_train = [];
ind =logical([]);
for i = 1:exp_data.num_sections
    t_exp = exp_data.extract_coords(i);
    y_exp = exp_data.extract_values(i);
    t_sam = train_data.extract_coords(i);
    y_sam = train_data.extract_values(i);
    y_train_i =[];
    for j=1:size(y_sam,2)
        if j==1
            [ti, y_exp_i, y_train_ij, ind_i] = common_interp(t_exp, y_exp, t_sam, y_sam(:,j), -1);
        else
            y_train_ij = interp1(t_sam, y_sam(:,j), ti);
        end
        y_train_i = [y_train_i,y_train_ij];
    end
    %y_exp = [y_exp ; y_exp_i];
    y_train = [y_train ; y_train_i];
    %coord_int = [coord_int; ti];
    ind = [ind; ind_i];
end
end