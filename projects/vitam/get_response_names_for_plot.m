function names = get_response_names_for_plot(data)

% response names
names = strrep({data.sections.name}, 'v_x_x',  'v_{x} x=');
names = strrep(names, 'b_x',  '{b} x=');
names = strrep(names, '_z',  ' z=');
names = strrep(names, 'cp.x',  'c_{p} x=');