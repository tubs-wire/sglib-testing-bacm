classdef VitamDataHandler < DataHandler
    properties
        q_j_k = [];
    end

    properties(Constant, Access = private)
        filters = {...
            'v_x_x1', 'v_x_x4', 'v_x_x6', 'v_x_x10', ...
            'uw_b_x1', 'uw_b_x4', 'uw_b_x6', 'uw_b_x10', ...
            'cf_z0', 'cp_z0'};
    end


    
    
    methods(Static)
        function data = fromParamSets(names) 
            data = VitamDataHandler();
            for i=1:length(names)
                data.add_param_set(names{i});
            end
            data.read_data();
        end
        
        function data = fromExperiment()
            % Experimental data source: https://turbmodels.larc.nasa.gov/backstep_val.html
            data = VitamDataHandler();
            data.read_data();
        end
    end
    
        
    methods    
        function data=data(data)
            data = {data.params, data.values};
        end
        
        
        function n = num_params(data)
            n = size(data.q_j_k, 2);
        end
        
        function q_j_k = params(data)
            q_j_k = data.q_j_k;
        end
        
        function q = get_param(data, k)
            q = data.q_j_k(:,k);
        end
        
        function add_param_set(data, filename, varargin)
            data.q_j_k = read_param_file(filename, 'extend', data.q_j_k, varargin{:});
        end
        
        function read_data(data, varargin)
            options=varargin2options(varargin);
            [dirspec,options]=get_option(options, 'dirspec', 'response_bfs');
            check_unsupported_options(options, mfilename);
            for i = 1:length(data.filters)
                filter = data.filters{i};
                [x_i_k,y_i_k, axis] = read_filtered_responses(data.q_j_k, filter, 'dirspec', dirspec);
                data.add_section(filter, x_i_k, y_i_k, axis);
            end
        end
        
    end
    methods(Static)
        function str = get_filter_name(filter)
            ind = find(filter=='_', 1, 'last' );
            str = [filter(1:ind-1), ' (', filter(ind+1), '=', filter(ind+2:end), ')'];
        end
        
    end
end
