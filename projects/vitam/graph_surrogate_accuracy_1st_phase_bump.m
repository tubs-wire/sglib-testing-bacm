function graph_surrogate_accuracy_1st_phase_bump
%% Load paramset and data
Q = generate_param_set();
[data, w_k, tdesc] = load_1st_phase_bump_data(Q);
data_exp = load_data({});

%% Surrogate model generating functions
% model_funcs = {@(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 1),...
%                @(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 2),...
%                @(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 3),...
%                @(Q, q, u, w)GPCSurrogateModel.fromProjection(Q, q, u, w, 1),...
%                @(Q, q, u, w)GPCSurrogateModel.fromProjection(Q, q, u, w, 2),...
%                @(Q, q, u, w)GPCSurrogateModel.fromProjection(Q, q, u, w, 3)};
%
% model_names = {'GPC_p1_Interp', 'GPC_p2_Interp', 'GPC_p3_Interp', 'GPC_p1_Proj', 'GPC_p2_Proj', 'GPC_p3_Proj'};
model_funcs = {@(Q, q, u, w)GPCSurrogateModel.fromInterpolation(Q, q, u, 1),...
    %@(Q, q, u, w)GPCSurrogateModel.fromProjection(Q, q, u, w, 1),...
    @(Q, q, u, w)PCSurrogateModel.fromInterpolation(Q, q, u, 1),...
    %@(Q, q, u, w)PCSurrogateModel.fromProjection(Q, q, u, w, 1),...
    @(Q, q, u, w)PCSurrogateModel.fromInterpolation(Q, q, u, 2)}...
    %@(Q, q, u, w)PCSurrogateModel.fromProjection(Q, q, u, w, 2)};

%model_names = {'GPC p1 Interp', 'GPC p1 Proj', 'PC p1 Interp', 'PC p1 Proj', 'PC p2 Interp', 'PC p2 proj'};
model_names = {'GPC p1 Interp', 'PC p1 Interp', 'PC p2 Interp'};

%% cross validate different degree gpces
q_j_l = data.params;
u_i_l = data.values;
K = 15;
n_s = 0.8;

[err_i_p, maxerr_i_p]  = train_and_cross_val(Q, q_j_l, u_i_l, K, model_funcs, n_s);

filters = data.filters;



for i=1:length(filters)
    if i<17
        subplot(8, 2, i)
    else
        figure
    end
    x = data.extract_coords(filters{i});
    v = data.extract_section(err_i_p, filters{i});
    vmax = data.extract_section(maxerr_i_p, filters{i});
    x_e = data_exp.extract_coords(filters{i});
    u_e = data_exp.extract_values(filters{i});
    plot(x,abs(v)*100)
    hold on
    plot(x,abs(vmax)*100, ':', 'LineWidth', 2)
    plot(x_e, u_e, 'LineWidth', 2)
    ylabel(filters{i})
    if i<17
        xlim([0,1])
    end
    if i==2 || i== 17
        leg_1 = strcat(model_names, '(av. abs err)*100');
        leg_2 = strcat(model_names, '(av. max err)*100');
        legend(leg_1{:}, leg_2{:}, 'experimental')
    end
end

function [e1,e2,einf]=compute_error(y_p, y_v)
dy = abs(y_p - y_v);
e1 = sum(dy, 1);
e2 = sum(dy.^2, 1).^(1/2);
einf = max(dy, [], 1);

function generate_plot(train_func, modelgen_func, modelgen_param, val_func, str)
Q = generate_param_set();
[train_data, w_k, tdesc] = train_func(Q);
[model_func, mdesc] = modelgen_func(modelgen_param);
model = model_func(Q, train_data, w_k);
[val_data, ~, vdesc] = val_func(Q);


filter = train_data.filters{end};
add = 3; num_plots = 12;
nums = add+(1:num_plots);
full_desc = [mdesc ', train=' tdesc ' val=' vdesc ' ' str];
filename = prepfilename([mdesc ' ' tdesc ' ' vdesc]);
plot_results(train_data, model, val_data, filter, nums, full_desc )

%fullscreen;
set(gcf, 'units','pixel','innerposition',[0 0 800 600])

basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir, 'notitle', false)
disp(filename)
%dock(gcf);


%#ok<*DEFNU>
function [data, w_k] = load_data(names, varargin)
data = VitamDataHandlerBump();
for i=1:length(names)
    data.add_param_set(names{i}, varargin{:});
end
data.read_data();
n = data.num_samples();
w_k = (1.0/n) * ones(n, 1);


function [data, w_k, desc] = load_1st_phase_bump_data(Q)
%warning('off', 'VITAMreadResponse:InterpData')
[data, w_k] = load_data({'complete_sparse_p3_corr', ...
    'complete_qmc_corr','complete_lhs_corr'},  'dirspec', 'params1');
desc = '1st_phase_points';

%% functions for creating different surrogates

function [model_func, desc] = surr_rbf_l1(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromL1Minimisation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF L_1 approx (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_linf(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromLInftyMinimisation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF L_\infty approx (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_dw(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromDistanceWeighting(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF distance weighting (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_nint(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF interpolation (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_gpc_int(varargin)
model_func = @(Q, train_data, w_k)GPCSurrogateModel.fromInterpolation(Q, train_data.data{:}, varargin{:}{:});
desc = strvarexpand('GPC interpolation (p=$varargin{1}{1}$)');

function [model_func, desc] = surr_gpc_proj(varargin)
model_func = @(Q, train_data, w_k)GPCSurrogateModel.fromProjection(Q, train_data.data{:}, w_k, varargin{:}{:});
desc = strvarexpand('GPC projection (p=$varargin{1}{1}$)');



function plot_results(train_data, model, val_data, filter, nums, title)
if nargin<6
    title=[];
end

multiplot_init(length(nums), [], 'title', title)
for num=nums
    multiplot
    show_result(train_data, model, val_data, num, filter)
end


function show_result(train_data, model, val_data, num, filter)

qv_j_k = val_data.params;

up_i_l = model.compute_response(qv_j_k(:,num));
up_is_l = train_data.extract_section(up_i_l, filter);

xs_i = train_data.extract_coords(filter);

ut_is_k = train_data.extract_values(filter);
ut_is_min = min(ut_is_k,[],2);
ut_is_max = max(ut_is_k,[],2);

uv_is_k = val_data.extract_values(filter);

hold off;
show_comparison(xs_i, up_is_l, uv_is_k(:,num), ut_is_min, ut_is_max)

function show_comparison(x, y_predicted, y_validate, y_train_min, y_train_max)
% Plot training data as reference
plot_between(x, y_train_min, y_train_max, 0.9*[1,1,1])
hold all;

%plot(xf, yi, 'k'); hold all;

plot(x, y_predicted, 'r', 'LineWidth', 2)
plot(x, y_validate, 'b--', 'LineWidth', 2)
%legend('Range', 'Prediction', 'Exact', 'Location', 'SouthEast')

d = 0.4;
plot_between(x, 0*y_predicted-d, abs(y_predicted - y_validate)-d, 0.94*[1,1,1])

