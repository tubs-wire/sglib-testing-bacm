function sensitivity_analysis_1

Colors = set_my_favorite_colors();

%%

Q = generate_param_set();
param_names = strrep(Q.param_names, '_',  ' ');

train_data = VitamDataHandler();
train_data.add_param_set('complete_qmc_corr');
train_data.add_param_set('complete_lhs_corr');
train_data.add_param_set('complete_sparse_p3_corr');
train_data.read_data()

exp_data = VitamDataHandler();
exp_data.read_data()

%kernel_width = 0.7;
%model = RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], {kernel_width});

model = GPCSurrogateModel.fromInterpolation(Q, train_data.data{:}, 1);
[partial_var, I_s, ratio_by_index]=gpc_sobol_partial_vars(model.u_i_alpha, model.V_u);

%%
basedir = get_data_dir('images');
response_names = strrep({train_data.sections.name}, 'v_x_x',  'v_{x} x=');
response_names = strrep(response_names, 'b_x',  '{b} x=');
response_names = strrep(response_names, '_z',  ' z=');
for i = 1:length({train_data.sections.name})
    figure
    resp_name_i = train_data.sections(i).name;
    coord_i = train_data.extract_coords(resp_name_i);
    partial_var_i = train_data.extract_section(partial_var, resp_name_i);
    coord_num_i = train_data.sections(i).axis;
    if coord_num_i == 1
        coord_name_i ='z';
    else
        coord_name_i ='x';
    end
    
    plot( coord_i, partial_var_i, 'LineWidth', 2)
    legend(param_names)
    xlabel(coord_name_i)
    ylabel(['partial variance of ',response_names{i}]);
    set(gcf, 'units','pixel','innerposition',[0 0 600 250])
    
    
    filename = ['sensitivity', resp_name_i];
    save_png(gcf, filename, 'figdir', basedir)
    disp(filename)
end
close all