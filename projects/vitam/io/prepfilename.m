function name=prepfilename(name)
% PREPFILENAME Changes a name so that it can be used as a filename
%   NAME=PREPFILENAME(NAME) changes NAME, e.g. by replacing spaces with
%   underscores, making it usable as a filename.
%    Replacements:
%      ' ' => '_'
%      '=' => '_'
%      '.' => 'p'
%    Remove:
%      '(', ')', '\'
%
% Example (<a href="matlab:run_example prepfilename">run</a>)
%    name = 'foo bar (x = 3.1).png'
%    prepfilename(name)
%    % should print 'foo_bar_x___3p1.png
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


findchar = ' =.';
repchar  = '__p';
remchar  = '()\';

name = lower(name);
for i=1:length(findchar)
    cf = findchar(i); cr = repchar(i);
    name(name==cf) = cr;
end

for i=1:length(remchar)
    cr = remchar(i);
    name(name==cr) = [];
end
