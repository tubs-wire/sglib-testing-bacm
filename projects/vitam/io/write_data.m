function write_data(filename, data, varargin)
% WRITE_DATA Write struct/tabular data to csv file.
%
% Example (<a href="matlab:run_example write_data">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.


options = varargin2options(varargin, mfilename);
[dirspec, options] = get_option(options, 'dirspec', 'data');
[ext, options] = get_option(options, 'ext', 'dat');
check_unsupported_options(options);


if isa(data, 'table')
    T = data;
else
    num = {};
    vars = {};
    for i=1:2:length(data)
        num{end+1} = reshape(data{i+1}, [], 1); %#ok<AGROW>
        vars{end+1} = data{i}; %#ok<AGROW>
    end
    T = table( num{:}, 'VariableNames', vars ); 
end

dir=get_data_dir(dirspec);
filename = fullfile(dir, [filename, '.' ext]);
makesavepath(filename);

writetable(T, filename);

% Make sure we read in the data *exactly* the way we wrote it...
% (otherwise hashes become inconsistent.)
T2 = readtable(filename);
assert(isequal(T, T2));
