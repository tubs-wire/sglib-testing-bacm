function write_array(filename, data, varargin)
% WRITE_ARRAY Write an array to a file.
%
% Example (<a href="matlab:run_example write_array">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin, mfilename);
[dirspec, options] = get_option(options, 'dirspec', 'data');
[ext, options] = get_option(options, 'ext', 'dat');
check_unsupported_options(options);


dir=get_data_dir(dirspec);
filename = fullfile(dir, [filename, '.' ext]);
makesavepath(filename);


%save(filename, 'data', '-ascii');
dlmwrite(filename, data, 'delimiter', '\t', 'precision', 6)
