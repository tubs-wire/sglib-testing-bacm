classdef DataHandler < handle
    
    properties
        sections
        values
        coords
    end
    
    methods
        function handler = DataHandler()
            handler.sections = struct('name', {}, 'i1', {}, 'i2', {}, 'axis', {});
            handler.coords = zeros(0,0);
            handler.values = zeros(0,0);
        end
        
        
        function add_section(handler, name, coords, values, axis)
            assert(size(coords, 1)==size(values,1), 'Coords and values must match in the first dimension');
            
            section = struct();
            section.name = name;
            section.i1 = size(handler.values, 1)+1;
            section.i2 = size(handler.values, 1)+size(values, 1);
            section.axis = axis;
            
            handler.sections(end+1) = section;
            handler.values = [handler.values; values];
            handler.coords = [handler.coords; coords];
        end
        
        function n = num_sections(handler)
            n = length(handler.sections);
        end
        
        function ind = find_section(handler, name)
            ind = find(strcmp({handler.sections.name}, name));
            assert(numel(ind)==1, 'Should find unique section');
        end
        
        function section = get_section(handler, name_or_ind)
            if ischar(name_or_ind)
                section = handler.get_section_by_name(name_or_ind);
            else
                section = handler.sections(name_or_ind);
            end
            
        end
        
        function section = get_section_by_name(handler, name)
            ind = handler.find_section(name);
            section = handler.sections(ind);
        end
        
        function ys = extract_section(handler, y, name_or_ind)
            section = handler.get_section(name_or_ind);
            ys = y(section.i1:section.i2, :);
        end
        
        function ys = extract_values(handler, name_or_ind)
            ys = handler.extract_section(handler.values, name_or_ind);
        end
        
        function xs = extract_coords(handler, name_or_ind)
            xs = handler.extract_section(handler.coords, name_or_ind);
        end
        
        function n = num_samples(handler)
            n = size(handler.values, 2);
        end
    end
    
    
end

