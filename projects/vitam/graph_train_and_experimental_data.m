function graph_train_and_experimental_data(train_data, exp_data, filename_prep)
    %% Set initial info for plots
% directory to store images
basedir = get_data_dir('images');

% response names
response_names = get_response_names_for_plot(exp_data);

for i = 1:exp_data.num_sections
    figure
    resp_name_i = exp_data.sections(i).name;
    coord_i = exp_data.extract_coords(resp_name_i);
    y_i = exp_data.extract_section(exp_data.values, resp_name_i);
    coord_name_i = exp_data.sections(i).axis;
    coord_t_i = train_data.extract_coords(resp_name_i);
    y_t_i = train_data.extract_section(train_data.values, resp_name_i);
    % plot train data
    plot(coord_t_i, y_t_i, 'Color', [0.7, .7, .7])
    hold on
    plot( coord_i, y_i, '-x', 'LineWidth', 2)
    xlabel(coord_name_i)
    ylabel(response_names{i});
    xlim([min(coord_i), max(coord_i)])
    y_min = min([min(y_i), min(y_t_i)]);
    y_max = max([max(y_i), max(y_t_i)]);
    if coord_name_i == 'z'
        set(gcf, 'units','pixel','innerposition',[0 0 500 200])
    else
        set(gcf, 'units','pixel','innerposition',[0 0 2000 300])
    end
    ylim([y_min, y_max])  
    filename = [filename_prep, resp_name_i];
    save_png(gcf, filename, 'figdir', basedir)
    disp(filename)
end
%close all
end