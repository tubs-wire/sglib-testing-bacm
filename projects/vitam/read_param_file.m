function [params, hashes] = read_param_file(name, varargin)
% READ_PARAM_FILE Read a file with parameter values.
%
% Example (<a href="matlab:run_example read_param_file">run</a>)
%
% See also

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

options = varargin2options(varargin, mfilename);
[fileversion, options]  = get_option(options, 'fileversion', 2);
[dirspec, options] = get_option(options, 'dirspec', 'params1');
[ext, options] = get_option(options, 'ext', 'dat');
[ext_params, options] = get_option(options, 'extend', []);
check_unsupported_options(options);

basedir = get_data_dir(dirspec);
filename = fullfile(basedir, [name, '.', ext]);
T=readtable(filename);
params = T{:,1:5}';
hashes = T{:,6}';

if fileversion>=2
    hashes2 = generate_hashes(params);
    if ~isequal(hashes2, hashes)
        error('sglib:read_param_file', 'Trying to read old param file with inconsistent hash values');
    end
end

if ~isempty(ext_params)
    params = [ext_params, params];
end
