function graph_surrogate_accuracy

test_new

function test_new

% gpc, interpolate from lhs, test on qmc (p=1,2)
% gpc, interpolate from qmc, test on lhs (p=1,2)
% gpc, interpolate from qmc+lhs, test on sparse p3 (p=1,2)
% gpc, interpolate from sparse p3, test on lhs (p=1,2,3)
% gpc, project from qmc, test on lhs (p=1,2)
% gpc, project from sparse p3, tet on lhs (p=1,2,3)
% rbf, norm_interpolate from qmc, test on lhs (diff a)
% rbf, norm_interpolate from qmc+sparse, test on lhs (diff a)
% rbf, distance_weighting from qmc, test on lhs (diff a)
% rbf, distance_weighting from lhs, test on qmc (diff a)
% rbf, linfty_min from lhs, test on sparse (diff a)
% rbf, linfty_min from qmc, test on sparse (diff a)

lhs = @load_lhs;
qmc = @load_qmc;
lhs_qmc = @load_lhs_qmc;
sparsep3 = @load_sparse_p3;
rbf_dw = @surr_rbf_dw;
rbf_l1 = @surr_rbf_l1;
rbf_linf = @surr_rbf_linf;
rbf_nint = @surr_rbf_nint;
gpc_int = @surr_gpc_int;
gpc_proj = @surr_gpc_proj;

table = {
    lhs, gpc_int, 1, qmc;
    lhs, gpc_int, 2, qmc;
    lhs, gpc_int, 3, qmc;
    qmc, gpc_int, 1, lhs;
    qmc, gpc_int, 2, lhs;
    qmc, gpc_int, 3, lhs;
    qmc, gpc_proj, 1, lhs;
    qmc, gpc_proj, 2, lhs;
    sparsep3, gpc_proj, 1, lhs;
    sparsep3, gpc_proj, 1, qmc;
    sparsep3, gpc_proj, 1, lhs_qmc;
    sparsep3, gpc_proj, 2, lhs;
    sparsep3, gpc_proj, 3, lhs;
    qmc, rbf_nint, 0.6, lhs;
    qmc, rbf_nint, 1.0, lhs;
    qmc, rbf_nint, 1.6, lhs;
    lhs, rbf_nint, 0.6, qmc;
    lhs_qmc, rbf_nint, 0.6, sparsep3;
    sparsep3, rbf_nint, 0.6, lhs_qmc;
    lhs_qmc, rbf_dw, 0.6, sparsep3;
    lhs_qmc, rbf_dw, 1.0, sparsep3;
    lhs_qmc, rbf_dw, 1.6, sparsep3;
    qmc, rbf_linf, 0.6, lhs;
    sparsep3, rbf_linf, 0.6, lhs_qmc;
    };

tt = "";
for i = 1:size(table,1)
   [train_func, modelgen_func, modelgen_param, val_func] = table{i,:};
   if ~iscell(modelgen_param)
       modelgen_param = {modelgen_param};
   end
   [desc,e1,e2,einf]=generate_errors(train_func, modelgen_func, modelgen_param, val_func);
   disp({desc,e1,e2,einf});
   str = strvarexpand('(abserr=$e1$, maxerr=$einf$)');
   tt = tt + desc + " & " + e1 + " & " + einf + "\\" + newline;
   generate_plot(train_func, modelgen_func, modelgen_param, val_func, str);
end
tt = strrep(tt, ', train=', ' & ');
tt = strrep(tt, ', val=', ' & ');
disp(tt);

function [full_desc,e1_l,e2_l,einf_l]=generate_errors(train_func, modelgen_func, modelgen_param, val_func)
Q = generate_param_set();
[train_data, w_k, tdesc] = train_func(Q);
[model_func, mdesc] = modelgen_func(modelgen_param);
model = model_func(Q, train_data, w_k);
[val_data, wv_l, vdesc] = val_func(Q);
full_desc = [mdesc ', train=' tdesc ', val=' vdesc];

qv_j_l = val_data.params;
up_i_l = model.compute_response(qv_j_l);
uv_i_l = val_data.values;
[e1_l,e2_l,einf_l]=compute_error(up_i_l, uv_i_l);

r=@(x)(round(x, 3, 'significant'));
e1_l = r(e1_l * wv_l);
e2_l = r(sqrt(e2_l.^2 * wv_l));
einf_l = r(max(einf_l));

function [e1,e2,einf]=compute_error(y_p, y_v)
dy = abs(y_p - y_v);
e1 = sum(dy, 1);
e2 = sum(dy.^2, 1).^(1/2);
einf = max(dy, [], 1);

function generate_plot(train_func, modelgen_func, modelgen_param, val_func, str)
Q = generate_param_set();
[train_data, w_k, tdesc] = train_func(Q);
[model_func, mdesc] = modelgen_func(modelgen_param);
model = model_func(Q, train_data, w_k);
[val_data, ~, vdesc] = val_func(Q);


filter = train_data.filters{end};
add = 3; num_plots = 12;
nums = add+(1:num_plots);
full_desc = [mdesc ', train=' tdesc ' val=' vdesc ' ' str];
filename = prepfilename([mdesc ' ' tdesc ' ' vdesc]);
plot_results(train_data, model, val_data, filter, nums, full_desc )

%fullscreen;
set(gcf, 'units','pixel','innerposition',[0 0 800 600])

basedir = get_data_dir('images');
save_png(gcf, filename, 'figdir', basedir, 'notitle', false)
disp(filename)
%dock(gcf);



function test_old
%% Build surrogate model for single response variables
Q = generate_param_set();

train_data = VitamDataHandler();
train_data.add_param_set('complete_qmc_corr');
%train_data.add_param_set('complete_lhs_corr');
%train_data.add_param_set('complete_sparse_p3_corr');
train_data.read_data()

%[q_j_k, w_k] = Q.get_integration_points(3);
%assert(norm(train_data.params-round_params(q_j_k))==0, 'Should match exactly');


val_data = VitamDataHandler();
val_data.add_param_set('complete_qmc_corr');
%val_data.add_param_set('complete_qmc_corr');
val_data.read_data()





%%
kernel_width = 0.7;
model = RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], {kernel_width});
model = RBFSurrogateModel.fromInterpolation(Q, train_data.data{:}, [], {kernel_width});
model = RBFSurrogateModel.fromDistanceWeighting(Q, train_data.data{:}, [], {kernel_width});
model = RBFSurrogateModel.fromLInftyMinimisation(Q, train_data.data{:}, [], {kernel_width});
model = GPCSurrogateModel.fromInterpolation(Q, train_data, Q, 2);
%model = GPCSurrogateModel.fromProjection(train_data, w_k, Q, 2);


filter = train_data.filters{end};
add = 3; num_plots = 12;
%add = 11; num_plots = 4;
nums = add+(1:num_plots);
plot_results(train_data, model, val_data, filter, nums)


%#ok<*DEFNU>
function [data, w_k] = load_data(names)
data = VitamDataHandler();
for i=1:length(names)
    data.add_param_set(names{i});
end
data.read_data();
n = data.num_samples();
w_k = (1.0/n) * ones(n, 1);
    

function [data, w_k, desc] = load_lhs(Q)
[data, w_k] = load_data({'complete_lhs_corr'});
desc = 'LHS';

function [data, w_k, desc] = load_qmc(Q)
[data, w_k] = load_data({'complete_qmc_corr'});
desc = 'QMC';

function [data, w_k, desc] = load_lhs_qmc(Q)
[data, w_k] = load_data({'complete_lhs_corr', 'complete_qmc_corr'});
desc = 'LHS+QMC';

function [data, w_k, desc] = load_sparse_p3(Q)
[data, ~] = load_data({'complete_sparse_p3_corr'});
[q_j_k, w_k] = Q.get_integration_points(3);
assert(norm(data.params-round_params(q_j_k))==0, 'Should match exactly');
desc = 'Sparse (P=3)';


function [model_func, desc] = surr_rbf_l1(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromL1Minimisation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF L_1 approx (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_linf(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromLInftyMinimisation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF L_\infty approx (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_dw(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromDistanceWeighting(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF distance weighting (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_rbf_nint(varargin)
model_func = @(Q, train_data, w_k)RBFSurrogateModel.fromNormInterpolation(Q, train_data.data{:}, [], varargin{:});
desc = strvarexpand('NRBF interpolation (r=$varargin{1}{1}$)');

function [model_func, desc] = surr_gpc_int(varargin)
model_func = @(Q, train_data, w_k)GPCSurrogateModel.fromInterpolation(Q, train_data.data{:}, varargin{:}{:});
desc = strvarexpand('GPC interpolation (p=$varargin{1}{1}$)');

function [model_func, desc] = surr_gpc_proj(varargin)
model_func = @(Q, train_data, w_k)GPCSurrogateModel.fromProjection(Q, train_data.data{:}, w_k, varargin{:}{:});
desc = strvarexpand('GPC projection (p=$varargin{1}{1}$)');



function plot_results(train_data, model, val_data, filter, nums, title)
if nargin<6
    title=[];
end

multiplot_init(length(nums), [], 'title', title)
for num=nums
    multiplot
    show_result(train_data, model, val_data, num, filter)
end


function show_result(train_data, model, val_data, num, filter)

qv_j_k = val_data.params;

up_i_l = model.compute_response(qv_j_k(:,num));
up_is_l = train_data.extract_section(up_i_l, filter);

xs_i = train_data.extract_coords(filter);

ut_is_k = train_data.extract_values(filter);
ut_is_min = min(ut_is_k,[],2);
ut_is_max = max(ut_is_k,[],2);

uv_is_k = val_data.extract_values(filter);

hold off;
show_comparison(xs_i, up_is_l, uv_is_k(:,num), ut_is_min, ut_is_max)

function show_comparison(x, y_predicted, y_validate, y_train_min, y_train_max)
% Plot training data as reference
plot_between(x, y_train_min, y_train_max, 0.9*[1,1,1])
hold all;

%plot(xf, yi, 'k'); hold all;

plot(x, y_predicted, 'r', 'LineWidth', 2)
plot(x, y_validate, 'b--', 'LineWidth', 2)
%legend('Range', 'Prediction', 'Exact', 'Location', 'SouthEast')

d = 0.4;
plot_between(x, 0*y_predicted-d, abs(y_predicted - y_validate)-d, 0.94*[1,1,1])

