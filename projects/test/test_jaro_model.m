hold off;

q = linspace(-3, 2);
y = M(q);
plot(y, q); grid on; hold on;
xlabel('y')
ylabel('q')


q_p = nan(size(q));
for i = 1:length(y)
    q_p(i) = exp_q_given_ym(y(i));
    q_p(i)
end
plot(y, q_p, 'r.');
xlabel('y');
ylabel('q');

phi = get_estimator(5);
q_p2 = phi(y);
if max(q_p2 - q_p)<0.03
    q_p2 = q_p2 + 0.03;
end
plot(y, q_p2, 'k.-');

hold off;

xlim([-2,2]); ylim([-2,2]); axis equal;

function EQ = exp_q_given_ym(y_m)
p_ym = pi_ym(y_m);
EQ = integral( @(q)(q .* pi_ym_q(y_m, q) ./ p_ym), -3, 3); %inf, inf);
%EQ = quadgk( @(q)(q .* pi_ym_q(y_m, q) ./ p_ym), -3, 3); %inf, inf);
end

function p = pi_ym(y_m)
    p = integral(@(q)pi_ym_q(y_m, q), -3, 3); %inf, inf);
    %p = quadgk(@(q)pi_ym_q(y_m, q), -3, 3); %inf, inf);
end

function y = M(q)
y = (q+1/2).^3 + q/2;
y = 0.6*(q+3/2).^3 + q/2+1.3;
%y = nthroot(q+1/2, 3) + q/2;
end

function p = pi_ym_q(y_m, q)
p = pi_ym_given_q(y_m, q) .* pi_q(q);
end

function p = pi_ym_given_q(y_m, q)
p = normal_pdf(y_m, M(q), 0.4);
end

function p = pi_q(q)
p = normal_pdf(q, 0, 1);
end


function phi_func = get_estimator(p)
n = p+1;
A = nan(n, n);
b = nan(n, 1);
for i=1:n
    b(i) = integral2(@(q,e)( q .* (M(q) + e).^(i-1) .* ...
        normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
    for j=1:i
        A(i,j) = integral2(@(q,e)( (M(q) + e).^(j-1) .* (M(q) + e).^(i-1) .* ...
            normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
        A(j,i) = A(i,j);
    end
end
phi_i = A \ b;

phi_func = @(y)(phi_i' * y.^((0:n-1)'));
1;
%%
delta = 1e-3*rand(size(phi_i)).*phi_i
compute_mse(phi_func)
compute_mse(@(y)((phi_i + delta)' * y.^((0:n-1)')))

% y_i = linspace(-10, 10, 6);
% q_i = arrayfun(@exp_q_given_ym, y_i);
% phi_interp = polyfit(y_i, q_i, 5)
% function q=foobar(y)
%     q = polyval(phi_interp, y)
% end
% mse = compute_mse(@foobar)
% %mse = compute_mse(@(y)(polyval(phi_interp, y)))
% 
% %compute_mse(@(y)(arrayfun(@exp_q_given_ym, y)))
% %%
% 1;
% 
% phi1 = rand(size(phi_i)) .* phi_i;
% phi2 = phi1 + 1e-10 * rand(size(phi_i));
% mse1 = compute_mse(@(y)(phi1)' * y.^((0:n-1)'))
% mse2 = compute_mse(@(y)(phi2)' * y.^((0:n-1)'))
% phi = 0.5*(phi1 + phi2);
% [(phi2 - phi1)' * 2*(A*phi - b), mse2 - mse1]

%%
end


function err = compute_mse(func)
    function err = mse(func, q, e)
        err = (func(M(q(:)') + e(:)') - q(:)') .^ 2;
        err = reshape(err, size(q));
    end
	err = integral2(@(q,e)( mse(func, q, e).* ...
    normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
end
