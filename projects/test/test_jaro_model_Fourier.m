hold off;
% parameter values
q = linspace(-3, 2);
% corresponding observations
y = M(q);
plot(y, q); grid on; hold on;

%% E[Q_Y]: Mean of posterior computed from numerical integration
q_p = nan(size(q));
for i = 1:length(y)
    q_p(i) = exp_q_given_ym(y(i));
    q_p(i)
end
plot(y, q_p, 'r.');
xlabel('y');
ylabel('q');

%% E[Q_Y] with MMSE using polynomials
% with polynomials
d = 5;
phi = get_estimator(d);
q_p2 = phi(y);
if max(q_p2 - q_p)<0.03
    q_p2 = q_p2 + 0.03;
end
plot(y, q_p2, 'k.-');
%% Plot samples of observation operator

N = 100000;
% samples of meas. error
e_j = normal_sample(N, 0, 0.4);
% sample of the parameter
q_j = normal_sample(N, 0, 1);
% samples of the observation operator
y_j = M(q_j) + e_j;
scatter1 = scatter(y_j,q_j,'MarkerFaceColor','g','MarkerEdgeColor','none', 'MarkerFaceAlpha', min(0.5, 300 / N)); 
xlim([min(y)-2,max(y)+1]); ylim([-3,2]); axis square;

%% E[Q_Y] with MMSE Fourier
% scaling of the observatior
p = 30;
%y1 = min(y_j*1.5);
%y2 = max(y_j*1.5);
y1 = min(y);
y2 =max(y);
scale_y = @(y)pi*(2*(y-y1)/(y2-y1) - 1);
phi = get_Fourier_estimator(p, scale_y);
q_p3 = phi(y);
if max(q_p3 - q_p)<0.03
    q_p3 = q_p3 + 0.03;
end
plot(y, q_p3, 'c.-');

%% Check Fourier regression
[a,b,q_f] = Fseries(y,q_p,20);
hold on
plot(y,q_f, 'LineWidth', 2)



compute_mse(@(y)arrayfun(@exp_q_given_ym,y))
compute_mse(phi)
compute_mse(phi_interp)




function EQ = exp_q_given_ym(y_m)
p_ym = pi_ym(y_m);
EQ = integral( @(q)(q .* pi_ym_q(y_m, q) ./ p_ym), -3, 3); %inf, inf);
end

function p = pi_ym(y_m)
p = integral(@(q)pi_ym_q(y_m, q), -3, 3); %inf, inf);
end

function y = M(q)
y = (q+1/2).^3 + q/2;
%y = 0.6*(q+3/2).^3 + q/2+1.3;
%y = nthroot(q+1/2, 3) + q/2;
end

function p = pi_ym_q(y_m, q)
p = pi_ym_given_q(y_m, q) .* pi_q(q);
end

function p = pi_ym_given_q(y_m, q)
p = normal_pdf(y_m, M(q), 0.4);
end

function p = pi_q(q)
p = normal_pdf(q, 0, 1);
end


function phi_func = get_estimator(p)
n = p+1;
A = nan(n, n);
b = nan(n, 1);
for i=1:n
    b(i) = integral2(@(q,e)( q .* (M(q) + e).^(i-1) .* ...
        normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
    for j=1:i
        A(i,j) = integral2(@(q,e)( (M(q) + e).^(j-1) .* (M(q) + e).^(i-1) .* ...
            normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
        A(j,i) = A(i,j);
    end
end
phi_i = A \ b;

phi_func = @(y)(phi_i' * y.^((0:n-1)'));

end

function phi_func = get_Fourier_estimator(p, scale_y)
% number of basis functions
n = 2*p+1;
% Define basis functions
N = cell(n, 1);
N{1} = @(y)(0.5*ones(size(y)));
for i = 1:p
    N{i+1} = @(y)(cos(i*scale_y(y)));
    N{p+i+1} =  @(y)(sin(i*scale_y(y)));
end


A = nan(n, n);
b = nan(n, 1);
% compute matrices in the system of equations by numerical integration
for i=1:n
    b(i) = integral2(@(q,e)( q .* N{i}(M(q) + e) .* ...
        normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
    for j=1:i
        A(i,j) = integral2(@(q,e)( N{i}(M(q) + e) .* N{j}(M(q) + e) .* ...
            normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
        A(j,i) = A(i,j);
    end
end
phi_i = A \ b;

phi_func = @(y)(phi_i'*cell2mat(cellfun(@(func) func(y), N ,'UniformOutput',false)));
end


function err = compute_mse(func)
    function err = mse(func, q, e)
        err = (func(M(q(:)') + e(:)') - q(:)') .^ 2;
        err = reshape(err, size(q));
    end
err = integral2(@(q,e)( mse(func, q, e).* ...
    normal_pdf(q, 0, 1) .* normal_pdf(e, 0, 0.4)), -3, 3, -2, 2);
end
