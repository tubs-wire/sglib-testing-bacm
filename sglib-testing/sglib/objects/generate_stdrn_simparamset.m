function paramset=generate_stdrn_simparamset(sigmas)
% Generate parameterset with independent normal parameters
% The input is a vector with the sigmas (std not the variance)
n=length(sigmas);
paramset=MySimParamSet();

for i=1:n
    param_name_i=strvarexpand('pn_$i$');
    paramset.add(param_name_i, NormalDistribution(0,sigmas(i)));
end

end