function h = plot_between(x, y_min, y_max, varargin)
% PLOT_BETWEEN Fills an area between some min and max values
%   PLOT_BETWEEN(X, Y_MIN, Y_MAX, COLOR) fills the area between Y_MIN and
%   Y_MAX for all X with color COLOR.
%
% Example (<a href="matlab:run_example plot_between">run</a>)
%   x = linspace(0, 2*pi, 100);
%   plot_between(x, sin(x), cos(x), [0.3, 0.6, 0.8])
%
% See also PLOT

%   Elmar Zander
%   Copyright 2018, Inst. of Scientific Computing, TU Braunschweig
%
%   This program is free software: you can redistribute it and/or modify it
%   under the terms of the GNU General Public License as published by the
%   Free Software Foundation, either version 3 of the License, or (at your
%   option) any later version. 
%   See the GNU General Public License for more details. You should have
%   received a copy of the GNU General Public License along with this
%   program.  If not, see <http://www.gnu.org/licenses/>.

x = x(:); y_min = y_min(:); y_max = y_max(:);
h=fill([x; flip(x)], [y_min; flip(y_max)], varargin{:})
